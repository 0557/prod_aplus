<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

        Router::parseExtensions('html', 'rss', 'xml','csv'); 
		//Router::parseExtentions('xml');
		Router::connect('/sitemap', array('controller' => 'sitemaps', 'action' => 'index'));
		
       // Router::connect('/', array('controller' => 'users', 'action' => 'home'));
		
		Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
		
	    Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin'=>true));
		Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin'=>true));
		
		 App::import('Model', 'Page');
		 $rout_page_obj=new Page();
		 $pages_data=$rout_page_obj->find('all',array("conditions"=>array("Page.status"=>'1'),"fields"=>array("Page.id","Page.id"),"recursive"=>"-1"));
		 $arr=array();
		 foreach($pages_data as $page)
		 {
		   $arr[]=$page['Page']['id'];
		 }
		
		 $router_pagealias=implode("|",$arr);
		  
		 Router::connect(
			'/:alias',
			array(
			   'controller' => 'pages',
				'action' => 'view',
				'ext'=>"html"
			),
			array(
				'alias' => $router_pagealias,
				'pass' => array(
					'alias'
				)
			)
		 );
 


/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
