<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

            Router::parseExtensions('html', 'rss', 'xml','csv'); 
		//Router::parseExtentions('xml');
            Router::connect('/sitemap', array('controller' => 'sitemaps', 'action' => 'index'));
		
	    Router::connect('/', array('controller' => 'homes', 'action' => 'home',));
            
            Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin'=>true));
            
            Router::connect("/admin/vendors/add", array('controller' => 'customers', 'action' => 'addVendor', 'admin'=>true));
           
            Router::connect("/admin/vendors", array('controller' => 'customers', 'action' => 'vendor', 'admin'=>true));
            
            Router::connect("/admin/vendors/edit/:id", array('controller' => 'customers', 'action' => 'vendorEdit'  ,'admin'=>true),array('pass'=>array('id')));
            
            Router::connect("/admin/vendors/view/:id", array('controller' => 'customers', 'action' => 'viewVendor'  ,'admin'=>true),array('pass'=>array('id')));
	
	
		
	

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();


/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
