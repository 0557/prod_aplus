<?php
//error_reporting(0);
App::uses('AppController', 'Controller');

class ActiveConfirmRptsController extends AppController {

    public $components = array('Paginator', 'Flash', 'Session');

    public function admin_index() {
		
        $this->Setredirect();    
        $full_date = '';
		$form_date='';
		$to_date='';
		$status='';
		 $conditions = array('ActiveConfirmRpt.store_id' => $this->Session->read('stores_id')); 
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['ActiveConfirmRpt']['full_date']!= "" || $this->request->data['ActiveConfirmRpt']['status']!= "") {			
				//echo 'ok';die;
				$this->Session->write('PostSearch', 'PostSearch');
				if($this->request->data['ActiveConfirmRpt']['full_date']!= ""){
				$full_date=$this->request->data['ActiveConfirmRpt']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}			
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);
				}
				else {
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}
				
				
				if($this->request->data['ActiveConfirmRpt']['status']!=''){
				$this->Session->write('status',$this->request->data['ActiveConfirmRpt']['status']);	
		        }else{
		        $this->Session->delete('status');	
				}
						
				}else{
				$this->Session->delete('PostSearch');
				}		

            if ($this->Session->check('PostSearch')) {			
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');
				$status=$this->Session->read('status');
            }
             
		   if($status!='' && $status!='all'){	
		        //echo $status;die;			
                $conditions[] = array('ActiveConfirmRpt.status' =>$status);			
				
			}
           
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'DATE(ActiveConfirmRpt.confirm_date_time) >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'DATE(ActiveConfirmRpt.confirm_date_time) <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
			if($this->request->data['ActiveConfirmRpt']['full_date']!= "" || $this->request->data['ActiveConfirmRpt']['status']!= "" ) {	
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'ActiveConfirmRpt.id' => 'desc'
                ),
				'limit' =>500
            );
			$this->set('full_date',$full_date);				
			//echo '<pre>';print_r($this->Paginator->paginate());die;
            $this->set('games', $this->Paginator->paginate()); 
			}else{
			$this->set('full_date',$full_date);	
            $this->set('games', '');
			}
			           
        } else {
            $this->set('full_date',$full_date);	
            $this->set('games', '');
        }
    	
	}

    
    public function admin_reset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('start_date');
        $this->Session->delete('end_date');
        $this->Session->delete('status');
        $redirect_url = array('controller' => 'active_confirm_rpts', 'action' => 'admin_index');
        return $this->redirect($redirect_url);
    }
}
