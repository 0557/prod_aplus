<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $coffee_shop_data;
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'RequestHandler',
        'Default',
        'Email',
        'Flash'
    );

    /**
     * Helpers
     *
     * @var array
     * @access public
     */
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Text',
        'Js',
        'Time',
        'thumbnail',
        'Custom'
            //'Layout'
    );

    public function __construct($request = null, $response = null) {

        parent::__construct($request, $response);
        if ($this->name == 'CakeError') {
            $this->_set(Router::getPaths());
            $this->request->params = Router::getParams();
            $this->constructClasses();
            $this->startupProcess();
        }
    }

    /**
     * beforeFilter
     *
     * @return void
     * @throws MissingComponentException
     */
    public function beforeFilter() {

		header('pragma: no-cache'); 
	    header('Cache-Control: no-cache, must-revalidate'); 
		$this->response->disableCache(); 
		
        parent::beforeFilter();

        Configure::write('Config.language', 'eng');

        App::import('Model', 'Setting');
        $setting_model_obj = new Setting();
        $settings = $setting_model_obj->getKeyValuePairs();
   //     pr($settings); die;
        Configure::write($settings);

        $this->_checkAuth();

        $this->RequestHandler->setContent('json', 'text/x-json');

        if (isset($this->request->params['admin']) && $this->name != 'CakeError') {
            $this->layout = 'admin';
        }



        if ($this->RequestHandler->isAjax()) {
            $this->layout = 'ajax';
        }


        if (!isset($this->request->params['admin']) &&
                Configure::read('site.status') == 1) {
            $this->layout = 'maintenance';
            $this->set('title_for_layout', __('Site down for maintenance'));
            $this->render('../Elements/blank');
        }

        if (isset($this->request->params['locale'])) {
            Configure::write('Config.language', $this->request->params['locale']);
        }
		
		$ruby_fproducts= array('Regular' =>'Regular', 'Plus' =>'Plus', 'Super' => 'Super', 'Premium' => 'Premium', 'Diesel' => 'Diesel', 'KERSN' => 'Kerosene', 'Off Road' => 'Off Road');
        $this->set('ruby_fproducts',$ruby_fproducts);
    }

    public function _checkAuth() {
		$is_admin = false;
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
            $is_admin = true;
        }
		
		$actionNotLogin = ($this->params['action'] == 'admin_login') ? true : false;

 	    if (!$actionNotLogin && $is_admin && !$this->Auth->user('id')) {
			$this->Session->setFlash(__('Authorization Required'));
			$this->redirect(array(
				'controller' => 'users',
				'action' => 'login',
				'admin' => true
			));
	    } else {
			$this->Auth->allow(); // ALLOW Login:
		}
		
        $this->set('UsersDetails', $this->Auth->user());
    }
	
	function getStoreId()
	{
		return $this->Session->read('store_id');
	}
	
	public function setFilterProductNumber($store_id)
	{
		$this->loadModel('RubyFprod');
		
		return $this->RubyFprod->find('list', array('conditions' => array('status' => 1, 'store_id' => $store_id), 'fields' => array('fuel_product_number', 'display_name')));
	}
	

    public function Setredirect() {
		$storeId = $this->Session->read('store_id');
        if (!$storeId) {
			$this->Session->setFlash(__('Please Select store'), 'default', array('class' => 'error'));
            $this->redirect(array('controller' => 'users', 'action' => 'index', 'admin' => true));
        }
    }
    
    public function alredyLogin() {
	   if(($this->Auth->user('id') != null))
		{
			$this->redirect(array('controller' => 'users', 'action' => 'index','admin'=>true));
		}
    }
	
	public function admin_dashboard()
	{
		$this->loadModel('TankReports');
		$this->loadModel('MopReports');
		$this->loadModel('TpReports');
		/* TpReports */
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{ $options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		 
			if($data = $this->TpReports->find('first', $options)){
				
			$this->set('tpdate', $data['TpReports']['createdate']);
	
		$options = array('conditions' => array('createdate'=>$data['TpReports']['createdate'],'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TpReports->find('all', $options);
		$this->set('treport', $list);
		}else{
			$this->set('tpdate', '');
		}
		
		/* MopReports */
		 $options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		 
			if($data = $this->MopReports->find('first', $options)){
				$this->set('mpdate', $data['MopReports']['createdate']);
			
	
		$options = array('conditions' => array('createdate'=>$data['MopReports']['createdate'],'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->MopReports->find('all', $options);
		
		//pr($list); die;
		$this->set('mreports',$list);
		}else{
			$this->set('mpdate', '');
		}
		/* TankReports */
		 $options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		 
			if($data = $this->TankReports->find('first', $options)){
				
			$this->set('tnpdate', $data['TankReports']['createdate']);
	
		$options = array('conditions' => array('createdate'=>$data['TankReports']['createdate'],'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TankReports->find('all', $options);
		$this->set('tnreports', $list);
	
		}else{
			$this->set('tnpdate', '');
		}
		}
	}
	
		

	
    public function appError($error) {
	$this->redirect(array('controller' => 'Errorexception', 'action' => 'index'));
	}
}
