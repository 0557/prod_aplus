<?php 
App::uses('AppController', 'Controller');

class BanksController extends AppController {

    public $components = array('Paginator', 'Session', 'Flash');
   
	public function admin_index() {	
		$this->Setredirect();
	
		$this->loadModel('Bank');  
			$search = '';
		    $conditions= array();		
			
			if($this->request->is('post')){				
				if(!empty($this->request->data['Bank']) && $this->request->data['Bank']['search'] != "" ) {
						$this->Session->delete('PostSearch');					
						$this->Session->delete('search');
						$this->Session->write('PostSearch', 'PostSearch');					
						$this->Session->write('search', $this->request->data['Bank']['search']);					
				} else {
						$this->Session->delete('PostSearch');					
						$this->Session->delete('search');
				}
			}
			
			
			if ($this->Session->check('PostSearch')) {
				$PostSearch = $this->Session->read('PostSearch');				
				$search = $this->Session->read('search');
			}
			
			if(isset($search) && $search!=''){
				//$conditions['Bank.name'] =$search;		
				
				$conditions = array(
					'Bank.name LIKE' =>  "%".$search."%" ,
				);
				
							
			}
		    //print_r($conditions);die;
			$this->Paginator->settings = array(
				'conditions' => $conditions,
				'limit' => 20,
				'order'=>array('Bank.id'=>'desc')
			);
			$data=$this->Paginator->paginate('Bank');	
    	    //print_r($data);die;
        $this->set('Banks', $data);
    }
	public function admin_reset() {	
	    //echo 'gfg';die;	
		$this->Session->delete('PostSearch');		
		$this->Session->delete('search');			
		$redirect_url = array('controller' => 'banks', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}
   

    public function admin_add() {
		
		  //echo 'fhgfj';die;
		  $this->loadModel('Bank');
          $this->Setredirect();
        if ($this->request->is('post')) {			      
   
            $exsists_bank=$this->Bank->find('first',array('conditions' => array('Bank.name'=>$this->request->data['Bank']['name'])));			
			
			if(!empty($exsists_bank)){
				 $this->Session->setFlash(__('This bank already exists with same name: '.$this->request->data['Bank']['name']), 'default', array('class' => 'error'));            
			}else{						
			
            if ($this->Bank->save($this->request->data)) {	
			//echo 'dfgd'; die;				
                //$this->Session->setFlash(__('The bank has been saved.'));
                //$this->redirect(array('action' => 'index'));
				
				$this->Session->setFlash(__('The bank has been saved.'));
                $this->redirect(array('action' => 'index'));;
				
            } else {					
                $this->Session->setFlash(__('Bank Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
               
            }
			}
        }
	
    }
	

    public function admin_edit($id = null) {
		  $this->loadModel('Bank');
          $this->Setredirect();
        if (!$this->Bank->exists($id)) {
            throw new NotFoundException(__('Invalid bank'));
        }
		
         if ($this->request->is('post')) {			      
   
            $exsists_bank=$this->Bank->find('first',array('conditions' => array('Bank.name'=>$this->request->data['Bank']['name'])));			
			
			if(!empty($exsists_bank)){
				 $this->Session->setFlash(__('This bank already exists with same name: '.$this->request->data['Bank']['name']), 'default', array('class' => 'error'));            
			}else{						
			$this->Bank->id = $id;
            if ($this->Bank->save($this->request->data)) {					
                $this->Session->setFlash(__('The bank has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {					
                $this->Session->setFlash(__('Bank Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
               
            }
			}
        }else{
        $edit_bank=$this->Bank->find('first',array('conditions' => array('Bank.id'=>$id)));		
		$this->set('edit_bank', $edit_bank);
		}
		
		
    }

   
    public function admin_delete($id = null) {		
		$this->loadModel('Bank'); 
        $this->Bank->id = $id;
        if (!$this->Bank->exists()) {
            throw new NotFoundException(__('Invalid bank'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Bank->delete()) {
            $this->Session->setFlash(__('The bank has been deleted.'));
        } else {
            $this->Session->setFlash(__('Bank Could Not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        $this->redirect(array('action' => 'index'));
    }
	

	

}
