<?php
ob_start();
App::uses('AppController', 'Controller');

class CashExpensesController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->loadmodel('CashExpense');  
        $full_date = '';
		$form_date='';
		$to_date='';
		$status='';
		$conditions = array('CashExpense.store_id' => $this->Session->read('stores_id'),'CashExpense.company_id' => $this->Session->read('Auth.User.company_id')); 
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 				
				$this->Session->write('PostSearch', 'PostSearch');
				if($this->request->data['CashExpense']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['CashExpense']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
				
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);
				} else {
			    //echo 'notok';die;
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}
        }
		
		 if ($this->Session->check('PostSearch')) {
				//echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');				
            }
		
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'DATE(CashExpense.report_date) >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'DATE(CashExpense.report_date) <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
		
	        $this->paginate = array('conditions' => $conditions,
			    'limit' => 100,
                'order' => array(
                    'CashExpense.id' => 'desc'
                )
            );
			//echo '<pre>';print_r($this->Paginator->paginate());die;
			$this->set('full_date',$full_date);	
			$this->set('status',$status);	
            $this->set('cashexpenses',$this->Paginator->paginate()); 
	
	
	}
    
    public function admin_reset() {       
        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
		$this->Session->delete('from_date');
		$this->Session->delete('to_date');	
        $redirect_url = array('controller' => 'cash_expenses', 'action' => 'index');
        return $this->redirect($redirect_url);
    }
    

    public function admin_delete($id = null) {
        if ($id) {
            if ($this->CashExpense->delete($id)) {
                $this->Session->setFlash(__('Cash expenses successfully Deleted'), 'default', array('class' => 'success'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Cash expenses not be deleted'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            }
        }
    }

    public function admin_add() {
        $this->Setredirect();
		
		    $this->loadModel('OtherCategories');
			$categories=$this->OtherCategories->find('list',array('conditions'=>array('OtherCategories.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherCategories.id','OtherCategories.category')));
			$this->set('categories',$categories);
			
        if ($this->request->is('post')) {
            //echo '<pre>';print_r($this->request->data);die;

            $this->request->data['CashExpense']['store_id'] = $this->Session->read('stores_id');
            $this->request->data['CashExpense']['company_id'] = $this->Session->read('Auth.User.company_id');
			$this->request->data['CashExpense']['report_date'] = date('Y-m-d');
            $this->request->data['CashExpense']['created'] = date('Y-m-d');

            //echo '<pre>'; print_r($this->request->data);die;
            $this->CashExpense->create();
            if ($this->CashExpense->save($this->request->data)) {
                $this->Session->setFlash(__('Cash expenses successfully added'), 'default', array('class' => 'success'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Cash expenses could not be saved. Please, try again.'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            }
        }
		
		
    }

    public function admin_edit($id = null) {
        $this->Setredirect();
        if (!$this->CashExpense->exists($id)) {
            throw new NotFoundException(__('Invalid report'));
        }
		
		$this->loadModel('OtherCategories');
			$categories=$this->OtherCategories->find('list',array('conditions'=>array('OtherCategories.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherCategories.id','OtherCategories.category')));
			$this->set('categories',$categories);
			
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['CashExpense']['id'] = $id;           
            $this->request->data['CashExpense']['modified'] = date('Y-m-d');
            //$this->CashExpense->create();

            if ($this->CashExpense->save($this->request->data)) {

                $this->Session->setFlash(__('Cash expenses successfully updated'), 'default', array('class' => 'success'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Cash expenses could not be updated'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('CashExpense.' . $this->CashExpense->primaryKey => $id));
            $this->request->data = $this->CashExpense->find('first', $options);
            if ($this->request->data['CashExpense']['store_id'] != $this->Session->read('stores_id') || $this->request->data['CashExpense']['company_id'] != $this->Session->read('Auth.User.company_id')) {
                $this->Session->setFlash(__('Store or company not matched.'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'cash_expenses', 'action' => 'index'));
            }
            //echo '<pre>';print_r($this->request->data);die;

            $this->set('CashExpense', $this->request->data);
        }
    }

}
