<?php
App::uses('AppController', 'Controller');
/**
 * Companies Controller
 *
 * @property Company $Company
 * @property PaginatorComponent $Paginator
 */
class CompaniesController extends AppController {
   public $uses=array("Company","SitePermission");
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if(!$this->SitePermission->CheckPermission($this->Auth->user('id'),'companies','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	          $this->redirect(array('controller'=>'users','action' => 'dashboard'));
        }
	 
		$this->Company->recursive = 0;
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Companies');
		$this->paginate = array("limit" => 5, "order" => "Company.id DESC");
		$this->set('companies', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if(!$this->SitePermission->CheckPermission($this->Auth->user('id'),'companies','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	          $this->redirect(array('controller'=>'users','action' => 'dashboard'));
        }
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'View Company');
		
		
		
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
		$this->set('company', $this->Company->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if(!$this->SitePermission->CheckPermission($this->Auth->user('id'),'companies','is_add')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	          $this->redirect(array('controller'=>'users','action' => 'dashboard'));
        }
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Add Company');
		
		if ($this->request->is('post')) {
			$this->Company->create();
			
			if ($this->Company->save($this->request->data)) {
				$this->loadModel('User');
				$this->User->unbindValidation('remove',array('first_name','last_name','phone','confirm_password'));
				$user_data=array();
				$user_data['User']['email']=$this->request->data['Company']['email'];
				$user_data['User']['company_id']=$this->Company->id;
				$user_data['User']['password']=$this->request->data['Company']['password'];
				$user_data['User']['username']=$this->request->data['Company']['name'];
				$user_data['User']['role_id']=5;
				$user_data['User']['status']=1;
				$this->User->create();
				$this->User->save($user_data);
			
				$this->Flash->success(__('The company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The company could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if(!$this->SitePermission->CheckPermission($this->Auth->user('id'),'companies','is_edit')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	          $this->redirect(array('controller'=>'users','action' => 'dashboard'));
        }
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Edit Company');
		
		
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Company->save($this->request->data)) {
				$this->loadModel('User');
				$this->User->unbindValidation('remove',array('first_name','last_name','phone','confirm_password'));
				$user_data=array();
				$user_data['User']['email']=$this->request->data['Company']['email'];
				$user_data['User']['id']=$this->request->data['Company']['user_id'];
				if(isset($this->request->data['Company']['password']) && $this->request->data['Company']['password']!=''){
			 	 $user_data['User']['password']=$this->request->data['Company']['password'];
				}
				
				$this->User->save($user_data);
				
				
				$this->Flash->success(__('The company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The company could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
			$this->request->data = $this->Company->find('first', $options);
			  $user = $this->Company->User->find('first', array(
				'conditions' => array("User.role_id"=>"5","User.company_id"=>$id),
				'recursive' => -1
              ));
			  $this->request->data['Company']['email']=$user['User']['email'];
			  $this->request->data['Company']['user_id']=$user['User']['id'];
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if(!$this->SitePermission->CheckPermission($this->Auth->user('id'),'companies','is_delete')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	          $this->redirect(array('controller'=>'users','action' => 'dashboard'));
        }
		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Invalid company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Company->delete()) {
			$this->Flash->success(__('The company has been deleted.'));
		} else {
			$this->Flash->error(__('The company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
