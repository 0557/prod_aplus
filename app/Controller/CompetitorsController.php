<?php

App::uses('AppController', 'Controller');

/**
 * Competitors Controller
 *
 * @property Competitor $Competitor
 * @property PaginatorComponent $Paginator
 */
class CompetitorsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->set('competitorss', 'active');
        if(isset( $_SESSION['store_id']) && !empty($_SESSION['store_id'])) {
         $this->paginate = array('conditions'=>array(/*'Competitor.corporation_id'=>$_SESSION['corporation_id'],*/'or'=>array('Competitor.store_id'=>$_SESSION['store_id'])),"limit" => 5, "order" => "Competitor.id DESC");   
        }else{
        $this->paginate = array(/*'conditions'=>array('Competitor.corporation_id'=>$_SESSION['corporation_id']),*/"limit" => 5, "order" => "Competitor.id DESC");
       
        }
        $this->set('competitors', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->set('competitorss', 'active');
        $id = base64_decode($id);
        if (!$this->Competitor->exists($id)) {
            throw new NotFoundException(__('Invalid competitor'));
        }
        $options = array('conditions' => array('Competitor.' . $this->Competitor->primaryKey => $id));
        $this->set('competitor', $this->Competitor->find('first', $options));
    }

    public function admin_add() {
        $this->set('competitorss', 'active');

        if ($this->request->is('post')) {
            $this->Competitor->create();
           // $this->request->data['Competitor']['corporation_id'] = $_SESSION['corporation_id'];
            
            if ($this->Competitor->save($this->request->data)) {
                $this->Flash->success(__('The competitor has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The competitor could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Competitor->Country->find('list');
        $stores = $this->Competitor->Store->find('list'/*,array('conditions'=>array('Store.corporation_id'=>$_SESSION['corporation_id']))*/);
        $this->set(compact('countries', 'stores'));
    }

    public function admin_edit($id = null) {
        
        $this->set('competitorss', 'active');
        $id = base64_decode($id);

        if (!$this->Competitor->exists($id)) {
            throw new NotFoundException(__('Invalid competitor'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Competitor->save($this->request->data)) {
                $this->Flash->success(__('The competitor has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The competitor could not be saved. Please, try again.'));
            }
        } 
            $options = array('conditions' => array('Competitor.' . $this->Competitor->primaryKey => $id));
            $this->request->data = $this->Competitor->find('first', $options);
            //pr($this->request->data);
           // die('i m here');
       
        $countries = $this->Competitor->Country->find('list');
        $stores = $this->Competitor->Store->find('list'/*,array('conditions'=>array('Store.corporation_id'=>$_SESSION['corporation_id']))*/);
        $states = $this->Competitor->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
        if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
            $cities = $this->Competitor->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
        }
          if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
            $ZipCode = $this->Competitor->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code') ));
        }
        $this->set(compact('countries', 'stores','states','cities','ZipCode'));
    }

    public function admin_delete($id = null) {
        $this->set('competitorss', 'active');
        $id = base64_decode($id);
        $this->Competitor->id = $id;
        if (!$this->Competitor->exists()) {
            throw new NotFoundException(__('Invalid competitor'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->Competitor->delete()) {
            $this->Flash->success(__('The competitor has been deleted.'));
        } else {
            $this->Flash->error(__('The competitor could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
