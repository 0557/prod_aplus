<?php
ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class ComplianceController extends AppController {

    public $components = array('Paginator');
   var $uses = array("User");

    public function admin_index() {

			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select  store.')); 
			}else{
				
				return $this->redirect(array('action' => 'notification'));
				
			}
    
	}

	public function admin_notification($id = null){
	  $this->loadModel('ComplianceNotification');
	  $this->loadModel('Store');
	  $this->loadModel('User');
      $this->set('notification', 'active');
			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select  store.')); 
				return $this->redirect(array('action' => 'index'));

			}
		
	
				if($id){
				if($this->ComplianceNotification->delete($id)){
						
						$this->Session->setFlash(__('Notification has been deleted.'));
						return $this->redirect(array('action' => 'notification'));
						
					}
					
				}


			$notification = $this->ComplianceNotification->find('all', array('conditions' => array('Store_id' => $this->Session->read('stores_id'),'company_id'=>$this->Session->read('Auth.User.company_id'))));
				$this->set('list', $notification);
			}
			
			
			public function admin_new_notification($id = null){
				
		  $this->loadModel('ComplianceNotification');
		  $this->loadModel('Store');
		  $this->set('notification', 'active');
				if(!$this->Session->read('stores_id')){ 
					$this->Session->setFlash(__('Please select  store.')); 
					return $this->redirect(array('action' => 'index'));

			}
	$storename = $this->Store->find('first',array('conditions' => array('Store.id' => $this->Session->read('stores_id'))));
	

		if($this->request->is('post')){
			
			$originalDate = $this->request->data['ComplianceNotification']['expiry_date'];
			$newDate = date("Y-m-d", strtotime($originalDate));
			$this->request->data['ComplianceNotification']['expiry_date'] = $newDate;
			$this->request->data['ComplianceNotification']['status'] = 'active';
			$this->request->data['ComplianceNotification']['store_name'] = $storename['Store']['name'];
			$this->request->data['ComplianceNotification']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['ComplianceNotification']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['ComplianceNotification']['created_at'] =date('Y-m-d');

						if($this->ComplianceNotification->save($this->data)){
							 $this->Session->setFlash(__('Submited new data successfully.'));
							 return $this->redirect(array('action' => 'notification'));
						}
			}

			
			}
			
			
			
		public function admin_edit_notification($id){

		  $this->loadModel('ComplianceNotification');
		$this->set('notification', 'active');
					if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select store.')); 
				return $this->redirect(array('action' => 'index'));

			}
	 
		$storename = $this->ComplianceNotification->find('first',array('conditions' => array('id'=>$id,'store_id'=>$this->Session->read('stores_id'))));
	
		$dat = array('id'=>$storename['ComplianceNotification']['id'],'license_name'=>$storename['ComplianceNotification']['license_name'],'expiry_date'=>$storename['ComplianceNotification']['expiry_date'],'notification_days'=>$storename['ComplianceNotification']['notification_days'],'status'=>'active','repeat_year'=>$storename['ComplianceNotification']['repeat_year'],'subject'=>$storename['ComplianceNotification']['subject'],'sender_email'=>$storename['ComplianceNotification']['sender_email'],'store_emails'=>$storename['ComplianceNotification']['store_emails'],'store_id'=>$storename['ComplianceNotification']['store_id'],'store_name'=>$storename['ComplianceNotification']['store_name'],'message'=>$storename['ComplianceNotification']['message']);
		
			$this->set('data', $dat);
			
		
	
			if($this->request->is('post')){
				
			$originalDate = $this->request->data['ComplianceNotification']['expiry_date'];
			$newDate = date("Y-m-d", strtotime($originalDate));
			$this->request->data['ComplianceNotification']['expiry_date'] = $newDate;
			
			
			
						if($this->ComplianceNotification->save($this->data)){
							 $this->Session->setFlash(__('Notification details Updated successfully.'));
							  $this->redirect(array('action' => 'notification'));
						}
						
		}
}
	public function admin_upload_store_lic($id = null){
		$this->loadModel('ComplianceUploadstores');
		$this->loadModel('Store');
		$this->set('upload_store_lic', 'active');
					if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select  store.')); 
				return $this->redirect(array('action' => 'index'));

			}
	
	
		if($id){
			if($this->ComplianceUploadstores->delete($id)){
				$this->Session->setFlash(__('store lic has been deleted.'));
				return $this->redirect(array('action' => 'upload_store_lic'));
			}
		}

		
			$uploads = $this->ComplianceUploadstores->find('all', array('conditions' => array('Store_id' => $this->Session->read('stores_id'),'company_id'=>$this->Session->read('Auth.User.company_id'))));
		$this->set('list', $uploads);
	}
	
	
public function admin_new_upload_store_lic($id = null){
		$this->loadModel('ComplianceUploadstores');
		$this->loadModel('Store');
		$this->set('upload_store_lic', 'active');
					if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select  store.')); 
				return $this->redirect(array('action' => 'index'));

			}
		$storename = $this->Store->find('first',array('conditions' => array('Store.id' => $this->Session->read('stores_id'))));
	
	

			
		//print_r($this->request->data);exit;
		
		if($this->request->is('post')){
			
		
				if($this->uploadFile('store_file')){
						
					$this->request->data['ComplianceUploadstores']['store_name'] = $storename['Store']['name'];
					$this->request->data['ComplianceUploadstores']['store_id'] = $this->Session->read('stores_id');
					$this->request->data['ComplianceUploadstores']['company_id'] =$this->Session->read('Auth.User.company_id');
					$this->request->data['ComplianceUploadstores']['created_at'] =date('Y-m-d');
					
				
					if($this->ComplianceUploadstores->save($this->data)){
						 $this->Session->setFlash('Store LIC File uploaded successfuly. ');
						 return $this->redirect(array('action' => 'upload_store_lic'));

					}
				}else{
						 $this->Session->setFlash('Sorry! file unable to uploaded');
				}
		}
		
			
	}
	
	
	
	function uploadFile($fieldName = 'attachment')
	{
			
		//$upload_dir = WWW_ROOT.str_replace("/", DS, 'stores');
		$uploadPath = WWW_ROOT.'files';
	
		$imageTypes = array("pdf", "doc", "docx", "xlsx","csv");
		if (isset($this->data['ComplianceUploadstores'][$fieldName]['type']) ) {
			$fike = $this->data['ComplianceUploadstores'][$fieldName];
			$_file = date('His') .'_'. $fike['name'];
			$full_image_path = $uploadPath . '/' .$_file;
			
			if (move_uploaded_file($fike['tmp_name'], $full_image_path)) {
				$this->request->data['ComplianceUploadstores'][$fieldName] = $_file;
				$this->Session->setFlash('File saved successfully');
				return true;
			} else {
				$this->Session->setFlash('Invalid type. Please try again with fike type '. implode(', ', $imageTypes));
				return false;
			}
		}
		return false;
	}
	
	
		public function admin_edit_upload_store_lic($id = null){
			
		 $this->loadModel('ComplianceUploadstores');
		  $this->loadModel('Store');
		$this->set('upload_store_lic', 'active');
			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select  store.')); 
				return $this->redirect(array('action' => 'index'));

			}
		if($this->request->is('post')){
			
			if($this->data['ComplianceUploadstores']['store_file']['name']!=""){

			$this->uploadFile('store_file');
		 }else{
			 $this->data['ComplianceUploadstores']['store_file'] = $this->data['ComplianceUploadstores']['oldfile'];
		 }
		if($this->ComplianceUploadstores->save($this->data)){
		 $this->Session->setFlash('Store LIC File uploaded successfuly. ');
			return $this->redirect(array('action' => 'upload_store_lic'));
		}
		  
		}
		
			$uploads = $this->ComplianceUploadstores->find('first', array('conditions' => array('Store_id' => $this->Session->read('stores_id'),'id'=>$id)));
			
			
		$this->set('list', $uploads);
	}
	
	
	
	public function admin_delete_notification($id){
			$this->loadmodel('ComplianceUploadstores');
	
		if($this->ComplianceUploadstores->delete($id)){
			
			$this->Session->setFlash(__('Notification has been deleted.'));
			return $this->redirect(array('action' => 'notification'));
			
		}
	}
	
	
	public function admin_sendstoreemail(){
		  $this->loadModel('ComplianceNotification');
		  	$notification = $this->ComplianceNotification->find('all');
			foreach($notification as $notification){
				
				$Date = $notification['ComplianceNotification']['expiry_date'];
				$days = $notification['ComplianceNotification']['notification_days'];
				$notification['ComplianceNotification']['store_emails'];
				$notification['ComplianceNotification']['message'];
				$notification['ComplianceNotification']['store_name'];
				
				$expiry =  date('Y-m-d', strtotime($Date. ' - '.$days.' days'));
				$today = date('Y-m-d');
				if($today == $expiry){
			
				
					$from = $notification['ComplianceNotification']['sender_email'];
					$to = $notification['ComplianceNotification']['store_emails'];
					$subject = $notification['ComplianceNotification']['subject'];
					$message = "<html><body><table width='100%'; rules='all' style='border:1px solid #3A5896;' cellpadding='10'><tr><td style='background:black;text-align:center'><img src='http://aplus1.net/backoffice/img/logo-main.png' alt='PHP Gang' /></td></tr><tr><td colspan=2>".$notification['ComplianceNotification']['subject'].",<br /><br />Store Name: ".$notification['ComplianceNotification']['store_name']."<br/>License Name: ".$notification['ComplianceNotification']['subject']."</td></tr><tr><td colspan=2 font='colr:#999999;'><I>aplus1.net<br>".$notification['ComplianceNotification']['message']."</I></td></tr></table></body></html>";
				/*	$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					// More headers
					$headers .= 'From: '.$from . "\r\n";
					//$headers .= 'Cc: myboss@example.com' . "\r\n";
					*/
					$Email = new CakeEmail();
					$Email->from(array($from  => 'Aplus1.net'));
					$Email->to($to);
					$Email->subject($subject);
					if($Email->send($message)){
					$this->Session->setFlash('Notications sent .');
						
					}


				//	if(mail($to,$subject,$message,$headers)){		
				//	}
						if($notification['ComplianceNotification']['repeat_year']=='Y'){
										
						$newdate = strtotime ( '+1 year' , strtotime ( $Date ) ) ;
						$expiry = date ( 'Y-m-d' , $newdate );
						$expiry = "'".$expiry."'";

						$this->ComplianceNotification->updateAll(array('expiry_date'=>$expiry) , array('id' => $notification['ComplianceNotification']['id']));
				}
				
				}
				
			}
	}
				
	public function admin_report(){
		  $this->loadModel('ComplianceNotification');
		  $this->loadModel('store');
			$this->set('reports', 'active');
				$storeslist = $this->store->find('list',array('fields'=>array('id','name')));
		
			$this->set('storeslist', $storeslist);
			if($this->request->is('post')){
				$fromDate = $this->request->data['ComplianceNotification']['expiry_from'];
				$toDate = $this->request->data['ComplianceNotification']['expiry_to'];
				$license_name = $this->request->data['ComplianceNotification']['license_name'];
				$store_id = $this->request->data['ComplianceNotification']['store_id'];
				$from  = date("Y-m-d", strtotime($fromDate));
				$to  = date("Y-m-d", strtotime($toDate));
								
								if($this->Session->read('Auth.User.StoreInfo')){
									
											if($license_name=='all'){
														$conditions = array('AND' =>
														array(
														   array('AND' => array(
																		  array('expiry_date >= ' => $from),
																		  array('expiry_date <= ' => $to)
																	))
														 ),'store_id'=>$this->Session->read('stores_id'));
											}else{
													$conditions = array('AND' =>
													array(
													   array('AND' => array(
																	  array('expiry_date >= ' => $from),
																	  array('expiry_date <= ' => $to)
																)),
													  'license_name'=>$license_name
													 ),'store_id'=>$this->Session->read('stores_id'));
											}
								}else{
											if($license_name=='all' && $store_id == ''){
														$conditions = array('AND' =>
														array(
														   array('AND' => array(
																		  array('expiry_date >= ' => $from),
																		  array('expiry_date <= ' => $to)
																	))
														 ),'company_id'=>$this->Session->read('Auth.User.company_id'));														
											}else if($license_name=='all' && $store_id != ''){
												
												$conditions = array('AND' =>
														array(
														   array('AND' => array(
																		  array('expiry_date >= ' => $from),
																		  array('expiry_date <= ' => $to)
																	)),'store_id'=>$store_id
														 ),'company_id'=>$this->Session->read('Auth.User.company_id'));
												
											}else if($license_name!='all' && $store_id == ''){
												$conditions = array('AND' =>
														array(
														   array('AND' => array(
																		  array('expiry_date >= ' => $from),
																		  array('expiry_date <= ' => $to)
																	)),'license_name'=>$license_name
														 ),'company_id'=>$this->Session->read('Auth.User.company_id'));
												
											}else{

												$conditions = array('AND' =>
												array(
												   array('AND' => array(
																  array('expiry_date >= ' => $from),
																  array('expiry_date <= ' => $to)
															)),
												  'license_name'=>$license_name,'store_id'=>$store_id
												 ),'company_id'=>$this->Session->read('Auth.User.company_id'));
											
												}
								}
						$report=$this->ComplianceNotification->find('all',array('conditions'=>$conditions));
						 
							$this->set('report', $report);
			}
			

	}
	public function admin_licenselist($id = null){ 
		 $this->loadModel('Licenselist');
		$this->set('licensetab', 'active');
			$list = $this->Licenselist->find('all', array('conditions' => array('store_id' => $this->Session->read('stores_id'))));
			if($id){
				if($this->Licenselist->delete($id)){
						
						$this->Session->setFlash(__('License has been deleted.'));
						return $this->redirect(array('action' => 'licenselist'));
						
					}
					
				}
			$this->set('list', $list);
		
	}
}
