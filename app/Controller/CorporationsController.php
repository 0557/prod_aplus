<?php

App::uses('AppController', 'Controller');

/**
 * Corporations Controller
 *
 * @property Corporation $Corporation
 * @property PaginatorComponent $Paginator
 */
class CorporationsController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('corpss', 'active');
        $this->set('title_for_layout', 'Corporation');

        $this->paginate = array("limit" => 5, "order" => "Corporation.id DESC");
        $this->set('corporations', $this->paginate());
    }

    public function admin_view($id = null) {

        $this->set('corpss', 'active');
        $this->set('title_for_layout', 'Corporation View');

        $id = base64_decode($id);
        if (!$this->Corporation->exists($id)) {
            throw new NotFoundException(__('Invalid corporation'));
        }
        $options = array('conditions' => array('Corporation.' . $this->Corporation->primaryKey => $id));

        $this->set('corporation', $this->Corporation->find('first', $options));
    }

    public function admin_add() {
        $this->loadModel('Country');
		 $this->loadModel('Company');
        $this->set('corpss', 'active');

        $this->set('title_for_layout', 'Add corporation');

        if ($this->request->is('post')) {
            $this->Corporation->create();
            if ($this->Corporation->save($this->request->data)) {
                $this->Session->setFlash(__('The corporation has been saved.'));
                $this->redirect(array('controller' => 'corporations', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The corporation could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Country->find('list', array("conditions" => array("Country.status" => "1"), 'fields' => array('Country.id', 'Country.name')));
		
		$companies = $this->Company->find('list');
        $this->set(compact('countries', 'companies'));
    }

    public function admin_edit($id = null) {
        $this->set('corpss', 'active');

        $this->set('title_for_layout', 'Edit corporation');

        $id = base64_decode($id);
        if (!$this->Corporation->exists($id)) {
            throw new NotFoundException(__('Invalid corporation'));
        }
        if ($this->request->is(array('post', 'put'))) {

            if ($this->Corporation->save($this->request->data)) {

                $this->Session->setFlash(__('The corporation has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The corporation could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Corporation.' . $this->Corporation->primaryKey => $id));
            $this->request->data = $this->Corporation->find('first', $options);
            $countries = $this->Corporation->Country->find('list', array("conditions" => array("Country.status" => "1"), 'fields' => array('Country.id', 'Country.name')));
            $states = $this->Corporation->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
            if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
                $cities = $this->Corporation->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
            }
            if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
                $ZipCode = $this->Corporation->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code')));
            }
            $this->set(compact('countries', 'states', 'cities', 'ZipCode'));
        }
    }

    public function admin_delete($id = null) {
        $id = base64_decode($id);

        $this->Corporation->id = $id;
        if (!$this->Corporation->exists()) {
            throw new NotFoundException(__('Invalid corporation'));
        }
        if ($this->Corporation->delete()) {
            $this->Session->setFlash(__('The corporation has been deleted.'));
        } else {
            $this->Session->setFlash(__('The corporation could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_filter_store($corporation_id = null) {
        if (empty($corporation_id)) {
            unset($_SESSION['store_id']);
            unset($_SESSION['corporation_id']);
        } else {
            $_SESSION['corporation_id'] = $corporation_id;
            unset($_SESSION['store_id']);
            $this->loadModel('Store');
            $store_name = $this->Store->find('list', array('conditions' => array('Store.corporation_id' => $corporation_id)));
            $this->set('store_name', $store_name);
        }
    }

    public function admin_SetstoreId($store_id = null) {	
		
        $this->autoRender = false;
        if (empty($store_id)) {
            unset($_SESSION['store_id']);
			$this->Session->delete('stores_id');
        } else {
            $_SESSION['store_id'] = $store_id;
			$this->Session->write('stores_id', $store_id);
			$this->loadModel('Store');
	        $store_data = $this->Store->find('first',array('conditions' => array('Store.id' =>$store_id)));			
			$pos_type= $store_data['Store']['pos_type'];
		    $this->Session->write('pos_type', $pos_type);
        }
    }
	
	public function admin_testdata2() {
		 $this->set('title_for_layout', 'Test Data2');
		
	}

}
