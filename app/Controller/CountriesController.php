<?php
App::uses('AppController', 'Controller');
/**
 * Countries Controller
 * @developer dotsquares
 *
 * @property Country $Country
 */
class CountriesController extends AppController {
var $uses = array('Country', 'SitePermission');
/**
 * index method
 *
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function index() {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_read'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		$this->Country->recursive = 0;
		$limit=(isset($this->params['named']['showperpage']))?$this->params['named']['showperpage']:"ALL";
		
	    $conditions=array();
		if($limit=='ALL') {
		 $paging_limit='1000000';
		} else {
		 $paging_limit=$limit;
		}

		$this->paginate=array("conditions"=>$conditions,"limit"=>$paging_limit,"order"=>"Country.".$this->Country->primaryKey);
        $this->set(compact('limit'));
		$this->set('countries', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function view($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_read'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function add() {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_add'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		}
$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
$this->set(compact('jsIncludes','cssIncludes'));
}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function edit($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_edit'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
			$this->request->data = $this->Country->find('first', $options);
		}
	$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
	$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
	$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function delete($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_delete'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash(__('Country deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Country was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	/**
     * admin_delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function deleteall()
	{
		$this->layout = 'ajax';
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_delete'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		$ids = explode(",", $this->params['data']['ids']);
		$flag = 0;
		foreach ($ids as $id)
		{
	    	$this->Country->id = $id;
	    	$this->Country->delete();
	    	$flag++;
		}
		if ($flag > 0)
		{
	    	$this->Session->setFlash(__('Country deleted successfully!'));
	    	$this->redirect(array('action' => 'index'));
		}
		else
		{
		    $this->Session->setFlash(__('Country was not deleted'));
	    	$this->redirect(array('action' => 'index'));
		}
    }

/**
 * admin_index method
 *
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function admin_index() {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_read'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		
		$this->Country->recursive = 0;
		$limit=(isset($this->params['named']['showperpage']))?$this->params['named']['showperpage']:"ALL";
		
	    $conditions=array();
	   if (isset($this->params['named']['keyword']) && $this->params['named']['keyword'] != '') {
			$conditions = array(
				'OR' => array(
					'Country.name LIKE ' => '%' . $this->params['named']['keyword']. '%',
				)
			);
        }
        if (!empty($this->request->data)) {
            if (isset($this->request->data['showperpage']) && $this->request->data['showperpage'] != '') {
                $limit = $this->request->data['showperpage'];
                $this->params['named'] = array("showperpage" => $limit);
            }
            if (isset($this->request->data['keyword']) && $this->request->data['keyword'] != '') {
                $this->params['named'] = array("keyword" => $this->request->data['keyword']);
                $conditions = array(
                    'OR' => array(
                        'Country.name LIKE ' => '%' . $this->request->data['keyword'] . '%',
                    )
                );
            }
        }
		
		if($limit=='ALL') {
		 $paging_limit='1000000';
		} else {
		 $paging_limit=$limit;
		}

		$this->paginate=array("conditions"=>$conditions,"limit"=>$paging_limit,"order"=>"Country.".$this->Country->primaryKey);
        $this->set(compact('limit'));
		$this->set('countries', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function admin_view($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_read'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function admin_add() {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_add'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		}
$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
$this->set(compact('jsIncludes','cssIncludes'));
}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function admin_edit($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_edit'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
			$this->request->data = $this->Country->find('first', $options);
		}
	$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
	$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
	$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 * @developer Chirag & Arnav (114)
 */
	public function admin_delete($id = null) {
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_delete'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash(__('Country deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Country was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	/**
     * admin_delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function admin_deleteall()
	{
		$this->layout = 'ajax';
		if (!$this->SitePermission->CheckPermission($this->Auth->user("id"), 'countries', 'is_delete'))
		{
	   		$this->Session->setFlash(__('You are not authorised to access that location'));
	    	$this->redirect(array('action' => 'dashboard'));
		}
		$ids = explode(",", $this->params['data']['ids']);
		$flag = 0;
		foreach ($ids as $id)
		{
	    	$this->Country->id = $id;
	    	$this->Country->delete();
	    	$flag++;
		}
		if ($flag > 0)
		{
	    	$this->Session->setFlash(__('Country deleted successfully!'));
	    	$this->redirect(array('action' => 'index'));
		}
		else
		{
		    $this->Session->setFlash(__('Country was not deleted'));
	    	$this->redirect(array('action' => 'index'));
		}
    }
}
