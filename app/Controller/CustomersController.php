<?php

App::uses('AppController', 'Controller');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_vendor() {

        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
        $this->set('vendorss', 'active');
//        if(isset( $_SESSION['store_id']) && !empty($_SESSION['store_id'])) {
//            $this->paginate = array('conditions' => array('Customer.customer_type' => 'Vendor','Customer.corporation_id'=>$_SESSION['corporation_id'],'or'=>array('Customer.store_id'=>$_SESSION['store_id'])), "limit" => 5, "order" => "Customer.id DESC");
//        }elseif(isset( $_SESSION['corporation_id']) && !empty($_SESSION['corporation_id'])){
//            $this->paginate = array('conditions' => array('Customer.customer_type' => 'Vendor','Customer.corporation_id'=>$_SESSION['corporation_id']), "limit" => 5, "order" => "Customer.id DESC");
//        }
//        else{
        $this->paginate = array('conditions' => array('Customer.customer_type' => 'Vendor'), "limit" => 5, "order" => "Customer.id DESC");
        //  }

        $this->set('vendor', $this->paginate());
    }

    public function admin_addVendor() {

        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
        $this->set('vendorss', 'active');
        if ($this->request->is('post')) {
            $this->Customer->create();
            $this->request->data['Customer']['customer_type'] = 'Vendor';

            if ($this->Customer->save($this->request->data)) {

                $this->Session->setFlash(__('The Vendor has been saved.'));
                return $this->redirect(array('action' => 'vendor'));
            } else {
                $this->Session->setFlash(__('The Vendor could not be saved. Please, try again.'));
            }
        }

        $countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));

        $corporations = $this->Customer->Corporation->find('list');
        $stores = array();
        $this->set(compact('stores', 'countries', 'corporations'));
    }

    public function admin_VendorEdit($id = null) {

        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
        $this->set('vendorss', 'active');
        $id = base64_decode($id);

        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        if ($this->request->is(array('post', 'put'))) {

            if ($this->request->data['Customer']['retail_type'] == 'wholesale') {
                $this->request->data['Customer']['corporation_id'] = '';
                $this->request->data['Customer']['store_id'] = '';
            }

            // pr($this->request->data);
            // die('i mm here');
            if ($this->Customer->save($this->request->data)) {
                $this->Session->setFlash(__('The Vendor has been saved.'));
                return $this->redirect(array('action' => 'vendor'));
            } else {
                $this->Session->error(__('The Vendor could not be saved. Please, try again.'));
            }
        }
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $this->request->data = $this->Customer->find('first', $options);

        $countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));
        $states = $this->Customer->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
        if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
            $cities = $this->Customer->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
        }
        if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
            $Zipcode = $this->Customer->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code')));
        }
        $corporations = $this->Customer->Corporation->find('list');
        $stores = $this->Customer->Store->find('list', array("Store.corporation_id" => $this->request->data['Customer']['corporation_id']));

        $this->set(compact('stores', 'countries', 'states', 'cities', 'corporations', 'Zipcode'));
    }
    
    public function admin_viewVendor($id = null) {
        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
        $this->set('vendorss', 'active');
        $id = base64_decode($id);

        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $this->set('customer', $this->Customer->find('first', $options));
    }

    public function admin_index() {

        $this->set('customerss', 'active');
//        if(isset( $_SESSION['store_id']) && !empty($_SESSION['store_id'])) {
//            $this->paginate = array('conditions' => array('Customer.customer_type' => 'Customer','Customer.corporation_id'=>$_SESSION['corporation_id'],'or'=>array('Customer.store_id'=>$_SESSION['store_id'])), "limit" => 5, "order" => "Customer.id DESC");
//        } elseif(isset( $_SESSION['corporation_id']) && !empty($_SESSION['corporation_id'])){
//        $this->paginate = array('conditions' => array('Customer.customer_type' => 'Customer','Customer.corporation_id'=>$_SESSION['corporation_id']), "limit" => 5, "order" => "Customer.id DESC");
//      }else{
        $this->paginate = array('conditions' => array('Customer.customer_type' => 'Customer'), "limit" => 5, "order" => "Customer.id DESC");
        //  }

        $this->set('customer', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->set('customerss', 'active');
        $id = base64_decode($id);

        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $this->set('customer', $this->Customer->find('first', $options));
    }


    public function admin_add() {

        $this->set('customerss', 'active');
        if ($this->request->is('post')) {
            $this->Customer->create();
            $this->request->data['Customer']['customer_type'] = 'Customer';

            if ($this->Customer->save($this->request->data)) {

                $this->Flash->success(__('The customer has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The customer could not be saved. Please, try again.'));
            }
        }
//       if(isset( $_SESSION['corporation_id']) && !empty($_SESSION['corporation_id'])){
//        $stores = $this->Customer->Store->find('list',array('conditions'=>array('Store.corporation_id'=>$_SESSION['corporation_id'])));
//       }else{
        $corporations = $this->Customer->Corporation->find('list');
        $stores = array();
        //   }
        $countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));
        $this->set(compact('stores', 'countries', 'corporations'));
    }

    public function admin_stores() {
        $this->layout = 'ajax';
        if (isset($this->request->data['corporation_id']) && $this->request->data['corporation_id'] != '') {
            $stores = $this->Customer->Store->find('list', array('conditions' => array('Store.corporation_id' => $this->request->data['corporation_id'])));
        } else {
            $stores = array();
        }
        $this->set(compact('stores'));
    }

    public function admin_edit($id = null) {

        $this->set('customerss', 'active');
        $id = base64_decode($id);
        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        if ($this->request->is(array('post', 'put'))) {

            if ($this->request->data['Customer']['retail_type'] == 'wholesale') {
                $this->request->data['Customer']['corporation_id'] = '';
                $this->request->data['Customer']['store_id'] = '';
            }

            if ($this->Customer->save($this->request->data)) {
                $this->Session->setFlash(__('The customer has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customer could not be saved. Please, try again.'));
            }
        }
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $this->request->data = $this->Customer->find('first', $options);

        $corporations = $this->Customer->Corporation->find('list');
        $stores = $this->Customer->Store->find('list', array("Store.corporation_id" => $this->request->data['Customer']['corporation_id']));

        $countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));
        $states = $this->Customer->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
        if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
            $cities = $this->Customer->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
        }
        if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
            $ZipCode = $this->Customer->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code')));
        }
        $this->set(compact('stores', 'countries', 'states', 'cities', 'corporations', 'ZipCode'));
    }

    public function admin_delete($id = null) {

        $id = base64_decode($id);

        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $data_type = $this->Customer->find('first', array('conditions' => array('Customer.id' => $id), 'fields' => array('customer_type')));


        if ($this->Customer->delete()) {
            $this->Session->setFlash(__('Successfully deleted.'));
        } else {
            $this->Session->setFlash(__('Could not be deleted. Please, try again.'));
        }
        if ($data_type['Customer']['customer_type'] == 'Customer') {
            return $this->redirect(array('action' => 'index'));
        }
        if ($data_type['Customer']['customer_type'] == 'Vendor') {
            return $this->redirect(array('action' => 'vendor'));
        }
    }

}
