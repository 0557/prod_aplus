<?php
//ob_start();
App::uses('AppController', 'Controller');

class DailyReportingsController extends AppController {

    public $components = array('Paginator','ExportXls');

    public function admin_index() {
        $this->Setredirect();
        $this->loadmodel('DailyReporting');
        
        $full_date = '';
		$form_date='';
		$to_date='';
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['DailyReporting']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['DailyReporting']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
				$this->Session->write('PostSearch', 'PostSearch');
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);
				$this->Session->write('to_date',$tdate);			   	
						
				} else {
			    //echo 'notok';die;
				$this->Session->delete('PostSearch');
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}		

            if ($this->Session->check('PostSearch')) {
				//echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');
            }
            $conditions = array('DailyReporting.store_id' => $this->Session->read('stores_id')); 
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'DailyReporting.reporting_date >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'DailyReporting.reporting_date <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
			if($this->request->data['DailyReporting']['full_date']!= "" ) {	
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'DailyReporting.reporting_date' => 'desc'
                )
            );
			$this->set('full_date',$full_date);	
            $this->set('DailyReportingReport', $this->Paginator->paginate()); 
			}else{
			$this->set('full_date',$full_date);	
            $this->set('DailyReportingReport', '');
			}
			           
        } else {
            $this->set('full_date',$full_date);	
            $this->set('DailyReportingReport', '');
        }
    }
	
	public function admin_export()
	{
 		        $conditions = array('DailyReporting.store_id' => $this->Session->read('stores_id')); 
	
	            $date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['DailyReporting']['full_date']!= "" ) {			
				//echo 'ok';die;
				//echo '<pre>';print_r($this->request->data);die;
				$full_date=$this->request->data['DailyReporting']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
			
				
				if(isset($fdate) && $fdate!=''){				
						
				$conditions['DailyReporting.reporting_date >='] = $fdate;			
				}
				if(isset($tdate) && $tdate!=''){						
				
				$conditions['DailyReporting.reporting_date <=']  = $tdate;
				}
				
				}	
				
			    //echo '<pre>';print_r($conditions);die;	
			
                $DailyReportings=$this->DailyReporting->find('all',array('conditions'=>$conditions,'fields'=>array('DailyReporting.reporting_date','DailyReporting.coupon_count','DailyReporting.coupon_amount'),'order'=>'DailyReporting.reporting_date desc'));
			    //echo '<pre>';print_r($DailyReportings);die;	
				
	$arr=array();
	$total_coupon=0;
	$total_coupon_amount=0;			
	foreach($DailyReportings as $DailyReporting)
	{
	$total_coupon += $DailyReporting['DailyReporting']['coupon_count'];
	$total_coupon_amount += $DailyReporting['DailyReporting']['coupon_amount'];	
	$res1['reporting_date']=date('Y-m-d',strtotime($DailyReporting['DailyReporting']['reporting_date']));	
	$res1['coupon_count']=$DailyReporting['DailyReporting']['coupon_count'];
	$res1['coupon_amount']=$DailyReporting['DailyReporting']['coupon_amount'];
	array_push($arr,$res1);		
	}
	array_push($arr,array('Total Sales',$total_coupon,$total_coupon_amount));
	//echo '<pre>';print_r($res1);die;
	$fileName = "daily_reports".time().".xls";			
	$headerRow = array('Reporting Date','Coupon Count','Coupon Amount');
	$this->ExportXls->export($fileName, $headerRow, $arr);
	}
	
	
	
	
	public function admin_reset() {
	
		$this->Session->delete('PostSearch');
		$this->Session->delete('full_date');
		$this->Session->delete('from_date');
		$this->Session->delete('to_date');
		$redirect_url = array('controller' => 'daily_reportings', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}

}
