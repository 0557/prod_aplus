<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 */
class EmailTemplatesController extends AppController {
 
 
	public $name = 'EmailTemplates';
	 public $uses=array("EmailTemplate","SitePermission");
	 public $components = array(
			'Default','Email'
	 );
	
 
 
 public function beforeFilter() {
		parent::beforeFilter();
 }
   
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	  
	  if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_read')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}
		
	    $limit = (isset($this->params['named']['showperpage'])) ? $this->params['named']['showperpage'] : Configure::read('site.admin_paging_limit');
        $conditions = array();
        if (isset($this->params['named']['keyword']) && $this->params['named']['keyword'] != '') {
            $conditions = array('OR' => array(
                    'EmailTemplate.title LIKE ' => '%' . $this->params['named']['keyword'] . '%',
                    'EmailTemplate.subject LIKE ' => '%' . $this->params['named']['keyword'] . '%'
                )
            );
        }

        if (!empty($this->request->data)) {
            if (isset($this->request->data['showperpage']) && $this->request->data['showperpage'] != '') {
                $limit = $this->request->data['showperpage'];
                $this->params['named'] = array("showperpage" => $limit);
            }
            if (isset($this->request->data['keyword']) && $this->request->data['keyword'] != '') {
                $this->params['named'] = array("keyword" => $this->request->data['keyword']);
                $conditions = array('OR' => array(
                        'EmailTemplate.title LIKE ' => '%' . $this->request->data['keyword'] . '%',
                        'EmailTemplate.subject LIKE ' => '%' . $this->request->data['keyword'] . '%'
                    )
                );
            }
        }
		######### Date Added: 2-/19/2015 ##########
		$conditions['EmailTemplate.status'] = 1;	

        $this->paginate = array("conditions" => $conditions, "limit" => $limit, "order" => "EmailTemplate.created DESC");

        $this->set(compact('limit'));
        $this->set('emailTemplates', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_read')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}
		
		$this->EmailTemplate->id = $id;
		if (!$this->EmailTemplate->exists()) {
			throw new NotFoundException(__('Invalid email template'));
		}
		$this->set('emailTemplate', $this->EmailTemplate->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_add')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}
		
		if ($this->request->is('post')) {
			$this->EmailTemplate->create();
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The email template has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		}
		
		
		$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js','admin/bootstrap-datepicker.js','admin/clockface.js','admin/bootstrap-timepicker.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css','admin/datepicker.css','admin/clockface.css','admin/timepicker.css');


		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		
		if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_edit')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}
		$this->EmailTemplate->id = $id;
		if (!$this->EmailTemplate->exists()) {
			throw new NotFoundException(__('Invalid email template'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The email template has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->EmailTemplate->read(null, $id);
		}
		
		$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js','admin/bootstrap-datepicker.js','admin/clockface.js','admin/bootstrap-timepicker.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css','admin/datepicker.css','admin/clockface.css','admin/timepicker.css');


		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
       
	  
		if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_delete')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}
        $this->EmailTemplate->id = $id;
        if (!$this->EmailTemplate->exists()) {
            throw new NotFoundException(__('Invalid EmailTemplate'));
        }
        $options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
        $unimage = $this->EmailTemplate->find('first', $options);
        $this->request->onlyAllow('post', 'delete');
        if ($this->EmailTemplate->delete()) {
            $this->Session->setFlash(__('EmailTemplate deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('EmailTemplate was not deleted'));
        $this->redirect(array('action' => 'index'));
    }
	
	public function admin_deleteall() {
		
		$this->layout	=	'ajax';
		if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'emailTemplates','is_delete')) {
			$this->Session->setFlash(__('You are not authorised to access that location'));
			$this->redirect(array('controller'=>'users','action' => 'dashboard'));
		}

		$userids	=	explode(",", $this->params['data']['ids']);
		$flag	=	0;
		foreach($userids as $ids){
			$this->EmailTemplate->id = $ids;
			$this->EmailTemplate->delete();
			$flag++;
		}
		if($flag > 0){
			$this->Session->setFlash(__('emailTemplates deleted successfully!'));
			$this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash(__('emailTemplates was not deleted'));
			$this->redirect(array('action' => 'index'));
		}


	}
}
