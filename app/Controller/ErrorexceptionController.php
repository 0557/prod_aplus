<?php
App::uses('AppController', 'Controller');
/**
 * Companies Controller
 *
 * @property Company $Company
 * @property PaginatorComponent $Paginator
 */
class ErrorexceptionController extends AppController {
   public $uses=array("Company","SitePermission");
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function index() {
$this->layout='blank';
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

	}
}
