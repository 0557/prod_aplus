<?php

App::uses('AppController', 'Controller');

/**
 * Posrequesters Controller
 *
 * @property Posrequester $Posrequester
 */
class FeedDataController extends AppController {
	

	public function pinger($stationId)
	{
		
		$this->debugEmail(null, 'Ping');
	}
	
	public function debugEmail($data, $cmd)
	{
		$to = 'satish.ashilwar@gmail.com';
		$subject = 'REMOTE POS DATA -- ' . date('YYYY-mm-dd-hh-mm-ss', strtotime('NOW')) . '-PUSHED IP-' . $_SERVER['REMOTE_ADDR']. ' -- '.$cmd;
		$message = print_r($data, true);
		$headers = 'From: info@axiomz.com' . "\r\n" . 'Reply-To: info@axiomz.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
		mail($to, $subject, $message, $headers);
		//rushikesh.rko@gmail.com
	}
	
	public function debugEmail2($subject, $data){
		$to = 'satish.ashilwar@gmail.com,sarangpthk@gmail.com';
		//$subject = 'REMOTE POS DATA -- ' . date('YYYY-mm-dd-hh-mm-ss', strtotime('NOW')) . '-PUSHED IP-' . $_SERVER['REMOTE_ADDR']. ' -- '.$cmd;
		//$message = print_r($data, true);
		$headers = 'From: info@axiomz.com' . "\r\n" . 'Reply-To: info@axiomz.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
		//mail($to, $subject, $message, $headers);
		//rushikesh.rko@gmail.com
	}
	

    /**
     * index method
     *
     * @return void
     */
    public function index($store_id) {
		$this->loadModel('Store');
		$stores = $this->Store->find('list');
        $this->set(compact('stores'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        $this->set('posrequester', $this->Posrequester->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
			$this->request->data['Posrequester']['synupid'] = strtolower(str_replace(" ","_", $this->request->data['Posrequester']['cmd'])).'_'.$this->getSyncId();
            $this->Posrequester->create();
            if ($this->Posrequester->save($this->request->data)) {
                $this->Session->setFlash(__('The posrequester has been saved'));
                $this->redirect(array('action' => 'all'));
            } else {
                $this->Session->setFlash(__('The posrequester could not be saved. Please, try again.'));
            }
        }
		
		$this->loadModel('Store');
		$stores = $this->Store->find('list');
        $this->set(compact('stores'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Posrequester->save($this->request->data)) {
                $this->Session->setFlash(__('The posrequester has been saved'));
                $this->redirect(array('action' => 'all'));
            } else {
                $this->Session->setFlash(__('The posrequester could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Posrequester->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        if ($this->Posrequester->delete()) {
            $this->Session->setFlash(__('Posrequester deleted'));
            $this->redirect(array('action' => 'all'));
        }
        $this->Session->setFlash(__('Posrequester was not deleted'));
        $this->redirect(array('action' => 'all'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function all() {
	
        $this->Posrequester->recursive = 0;
        $this->set('posrequesters', $this->paginate());
    }
	
	
	public function push()
	{
		
		//$this->process(61, $this->feedDirect(), '55A32FA750FBA77C48BBE371F4B5D2DB');
	}	
	
	public function process($storeId="", $synupid="", $textXML="") {
		//Get Response from POS
		
		if(!empty($this->request->data)) {
			$storeId = $this->request->data['store_id'];
			$xml = $this->request->data['xml'];
			
			$synupid = 'get_all_data_mop_2016-09-17-15-17-37';
			
			
		} else {
			
			$xml = file_get_contents('php://input');
			
			if ($textXML != "") {
				$xml = $textXML;
			}
		}
		
//$xml = <<<XML
//XML;
//$synupid = "791bccc12b0e15177d6f75e80b9752ad";
//$storeId = 60;

		//$this->debugEmail2('POS -'. $synupid, $xml);
		
		$this->loadModel('Posrequester');
		$process = $this->Posrequester->find('first', array('conditions' => array('store_id'=> $storeId, 'id' => 4150)));
		//get_all_totals_nettot22_2016-09-19-03-57-44s_60.xml
		//$process['Posrequester']['cmd'] = 'GET ALL TOTALS deptot22';
		
		//print_r($process);
		
		
		if ($xml != "" && !empty($process)) {
			$message =  "########### START Processing POS REQUEST ############ ". "\n";;
			$message .=  $xml . "\n";
			
			//$this->debugEmail2('POS RUBY  finalUpdate-'. $storeId, $xml);
			
			$xml = str_replace(":", "", $xml);
			$xmlObj = @simplexml_load_string($xml);

			$message .= "DATA Received" . "\n";
			
			
			
			try {
				//$xmlObj 	= base64_decode((string)$xmlObj->Row->data);
					$data 	= base64_decode((string)$xmlObj->Row->data);
				 //print_r($data);
				//exit;
			
			
				//$message .=  print_r($data, true) . "\n";

				$message .= 'Data Found for ' . $process['Posrequester']['cmd'] ;
				
				$process['Posrequester']['cmd'] = trim($process['Posrequester']['cmd']);
			//print_r($process);exit;
				if (strpos($process['Posrequester']['cmd'], 'GET ALL TOTALS sumtot') !== false ||
					strpos($process['Posrequester']['cmd'], 'GET ALL TOTALS nettot') !== false 
					) {
					
					//die('sasa');
					$this->loadModel('RubyHeaderMapper');
					$headerMappers = $this->RubyHeaderMapper->find('list', array('fields' => array('data_set','headers')));
					
					$message .= 'Data Found for sumtot12';
					
					//$headerMappers = $finalUpdate = array();
					$finalUpdate = array();
				
					$arrayData =  explode("\r\n", $data); 
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//print_r();exit;
						//print_r($row);
						$firstNode = trim(str_replace('"', "", (string)$row[1]));
						
						if(isset($headerMappers[$firstNode])) {
							$_heads = explode(',', $headerMappers[$firstNode]);
							$row[1] = $firstNode;
							
							if (count($_heads) == count($row)) {
								$finalUpdate[$row[1]][] = array_combine($_heads, $row);
							} else {
								$finalUpdate[$row[1]][] = $row;
								//print_r($_heads);
								//print_r($row);
								//die('Column Mismatched');
							}
						}
					}
					
					//print_r($finalUpdate);exit;
					
					$message .=  print_r($finalUpdate, true) . "\n";
					
					
					$this->loadModel('RubyHeader');
					$this->loadModel('RubyTicknbr');
					$this->loadModel('RubyUtankt');
					
					ini_set('memory_limit', '-1');
					
					foreach($finalUpdate as $rubyHeader => $rubyRow) {
						if ($rubyHeader == 'head') {
							$_data['RubyHeader'] 			  = $rubyRow[0];
							$_data['RubyHeader']['store_id']  = $storeId;
							
							$beginning_date_time_arr = str_split($_data['RubyHeader']['beginning_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['beginning_date_time'] = date('Y-m-d', strtotime('20'.$beginning_date_time_arr[2].$beginning_date_time_arr[0].$beginning_date_time_arr[1].$beginning_date_time_arr[3].$beginning_date_time_arr[4].$beginning_date_time_arr[5]));
							
							
							$ending_date_time_arr = str_split($_data['RubyHeader']['ending_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['ending_date_time'] = date('Y-m-d', strtotime('20'.$ending_date_time_arr[2].$ending_date_time_arr[0].$ending_date_time_arr[1].$ending_date_time_arr[3].$ending_date_time_arr[4].$ending_date_time_arr[5]));
							

							$_condition = array(
												'header' => $_data['RubyHeader']['header'],
												'beginning_date_time' => $_data['RubyHeader']['beginning_date_time'],
												'ending_date_time' => $_data['RubyHeader']['ending_date_time'],
												'store_id' => $_data['RubyHeader']['store_id'],
												'sequence_number' => $_data['RubyHeader']['sequence_number'],
												'requested_period_number' => $_data['RubyHeader']['requested_period_number'],
										);
							$_rowId = $this->RubyHeader->field('RubyHeader.id', $_condition);
							
							if($_rowId > 0 ) $_data['RubyHeader']['id']  = $_rowId;
							
							$this->RubyHeader->save($_data);
						} else if ($rubyHeader == 'ticknbr') {
							$message .= $this->deletInsertDataSet($message, 'RubyTicknbr', $rubyRow);
						}   else if ($rubyHeader == 'tottax') {
							$message .= $this->deletInsertDataSet($message, 'RubyTottax', $rubyRow);
						}   else if ($rubyHeader == 'tottxb') {
							$message .= $this->deletInsertDataSet($message, 'RubyTottxb', $rubyRow);
						} else if ($rubyHeader == 'tothr') {
							$message .= $this->deletInsertDataSet($message, 'RubyTothr', $rubyRow);
						} else if ($rubyHeader == 'totreghr') {
							$message .= $this->deletInsertDataSet($message, 'RubyTotreghr', $rubyRow);
						} else if ($rubyHeader == 'totsum') {
							$message .= $this->deletInsertDataSet($message, 'RubyTotsum', $rubyRow);
						} else if ($rubyHeader == 'pmtout') {
							$message .= $this->deletInsertDataSet($message, 'RubyPmtout', $rubyRow);
						} else if ($rubyHeader == 'pmtin') {
							$message .= $this->deletInsertDataSet($message, 'RubyPmtin', $rubyRow);
						} else if ($rubyHeader == 'totmop') {
							
							//Additional logic to manage row as per MOP
							$rubyRow = array_chunk($rubyRow[0], 2);
							$this->loadModel('RubyMop');
							$mopList = $this->RubyMop->find('list', array('order'=> array('number' => 'asc'),'fields' => array('number', 'name'), 'conditions' => array('store_id' => $storeId)));
							$_rubyRow = array();
							foreach($rubyRow as $key => $mopRow) {
								if($key > 0 && isset($mopList[$key]) && trim($mopList[$key]) != '*') {
									$_rubyRow[$key]['mop_name'] 	= trim($mopList[$key]);
									$_rubyRow[$key]['number_of_sales'] = $mopRow[0];
									$_rubyRow[$key]['value_of_sales']  = $mopRow[1];
								}
							}
							print_r($_rubyRow);
							$message .= $this->deletInsertDataSet($message, 'RubyTotmop', $_rubyRow);
						} else if ($rubyHeader == 'totcw') {
							$message .= $this->deletInsertDataSet($message, 'RubyTotcw', $rubyRow);
						} else if ($rubyHeader == 'memo1') {
							$message .= $this->deletInsertDataSet($message, 'RubyMemo1', $rubyRow);
						} else if ($rubyHeader == 'memo2') {
							//print_r($rubyRow);exit;
							$message .= $this->deletInsertDataSet($message, 'RubyMemo2', $rubyRow);
						} else if ($rubyHeader == 'memo3') {
							$message .= $this->deletInsertDataSet($message, 'RubyMemo3', $rubyRow);
						}  else if ($rubyHeader == 'payout') {
							$message .= $this->deletInsertDataSet($message, 'RubyPayout', $rubyRow);
						} else if ($rubyHeader == 'payin') {
							$message .= $this->deletInsertDataSet($message, 'RubyPayin', $rubyRow);
						} else if ($rubyHeader == 'ttlizr') {
							$message .= $this->deletInsertDataSet($message, 'RubyTtlizr', $rubyRow);
						} else if ($rubyHeader == 'poptot') {
							$message .= $this->deletInsertDataSet($message, 'RubyPoptot', $rubyRow);
						} else if ($rubyHeader == 'popdisp') {
							$message .= $this->deletInsertDataSet($message, 'RubyPopdisp', $rubyRow);
						} else if ($rubyHeader == 'sumftxex') {
							$message .= $this->deletInsertDataSet($message, 'RubySumftxex', $rubyRow);
						} else if ($rubyHeader == 'totnet') {
							$message .= $this->deletInsertDataSet($message, 'RubyTotnet', $rubyRow);
						} else if ($rubyHeader == 'totcard') {
							$message .= $this->deletInsertDataSet($message, 'RubyTotcard', $rubyRow);
						}
						
					}
				}  else if (strpos($process['Posrequester']['cmd'], 'GET ALL TOTALS ftotal') !== false) {
					$message .= 'Data Found for ftotal12';
					$headerMappers = $finalUpdate = array();

					$headerMappers['head']   = array('0', 'header', 'beginning_date_time', 'ending_date_time', 'store_number', 'sequence_number', 'operator_id', 'keyboard_number', 'requested_period_number', 'requested_set_number', 'next_lower_period_b', 'next_lower_period_e');

					$headerMappers['uprodt'] = array('0','name', 'fuel_product_number', 'fmop1', 'fuel_volume', 'fuel_value', 'fprodld', 'fpricemop0', 'fpricemop1', 'fpricemop2');
					
					$headerMappers['utankt']  = array('0','name', 'tank_number', 'fuel_volume', 'fuel_value');
					$headerMappers['userlvt'] = array('0','name','service_level_number','fuel_MOP_number','fuel_volume','fuel_value');
					
					
					
					$headerMappers['utiert'] = array('0','name', 'price_tier_number', 'fuel_product_number', 'fuel_volume', 'fuel_value', 'fuel_product_id');
					// 0,"utiert  ", 1, 1,           218.662,            437.11, 7
					
					
					$headerMappers['uautot'] = array('0','name','fuel_mop_number','fuel_volume','fuel_value');
					$headerMappers['uhoset'] = array('0','name','fueling_point_number','fuel_product_number','fuel_volume','fuel_value', 'fuel_product_id');
					$headerMappers['edispt'] = array('0','name','fueling_point_number','fuel_product_number','fuel_volume','fuel_value','fuel_pointing_status','fuel_product_id');
					$headerMappers['dcrstat'] = array('0','name','dcr_number','fuel_mop_number','number_of_customers','fuel_volume','fuel_value','percentage_fueling_point','percentage_of_dcr','percentage_of_fuel');
					$headerMappers['tottax'] = array('0','name','tax_table_index','total_taxes','total_taxable_sales','total_exempt_taxes','total_tax_exempt_sales');
					
					$arrayData =  explode("\r\n", $data); 
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//print_r();exit;
						//print_r($row);
						$firstNode = trim(str_replace('"', "", (string)$row[1]));
						
						if(isset($headerMappers[$firstNode])) {
							$row[1] = $firstNode;
							$finalUpdate[$row[1]][] = array_combine($headerMappers[$firstNode], $row);
						}
					}
					$message .=  print_r($finalUpdate, true) . "\n";
					
					
					$this->loadModel('RubyHeader');
					$this->loadModel('RubyUprodt');
					$this->loadModel('RubyUtankt');
					
					
					foreach($finalUpdate as $rubyHeader => $rubyRow) {
						if ($rubyHeader == 'head') {
							$_data['RubyHeader'] 			  = $rubyRow[0];
							$_data['RubyHeader']['store_id']  = $storeId;
							
							$beginning_date_time_arr = str_split($_data['RubyHeader']['beginning_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['beginning_date_time'] = date('Y-m-d', strtotime('20'.$beginning_date_time_arr[2].$beginning_date_time_arr[0].$beginning_date_time_arr[1].$beginning_date_time_arr[3].$beginning_date_time_arr[4].$beginning_date_time_arr[5]));
							
							
							$ending_date_time_arr = str_split($_data['RubyHeader']['ending_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['ending_date_time'] = date('Y-m-d', strtotime('20'.$ending_date_time_arr[2].$ending_date_time_arr[0].$ending_date_time_arr[1].$ending_date_time_arr[3].$ending_date_time_arr[4].$ending_date_time_arr[5]));
							
							
							$_rowId = $this->RubyHeader->field('RubyHeader.id', $_data['RubyHeader']);
							
							if($_rowId > 0 ) $_data['RubyHeader']['id']  = $_rowId;
							
							$this->RubyHeader->save($_data);
						} else if ($rubyHeader == 'uprodt') {
							
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'fuel_product_number' => $_value['fuel_product_number'],
												'fmop1' 			  => $_value['fmop1'],
											  );
								
								$_rowId = $this->RubyUprodt->field('RubyUprodt.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyUprodt->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyUprodt->saveAll($_updateRubyRow);
						} else if ($rubyHeader == 'utankt') {
							$this->deletInsertDataSet($message, 'RubyUtankt', $rubyRow);
						} else if ($rubyHeader == 'userlvt') {
							$this->deletInsertDataSet($message, 'RubyUserlvt', $rubyRow);
						}  else if ($rubyHeader == 'uautot') {
							$this->loadModel('RubyUautot');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {							
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'fuel_mop_number' => $_value['fuel_mop_number'],
											  );
											  
								$_rowId = $this->RubyUautot->field('RubyUautot.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
								
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyUautot->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyUautot->saveAll($_updateRubyRow);
						}   else if ($rubyHeader == 'uhoset') {
							$this->loadModel('RubyUhoset');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {							
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'fuel_product_number' => $_value['fuel_product_number'],
											  );
											  
								$_rowId = $this->RubyUhoset->field('RubyUhoset.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
								
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyUhoset->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyUhoset->saveAll($_updateRubyRow);
						}  else if ($rubyHeader == 'edispt') {
							$this->loadModel('RubyEdispt');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {							
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'fuel_product_number' => $_value['fuel_product_number'],
											  );
											  
								$_rowId = $this->RubyEdispt->field('RubyEdispt.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
								
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyEdispt->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyEdispt->saveAll($_updateRubyRow);
						}  else if ($rubyHeader == 'dcrstat') {
							$this->loadModel('RubyDcrstat');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {							
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'fuel_mop_number' => $_value['fuel_mop_number'],
											  );
											  
								$_rowId = $this->RubyDcrstat->field('RubyDcrstat.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
								
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							$this->RubyDcrstat->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyDcrstat->saveAll($_updateRubyRow);
						} else if ($rubyHeader == 'utiert') {
							$this->loadModel('RubyUtiert');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {							
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'name' 			 => $_value['name'],
												'price_tier_number' => $_value['price_tier_number'],
											  );
											  
								$_rowId = $this->RubyUtiert->field('RubyUtiert.id', $_condition);
								if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
								
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyUtiert->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyUtiert->saveAll($_updateRubyRow);
						}
					}
				} else if (strpos($process['Posrequester']['cmd'], 'GET ALL TOTALS plutot') !== false) {
					$message .= 'Start processing GET ALL TOTALS '. $process['Posrequester']['cmd'];
					$headerMappers = $finalUpdate = array();

					$headerMappers['head']   = array('0', 'header', 'beginning_date_time', 'ending_date_time', 'store_number', 'sequence_number', 'operator_id', 'keyboard_number', 'requested_period_number', 'requested_set_number', 'next_lower_period_b', 'next_lower_period_e');

					$headerMappers['pluttl'] = array('0','data_name','plu_no', 'modifier_name', 'item_sold_at_discount_price', 'item_sold_at_promo_price', 'item_sold_at_override_price', 'selling_price', 'base_seling_price' ,'no_of_items_sold', 'no_of_customer', 'reserve', 'item_sold_outside', 'item_sold_at_combo_price', 'item_sold_at_match_price', 'item_sold_at_open_price');
					
					//0	data_name	plu_no	modifier_name	item_sold_at_discount_price	item_sold_at_promo_price	item_sold_at_override_price		selling_price	no_of_items_sold	No_of_customer	reserve	item_sold_outside	item_sold_at_combo_price	item_sold_at_match_price

					$arrayData =  explode("\r\n", $data); 
					
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						$firstNode = trim(str_replace('"', "", (string)$row[1]));
						
						if(isset($headerMappers[$firstNode])) {
							//print_r($headerMappers[$firstNode]);
							//print_r($row);
							$row[1] = $firstNode;
							$finalUpdate[$row[1]][] = @array_combine($headerMappers[$firstNode], $row);
						}
					}
					$message .=  print_r($finalUpdate, true) . "\n";
					
					//print_r($finalUpdate);exit;
					
					$this->loadModel('RubyHeader');
					$this->loadModel('RubyPluttl');
					$this->loadModel('RGroceryItem');
					
					foreach($finalUpdate as $rubyHeader => $rubyRow) {
						if ($rubyHeader == 'head') {
							$_data['RubyHeader'] 			  = $rubyRow[0];
							$_data['RubyHeader']['store_id']  = $storeId;
							
							$beginning_date_time_arr = str_split($_data['RubyHeader']['beginning_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['beginning_date_time'] = date('Y-m-d', strtotime('20'.$beginning_date_time_arr[2].$beginning_date_time_arr[0].$beginning_date_time_arr[1].$beginning_date_time_arr[3].$beginning_date_time_arr[4].$beginning_date_time_arr[5]));
							
							
							$ending_date_time_arr = str_split($_data['RubyHeader']['ending_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['ending_date_time'] = date('Y-m-d', strtotime('20'.$ending_date_time_arr[2].$ending_date_time_arr[0].$ending_date_time_arr[1].$ending_date_time_arr[3].$ending_date_time_arr[4].$ending_date_time_arr[5]));
							
							
							$_rowId = $this->RubyHeader->field('RubyHeader.id', $_data['RubyHeader']);
							
							if($_rowId > 0 ) $_data['RubyHeader']['id']  = $_rowId;
							
							$this->RubyHeader->save($_data);
						} else if ($rubyHeader == 'pluttl') {
							
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												'data_name' 			 => $_value['data_name'],
												'plu_no' 		=> $_value['plu_no'],
											  );
								
								$_rowId = $this->RubyPluttl->field('RubyPluttl.id', $_condition);
							
								if($_rowId > 0 ) { 
									$_value['id']  = $_rowId;
								}
								
								///echo $_value['plu_no'] . '<br>';
								$groceryId = $this->RGroceryItem->field('RGroceryItem.id', array('plu_no' => $_value['plu_no']));
								
								if ($groceryId) {
									//$this->RGroceryItem->id = $groceryId;
									//$this->RGroceryItem->saveField('sales_inventory', $_value['no_of_items_sold']);
									//var_dump($_value['no_of_items_sold']);
									$message .=  $_value['plu_no'] . "no_of_items_sold " . $_value['no_of_items_sold'] . "\n";
								}
								
								$_value['store_id']  = $storeId;
								$_updateRubyRow[$_key] = $_value;
							}
							
							//$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyPluttl->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyPluttl->saveAll($_updateRubyRow);
							
							//echo $message;exit;
							
						}
					}
				} else if ($process['Posrequester']['cmd'] == 'GET ALL DATA mop') {
					$this->loadModel('RubyMop');
					$message .= 'Data Found for ftotal12';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						$_row['number']     = $row[1];
						$_row['name']  		= $row[2];
						$_row['store_id']   = $storeId;
						$finalUpdate[] = $_row;
					}

					try {
						$this->RubyMop->deleteAll(array('RubyMop.store_id'=> $storeId));
						$this->RubyMop->saveAll($finalUpdate);
					} catch(Exception $e) {
						echo $e->getMessage();
					}
					
					//print_r($finalUpdate);exit;
					$message  .=  print_r($finalUpdate, true) . "\n";
				} else if ($process['Posrequester']['cmd'] == 'GET ALL DATA ftank') {
					$this->loadModel('RubyFtank');
					$message .= 'Data Found for ftotal12';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//$row['name'] = trim(str_replace('"', "", (string)$row[1]));
						
						$_row = array_combine(array('0', 'tank_number', 'name'), $row);
						$_row['store_id']  = $storeId;
						$finalUpdate[] = $_row;
					}
					
					$this->RubyFtank->saveAll($finalUpdate);
					$message  .=  print_r($finalUpdate, true) . "\n";
				} else if ($process['Posrequester']['cmd'] == 'GET ALL DATA plu') {
					$this->loadModel('RGroceryItem');
					$message .= 'Data Found for RGroceryItem';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//$row['name'] = trim(str_replace('"', "", (string)$row[1]));
						
						//$_row = array_combine(array('0', 'plu_no', 'description', 'price'), $row);
						$_row['store_id']  = $storeId;
						$_row['plu_no']  = $row[1];
						$_row['r_grocery_department_id']  = $row[3];
						$_row['price']  = $row[4];
						$_row['description']  = $row[7];
					
						$rowId = $this->RGroceryItem->field('RGroceryItem.id', array('RGroceryItem.store_id' => $storeId, 'RGroceryItem.plu_no' => $_row['plu_no']));
						
						if ($rowId > 0) {
							$_row['id']  = $rowId;
							$message .= 'row updated '. $rowId;
						}
						
						$finalUpdate[] = $_row;
					}
					
					$this->RubyFtank->saveAll($finalUpdate);
					$message  .=  print_r($finalUpdate, true) . "\n";
				} else if ($process['Posrequester']['cmd'] == 'GET ALL DATA dep') {
					$this->loadModel('RubyDepartment');
					$message .= 'Data Found for RubyDepartment';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//$row['name'] = trim(str_replace('"', "", (string)$row[1]));
						
						//$_row = @array_combine(array('0', 'number', 'name', 'tax'), $row);
						$_row['store_id']  = $storeId;
						$_row['number']  = $row[1];
						$_row['name']  = $row[2];
						$_row['tax']  = $row[3];
						
						$finalUpdate[] = $_row;
					}
					
					$this->RubyDepartment->saveAll($finalUpdate);
					$message  .=  print_r($finalUpdate, true) . "\n";
				} else if ($process['Posrequester']['cmd'] == 'GET ALL DATA fprod') {
					$this->loadModel('RubyFprod');
					$message .= 'Data Found for RubyFprod';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						//$row['name'] = trim(str_replace('"', "", (string)$row[1]));
						
						//$_row = @array_combine(array('0', 'number', 'name', 'tax'), $row);
						$_row['store_id']  = $storeId;
						$_row['fuel_product_number']  = $row[1];
						$_row['fuel_product_name']  = $row[2];
						$_row['display_name']  		= $row[2];
						$_row['status']  		= 1;
						//$_row['tax']  = $row[3];
						$finalUpdate[] = $_row;
					}
					
					$this->RubyFprod->saveAll($finalUpdate);
					$message  .=  print_r($finalUpdate, true) . "\n";
				} else if (strpos($process['Posrequester']['cmd'], 'GET ALL TOTALS deptot') !== false) {	
				
					//$this->loadModel('RubyDeptotal');
					$message .= 'Data Found for GET ALL TOTALS deptot12';
					
					$arrayData =  explode("\r\n", $data);
					$message  .=  print_r($arrayData, true) . "\n";
					
					//$this->RubyDeptotal->saveAll($finalUpdate);
					$message  .=  print_r($arrayData, true) . "\n";
					
					$headerMappers['head']   = array('0', 'header', 'beginning_date_time', 'ending_date_time', 'store_number', 'sequence_number', 'operator_id', 'keyboard_number', 'requested_period_number', 'requested_set_number', 'next_lower_period_b', 'next_lower_period_e');
					
					$headerMappers['deptot'] = array('0', 'name', 'department_number', 'number_of_trasactions', 'f4', 'f5', 'f6', 'number_items', 'net_sales', 'f9', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15');
						
					foreach($arrayData as $key => $value) {
						$row = str_getcsv($value);
						$firstNode = trim(str_replace('"', "", (string)$row[1]));
						if(isset($headerMappers[$firstNode])) {
							$row[1] = $firstNode;
							$finalUpdate[$row[1]][] = array_combine($headerMappers[$firstNode], $row);
						}
					}
					
					foreach($finalUpdate as $rubyHeader => $rubyRow) {
						if ($rubyHeader == 'head') {
							$_data['RubyHeader'] 			  = $rubyRow[0];
							$_data['RubyHeader']['store_id']  = $storeId;
							
							$beginning_date_time_arr = str_split($_data['RubyHeader']['beginning_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['beginning_date_time'] = date('Y-m-d', strtotime('20'.$beginning_date_time_arr[2].$beginning_date_time_arr[0].$beginning_date_time_arr[1].$beginning_date_time_arr[3].$beginning_date_time_arr[4].$beginning_date_time_arr[5]));
							
							
							$ending_date_time_arr = str_split($_data['RubyHeader']['ending_date_time'], 2);//mmddyyhhmmss
							$_data['RubyHeader']['ending_date_time'] = date('Y-m-d', strtotime('20'.$ending_date_time_arr[2].$ending_date_time_arr[0].$ending_date_time_arr[1].$ending_date_time_arr[3].$ending_date_time_arr[4].$ending_date_time_arr[5]));
						} else if ($rubyHeader == 'deptot') {
							$this->loadModel('RubyDeptotal');
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {
								
								$_value['store_id'] 		   = $_data['RubyHeader']['store_id'];
								$_value['beginning_date_time'] = $_data['RubyHeader']['beginning_date_time'];
								$_value['ending_date_time']    = $_data['RubyHeader']['ending_date_time'];
																								
								//$_rowId = $this->RubyDeptotal->field('RubyDeptotal.id', $_condition);
								//if($_rowId > 0 ) $_value['id']  = $_rowId;
								
								$_updateRubyRow[$_key] = $_value;
							}
							
							$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyDeptotal->deleteAll(
								array(
								'store_id'=> $_data['RubyHeader']['store_id'],
								'beginning_date_time' => $_value['beginning_date_time'],
								'ending_date_time' => $_value['ending_date_time']
								)
							);
							$this->RubyDeptotal->saveAll($_updateRubyRow);
							//print_r($_updateRubyRow);exit;
							
							
						}
						
					}
					
					//$message  .=  print_r($finalUpdate, true) . "\n";
						
				} else if (strpos($process['Posrequester']['cmd'], 'vrubyrept^reptname=plu') !== false) {
					$message .= 'Start processing vrubyrept^reptname=plu^period=2^filename=current '. $process['Posrequester']['cmd'];
					$headerMappers = $finalUpdate = array();

					$headerMappers['head']   = array('0', 'header', 'beginning_date_time', 'ending_date_time', 'store_number', 'sequence_number', 'operator_id', 'keyboard_number', 'requested_period_number', 'requested_set_number', 'next_lower_period_b', 'next_lower_period_e');

					$headerMappers['pluttl'] = array('0','data_name','plu_no', 'modifier_name', 'item_sold_at_discount_price', 'item_sold_at_promo_price', 'item_sold_at_override_price', 'selling_price', 'base_seling_price' ,'no_of_items_sold', 'no_of_customer', 'reserve', 'item_sold_outside', 'item_sold_at_combo_price', 'item_sold_at_match_price', 'item_sold_at_open_price');
					
					//0	data_name	plu_no	modifier_name	item_sold_at_discount_price	item_sold_at_promo_price	item_sold_at_override_price		selling_price	no_of_items_sold	No_of_customer	reserve	item_sold_outside	item_sold_at_combo_price	item_sold_at_match_price

					$dataObj = simplexml_load_string(str_replace(":", "", $data));
					print_r($dataObj);
					
					$vsperiod =  (array)$dataObj->vsperiod;
					$arrayData['head'] = $vsperiod['@attributes'];
					$arrayData['head']['beginning_date_time'] = substr($arrayData['head']['periodBeginDate'], 0, 10);
					$arrayData['head']['ending_date_time'] = date("Y-m-d",strtotime("+1 day", strtotime($arrayData['head']['beginning_date_time'])));
					
					foreach($dataObj->totals->pluInfo as $_key => $pluinforow) {
						$_prow = (array)$pluinforow;
						$vspluBase = (array)$_prow['vspluBase'];
						
						$_prow['plu_no'] = $vspluBase['upc'];
						$_prow['modifier_name'] = $vspluBase['modifier'];
						$_prow['selling_price'] = $_prow['salePrice'];
						$_prow['base_seling_price'] = $_prow['originalPrice'];
						
						$netSales = $_prow['netSales'];
						
						$_prow['no_of_items_sold'] = (String)$netSales->count;
						
						$arrayData['pluttl'][] = $_prow;
						//print_r($pluinforow);exit;
					}
					
					

					$finalUpdate = $arrayData;
					
					$message .=  print_r($finalUpdate, true) . "\n";
					
					//print_r($finalUpdate);exit;
					
					$this->loadModel('RubyHeader');
					$this->loadModel('RubyPluttl');
					$this->loadModel('RGroceryItem');
					
					foreach($finalUpdate as $rubyHeader => $rubyRow) {
						if ($rubyHeader == 'head') {
							
							$_data['RubyHeader'] 			  = $rubyRow;
							$_data['RubyHeader']['store_id']  = $storeId;
							
							//var_dump($_data);exit;
							//$beginning_date_time_arr = str_split($_data['RubyHeader']['beginning_date_time'], 2);//mmddyyhhmmss
							//$_data['RubyHeader']['beginning_date_time'] = date('Y-m-d', strtotime('20'.$beginning_date_time_arr[2].$beginning_date_time_arr[0].$beginning_date_time_arr[1].$beginning_date_time_arr[3].$beginning_date_time_arr[4].$beginning_date_time_arr[5]));
							
							
							//$ending_date_time_arr = str_split($_data['RubyHeader']['ending_date_time'], 2);//mmddyyhhmmss
							//$_data['RubyHeader']['ending_date_time'] = date('Y-m-d', strtotime('20'.$ending_date_time_arr[2].$ending_date_time_arr[0].$ending_date_time_arr[1].$ending_date_time_arr[3].$ending_date_time_arr[4].$ending_date_time_arr[5]));
							
							//var_dump($_data);
							$_rowId = $this->RubyHeader->field('RubyHeader.id', array(
									'beginning_date_time' => $rubyRow['beginning_date_time'],
									'ending_date_time' => $rubyRow['ending_date_time'],
									)
							);
							
							//var_dump($_rowId);
							
							if($_rowId > 0 ) $_data['RubyHeader']['id']  = $_rowId;
							
							//exit;
							
							$this->RubyHeader->save($_data);
						} else if ($rubyHeader == 'pluttl') {
							
							$_updateRubyRow = array();
							foreach($rubyRow as $_key => $_value) {
								$_value['ruby_header_id'] = $this->RubyHeader->id;
								
								$_condition = array(
												'ruby_header_id' => $_value['ruby_header_id'],
												//'data_name' 		=> $_value['data_name'],
												'plu_no' 		=> $_value['plu_no'],
											  );
								
								$_rowId = $this->RubyPluttl->field('RubyPluttl.id', $_condition);
							
								if($_rowId > 0 ) { 
									$_value['id']  = $_rowId;
								}
								
								///echo $_value['plu_no'] . '<br>';
								$groceryId = $this->RGroceryItem->field('RGroceryItem.id', array('plu_no' => $_value['plu_no']));
								
								if ($groceryId) {
									//$this->RGroceryItem->id = $groceryId;
									//$this->RGroceryItem->saveField('sales_inventory', $_value['no_of_items_sold']);
									//var_dump($_value['no_of_items_sold']);
									$message .=  $_value['plu_no'] . "no_of_items_sold " . $_value['no_of_items_sold'] . "\n";
								}
								
								$_value['store_id']  = $storeId;
								$_updateRubyRow[$_key] = $_value;
							}
							
							//$message .=  print_r($_updateRubyRow, true) . "\n";
							
							$this->RubyPluttl->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
							$this->RubyPluttl->saveAll($_updateRubyRow);
							
							//echo $message;exit;
							
						}
					}
				}
				
			} catch(Exception $e) {
				$message .= $e->getMessage();
			}
			
			if($process['Posrequester']['recursive'] > 0) {
				$updateData['Posrequester']['id'] = $process['Posrequester']['id'];
				$updateData['Posrequester']['status'] = 'PROCESS';
				$updateData['Posrequester']['next_scheduled'] = strtotime($process['Posrequester']['after_hour']);
				$this->Posrequester->save($updateData);
				$message .=  "Recursive Received - Next Sceduled - ". date(DATE_ATOM,$updateData['Posrequester']['next_scheduled']) . "\n";	
			} else {
				$message .=  " Delete expired request" . "\n";	
				$this->Posrequester->delete();
			}
			$message .=  "########### END Processing POS REQUEST ############ ". "\n";		
			$this->debugEmail2('POS RUBY  Store-'. $storeId, $message);
		}
		
		//echo "All Request Process Successfully";
		exit;
    }
	
	public function deleteOldData()
	{
		$this->Posrequester->deleteAll(array('status'=> 'PROCESS', 'recursive' => 0));
	}
	
	function processSendRequest()
	{
		$rdata = $this->Posrequester->find('all', array('conditions' => array('status'=> 'SEND_POS')));
		foreach($rdata as $key => $process) {
			if(!empty($process['Posrequester'])) {
				$file = $process['Posrequester']['store_id'].'/'.$process['Posrequester']['synupid'].'.xml';
				if($file != "" && is_file(WWW_ROOT.'files/'. $file)) {
					$xml = file_get_contents(WWW_ROOT.'files/'. $file);
					$xml = str_replace(":", "", $xml);
					$xmlObj = @simplexml_load_string($xml);
					if ($xmlObj->ItemMaintenance->ITTDetail) {
						$ITTDetail = $xmlObj->ItemMaintenance->ITTDetail;
						$this->_updatePLUs($ITTDetail, $process);
					} elseif($xmlObj->MerchandiseCodeMaintenance->MCTDetail) {
						$MCTDetail = $xmlObj->MerchandiseCodeMaintenance->MCTDetail;
						$this->_updateDepartments($MCTDetail, $process);
					} elseif($xmlObj->TankProductMaintenance->TPTDetail) {
						$TankData = $xmlObj->TankProductMaintenance->TPTDetail->TankData;
						$this->_updateTanks($TankData, $process);
					} elseif($xmlObj->periodInfo) {
						$periodInfo = $xmlObj->periodInfo;
						$this->_updateShifts($periodInfo, $process);
						//die('test');
					} elseif($xmlObj->totals->tierProductInfo) {
						$this->_tierProductInfo($xmlObj, $process);
						//print_r($xmlObj);exit;
					}
					//exit;
					//############### DELETE RECORD ########################
					unlink(WWW_ROOT.'files/'. $file);
					$this->Posrequester->id = $process['Posrequester']['id'];
					
					if($process['Posrequester']['recursive'] > 0) {
						$updateData['Posrequester']['id'] = $process['Posrequester']['id'];
						$updateData['Posrequester']['status'] = 'PROCESS';
						$updateData['Posrequester']['next_scheduled'] = strtotime($process['Posrequester']['after_hour']);
						$this->Posrequester->save($updateData);
					} else {
						$this->Posrequester->saveField('status', 'PROCESS');
					}
					
					$this->debugEmail2('File Processed - '. $file, $process);
					//############### DELETE RECORD ########################
					
					//exit;
				} else {
					echo "File NOT Found ".WWW_ROOT.'files/'. $file . "<BR>";
				}
			}
		}
	}
	
	function processRecursiveRequest()
	{
		$now = strtotime('NOW');
		
		//Fetch all request whoes scheduled either passed or matched with current time
		$rdata = $this->Posrequester->find('all', array('conditions' => array('status'=> 'PROCESS', 'recursive' => 1, 'next_scheduled <=' => $now)));
		
		if (!empty($rdata)) {
			foreach($rdata as $key => $process) {
				if(!empty($process['Posrequester'])) {
					if($process['Posrequester']['recursive'] > 0) {
						$updateData['Posrequester']['id'] = $process['Posrequester']['id'];
						$updateData['Posrequester']['status']  = 'PENDING';
						$updateData['Posrequester']['active']  = 1;
						$updateData['Posrequester']['synupid'] = strtolower(str_replace(" ","_", $data['Posrequester']['cmd'])).'_'.$this->getSyncId();
						$this->Posrequester->save($updateData);
					}
				}
			}
		}
	}
	
	function getSyncId()
	{
		return date("Y-m-d-H-i-s");
	}
	
	
	function _updateRequestStatus($updateData)
	{
		if (isset($updateData['Posrequester']['next_scheduled']) ) {
			if ($updateData['Posrequester']['recursive'] > 0 ) {
				$updateData['Posrequester']['status'] = 'PENDING';
				$updateData['Posrequester']['active'] = 1;
				$updateData['Posrequester']['synupid'] = strtolower(str_replace(" ","_", $updateData['Posrequester']['cmd'])).'_'.$this->getSyncId();
				
				//$updateData['Posrequester']['cmd'].'_'.$this->getSyncId();
				$this->Posrequester->save($updateData);
				$this->debugEmail($updateData, 'next_scheduled NOW '. "$nowHour >= ". $updateData['Posrequester']['next_scheduled']);
			} else {
				//delete iterator_apply
				$this->Posrequester->id = $updateData['Posrequester']['id'];
				$this->Posrequester->delete();
			}
		}
	}
	
	function _updateDepartments($MCTDetail, $configData)
	{
		$this->loadModel('Department');
        $categoryData = array();
		
        foreach ($MCTDetail as $key => $MCT) {
            $updateData = array();
			$updateData['merchandise_code'] = (string) $MCT->MerchandiseCode;
            $updateData['name'] = (string) $MCT->MerchandiseCodeDescription;
			$updateData['active_flag'] = (string) $MCT->ActiveFlag['value'];;
            $updateData['tax_strategy_id'] = (string) $MCT->TaxStrategyID;
			$updateData['minAmt'] = (string) $MCT->OpenSaleLimits->LowAmount;
			$updateData['maxAmt'] = (string) $MCT->OpenSaleLimits->HighAmount;
			$updateData['isNegative'] = (string) $MCT->NegativeFlag['value'];
			$updateData['isAllowFS'] = (string) $MCT->ProhibitOpenSalesFlag['value'];
			
            // flags taxRates
            $updateData['store_id'] = $configData['Posrequester']['store_id'];

            $depId = $this->Department->field('Department.merchandise_code', array('merchandise_code' => $updateData['merchandise_code']));
            if ($depId > 0) {
                $updateData['id'] = $depId;
            }
            $updateData['sync_id'] = $configData['Posrequester']['synupid'];
            $categoryData[] = $updateData;
        }
		print_r($categoryData);exit;
        $this->Department->saveAll($categoryData);
	
	}
	
	 function _updatePLUs($plus, $configData) {
        $this->loadModel('Plus');
        $categoryData = array();
        //print_r($configData);exit;
        foreach ($plus as $key => $plu) {
			//print_r($plu);exit;
            $updateData = array();
            $updateData['upc'] = (string) $plu->ItemCode->POSCode;
            $updateData['upcModifier'] = (string) $plu->ItemCode->POSCodeModifier;
            $updateData['description'] = (string) $plu->ITTData->Description;
            $updateData['department'] = (string) $plu->ITTData->MerchandiseCode;
            $updateData['price'] = (string) $plu->ITTData->RegularSellPrice;
            $updateData['SellUnit'] = (string) $plu->ITTData->SellingUnits;

            // flags taxRates
            $updateData['store_id'] = $configData['Posrequester']['store_id'];

            $plusId = $this->Plus->field('Plus.id', array('upc' => $updateData['upc']));
            if ($plusId > 0) {
                $updateData['id'] = $plusId;
            }
            $updateData['sync_id'] = $configData['Posrequester']['synupid'];
            $categoryData[] = $updateData;
        }
		//print_r($categoryData);exit;
        $this->Plus->saveAll($categoryData);
    }
	
	function _updateTanks($tanks, $configData) {
        $this->loadModel('FuelType');
        $categoryData = array();
        //print_r($tanks);exit;
        foreach ($tanks as $key => $tank) {
			//print_r($tank);exit;
            $updateData = array();
            $updateData['tank'] = (string) $tank->TankID;
            $updateData['name'] = (string) $tank->TankDescription;
            $updateData['store_id'] = $configData['Posrequester']['store_id'];
			$updateData['fuel_product_id'] = (string) $tank->FuelProductID;
			//print_r($updateData);exit;
            
            $tId = $this->FuelType->field('FuelType.tank', array('tank' =>$updateData['tank']));
            if ($tId > 0) {
                $updateData['id'] = $tId;
            } else {
				$updateData['sync_id'] = $configData['Posrequester']['synupid'];
				$categoryData[] = $updateData;
			}
        }
		
		//print_r($categoryData);exit;
        $this->FuelType->saveAll($categoryData);
    }
	
	
	function _updateShifts($periodInfo, $configData) {
        $this->loadModel('Shift');
        $categoryData = array();
		
        foreach ($periodInfo as $key => $period) {
			if ($period->name != 'current') {
				foreach($period->reportParameters as $_k => $param) {
					$par = (array)$param->reportParameter;
					$num = (string)$param->reportParameter;
					if($num == 1)  {
					//Shiftwise 
						$name = (string)$par['@attributes']['name'];
						$nameParams = explode('.', (string)$period->name);
						
						$updateData = array();
						$updateData['number'] = $nameParams[1];
						$updateData['name'] = (string) $nameParams[0];
						
						$updateData['desc'] = (string) $period->desc;
						$updateData['store_id'] = $configData['Posrequester']['store_id'];
						$updateData['reportParameter'] = $name.'|'.$num;
						
						$tId = $this->Shift->field('Shift.id', $updateData);
						if ($tId > 0) {
							$updateData['id'] = $tId;
						}
						$updateData['sync_id'] = $configData['Posrequester']['synupid'];
						$categoryData[] = $updateData;
					}
				}
			}
        }
		
		///print_r($categoryData);exit;
        $this->Shift->saveAll($categoryData);
    }
	
	
	
	function _pluInfo($xmlObj, $configData) {
        
		$this->loadModel('Plus');
		$this->loadModel('StoreInventory');
		
		$message =  "########### Start updating Sales Data ############ ". "\n";
		list($shiftId, $message, $period_beginDate) = $this->getGenerateShiftId($xmlObj, $configData, $message);
		
		$categoryData = array();
		if ($shiftId) {
			//Update sales
			$productInfo = $xmlObj->totals->pluInfo;
			foreach ($productInfo as $key => $plud) {
				
			//	print_r($plud);exit;
				
				$vsdeptBase =  (array)$plud->vspluBase;
				
				$upc = (string)$plud->vspluBase->upc;
				$name = (string)$plud->vspluBase->name;
				
				$deptUpdateData = array('upc' => $upc, 'store_id' => $configData['Posrequester']['store_id']);
				
				//print_r($fuelUpdateData);exit;
				$message .=  "Search Dept data ".print_r($deptUpdateData, true). "\n";	
				
				$plu_id = $this->Plus->field('Plus.id', $deptUpdateData);
				
				if(!$plu_id ) {
					$message .=  "Create New PLU ".print_r($upc, true). "\n";	
					$this->Plus->save($deptUpdateData);
					$plu_id = $this->Plus->id;
				}  else {
					$message .=  "Matching Plus Found ".print_r($name, true). "\n";	
				}
							
				$updateData = array();
				$updateData['plus_id'] 			= $plu_id;
				$updateData['shift_id'] 		= $shiftId;
				$updateData['date'] 			= $period_beginDate;
				$updateData['store_id']			= $configData['Posrequester']['store_id'];
				$updateData['name']				= $name;
				
				
				$invId = $this->StoreInventory->field('StoreInventory.id', $updateData);
								
				if ($invId > 0) {
					$updateData['id'] = $invId;
				} 
				
				$salePrice = (string)$plud->salePrice;
				$count 	   = (string)$plud->netSales->count;
				$saleAmount = (float)$salePrice * (float)$count;
				
				$updateData['sale_qty'] = $count;
				$updateData['sale_amount'] = number_format($saleAmount, 3);
				
				//$updateData['grossSales'] = (string)$plud->grossSales;
				$updateData['percentOfSales'] = (string)$plud->percentOfSales;
								
				if ($period_beginDate  == date('yy-mm-dd')) {
					$updateData['status'] = 'Running';
				} else {
					$updateData['status'] = 'Close';
				}
				//pr($updateData);exit;
				$message .=  "Prepare Data FoR Update ".print_r($updateData, true). "\n";	
				if (isset($updateData['id']) && $updateData['id'] > 0) {
					$this->StoreInventory->save($updateData);
					//$message .=  "Updated Previous Data ".print_r($updateData, true). "\n";	
				} else {
					$categoryData[] = $updateData;
				}
			}
			
			if (!empty($categoryData)) {
				$this->StoreInventory->saveAll($categoryData);
				$message .=  "Updated New Data ".print_r($updateData, true). "\n";	
			}
			
			$message .=  "########### END updating Sales Data ############ ". "\n";
		}
		return $message;
    }
	
	
	
	function _deptInfo($xmlObj, $configData) {
        
		$this->loadModel('Department');
		$this->loadModel('DepartmentInventory');
		
		$message =  "########### Start updating Sales Data ############ ". "\n";
		list($shiftId, $message, $period_beginDate) = $this->getGenerateShiftId($xmlObj, $configData, $message);
		
		$categoryData = array();
		if ($shiftId) {
			//Update sales
			$productInfo = $xmlObj->totals->deptInfo;
			foreach ($productInfo as $key => $dept) {
				
				$vsdeptBase =  (array)$dept->vsdeptBase;
				
				$sysid = (string)$vsdeptBase['@attributes']['sysid'];
				$number = (string)$vsdeptBase['@attributes']['deptType'];
				$fname = (string)$dept->vsdeptBase->name;
				
				$deptUpdateData = array('name' => $fname, 'store_id' => $configData['Posrequester']['store_id']);
				
				//print_r($fuelUpdateData);exit;
				$message .=  "Search Dept data ".print_r($deptUpdateData, true). "\n";	
				
				$dept_id = $this->Department->field('Department.id', $deptUpdateData);
				
				//var_dump($dept_id);
				//pr($deptUpdateData);
				//exit;
				
				
				
				if(!$dept_id ) {
					$message .=  "Create New Depart ".print_r($fname, true). "\n";	
					$this->Department->save($deptUpdateData);
					$dept_id = $this->Department->id;
				}  else {
					$message .=  "Matching Fuel Type Found ".print_r($fname, true). "\n";	
				}
							
				$updateData = array();
				$updateData['department_id'] 	= $dept_id;
				$updateData['shift_id'] 		= $shiftId;
				$updateData['date'] 			= $period_beginDate;
				$updateData['store_id']			= $configData['Posrequester']['store_id'];
				$updateData['name']				= $fname;
				
				
				$invId = $this->DepartmentInventory->field('DepartmentInventory.id', $updateData);
								
				if ($invId > 0) {
					$updateData['id'] = $invId;
				} 
				$updateData['sale_qty'] = (string)$dept->netSales->itemCount;
				$updateData['sale_amount'] = (string)$dept->netSales->amount;
				
				$updateData['grossSales'] = (string)$dept->grossSales;
				$updateData['percentOfSales'] = (string)$dept->percentOfSales;
								
				if ($period_beginDate  == date('yy-mm-dd')) {
					$updateData['status'] = 'Running';
				} else {
					$updateData['status'] = 'Close';
				}
				//pr($updateData);exit;
				$message .=  "Prepare Data FoR Update ".print_r($updateData, true). "\n";	
				if (isset($updateData['id']) && $updateData['id'] > 0) {
					$this->DepartmentInventory->save($updateData);
					//$message .=  "Updated Previous Data ".print_r($updateData, true). "\n";	
				} else {
					$categoryData[] = $updateData;
				}
			}
			
			if (!empty($categoryData)) {
				$this->DepartmentInventory->saveAll($categoryData);
				$message .=  "Updated New Data ".print_r($updateData, true). "\n";	
			}
			
			$message .=  "########### END updating Sales Data ############ ". "\n";
		}
		return $message;
    }
	
	function getGenerateShiftId($xmlObj, $configData, $message)
	{
		$this->loadModel('Shift');
		$shiftId = null;
		if ($vsperiod = (array)$xmlObj->vsperiod) {
			$vsperiodData =  $vsperiod['@attributes'];
			$updateData = array();
			$updateData['number'] = (string)$vsperiodData['periodSeqNum'];
			$updateData['store_id'] = $configData['Posrequester']['store_id'];
			
			$message .=  "Check Previous Shift ".print_r($updateData, true). "\n";	
			$_name = (string) $vsperiodData['periodBeginDate'];
			$updateData['periodBeginDate'] = (string)$vsperiodData['periodBeginDate'];
			
			$periodEndDate = null;
			if (isset($vsperiodData['periodEndDate'])) {
				$periodEndDate = (string) $vsperiodData['periodEndDate'];
				$updateData['periodEndDate'] = (string)$vsperiodData['periodEndDate'];
			}
			
			$period_beginDate = substr($_name, 0, 10);
			
			if (is_null($periodEndDate)) {
				//2014-09-19T233552-0400
				$dateStr = explode('-', $period_beginDate);
				$dateStr[2] = $dateStr[2] + 1;
				$period_beginDate = implode('-', $dateStr);
			} else {
				$period_beginDate = substr($periodEndDate, 0, 10);
			}
			
			//$period_beginDate = substr($_name,0, 10);
			
			$updateData['desc']		= (string) $vsperiodData['periodBeginDate'];
			$updateData['name'] 	= $period_beginDate;
			
			$shiftId = $this->Shift->field('id', array('number' => $updateData['number'], 'store_id' => $updateData['store_id']));
			
			$message .=  "Check first Shift ".print_r($shiftId, true). "\n";	
			
			if ($shiftId > 0) {
				$message .=  "Shift Found ".print_r($updateData, true). "\n";	
				//$updateData['id'] = $tId;
				//$shiftId = $shiftData['Shift']['id'];
			} else {
				$message .=  "Create New Shift in System".print_r($updateData, true). "\n";	
				$this->Shift->save($updateData);
				$shiftId = $this->Shift->id;
			}
			
			return array($shiftId, $message, $period_beginDate);
			
		}
	}
	
	
	function _tierProductInfo($xmlObj, $configData) {
        $this->loadModel('Shift');
		$this->loadModel('FuelType');
		$this->loadModel('FuelInventory');
		
		$message .=  "########### Start updating Sales Data ############ ". "\n";
		
        $categoryData = array();
		
		$vsperiod = (array)$xmlObj->vsperiod;
		$vsperiodData =  $vsperiod['@attributes'];
		$updateData = array();
		$updateData['number'] = (string)$vsperiodData['periodSeqNum'];
		$updateData['store_id'] = $configData['Posrequester']['store_id'];
		
		$message .=  "Check Previous Shift ".print_r($updateData, true). "\n";	
		$_name = (string) $vsperiodData['periodBeginDate'];
		$updateData['periodBeginDate'] = (string)$vsperiodData['periodBeginDate'];
		
		$periodEndDate = null;
		if (isset($vsperiodData['periodEndDate'])) {
			$periodEndDate = (string) $vsperiodData['periodEndDate'];
			$updateData['periodEndDate'] = (string)$vsperiodData['periodEndDate'];
		}
		
		$period_beginDate = substr($_name, 0, 10);
		
		if (is_null($periodEndDate)) {
			//2014-09-19T233552-0400
			$dateStr = explode('-', $period_beginDate);
			$dateStr[2] = $dateStr[2] + 1;
			$period_beginDate = implode('-', $dateStr);
		} else {
			$period_beginDate = substr($periodEndDate, 0, 10);
		}
		
		//$period_beginDate = substr($_name,0, 10);
		
		$updateData['desc']		= (string) $vsperiodData['periodBeginDate'];
		$updateData['name'] 	= $period_beginDate;
		
		$shiftId = $this->Shift->field('id', array('number' => $updateData['number'], 'store_id' => $updateData['store_id']));
		
		$message .=  "Check first Shift ".print_r($shiftData, true). "\n";	
		
		if ($shiftId > 0) {
			$message .=  "Shift Found ".print_r($updateData, true). "\n";	
			//$updateData['id'] = $tId;
			//$shiftId = $shiftData['Shift']['id'];
		} else {
			$message .=  "Create New Shift in System".print_r($updateData, true). "\n";	
			$this->Shift->save($updateData);
			$shiftId = $this->Shift->id;
		}
		
		if ($shiftId) {
			//Update sales
			$productInfo = $xmlObj->totals->tierProductInfo->productInfo;
			foreach ($productInfo as $key => $product) {
				
				$fuelfuelProdBase =  (array)$product->fuelfuelProdBase;
				
				$sysid = (string)$fuelfuelProdBase['@attributes']['sysid'];
				$number = (string)$fuelfuelProdBase['@attributes']['number'];
				$fname = (string)$product->fuelfuelProdBase->name;
				$fuelUpdateData = array('name' => $fname, 'store_id' => $configData['Posrequester']['store_id'], 'fuel_product_id' => $sysid);
				//print_r($fuelUpdateData);exit;
				$message .=  "Search Fuel Type ".print_r($fuelUpdateData, true). "\n";	
				$fuel_type_id = $this->FuelType->field('FuelType.id', $fuelUpdateData);
				//var_dump($fuel_type_id);exit;
				if(!$fuel_type_id ) {
					$message .=  "Create New Type ".print_r($fname, true). "\n";	
					$this->FuelType->save($fuelUpdateData);
					$fuel_type_id = $this->FuelType->id;
				}  else {
					$message .=  "Matching Fuel Type Found ".print_r($fname, true). "\n";	
				}
							
				$updateData = array();
				$updateData['fuel_type_id'] = $fuel_type_id;
				$updateData['shift_id'] = $shiftId;
				$updateData['date'] = $period_beginDate;
				$updateData['store_id'] = $configData['Posrequester']['store_id'];
				
				$invId = $this->FuelInventory->field('FuelInventory.id', $updateData);
				if ($invId > 0) {
					$updateData['id'] = $invId;
				} 
				$updateData['sale_qty'] = (string)$product->fuelInfo->volume;
				$updateData['sale_amount'] = (string)$product->fuelInfo->amount;
				
				
				
				if ($period_beginDate  == date('yy-mm-dd')) {
					$updateData['status'] = 'Running';
				} else {
					$updateData['status'] = 'Close';
				}
				
				$message .=  "Prepare Data FoR Update ".print_r($updateData, true). "\n";	
				if (isset($updateData['id']) && $updateData['id'] > 0) {
					$this->FuelInventory->save($updateData);
					//$message .=  "Updated Previous Data ".print_r($updateData, true). "\n";	
				} else {
					$categoryData[] = $updateData;
				}
			}
		}
		
		if(!empty($categoryData)) {
			$this->FuelInventory->saveAll($categoryData);
			$message .=  "Updated New Data ".print_r($updateData, true). "\n";	
		}
		$message .=  "########### END updating Sales Data ############ ". "\n";
		
		return $message;
    }
	

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        $this->set('posrequester', $this->Posrequester->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
		
        if ($this->request->is('post')) {
			die('test');
			$this->request->data['Posrequester']['synupid'] = $this->request->data['Posrequester']['cmd'].'_'.$this->getSyncId();
            $this->Posrequester->create();
            if ($this->Posrequester->save($this->request->data)) {
                $this->Session->setFlash(__('The posrequester has been saved'));
                $this->redirect(array('action' => 'all'));
            } else {
                $this->Session->setFlash(__('The posrequester could not be saved. Please, try again.'));
            }
        }
		$this->loadModel('Station');
		$stations = $this->Station->find('list');
        $this->set(compact('stations'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Posrequester->save($this->request->data)) {
                $this->Session->setFlash(__('The posrequester has been saved'));
                $this->redirect(array('action' => 'all'));
            } else {
                $this->Session->setFlash(__('The posrequester could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Posrequester->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Posrequester->id = $id;
        if (!$this->Posrequester->exists()) {
            throw new NotFoundException(__('Invalid posrequester'));
        }
        if ($this->Posrequester->delete()) {
            $this->Session->setFlash(__('Posrequester deleted'));
            $this->redirect(array('action' => 'all'));
        }
        $this->Session->setFlash(__('Posrequester was not deleted'));
        $this->redirect(array('action' => 'all'));
    }
	
	function admin_setup($stationId)
	{
		$jobs = array('banner','bottle','category','currency','custid','dcrhead','dcrtrail','dep','employee','feechrg','fmop','fparm','fprice','fprod','fpump','fservlev','ftank','logo','menukey','mop','plu','plupro','prodcode','salescfg','softkey','syscfg','training','version','cattot','cshtot','deptot','ejour','ftotal','payroll','paytot','plutot','sumtot');
		
		foreach($jobs as $key => $job) {
			$data['Posrequester']['synupid'] = md5(rand());
			$data['Posrequester']['cmd'] 	 = $job;
			$data['Posrequester']['active']  = 1;
			$data['Posrequester']['station_id']  = $stationId;
			$this->Posrequester->create();
			$this->Posrequester->save($data);
		}
		
		$this->Session->setFlash(__('Posrequester added'));
        $this->redirect(array('action' => 'all'));
			
		exit;
	}
	
	
	function exportpluxml($storeId) {
		if($storeId) {
			$this->loadModel('Plus');
			$this->Plus->recursive = -1;
			$rdata = $this->Plus->find('all', array('conditions' => array('Plus.store_id' => $storeId)));
			
			$posReq = ' <?xml version="1.0" encoding="UTF-8"?>';
			$posReq = '<inventory store_id="'.$storeId.'">';
			
			foreach($rdata as $key => $data) {
				if(isset($data['Plus']) && !empty($data['Plus'])) {
					if ($data['Plus']['id']) {
						$posReq .= '<plu>';
						$posReq .= '<id>'.$data['Plus']['id'].'</id>';
						$posReq .= '<upc>'.$data['Plus']['upc'].'</upc>';
						$posReq .= '<physical_qty>'.$data['Plus']['physical_qty'].'</physical_qty>';
						$posReq .= '<description>'.$data['Plus']['description'].'</description>';
						$posReq .= '</plu>';
					}
				}
			}
			
			$posReq .= '</inventory>';
			header("Content-type: text/xml; charset=utf-8");
			echo $posReq;
		}
		die();
	}
	
	function deletInsertDataSet($message, $modelName, $rubyRow)
	{
		$this->loadModel($modelName);
		$_updateRubyRow = array();
		foreach($rubyRow as $_key => $_value) {
			$_value['ruby_header_id'] = $this->RubyHeader->id;
			$_updateRubyRow[$_key] = $_value;
		}
		
		//$message .=  print_r($_updateRubyRow, true) . "\n";
		try {
			$this->$modelName->deleteAll(array('ruby_header_id' => $this->RubyHeader->id), false);
			$this->$modelName->saveAll($_updateRubyRow);
			//print_r($_updateRubyRow);
		} catch(Exception $e) {
			echo $e->getMessage();
		}
		
		//return $message;
	}
	
	
	
}
