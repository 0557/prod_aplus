<?php

App::uses('AppController', 'Controller');

/**
 * FuelDepartments Controller
 *
 * @property FuelDepartment $FuelDepartment
 * @property PaginatorComponent $Paginator
 */
class FuelDepartmentsController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->set('departments', 'active');
        $this->FuelDepartment->recursive = 0;
        $this->set('fuelDepartments', $this->Paginator->paginate());
    }

    public function admin_view($id = null) {
        if (!$this->FuelDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        $options = array('conditions' => array('FuelDepartment.' . $this->FuelDepartment->primaryKey => $id));
        $this->set('fuelDepartment', $this->FuelDepartment->find('first', $options));
    }

    public function admin_add() {
        $this->set('departments', 'active');
        if ($this->request->is('post')) {
            $this->FuelDepartment->create();
            if ($this->FuelDepartment->save($this->request->data)) {
                $this->Flash->success(__('The fuel department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The fuel department could not be saved. Please, try again.'));
            }
        }
    }

    public function admin_edit($id = null) {
        $this->set('departments', 'active');
        if (!$this->FuelDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->FuelDepartment->save($this->request->data)) {
                $this->Flash->success(__('The fuel department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The fuel department could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('FuelDepartment.' . $this->FuelDepartment->primaryKey => $id));
            $this->request->data = $this->FuelDepartment->find('first', $options);
        }
    }

    public function admin_delete($id = null) {
        $this->FuelDepartment->id = $id;
        if (!$this->FuelDepartment->exists()) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->FuelDepartment->delete()) {
            $this->Flash->success(__('The fuel department has been deleted.'));
        } else {
            $this->Flash->error(__('The fuel department could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
