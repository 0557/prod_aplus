<?php

App::uses('AppController', 'Controller');

/**
 * FuelInvoices Controller
 *
 * @property FuelInvoice $FuelInvoice
 * @property PaginatorComponent $Paginator
 */
class FuelInvoicesController extends AppController {

    public $components = array('Paginator');


    public function admin_index() {
        $this->set('fuel_invoice', 'active');
        //$this->FuelInvoice->recursive = 0;
        $this->set('fuelInvoices', $this->Paginator->paginate());
        $fuel=$this->FuelInvoice->find('all');
       // echo'<pre>';print_r($fuel);exit;
    }

    public function admin_view($id = null) {
        $this->set('fuel_invoice', 'active');
        if (!$this->FuelInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel invoice'));
        }
        $options = array('conditions' => array('FuelInvoice.' . $this->FuelInvoice->primaryKey => $id));
        
        $product_id = $this->set('fuelInvoice', $this->FuelInvoice->find('first', $options));
        
         $product_id_arr = explode(',', $product_id['FuelInvoice']['product_id']);

   
        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('all', array('conditions' => array('WholesaleProduct.id' => $product_id_arr)));

        $this->set('product', $product);

    }

    public function admin_add() {
        $this->set('fuel_invoice', 'active');
        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
         $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
             
        $this->loadmodel('TaxeZone');
       
       
        if ($this->request->is('post')) {
			//pr($this->request->data);
            $this->FuelInvoice->create();
			
			
						
			if($this->request->data['FuelInvoice']['files']['name'] !="") {
				
				$file = $this->request->data['FuelInvoice']['files'];
				$ext = pathinfo($file['name'], PATHINFO_EXTENSION); 
				$file_name = time().'.'.$ext; 
				$file['name'] = $file_name;
			
				move_uploaded_file($this->request->data['FuelInvoice']['files']['tmp_name'], WWW_ROOT.'uploads/purchaseinvoice/' . $file['name']);
				$this->request->data['FuelInvoice']['files'] = $file['name'];
			}

			
			
			
            $this->request->data['FuelInvoice']['load_date'] = $this->Default->Dateconvert($this->data['FuelInvoice']['load_date']);
            $this->request->data['FuelInvoice']['receving_date'] = $this->Default->Dateconvert($this->data['FuelInvoice']['receving_date']);
			
			/*pr($this->request->data['FuelInvoice']['load_date']);
			pr($this->request->data['FuelInvoice']['receving_date']);
			die;*/
           //  $this->request->data['FuelInvoice']['bol'] = $this->Default->Getdate($this->data['FuelInvoice']['bol']);
           // $this->request->data['FuelInvoice']['po'] = $this->Default->Getdate($this->data['FuelInvoice']['po']);
              if (!empty($this->request->data['FuelProduct']['product_id']) || isset($this->request->data['FuelProduct']['product_id'])) {
                if ($this->FuelInvoice->save($this->request->data)) {
                    $getlast_id = $this->FuelInvoice->getLastInsertId();
                   
                    $tax_save = array();
                    $tax_save['TaxeZone']['fuel_invoice_id'] = $getlast_id;
                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                            //echo'<pre>';print_r($tax_save);
                            if (!$this->TaxeZone->save($tax_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    $product_save = array();
                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {
                        $product_save['FuelProduct']['type'] = 'Purchase Invoice';
                        $product_save['FuelProduct']['fuel_purchase_invoice_id'] = $getlast_id;
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key => $value) {
							if($value!=''){
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key];
                            // echo'<pre>';print_r($product_save);exit;
                            $this->FuelProduct->save($product_save);
						}
                        }
                    }
                    $this->Session->setFlash(__('The fuel invoice has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } 
            }
            else {
                    $error = 'Select Atleast One Product';
                    $this->set('error', $error);
                    $this->Session->setFlash(__($error));
                }
        }
        
        $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
        $wholesale_supplier = $this->Customer->find('list', array('conditions' => array('retail_type' => 'wholesale', 'customer_type' => 'Vendor')));
        $this->set(compact('wholesale_supplier', 'product'));
    }




    public function admin_edit($id = null) {
		
		
        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('fuel_invoice', 'active');
        if (!$this->FuelInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel invoice'));
        }

        if ($this->request->is(array('post', 'put'))) {
		
            $this->request->data['FuelInvoice']['load_date'] = $this->Default->Dateconvert($this->data['FuelInvoice']['load_date']);
            $this->request->data['FuelInvoice']['receving_date'] = $this->Default->Dateconvert($this->data['FuelInvoice']['receving_date']);
           // $this->request->data['FuelInvoice']['bol'] = $this->Default->Getdate($this->data['FuelInvoice']['bol']);
          //  $this->request->data['FuelInvoice']['po'] = $this->Default->Getdate($this->data['FuelInvoice']['po']);
		  
		  
		  	if($this->request->data['FuelInvoice']['files']['name'] !="") {
				
				$file = $this->request->data['FuelInvoice']['files'];
				$ext = pathinfo($file['name'], PATHINFO_EXTENSION); 
				$file_name = time().'.'.$ext; 
				$file['name'] = $file_name;
			
				move_uploaded_file($this->request->data['FuelInvoice']['files']['tmp_name'], WWW_ROOT.'uploads/purchaseinvoice/' . $file['name']);
				$this->request->data['FuelInvoice']['files'] = $file['name'];
				
				$unlinkpath = WWW_ROOT.'uploads/purchaseinvoice/'; 	
				//echo 	$unlinkpath.$this->request->data['FuelInvoice']['pre_img_file'] ;die;
				 
                if($this->request->data['FuelInvoice']['pre_img_file'] != '' && file_exists($unlinkpath.$this->request->data['FuelInvoice']['pre_img_file']) ){
				unlink($unlinkpath.$this->request->data['FuelInvoice']['pre_img_file']);
				}
					
			}else{
		  
		  	 $this->request->data['FuelInvoice']['files'] = $this->request->data['FuelInvoice']['pre_img_file'];
			}
		  
		  
           if (!empty($this->request->data['FuelProduct']['product_id'])) {
                if ($this->FuelInvoice->save($this->request->data)) {
                  
                   $this->TaxeZone->deleteAll(array('TaxeZone.fuel_invoice_id' => $id));
                    $getlast_id = $this->FuelInvoice->getLastInsertId();
                    $tax_save = array();
                    $tax_save['TaxeZone']['fuel_invoice_id'] = $id;
                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                            //echo'<pre>';print_r($tax_save);
                            if (!$this->TaxeZone->save($tax_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    $product_save = array();
                    $product_save['FuelProduct']['fuel_purchase_invoice_id'] = $id;
                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {
                         $this->FuelProduct->deleteAll(array('FuelProduct.fuel_purchase_invoice_id' => $id), false);
                        $product_save['FuelProduct']['type'] = 'Purchase Invoice';
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key => $value) {
							if($value!=''){
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key];

                            $this->FuelProduct->save($product_save);
						  }
                        }
                    }
                        $this->Session->setFlash(__('The fuel invoice has been saved.'));
                         return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The fuel invoice could not be saved. Please, try again.'));
                }
            } else {
                     $this->Session->setFlash(__('Select Atleast One Product'));
            }
        } else {
            $options = array('conditions' => array('FuelInvoice.' . $this->FuelInvoice->primaryKey => $id));
            $this->request->data = $this->FuelInvoice->find('first', $options);
        }

        $supplier_id = $this->request->data['FuelInvoice']['supplier_id'];

      //  $product = $this->WholesaleProduct->find('list');
        $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
        $wholesale_supplier = $this->Customer->find('list', array('conditions' => array('retail_type' => 'wholesale', 'customer_type' => 'Vendor')));
        $this->set(compact('wholesale_supplier', 'product', 'supplier_id'));
    }

    public function admin_delete($id = null) {
        $this->FuelInvoice->id = $id;
        if (!$this->FuelInvoice->exists()) {
            throw new NotFoundException(__('Invalid fuel invoice'));
        }
		
		$F_invoice = $this->FuelInvoice->find('all', array(
		'fields' => array( 'FuelInvoice.files'),
        'conditions' => array('FuelInvoice.id =' => $id),
    ));
	//pr($F_invoice);
	//echo $F_invoice[0]['FuelInvoice']['files'];
        //$this->request->allowMethod('post', 'delete');
		$unlinkpath = WWW_ROOT.'uploads/purchaseinvoice/'; 
		
        if($F_invoice[0]['FuelInvoice']['files'] != '' && file_exists($unlinkpath.$F_invoice[0]['FuelInvoice']['files']) )
		{
			unlink($unlinkpath.$F_invoice[0]['FuelInvoice']['files']);
		}
			if ($this->FuelInvoice->delete()) {
				$this->Session->setFlash(__('The fuel invoice has been deleted.'));
			} else {
	
				$this->Session->setFlash(__('The fuel invoice could not be deleted. Please, try again.'));
			}
		
        return $this->redirect(array('action' => 'index'));
    }
	
	public function admin_deleteproduct($id = null) {
        if(empty($id)){
            return;
         }
			 $this->loadmodel('FuelProduct');
             $F_invoice_id = $this->FuelProduct->find('list', array(
		'fields' => array( 'id','FuelProduct.fuel_purchase_invoice_id'),
        'conditions' => array('FuelProduct.id =' => $id),
    ));
	
			//echo $F_invoice_id[$id];die;
            if( $this->FuelProduct->delete($id)){
			$this->Session->setFlash(__('The product details has been deleted.'));
			}else{
			$this->Session->setFlash(__('The product details could not be deleted. Please, try again.'));
				}
			$this->redirect(array('action' => 'edit', $F_invoice_id[$id]));
    }	

    public function products($id = null) {
        if(empty($id)){
            return false;
         }		    
             $this->loadmodel('WholesaleProduct');
             $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
             $product = $this->WholesaleProduct->find('first', array('conditions' => array('WholesaleProduct.id' => $id)));
			  //pr($product);		
              $this->set(compact('product', 'id'));
    }

}
