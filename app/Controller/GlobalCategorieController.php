<?php
App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class GlobalCategorieController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index($id=null,$name=null) {
		  $this->loadModel('GlobalCategorie');
           $this->Setredirect();
		$list  = $this->GlobalCategorie->find('all');
	        $this->set('glist', $list);
	        $this->set('id', $id);
	        $this->set('name', $name);
	 if ($this->request->is('post')) {
         //'RGroceryItem.r_grocery_department_id'=>$this->request->data['RGroceryItem']['department'],
			$findid = $this->GlobalCategorie->find('first',array('conditions'=>array('GlobalCategorie.g_category'=>$this->request->data['GlobalCategorie']['g_category'])));
            
            if(empty($findid)){
						if ($this->GlobalCategorie->save($this->request->data)) {
							  $this->Session->setFlash(__('Data has been saved.'));
							return $this->redirect(array('action' => 'index'));
					}
			}else{
				  $this->Session->setFlash(__('Category already exist.'));
				  
							return $this->redirect(array('action' => 'index'));
			}
	 }
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
						if ($this->GlobalCategorie->delete($id)) {
							  $this->Session->setFlash(__('Data has been deleted.'));
							return $this->redirect(array('action' => 'index'));
			}else{
				  $this->Session->setFlash(__('error occured.'));
				  
							return $this->redirect(array('action' => 'index'));
			}
	
	}
public function admin_exportglobalcategory($name){
	$this->set('categoryname',$name);
	 if (isset($this->request->data['GlobalCategoryImport']['files']['name']) && !empty($this->request->data['GlobalCategoryImport']['files']['name'])) {
		 if($this->uploadFile('files')){
			 //echo $this->request->data['GlobalCategoryImport']['category'];
			 //print_r($this->request->data);exit;
		                $array11 = explode(".", $this->request->data['GlobalCategoryImport']['files']);
                        $file_ext = end($array11);
		                if ($file_ext == 'xlsx' || $file_ext == 'xlx') {
                            $import_File = $this->__ImportExcel($this->request->data['GlobalCategoryImport']['files'],$this->request->data['GlobalCategoryImport']['category']);
                        } else if ($file_ext == 'csv') {
                            $import_File = $this->__ImportCsv($this->request->data['GlobalCategoryImport']['files'],$this->request->data['GlobalCategoryImport']['categroy']);
                        }
                        if (!$import_File) {
                            throw new Exception();
                        }else{
								$this->Session->setFlash('File data imported successfully');
								return $this->redirect(array('action' => 'index'));
								
						}
                    }
	 }
}

    protected function __ImportExcel($filename,$category) {

        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel/PHPExcel' . DS . 'IOFactory.php'));
        $inputFileName = $file;

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        //  echo $highestColumn; die;
        //  Loop through each row of the worksheet in turn
        $i = 0;
        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
             // pr($rowData); die;
            if ($i != 0) {
				
				$this->__saveGroceryItem($rowData,$category);
				
              
            }
            $i++;
        }
      
        // die;
        return 1;
    }

    protected function __ImportCsv($filename,$category) {
        $this->autoRender = false;
        $this->loadModel('RGroceryDepartment');
        $this->loadModel('RGroceryInvoice');
        $this->loadModel('RGroceryInvoiceProduct');
        $this->loadModel('RGroceryItem');
      
        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        $handle = fopen($file, "r");
        $i = 0;
        $rowData = array();
        do {
            if (isset($rowData[0])) {
                if ($i != 0) {
					$senddata[0] = $rowData;
					$this->__saveGroceryItem($senddata,$category);
                }
                $i++;
            }
        } while ($rowData = fgetcsv($handle, 1000, ",", "'"));


        return 1;
    }
	
	 protected function __saveGroceryItem($rowData, $category) {
        $this->autoRender = false;
         // pr($rowData); die;
 $this->loadModel('Globalupc');
        $grocery_item_Save = array();
      
	          $rdeptid  = $rowData['0']['0'];
		$string = preg_replace('/\s+/', '', $rowData['0']['0']);
	//	echo $string;die;
		
			  
        $grocery_item_Save['Globalupc']['plu_no'] = str_replace('"','',$string);
        $grocery_item_Save['Globalupc']['description'] = $rowData['0']['1'];
        $grocery_item_Save['Globalupc']['cost'] = $rowData['0']['2'];
        $grocery_item_Save['Globalupc']['category'] = $category;
        $this->Globalupc->create();
        if($this->Globalupc->save($grocery_item_Save)){
		}
        return 1;
    }
	
function uploadFile($fieldName = 'files')
	{
			
		//$upload_dir = WWW_ROOT.str_replace("/", DS, 'stores');
		$uploadPath = WWW_ROOT.'files';
	
		$imageTypes = array("xlx", "xlsx","csv");
		$array11 = explode(".", $this->request->data['GlobalCategoryImport'][$fieldName]['name']);
                        $file_ext = end($array11);
						
		if (in_array($file_ext,$imageTypes)){
			$fike = $this->data['GlobalCategoryImport'][$fieldName];
			$_file = date('His') .'_'. $fike['name'];
			 $full_image_path = $uploadPath . '/' .$_file;
			
			if (move_uploaded_file($fike['tmp_name'], $full_image_path)) {
				$this->request->data['GlobalCategoryImport'][$fieldName] = $_file;
			
				return true;
			} else {
				$this->Session->setFlash('Error Occured');
				return false;
			}
		} else {
				$this->Session->setFlash('Invalid type. Please try again with fike type '. implode(', ', $imageTypes));
				return false;
			}
		return false;
	}
}
