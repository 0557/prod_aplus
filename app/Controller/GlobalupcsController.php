<?php

App::uses('AppController', 'Controller');

/**
 * Globalupcss Controller
 *
 * @property Globalupcs $Globalupcs
 * @property PaginatorComponent $Paginator
 */
class GlobalupcsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
  public function admin_index(){
	    $this->Setredirect();
		 $this->loadModel('Globalupcs');
    
	 if (isset($this->request->data['Globalupcs']['files']['name']) && !empty($this->request->data['Globalupcs']['files']['name'])) {
		// print_r($this->request->data['Globalupcs']['files']['tmp_name']);exit;
		 if($this->uploadFile('files')){
		// print_r($this->request->data['Globalupcs']['files']);
		 //echo $this->request->data['Globalupcs']['files']['name'];
                        $array11 = explode(".", $this->request->data['Globalupcs']['files']);
                        $file_ext = end($array11);
				//		print_r($array11);
                       //  echo $file_ext;						   die;
                        if ($file_ext == 'xlsx' || $file_ext == 'xlx') {
                            $import_File = $this->__ImportExcel($this->request->data['Globalupcs']['files']);
                        } else if ($file_ext == 'csv') {
                            $import_File = $this->__ImportCsv($this->request->data['Globalupcs']['files']);
                        }
                        if (!$import_File) {
                            throw new Exception();
                        }else{
								$this->Session->setFlash('File data imported successfully');
								return $this->redirect(array('action' => 'index'));
								
						}
                    }
	 }
	 
        $this->set('Globalupcslist', $this->Globalupcs->find('all'));
		
}
    

    protected function __ImportExcel($filename) {

        $this->loadModel('RGroceryDepartment');
        $this->loadModel('Globalupcs');
    
        
        //$corporation_Id = $_SESSION['corporation_id'];
        $store_Id = $this->Session->read('stores_id');
      
        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel/PHPExcel' . DS . 'IOFactory.php'));
        $inputFileName = $file;

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        //  echo $highestColumn; die;
        //  Loop through each row of the worksheet in turn
        $i = 0;
        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
             // pr($rowData); die;
            if ($i != 0) {
				
				$this->__saveGroceryItem($rowData,$store_Id);
				
              
            }
            $i++;
        }
      
        // die;
        return 1;
    }

    protected function __ImportCsv($filename) {
        $this->autoRender = false;
        $this->loadModel('Globalupcs');
       // $corporation_Id = $_SESSION['corporation_id'];
        $store_Id = $this->Session->read('stores_id');
      
        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        $handle = fopen($file, "r");
        $i = 0;
        $rowData = array();
        do {
            if (isset($rowData[0])) {
                if ($i != 0) {
					$senddata[0] = $rowData;
					$this->__saveGroceryItem($senddata,$store_Id);
                  
                }
                $i++;
            }
        } while ($rowData = fgetcsv($handle, 1000, ",", "'"));


        return 1;
    }
	
	 protected function __saveGroceryItem($rowData, $store_Id) {
        $this->autoRender = false;
//           pr($rowData); die;
        $grocery_item_Save = array();
      
        $grocery_item_Save['Globalupcs']['plu_no'] = str_replace('"','',$rowData['0']['0']);
        $grocery_item_Save['Globalupcs']['description'] = $rowData['0']['1'];
        $grocery_item_Save['Globalupcs']['category'] = $rowData['0']['2'];
        $grocery_item_Save['Globalupcs']['cost'] = str_replace(',','',$rowData['0']['3']);
      
        $this->Globalupcs->create();
	//	 pr($grocery_item_Save); die;
        if($this->Globalupcs->save($grocery_item_Save)){
	//	echo 'asd'; exit;	
		}
        return 1;
    }
	
function uploadFile($fieldName = 'files')
	{
			
		//$upload_dir = WWW_ROOT.str_replace("/", DS, 'stores');
		$uploadPath = WWW_ROOT.'files';
	
		$imageTypes = array("xlx", "xlsx","csv");
		$array11 = explode(".", $this->request->data['Globalupcs'][$fieldName]['name']);
                        $file_ext = end($array11);
						
		if (in_array($file_ext,$imageTypes)){
			$fike = $this->data['Globalupcs'][$fieldName];
			$_file = date('His') .'_'. $fike['name'];
			 $full_image_path = $uploadPath . '/' .$_file;
			
			if (move_uploaded_file($fike['tmp_name'], $full_image_path)) {
				$this->request->data['Globalupcs'][$fieldName] = $_file;
			
				return true;
			} else {
				$this->Session->setFlash('Error Occured');
				return false;
			}
		} else {
				$this->Session->setFlash('Invalid type. Please try again with fike type '. implode(', ', $imageTypes));
				return false;
			}
		return false;
	}
	
}
