<?php
error_reporting(0);
ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class GroceryController extends AppController {

    //   public $components = array('Paginator', 'Mpdf.Mpdf');
    public $components = array('Paginator');

    public function admin_index() {

	if(!$_SESSION['store_id']) $this->Session->setFlash(__('Please select  store.')); 
	if($_SESSION['store_id'] !="") $this->set('display', 'yes');
	}
	public function admin_inventory(){
		$this->set('inventory', 'active');
	}

	public function admin_track_non_scanned_item(){
		
		$this->set('track_non_scanned_item', 'active');
	}
	public function admin_reports(){
		
		$this->set('reports', 'active');
	}
	public function admin_purchase_packs($id=null){
		$this->Setredirect();
		$this->loadModel('Customer');
		$this->loadModel('PurchasePacks');
	
	if($this->PurchasePacks->delete($id)){
		$this->Session->setFlash(__('Data deleted successfully.'));
		return $this->redirect(array('action' => 'purchase_packs'));
		}
				
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
		$list = $this->PurchasePacks->find('all',array('conditions'=>array('PurchasePacks.company_id' => $this->Session->read('Auth.User.company_id'),'PurchasePacks.store_id' => $this->Session->read('stores_id'))));
		
		$this->set('list',$list);
		
		if($this->request->is('post')){
			$conditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'PurchasePacks.store_id' => $this->Session->read('stores_id'),
		/*	'PurchasePacks.vendor'=>$this->request->data['PurchasePacks']['vendor'],
			'PurchasePacks.Margin'=>$this->request->data['PurchasePacks']['Margin'],*/
			'PurchasePacks.Item_Scan_Code'=>$this->request->data['PurchasePacks']['Item_Scan_Code'],
			'PurchasePacks.Item_Name'=>$this->request->data['PurchasePacks']['Item_Name'],
			'PurchasePacks.Item#'=>$this->request->data['PurchasePacks']['Item#'],
			/*'PurchasePacks.Case_Cost >='=>$this->request->data['PurchasePacks']['min_cost'],
			'PurchasePacks.Case_Cost <='=>$this->request->data['PurchasePacks']['max_cost']*/
			);
			$list = $this->PurchasePacks->find('all',array('conditions'=>array_filter($conditions)));
			$this->set('list',$list);
			}
			
	}
	/*		public function admin_new_pack(){
				$this->loadModel('PurchasePacks');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
			if($this->request->is('post')){
		$this->request->data['PurchasePacks']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['PurchasePacks']['store_id'] = $this->Session->read('stores_id');
			if($this->request->data['PurchasePacks']['purchase_date']){
				$newdate = explode('-',$this->request->data['PurchasePacks']['purchase_date']);
			$pdate = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			$this->request->data['PurchasePacks']['purchase_date'] = $pdate;
			}
			$this->request->data['PurchasePacks']['created_at'] = date('Y-m-d');
			if($this->PurchasePacks->save($this->data)){
				
			$finditem = $this->RGroceryItem->find('first',array('conditions'=>array('plu_no'=>$this->request->data['PurchasePacks']['Item_Scan_Code'],'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));
if(isset($finditem['RGroceryItem']['id'])){
            $this->request->data['RGroceryItem']['id'] = $finditem['RGroceryItem']['id'];


            $this->request->data['RGroceryItem']['Item#'] = $this->request->data['PurchasePacks']['Item#'];

            $this->request->data['RGroceryItem']['purchase'] = $finditem['RGroceryItem']['purchase'] + $this->request->data['PurchasePacks']['purchase'];

			//print_r($this->data);exit;

			$this->RGroceryItem->save($this->data);
}

if(isset($this->request->data['PurchasePacks']['url'])){
						$this->Session->setFlash(__('Submited new data successfully.'));
							 return $this->redirect(array('action' => 'r_grocery_invoices'));
exit;	
}
						$this->Session->setFlash(__('Submited new data successfully.'));
							 return $this->redirect(array('action' => 'purchase_packs'));
						}
			}
			}
			
	public function admin_edit_purchasepack($id){
				$this->loadModel('PurchasePacks');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
				$this->loadModel('RGroceryInvoice');
				
		$this->loadModel('RGroceryItem');	  
		$item= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		//pr($item);die;
		$this->set('item', $item);					
				
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
		
		$conditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'PurchasePacks.store_id' => $this->Session->read('stores_id'),
			'PurchasePacks.id'=>$id
			);
			$list = $this->PurchasePacks->find('first',array('conditions'=>$conditions));
			$this->set('list',$list);
		
			if($this->request->is('post')){
			$this->request->data['PurchasePacks']['updated_at'] =date('Y-m-d');
			if($this->request->data['PurchasePacks']['purchase_date']){
				$newdate = explode('-',$this->request->data['PurchasePacks']['purchase_date']);
			$pdate = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			$this->request->data['PurchasePacks']['purchase_date'] = $pdate;
			}
			if($this->PurchasePacks->save($this->data)){
				
			$purchase_conditions = array('PurchasePacks.id' =>$id
			);			
			
			$this->PurchasePacks->virtualFields['p_gross_amount'] = 'SUM(PurchasePacks.gross_amount)';
		    $PurchasePacks=$this->PurchasePacks->find('first',array('conditions' => $purchase_conditions));	
			
			//pr($PurchasePacks);die;	
			$p_gross_amount=0;
			if(isset($PurchasePacks)){		
		    $p_gross_amount=$PurchasePacks['PurchasePacks']['p_gross_amount'];
			}			
				
					
		    $invoice_conditions = array('RGroceryInvoice.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'RGroceryInvoice.store_id' => $this->Session->read('stores_id'),
			'RGroceryInvoice.vendor_id'=>$PurchasePacks['PurchasePacks']['vendor'],
			'RGroceryInvoice.invoice'=>$PurchasePacks['PurchasePacks']['invoice']
			);		
			
			
			$this->RGroceryInvoice->virtualFields['i_gross_amount'] = 'SUM(RGroceryInvoice.gross_amount)';
		    $RGroceryInvoice = $this->RGroceryInvoice->find('first',array('conditions' => $invoice_conditions));			
				
			$i_gross_amount=0;
			if(isset($RGroceryInvoice)){		
		    $i_gross_amount=$RGroceryInvoice['RGroceryInvoice']['i_gross_amount'];
			}				
			
			
			
			$status=0;
									
			if($i_gross_amount==$p_gross_amount)
			{
			$status=1;
			}
			
			if($i_gross_amount>0 && $i_gross_amount!=$p_gross_amount && $i_gross_amount!=0)
			{
			$status=2;
			}
			
			if($p_gross_amount==0)
			{
			$status=3;
			}
			
			if(isset($RGroceryInvoice)){
		    $this->request->data['RGroceryInvoice']['retail_status'] =$status;
			$this->RGroceryInvoice->id =$RGroceryInvoice['RGroceryInvoice']['id'];				
			$this->RGroceryInvoice->save($this->data);
			}
				
				$finditem = $this->RGroceryItem->find('first',array('conditions'=>array('plu_no'=>$this->request->data['PurchasePacks']['Item_Scan_Code'])));
				if(isset($finditem['RGroceryItem']['id'])){					
					
					$this->request->data['RGroceryItem']['id'] = $finditem['RGroceryItem']['id'];
					$this->request->data['RGroceryItem']['Item#'] = $this->request->data['PurchasePacks']['Item#'];
					$this->request->data['RGroceryItem']['purchase'] = $this->request->data['PurchasePacks']['purchase'];					
					//print_r($this->data);exit;				
					$this->RGroceryItem->save($this->data);
				}
							 $this->Session->setFlash(__('Submited new data updated successfully.'));
							 return $this->redirect(array('action' => 'purchase_packs'));
						}
			}
			
			}*/
			
	public function admin_new_pack(){
				$this->loadModel('PurchasePacks');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
	
			if($this->request->is('post')){
			$count = $this->Customer->query("SELECT *  FROM purchase_packs WHERE Item_Scan_Code=".$this->request->data['PurchasePacks']['Item_Scan_Code']." AND store_id= ".$this->Session->read('stores_id')."");
			$num=count($count);
			if($num==0){
		
				$this->request->data['PurchasePacks']['company_id'] =  $this->Session->read('Auth.User.company_id');
				$this->request->data['PurchasePacks']['store_id'] = $this->Session->read('stores_id');
				$this->request->data['PurchasePacks']['created_at'] = date('Y-m-d');
				if($this->PurchasePacks->save($this->data)){
					$this->Session->setFlash(__('Submited new data successfully.'));
					return $this->redirect(array('action' => 'purchase_packs'));
				}
			}else{
					 	//pr($count) ; die;
						
				$newid=$count[0]['purchase_packs']['id'];
				$this->Session->setFlash(__('This PLU already exists.'));
					return $this->redirect(array('action' => 'edit_purchasepack',$newid));
			
			}
		}
		$item= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		//pr($item);die;
		$this->set('item', $item);
			}
			
	public function admin_edit_purchasepack($id){
				$this->loadModel('PurchasePacks');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
				$this->loadModel('RGroceryInvoice');
				
		$this->loadModel('RGroceryItem');	  
		$item= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		//pr($item);die;
		$this->set('item', $item);					
				
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
		
		$conditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'PurchasePacks.store_id' => $this->Session->read('stores_id'),
			'PurchasePacks.id'=>$id
			);
			$list = $this->PurchasePacks->find('first',array('conditions'=>$conditions));
			$this->set('list',$list);
		
			if($this->request->is('post')){
					/*$count = $this->Customer->query("SELECT *  FROM purchase_packs WHERE Item_Scan_Code=".$this->request->data['PurchasePacks']['Item_Scan_Code']." AND store_id= ".$this->Session->read('stores_id')." AND id!=".$id."");
			$num=count($count);
				if($num==0){*/
				    unset($this->request->data['PurchasePacks']['Item_Scan_Code']);
					$this->request->data['PurchasePacks']['id']=$id;
					$this->request->data['PurchasePacks']['updated_at'] =date('Y-m-d');
					if($this->PurchasePacks->save($this->data)){						
						$this->Session->setFlash(__('Data updated successfully.'), 'default', array('class' => 'success'));
						return $this->redirect(array('action' =>'edit_purchasepack',$id));
					}
					else{					
			     $this->Session->setFlash(__('Some problem occurred.Please try again.'), 'default', array('class' => 'error'));
					return $this->redirect(array('action' =>'edit_purchasepack',$id));
				    }
				/*}else{
					$newid=$count[0]['purchase_packs']['id'];
					$this->Session->setFlash(__('This PLU already exists.'));
					return $this->redirect(array('action' => 'edit_purchasepack',$newid));
				}*/
			}
			
	}
			
public function admin_importpacksfile(){

	 if (isset($this->request->data['PurchasePacks']['files']['name']) && !empty($this->request->data['PurchasePacks']['files']['name'])) {
		// print_r($this->request->data['RGroceryItem']['files']['tmp_name']);exit;
		 if($this->uploadFile('files')){
		// print_r($this->request->data['RGroceryItem']['files']);
		 //echo $this->request->data['RGroceryItem']['files']['name'];
                        $array11 = explode(".", $this->request->data['PurchasePacks']['files']);
                        $file_ext = end($array11);
				//		print_r($array11);
                       //  echo $file_ext;						   die;
                        if ($file_ext == 'xlsx' || $file_ext == 'xlx') {
                            $import_File = $this->__ImportExcel($this->request->data['PurchasePacks']['files']);
                        } else if ($file_ext == 'csv') {
                            $import_File = $this->__ImportCsv($this->request->data['PurchasePacks']['files']);
                        }
                        if (!$import_File) {
                            throw new Exception();
                        }else{
								$this->Session->setFlash('File data imported successfully');
								return $this->redirect(array('action' => 'purchase_packs'));
								
						}
                    }
	 }
}

    protected function __ImportExcel($filename) {

     
        $this->loadModel('PurchasePacks');
    
        
        //$corporation_Id = $_SESSION['corporation_id'];
        $store_Id = $this->Session->read('stores_id');
      
        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel/PHPExcel' . DS . 'IOFactory.php'));
        $inputFileName = $file;

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        //  echo $highestColumn; die;
        //  Loop through each row of the worksheet in turn
        $i = 0;
        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
             // pr($rowData); die;
            if ($i != 0) {
				
				$this->__saveGroceryItem($rowData,$store_Id);
				
              
            }
            $i++;
        }
      
        // die;
        return 1;
    }

    protected function __ImportCsv($filename) {
        $this->autoRender = false;
      
        $this->loadModel('PurchasePacks');
       
       // $corporation_Id = $_SESSION['corporation_id'];
        $store_Id = $this->Session->read('stores_id');
      
        $file = WWW_ROOT . 'files/' . $filename;
        unset($_SESSION['file_upload']);
        $handle = fopen($file, "r");
        $i = 0;
        $rowData = array();
        do {
            if (isset($rowData[0])) {
                if ($i != 0) {
					$senddata[0] = $rowData;
					$this->__saveGroceryItem($senddata,$store_Id);
                  
                }
                $i++;
            }
        } while ($rowData = fgetcsv($handle, 1000, ",", "'"));


        return 1;
    }
	
	 protected function __saveGroceryItem($rowData, $store_Id) {
        $this->autoRender = false;
//           pr($rowData); die;
        $grocery_item_Save = array();
      $this->loadModel('RGroceryItem');
        $grocery_item_Save['PurchasePacks']['Item_Scan_Code'] = str_replace("'","",$rowData['0']['0']);
        $grocery_item_Save['PurchasePacks']['Item_Name'] = $rowData['0']['1'];
        $grocery_item_Save['PurchasePacks']['Units_Per_Case'] = $rowData['0']['2'];
        $grocery_item_Save['PurchasePacks']['Case_Cost'] = $rowData['0']['3'];
        $grocery_item_Save['PurchasePacks']['Discount'] = $rowData['0']['4'];
        $grocery_item_Save['PurchasePacks']['Rebate'] = $rowData['0']['5'];
        $grocery_item_Save['PurchasePacks']['Net_Cost'] = $rowData['0']['6'];
        $grocery_item_Save['PurchasePacks']['Case_Retail'] = $rowData['0']['7'];
        $grocery_item_Save['PurchasePacks']['Margin'] = $rowData['0']['8'];
        $grocery_item_Save['PurchasePacks']['vendor'] = $rowData['0']['9'];
        $grocery_item_Save['PurchasePacks']['Item#'] = $rowData['0']['11'];
		
		$newdate = explode('-',str_replace("'","",$rowData['0']['10']));
		 $pdate = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
		//die;
        $grocery_item_Save['PurchasePacks']['purchase_date'] = $pdate;
        $grocery_item_Save['PurchasePacks']['Unit_per_cost'] = $rowData['0']['3']/$rowData['0']['2'];
        $grocery_item_Save['PurchasePacks']['store_id'] = $store_Id;
        $grocery_item_Save['PurchasePacks']['company_id'] = $this->Session->read('Auth.User.company_id');
        $this->PurchasePacks->create();
        if($this->PurchasePacks->save($grocery_item_Save)){
			
			$finditem = $this->RGroceryItem->find('first',array('conditions'=>array('plu_no'=> str_replace("'","",$rowData['0']['0']),'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));
if(isset($finditem['RGroceryItem']['id'])){
            $this->request->data['RGroceryItem']['id'] = $finditem['RGroceryItem']['id'];


            $this->request->data['RGroceryItem']['Item#'] = $rowData['0']['11'];

            $this->request->data['RGroceryItem']['purchase'] = $finditem['RGroceryItem']['purchase'] + $rowData['0']['12'];

			//print_r($this->data);exit;

			$this->RGroceryItem->save($this->data);
}
			
		}
        return 1;
    }
	function uploadFile($fieldName = 'files')
	{
			
		//$upload_dir = WWW_ROOT.str_replace("/", DS, 'stores');
		$uploadPath = WWW_ROOT.'files';
	
		$imageTypes = array("xlx", "xlsx","csv");
		$array11 = explode(".", $this->request->data['PurchasePacks'][$fieldName]['name']);
                        $file_ext = end($array11);
						
		if (in_array($file_ext,$imageTypes)){
			$fike = $this->data['PurchasePacks'][$fieldName];
			$_file = date('His') .'_'. $fike['name'];
			 $full_image_path = $uploadPath . '/' .$_file;
			
			if (move_uploaded_file($fike['tmp_name'], $full_image_path)) {
				$this->request->data['PurchasePacks'][$fieldName] = $_file;
			
				return true;
			} else {
				$this->Session->setFlash('Error Occured');
				return false;
			}
		} else {
				$this->Session->setFlash('Invalid type. Please try again with fike type '. implode(', ', $imageTypes));
				return false;
			}
		return false;
	}
	
	/*
public function admin_getinventory(){
		$this->loadModel('RGroceryItem');
	 $this->autoRender = false; 

		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.plu_no'=>$this->request->data['pul'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));
		if(!empty($first)){
			echo ;
			}else{
				exit;
		}
		
	}	
	public function admin_getinventoryid(){
		$this->loadModel('RGroceryItem');
	 $this->autoRender = false; 

		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.plu_no'=>$this->request->data['pul'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));
		if(!empty($first)){
			echo $first['RGroceryItem']['id'];
			}else{
				exit;
		}
		
	}
	public function admin_getgitem(){
		$this->loadModel('PurchasePack');
	 $this->autoRender = false; 
echo $this->Session->read('stores_id');
		$first = $this->PurchasePack->find('first',array('conditions' => array('PurchasePack.Item_Scan_Code'=>$this->request->data['pul'],'PurchasePack.store_id'=>$this->Session->read('stores_id')),  "order" => "PurchasePack.id DESC"));
		if(!empty($first)){
			echo $first['PurchasePack']['Item#'];
			}else{
				exit;
		}
		
	}
	
	*/
	
  public function admin_getinvoice(){
  $this->loadModel('RGroceryInvoice');
  $this->autoRender = false; 
//echo $this->request->data['item'];
		$first = $this->RGroceryInvoice->find('all',array('conditions' => array('RGroceryInvoice.vendor_id'=>$this->request->data['vendor'],'RGroceryInvoice.store_id'=>$this->Session->read('stores_id'),'RGroceryInvoice.company_id' => $this->Session->read('Auth.User.company_id')),  "order" => "RGroceryInvoice.id DESC"));
		if(!empty($first)){
			$data=array();
			foreach($first as $first){
				array_push($data,$first['RGroceryInvoice']['invoice']);
				}
				echo json_encode($data);
			}else{
				exit;
			}
		
		}
		
		public function admin_purchase_report(){
			$this->Setredirect();
			$this->loadModel('Customer');
			$this->loadModel('PurchasePacks');
			$this->loadModel('RGroceryInvoice');
			$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));	
			    $this->set('vendor',$vendor);			
			    $vendor_id='';
			    $invoice_id='';
				$invoice_array=array();
				$pconditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
					'PurchasePacks.store_id' => $this->Session->read('stores_id')
					);
				$iconditions = array('RGroceryInvoice.company_id' =>  $this->Session->read('Auth.User.company_id'),
					'RGroceryInvoice.store_id' => $this->Session->read('stores_id')
					);
				if($this->request->is('post')){
					
					//pr($this->request->data);die;
					if($this->request->data['PurchasePacks']['vendor']!= "" || $this->request->data['PurchasePacks']['invoice']!= "") {

						$this->Session->write('vendor',$this->request->data['PurchasePacks']['vendor']);
						$this->Session->write('invoice', $this->request->data['PurchasePacks']['invoice']);
						$vendor_id=$this->Session->read('vendor');
						$invoice_id=$this->Session->read('invoice');
					
				} else {
						$this->Session->delete('vendor');
						$this->Session->delete('invoice');						
				}
				
				}
					
				    if($vendor_id!=''){
					$pconditions = array(
					'PurchasePacks.vendor'=>$vendor_id				
					);	
					
					$iconditions = array(
					'RGroceryInvoice.vendor_id'=>$vendor_id				
					);				
					
					$first = $this->RGroceryInvoice->find('all',array('conditions' => array('RGroceryInvoice.vendor_id'=>$vendor_id,'RGroceryInvoice.store_id'=>$this->Session->read('stores_id'),'RGroceryInvoice.company_id' => $this->Session->read('Auth.User.company_id')),  "order" => "RGroceryInvoice.id DESC"));				
					//pr($first);die;				
					foreach($first as $invoice_row){			
					$invoice_array[$invoice_row['RGroceryInvoice']['invoice']]=$invoice_row['RGroceryInvoice']['invoice'];
					}
					//pr($invoice_array);die;								
		            }
					
					 if($invoice_id!=''){
					$pconditions = array(
					'PurchasePacks.invoice'=>$invoice_id
					);	
					$iconditions = array(
					'RGroceryInvoice.invoice'=>$invoice_id
					);					
		            }
					
					
					$list = $this->PurchasePacks->find('all',array('conditions'=>array_filter($pconditions)));
					$this->set('list',$list);
					
					$grosstotal = $this->PurchasePacks->find('all',array('conditions'=>array_filter($pconditions),'fields'=>array('sum(PurchasePacks.gross_amount) as totalgross'))); 
					$this->set('grosstotal',$grosstotal);
					
					
					
					$totalinvoice = $this->RGroceryInvoice->find('all',array('conditions'=>array_filter($iconditions),'fields'=>array('sum(RGroceryInvoice.gross_amount) as totalinvoice'))); 
					
					$this->set('totalinvoice',$totalinvoice);
					
					$this->set('vendor_id',$vendor_id);
					$this->set('invoice_id',$invoice_id);
					$this->set('invoice_array',$invoice_array);					
				
					
		}
		
		
			public function admin_purchasereportreset() {			
			    $this->Session->delete('vendor');
				$this->Session->delete('invoice');				
			    $redirect_url = array('controller' => 'grocery', 'action' => 'purchase_report')	;
			    return $this->redirect( $redirect_url );
			}
			
		
		
		public function admin_new_pack_ajax(){
				$this->loadModel('PurchasePacks');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
				$this->loadModel('RGroceryInvoice');
				$this->loadModel('ItemMapping');					
			    $this->autoRender = false;

		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
			if($this->request->is('post')){
           // print_r($this->data);exit;
			$this->request->data['PurchasePacks']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['PurchasePacks']['store_id'] = $this->Session->read('stores_id');
			if(@$this->request->data['purchase_date']!=""){
				$newdate = explode('-',$this->request->data['purchase_date']);
			$pdate = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			$this->request->data['purchase_date'] = $pdate;
			}

			$this->request->data['PurchasePacks']['Item_Scan_Code'] = @$this->request->data('Item_Scan_Code');
			$this->request->data['PurchasePacks']['vendor'] = @$this->request->data('vendor');
			$this->request->data['PurchasePacks']['invoice'] = @$this->request->data('invoice');
			$this->request->data['PurchasePacks']['Item_Name'] = @$this->request->data('Item_Name');
			$this->request->data['PurchasePacks']['Item#'] = @$this->request->data('Item');
			$this->request->data['PurchasePacks']['Case_Cost'] = @$this->request->data('Case_Cost');
			$this->request->data['PurchasePacks']['Units_Per_Case'] = @$this->request->data('Units_Per_Case');
			if($this->request->data['PurchasePacks']['Units_Per_Case']==null || $this->request->data['PurchasePacks']['Units_Per_Case'] ==""){
$this->request->data['PurchasePacks']['Units_Per_Case']=1;
}
			$this->request->data['PurchasePacks']['Case_Retail'] = @$this->request->data('Case_Retail');
			if($this->request->data['PurchasePacks']['Case_Retail']==null || $this->request->data['PurchasePacks']['Case_Retail'] ==""){
$this->request->data['PurchasePacks']['Case_Retail']=1;
}
			$margin = round((((@$this->request->data('Case_Retail') - @$this->request->data('Case_Cost'))/@$this->request->data('Case_Retail'))*100),2);

			
			$this->request->data['PurchasePacks']['Margin'] = $margin;
			$this->request->data['PurchasePacks']['purchase'] = @$this->request->data('purchase');
			$this->request->data['PurchasePacks']['purchase_date'] = @$this->request->data('purchase_date');
			$this->request->data['PurchasePacks']['Unit_per_cost'] = @$this->request->data('Case_Cost')/@$this->request->data('Units_Per_Case');
			$this->request->data['PurchasePacks']['gross_amount'] = @$this->request->data('gross_amount');
			$this->request->data['PurchasePacks']['created_at'] = date('Y-m-d');
			
			$insert_update_type='added';
			
			$findpurchaseitem = $this->ItemMapping->find('first',array('conditions'=>array('Item_Scan_Code'=>$this->request->data('Item_Scan_Code'),'ItemMapping.store_id'=>$this->Session->read('stores_id'))));
			if(isset($findpurchaseitem['ItemMapping']['id'])){
           	 $this->request->data['ItemMapping']['id'] = $findpurchaseitem['ItemMapping']['id'];
			//$insert_update_type='updated';
			$this->request->data['ItemMapping']['updated_at'] = date('Y-m-d');
			}else{
				$this->request->data['ItemMapping']['created_at'] = date('Y-m-d');
			}
			
			$this->request->data['ItemMapping']['Item_Scan_Code'] = @$this->request->data('Item_Scan_Code');
			$this->request->data['ItemMapping']['Item_Name'] = @$this->request->data('Item_Name');
			$this->request->data['ItemMapping']['Item#'] = @$this->request->data('Item');
			$this->request->data['ItemMapping']['Case_Cost'] = @$this->request->data('Case_Cost');
			$this->request->data['ItemMapping']['Units_Per_Case'] = @$this->request->data('Units_Per_Case');
			if($this->request->data['ItemMapping']['Units_Per_Case']==null || $this->request->data['ItemMapping']['Units_Per_Case'] ==""){
$this->request->data['ItemMapping']['Units_Per_Case']=1;
}
			$this->request->data['ItemMapping']['Case_Retail'] = @$this->request->data('Case_Retail');
			if($this->request->data['ItemMapping']['Case_Retail']==null || $this->request->data['ItemMapping']['Case_Retail'] ==""){
				$this->request->data['ItemMapping']['Case_Retail']=1;
				}
			$this->request->data['ItemMapping']['store_id'] = @$this->Session->read('stores_id');
			$this->request->data['ItemMapping']['vendor'] = @$this->request->data('vendor');
				
			if($this->PurchasePacks->save($this->data)){	
			$this->ItemMapping->save($this->data);		
				
			$invoice_conditions = array('RGroceryInvoice.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'RGroceryInvoice.store_id' => $this->Session->read('stores_id'),
			'RGroceryInvoice.vendor_id'=>$this->request->data('vendor'),
			'RGroceryInvoice.invoice'=>$this->request->data('invoice')
			);		
			
			$purchase_conditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
			'PurchasePacks.store_id' => $this->Session->read('stores_id'),
			'PurchasePacks.vendor'=>$this->request->data('vendor'),
			'PurchasePacks.invoice'=>$this->request->data('invoice')
			);
			
			
			$this->RGroceryInvoice->virtualFields['i_gross_amount'] = 'SUM(RGroceryInvoice.gross_amount)';
		    $RGroceryInvoice = $this->RGroceryInvoice->find('first',array('conditions' => $invoice_conditions));
			
			
			$i_gross_amount=0;
			if(isset($RGroceryInvoice)){		
		    $i_gross_amount=$RGroceryInvoice['RGroceryInvoice']['i_gross_amount'];
			}				
			
			$this->PurchasePacks->virtualFields['p_gross_amount'] = 'SUM(PurchasePacks.gross_amount)';
		    $PurchasePacks=$this->PurchasePacks->find('first',array('conditions' => $purchase_conditions));				
			
			//pr($PurchasePacks);die;	
			$p_gross_amount=0;
			if(isset($PurchasePacks)){		
		    $p_gross_amount=$PurchasePacks['PurchasePacks']['p_gross_amount'];
			}			
			
			$status=0;
				
			if($i_gross_amount==$p_gross_amount)
			{
			$status=1;
			}
			
			if($i_gross_amount>0 && $i_gross_amount!=$p_gross_amount && $i_gross_amount!=0)
			{
			$status=2;
			}
			
			if($p_gross_amount==0)
			{
			$status=3;
			}
			
			if(isset($RGroceryInvoice)){
			$this->request->data['RGroceryInvoice']['retail_status'] =$status;
			$this->RGroceryInvoice->id =$RGroceryInvoice['RGroceryInvoice']['id'];				
			$this->RGroceryInvoice->save($this->data);
			}
			
				
			$finditem = $this->RGroceryItem->find('first',array('conditions'=>array('plu_no'=>$this->request->data['Item_Scan_Code'],'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));
if(isset($finditem['RGroceryItem']['id'])){
            $this->request->data['RGroceryItem']['id'] = $finditem['RGroceryItem']['id'];
            $this->request->data['RGroceryItem']['Item#'] = $this->request->data['Item'];
            $this->request->data['RGroceryItem']['purchase'] = $finditem['RGroceryItem']['purchase'] + $this->request->data['purchase'];			
			//print_r($this->data);exit;

			$this->RGroceryItem->save($this->data);
}
echo 'Purchase pack '.$insert_update_type;
}else{
echo 'Purchase date required';
	
}
			}
			}
			
	public function admin_deletepack($id=null){
	 $this->autoRender = false ;	
	 $this->loadModel('PurchasePacks');
	  $this->loadModel('RGroceryItem');
	 
	 $finditem = $this->PurchasePacks->findAllById($id);
	 $plu_num=$finditem[0]['PurchasePacks']['Item_Scan_Code'];
	 $purchase=$finditem[0]['PurchasePacks']['purchase'];
	 //echo '<pre>';print_r($purchase);die;
	 $finditem1 = $this->RGroceryItem->find('first',array('conditions'=>array('plu_no'=>$plu_num,'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));	
	 //echo '<pre>';print_r($finditem1);die;
	 $grocery_item_purchase=$finditem1['RGroceryItem']['purchase'];
	 //echo '<pre>';print_r($grocery_item_purchase);die;
	 $final_purchase= $grocery_item_purchase-$purchase;
	 
	 
	 if ($id) {
		 $this->RGroceryItem->id = $finditem1['RGroceryItem']['id'];
   		 $this->RGroceryItem->saveField('purchase',$final_purchase);
	 }
	if($this->PurchasePacks->delete($id)){
		$this->Session->setFlash(__('Data deleted successfully.'));
		return $this->redirect(array("controller" => "r_grocery_invoices", "action" => "index","admin" => true));
	}
}		
	public function admin_purchasegetplu(){		
		$this->loadModel('RGroceryItem');
	    $this->autoRender = false;
		
		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.id'=>$this->request->data['id'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));	
		//pr($first);die;
		if(!empty($first)){
			$inven = $first['RGroceryItem']['current_inventory']-$first['RGroceryItem']['sales_inventory'];
			$data['result'] = array('plu_no'=>$first['RGroceryItem']['plu_no'],'id'=>$first['RGroceryItem']['id'],'inven'=>$inven,'Item'=>$first['RGroceryItem']['Item#'],'price'=>$first['RGroceryItem']['price']);
			//pr($data);die;
			echo json_encode($data);
			}else{
			exit;
		}
		
	}
			
	public function admin_purchasegetplu1(){		
		$this->loadModel('RGroceryItem');
	    $this->autoRender = false;
		
		$count = $this->RGroceryItem->query("SELECT *  FROM items_mappings WHERE Item_Name=".$this->request->data['id']." AND store_id= ".$this->Session->read('stores_id')."");
	//	pr($count); die;
		$num=count($count);
		if($num==0){
		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.id'=>$this->request->data['id'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));	
		//pr($first);die;
			if(!empty($first)){
				$inven = $first['RGroceryItem']['current_inventory']-$first['RGroceryItem']['sales_inventory'];
				$data['result'] = array('plu_no'=>$first['RGroceryItem']['plu_no'],'id'=>$first['RGroceryItem']['id'],'inven'=>$inven,'Item'=>$first['RGroceryItem']['Item#'],'price'=>$first['RGroceryItem']['price'],'unitpercase'=>'','trick'=>'no','Case_Retail'=>'');
				//pr($data);die;
				echo json_encode($data);
			}else{
				exit;
			}
		
		}else{
			$newid=$count[0]['items_mappings']['id'];
				$purchasepack = $this->RGroceryItem->query("SELECT *  FROM items_mappings WHERE id=".$newid." AND store_id= ".$this->Session->read('stores_id')."");
				//pr($purchasepack); die;
				if(!empty($purchasepack)){
					$data['result'] = array('plu_no'=>$purchasepack[0]['items_mappings']['Item_Scan_Code'],'id'=>'','inven'=>'','Item'=>$purchasepack[0]['items_mappings']['Item#'],'price'=>$purchasepack[0]['items_mappings']['Case_Cost'],'unitpercase'=>$purchasepack[0]['items_mappings']['Units_Per_Case'],'trick'=>'yes','Case_Retail'=>$purchasepack[0]['items_mappings']['Case_Retail']);
					echo json_encode($data);
				}else{
					exit;
				}
			
		}
		
	}
	
	public function admin_getitemnamecost(){
	//$this->loadModel('RGroceryItem');
	$this->loadModel('PurchasePacks');
	$this->autoRender = false;
	//$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.id'=>$this->request->data['id'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));	
	    //$Item_Name=$first['RGroceryItem']['description'];	
			
		$second= $this->PurchasePacks->find('first',array('conditions' => array('PurchasePacks.Item_Name'=>$this->request->data['id'],'PurchasePacks.store_id'=>$this->Session->read('stores_id')),  "order" => "PurchasePacks.id DESC"));
		if(!empty($second)){		
			$data['result'] = array('Case_Cost'=>$second['PurchasePacks']['Case_Cost'],'Case_Retail'=>$second['PurchasePacks']['Case_Retail']);
			echo json_encode($data);
			}else{
			exit;
		}
		
	}
	
	public function admin_getdesc(){
		$this->loadModel('RGroceryItem');
	    $this->autoRender = false; 

		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.plu_no'=>$this->request->data['pul'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));
		if(!empty($first)){
			$inven = $first['RGroceryItem']['current_inventory']-$first['RGroceryItem']['sales_inventory'];
			$data['result'] = array('desc'=>$first['RGroceryItem']['description'],'id'=>$first['RGroceryItem']['id'],'inven'=>$inven,'Item'=>$first['RGroceryItem']['Item#'],'price'=>$first['RGroceryItem']['price']);
			echo json_encode($data);
			}else{
			$data['result'] = array('desc'=>'','id'=>'','inven'=>'','Item'=>'','price'=>'');
			echo json_encode($data);
		}
		
	}
	
	public function admin_getdesc1(){
		$this->loadModel('RGroceryItem');
	    $this->autoRender = false; 

			$count = $this->RGroceryItem->query("SELECT *  FROM items_mappings WHERE Item_Scan_Code=".$this->request->data['pul']." AND store_id= ".$this->Session->read('stores_id')."");
			$num=count($count);
			//pr($num); die;
			if($num==0)
			{	
			$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.plu_no'=>$this->request->data['pul'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));
				if(!empty($first)){
					$inven = $first['RGroceryItem']['current_inventory']-$first['RGroceryItem']['sales_inventory'];
					$data['result'] = array('desc'=>$first['RGroceryItem']['description'],'id'=>$first['RGroceryItem']['id'],'inven'=>$inven,'Item'=>$first['RGroceryItem']['Item#'],'price'=>$first['RGroceryItem']['price'],'unitpercase'=>'','trick'=>'no','Case_Retail'=>'');
					echo json_encode($data);
					}else{
					$data['result'] = array('desc'=>'','id'=>'','inven'=>'','Item'=>'','price'=>'','unitpercase'=>'','trick'=>'no','Case_Retail'=>'');
					echo json_encode($data);
				}
			}else{
				$newid=$count[0]['items_mappings']['id'];
				$purchasepack = $this->RGroceryItem->query("SELECT *  FROM items_mappings WHERE id=".$newid." AND store_id= ".$this->Session->read('stores_id')."");
				//pr($purchasepack); die;
				if(!empty($purchasepack)){
					$data['result'] = array('desc'=>$purchasepack[0]['items_mappings']['Item_Name'],'id'=>'','inven'=>'','Item'=>$purchasepack[0]['items_mappings']['Item#'],'price'=>$purchasepack[0]['items_mappings']['Case_Cost'],'unitpercase'=>$purchasepack[0]['items_mappings']['Units_Per_Case'],'trick'=>'yes','Case_Retail'=>$purchasepack[0]['items_mappings']['Case_Retail']);
					echo json_encode($data);
				}else{
					$data['result'] = array('desc'=>'','id'=>'','inven'=>'','Item'=>'','price'=>'','unitpercase'=>'','trick'=>'no','Case_Retail'=>'');
					echo json_encode($data);
				}
			}
	}
	
	public function admin_getcost(){
	$this->loadModel('PurchasePacks');
	$this->autoRender = false; 
		$first = $this->PurchasePacks->find('first',array('conditions' => array('PurchasePacks.Item_Scan_Code'=>$this->request->data['pul'],'PurchasePacks.store_id'=>$this->Session->read('stores_id')),  "order" => "PurchasePacks.id DESC"));
		if(!empty($first)){		
			$data['result'] = array('Case_Cost'=>$first['PurchasePacks']['Case_Cost'],'Units_Per_Case'=>$first['PurchasePacks']['Units_Per_Case'],'Case_Retail'=>$first['PurchasePacks']['Case_Retail']);
			echo json_encode($data);
			}else{
			exit;
		}
		
	}
	
	
		public function admin_getitemcost(){
	$this->loadModel('PurchasePacks');
	$this->autoRender = false; 
		$first = $this->PurchasePacks->find('first',array('conditions' => array('PurchasePacks.Item#'=>$this->request->data['item'],'PurchasePacks.store_id'=>$this->Session->read('stores_id')),  "order" => "PurchasePacks.id DESC"));
		if(!empty($first)){		
			$data['result'] = array('Case_Cost'=>$first['PurchasePacks']['Case_Cost'],'Case_Retail'=>$first['PurchasePacks']['Case_Retail']);
			echo json_encode($data);
			}else{
			exit;
		}
		
	}
	
	
	
	public function admin_getplu(){
		/*$this->loadModel('PurchasePack');
	 $this->autoRender = false; 
//echo $this->request->data['item'];
		$first = $this->PurchasePack->find('first',array('conditions' => array('PurchasePack.Item#'=>$this->request->data['item'],'PurchasePack.store_id'=>$this->Session->read('stores_id')),  "order" => "PurchasePack.id DESC"));
		if(!empty($first)){
			
			$data['result'] = array('plu'=>$first['PurchasePack']['Item_Scan_Code'],'desc'=>$first['PurchasePack']['Item_Name']);
			echo json_encode($data);
			
			}else{
				exit;
			}*/
			
		
		$this->loadModel('RGroceryItem');
	    $this->autoRender = false; 

		$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.Item#'=>$this->request->data['item'],'RGroceryItem.store_id'=>$this->Session->read('stores_id')),  "order" => "RGroceryItem.id DESC"));
		if(!empty($first)){
			$inven = $first['RGroceryItem']['current_inventory']-$first['RGroceryItem']['sales_inventory'];
			$data['result'] = array('desc'=>$first['RGroceryItem']['description'],'id'=>$first['RGroceryItem']['id'],'inven'=>$inven,'plu_no'=>$first['RGroceryItem']['plu_no'],'price'=>$first['RGroceryItem']['price']);
			echo json_encode($data);
			}else{
				exit;
		}
		
		
			
		
		}
			
	public function admin_getplu1(){

		$this->loadModel('ItemMapping');	
	    $this->autoRender = false; 
		$first = $this->ItemMapping->find('first',array('conditions' => array('ItemMapping.Item#'=>$this->request->data['item'],'ItemMapping.store_id'=>$this->Session->read('stores_id')),  "order" => "ItemMapping.id DESC"));
			if(!empty($first)){
				$data['result'] = array('plu'=>$first['ItemMapping']['Item_Scan_Code'],'des'=>$first['ItemMapping']['Item_Name'],'unirpercase'=>$first['ItemMapping']['Units_Per_Case'],'casecost'=>$first['ItemMapping']['Case_Cost'],'caseretails'=>$first['ItemMapping']['Case_Retail']);
				echo json_encode($data);
				}else{
					exit;
				}
		}
			
}