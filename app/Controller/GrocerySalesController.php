<?php
App::uses('AppController', 'Controller');
//error_reporting(0);
class GrocerySalesController extends AppController {

	public $components = array('Paginator','Flash','Session','ExportXls');

	public function admin_index() {
		
			 $this->Setredirect();    
			 $full_date = '';
			 $form_date='';
			 $to_date='';
			//echo  $this->Session->read('stores_id');die;
		   $conditions = array('GrocerySale.store_id' => $this->Session->read('stores_id'),'GrocerySale.department_number' => array(1,2)); 
           if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 			
				//echo 'ok';die;
				
				if($this->request->data['GrocerySale']['full_date']!= ""){
				$full_date=$this->request->data['GrocerySale']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}	
				$this->Session->write('PostSearch', 'PostSearch');		
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);
				}
				else {
				$this->Session->delete('PostSearch');
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}	

            if ($this->Session->check('PostSearch')) {			
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');				
            }
             	 
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'GrocerySale.ending_date_time >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'GrocerySale.ending_date_time <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
			
		    $grocery_sales=$this->GrocerySale->find('all', array('conditions' =>$conditions));
		
		    //echo '<pre>';print_r($grocery_sales);die;
		
			$this->set('full_date',$full_date);				
			//echo '<pre>';print_r($this->Paginator->paginate());die;
            $this->set('grocery_sales',$grocery_sales); 
			
			           
        } else {		
            $this->set('full_date',$full_date);	
            $this->set('grocery_sales','');
        }   	
	
		
	}
	
	public function admin_export()
	{
 		        $conditions = array('GrocerySale.store_id' => $this->Session->read('stores_id'),'GrocerySale.department_number' => array(1,2)); 
	
	            $date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				
				//echo '<pre>';print_r($this->request->data);die;
				
				if($this->request->data['grocery_sales']['full_date']!= "" ) {			
				//echo 'ok';die;
				//echo '<pre>';print_r($this->request->data);die;
				$full_date=$this->request->data['grocery_sales']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
			
				
				if(isset($fdate) && $fdate!=''){				
						
				$conditions['GrocerySale.ending_date_time >='] = $fdate;			
				}
				if(isset($tdate) && $tdate!=''){						
				
				$conditions['GrocerySale.ending_date_time <=']  = $tdate;
				}
				
				}					
			    //echo '<pre>';print_r($conditions);die;
                $grocery_sales=$this->GrocerySale->find('all', array('conditions' =>$conditions));
			    //echo '<pre>';print_r($grocery_sales);die;	
				$arr=array();		
				if(isset($grocery_sales) && $grocery_sales!='' && count($grocery_sales)>0){
				        $date_array=array();
						foreach($grocery_sales as $grocery_sales_row)
						{
						$date=$grocery_sales_row['GrocerySale']['ending_date_time'];	
						array_push($date_array,$date);
						}
						$date_array=array_unique($date_array);
						//echo '<pre>';print_r($date_array);die;
						$nwdate_array=array();
						foreach($date_array as $key=>$val)
						{
						array_push($nwdate_array,$val);	
						}
						//echo '<pre>';print_r($nwdate_array);die;
						
						$date_count=count($nwdate_array);
	                    //echo $date_count;die;	
						$cstmz_grocery_sales=array();
	
						for($i=0;$i<$date_count;$i++)
						{
						foreach($grocery_sales as $grocery_sales_row)	
						{	
						if($grocery_sales_row['GrocerySale']['department_number']=='1' && $grocery_sales_row['GrocerySale']['ending_date_time']==$nwdate_array[$i]){
						$cigarate_net_sales=$grocery_sales_row['GrocerySale']['net_sales'];
						}
						if($grocery_sales_row['GrocerySale']['department_number']=='2' && $grocery_sales_row['GrocerySale']['ending_date_time']==$nwdate_array[$i]){
						$tobaco_net_sales=$grocery_sales_row['GrocerySale']['net_sales'];
						}
						$sub_array=array('date'=>$nwdate_array[$i],'cigarate_net_sales'=>$cigarate_net_sales,'tobaco_net_sales'=>$tobaco_net_sales);
						}	
						array_push($cstmz_grocery_sales,$sub_array);	
						}
				
				 //echo '<pre>';print_r($cstmz_grocery_sales);die;	
				
				         $tot_cig_sl=0;						 
						 $tot_grnt_sl=0;
						 $tot_grt_sl=0;					 
						 $tot_tob_sl=0;					 
						 $tot_total=0;						 
						 $tot_sl_tx=0;	
						 foreach ($cstmz_grocery_sales as $data)
						 {							 
					     $Ruby2Tax=  ClassRegistry::init('Ruby2Tax')->find('all', array('fields' => array('taxable_sales', 'non_taxable_sales','net_tax'),'conditions'=>array('Ruby2Tax.period_end_date'=>$data['date'],'Ruby2Tax.tax_sysid'=>'1','store_id'=>$this->Session->read('stores_id')))); 
						 //echo '<pre>';print_r($Ruby2Tax[0]['Ruby2Tax']);
						 $Ruby2Summary=ClassRegistry::init('Ruby2Summary')->find('all', array('fields' => array('fuel_sales'),'conditions'=>array('Ruby2Summary.period_end_date'=>$data['date'],'store_id'=>$this->Session->read('stores_id')))); 	
						 //echo '<pre>';print_r($Ruby2Summary[0]['Ruby2Summary']);die;  
						 $cigarate_net_sales=0;
						 if(isset($data['cigarate_net_sales']) && $data['cigarate_net_sales']!=''){
						 $cigarate_net_sales=$data['cigarate_net_sales'];
						 }
						 
						 $tobaco_net_sales=0;
						 if(isset($data['tobaco_net_sales']) && $data['tobaco_net_sales']!=''){
						 $tobaco_net_sales=$data['tobaco_net_sales'];
						 }
						 
						 $fuel_sales=0;
						 if(isset($Ruby2Summary[0]['Ruby2Summary']['fuel_sales']) && $Ruby2Summary[0]['Ruby2Summary']['fuel_sales']!=''){
						 $fuel_sales=$Ruby2Summary[0]['Ruby2Summary']['fuel_sales'];
						 }
						 
						 $ruby2_non_taxable_sales=0;
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales']) && $Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales']!=''){
						 $ruby2_non_taxable_sales=$Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales'];
						 }
						 $non_taxable_sales=$ruby2_non_taxable_sales-$fuel_sales; 
						 
						 $ruby2_taxable_sales=0; 
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['taxable_sales']) && $Ruby2Tax[0]['Ruby2Tax']['taxable_sales']!=''){
						 $ruby2_taxable_sales=$Ruby2Tax[0]['Ruby2Tax']['taxable_sales'];
						 }
						 $taxable_sales=($cigarate_net_sales+$tobaco_net_sales)-($ruby2_taxable_sales);
						 $fuel_sales=0;
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['net_tax']) && $Ruby2Tax[0]['Ruby2Tax']['net_tax']!=''){
						 $fuel_sales=$Ruby2Tax[0]['Ruby2Tax']['net_tax'];
						 }
						 $tot_cig_sl=$tot_cig_sl+$cigarate_net_sales;
						 
						 $tot_grnt_sl=$tot_grnt_sl+$non_taxable_sales;
						 $tot_grt_sl=$tot_grt_sl+$taxable_sales;						
						 
						 $tot_tob_sl=$tot_tob_sl+$tobaco_net_sales;
					
						 $tot_sl_tx=$tot_sl_tx+$fuel_sales;					
						 
		                 $total=$cigarate_net_sales+$non_taxable_sales+$taxable_sales+$tobaco_net_sales;
						 $tot_total=$tot_total+$total;	
						 $res1['date']=$data['date'];
						 $res1['cigarate_net_sales']=$cigarate_net_sales;	
	                     $res1['non_taxable_sales']=$non_taxable_sales;
						 $res1['taxable_sales']=$taxable_sales;
						 $res1['tobaco_net_sales']=$tobaco_net_sales;
						 $res1['total']=$total;
						 $res1['fuel_sales']=$fuel_sales;
	                     array_push($arr,$res1);	
						 }
				 array_push($arr,array('Total Sales',$tot_cig_sl,$tot_grnt_sl,$tot_grt_sl,$tot_tob_sl,$tot_total,$tot_sl_tx));
				}
				//echo '<pre>';print_r($res1);die;
				$fileName = "grocery_sales_reports".time().".xls";			
				$headerRow = array('Date','Cigarates','Grocery Non Taxable','Grocery Taxable','Tobacco','Total','Sales Tax');
				$this->ExportXls->export($fileName, $headerRow, $arr);
	}
	
	
	
	public function admin_reset() {	    
		$this->Session->delete('PostSearch');
		$this->Session->delete('full_date');
		$this->Session->delete('from_date');
		$this->Session->delete('to_date');
		$redirect_url = array('controller' => 'grocery_sales', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}


}
