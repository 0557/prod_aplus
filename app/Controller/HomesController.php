<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class HomesController extends AppController {

    var $uses = array('EmailTemplate', 'Home');

    public function index() {
        $this->layout = 'index'; 
    }
    
    public function home(){
        $this->layout = 'index';
     
    }
    public function about() {
        
    }

    public function blog() {
        
    }

    public function categories() {
        
    }

    public function faq() {
        
    }

    public function search_result() {
        
    }

    public function error() {
        
    }

  

}
