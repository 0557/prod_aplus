<?php
ob_start();
App::uses('AppController', 'Controller');

class HouseacccustsalesController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {

        $this->Setredirect();
        $this->loadmodel('Houseacccustsale');  
        $full_date = '';
		$form_date='';
		$to_date='';
		$search_key='';
		$conditions = array('Houseacccustsale.store_id' => $this->Session->read('stores_id'),'Houseacccustsale.company_id' => $this->Session->read('Auth.User.company_id')); 
        
		$this->set('full_date',$full_date);	
		$this->set('search_key',$search_key);				
		$this->paginate = array('conditions' => $conditions,
			'order' => array(
				'Houseacccustsale.id'=>'desc'
			)
		);
		$this->set('full_date',$full_date);	
		$this->set('search_key',$search_key);
		$this->set('Salereports', $this->Paginator->paginate()); 
            //$this->set('Salereports', '');
        
    }
	
	
	
	public function admin_delete($id=null) {
	if($id){
			if($this->Houseacccustsale->delete($id)){					
				$this->Session->setFlash(__('Successfully Deleted'), 'default', array('class' => 'success'));
				return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));				
			}else{
				$this->Session->setFlash(__('Report could not be deleted'), 'default', array('class' => 'error'));
				return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
			}
		}
	
	}
	 public function admin_add() {		 
          $this->Setredirect();
        if ($this->request->is('post')) {			
		   //echo '<pre>';print_r($this->request->data);die;
			
			if($this->request->data['Houseacccustsale']['image']['name']!=''){			          
					$filename = null;									
					if (!empty($this->request->data['Houseacccustsale']['image']['tmp_name']) && is_uploaded_file($this->request->data['Houseacccustsale']['image']['tmp_name'])
					) {										
					$file=$this->request->data['Houseacccustsale']['image'];							
					$ext = pathinfo($file['name'], PATHINFO_EXTENSION); 					
					$filename = time().'.'.$ext; 								
					move_uploaded_file($this->request->data['Houseacccustsale']['image']['tmp_name'], WWW_ROOT.'houseacccustsaledocs'.DS.$filename);					
					}									
					$this->request->data['Houseacccustsale']['image']= $filename;				
			}else{
					$this->request->data['Houseacccustsale']['image']='';	
			}	
         
            $this->request->data['Houseacccustsale']['store_id'] = $this->Session->read('stores_id');
            $this->request->data['Houseacccustsale']['company_id'] = $this->Session->read('Auth.User.company_id');
            $this->request->data['Houseacccustsale']['created'] = date('Y-m-d');
				
			//echo '<pre>'; print_r($this->request->data);die;
            $this->Houseacccustsale->create();
            if ($this->Houseacccustsale->save($this->request->data)) {
                $this->Session->setFlash(__('You have successfully added report'), 'default', array('class' => 'success'));
				return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
            } else {
                $this->Session->setFlash(__('House account cust sales could not be saved. Please, try again.'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
            }
			
        }
	
    }
	
	 public function admin_edit($id = null) {		
          $this->Setredirect();
        if (!$this->Houseacccustsale->exists($id)) {
            throw new NotFoundException(__('Invalid report'));
        }
        if ($this->request->is(array('post','put'))) {  
		
		      $this->request->data['Houseacccustsale']['id']=$id;
		    if($this->request->data['Houseacccustsale']['image']['name']!=''){			          
					$filename = null;									
					if (!empty($this->request->data['Houseacccustsale']['image']['tmp_name']) && is_uploaded_file($this->request->data['Houseacccustsale']['image']['tmp_name'])
					) {										
					$file=$this->request->data['Houseacccustsale']['image'];							
					$ext = pathinfo($file['name'], PATHINFO_EXTENSION); 					
					$filename = time().'.'.$ext; 								
					move_uploaded_file($this->request->data['Houseacccustsale']['image']['tmp_name'], WWW_ROOT.'houseacccustsaledocs'.DS.$filename);					
					}									
					$this->request->data['Houseacccustsale']['image']= $filename;	
					
					unlink(WWW_ROOT.'houseacccustsaledocs'.DS.$this->request->data['Houseacccustsale']['pre_image']);		
			}else{	

					unset($this->request->data['Houseacccustsale']['image']);
			}
			        unset($this->request->data['Houseacccustsale']['pre_image']);
				
		    //$this->Houseacccustsale->create();
            
            if ($this->Houseacccustsale->save($this->request->data)) {
				
                $this->Session->setFlash(__('Report successfully updated'), 'default', array('class' => 'success'));
				return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
            } else {
                $this->Session->setFlash(__('Report could not be updated'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
            }
        } else {
            $options = array('conditions' => array('Houseacccustsale.' . $this->Houseacccustsale->primaryKey => $id));
            $this->request->data = $this->Houseacccustsale->find('first', $options);
			if($this->request->data['Houseacccustsale']['store_id']!=$this->Session->read('stores_id') || $this->request->data['Houseacccustsale']['company_id']!=$this->Session->read('Auth.User.company_id')){
		    $this->Session->setFlash(__('Store or company not matched.'), 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'houseacccustsales','action' => 'index'));	
			}
			//echo '<pre>';print_r($this->request->data);die;
			
			 $this->set('Houseacccustsale',$this->request->data); 
        }		
		
    }
	
	


	 


}
