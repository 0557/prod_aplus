<?php
error_reporting(0);
App::uses('AppController', 'Controller');

/**
 * PurchaseInvoices Controller
 *
 * @property PurchaseInvoice $PurchaseInvoice
 * @property PaginatorComponent $Paginator
 */
class ItemMappingsController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
	$this->Setredirect();
	$this->loadModel('Customer');
	$this->loadModel('ItemMapping');			
	$this->loadModel('RGroceryItem');
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
		$conditions[] = array(
						'ItemMapping.store_id' => $this->Session->read('stores_id')
					);
		if($this->request->is('post')){
			
					if(!empty($this->request->data['ItemMapping']['Item_Scan_Code']) || $this->request->data['ItemMapping']['Item_Name'] != "" || $this->request->data['ItemMapping']['Item'] != "") {
					
					  //  echo '<pre>';  print_r($this->request->data);exit;	
				$this->Session->write('PostSearch', 'PostSearch');
				$this->Session->write('page',1);
				$this->Session->write('Item_Scan_Code', $this->request->data['ItemMapping']['Item_Scan_Code']);
				$this->Session->write('Item_Name', $this->request->data['ItemMapping']['Item_Name']);
				$this->Session->write('Item', $this->request->data['ItemMapping']['Item']);
				} else {
						$this->Session->delete('PostSearch');
						$this->Session->delete('Item_Scan_Code');
						$this->Session->delete('Item_Name');
						$this->Session->delete('Item');
						$this->Session->delete('page');
				}
		}else {
				
				$this->Session->delete('page');
			}
			if ($this->Session->check('PostSearch')) {
					
				 $this->Session->read('Item_Scan_Code');
				 $Item_Scan_Code = $this->Session->read('Item_Scan_Code');
				 $Item_Name = $this->Session->read('Item_Name');
				 $Item = $this->Session->read('Item');
			}
			
			if(isset($Item_Scan_Code) && $Item_Scan_Code!=''){ 
				$conditions[] = array(
						'ItemMapping.Item_Scan_Code' => $Item_Scan_Code,
					);
				}
			if(isset($Item_Name) && $Item_Name!=''){
				$conditions[] = array(
						'ItemMapping.Item_Name'=> $Item_Name 
					);
			}
			if(isset($Item) && $Item!=''){
				$conditions[] = array(
						'ItemMapping.Item#'=> $Item 
					);
			}
			$this->set('Item_Scan_Code', $Item_Scan_Code);
			$this->set('Item_Name', $Item_Name);
			$this->set('Item', $Item);
			
		if($this->Session->read('page')!=''){
		$this->redirect(array('action' => 'index', 'page' => 1));	
			
		}		
			
        $this->paginate = array('conditions' =>$conditions,"limit" => 10, "order" => "ItemMapping.id DESC");
	 	$this->set('list', $this->paginate());
		
		$itemname= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		$this->set('itemname', $itemname);		
	}
		public function admin_reset() {
	
					$this->Session->delete('PostSearch');
						$this->Session->delete('Item_Scan_Code');
						$this->Session->delete('Item_Name');
						$this->Session->delete('Item');
						
					$redirect_url = array('controller' => 'ItemMappings', 'action' => 'index')	;
					return $this->redirect( $redirect_url );
	}

	public function admin_add(){
				$this->loadModel('ItemMapping');
				$this->loadModel('Customer');
				$this->loadModel('RGroceryItem');
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
	
			if($this->request->is('post')){
			$count = $this->Customer->query("SELECT *  FROM items_mappings WHERE Item_Scan_Code=".$this->request->data['ItemMapping']['Item_Scan_Code']." AND store_id= ".$this->Session->read('stores_id')."");
			$num=count($count);
			if($num==0){
		
				$this->request->data['ItemMapping']['store_id'] = $this->Session->read('stores_id');
				$this->request->data['ItemMapping']['created_at'] = date('Y-m-d');
				if($this->ItemMapping->save($this->data)){
					$this->Session->setFlash(__('Submited new data successfully.'));
					return $this->redirect(array('action' => 'index'));
				}
			}else{
					 	//pr($count) ; die;
				$newid=$count[0]['items_mappings']['id'];
				$this->Session->setFlash(__('This PLU already exists.'));
				return $this->redirect(array('action' => 'edit',$newid));
			
			}
		}
		$item= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		//pr($item);die;
		$this->set('item', $item);
			}
	 function admin_edit($id){
		
		$this->loadModel('ItemMapping');
		$this->loadModel('Customer');
		$this->loadModel('RGroceryItem');
			  
		$item= $this->RGroceryItem->find('list',array('fields'=>'id, description','conditions' => array('RGroceryItem.store_id'=>$this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));	
		$this->set('item', $item);					
				
		$vendor = $this->Customer->find('list',array('conditions'=>array('Customer.type'=>'grocery','Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id'))));
		$this->set('vendor',$vendor);
		
		$conditions = array(
			'ItemMapping.store_id' => $this->Session->read('stores_id'),
			'ItemMapping.id'=>$id
			);
			$list = $this->ItemMapping->find('first',array('conditions'=>$conditions));
			$this->set('list',$list);
		//pr($list); 
			if($this->request->is('post')){
					$count = $this->Customer->query("SELECT *  FROM items_mappings WHERE Item_Scan_Code=".$this->request->data['ItemMapping']['Item_Scan_Code']." AND store_id= ".$this->Session->read('stores_id')." AND id!=".$id."");
			$num=count($count);
				if($num==0){
					$this->request->data['ItemMapping']['updated_at'] =date('Y-m-d');
					if($this->ItemMapping->save($this->data)){
						 $this->Session->setFlash(__('Submited new data updated successfully.'));
						 return $this->redirect(array('action' => 'index'));
					}
				}else{
					$newid=$count[0]['items_mappings']['id'];
					$this->Session->setFlash(__('This PLU already exists.'));
					return $this->redirect(array('action' => 'edit',$newid));
				}
			}
			
	}
	
	 function admin_delete($id){
		 $this->loadModel('ItemMapping');
		 if($this->ItemMapping->delete($id)){
					$this->Session->setFlash(__('Data deleted successfully.'));
					return $this->redirect(array('action' => 'index'));
					
				}
		 
		 }
}
