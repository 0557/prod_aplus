<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class ItemReconcilationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
		error_reporting(0);
         $this->Setredirect();
		 $this->loadModel('RubyDepartment');
		 $this->loadModel('RGroceryItem');
		//echo $this->Session->read('stores_id'); die;
		 	 $rGroceryDepartments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'), 'conditions'=>array('RubyDepartment.store_id'=>$this->Session->read('stores_id'))));
		
		$this->set('departmentlist',$rGroceryDepartments);

		$pluItemBySale = $this->pluItemBySale();
		
	
			$conditions= array('RGroceryItem.store_id' => $this->Session->read('stores_id'));
			$conditions['RGroceryItem.company_id']=$this->Session->read('Auth.User.company_id');
			$search = '';
			if($this->request->is('post')){
			
				$scan_code = $this->request->data['RGroceryItem']['scan_code'];
				$department = $this->request->data['RGroceryItem']['department'];
				$item_name = $this->request->data['RGroceryItem']['item_name'];
				$this->Session->write('scan_code', $scan_code );
				$this->Session->write('department', $department );
				$this->Session->write('item_name', $item_name );
			
			if(isset($scan_code) && $scan_code!=''){
				$conditions['RGroceryItem.plu_no'] = $scan_code;
			}
			if(isset($department) && $department!=''){
				$conditions['RGroceryItem.r_grocery_department_id '] = $department;
			}
			
			if(isset($item_name) && $item_name!=''){
				$conditions['RGroceryItem.description like'] = "%".trim($item_name)."%";
			}
			$data = $this->RGroceryItem->find('all',array( 
			'conditions'=>$conditions,
			'order'=>array('RGroceryItem.id'=>'desc')
			));
		
			} else {
				
				$this->Session->delete('scan_code');
			$this->Session->delete('department' );
			$this->Session->delete('item_name' );
				$data="";
			}
			
		$this->set('rGroceryItems', $data);
    }
	
	public function admin_report() {
		//pr($this->request->data); die;
		$this->loadModel('RGroceryItem');
		$conditions= array('RGroceryItem.store_id' => $this->Session->read('stores_id'));
		$conditions['RGroceryItem.company_id']=$this->Session->read('Auth.User.company_id');
		$scan_code=$this->request->data['plu1'];
		$item_name=$this->request->data['item1'];
		$department=$this->request->data['department1'];
		
		$pluItemBySale = $this->pluItemBySale();
		
		if(isset($scan_code) && $scan_code!=''){
				$conditions[] = array(
					'RGroceryItem.plu_no' => $scan_code,
				);
			}
			if(isset($department) && $department!=''){
				$conditions[] = array(
					'RGroceryItem.r_grocery_department_id ' => $department,
				);
			}
			
			if(isset($item_name) && $item_name!=''){
				$conditions[] = array(
					'RGroceryItem.description ' => $item_name,
				);
			}
			
			$data = $this->RGroceryItem->find('all',array( 
			'conditions'=>$conditions,
			'order'=>array('RGroceryItem.id'=>'desc')
			));
		
		$this->set('data', $data);
		//pr($data); die;
		
			$filename=time()."_file.pdf";
			$this->set('filename', $filename);
			$this->layout = '/pdf/default1';
			$this->render('/Pdf/my_pdf_view');
			$redirect_url = array('controller' => 'ItemReconcilations', 'action' => 'index')	;
			return $this->redirect( $redirect_url );
		
	}
	
	
	public function admin_newplu() {
		$this->Session->delete('oldval');
	$this->Session->delete('index');
	$this->Session->delete('ScoreCardCriteria');
		$this->layout = '/fancy';
		$this->render('/Fancy/itemplu');
	}
	
	public function admin_getnewplu() {
	
		$this->autoRender = false;
		$this->response->type('json');
		
		$this->loadModel('RGroceryItem');
		$rowcount = $this->RGroceryItem->find('count', array('conditions' => array('RGroceryItem.plu_no' => $_POST['pul'],'RGroceryItem.store_id' => $this->Session->read('stores_id'),'RGroceryItem.company_id' =>$this->Session->read('Auth.User.company_id') )));
		
		$pluItemBySale=$this->pluItemBySale();
		$sales_inventory = @$pluItemBySale[$_POST['pul']];
		
		
		$fetch = $this->RGroceryItem->find('all', array(
		'conditions' => array('RGroceryItem.plu_no' => $_POST['pul'],'RGroceryItem.store_id' => $this->Session->read('stores_id'),'RGroceryItem.company_id' =>$this->Session->read('Auth.User.company_id')),
		'order' => array('RGroceryItem.id DESC'),
		'limit' => 1
		));
		$fetch[0]['RGroceryItem']['salesinventory']=$sales_inventory;
		
		$current_inventory=$fetch[0]['RGroceryItem']['current_inventory'];
		$purchase=$fetch[0]['RGroceryItem']['purchase'];
		
		$hand_value=(($current_inventory + $purchase) - $sales_inventory);
		
		$fetch[0]['RGroceryItem']['hand_value']=$hand_value;
		
		//pr($fetch[0]['RGroceryItem']);
		$json = json_encode($fetch[0]['RGroceryItem']);
   		$this->response->body($json);
	
	}
	
	public function admin_addplu() {
		    $posttype = $_POST['posttype'];
			$index = $_POST['index'];
			$this->Session->write('index',$index);
			$oval=array();
			$a[$index] = $_POST;
			//pr($_POST); die;
			if($this->Session->read('oldval')) {
				$oval = $this->Session->read('oldval');
			} else {
				if ($posttype=='new') {
					$oval = array();
				} elseif($posttype=='submit') {
					$oval = $a;
				}
			}
			
			if ($posttype=='new') {
				$oval = array_merge($oval, $a);
			} 	
				$this->Session->write('oldval', $oval);
				
				$this->Session->write('ScoreCardCriteria', $this->Session->read('oldval') );
				//pr($this->Session->read('ScoreCardCriteria'));
		}
	
		function pluItemBySale($plu_no = null)
		{
			$this->loadModel('RubyDepartment');
			if($plu_no) {
				$rSdata = $this->RubyDepartment->query('SELECT ruby_header_id, plu_no, sum(no_of_items_sold) as no_of_items_sold  FROM `ruby_pluttls`  where plu_no='. $plu_no . ' AND store_id='. $this->Session->read('stores_id') . ' group by plu_no,store_id' );
			} else {
				$rSdata = $this->RubyDepartment->query('SELECT ruby_header_id, plu_no, sum(no_of_items_sold) as no_of_items_sold  FROM `ruby_pluttls`  where store_id='. $this->Session->read('stores_id') . ' group by plu_no,store_id' );	
			}
			
			$pluItemBySale = array();
			if(!empty($rSdata)) {
				foreach($rSdata as $key => $value) {
					if(isset($value['ruby_pluttls']['plu_no'])) {
						$pluItemBySale[$value['ruby_pluttls']['plu_no']] = @$value[0]['no_of_items_sold'];
					}
				}
			}
			
			$this->set('pluItemBySale',$pluItemBySale);
			
		
		
			return $pluItemBySale;
		}

	public function admin_reset() {
	$this->Session->delete('oldval');
	$this->Session->delete('index');
	$this->Session->delete('ScoreCardCriteria');
		$redirect_url = array('controller' => 'ItemReconcilations', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}
	
	public function admin_on_hand_update(){
		$this->Session->delete('oldval');
	$this->Session->delete('index');
	$this->Session->delete('ScoreCardCriteria');
        $this->autoRender = false;
		$this->loadModel('RGroceryItem');
		
			if($this->request->is('post')){
//				 pr($this->request->data); die;
					$idstr=$this->request->data['ids'];
					$cost=$this->request->data['cost'];
					//$idarr=implode(',',$idstr) ;
					$length = sizeof($idstr);
					for($i=0;$i<$length;$i++){
							$this->RGroceryItem->query("UPDATE r_grocery_items SET on_self =". $cost[$i]." WHERE id = ".$idstr[$i]."");
					}
				echo 'updated';
			}
	}

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

	 
	 
	 
	 
	 	 public function admin_shortover_update() {
					$this->loadmodel('RGroceryItem');
					 $this->loadModel('Globalupcs');
					$this->autoRender = false;
											
				$size = sizeof($this->request->data['ids']);
					$yes = 0;
						for($i = 0; $i < $size; $i++ ){
$itemid = explode('*',$this->request->data['ids'][$i]);

	$newid = $itemid[1];
	$findplu = $this->RGroceryItem->find('first',array('conditions'=>array('RGroceryItem.id'=>$itemid[0],'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));
	//echo $this->request->data['shortover'][$newid];
//print_r($findplu['RGroceryItem']['current_inventory']); exit;
//echo $newid;
//echo $this->request->data['inventory'][$newid];
//print_r($this->request->data['inventory']);
	//					 exit;
						 if(@$findplu['RGroceryItem']['id']){
						 if(@$findplu['RGroceryItem']['plu_no']){
							 $this->request->data['RGroceryItem']['on_self'] = '0';
							 $this->request->data['RGroceryItem']['adujust'] = '0';
							 $this->request->data['RGroceryItem']['id'] = $findplu['RGroceryItem']['id'];
							 $this->request->data['RGroceryItem']['current_inventory'] = str_replace(',','',$this->request->data['shortover'][$newid]) + $findplu['RGroceryItem']['current_inventory'];
							 					if($this->RGroceryItem->save($this->data)){
												$yes=1;
													}
					}
					}

							
					}
					
					if($yes==1){
							echo 'Selected items imported';
							}else{
								echo 'Selected items not imported';
								}

			}
	 
	 }
