<?php
error_reporting(0);
App::uses('AppController', 'Controller');
class ItemSalesbydatesController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session');
	
    public function admin_index1() {
		
		$this->Setredirect();		
		$this->loadModel('RubyUprodt');	
		$this->loadModel('RGroceryItem');
		$this->loadModel('PurchasePack');
		$this->loadModel('RubyDepartment');
		
			
		$ruby_header_data = $this->RubyUprodt->find('first',array('conditions'=>array('store_id' =>$this->Session->read('stores_id')),'order'=>array('RubyUprodt.id DESC')));
		//pr($this->request->data);die;
		
		    $start_date=date('Y-m-01');
			$this->Session->write('start_date',$start_date);
		    $end_date= $ruby_header_data['RubyHeader']['ending_date_time'];
			$this->Session->write('end_date',$end_date);			
		    $conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
			if($this->request->is('post')){		
			
			
				if($this->request->data['date-range'] != "") {
					    //echo $this->request->data['date-range'];die;	
						$date_range_array=explode('-',$this->request->data['date-range']);	
						//pr($date_range_array);die;	
						$starts_date=$date_range_array[0].'-'.$date_range_array[1].'-'.$date_range_array[2];	
						$ends_date=$date_range_array[3].'-'.$date_range_array[4].'-'.$date_range_array[5];												
						$this->Session->write('start_date',$starts_date);	
						$this->Session->write('end_date',$ends_date);	
				} else {
					    $this->Session->delete('start_date');
						$this->Session->delete('end_date');
				}	
				
			    if($this->request->data['item_salesbydates']['plu_no'] != "") {	
						$this->Session->write('plu_no',$this->request->data['item_salesbydates']['plu_no']);	
				} else {
						$this->Session->delete('plu_no');
				}	
				
				   if($this->request->data['item_salesbydates']['department_id'] != "") {	
						$this->Session->write('department_id',$this->request->data['item_salesbydates']['department_id']);	
				} else {
						$this->Session->delete('department_id');
				}	
					
			}else{
			$this->Session->delete('plu_no');	
			$this->Session->delete('department_id');
			}
			
			
			$start_date=$this->Session->read('start_date');
			$end_date=$this->Session->read('end_date');
			
			if ($this->Session->read('plu_no')!='') {
				$plu_no = $this->Session->read('plu_no');
			}
				if ($this->Session->read('department_id')!='') {
				$department_id = $this->Session->read('department_id');
			}
			
			if(isset($start_date) && $start_date!=''){				
				$conditions['RubyHeader.ending_date_time >='] = $start_date;			
			}
			if(isset($end_date) && $end_date!=''){
				$conditions['RubyHeader.ending_date_time <='] = $end_date;				
			}
			
			if(isset($plu_no) && $plu_no!=''){
				$conditions['ItemSalesbydate.plu_no']=$plu_no;				
			}
		$all_date=$start_date.' - '.$end_date;		
		$this->set('all_date',$all_date);		
		$this->set('plu_no',$plu_no);
		$this->set('department_id',$department_id);		
		//pr($conditions);//die;		
		$ItemSalesbydates=$this->ItemSalesbydate->find('all',array('conditions' => $conditions));
		//echo $total = $this->ItemSalesbydate->find('count',array('conditions' => $conditions));	 die;	
		//pr($ItemSalesbydates);die;		
		if(count($ItemSalesbydates)!=''){
		foreach ($ItemSalesbydates as $key => $ItemSalesbydate) {
			$item_plu_no =  $ItemSalesbydate['ItemSalesbydate']['plu_no'];
		    $store_id =  $ItemSalesbydate['ItemSalesbydate']['store_id'];
			
			
			$pprow = $this->PurchasePack->query("select purchase_packs.Unit_per_cost from purchase_packs where Item_Scan_Code='".$item_plu_no."' and store_id='".$store_id."'");
			$Unit_per_cost = $pprow[0]['purchase_packs']['Unit_per_cost'];
			$ItemSalesbydates[$key]['ItemSalesbydate']['Unit_per_cost'] = $Unit_per_cost;
			
				
			
			if($department_id != "") {
			
				$conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));				
				$countrow = $this->RGroceryItem->query("select * from r_grocery_items where plu_no='".$ItemSalesbydate['ItemSalesbydate']['plu_no']."' and r_grocery_department_id='".$department_id."' and store_id='".$store_id."'");	
				
				//pr($countrow); die;			
				if(count($countrow)>0){
					//echo 'if ok';
		   $results = $this->RGroceryItem->query("SELECT description AS item_description, department_name AS item_dept, price AS item_price from `r_grocery_items` WHERE `store_id` = $store_id AND `plu_no` =  $item_plu_no");
		   
		   $item_description = $results[0]['r_grocery_items']['item_description'];
		   $item_dept = $results[0]['r_grocery_items']['item_dept'];
		   $item_price = $results[0]['r_grocery_items']['item_price'];
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_description'] = $item_description;
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_dept'] = $item_dept;
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_price'] = $item_price;
				}
				else
				{
					$ItemSalesbydates='';
				}

			}
			else
			{
				
				
		   $results = $this->RGroceryItem->query("SELECT description AS item_description, department_name AS item_dept, price AS item_price from `r_grocery_items` WHERE `store_id` = $store_id AND `plu_no` =  $item_plu_no");		
		   $item_description = $results[0]['r_grocery_items']['item_description'];
		   $item_dept = $results[0]['r_grocery_items']['item_dept'];
		   $item_price = $results[0]['r_grocery_items']['item_price'];
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_description'] = $item_description;
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_dept'] = $item_dept;
		   $ItemSalesbydates[$key]['ItemSalesbydate']['item_price'] = $item_price;
			}
			
		 }
		}
		
		 //pr($results);die;
	//pr($ItemSalesbydates);die;	
    $this->set('ItemSalesbydates', $ItemSalesbydates);
	

	
		$dep_name='';
	$RGroceryItems = '';
	if($this->Session->read('plu_no')!="") {
		$conditions2= array('RGroceryItem.store_id' =>$this->Session->read('stores_id'), 'RGroceryItem.plu_no' =>$this->Session->read('plu_no'));
		$RGroceryItems=$this->RGroceryItem->find('all',array('conditions' => $conditions2));
		
		$r_grocery_department_id=$RGroceryItems[0]['RGroceryItem']['r_grocery_department_id'];
		
		$condi= array('RubyDepartment.store_id' =>$this->Session->read('stores_id'), 'RubyDepartment.number' =>$r_grocery_department_id);
		$Ruby=$this->RubyDepartment->find('all',array('conditions' => $condi));	
		//pr($Ruby); die;
		$dep_name=$Ruby[0]['RubyDepartment']['name'];
		
		
	}
	$this->set('RGroceryItems', $RGroceryItems);
	$this->set('dep_name', $dep_name);
	
	$PurchasePacks = '';
	if($this->Session->read('plu_no')!="") {		
		
		$conditions3= array('PurchasePack.store_id' =>$this->Session->read('stores_id'), 'PurchasePack.Item_Scan_Code' =>$this->Session->read('plu_no'));
		$this->PurchasePack->virtualFields['Max_Margin'] = 'max(PurchasePack.Margin)';
		$PurchasePacks=$this->PurchasePack->find('all',array('conditions' => $conditions3));
				
		//pr($PurchasePacks);die;
		
		foreach ($PurchasePacks as $key => $PurchasePack) {
		  $Item_Scan_Code =  $PurchasePack['PurchasePack']['Item_Scan_Code'];
		  $store_id =  $PurchasePack['PurchasePack']['store_id'];
		  $results2= $this->RGroceryItem->query("SELECT `description` AS `item_description`,`department_name` AS `item_dept`, `price` AS `item_price` from `r_grocery_items` WHERE `store_id` = '".$store_id."' AND `plu_no` = '".$Item_Scan_Code."' ");          
		  $item_price = $results2[0]['r_grocery_items']['item_price'];
		  $PurchasePacks[$key]['PurchasePack']['item_price'] = $item_price;
		 }
	}
	
	$this->set('PurchasePacks', $PurchasePacks);	

	
 $condition_dept= array('RubyDepartment.store_id' =>$this->Session->read('stores_id'));
 $RubyDepartment=$this->RubyDepartment->find('list',array('fields' =>array('number','name'),'conditions' => $condition_dept)); $this->set('RubyDepartment', $RubyDepartment);
	
    }
	
	
	public function admin_index() {
		$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
			$this->loadModel('RGroceryItem');
		$this->loadModel('PurchasePack');
		$this->loadModel('RubyDepartment');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->Session->read('stores_id');
		$conditions = array('RubyPluttl.store_id' => $storeId);
		//pr($this->request->data); die;
	if ($this->request->is('post')) {

					$conditions['RubyHeader.store_id'] = $storeId;
					
				if($this->request->data['date-range'] != "") {
						$date_range_array=explode('-',$this->request->data['date-range']);	
						$starts_date=$date_range_array[0].'-'.$date_range_array[1].'-'.$date_range_array[2];	
						$ends_date=$date_range_array[3].'-'.$date_range_array[4].'-'.$date_range_array[5];												
						$this->Session->write('start_date',$starts_date);	
						$this->Session->write('end_date',$ends_date);	
				} else {
					    $this->Session->delete('start_date');
						$this->Session->delete('end_date');
				}	
						$start_date=$this->Session->read('start_date');
						$end_date=$this->Session->read('end_date');
						
						if(isset($start_date) && $start_date!=''){				
						$conditions['RubyHeader.ending_date_time >='] = $start_date;			
						}
						if(isset($end_date) && $end_date!=''){
						$conditions['RubyHeader.ending_date_time <='] = $end_date;				
						}
				
				
				$all_date=$start_date.' - '.$end_date;		
				$this->set('all_date',$all_date);		
				/*$to_dates = explode('-', $this->request->data['from']);//14-12-2015
				$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
				$conditions['RubyHeader.ending_date_time'] = $to_date;
				$this->set('enddate',$to_date);*/

//print_r($this->request->data['ruby_deptotals']['department']);


 				if($this->request->data['ruby_deptotals']['plu_no'] != "") {	
						$this->Session->write('plu_no',$this->request->data['ruby_deptotals']['plu_no']);	
				} else {
						$this->Session->delete('plu_no');
				}
					if ($this->Session->read('plu_no')!='') {
				$plu_no = $this->Session->read('plu_no');
			}	
				
				if(isset($plu_no) && $plu_no!=''){
					$conditions['RubyPluttl.plu_no']=$plu_no;				
				}
				$this->set('plu_no',$plu_no);
				
			if($this->request->data['ruby_deptotals']['department']){
		//	echo $this->request->data['department'];
	$conditions['r_grocery_item.r_grocery_department_id'] = $this->request->data['ruby_deptotals']['department'];
	$conditions['r_grocery_item.store_id'] = $storeId;
			
			$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc')));
							
							
							//pr($conditions);die;
			$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
						//	echo $storeId; 
							//pr($fvolume);exit;
		}else{
		
		
		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc')));
							
							
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
		}
	
		
		$dep_name='';
	$RGroceryItems = '';
	if($this->Session->read('plu_no')!="") {
		$conditions2= array('RGroceryItem.store_id' =>$this->Session->read('stores_id'), 'RGroceryItem.plu_no' =>$this->Session->read('plu_no'));
		$RGroceryItems=$this->RGroceryItem->find('all',array('conditions' => $conditions2));
		
		$r_grocery_department_id=$RGroceryItems[0]['RGroceryItem']['r_grocery_department_id'];
		
		$condi= array('RubyDepartment.store_id' =>$this->Session->read('stores_id'), 'RubyDepartment.number' =>$r_grocery_department_id);
		$Ruby=$this->RubyDepartment->find('all',array('conditions' => $condi));	
		//pr($Ruby); die;
		$dep_name=$Ruby[0]['RubyDepartment']['name'];
		
		
	}
	$this->set('RGroceryItems', $RGroceryItems);
	$this->set('dep_name', $dep_name);
	
	$PurchasePacks = '';
	if($this->Session->read('plu_no')!="") {		
		
		$conditions3= array('PurchasePack.store_id' =>$this->Session->read('stores_id'), 'PurchasePack.Item_Scan_Code' =>$this->Session->read('plu_no'));
		$this->PurchasePack->virtualFields['Max_Margin'] = 'max(PurchasePack.Margin)';
		$PurchasePacks=$this->PurchasePack->find('all',array('conditions' => $conditions3));
				
		//pr($PurchasePacks);die;
		
		foreach ($PurchasePacks as $key => $PurchasePack) {
		  $Item_Scan_Code =  $PurchasePack['PurchasePack']['Item_Scan_Code'];
		  $store_id =  $PurchasePack['PurchasePack']['store_id'];
		  $results2= $this->RGroceryItem->query("SELECT `description` AS `item_description`,`department_name` AS `item_dept`, `price` AS `item_price` from `r_grocery_items` WHERE `store_id` = '".$store_id."' AND `plu_no` = '".$Item_Scan_Code."' ");          
		  $item_price = $results2[0]['r_grocery_items']['item_price'];
		  $PurchasePacks[$key]['PurchasePack']['item_price'] = $item_price;
		 }
	}
	
	$this->set('PurchasePacks', $PurchasePacks);	
	
	
	

	}else{

	$first = $this->RubyPluttl->find('first',array('conditions'=>array('RubyPluttl.store_id'=>$storeId), 'order'=>array('RubyPluttl.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyPluttl']['ruby_header_id'])  {

		$rubyfind = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.id'=>$first['RubyPluttl']['ruby_header_id'],'RubyHeader.store_id' => $storeId), 'order'=>array('RubyHeader.id'=>'desc')));
		
		$conditions['RubyHeader.ending_date_time']=$rubyfind['RubyHeader']['ending_date_time'];
		}	
//	echo $rubyfind['RubyHeader']['ending_date_time'];
//	echo $rubyfind['RubyHeader']['id'];
		$this->set('enddate',$rubyfind['RubyHeader']['ending_date_time']);
		}		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc')));
							
							
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));

	}


		$this->set('somevalue', $sumevalue);
		$this->set('pluttls', $fvolume);
		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
	}
	
	public function admin_reset() {
		$this->Session->delete('end_date');
		$this->Session->delete('plu_no');
		$redirect_url = array('controller' => 'item_salesbydates', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}


	
	
	

	
	
	
	
	
}
