<?php
ob_start();
//error_reporting(0);
App::uses('AppController', 'Controller');

class LiveInventoriesController extends AppController 
{
    public $components = array('Paginator', 'Flash', 'Session');
    
    public function admin_index()
    {		
        $this->Setredirect();
        $this->loadmodel('LiveInventory'); 
		$this->loadmodel('RGroceryItem'); 
		$this->loadmodel('RubyDepartment'); 
        $date = '';	
		$department='';		
		$searchkey='';
		
		$Departments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'), 'conditions'=>array('RubyDepartment.store_id'=>$this->Session->read('stores_id'))));
		//echo '<pre>';print_r($Departments);die;   
		$this->set('departments',$Departments);		
		
		//echo $this->Session->read('stores_id'); die;
		$conditions = array('LiveInventory.store_id' => $this->Session->read('stores_id'),'RGroceryItem.store_id' => $this->Session->read('stores_id'),'RGroceryItem.company_id' => $this->Session->read('Auth.User.company_id')); 
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;      
				
				if($this->request->data['LiveInventory']['date']!= "" || $this->request->data['LiveInventory']['lvdepartment']!= "" || $this->request->data['LiveInventory']['searchkey']!= ""){	
				$this->Session->write('PostSearch', 'PostSearch');
				
				if($this->request->data['LiveInventory']['date']!= "" ) {			
				//echo 'ok';die;
				$date=$this->request->data['LiveInventory']['date'];
				$this->Session->write('date',$date);
				} else {
			  	$this->Session->delete('date');		
				}
				
				if($this->request->data['LiveInventory']['lvdepartment']!=''){
				$this->Session->write('department',$this->request->data['LiveInventory']['lvdepartment']);	
		        }else{
		        $this->Session->delete('department');	
				}
				
				if($this->request->data['LiveInventory']['searchkey']!= "" ) {			
				//echo 'ok';die;
				$search_key=$this->request->data['LiveInventory']['searchkey'];
				$this->Session->write('searchkey',$search_key);
				} else {
			  	$this->Session->delete('searchkey');		
				}								
					
				}else{
				$this->Session->delete('PostSearch');
				}
					
			           
        }
		
		 if ($this->Session->check('PostSearch')) {
				//echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $date=$this->Session->read('date');
				$department=$this->Session->read('department');
				$searchkey=$this->Session->read('searchkey');
            }
             
		   if($department!=''){			   
		        //echo $department;die;	
				$conditions[] = array(
                    'RGroceryItem.r_grocery_department_id ' =>$department,
                );			
				
			}			        	
		
			if(isset($date) && $date!=''){						
				$conditions[] = array(
                    'DATE(LiveInventory.periodBeginDate)' => $date,
                );		
			}
			
			if($searchkey!=''){	
		        //echo $search_key;die;			
                $conditions[] = array(
				 'OR' => array(
                    'LiveInventory.upc LIKE' =>'%'.$searchkey.'%',
					'LiveInventory.name LIKE' =>'%'.$searchkey.'%'
                    )
				);			
				
			}		
		    //echo '<pre>';print_r($conditions);die;	
	        $this->paginate = array('conditions' => $conditions,
			    'limit' => 100,
				'group' => 'LiveInventory.upc',
                'order' => array(
                    'LiveInventory.periodBeginDate' => 'desc'
                )
            );		
			//echo '<pre>';print_r($this->Paginator->paginate());die;
			$this->set('date',$date);	
			$this->set('department',$department);	
			$this->set('searchkey',$searchkey);				
            $this->set('live_inventories',$this->Paginator->paginate()); 	        
			
				
	}	
	
    
    public function admin_reset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('start_date');
        $this->Session->delete('end_date');
        $this->Session->delete('department');
        $redirect_url = array('controller' => 'live_inventories', 'action' => 'admin_index');
        return $this->redirect($redirect_url);
    }
	
	 public function admin_delete($id = null) {
        if ($id) {
            if ($this->LiveInventory->delete($id)) {
                $this->Session->setFlash(__('Live inventory successfully Deleted'), 'default', array('class' => 'success'));
                return $this->redirect(array('controller' => 'live_inventories', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Live inventory not be deleted'), 'default', array('class' => 'error'));
                return $this->redirect(array('controller' => 'live_inventories', 'action' => 'index'));
            }
        }
    }
	

	
	
}
