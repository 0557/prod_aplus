<?php

ob_start();
App::uses('AppController', 'Controller');

class LotteriesController extends AppController {

    public $components = array('Paginator');

    public function admin_salesreport() {

        $this->Setredirect();
        $this->loadmodel('Lottery');
        $this->loadmodel('GamePacks');
        $full_date = '';
        $form_date = '';
        $to_date = '';

        if ($this->request->is('post')) {
            //echo '<pre>';print_r($this->request->data);die;       
            $date = array();
            $formarr = array();
            $toarr = array();

            if ($this->request->data['Lottery']['full_date'] != "") {
                //echo 'ok';die;
                $full_date = $this->request->data['Lottery']['full_date'];
                if (isset($full_date) && $full_date != '') {
                    $date = explode(' - ', $full_date);
                    $form_date = $date[0];
                    $formarr = explode('/', $form_date);
                    $fdate = $formarr[2] . "-" . $formarr[0] . "-" . $formarr[1];

                    $to_date = $date[1];
                    $toarr = explode('/', $to_date);
                    $tdate = $toarr[2] . "-" . $toarr[0] . "-" . $toarr[1];
                }
                $this->Session->write('PostSearch', 'PostSearch');
                $this->Session->write('full_date', $full_date);
                $this->Session->write('fdate', $fdate);
                $this->Session->write('tdate', $tdate);
            }
            $conditions = array('Lottery.store_id' => $this->Session->read('stores_id'));

            if (isset($fdate) && $fdate != '') {
                $conditions[] = array(
                    'Lottery.updated >=' => $fdate,
                );
            }
            if (isset($tdate) && $tdate != '') {
                $conditions[] = array(
                    'Lottery.updated <=' => $tdate,
                );
            }

            $this->Session->write('conditions', $conditions);
        }

        if ($this->Session->check('conditions')) {
            $conditions = $this->Session->read('conditions');
            $PostSearch = $this->Session->read('PostSearch');
            $full_date = $this->Session->read('full_date');
            $fdate = $this->Session->read('fdate');
            $tdate = $this->Session->read('tdate');
        } else {
            $conditions = null;
        }

        if (isset($conditions) && !empty($conditions)) {
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'Lottery.id' => 'desc'
                ),
                'group' => 'Lottery.id',
            );
            $this->set('full_date', $full_date);
            $this->set('LotterySalesReport', $this->Paginator->paginate());
        }
    }

    public function admin_reset() {
        $this->Session->delete('conditions');
        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('from_date');
        $this->Session->delete('to_date');
        $redirect_url = array('controller' => 'lotteries', 'action' => 'salesreport');
        return $this->redirect($redirect_url);
    }

}
