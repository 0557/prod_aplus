<?php

ob_start();
App::uses('AppController', 'Controller');

class LotteriesController extends AppController {

    public $components = array('Paginator');

    public function admin_salesreport() {

        $this->Setredirect();
        $this->loadmodel('Lottery');
        $this->loadmodel('GamePacks');
        $full_date = '';
		$form_date='';
		$to_date='';
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['Lottery']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['Lottery']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
				$this->Session->write('PostSearch', 'PostSearch');
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);				   	
						
				} else {
			    //echo 'notok';die;
				$this->Session->delete('PostSearch');
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}		

            if ($this->Session->check('PostSearch')) {
				//echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');
            }
            $conditions = array('Lottery.store_id' => $this->Session->read('stores_id')); 
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'Lottery.updated >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'Lottery.updated <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
			if($this->request->data['Lottery']['full_date']!= "" ) {	
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'LotteryDailyReading.id' => 'desc'
                )
            );
			$this->set('full_date',$full_date);	
            $this->set('LotterySalesReport', $this->Paginator->paginate()); 
			}else{
			$this->set('full_date',$full_date);	
            $this->set('LotterySalesReport', '');
			}
			           
        } else {
            $this->set('full_date',$full_date);	
            $this->set('LotterySalesReport', '');
        }
    }
	
	
	public function admin_reset() {
	
		$this->Session->delete('PostSearch');
		$this->Session->delete('full_date');
		$this->Session->delete('from_date');
		$this->Session->delete('to_date');
		$redirect_url = array('controller' => 'lotteries', 'action' => 'salesreport')	;
		return $this->redirect( $redirect_url );
		
	}

}
