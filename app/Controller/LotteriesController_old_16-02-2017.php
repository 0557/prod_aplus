<?php
ob_start();
App::uses('AppController', 'Controller');


class LotteriesController extends AppController {

 
    public $components = array('Paginator');
	
	 public function admin_salesreport() {
		 
        $this->Setredirect(); 
	    $this->loadmodel('Lottery');
		$this->loadmodel('GamePacks');  
        $update_date=''; 
		if($this->request->is('post')){			
							
					if($this->request->data['update_date'] != "") {					   	
							$this->Session->write('PostSearch', 'PostSearch');
							$this->Session->write('update_date',$this->request->data['update_date']);	
					} else {
							$this->Session->delete('PostSearch');
							$this->Session->delete('update_date');
					}	
			
		
		if ($this->Session->check('PostSearch')) {
		$PostSearch = $this->Session->read('PostSearch');
		$update_date = $this->Session->read('update_date');				
		}
		
		$conditions= array('Lottery.store_id' =>$this->Session->read('stores_id'));
		
		if(isset($update_date) && $update_date!=''){
		$conditions[] = array(
		'Lottery.updated' => $update_date,
		);			
		}						
		$this->set('update_date', $update_date);
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'LotteryDailyReading.id' => 'desc'
			)
		);
		$this->set('LotterySalesReport', $this->Paginator->paginate());		
		}else{
		$this->set('update_date', $update_date);
		$this->set('LotterySalesReport','');		
		}

		
		
    }   
  

}
