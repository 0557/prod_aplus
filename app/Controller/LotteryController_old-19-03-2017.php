<?php

ob_start();
App::uses('AppController', 'Controller');
ini_set('memory_limit', '256M');
set_time_limit(0);

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class LotteryController extends AppController {

    //   public $components = array('Paginator', 'Mpdf.Mpdf');
    public $components = array('Paginator');

    public function admin_daily_report() {
        if (!$this->Session->read('stores_id')) {
            $this->Session->setFlash(__('Please select  store and try again.'));
        } else {
            $this->set('display', 'yes');
        }

        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->loadmodel('LotteryDailyReading');


        if (isset($this->request->data) && !empty($this->request->data)) {
            $this->request->data['lottery_daily_readings']['store_id'] = $this->Session->read('stores_id');
            $this->request->data['lottery_daily_readings']['company_id'] = $this->Session->read('Auth.User.company_id');

            //pr($this->request->data);

            $prev_ticket = $this->request->data['lottery_daily_readings']['prev_ticket'];
            $start_ticket = $this->request->data['lottery_daily_readings']['start_ticket'];
            $bin_no = $this->request->data['lottery_daily_readings']['bin_no'];
            $game_no = $this->request->data['lottery_daily_readings']['game_no'];
            $pack_no = $this->request->data['lottery_daily_readings']['pack_no'];
            $today_reading = $this->request->data['lottery_daily_readings']['today_reading'];
            $packdate = $this->request->data['lottery_daily_readings']['packdate'];
            $status = $this->request->data['lottery_daily_readings']['status'];
            $ticket_value = $this->request->data['lottery_daily_readings']['ticket_value'];
            //$ticket_value = substr($ticket_value, 1);

            $datetime = new DateTime();
            $newDate = $datetime->createFromFormat('m-d-Y', $packdate);
            $packdate = $newDate->format('Y-m-d');
            //echo "--" . $packdate;


            $conditions = array(
                'game_no' => $game_no,
                'pack_no' => $pack_no,
                'company_id' => $this->Session->read('Auth.User.company_id'),
                'store_id' => $this->Session->read('stores_id'),
                'created' => $packdate,
                'updated' => $packdate
            );


            $old_data = $this->LotteryDailyReading->find('first', array(
                'conditions' => $conditions
            ));

            //pr($old_data);
            if (empty($old_data)) {

                if ($prev_ticket == -1) {
                    $previous_ticket = $start_ticket;
                } else {
                    $previous_ticket = $prev_ticket;
                }

                if (in_array($status, array('Sold Out', 'Return', 'Settle'))) {
                    $today_reading = $today_reading + 1;
                    $today_sold = abs($today_reading - $previous_ticket);
                    $today_sold_value = $today_sold * $ticket_value;
                } else {
                    $today_sold = abs($today_reading - $previous_ticket);
                    $today_sold_value = $today_sold * $ticket_value;
                }

                $save_data = array(
                    'game_no' => $game_no,
                    'pack_no' => $pack_no,
                    'start_ticket' => $start_ticket,
                    'end_ticket' => $this->request->data['lottery_daily_readings']['end_ticket'],
                    'today_reading' => $today_reading,
                    'prev_ticket' => $previous_ticket,
                    'bin_no' => $bin_no,
                    'today_sold' => $today_sold,
                    'today_sold_value' => $today_sold_value,
                    'store_id' => $this->request->data['lottery_daily_readings']['store_id'],
                    'company_id' => $this->request->data['lottery_daily_readings']['company_id'],
                    'status' => $status,
                    'created' => $packdate,
                    'updated' => $packdate,
                );

                if ($this->LotteryDailyReading->save($save_data)) {
                    unset($conditions['created']);
                    unset($conditions['updated']);

                    $this->GamePacks->updateAll(array('status' => "'" . $status . "'"), $conditions);
                    $this->request->data = array();
                }
            } else {
//                $old_date = $old_data['LotteryDailyReading']['created'];
//                if ($old_date == $packdate) {
//                    $previous_ticket = $prev_ticket;
//                } else {
//                    $previous_ticket = $old_data['LotteryDailyReading']['today_reading'];
//                }
                //echo "In data present--".$old_data['LotteryDailyReading']['prev_ticket'];
                $previous_ticket = $prev_ticket;

                if (in_array($status, array('Sold Out', 'Return', 'Settle'))) {
                    $today_reading = $today_reading + 1;
                    $today_sold = abs($today_reading - $previous_ticket);
                    $today_sold_value = $today_sold * $ticket_value;
                } else {
                    $today_sold = abs($today_reading - $previous_ticket);
                    $today_sold_value = $today_sold * $ticket_value;
                }

                $save_data = array(
                    'game_no' => $game_no,
                    'pack_no' => $pack_no,
                    'start_ticket' => $start_ticket,
                    'end_ticket' => $this->request->data['lottery_daily_readings']['end_ticket'],
                    'today_reading' => $today_reading,
                    'prev_ticket' => $previous_ticket,
                    'bin_no' => $bin_no,
                    'today_sold' => $today_sold,
                    'today_sold_value' => $today_sold_value,
                    'store_id' => $this->request->data['lottery_daily_readings']['store_id'],
                    'company_id' => $this->request->data['lottery_daily_readings']['company_id'],
                    'status' => "'" . $status . "'",
                    'created' => "'" . $packdate . "'",
                    'updated' => "'" . $packdate . "'",
                );

                if ($this->LotteryDailyReading->updateAll($save_data, $conditions)) {

                    $this->Session->setFlash(__('Reading Enter successfully.'));

                    unset($conditions['created']);
                    unset($conditions['updated']);

                    $this->GamePacks->updateAll(array('status' => "'" . $status . "'"), $conditions);
                    $this->request->data = array();
                }
            }
        } else {
            $this->request->data = array();
        }

        if (isset($packdate) && $packdate != "") {
//            $datetime = new DateTime();
//            $newDate = $datetime->createFromFormat('m-d-Y', $save_date);
//            $save_date = $newDate->format('Y-m-d');
            $next = $packdate;
            //echo "<br>next save date=" . $next;

            $save_date2 = $newDate->format('m-d-Y');
            $this->set('pack_date', $save_date2);
            //echo "<br>pack save date=" . $save_date2;
        }

        $readings = $this->LotteryDailyReading->query(
                'SELECT * FROM `lottery_daily_readings` AS e '
                . 'INNER JOIN `game_packs` AS u '
                . 'ON e.game_no = u.game_no '
                . 'and (e.created = "' . $next . '" '
                . 'or e.updated = "' . $next . '" )'
                . 'and e.store_id = ' . $this->Session->read('stores_id') . ' '
                . 'and e.company_id = ' . $this->Session->read("Auth.User.company_id") . ' '
                . 'and e.store_id = u.store_id' . ' '
                . 'group by e.id');
        //pr($readings);
        $this->set('readings', $readings);
    }

    public function admin_getpacksactive1() {
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->loadmodel('LotteryDailyReading');
        $this->autoRender = false;
        //echo $this->request->data('game_no');die;
        $date = $this->request->data('date');

        $datetime = new DateTime();
        $newDate = $datetime->createFromFormat('m-d-Y', $date);
        $save_date = $newDate->format('Y-m-d');

        //echo $save_date;die;
        $dat = array('bin_no' => $this->request->data('bin_no'), //array('game_no' => $this->request->data('game_no'),
            'status' => array('active', 'Counting'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store_id' => $this->Session->read('stores_id'));

        $games = $this->GamePacks->find('all', array(
            'conditions' => $dat,
            'fields' => array('game_no', 'gamename')
        ));
        //$game_no = $games[0]['GamePacks']['game_no'];
        $result = array(
            'games' => $games,
                //'game_no' => $game_no
        );
        echo json_encode($result);
    }

    public function admin_getTicketData($no = null) {
        $this->autoRender = false;
        $this->loadModel('ImportedGames');
        $this->loadmodel('GamePacks');
        $this->loadModel('LotteryDailyReading');

        $game_no = $this->request->data('game_no');
        $bin_no = $this->request->data('bin_no');
        $date = $this->request->data('date');

        $datetime = new DateTime();
        $newDate = $datetime->createFromFormat('m-d-Y', $date);
        $date = $newDate->format('Y-m-d');


        $dat = array('game_no' => $game_no, //$this->request->data('game_no'),//array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store' => $this->Session->read('stores_id'));

        $ticket_data = $this->ImportedGames->find('first', array(
            'fields' => array('start_ticket', 'end_ticket', 'tickets_pack', 'game_name'),
            'conditions' => $dat,
        ));


        $dat = array('game_no' => $game_no, //array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store_id' => $this->Session->read('stores_id'),
            'bin_no' => $bin_no
        );


        $pack = $this->GamePacks->find('first', array(
            'conditions' => $dat,
            'fields' => array('pack_no', 'ticket_order', 'ticket_value')
        ));

        //echo "<pre>",print_r($pack_no),"</pre>";
        //echo $game_no;die;
        $prev_ticket_data = $this->LotteryDailyReading->find('all', array(
            'conditions' => $dat,
            //'fields' => array('today_reading'),
            'order' => 'LotteryDailyReading.id desc'
        ));

        $prev_ticket = 0;
        $prev_date = $prev_ticket_data[0]['LotteryDailyReading']['created'];
        if ($date == $prev_date) {
            $prev_ticket = $prev_ticket_data[0]['LotteryDailyReading']['prev_ticket'];
        } else {
            $prev_ticket = $prev_ticket_data[0]['LotteryDailyReading']['today_reading'];
        }

//echo "<pre>",print_r($prev_ticket),"</pre>";
        $result = array(
            'ticket_data' => $ticket_data,
            'pack_no' => $pack['GamePacks']['pack_no'],
            'ticket_order' => $pack['GamePacks']['ticket_order'],
            'ticket_value' => $pack['GamePacks']['ticket_value'],
            'prev_ticket' => $prev_ticket
        );
        echo json_encode($result);
    }

    public function get_reports() {
        $this->autoRender = false;
        //$this->Render = false;
        $this->loadmodel('LotteryDailyReading');
        //pr($this->data['date']);
        $date = $this->data['date'];

        $datetime = new DateTime();
        $newDate = $datetime->createFromFormat('m-d-Y', $date);
        $date = $newDate->format('Y-m-d');


        //$date = date('Y-m-d', strtotime($date));
        //echo $date;
        $readings = $this->LotteryDailyReading->query(
                'SELECT * FROM `lottery_daily_readings` AS e '
                . 'INNER JOIN `game_packs` AS u '
                . 'ON e.game_no = u.game_no '
                . 'and (e.created = "' . $date . '" '
                . 'or e.updated = "' . $date . '" )'
                . 'and e.store_id = ' . $this->Session->read('stores_id') . ' '
                . 'and e.company_id = ' . $this->Session->read("Auth.User.company_id") . ' '
                . 'and e.store_id = u.store_id' . ' '
                . 'group by e.id');
        //pr($readings);
        //$this->set('readings', $readings);
        echo json_encode($readings);
    }

    public function admin_gettcikets($id = null) {
        $this->loadmodel('LotteryDailyReading');
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->autoRender = false;

        $olddat = array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'pack_no' => $this->request->data('pack_no'),
            'store_id' => $this->Session->read('stores_id'));


        $imported_games = $this->ImportedGames->find('first', array(
            'fields' => array('start_ticket', 'end_ticket', 'tickets_pack'),
            'conditions' => array('game_no' => $this->request->data('game_no'),
                'store' => $this->Session->read('stores_id'))
        ));
        //pr($limit_tickets);
        //echo json_encode($imported_games);
        $start_ticket = $imported_games['ImportedGames']['start_ticket'];
        $end_ticket = $imported_games['ImportedGames']['end_ticket'];
        $starting_inventory = $imported_games['ImportedGames']['tickets_pack'];

        $limit_tickets = $this->LotteryDailyReading->find('first', array(
            'fields' => array('updated_start_ticket', 'updated_end_ticket', 'starting_inventory', 'today_reading'),
            'conditions' => $olddat,
            'order' => array('id DESC '),
            'LIMIT' => 0, 1
        ));
        $updated_start_ticket = $limit_tickets['LotteryDailyReading']['updated_start_ticket'];
        $updated_end_ticket = $limit_tickets['LotteryDailyReading']['updated_end_ticket'];
        $updated_starting_inventory = $limit_tickets['LotteryDailyReading']['starting_inventory'];
        $today_reading = $limit_tickets['LotteryDailyReading']['today_reading'];

        $avail_condition = array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'pack_no' => $this->request->data('pack_no'),
            'store_id' => $this->Session->read('stores_id'));


        $available_ticket = $this->GamePacks->find('first', array('conditions' => $avail_condition));

//pr($available_ticket);
        $data = array(
            'end_ticket' => $end_ticket,
            'start_ticket' => $start_ticket,
            'starting_inventory' => $starting_inventory,
            'updated_start_ticket' => $updated_start_ticket,
            'updated_end_ticket' => $updated_end_ticket,
            'updated_starting_inventory' => $updated_starting_inventory,
            'availa' => $available_ticket['GamePacks']['available'],
            'ticket_order' => $available_ticket['GamePacks']['ticket_order'],
            'today_reading' => $today_reading
        );


        echo json_encode($data);
    }

    public function admin_index() {
        if (!$this->Session->read('stores_id')) {
            $this->Session->setFlash(__('Please select  store.'));
        } else {
            $this->set('display', 'yes');
        }
    }

    public function admin_game_import() {
        $this->loadmodel('Games');
        $this->set('import_game', 'active');
//	$games = $this->Games->find('all',array('conditions' => array('status' => 0)));
//	$this->set('Games', $games);
    }

    public function admin_import_game() {

        $this->loadmodel('ImportedGames');
        $this->loadmodel('Games');
        $this->autoRender = false;
        $size = sizeof($this->request->data['ids']);
        $yes = 0;

        for ($i = 0; $i < $size; $i++) {
            $getgame = $this->Games->find('first', array('conditions' => array('id' => $this->request->data['ids'][$i])));
            if ($getgame['Games']['id']) {
                $this->ImportedGames->create();
                $dat = array('games' => $this->request->data['ids'][$i], 'state' => $this->request->data['state'], 'status' => 'Ready to sale', 'store' => $this->Session->read('stores_id'), 'game_name' => $getgame['Games']['game_name'], 'value' => $getgame['Games']['value'], 'tickets_pack' => $getgame['Games']['tickets_pack'], 'start_ticket' => $getgame['Games']['start_ticket'], 'end_ticket' => $getgame['Games']['end_ticket'], 'game_no' => $getgame['Games']['game_no'], 'company_id' => $this->Session->read('Auth.User.company_id'), 'available' => $getgame['Games']['tickets_pack'], 'created_at' => date('Y-m-d'));
                if ($this->ImportedGames->save($dat)) {
                    $yes = 1;
                }
            }
        }
        if ($yes == 1) {
            $this->Session->setFlash(__('Selected games imported successfully.'));
        } else {
            $this->Session->setFlash(__('Selected games already imported successfully.'));
        }
    }

    public function admin_getgamesbystate() {
        $this->loadmodel('Games');
        $this->loadmodel('ImportedGames');
        $this->autoRender = false;
        $getgame = $this->Games->find('all', array('conditions' => array('state' => $this->request->data['ids'])));
        if (isset($getgame) && !empty($getgame)) {
            foreach ($getgame as $data) {
                $ready = $this->ImportedGames->find('first', array('conditions' => array('state' => $_POST['ids'], 'game_no' => $data["Games"]["game_no"], 'store' => $this->Session->read('stores_id'), 'company_id' => $this->Session->read('Auth.User.company_id'))));

                echo '<tr>
                                                   <td><input type="checkbox" name="import" id="import" value="' . $data["Games"]["id"] . '"';
                if (isset($ready['ImportedGames']['id']) && !empty($ready)) {
                    echo 'disabled';
                }
                echo '/>&nbsp;</td>
                                                      
                                                      <td>' . $data["Games"]["game_no"] . '</td>
                                                    <td>' . $data["Games"]["game_name"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["value"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["tickets_pack"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["start_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["end_ticket"] . '&nbsp;</td>';



                if ($ready['ImportedGames']['id']) {

                    echo '<td><button class="btn default btn-xs red-stripe">Ready to sale</button></td>';
                } else {
                    echo '<td><button class="btn warning">Not Ready Yet</button></td>';
                }
                echo '</tr>';
            }
        } else {

            echo ' <tr>
                                                <td colspan="10">  No games found </td>
                                            </tr>';
        }
    }

    public function admin_getgames() {
        $this->loadmodel('Games');
        $this->autoRender = false;
        $getgame = $this->Games->find('all', array('conditions' => array('state' => $this->request->data['ids'])));
        if (isset($getgame) && !empty($getgame)) {

            foreach ($getgame as $data) {
                echo '<tr>
                                                      
                                                      <td>' . $data["Games"]["game_no"] . '</td>
                                                    <td>' . $data["Games"]["game_name"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["value"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["tickets_pack"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["start_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["Games"]["end_ticket"] . '&nbsp;</td>
													<td>
													<a href="' . Router::url('/') . 'admin/lottery/edit_game/' . $data["Games"]["id"] . '" class="btn default btn-xs red-stripe">Edit</a> <button class="btn default btn-xs red-stripe" id="' . $data["Games"]["id"] . '" onclick = "removegame(this.id);">Delete</button></td>
                                                </tr>';
            }
        } else {

            echo ' <tr>
                                                <td colspan="10">  No games found </td>
                                            </tr>';
        }
    }

    public function admin_games() {
        $this->loadmodel('ImportedGames');
        $this->set('listofgames', 'active');
        $games = $this->ImportedGames->find('all', array('conditions' => array('store' => $this->Session->read('stores_id'), 'status' => 'Ready to sale', 'company_id' => $this->Session->read('Auth.User.company_id'))));
        $this->set('Games', $games);
    }

    public function admin_delete_game($id, $gid) {
        $this->loadmodel('Games');
        $this->loadmodel('ImportedGames');

        if ($this->ImportedGames->delete($gid)) {

            $this->Session->setFlash(__('Imported games has been deleted.'));
            return $this->redirect(array('action' => 'games'));
        }
    }

    function admin_delete_cpack($id) {
        $this->loadmodel('GamePacks');
        if ($this->GamePacks->delete($id)) {

            $this->Session->setFlash(__('Confirmed Pack  has been deleted.'));
            return $this->redirect(array('action' => 'confirm_pack'));
        }
    }

    function admin_delete_apack($id) {
        $this->loadmodel('GamePacks');
        if ($this->GamePacks->updateAll(array('status' => "'confirm'"), array('id' => $id))) {
            $this->Session->setFlash(__('Activated Pack  has been deleted.'));
            return $this->redirect(array('action' => 'activate_pack'));
        }
    }

    public function admin_edit_game($id) {
        $this->loadmodel('ImportedGames');

        if ($this->request->is('post')) {

            $this->request->data['ImportedGames']['id'] = $id;

            if ($this->ImportedGames->save($this->request->data)) {
                $this->Session->setFlash(__('Updated successfully.'), 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'games'));
            } else {
                $this->Session->setFlash(__('Some error occurred.please try again.'), 'default', array('class' => 'error'));
                return $this->redirect(array('action' => 'games'));
            }
        } else {
            $this->set('listofgames', 'active');
            $getgame = $this->ImportedGames->find('first', array('conditions' => array('id' => $id)));
            //echo '<pre>';print_r($getgame);die;
            $this->set('editgame', $getgame);
        }
    }

    public function admin_new_game() {
        pr($this->request->data);
        $this->loadmodel('ImportedGames');

        if ($this->request->is('post')) {
            $this->ImportedGames->create();
            $this->request->data['ImportedGames']['value'] = $this->request->data['ImportedGames']['value'];
            $this->request->data['ImportedGames']['status'] = 'Ready to sale';
            $this->request->data['ImportedGames']['store'] = $this->Session->read('stores_id');
            $this->request->data['ImportedGames']['company_id'] = $this->Session->read('Auth.User.company_id');
            $this->request->data['ImportedGames']['created_at'] = date('Y-m-d');
            $this->request->data['ImportedGames']['updated_at'] = date('Y-m-d');

            if ($this->ImportedGames->save($this->request->data)) {
                $this->Flash->success(__('The game has been saved.'));
                return $this->redirect(array('action' => 'games'));
            } else {
                $this->Flash->error(__('Error game has been saved.'));
                $this->set('listofgames', 'active');
            }
        }


//        if (isset($_POST['submit'])) {
//            $dat = array('game_no' => $this->request->data('game_no'), 'status' => 'Ready to sale', 'store' => $this->Session->read('stores_id'), 'game_name' => $this->request->data('game_name'), 'value' => $this->request->data('value'), 'tickets_pack' => $this->request->data('tickets_pack'), 'state' => $this->request->data('state'), 'start_ticket' => $this->request->data('start_ticket'), 'company_id' => $this->Session->read('Auth.User.company_id'), 'end_ticket' => $this->request->data('end_ticket'), 'craeted_at' => date('Y-m-d'));
//            $this->ImportedGames->create();
////	$dat = array('game_no'=>$_POST['game_no'],'game_name'=>$_POST['corporation'],'state'=>$_POST['state'],'store'=>$_POST['store'],'game_name'=>$getgame['Games']['game_name'],'value'=>$getgame['Games']['value'],'tickets_pack'=>$getgame['Games']['tickets_pack'],'start_ticket'=>$getgame['Games']['start_ticket'],'end_ticket'=>$getgame['Games']['end_ticket'],'game_no'=>$getgame['Games']['game_no'],'created_at'=>date('Y-m-d'));	
//
//            if ($this->ImportedGames->save($dat)) {
//                $this->Session->setFlash(__('Inserted successfully.'));
//                return $this->redirect(array('action' => 'games'));
//            }
//        } else {
//            $this->set('listofgames', 'active');
//        }
    }

    public function admin_confirm_pack() {

        $this->set('confirmpack', 'active');
        $this->loadmodel('ImportedGames');
        $this->loadmodel('GamePacks');

        $games = $this->ImportedGames->find('list', array('conditions' => array('status' => 'Ready to sale', 'store' => $this->Session->read('stores_id')), 'company_id' => $this->Session->read('Auth.User.company_id'), 'fields' => array('game_no', 'game_no'), 'group' => 'game_no'));

        $this->set('games', $games);

        if ($this->request->is('post')) {


            $gamedata = $this->ImportedGames->find('first', array('conditions' => array('status' => 'Ready to sale', 'game_no' => $this->request->data['ImportedGames']['game_no'], 'company_id' => $this->Session->read('Auth.User.company_id'), 'store' => $this->Session->read('stores_id'))));


            $valueas = $gamedata['ImportedGames']['value'];
            /* if (substr($gamedata['ImportedGames']['value'], 0, 1) == '$') {
              $valueas = substr($gamedata['ImportedGames']['value'], 1);
              } */
            $face_value = $valueas * $gamedata['ImportedGames']['tickets_pack'];

            $profit = (5 * $face_value) / 100;


            $valueas = $gamedata['ImportedGames']['tickets_pack'];


            $net_value = $face_value - $profit;
            if ($this->request->data['ImportedGames']['packdate'] != "") {
                $newDate = date("Y-m-d", strtotime($this->request->data['ImportedGames']['packdate']));
            } else {
                $newDate = "";
            }

            $dat = array('game_no' => $this->request->data['ImportedGames']['game_no'],
                'gamename' => $gamedata['ImportedGames']['game_name'],
                'ticket_value' => $gamedata['ImportedGames']['value'],
                'ticket_per_pack' => $gamedata['ImportedGames']['tickets_pack'],
                'start_tkt' => $gamedata['ImportedGames']['start_ticket'],
                'end_tkt' => $gamedata['ImportedGames']['end_ticket'],
                'net_value' => $net_value, 'face_value' => $face_value,
                'scan_ticket_code' => $this->request->data['ImportedGames']['scan_ticket_code'],
                'pack_no' => $this->request->data['ImportedGames']['pack_no'],
                'packdate' => $newDate,
                'available' => $gamedata['ImportedGames']['tickets_pack'],
                'company_id' => $this->Session->read('Auth.User.company_id'),
                'cdate' => date('Y-m-d'),
                'status' => 'confirm',
                'confirm_date_time' => date('Y-m-d H:i:s'),
                'store_id' => $this->Session->read('stores_id'));
            $datav = array('game_no' => $this->request->data['ImportedGames']['game_no'], 'pack_no' => $this->request->data['ImportedGames']['pack_no'], 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'));

            $games = $this->GamePacks->find('first', array('conditions' => $datav));



            if ($games['GamePacks']['pack_no'] != "") {
                $this->Session->setFlash(__('This pack no ' . $this->request->data['ImportedGames']['pack_no'] . 'existed already'));
            } else {

                if ($this->GamePacks->save($dat)) {
                    $this->Session->setFlash(__('Confirm pack added successfully.'));
                } else {
                    $this->Session->setFlash(__('Pack not confirmed, there was something wrong.'));
                }
            }
        }

        $confirm_packs = $this->GamePacks->find('all', array('conditions' => array('status' => 'confirm', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'))));
        $this->set('confirm_packs', $confirm_packs);

        $active_packs = $this->GamePacks->find('all', array('conditions' => array('status' => 'active', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'))));
        $this->set('active_packs', $active_packs);
    }

    public function admin_activate_pack() {

        $this->set('activatepack', 'active');
        $this->loadmodel('GamePacks');

        $games = $this->GamePacks->find('list', array('conditions' => array('status' => 'confirm', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id')), 'fields' => array('game_no', 'game_no'), 'group' => 'game_no'));

        $this->set('games', $games);
        //pr($games);
        if ($this->request->is('post')) {
            $binno;
            if ($this->request->data['GamePacks']['bin_no'] != "") {
                $binno = $this->request->data['GamePacks']['bin_no'];
            }

            $upd = array('activate_date_time' => "'" . date("Y-m-d H:i:s") . "'", 'status' => '"active"', 'bin_no' => $binno, 'ticket_order' => $this->request->data['GamePacks']['ticket_order']);

            if ($this->GamePacks->updateAll($upd, array('game_no' => $this->request->data['GamePacks']['game_no'], 'pack_no' => $this->request->data['GamePacks']['pack_no']))) {

                $this->Session->setFlash(__('Activated successfully.'));
            }
        }

        $active_packs = $this->GamePacks->find('all', array('conditions' => array('status' => 'active', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'))));
        $this->set('active_packs', $active_packs);

        $confirm_packs = $this->GamePacks->find('all', array('conditions' => array('status' => 'confirm', 'store_id' => $this->Session->read('stores_id'))));
        $this->set('confirm_packs', $confirm_packs);
    }

    public function admin_finish_daily_report() {
//        $this->loadmodel('GamePacks');
//        $olddata = array('store_id' => $this->Session->read('stores_id'), 'company_id' => $this->Session->read('Auth.User.company_id'));
//        $oldgames = $this->GamePacks->find('first', array('conditions' => $olddata));
//
//        $next = $oldgames['GamePacks']['next_sold'];
//        $newnext = date('Y-m-d', strtotime($next . ' + 1 days'));
//
//        $this->GamePacks->updateAll(array('next_sold' => $newnext), $olddat);
//        $this->GamePacks->updateAll(array('available' => $availa), $olddat);
//        $this->Session->setFlash(__('Daily reading finished successfully.'));
        return $this->redirect(array('action' => 'daily_reading'));
    }

    public function admin_getpacks($g = null) {
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->autoRender = false;
        $dat = array('game_no' => $this->request->data('game_no'), 'status' => 'confirm', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'));

        $games = $this->GamePacks->find('all', array('conditions' => $dat));
        //	print_r($games);
//        echo '<option value="" selected></option>';
//        foreach ($games as $games) {
//            echo '<option value="' . $games['GamePacks']['pack_no'] . '">' . $games['GamePacks']['pack_no'] . '</option>';
//        }
        //$this->request->data('game_no')
        $dat = array('game_no' => $this->request->data('game_no'), 'company_id' => $this->Session->read('Auth.User.company_id'), 'store' => $this->Session->read('stores_id'));
        $ticket_limits = $this->ImportedGames->find('first', array(
            'fields' => array('start_ticket', 'end_ticket'),
            'conditions' => $dat,
        ));


        $data = array('game_packs' => $games, 'ticket_limits' => $ticket_limits);
        echo json_encode($data);
        //pr($data);
    }

    public function admin_getticketorder($g = null) {
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->autoRender = false; //$this->request->data('game_no')
        $dat = array('game_no' => $this->request->data('game_no'), 'status' => 'confirm', 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'));
        $ticket_order = $this->GamePacks->find('all', array(
            'fields' => array('ticket_order'),
            'conditions' => $dat
        ));

        $dat = array('game_no' => $this->request->data('game_no'), 'company_id' => $this->Session->read('Auth.User.company_id'), 'store' => $this->Session->read('stores_id'));
        $ticket_limits = $this->ImportedGames->find('first', array(
            'fields' => array('start_ticket', 'end_ticket'),
            'conditions' => $dat
        ));


        //pr($ticket_order[0]['GamePacks']['ticket_order']);
        //echo $ticket_order[0]['GamePacks']['ticket_order'];
        $data = array('ticket_order' => $ticket_order[0]['GamePacks']['ticket_order'], 'ticket_limits' => $ticket_limits);
        echo json_encode($data);
        //pr($data);
    }

    public function admin_getpacksactive($no = null) {
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->loadmodel('LotteryDailyReading');
        $this->autoRender = false;
        //echo $this->request->data('game_no');die;
        $date = $this->request->data('date');

        $datetime = new DateTime();
        $newDate = $datetime->createFromFormat('m-d-Y', $date);
        $save_date = $newDate->format('Y-m-d');


        //echo $save_date;die;
        $dat = array('bin_no' => $this->request->data('bin_no'), //array('game_no' => $this->request->data('game_no'),
            'status' => 'active',
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store_id' => $this->Session->read('stores_id'));

        $games = $this->GamePacks->find('all', array('conditions' => $dat));
        $game_no = $games[0]['GamePacks']['game_no'];


        $dat = array('game_no' => $game_no, //$this->request->data('game_no'),//array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store' => $this->Session->read('stores_id'));

        $ticket_data = $this->ImportedGames->find('first', array(
            'fields' => array('start_ticket', 'end_ticket', 'tickets_pack', 'game_name'),
            'conditions' => $dat,
        ));


        $dat = array('game_no' => $game_no, //array('game_no' => $this->request->data('game_no'),
            'company_id' => $this->Session->read('Auth.User.company_id'),
            'store_id' => $this->Session->read('stores_id'),
                //'created' => $save_date
        );

        //echo $game_no;die;
        $prev_ticket = $this->LotteryDailyReading->find('all', array(
            'conditions' => $dat,
            'fields' => array('today_reading'),
            'order' => 'LotteryDailyReading.id desc'
        ));
        //echo json_encode($prev_ticket);die;
        //echo "<pre>",print_r($prev_ticket),"</pre>";
        $result = array(
            'games' => $games,
            'ticket_data' => $ticket_data,
            'game_no' => $game_no,
            'prev_ticket' => $prev_ticket[0]['LotteryDailyReading']['today_reading']
        );
        echo json_encode($result);
        //echo "test";
    }

    public function admin_getgamedata() {
        $this->loadmodel('GamePacks');
        $this->loadmodel('ImportedGames');
        $this->autoRender = false;
        $dat = array('scan_ticket_code' => $this->request->data('scan'), 'company_id' => $this->Session->read('Auth.User.company_id'), 'store_id' => $this->Session->read('stores_id'));

        $games = $this->GamePacks->find('first', array('conditions' => $dat));
        $result['gno'] = $games['GamePacks']['game_no'];
        $result['pack'] = $games['GamePacks']['pack_no'];

        $olddat = array('game_no' => $games['GamePacks']['game_no'], 'store' => $this->Session->read('stores_id'));

        $oldgames = $this->ImportedGames->find('first', array('conditions' => $olddat));
        $result['prev'] = $oldgames['ImportedGames']['available'];


        return json_encode($result);
    }

    public function admin_reports() {
        $this->Setredirect();
        $this->loadModel('RubyDailyreporting');

        //pr($this->request->data);


        $fields = array('reporting_date', 'Net_Online_Sales', 'lotto_Net_online_sales', 'Net_online_cashes', 'Scratch_off_cashes', 'Settlement',
            'Adjustment', 'OScratch_off_credit', 'Online_credit', 'Lotto_Commission', 'Lotto_Balance');

        $conditions = array('RubyDailyreporting.store_id' => $this->Session->read('stores_id'));

        if (isset($this->request->data) && !empty($this->request->data)) {

            $full_dates = $this->request->data['RubyDailyreporting']['reporting_date'];
            $dates = explode(' - ', $this->request->data['RubyDailyreporting']['reporting_date']);


            $d1 = $dates[0];
            $d2 = $dates[1];
            $date1 = new DateTime($d1);
            $start_date = $date1->format('Y-m-d');

            $date2 = new DateTime($d2);
            $end_date = $date2->format('Y-m-d');

            $formatedDate = array($start_date, $end_date);

            if ($this->request->data['RubyDailyreporting']['reporting_date'] != "") {
                $this->Session->write('PostSearch', 'PostSearch');
                //$this->Session->write('update_date', $this->request->data['Lottery']['update_date']);
                $this->Session->write('full_dates', $full_dates);
                $this->Session->write('start_date', $start_date);
                $this->Session->write('end_date', $end_date);
            } else {
                $this->Session->delete('PostSearch');
                $this->Session->delete('full_dates');
                $this->Session->delete('start_date');
                $this->Session->delete('end_date');
            }

            if ($this->Session->check('PostSearch')) {
                $PostSearch = $this->Session->read('PostSearch');
                //$update_date = $this->Session->read('update_date');
                $formatedDate = $this->Session->read('full_dates');
                $start_date = $this->Session->read('start_date');
                $end_date = $this->Session->read('end_date');
            }

            if (isset($start_date) && $start_date != '') {
                $conditions[] = array(
                    'RubyDailyreporting.reporting_date >=' => $start_date,
                );
            }
            if (isset($end_date) && $end_date != '') {
                $conditions[] = array(
                    'RubyDailyreporting.reporting_date <=' => $end_date,
                );
            }
            //pr($formatedDate);
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'RubyDailyreporting.id' => 'desc'
                ),
                'fields' => $fields
            );
            $reportings = $this->Paginator->paginate('RubyDailyreporting');
            $this->set('reportings', $reportings);
            $this->set('full_dates', $full_dates);
            //pr($reportings);
        } else if ($this->Session->check('PostSearch')) {
            $PostSearch = $this->Session->read('PostSearch');
            //$update_date = $this->Session->read('update_date');
            $formatedDate = $this->Session->read('full_dates');

            //pr($formatedDate);
            if (isset($start_date) && $start_date != '') {
                $conditions[] = array(
                    'RubyDailyreporting.reporting_date >=' => $start_date,
                );
            }
            if (isset($end_date) && $end_date != '') {
                $conditions[] = array(
                    'RubyDailyreporting.reporting_date <=' => $end_date,
                );
            }
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'RubyDailyreporting.id' => 'desc'
                ),
                'fields' => $fields
            );
            $reportings = $this->Paginator->paginate('RubyDailyreporting');
            $this->set('reportings', $reportings);
            $this->set('full_dates', $full_dates);
        } else {
            $this->set('full_dates', $full_dates);
            $this->set('reportings', '');
        }
    }

    public function admin_reset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_dates');
        $this->Session->delete('start_date');
        $this->Session->delete('end_date');
        $redirect_url = array('controller' => 'lottery', 'action' => 'reports');
        return $this->redirect($redirect_url);
    }

    public function admin_daily_reading() {

        $this->Setredirect();
        $this->loadmodel('Lottery');
        $full_date = '';
        $form_date = '';
        $to_date = '';
        if ($this->request->is('post')) {
            //echo '<pre>';print_r($this->request->data);die;       
            $date = array();
            $formarr = array();
            $toarr = array();
            if ($this->request->data['Lottery']['full_date'] != "") {
                //echo 'ok';die;
                $full_date = $this->request->data['Lottery']['full_date'];
                if (isset($full_date) && $full_date != '') {
                    $date = explode(' - ', $full_date);
                    $form_date = $date[0];
                    $formarr = explode('/', $form_date);
                    $fdate = $formarr[2] . "-" . $formarr[0] . "-" . $formarr[1];

                    $to_date = $date[1];
                    $toarr = explode('/', $to_date);
                    $tdate = $toarr[2] . "-" . $toarr[0] . "-" . $toarr[1];
                }
                $this->Session->write('PostSearch', 'PostSearch');
                $this->Session->write('full_date', $full_date);
                $this->Session->write('from_date', $fdate);
                $this->Session->write('to_date', $tdate);
            } else {
                //echo 'notok';die;
                $this->Session->delete('PostSearch');
                $this->Session->delete('full_date');
                $this->Session->delete('from_date');
                $this->Session->delete('to_date');
            }

            if ($this->Session->check('PostSearch')) {
                //echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $full_date = $this->Session->read('full_date');
                $from_date = $this->Session->read('from_date');
                $to_date = $this->Session->read('to_date');
            }
            $conditions = array('Lottery.store_id' => $this->Session->read('stores_id'));

            if (isset($from_date) && $from_date != '') {
                $conditions[] = array(
                    'Lottery.updated >=' => $from_date,
                );
            }
            if (isset($to_date) && $to_date != '') {
                $conditions[] = array(
                    'Lottery.updated <=' => $to_date,
                );
            }
            //echo '<pre>';print_r($conditions);die;
            if ($this->request->data['Lottery']['full_date'] != "") {
                $this->paginate = array('conditions' => $conditions,
                    'order' => array(
                        'Lottery.id' => 'desc'
                    ),
                    'fields' =>array('Lottery.updated','SUM(Lottery.today_sold) as  today_sold','SUM(Lottery.today_sold_value) as today_sold_value'),
                    'group' => array('Lottery.updated')
                );

                //echo '<pre>';print_r($this->Paginator->paginate());die;
                $this->set('full_date', $full_date);
                $this->set('DailyReading', $this->Paginator->paginate());
            } else {
                $this->set('full_date', $full_date);
                $this->set('DailyReading', '');
            }
        } else {
            $this->set('full_date', $full_date);
            $this->set('DailyReading', '');
        }
    }

    public function admin_dreset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('from_date');
        $this->Session->delete('to_date');
        $redirect_url = array('controller' => 'lottery', 'action' => 'daily_reading');
        return $this->redirect($redirect_url);
    }

    public function admin_lotto_settlements() {

        $this->set('lottosettlements', 'active');
    }

    public function admin_pack_history() {

        $this->set('packhistory', 'active');
    }

    public function admin_return_pack() {

        $this->set('returnpack', 'active');
    }

    public function admin_settle_pack() {

        $this->set('settlepack', 'active');
    }

}
