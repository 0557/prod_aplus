<?php
App::uses('AppController', 'Controller');
/**
 * Managers Controller
 *
 * @property Manager $Manager
 */
class ManagersController extends AppController {
public $uses=array("Manager","SitePermission");

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	   if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'managers','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            $this->Manager->recursive = 0;
		$limit=(isset($this->params['named']['showperpage']))?$this->params['named']['showperpage']:Configure::read('site.admin_paging_limit');
		$conditions=array();		
		if(isset($this->params['named']['keyword']) && $this->params['named']['keyword']!=''){
		  $conditions['Manager.name LIKE ']='%'.$this->params['named']['keyword'].'%';
		}
		  
		if(!empty($this->request->data)){
		  if(isset($this->request->data['showperpage']) && $this->request->data['showperpage']!=''){
		    $limit=$this->request->data['showperpage'];
		    $this->params['named']=array("showperpage"=>$limit);
		  }
		  if(isset($this->request->data['keyword']) && $this->request->data['keyword']!=''){
		    $this->params['named']=array("keyword"=>$this->request->data['keyword']);
			$conditions['Manager.name LIKE ']='%'.$this->request->data['keyword'].'%';
		  }
		}
		
		
		$this->paginate=array("conditions"=>$conditions,"limit"=>$limit,"order"=>"Manager.created DESC");
		
		
		$this->set('managers', $this->paginate());
		$this->set(compact('limit'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	  if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'managers','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if (!$this->Manager->exists($id)) {
			throw new NotFoundException(__('Invalid manager'));
		}
		$options = array('conditions' => array('Manager.' . $this->Manager->primaryKey => $id));
		$this->set('manager', $this->Manager->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'managers','is_add')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if ($this->request->is('post')) {
			$this->Manager->create();
			if ($this->Manager->save($this->request->data)) {
				$this->Session->setFlash(__('The manager has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manager could not be saved. Please, try again.'));
			}
		}
		
			$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'managers','is_edit')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if (!$this->Manager->exists($id)) {
			throw new NotFoundException(__('Invalid manager'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Manager->save($this->request->data)) {
				$this->Session->setFlash(__('The manager has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manager could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Manager.' . $this->Manager->primaryKey => $id));
			$this->request->data = $this->Manager->find('first', $options);
		}
			$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'managers','is_delete')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            $this->Manager->id = $id;
		if (!$this->Manager->exists()) {
			throw new NotFoundException(__('Invalid manager'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Manager->delete()) {
			$this->Session->setFlash(__('Manager deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Manager was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
