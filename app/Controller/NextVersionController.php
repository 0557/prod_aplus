<?php
App::uses('AppController', 'Controller');
/**
 * RubyUprodts Controller
 *
 * @property RubyUprodt $RubyUprodt
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class NextVersionController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $uses = array('RubyUprodt','Corporation','Store','RubyHeader');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {

	}
}
