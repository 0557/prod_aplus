<?php

ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class NotificationmailController extends AppController {
	
public function __beforeFilter() {
        parent::beforeFilter();
       $this->Auth->allow('index');
    }

			 public function index() {
				 
		  $this->layout = 'blank';
		  
			$this->loadModel('ComplianceNotification');

			
		  	$notification = $this->ComplianceNotification->find('all');
			
			foreach($notification as $notification){
				
				$Date = $notification['ComplianceNotification']['expiry_date'];
				$days = $notification['ComplianceNotification']['notification_days'];
				$notification['ComplianceNotification']['store_emails'];
				$notification['ComplianceNotification']['message'];
				$notification['ComplianceNotification']['store_name'];
				
				$expiry =  date('Y-m-d', strtotime($Date. ' - '.$days.' days'));
				
				 $today = date('Y-m-d');
				if($today == $expiry){
			
				
					$from = $notification['ComplianceNotification']['sender_email'];
					$to = $notification['ComplianceNotification']['store_emails'];
					$subject = $notification['ComplianceNotification']['subject'];
					$message = "<html><body><table width='100%'; rules='all' cellpadding='10'><tr><td style='background:green;text-align:left'><img src='http://aplus1.net/backoffice/img/logo-main.png' alt='PHP Gang' /></td></tr><tr><td colspan=2>OBJECT- REMINDER TO RENEW STORE LICENSE<br /><br />Dear Sir,<br/><p style='margin-left:25px;'>".$notification['ComplianceNotification']['message']."</p>Store Name: ".$notification['ComplianceNotification']['store_name']."<br/>License Name: ".$notification['ComplianceNotification']['license_name']."<br/>Expiry Date: ".$notification['ComplianceNotification']['expiry_date']."<br/><br/><br/><b>Your Sincerely</b><br/>Aplus1 Team<br/>Phone - 856-343-4720<br/>Email id - support@aplus1.net</td></tr></table></body></html>";
					
						$Email = new CakeEmail();
						$Email->from(array($from  => 'Aplus1.net'));
						$Email->to($to);
						$Email->subject($subject);
						$Email->emailFormat('html');
						$Email->send($message);
					
						if($notification['ComplianceNotification']['repeat_year']=='Y'){
										
						$newdate = strtotime ( '+1 year' , strtotime ( $Date ) ) ;
						$expiry = date ( 'Y-m-d' , $newdate );
						$expiry = "'".$expiry."'";

						$this->ComplianceNotification->updateAll(array('expiry_date'=>$expiry) , array('id' => $notification['ComplianceNotification']['id']));
				}
				
				}
				
			}

	}

}
