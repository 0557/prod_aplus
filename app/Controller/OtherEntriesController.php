<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class OtherEntriesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function admin_settings() {
		$this->Setredirect();
		$this->loadModel('OtherCategories');
		$this->loadModel('OtherSources');
		$this->loadModel('OtherWithdraw');
		$this->loadModel('OtherChargeaccount');
		$this->loadModel('OtherAccount');
		$cat = $this->OtherCategories->find('list',array('conditions'=>array('OtherCategories.company_id' => $this->Session->read('Auth.User.company_id'),'OtherCategories.store_id' => $this->Session->read('stores_id')),'fields'=>array('category','category')));
		$conditions = array('OtherCategories.company_id' => $this->Session->read('Auth.User.company_id'),'OtherCategories.store_id' => $this->Session->read('stores_id'));
		$catlist = $this->OtherCategories->find('all',array('conditions'=>$conditions,'order'=>array('id'=>'desc')));
		$this->set('allcats',$catlist);
		$this->set('catelist',$cat);
		
		$sourccat = $this->OtherSources->find('list',array('conditions'=>array('OtherSources.company_id' => $this->Session->read('Auth.User.company_id'),'OtherSources.store_id' => $this->Session->read('stores_id')),'fields'=>array('source','source')));
		
		$this->set('sourcelistitems',$sourccat);
		
		$sourcelist = $this->OtherSources->find('all',array('conditions'=>array('OtherSources.company_id' => $this->Session->read('Auth.User.company_id'),'OtherSources.store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));
		$this->set('sourcelist',$sourcelist);
		
		$withdrawlistitems = $this->OtherWithdraw->find('list',array('conditions'=>array('OtherWithdraw.company_id' => $this->Session->read('Auth.User.company_id'),'OtherWithdraw.store_id' => $this->Session->read('stores_id')),'fields'=>array('withdraw','withdraw')));
		
		$this->set('withdrawlistitems',$withdrawlistitems);
		
		$withdrawlist = $this->OtherWithdraw->find('all',array('conditions'=>array('OtherWithdraw.company_id' => $this->Session->read('Auth.User.company_id'),'OtherWithdraw.store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));
		$this->set('withdrawlist',$withdrawlist);
		
		$accountlistitems = $this->OtherAccount->find('list',array('conditions'=>array('OtherAccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherAccount.store_id' => $this->Session->read('stores_id')),'fields'=>array('account','account')));
		
		$this->set('accountlistitems',$accountlistitems);
		
		$accountlist = $this->OtherAccount->find('all',array('conditions'=>array('OtherAccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherAccount.store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));
		$this->set('accountlist',$accountlist);
		
		$chaccountlistitems = $this->OtherChargeaccount->find('list',array('conditions'=>array('OtherChargeaccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherChargeaccount.store_id' => $this->Session->read('stores_id')),'fields'=>array('account','account')));
		
		$this->set('chaccountlistitems',$chaccountlistitems);
		
		$chaccountlist = $this->OtherChargeaccount->find('all',array('conditions'=>array('OtherChargeaccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherChargeaccount.store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));
		$this->set('chaccountlist',$chaccountlist);
		
	}
	public function admin_newcategory() {
		$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherCategories');
	
//		echo $this->request->data['OtherEntries']['category'];exit;

$this->request->data['OtherCategories']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['OtherCategories']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['OtherCategories']['created_at'] = date('y-m-d');
			
			
		$this->request->data['OtherCategories']['category'] = $this->request->data['cat'];
		if($this->OtherCategories->save($this->data)){
			$inid = $this->OtherCategories->getInsertID();
				echo '<tr>';
						echo '<td>'.$this->request->data['cat'].'</td>';
						echo '<td>active</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $inid;?>,'OtherCategories','catde');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						<?php 
						echo '</td>';
						echo '</tr>';
			$this->Session->setFlash(__('Submited new data successfully.'));
		}
	} 

	function admin_filtercategories(){
			$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherCategories');
		
		$conditions = array('OtherCategories.company_id' => $this->Session->read('Auth.User.company_id'),'OtherCategories.store_id' => $this->Session->read('stores_id'));
		
		if(isset($this->request->data['cat'])){
			$conditions['OtherCategories.category'] = $this->request->data['cat'];
		}
		if(isset($this->request->data['statuscat'])){
			$conditions['OtherCategories.status'] = $this->request->data['statuscat'];
		}
		
		$catlist = $this->OtherCategories->find('all',array('conditions'=>array_filter($conditions),'order'=>array('id'=>'desc')));
		 if(isset($catlist) && !empty($catlist)){
						foreach($catlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherCategories']['category'].'</td>';
						echo '<td>'.$catn['OtherCategories']['status'].'</td>';
					echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherCategories']['id'];?>,'OtherCategories','catde');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} 
	}
	
	public function admin_newsource() {
		$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherSources');
	
//		echo $this->request->data['OtherEntries']['category'];exit;

$this->request->data['OtherSources']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['OtherSources']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['OtherSources']['created_at'] = date('y-m-d');
			
			
		$this->request->data['OtherSources']['source'] = $this->request->data['cat'];
		if($this->OtherSources->save($this->data)){
			$inid = $this->OtherChargeaccount->getLastInsertId();
				echo '<tr>';
						echo '<td>'.$this->request->data['cat'].'</td>';
						echo '<td>active</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $inid;?>,'OtherSources','sourcedeta');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
			$this->Session->setFlash(__('Submited new data successfully.'));
		}
	}
	function admin_filtercsource(){
			$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherSources');
		
		$conditions = array('OtherSources.company_id' => $this->Session->read('Auth.User.company_id'),'OtherSources.store_id' => $this->Session->read('stores_id'));
		
		if(isset($this->request->data['sourcename'])){
			$conditions['OtherSources.source'] = $this->request->data['sourcename'];
		}
		if(isset($this->request->data['statusinc'])){
			$conditions['OtherSources.status'] = $this->request->data['statusinc'];
		}
		
		$catlist = $this->OtherSources->find('all',array('conditions'=>array_filter($conditions),'order'=>array('id'=>'desc')));
		 if(isset($catlist) && !empty($catlist)){
						foreach($catlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherSources']['source'].'</td>';
						echo '<td>'.$catn['OtherSources']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherSources']['id'];?>,'OtherSources','sourcedeta');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} 
		
	}
public function admin_withdrawer() {
		$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherWithdraw');
	
//		echo $this->request->data['OtherEntries']['category'];exit;

$this->request->data['OtherWithdraw']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['OtherWithdraw']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['OtherWithdraw']['created_at'] = date('y-m-d');
			
			
		$this->request->data['OtherWithdraw']['withdraw'] = $this->request->data['cat'];
		if($this->OtherWithdraw->save($this->data)){
			$inid = $this->OtherChargeaccount->getLastInsertId();
				echo '<tr>';
						echo '<td>'.$this->request->data['cat'].'</td>';
						echo '<td>active</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $inid;?>,'OtherWithdraw','drawerdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
			$this->Session->setFlash(__('Submited new data successfully.'));
		}
	}
	
	function admin_filterwithdrawer(){
			$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherWithdraw');
		
		$conditions = array('OtherWithdraw.company_id' => $this->Session->read('Auth.User.company_id'),'OtherWithdraw.store_id' => $this->Session->read('stores_id'));
		
		if(isset($this->request->data['withdrawfilter'])){
			$conditions['OtherWithdraw.withdraw'] = $this->request->data['withdrawfilter'];
		}
		if(isset($this->request->data['withstatus'])){
			$conditions['OtherWithdraw.status'] = $this->request->data['withstatus'];
		}
		
		$catlist = $this->OtherWithdraw->find('all',array('conditions'=>array_filter($conditions),'order'=>array('id'=>'desc')));
		 if(isset($catlist) && !empty($catlist)){
						foreach($catlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherWithdraw']['withdraw'].'</td>';
						echo '<td>'.$catn['OtherWithdraw']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherWithdraw']['id'];?>,'OtherWithdraw','drawerdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} 
		
	}
	
public function admin_account() {
		$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherAccount');
	
//		echo $this->request->data['OtherEntries']['category'];exit;

$this->request->data['OtherAccount']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['OtherAccount']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['OtherAccount']['created_at'] = date('y-m-d');
			
			
		$this->request->data['OtherAccount']['account'] = $this->request->data['cat'];
		if($this->OtherAccount->save($this->data)){
			$inid = $this->OtherChargeaccount->getLastInsertId();
				echo '<tr>';
						echo '<td>'.$this->request->data['cat'].'</td>';
						echo '<td>active</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $inid;?>,'OtherAccount','accountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
			$this->Session->setFlash(__('Submited new data successfully.'));
		}
	}
	
	function admin_filteraccount(){
			$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherAccount');
		
		$conditions = array('OtherAccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherAccount.store_id' => $this->Session->read('stores_id'));
		
		if(isset($this->request->data['accountsearch'])){
			$conditions['OtherAccount.account'] = $this->request->data['accountsearch'];
		}
		if(isset($this->request->data['accountstatus'])){
			$conditions['OtherAccount.status'] = $this->request->data['accountstatus'];
		}
		
		$catlist = $this->OtherAccount->find('all',array('conditions'=>array_filter($conditions),'order'=>array('id'=>'desc')));
		 if(isset($catlist) && !empty($catlist)){
						foreach($catlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherAccount']['account'].'</td>';
						echo '<td>'.$catn['OtherAccount']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherAccount']['id'];?>,'OtherAccount','accountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} 
		
	}
	
	
public function admin_chaccount() {
		$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherChargeaccount');
	
//		echo $this->request->data['OtherEntries']['category'];exit;

$this->request->data['OtherChargeaccount']['company_id'] =  $this->Session->read('Auth.User.company_id');
			$this->request->data['OtherChargeaccount']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['OtherChargeaccount']['created_at'] = date('y-m-d');
			
			
		$this->request->data['OtherChargeaccount']['account'] = $this->request->data['cat'];
		$this->request->data['OtherChargeaccount']['description'] = $this->request->data['Description'];
		if($this->OtherChargeaccount->save($this->data)){
			$inid = $this->OtherChargeaccount->getLastInsertId();
				echo '<tr>';
						echo '<td>'.$this->request->data['cat'].'</td>';
						echo '<td>'.$this->request->data['Description'].'</td>';
						echo '<td>active</td>';
						echo '<td>';
						?>
						<span class="newicon" onclick = "deletefunction(<?php echo $inid;?>,'OtherChargeaccount','chaccountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
								
							
								
								<?php echo '</td>';
						echo '</tr>';
			$this->Session->setFlash(__('Submited new data successfully.'));
		}
	}
	
	function admin_chfilteraccount(){
			$this->Setredirect();
		$this->autoRender = false;
		$this->loadModel('OtherChargeaccount');
		
		$conditions = array('OtherChargeaccount.company_id' => $this->Session->read('Auth.User.company_id'),'OtherChargeaccount.store_id' => $this->Session->read('stores_id'));
		
		if(isset($this->request->data['accountsearch'])){
			$conditions['OtherChargeaccount.account'] = $this->request->data['accountsearch'];
		}
		if(isset($this->request->data['accountstatus'])){
			$conditions['OtherChargeaccount.status'] = $this->request->data['accountstatus'];
		}
		
		$catlist = $this->OtherChargeaccount->find('all',array('conditions'=>array_filter($conditions),'order'=>array('id'=>'desc')));
		 if(isset($catlist) && !empty($catlist)){
						foreach($catlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherChargeaccount']['account'].'</td>';
						echo '<td>'.$catn['OtherChargeaccount']['description'].'</td>';
						echo '<td>'.$catn['OtherChargeaccount']['status'].'</td>';
						echo '<td>';
						?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherChargeaccount']['id'];?>,'OtherChargeaccount','chaccountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
								
								
								
								<?php echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} 
		
	}
	
	function admin_deleterecord(){
		$this->Setredirect();
		$this->autoRender = false;
		
		$model = $this->request->data['model'];
		$id = $this->request->data['id'];
		if($model == 'OtherChargeaccount'){
			$this->loadModel('OtherChargeaccount');
					if($this->OtherChargeaccount->delete($id)){
						
							$sourcelist = $this->OtherChargeaccount->find('all',array('conditions'=>array('company_id' => $this->Session->read('Auth.User.company_id'),'store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));

								if(isset($sourcelist) && !empty($sourcelist)){
								foreach($sourcelist as $catn){
								echo '<tr>';
								echo '<td>'.$catn['OtherChargeaccount']['account'].'</td>';
								echo '<td>'.$catn['OtherChargeaccount']['description'].'</td>';
								echo '<td>'.$catn['OtherChargeaccount']['status'].'</td>';
								echo '<td>';?>
								<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherChargeaccount']['id'];?>,'OtherChargeaccount','chaccountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
								
								
								<?php 
								echo '</td>';
								echo '</tr>';
								}
								
								}
						
						
						}
		}else if($model == 'OtherAccount'){
			
				$this->loadModel('OtherAccount');
					if($this->OtherAccount->delete($id)){
						
							$sourcelist = $this->OtherAccount->find('all',array('conditions'=>array('company_id' => $this->Session->read('Auth.User.company_id'),'store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));

								if(isset($sourcelist) && !empty($sourcelist)){
								foreach($sourcelist as $catn){
								echo '<tr>';
									echo '<td>'.$catn['OtherAccount']['account'].'</td>';
									echo '<td>'.$catn['OtherAccount']['status'].'</td>';
									echo '<td>';?>
									<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherAccount']['id'];?>,'OtherAccount','accountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
									
									
									<?php 
									echo '</td>';
									echo '</tr>';
								}
								
								}
						
						
						}
			
		}else if($model == 'OtherWithdraw'){
			
				$this->loadModel('OtherWithdraw');
					if($this->OtherWithdraw->delete($id)){
						
							$sourcelist = $this->OtherWithdraw->find('all',array('conditions'=>array('company_id' => $this->Session->read('Auth.User.company_id'),'store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));

								if(isset($sourcelist) && !empty($sourcelist)){
								foreach($sourcelist as $catn){
								echo '<tr>';
								echo '<td>'.$catn['OtherWithdraw']['withdraw'].'</td>';
								echo '<td>'.$catn['OtherWithdraw']['status'].'</td>';
								echo '<td>';?>
								<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherWithdraw']['id'];?>,'OtherWithdraw','drawerdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
								
							
								<?php 
								echo '</td>';
								echo '</tr>';
								}
								
								}
						
						
						}
			
		}else if($model == 'OtherSources'){
			
				$this->loadModel('OtherSources');
					if($this->OtherSources->delete($id)){
						
							$sourcelist = $this->OtherSources->find('all',array('conditions'=>array('company_id' => $this->Session->read('Auth.User.company_id'),'store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));

								if(isset($sourcelist) && !empty($sourcelist)){
								foreach($sourcelist as $catn){
								echo '<tr>';
								echo '<td>'.$catn['OtherSources']['source'].'</td>';
								echo '<td>'.$catn['OtherSources']['status'].'</td>';
									echo '<td>';?>
								<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherSources']['id'];?>,'OtherSources','sourcedeta');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
								
								
								<?php 
								echo '</td>';
								echo '</tr>';
								}
								
								}
						
						
						}
			
		}else if($model == 'OtherCategories'){
				$this->loadModel('OtherCategories');
					if($this->OtherCategories->delete($id)){
						
							$sourcelist = $this->OtherCategories->find('all',array('conditions'=>array('company_id' => $this->Session->read('Auth.User.company_id'),'store_id' => $this->Session->read('stores_id')),'order'=>array('id'=>'desc')));

								if(isset($sourcelist) && !empty($sourcelist)){
								foreach($sourcelist as $catn){
									echo '<tr>';
									echo '<td>'.$catn['OtherCategories']['category'].'</td>';
									echo '<td>'.$catn['OtherCategories']['status'].'</td>';
									echo '<td>';?>
									<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherCategories']['id'];?>,'OtherCategories','catde');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
									
									<?php 
									echo '</td>';
									echo '</tr>';
								}
								
								}
						
						
						}
			
		}
	
	}
	
}
