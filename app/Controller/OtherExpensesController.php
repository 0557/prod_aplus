<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class OtherExpensesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
			$this->Setredirect();
		$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherCategories');
			
			$catgryies=$this->OtherCategories->find('list',array('conditions'=>array('OtherCategories.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherCategories.id','OtherCategories.category')));
		
		$this->set('banklist',$banks);
		$this->set('catgryies',$catgryies);
	}
	 public function admin_add(){
	$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherCategories');
			
			$catgryies=$this->OtherCategories->find('list',array('conditions'=>array('OtherCategories.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherCategories.id','OtherCategories.category')));
		
		$this->set('banklist',$banks);
		$this->set('catgryies',$catgryies);
	}
}