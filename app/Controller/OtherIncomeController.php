<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class OtherIncomeController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
			$this->Setredirect();
		$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherSources');
			
			$catgryies=$this->OtherSources->find('list',array('conditions'=>array('OtherSources.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherSources.id','OtherSources.source')));
		
		$this->set('banklist',$banks);
		$this->set('source',$catgryies);
	}
	 public function admin_add(){
	$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherSources');
			
			$catgryies=$this->OtherSources->find('list',array('conditions'=>array('OtherSources.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherSources.id','OtherSources.source')));
		
		$this->set('banklist',$banks);
		$this->set('source',$catgryies);
	}
}