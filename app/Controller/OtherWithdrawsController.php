<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class OtherWithdrawsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
			$this->Setredirect();
		$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherWithdraw');
			
			$catgryies=$this->OtherWithdraw->find('list',array('conditions'=>array('OtherWithdraw.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherWithdraw.id','OtherWithdraw.withdraw')));
		
		$this->set('banklist',$banks);
		$this->set('withdraw',$catgryies);
	}
	 public function admin_add(){
	$this->loadModel('RBankAccount');
			$banks=$this->RBankAccount->find('list',array('conditions'=>array('store' => $this->Session->read('stores_id')),'fields'=>array('id','account_name')));
			
			$this->loadModel('OtherWithdraw');
			
			$catgryies=$this->OtherWithdraw->find('list',array('conditions'=>array('OtherWithdraw.store_id' => $this->Session->read('stores_id')),'fields'=>array('OtherWithdraw.id','OtherWithdraw.withdraw')));
		
		$this->set('banklist',$banks);
		$this->set('withdraw',$catgryies);
	}
}