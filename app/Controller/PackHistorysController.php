<?php
ob_start();
//error_reporting(0);
App::uses('AppController', 'Controller');

class PackHistorysController extends AppController 
{
    public $components = array('Paginator', 'Flash', 'Session');
    
    public function admin_index()
    {
        $this->Setredirect();
        $this->loadmodel('PackHistory');  
        $full_date = '';
		$form_date='';
		$to_date='';
		$status='';
		$conditions = array('PackHistory.store_id' => $this->Session->read('stores_id'),'PackHistory.company_id' => $this->Session->read('Auth.User.company_id')); 
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				
				if($this->request->data['PackHistory']['full_date']!= "" || $this->request->data['PackHistory']['pstatus']!= "")                {	
				$this->Session->write('PostSearch', 'PostSearch');
				
				if($this->request->data['PackHistory']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['PackHistory']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
				
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);
				} else {
			    //echo 'notok';die;
				
				$this->Session->delete('full_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}
				
				
				if($this->request->data['PackHistory']['pstatus']!=''){
				$this->Session->write('status',$this->request->data['PackHistory']['pstatus']);	
		        }else{
		        $this->Session->delete('status');	
				}				
					
				}else{
				$this->Session->delete('PostSearch');
				}
					
			           
        }
		
		 if ($this->Session->check('PostSearch')) {
				//echo 'ok';die;
                $PostSearch = $this->Session->read('PostSearch');
                $full_date=$this->Session->read('full_date');
			    $from_date=$this->Session->read('from_date');
			    $to_date=$this->Session->read('to_date');
				$status=$this->Session->read('status');
            }
             
		   if($status!=''){			   
		        //echo $status;die;	
				$conditions[] = array(
                    'PackHistory.status ' =>$status,
                );			
				
			}
			        	
			if(isset($from_date) && $from_date!=''){				
                $conditions[] = array(
                    'DATE(PackHistory.updated) >=' => $from_date,
                );
			}
			if(isset($to_date) && $to_date!=''){						
				$conditions[] = array(
                    'DATE(PackHistory.updated) <=' => $to_date,
                );		
			}
		    //echo '<pre>';print_r($conditions);die;
			
		
		
	        $this->paginate = array('conditions' => $conditions,
			    'limit' => 100,
                'order' => array(
                    'PackHistory.id' => 'desc'
                )
            );
			$this->set('full_date',$full_date);	
			$this->set('status',$status);	
            $this->set('games',$this->Paginator->paginate()); 
	
	
	}	
	
    
    public function admin_reset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('start_date');
        $this->Session->delete('end_date');
        $this->Session->delete('status');
        $redirect_url = array('controller' => 'pack_historys', 'action' => 'admin_index');
        return $this->redirect($redirect_url);
    }
	
	
	public function admin_getgames(){
		$this->autoRender = false;
		$upadated=$this->request->data['updated'];		
		//echo $upadated; die;		
		//$this->loadModel('PackHistory');
		$games_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'));	
		//,'DATE(PackHistory.updated) >=' =>$upadated,'DATE(PackHistory.updated) <=' =>$upadated	        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $games_conditions));
		//echo '<pre>';print_r($PackHistory); die;	
		echo "<option value=''>Select Game</option>";
		foreach($PackHistory as $pack_history)
		{		
		//$game_nos[$pack_history['PackHistory']['game_no']]=$pack_history['PackHistory']['game_no'];
		 $val=$pack_history['PackHistory']['game_no'];
		 echo "<option value='$val'>$val</option>";
		}
		
	}
	
	public function admin_getpacks(){
		$this->autoRender = false;
		$gameno=$this->request->data['gameno'];		
		$packs_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'),'PackHistory.game_no' =>$gameno);		        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $packs_conditions));
		print_r($PackHistory[0]['PackHistory']['pack_no']);
	}
	
	
	public function admin_savepacks(){
		$this->autoRender = false;
		$updated=$this->request->data['updated'];	
		$game_no=$this->request->data['game_no'];	
		$pack_no=$this->request->data['pack_no'];	
		$status=$this->request->data['status'];		
		$packs_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'),'PackHistory.game_no' =>$game_no,'PackHistory.pack_no' =>$pack_no);		        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $packs_conditions));
		//print_r($PackHistory);die;
		//print_r($PackHistory[0]['PackHistory']['id']);die;
		$pack_id=$PackHistory[0]['PackHistory']['id'];
		if($pack_id!='')
		{
		$pack_data['PackHistory']['id']=$pack_id;
		$pack_data['PackHistory']['status']=$this->request->data['status'];		
		$pack_data['PackHistory']['updated']=$updated;	
		//echo '<pre>';print_r($pack_data);die;
				if ($this->PackHistory->save($pack_data))
				{ 
				echo 'Successfully upadted';
				}else
				{
				echo 'Not upadted';
				}
		}else{
		echo 'Game packs not exists!';	
		}
		
	}
	
	
}
