<?php
ob_start();
//error_reporting(0);
App::uses('AppController', 'Controller');

class PackHistorysController extends AppController 
{
    public $components = array('Paginator', 'Flash', 'Session');
    
    public function admin_index()
    {
        //$this->Setredirect();
        
        $full_date = '';
        $conditions = array('PackHistory.store_id' => $this->Session->read('stores_id'));
        
        if(isset($this->request->data) && !empty($this->request->data))
	{
            //pr($this->request->data);
            $status = $this->request->data['PackHistory']['status'];
            $full_date = $this->request->data['PackHistory']['full_date'];

            $dates = explode(' - ', $this->request->data['PackHistory']['full_date']);


            $d1 = $dates[0];
            $d2 = $dates[1];
            $date1 = new DateTime($d1);
            $start_date = $date1->format('Y-m-d');

            $date2 = new DateTime($d2);
            $date2->modify('+1 day');
            $end_date = $date2->format('Y-m-d');
   
            //echo $start_date . " - " . $end_date . " - " . $status;
            
            if ($status != "") {
                $this->Session->write('status', $status);
            } else {
                $this->Session->delete('status');
            }
            
            if ($this->request->data['PackHistory']['full_date'] != "") {
                $this->Session->write('PostSearch', 'PostSearch');
                //$this->Session->write('update_date', $this->request->data['Lottery']['update_date']);
                $this->Session->write('full_date', $full_date);
                $this->Session->write('start_date', $start_date);
                $this->Session->write('end_date', $end_date);
                $this->Session->write('status', $status);
            } else {
                $this->Session->delete('PostSearch');
                $this->Session->delete('full_date');
                $this->Session->delete('start_date');
                $this->Session->delete('end_date');
                $this->Session->delete('status');
            }

            if ($this->Session->check('PostSearch')) {
                $PostSearch = $this->Session->read('PostSearch');
                //$update_date = $this->Session->read('update_date');
                $full_date = $this->Session->read('full_date');
                $start_date = $this->Session->read('start_date');
                $end_date = $this->Session->read('end_date');
                $account_name = $this->Session->read('status');
            }
            
            
            
            if(isset($start_date) && $start_date != "")
            {
                $conditions[] = array(
                    'PackHistory.updated >=' => $start_date,
                );
            }
            if(isset($end_date) && $end_date != "")
            {
                $conditions[] = array(
                    'PackHistory.updated <=' => $end_date,
                );
            }
            
            if ($status == "Sold Out") {
                $conditions[] = array(
                    'PackHistory.status LIKE ' => '%' . $status . '%',
                );
            } else if ($status == "Settle") {
               
                $conditions[] = array(
                    'PackHistory.status LIKE' => '%' . $status . '%',
                );
            } else if ($status == "Return") {
                
                $conditions[] = array(
                    'PackHistory.status LIKE' => '%' . $status . '%',
                );
            } 
            
            else if ($start_date != "" && $end_date != "") {
                $conditions[] = array(
                    'PackHistory.status NOT IN' => array('confirm', 'active'),
                );
            }
            //pr($conditions);
            //pr($formatedDate);
            $this->paginate = array('conditions' => $conditions,
                'order' => array(
                    'PackHistory.id' => 'desc'
                ),
                'limit' => '500'
                    //'fields' => $fields
            );
            //$this->Paginator->settings = array('limit' => '');
            $this->set('full_date', $full_date);
            $this->set('games', $this->Paginator->paginate());
            
        }
        
    }	
	
    
    public function admin_reset() {

        $this->Session->delete('PostSearch');
        $this->Session->delete('full_date');
        $this->Session->delete('start_date');
        $this->Session->delete('end_date');
        $this->Session->delete('status');
        $redirect_url = array('controller' => 'pack_historys', 'action' => 'admin_index');
        return $this->redirect($redirect_url);
    }
	
	
	public function admin_getgames(){
		$this->autoRender = false;
		$upadated=$this->request->data['updated'];		
		//echo $upadated; die;		
		//$this->loadModel('PackHistory');
		$games_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'),'DATE(PackHistory.updated) >=' =>$upadated,'DATE(PackHistory.updated) <=' =>$upadated);		        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $games_conditions));
		//echo '<pre>';print_r($PackHistory); die;	
		echo "<option value=''>Select Game</option>";
		foreach($PackHistory as $pack_history)
		{		
		//$game_nos[$pack_history['PackHistory']['game_no']]=$pack_history['PackHistory']['game_no'];
		 $val=$pack_history['PackHistory']['game_no'];
		 echo "<option value='$val'>$val</option>";
		}
		
	}
	
	public function admin_getpacks(){
		$this->autoRender = false;
		$gameno=$this->request->data['gameno'];		
		$packs_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'),'PackHistory.game_no' =>$gameno);		        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $packs_conditions));
		print_r($PackHistory[0]['PackHistory']['pack_no']);
	}
	
	
	public function admin_savepacks(){
		$this->autoRender = false;
		$updated=$this->request->data['updated'];	
		$game_no=$this->request->data['game_no'];	
		$pack_no=$this->request->data['pack_no'];	
		$status=$this->request->data['status'];		
		$packs_conditions= array('PackHistory.store_id' =>$this->Session->read('stores_id'),'PackHistory.company_id' =>$this->Session->read('Auth.User.company_id'),'DATE(PackHistory.updated)' =>$updated,'PackHistory.game_no' =>$game_no,'PackHistory.pack_no' =>$pack_no);		        
		$PackHistory = $this->PackHistory->find('all',array('conditions' => $packs_conditions));
		//print_r($PackHistory);die;
		//print_r($PackHistory[0]['PackHistory']['id']);die;
		$pack_id=$PackHistory[0]['PackHistory']['id'];
		if($pack_id!='')
		{
		$pack_data['PackHistory']['id']=$pack_id;
		$pack_data['PackHistory']['status']=$this->request->data['status'];		
		//echo '<pre>';print_r($pack_data);die;
				if ($this->PackHistory->save($pack_data))
				{ 
				echo 'Successfully upadted';
				}else
				{
				echo 'Not upadted';
				}
		}else{
		echo 'Game packs not exists!';	
		}
		
	}
	
	
}
