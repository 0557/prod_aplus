<?php

App::uses('AppController', 'Controller');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class PaymentController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

      public function admin_index($id=null) {
	
        $where = array('conditions' => array('Payment.store_id' => $this->Session->read('stores_id')), "order" => "Payment.id DESC");
		
        $this->set('payment', $this->Payment->find('all',$where));
		
		if($id){
			if($this->Payment->delete($id)){
				$this->Session->setFlash(__('The Payment has been deleted.'));
				 return $this->redirect(array('action' => 'index'));
			}
		}
    }

   

    public function admin_edit($id = null) {
		
      $this->loadModel('Payment');
        $title_for_layout = 'Payments';
        $this->set('title_for_layout', $title_for_layout);
		
			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select store.')); 
				return $this->redirect(array('action' => 'index'));

			}else{
        if ($this->request->is('post')) {
			if($this->request->data['Payment']['Paymentdate']){
				$newdate = explode('-',$this->request->data['Payment']['Paymentdate']);
				$this->request->data['Payment']['Paymentdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
            if ($this->Payment->save($this->request->data)) {

                $this->Session->setFlash(__('The Payment has been updated.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Payment could not be saved. Please, try again.'));
            }
        }
		
		$editrecord = $this->Payment->find('first',array('conditions'=>array('Payment.store_id'=>$this->Session->read('stores_id'),'Payment.id'=>$id)));
$this->set('editrecord',$editrecord);		
		}

    }
    
   

    public function admin_add($id=null) {
		
      $this->loadModel('Payment');
        $title_for_layout = 'Payments';
        $this->set('title_for_layout', $title_for_layout);
		
			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select store.')); 
				return $this->redirect(array('action' => 'index'));

			}else{
				        if ($this->request->is('post')) {
            $this->Payment->create();

			
			if($this->request->data['Payment']['Paymentdate']){
				$newdate = explode('-',$this->request->data['Payment']['Paymentdate']);
				$this->request->data['Payment']['Paymentdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			
   $this->request->data['Payment']['company_id'] = $this->Session->read('Auth.User.company_id');
   $this->request->data['Payment']['store_id'] = $this->Session->read('stores_id');
  
            if ($this->Payment->save($this->request->data)) {

                $this->Session->setFlash(__('The Payment has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Payment could not be saved. Please, try again.'));
            }
        }
		
		
			}


    }


}
