<?php

App::uses('AppController', 'Controller');

/**
 * PurchaseInvoices Controller
 *
 * @property PurchaseInvoice $PurchaseInvoice
 * @property PaginatorComponent $Paginator
 */
class PurchaseInvoicesController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->set('purchase_invoice', 'active');

        $this->paginate = array('conditions' => array(/*'PurchaseInvoice.corporation_id' => $_SESSION['corporation_id'], */'PurchaseInvoice.store_id' => $_SESSION['store_id']), "limit" => 10, "order" => "PurchaseInvoice.id DESC");

        $this->set('purchaseInvoices', $this->paginate());
		
//		print_r($this->paginate());
    }

    public function admin_view($id = null) {
        $this->Setredirect();
        $this->set('purchase_invoice', 'active');

        if (!$this->PurchaseInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel invoice'));
        }
        $options = array('conditions' => array('PurchaseInvoice.' . $this->PurchaseInvoice->primaryKey => $id));

		$fuelInvoice=$this->PurchaseInvoice->find('first', $options);
		//pr($fuelInvoice); die;
        $this->set('fuelInvoice',$fuelInvoice );
		
		$storeId=$this->Session->read('store_id');
		$product = $this->setFilterProductNumber($storeId); 
		$this->set('product',$product );
    }

    public function admin_add() {
        $this->Setredirect();
        $this->set('purchase_invoice', 'active');
        $this->loadmodel('Rproduct');
		 $this->loadmodel('RubyFprod');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('Store');
		$this->PurchaseInvoice->create();
		$storeId=$this->Session->read('store_id');
		 $this->loadmodel('PurchaseInvoice');
		
		
		
        if ($this->request->is('post')) { 
		
            $this->request->data['PurchaseInvoice']['store_id'] = $storeId;
            $this->request->data['PurchaseInvoice']['company_id'] = $this->Session->read('Auth.User.company_id');
			
			if($this->request->data['PurchaseInvoice']['files']['tmp_name']!="" || $this->request->data['PurchaseInvoice']['files']['tmp_name']!=null){
				
					$this->uploadFile('files');
			}else{
					$this->request->data['PurchaseInvoice']['files']="";
			}
		
			$this->PurchaseInvoice->save($this->request->data);
			
			 $getlast_id = $this->PurchaseInvoice->getLastInsertId();
		
		//	pr($this->request->data); die;
				
                if ($this->PurchaseInvoice->save($this->request->data)) {
					
					$i=0;
					$product_save = array();
					
					foreach($this->request->data['FuelProduct']['product_id'] as $fuelproduct){
						 $this->FuelProduct->create();
						$gal=$this->request->data['FuelProduct']['gallons_delivered'];
						if($gal[$i]!=""){
							$product_save['FuelProduct']['type'] = 'R Fuel Purchase Invoice';
							$product_save['FuelProduct']['r_purchase_invoice_id'] = $getlast_id;
							 $product_save['FuelProduct']['product_id'] = $this->request->data['FuelProduct']['product_id'][$i];
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$i];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$i];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$i];						
							 $this->FuelProduct->save($product_save);
						}
						
						$i++;
					}
					
					
				
				$this->Session->setFlash(__('The R fuel Purchase invoice has been saved.'));
                    return $this->redirect(array('action' => 'index'));
            }
        }
		
		$product = $this->setFilterProductNumber($storeId); //pr($prodList); die;
        //$product = $this->RubyFprod->find('list',array('fields' =>array('id','fuel_product_name'),'conditions' => array('RubyFprod.store_id'=>$this->Session->read('stores_id'))));
        $wholesale_supplier = $this->Customer->find('list', array('conditions' => array("Customer.company_id" => $this->Session->read('Auth.User.company_id'), "Customer.type"=>'Fuel',"Customer.store_id" => $storeId)));
		
        $stores = $this->Store->find('list',array('conditions' => array('company_id'=>$this->Session->read('Auth.User.company_id'))));
        $this->set(compact('wholesale_supplier', 'product', 'stores'));
    }

    public function admin_edit($id = null) {
		
		
        $this->Setredirect();
        $this->set('purchase_invoice', 'active');
        $this->loadmodel('Rproduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('Store');
        $this->loadmodel('PurchaseInvoice');
        if (!$this->PurchaseInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel invoice'));
        }
			
			
        if ($this->request->is(array('post', 'put'))) {
          
		
        	if($this->request->data['PurchaseInvoice']['files']['tmp_name']!="" || $this->request->data['PurchaseInvoice']['files']['tmp_name']!=null){
				$this->uploadFile('files');
			}else{
				$this->request->data['PurchaseInvoice']['files']=$this->request->data['PurchaseInvoice']['oldfiles'];
			}
			 // pr($this->request->data);die;
			
                if ($this->PurchaseInvoice->save($this->request->data)) {
					
					 $getlast_id = $this->request->data['PurchaseInvoice']['id']; 
					
					$i=0;
					$product_save = array();
					$this->request->data['FuelProduct']['product_id']=array_filter($this->request->data['FuelProduct']['product_id']);
					 $fdata= $this->PurchaseInvoice->query("DELETE from `fuel_products` WHERE `r_purchase_invoice_id` = '".$id."'");  
		//pr($fdata); die;
					
					foreach($this->request->data['FuelProduct']['product_id'] as $fuelproduct){
						 $this->FuelProduct->create();
						//pr($this->request->data['FuelProduct']);die;
						$gal=$this->request->data['FuelProduct']['gallons_delivered'];
						if($gal[$i]!=""){
							$product_save['FuelProduct']['type'] = 'R Fuel Purchase Invoice';
							 $product_save['FuelProduct']['r_purchase_invoice_id'] = $id;
							 $product_save['FuelProduct']['product_id'] = $this->request->data['FuelProduct']['product_id'][$i];
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$i];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$i];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$i];						
							 
							 
							 $this->FuelProduct->save($product_save);
						}
						
						$i++;
					}
				
				$this->Session->setFlash(__('The R fuel invoice has been saved.'));
                    return $this->redirect(array('action' => 'index'));
            }else {
                    $this->Session->setFlash(__('The R  fuel invoice could not be saved. Please, try again.'));
                }
        } else {
            $options = array('conditions' => array('PurchaseInvoice.id' => $id));
            $this->request->data = $this->PurchaseInvoice->find('first', $options);
        }

		 
        $supplier_id = $this->request->data['PurchaseInvoice']['supplier_id'];

       $wholesale_supplier = $this->Customer->find('list', array('conditions' => array("Customer.company_id" => $this->Session->read('Auth.User.company_id'), "Customer.type"=>'Fuel',"Customer.store_id" => $_SESSION['store_id'])));
	   
	   $storeId=$this->Session->read('store_id');
		$product = $this->setFilterProductNumber($storeId);
	   
	   
	
        $stores = $this->Store->find('list',array('conditions' => array('company_id'=>$this->Session->read('Auth.User.company_id'))));
        $this->set(compact('wholesale_supplier', 'product', 'supplier_id', 'stores','fdata'));
    }

    public function admin_delete($id = null) {
		
        $this->Setredirect();
        $this->PurchaseInvoice->id = $id;
		
        if (!$this->PurchaseInvoice->exists()) {
            throw new NotFoundException(__('Invalid purchase invoice'));
        }
	
		 $fdata= $this->PurchaseInvoice->query("DELETE from `fuel_products` WHERE `r_purchase_invoice_id` = '".$id."'");  
        // $this->request->allowMethod('post', 'delete');
        if ($this->PurchaseInvoice->delete()) {
			
            $this->Session->setFlash(__('The purchase invoice has been deleted.'));
        } else {
            $this->Session->setFlash(__('The purchase invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function products($id = null) {

        $this->loadmodel('Rproduct');
		$storeId=$this->Session->read('store_id');
		$product = $this->setFilterProductNumber($storeId);
        $products = $this->Rproduct->find('list');
      //  $product = $this->Rproduct->find('first', array('conditions' => array('Rproduct.id' => $id)));

        $this->set(compact('products', 'product', 'id'));
    }
	    public function product_name($id = null) {
		//return $id; exit;	
        $this->loadmodel('Rproduct');
        $product = $this->Rproduct->find('first', array('conditions' => array('Rproduct.id' => $id)));
		if(!empty($product)) {
			return $product['Rproduct']['name'];
		}
    }
	
function uploadFile($fieldName = 'files')
	{
			
		//$upload_dir = WWW_ROOT.str_replace("/", DS, 'stores');
		$uploadPath = WWW_ROOT.'files';
	
		$imageTypes = array("pdf", "doc", "docx", "xlsx","csv");
		if (isset($this->data['PurchaseInvoice'][$fieldName]['type']) ) {
			$fike = $this->data['PurchaseInvoice'][$fieldName];
			$_file = date('His') .'_'. $fike['name'];
			$full_image_path = $uploadPath . '/' .$_file;
			
			if (move_uploaded_file($fike['tmp_name'], $full_image_path)) {
				$this->request->data['PurchaseInvoice'][$fieldName] = $_file;
				$this->Session->setFlash('File saved successfully');
				return true;
			} else {
				$this->Session->setFlash('Invalid type. Please try again with fike type '. implode(', ', $imageTypes));
				return false;
			}
		}else{
			$this->request->data['PurchaseInvoice'][$fieldName] = "";
		}
		return false;
	}
}
