<?php
App::uses('AppController', 'Controller');

class RbankAccountsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $uses=array('Corporation','Store','RBankAccount','Rbank');
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->RbankAccount->recursive = 0;
		
		
		
	}


	public function admin_index($id=null) {
		$this->Setredirect();
		$this->loadModel('RBankAccount');
		
		$corporation=$this->RBankAccount->find('all',array('conditions'=>array('store' => $this->Session->read('stores_id'))));
		
		$this->set('corporation_list',$corporation);
		if($id){
		if ($this->RBankAccount->delete($id))
				{   
				
				return $this->redirect(array('controller' => 'r_bank_accounts','action' => 'index'));
				$this->Session->setFlash(__("You have successfully added new Note"),'flash_notification');
				
		    	}
		
		}
		//	echo'<pre>';print_r($corporation);exit;
		
		
	}
	public function admin_edit($id=null) {
		
		$this->loadModel('RBankAccount');
		$accountlist=$this->RBankAccount->find('first',array('conditions'=>array('store' => $this->Session->read('stores_id'),'id'=>$id)));
		
		$this->set('account',$accountlist);
		
		$corporation=$this->Corporation->find('list');
		$this->set('corporation_list',$corporation);
		//echo'<pre>';print_r($corporation);exit;
		
		
	}
	public function admin_add() {
		
		//$this->Setredirect();
	//	$this->RbankAccount->recursive = 0;
		
		
		$corporation=$this->Corporation->find('list');
		$this->set('corporation_list',$corporation);
		//echo'<pre>';print_r($corporation);exit;
		
		
	}
	
	public function get_store_id () {
		$this->render=false;
		$corporation_id=$_REQUEST['corporation'];		
		$store=$this->Store->find('list',array('conditions'=>array('Store.corporation_id'=>$corporation_id)));
		$this->set('store_list',$store);
	}
	
	public function get_account_id () {
		
		$this->render=false;
		$corporation_id=$_REQUEST['corporation'];		
		
		$banks=$this->Rbank->find('all',array('conditions'=>array('Rbank.corporation_id'=>$corporation_id)));
      $this->set('banks',$banks);
     // echo'<pre>';print_r($banks);exit;
		$accounts=$this->RBankAccount->find('all',array('conditions'=>array('RBankAccount.corporation'=>$corporation_id)));
		
		$accountsListArr = array();
		if(!empty($accounts)) {
			foreach($accounts as $key => $_acc) {
				$accountsListArr[$_acc['RBankAccount']['id']] = $_acc['RBankAccount']['account_name'] . ' - '.$_acc['RBankAccount']['bank_name'] . ' - '.$_acc['RBankAccount']['routing_no'];
			}
		}
		$this->set('accounts_list', $accountsListArr);
		//var_dump('sa');exit;
		
		
	}
		
   
   public function  admin_add_account() 
	 {  
	   
		$this->layout='admin';
	//echo'<pre>';print_r($_REQUEST);exit;
		
		if (!empty($this->request->data))
		{
		
			   $this->request->data['RBankAccount']['create_date']=date('Y-m-d');
			   if(!$this->request->data['RBankAccount']['store']){
		      $this->request->data['RBankAccount']['store']=  $this->request->data['store'];
			   }
				if ($this->RBankAccount->save($this->request->data))
				{   
				
				return $this->redirect(array('controller' => 'r_bank_accounts','action' => 'index'));
				$this->Session->setFlash(__("You have successfully added new Note"),'flash_notification');
				
		    	}
		}
        
		else 
		{
			//$this->request->data = $this->Job->find('first',array('conditions'=>array('OtherBuses.id'=>$id)));
		}
    }

 
}
