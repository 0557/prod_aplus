<?php
App::uses('AppController', 'Controller');

class RbanksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
   public $uses=array('Corporation','Store','Rbank');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		 $this->Rbank->recursive = 0;
		 $this->set('rbankin', 'active');
		$this->Setredirect();
		
	}


	public function admin_index() {
		 $this->set('rbankin', 'active');
		// $this->Setredirect();
		$this->Rbank->recursive = 0;
		$storeId = $_SESSION['store_id'];
		$corporation=$this->Corporation->find('list');
		$this->set('corporation_list',$corporation);
		$accountdetail=$this->Rbank->find('all');
		$this->set('account',$accountdetail);
		//echo'<pre>';print_r($accountdetail);exit;
		
		
	}
	public function save_bank_detail () 
	{
		
		$this->render=false;
		$storeId = $_SESSION['store_id'];
		$corporation = $_REQUEST['corporation'];
		$accountid = $_REQUEST['accountid'];
		$store_id = $_REQUEST['store'];
		$bankname = $_REQUEST['bankname'];
		$date = $_REQUEST['date'];
		
		$payee = $_REQUEST['payee'];
		$paidcheckno = $_REQUEST['paidcheckno'];
		$paidamount = $_REQUEST['paidamount'];
		//$rcheck = $_REQUEST['rcheck'];
		$ramount = $_REQUEST['ramount'];
		$balance = $_REQUEST['balance'];
		$status = $_REQUEST['deposit_status'];
		
		
	//	$this->request->data['Rbank']['store_id'] = $store_id;	
      $this->request->data['Rbank']['corporation_id'] = $corporation;	
		$this->request->data['Rbank']['account_id'] = $accountid;
		$this->request->data['Rbank']['date'] = $date;	
	
		$this->request->data['Rbank']['payee'] = $payee;
		$this->request->data['Rbank']['paid_check'] = $paidcheckno;	
		$this->request->data['Rbank']['paid_amount'] = $paidamount;
	//	$this->request->data['Rbank']['receive_check'] = $rcheck;
		$this->request->data['Rbank']['receive_amount'] = $ramount;
		$this->request->data['Rbank']['balance'] = $balance;
		$this->request->data['Rbank']['deposit_status'] = $status;
		if($this->Rbank->save($this->request->data))
		{
			echo 'Save';
		}
		else {
			echo 'Unable to Save';
			}

		exit;
	}
	
	public function admin_delete($id=null) {
		
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
	
		if ($this->Rbank->delete($id)) {
			
			//$this->Session->setFlash(__('Your Account are successfully deleted'),'flash_notification');
			return $this->redirect(array('action' => 'index'));
			
		}
	}
	
	 public function get_bankaccount_xls(){
		  
			
		$this->autoRender=false;	
		   
      header("Content-Type: application/vnd.ms-excel");
		   
		 
		$this->Rbank->recursive = 0;
		$storeId = $this->getStoreId();
		
	   	$account=$this->Rbank->find('all',array('conditions'=>array('Rbank.store_id'=>$storeId),'order' => array('Rbank.id DESC' ))); 
		   
		
		    echo 'Sl. No.'."\t";
		    echo 'Date '."\t";
		    echo'Check No '."\t";
          echo'Transaction Description '."\t";
          echo'Payment Fee Withdrawal (-) '."\t";
          echo'Deposit Credit (+) '."\t";
          echo'Balance '."\n"."\n";
         
          $i=1;
          foreach($account as $bank)
          {
          	echo $i++."\t";
		  	echo $bank['Rbank']['date']."\t";
		  	echo $bank['Rbank']['payee']."\t";
		  	echo $bank['Rbank']['paid_check']."\t";
		  	echo $bank['Rbank']['paid_amount']."\t";
		  	echo $bank['Rbank']['receive_amount']."\t";
		  	echo $bank['Rbank']['balance']."\t"."\n";
		  
		  }
          
  		
           header("Content-disposition: attachment; filename=".rand().".xls");
 
	  } 
	  
	  public function admin_edit($id=null) 
		{
			$this->set('admin', 'active');
		// $this->set('r_banks', 'active');
		 $this->layout='admin';
		
		$banks= $this->Rbank->findById($id);
		
		$this->set('bank',$banks);
		
		//echo'<pre>';print_r($banks);exit;
		
		
		if (!$id) {
			throw new NotFoundException(__('Invalid Rbank'));
		}
		if (!$banks) {
		throw new NotFoundException(__('Invalid Rbank'));
		}

		if (!empty($this->request->data)) {
			$this->Rbank->id = $id;
	     	if($this->request->data['Rbank']['deposit_status']==1)
			{
					$this->request->data['Rbank']['deposit_status']=1;
			}	
			 else
			{
				$this->request->data['Rbank']['deposit_status']='';
			}
		//	echo'<pre>';print_r($this->request->data);exit;
			if ($this->Rbank->save($this->request->data)) {
				
				 
				//$this->Session->setFlash(__("Rbank has been successfully update"),'flash_notification');
				
			   	return $this->redirect(array('action' => 'index','admin'=>true));
				
			} else {
				$this->Session->setFlash(__('Unable to update your Rbank.'));
			}
		}

		if (!$this->request->data) {
		$this->request->data = $banks;	
	//	echo'<pre>';print_r($this->request->data);exit;
		}
	}

   public function get_all_banks()
	
		{
		
		$this->render=false;
		$corporation_id=$_REQUEST['corporation'];	
		$account_id=$_REQUEST['account_id'];		
		
		$banks=$this->Rbank->find('all',array('conditions'=>array('Rbank.corporation_id'=>$corporation_id,'Rbank.account_id'=>$account_id)));
      $this->set('banks',$banks);
     // echo'<pre>';print_r($banks);exit;
		
		
	
		
	}
	public function get_all_reports()
	
		{
		
		$this->render=false;
		$from=$_REQUEST['datefrom'];	
		$to=$_REQUEST['dateto'];
		$corporation_id=$_REQUEST['corporation'];	
		$account_id=$_REQUEST['account_id'];	
		
		$conditions = array(
        'conditions' => array(
        'and' => array(
                        array('Rbank.date <= ' => $from,
                              'Rbank.date >= ' => $to
                             ),
            'Rbank.corporation_id'=>$corporation_id,
            'Rbank.account_id'=>$account_id
            )));	
		
		$report=$this->Rbank->find('all',array($conditions));
      $this->set('banks',$report);
      //echo'<pre>';print_r($report);exit;
		
	}
	public function admin_report() {
		 $this->set('rbanklist', 'active');
		 //$this->Setredirect();
		$corporation=$this->Corporation->find('list');
		$this->set('corporation_list',$corporation);
		
	}
	
	public function get_xlsreport(){
		  
		 $this->set('r_banks', 'active');
		$this->autoRender=false;	
		   
      header("Content-Type: application/vnd.ms-excel");
		   
		 
		$this->Rbank->recursive = 0;
		$from=$_REQUEST['datefrom'];	
		$to=$_REQUEST['dateto'];
		$corporation_id=$_REQUEST['corporation'];	
		$account_id=$_REQUEST['account_id'];	
		
		$conditions = array(
        'conditions' => array(
        'and' => array(
                        array('Rbank.date <= ' => $from,
                              'Rbank.date >= ' => $to
                             ),
            'Rbank.corporation_id'=>$corporation_id,
            'Rbank.account_id'=>$account_id
            )));	
		
		$account=$this->Rbank->find('all',array($conditions));
     
		
		    echo 'Sl. No.'."\t";
		    echo 'Date '."\t";
		    echo'Check No '."\t";
          echo'Transaction Description '."\t";
          echo'Payment Fee Withdrawal (-) '."\t";
          echo'Deposit Credit (+) '."\t";
          echo'Balance '."\n"."\n";
         
          $i=1;
          foreach($account as $bank)
          {
          	echo $i++."\t";
		  	echo $bank['Rbank']['date']."\t";
		  	echo $bank['Rbank']['payee']."\t";
		  	echo $bank['Rbank']['paid_check']."\t";
		  	echo $bank['Rbank']['paid_amount']."\t";
		  	echo $bank['Rbank']['receive_amount']."\t";
		  	echo $bank['Rbank']['balance']."\t"."\n";
		  
		  }
          
  		
           header("Content-disposition: attachment; filename=".rand().".xls");
           
      exit();     
	  } 

  

 
}
