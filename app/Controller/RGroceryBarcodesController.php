<?php 
App::uses('AppController', 'Controller');
App::uses('BarcodeHelper','Vendor');

class RGroceryBarcodesController extends AppController {

   
    public $components = array('Paginator');

   
    public function admin_index() {
		
		error_reporting(0);
		$this->loadModel('RubyDepartment');
		$this->Setredirect();
        $this->RGroceryBarcode->recursive = 0;
				$rGroceryDepartments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'),
				 'conditions'=>array('RubyDepartment.store_id'=>$this->Session->read('stores_id')),
				 'order'=>array('RubyDepartment.name'=>'ASC'),
				));
		$this->set('departmentlist',$rGroceryDepartments);
		$this->Session->write('department_id',"");
		$this->Session->write('plu_no',"");
		$this->Session->write('page_no',"");		
		if ($this->request->is('post')) {			
			//print_r($this->request['data']);die;			
			
			$barcodepath =  WWW_ROOT .'img/barcode/*';			
			$files = glob($barcodepath);
			foreach($files as $file){
			if(is_file($file))
			unlink($file); 
			}
			
			
			if($this->request->data['RGroceryBarcode']['cat']!=''){ 
			    $this->Session->delete('checkpost1','yes');
			    $this->Session->write('checkpost','yes');
				$this->Session->write('department_id',$this->request->data['RGroceryBarcode']['cat']);
				
				$this->set('Globalupcslist', $this->RGroceryBarcode->find('all',array('conditions'=>array('RGroceryBarcode.r_grocery_department_id'=>$this->request->data['RGroceryBarcode']['cat'],'RGroceryBarcode.store_id'=>$this->Session->read('stores_id')))));				
				
		  	}
			
			if($this->request['data']['RGroceryBarcode']['plu_no']!=''){				
				$this->Session->delete('checkpost','yes');
				//echo $this->request['data']['plu_no'];die;
				$this->Session->write('checkpost1','yes');
				$this->Session->write('plu_no',$this->request['data']['RGroceryBarcode']['plu_no']);
				
				if($this->request['data']['RGroceryBarcode']['page_no']!=''){					
				//echo $this->request['data']['RGroceryBarcode']['page_no'];die;	
				$this->Session->write('page_no',$this->request['data']['RGroceryBarcode']['page_no']);	
				}
				
				//$plu_res=$this->RGroceryBarcode->find('all',array('conditions'=>array('RGroceryBarcode.plu_no'=>$this->request['data']['plu_no'],'RGroceryBarcode.store_id'=>$this->Session->read('stores_id'))));
				
				//print_r($plu_res);die;
				
				$this->set('Globalupcslist1', $this->RGroceryBarcode->find('all',array('conditions'=>array('RGroceryBarcode.plu_no'=>$this->request['data']['RGroceryBarcode']['plu_no'],'RGroceryBarcode.store_id'=>$this->Session->read('stores_id')))));
			}
			

	   } else {
			$this->Session->delete('checkpost','yes');
			$this->Session->delete('checkpost1','yes');
			$this->Session->delete('department_id',$this->request->data['RGroceryBarcode']['cat']);
			$this->Session->delete('plu_no',$this->request['data']['RGroceryBarcode']['plu_no']);
			$this->Session->delete('page_no',$this->request['data']['RGroceryBarcode']['page_no']);
	   }
	   
	   $this->set('department_id', $this->Session->read('department_id'));
       $this->set('plu_no', $this->Session->read('plu_no'));
       $this->set('page_no', $this->Session->read('page_no'));
    }
	

	
	 public function admin_barcode() {
		 
		 if ($this->request->is('post')) {
			 $this->loadModel('RubyDepartment');
			 //pr($this->request->data); die;
			 if($this->request->data['RGroceryBarcode']['department_id']){
				 
				 $barcodelist= $this->RGroceryBarcode->find('all',array('conditions'=>array('RGroceryBarcode.r_grocery_department_id'=>$this->request->data['RGroceryBarcode']['department_id'],'RGroceryBarcode.store_id'=>$this->Session->read('stores_id'))));
				 
				$department_id=$this->request->data['RGroceryBarcode']['department_id'];
				$department=$this->RubyDepartment->find('list',array('fields' => array('number', 'name'),
				 'conditions'=>array('RubyDepartment.store_id'=>$this->Session->read('stores_id'),'RubyDepartment.number'=>$this->request->data['RGroceryBarcode']['department_id'])
				));				
				//pr($department[$department_id]); die;
				 //pr($barcodelist); die;
				 $barcodelistcount=count($barcodelist);
				 if($barcodelistcount>0){
				 	 $new_barcodelist=array();
					 foreach($barcodelist as $data)
					 {
						$plu=$data['RGroceryBarcode']['plu_no']; 
						$data_to_encode=$plu;
						$barcode=new BarcodeHelper(); 
						// Generate Barcode data
						$barcode->barcode();
						$barcode->setType('C128');
						$barcode->setCode($data_to_encode);
						$barcode->setSize(80,200);
						// Generate filename            
						$random = rand(0,1000000); 
						$file = 'img/barcode/code_'.$random.'.png';
						// Generates image file on server            
						$barcode->writeBarcodeFile($file); 
						$data['RGroceryBarcode']=array_merge($data['RGroceryBarcode'], array("barimage"=>$file,"department"=>$department[$department_id]));
						array_push($new_barcodelist,$data['RGroceryBarcode']); 
						
						//array_push($data['RGroceryBarcode'],  $data['RGroceryBarcode']['barimage'] = $file); 
					 }
					//pr($data); die;
					$this->set('data', $new_barcodelist);
					$filename=time()."_barcode.pdf";
					$this->set('filename', $filename);
					$this->layout = '/pdf/default1';
					$this->render('/Pdf/barcode_pdf_view');
					
					$this->Session->setFlash(__('Barcodes successfully created.'));
				}else {					 
					$this->Session->delete('department_id', $this->Session->read('department_id'));
					$this->Session->setFlash(__('There are no items to create barcode.'), 'default', array('class' => 'error'));
				}
				 
			 }
			 
		 } 
		 
		 $this->redirect(array('controller' => 'r_grocery_barcodes', 'action' => 'index'));
		
		 
	 }
	 
	 
	 
	  public function admin_barcode1() {
		 //$this->autoRender = false;
		 if ($this->request->is('post')) {
			 $this->loadModel('RubyDepartment');
			 //print_r($this->request->data); die;
			 if($this->request->data['RGroceryBarcode']['plu_no']){
				 
				 $barcodelist= $this->RGroceryBarcode->find('all',array('conditions'=>array('RGroceryBarcode.plu_no'=>$this->request->data['RGroceryBarcode']['plu_no'],'RGroceryBarcode.store_id'=>$this->Session->read('stores_id'))));
				 
				$plu_no=$this->request->data['RGroceryBarcode']['plu_no'];
				$page_no=$this->request->data['RGroceryBarcode']['page_no'];
			/*	$department=$this->RubyDepartment->find('list',array('fields' => array('number', 'name'),
				 'conditions'=>array('RubyDepartment.store_id'=>$this->Session->read('stores_id'),'RubyDepartment.number'=>$this->request->data['RGroceryBarcode']['department_id'])
				));	*/			
				//pr($department[$department_id]); die;
				//echo '<pre>'; print_r($barcodelist); die;
				 $barcodelistcount=count($barcodelist);
				 if($barcodelistcount>0){
				 	 $new_barcodelist=array();
					 foreach($barcodelist as $data)
					 {
						$plu=$data['RGroceryBarcode']['plu_no']; //echo $plu;die;
						$data_to_encode=$plu;
						$barcode=new BarcodeHelper(); 
						// Generate Barcode data
						$barcode->barcode();
						$barcode->setType('C128');
						$barcode->setCode($data_to_encode);
						$barcode->setSize(80,200);
						// Generate filename            
						$random = rand(0,1000000); 
						$file = 'img/barcode/code_'.$random.'.png';
						// Generates image file on server            
						$barcode->writeBarcodeFile($file); 
						$data['RGroceryBarcode']=array_merge($data['RGroceryBarcode'], array("barimage"=>$file,"plu_no"=>$plu_no,"page_no"=>$page_no));
						array_push($new_barcodelist,$data['RGroceryBarcode']); 
						
						//array_push($data['RGroceryBarcode'],  $data['RGroceryBarcode']['barimage'] = $file); 
					 }
					//echo '<pre>'; print_r($new_barcodelist); die;
					$this->set('data', $new_barcodelist);
					$filename=time()."_barcode.pdf";
					$this->set('filename', $filename);
					$this->layout = '/pdf/default1';
					$this->render('/Pdf/barcode_pdf_view1');
					
					$this->Session->setFlash(__('Barcodes successfully created.'));
				}else {					 
					$this->Session->delete('plu_no', $this->Session->read('plu_no'));
					$this->Session->setFlash(__('There are no items to create barcode.'), 'default', array('class' => 'error'));
				}
				 
			 }
			 
		 } 
		 
		 $this->redirect(array('controller' => 'r_grocery_barcodes', 'action' => 'index'));
		
		 
	 }

	 
	 public function admin_reset() {	
		$this->Session->delete('PostSearch');		
		$this->Session->delete('department_id');
		$this->Session->delete('plu_no');
		$this->Session->delete('page_no');
		$this->Session->delete('checkpost');
		$this->Session->delete('checkpost1');				
		$redirect_url = array('controller' => 'r_grocery_barcodes', 'action' => 'index');
		return $this->redirect( $redirect_url );
		
	}
	
	 
	
  
}
