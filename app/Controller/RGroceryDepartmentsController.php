<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryDepartments Controller
 *
 * @property RGroceryDepartment $RGroceryDepartment
 * @property PaginatorComponent $Paginator
 */
class RGroceryDepartmentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->RGroceryDepartment->recursive = -1;
        $this->paginate = array('conditions' => array(/*'RGroceryDepartment.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryDepartment.store_id' => $_SESSION['store_id']), "limit" => 15, "order" => "RGroceryDepartment.id DESC");
        $this->set('rGroceryDepartments', $this->Paginator->paginate());
    }
	
	public function admin_update()
	{
		$this->loadModel('RGroceryItem');
		$this->loadModel('RubyDepartment');
		
		$allItems = $this->RGroceryItem->find('all');
		foreach($allItems as $key => $_item) {
			$department= $this->RubyDepartment->find(
				'first', array(
				'conditions' => array(
					'store_id' => $_item['RGroceryItem']['store_id'], 
					'number' => $_item['RGroceryItem']['r_grocery_department_id']
					)
				)
			);
			if(!empty($department)) {
				$this->RGroceryItem->id = $_item['RGroceryItem']['id'];
				$this->RGroceryItem->saveField('department_name', $department['RubyDepartment']['name']);
			}
			
		}
		die('success');
	}

    public function admin_view($id = null) {
        if (!$this->RGroceryDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery department'));
        }
        $options = array('conditions' => array('RGroceryDepartment.' . $this->RGroceryDepartment->primaryKey => $id));
        $this->set('rGroceryDepartment', $this->RGroceryDepartment->find('first', $options));
    }

    public function admin_add() {
        $this->Setredirect();
        if ($this->request->is('post')) {
            $this->RGroceryDepartment->create();

            //pr($this->data); die;
          $this->request->data['RGroceryDepartment']['company_id'] = $this->Session->read('Auth.User.company_id');
            $this->request->data['RGroceryDepartment']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RGroceryDepartment']['customerId'] = $this->data['RGroceryDepartment']['customerId1'] . "," . $this->data['RGroceryDepartment']['customerId2'];
            $this->request->data['RGroceryDepartment']['blue_laws'] = $this->data['RGroceryDepartment']['blue_laws1'] . "," . $this->data['RGroceryDepartment']['blue_laws2'];

            if ($this->RGroceryDepartment->save($this->request->data)) {
                $this->Session->setFlash(__('Department Information Is saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Department Information Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        }
    }

    public function admin_edit($id = null) {
        $this->Setredirect();
        if (!$this->RGroceryDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid Department'));
        }
        if ($this->request->is(array('post', 'put'))) {
            
         //   $this->request->data['RGroceryDepartment']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RGroceryDepartment']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RGroceryDepartment']['customerId'] = $this->data['RGroceryDepartment']['customerId1'] . "," . $this->data['RGroceryDepartment']['customerId2'];
            $this->request->data['RGroceryDepartment']['blue_laws'] = $this->data['RGroceryDepartment']['blue_laws1'] . "," . $this->data['RGroceryDepartment']['blue_laws2'];
            
            if ($this->RGroceryDepartment->save($this->request->data)) {
                $this->Session->setFlash(__('Department Information Is saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Department Information Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('RGroceryDepartment.' . $this->RGroceryDepartment->primaryKey => $id));
            $this->request->data = $this->RGroceryDepartment->find('first', $options);
        }
        $corporations = $this->RGroceryDepartment->Corporation->find('list');
        $stores = $this->RGroceryDepartment->Store->find('list');
        $this->set(compact('corporations', 'stores'));
    }

    public function admin_delete($id = null) {
        $this->Setredirect();
        $this->RGroceryDepartment->id = $id;
        if (!$this->RGroceryDepartment->exists()) {
            throw new NotFoundException(__('Invalid r grocery department'), 'default', array('class' => 'error'));
        }
     //   $this->request->allowMethod('post', 'delete');
        if ($this->RGroceryDepartment->delete()) {
            $this->Session->setFlash(__('The r grocery department has been deleted.'));
        } else {
            $this->Session->setFlash(__('The r grocery department could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
