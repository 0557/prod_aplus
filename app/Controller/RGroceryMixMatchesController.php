<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryMixMatches Controller
 *
 * @property RGroceryMixMatch $RGroceryMixMatch
 * @property PaginatorComponent $Paginator
 */
class RGroceryMixMatchesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->paginate = array('conditions' => array(/*'RGroceryMixMatch.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryMixMatch.store_id' => $_SESSION['store_id']), "limit" => 10, "order" => "RGroceryMixMatch.id DESC");
        $this->set('rGroceryMixMatches', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
           $this->Setredirect();
        if (!$this->RGroceryMixMatch->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery mix match'));
        }
        $options = array('conditions' => array('RGroceryMixMatch.' . $this->RGroceryMixMatch->primaryKey => $id));
        $this->set('rGroceryMixMatch', $this->RGroceryMixMatch->find('first', $options));
    }

    public function admin_add() {
           $this->Setredirect();
        $this->loadModel('RGroceryDepartment');
        $this->loadModel('RGroceryItem');
        $this->loadModel('RGroceryMixMatchParticipate');
        $this->loadModel('RGroceryMixMatchPackage');
        if ($this->request->is('post')) {

            $this->RGroceryMixMatch->create();
            $datasource = $this->RGroceryMixMatch->getDataSource();
            try {
                $datasource->begin();
                if (empty($this->request->data['RGroceryMixMatch']['start_date'])) {
                    $this->request->data['RGroceryMixMatch']['start_date'] = date('d/m/Y - H:m');
                }
                if (empty($this->request->data['RGroceryMixMatch']['end_date'])) {
                    $this->request->data['RGroceryMixMatch']['end_date'] = date('d/m/Y - H:m');
                }
                $this->request->data['RGroceryMixMatch']['store_id'] = $_SESSION['store_id'];
              //  $this->request->data['RGroceryMixMatch']['corporation_id'] = $_SESSION['corporation_id'];
                $this->request->data['RGroceryMixMatch']['start_date'] = $this->Default->Getdate($this->data['RGroceryMixMatch']['start_date']);
                $this->request->data['RGroceryMixMatch']['end_date'] = $this->Default->Getdate($this->data['RGroceryMixMatch']['end_date']);
                if (!$this->RGroceryMixMatch->save($this->request->data)) {
                    throw new Exception();
                }

                $get_mix_match_id = $this->RGroceryMixMatch->getLastInsertId();

                if (isset($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id']) && !empty($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id'][0])) {
                    $product_save = array();
                    $product_save['RGroceryMixMatchParticipate']['r_grocery_mix_match_id'] = $get_mix_match_id;
                    foreach ($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id'] as $key => $value) {
                        // Code for find Item Id
                        $item = $this->RGroceryItem->find('first', array('conditions' => array(/*'RGroceryItem.corporation_id' => $_SESSION['corporation_id'], */'RGroceryItem.store_id' => $_SESSION['store_id'], 'RGroceryItem.plu_no' => $value), 'fields' => array('RGroceryItem.id')));
                        if (!empty($item)) {
                            $this->RGroceryMixMatchParticipate->create();
                            $product_save['RGroceryMixMatchParticipate']['r_grocery_department_id'] = $this->data['RGroceryMixMatchParticipate']['r_grocery_department_id'][$key];
                            $product_save['RGroceryMixMatchParticipate']['r_grocery_item_id'] = $item['RGroceryItem']['id'];
                            $product_save['RGroceryMixMatchParticipate']['description'] = $this->data['RGroceryMixMatchParticipate']['description'][$key];
                            if (!$this->RGroceryMixMatchParticipate->save($product_save)) {
                                throw new Exception();
                            }
                        } else {
                            throw new Exception( ++$key);
                        }
                    }
                }

                if (isset($this->request->data['RGroceryMixMatchPackage']['quantity']) && !empty($this->request->data['RGroceryMixMatchPackage']['quantity'][0])) {
                    $product_save = array();
                    $product_save['RGroceryMixMatchPackage']['r_grocery_mix_match_id'] = $get_mix_match_id;
                    foreach ($this->request->data['RGroceryMixMatchPackage']['quantity'] as $key1 => $value1) {
                        $this->RGroceryMixMatchPackage->create();
                        $product_save['RGroceryMixMatchPackage']['quantity'] = $value1;
                        $product_save['RGroceryMixMatchPackage']['numeric_field'] = $this->data['RGroceryMixMatchPackage']['numeric_field'][$key1];
                        $product_save['RGroceryMixMatchPackage']['price'] = $this->data['RGroceryMixMatchPackage']['price'][$key1];
                        $product_save['RGroceryMixMatchPackage']['numeric'] = $this->data['RGroceryMixMatchPackage']['numeric'][$key1];
                        if (!$this->RGroceryMixMatchPackage->save($product_save)) {
                            throw new Exception();
                        }
                    }
                }
                if ($datasource->commit()) {
                    $this->Session->setFlash(__('The Mix Match Inforamtion has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                }
            } catch (Exception $ex) {
                // numeric for department Id Exception
                if (is_numeric($ex->getMessage())) {
                    $product_no = $ex->getMessage();
                    //  echo $product_no;  die;
                    $this->Session->setFlash(__("Invalid Item/Plu For Items /UPC Participate  No. $product_no"), 'default', array('class' => 'error'));
                } else {
                    $this->Session->setFlash(__('The Mix Match Inforamtion could not be saved. Please, try again.'), 'default', array('class' => 'error'));
                }
                $datasource->rollback();
            }
        }
        $rGroceryDepartment = $this->RGroceryDepartment->find('list', array('conditions' => array(/*'RGroceryDepartment.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryDepartment.store_id' => $_SESSION['store_id'])));
        $this->set(compact('rGroceryDepartment'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
           $this->Setredirect();

        if (!$this->RGroceryMixMatch->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery mix match'));
        }
        $this->loadModel('RGroceryDepartment');
        $this->loadModel('RGroceryItem');
        $this->loadModel('RGroceryMixMatchParticipate');
        $this->loadModel('RGroceryMixMatchPackage');
        if ($this->request->is(array('post', 'put'))) {
            $this->RGroceryMixMatch->create();
            $datasource = $this->RGroceryMixMatch->getDataSource();
            try {
                $datasource->begin();
                $this->request->data['RGroceryMixMatch']['start_date'] = $this->Default->Getdate($this->data['RGroceryMixMatch']['start_date']);
                $this->request->data['RGroceryMixMatch']['end_date'] = $this->Default->Getdate($this->data['RGroceryMixMatch']['end_date']);
                if (!$this->RGroceryMixMatch->save($this->request->data)) {
                    throw new Exception();
                }

                if (isset($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id']) && !empty($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id'][0])) {
                    $this->RGroceryMixMatchParticipate->deleteAll(array('RGroceryMixMatchParticipate.r_grocery_mix_match_id' => $id), false);
                    $product_save = array();
                    $product_save['RGroceryMixMatchParticipate']['r_grocery_mix_match_id'] = $id;
                    foreach ($this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id'] as $key => $value) {
                        // Code for find Item Id
                        $item = $this->RGroceryItem->find('first', array('conditions' => array(/*'RGroceryItem.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryItem.store_id' => $_SESSION['store_id'], 'RGroceryItem.plu_no' => $value), 'fields' => array('RGroceryItem.id')));
                        if (!empty($item)) {
                            $this->RGroceryMixMatchParticipate->create();
                            $product_save['RGroceryMixMatchParticipate']['r_grocery_department_id'] = $this->data['RGroceryMixMatchParticipate']['r_grocery_department_id'][$key];
                            $product_save['RGroceryMixMatchParticipate']['r_grocery_item_id'] = $item['RGroceryItem']['id'];
                            $product_save['RGroceryMixMatchParticipate']['description'] = $this->data['RGroceryMixMatchParticipate']['description'][$key];
                            if (!$this->RGroceryMixMatchParticipate->save($product_save)) {
                                throw new Exception();
                            }
                        } else {
                            throw new Exception( ++$key);
                        }
                    }
                }

                if (isset($this->request->data['RGroceryMixMatchPackage']['quantity']) && !empty($this->request->data['RGroceryMixMatchPackage']['quantity'][0])) {

                    $this->RGroceryMixMatchPackage->deleteAll(array('RGroceryMixMatchPackage.r_grocery_mix_match_id' => $id), false);
                    $product_save = array();
                    $product_save['RGroceryMixMatchPackage']['r_grocery_mix_match_id'] = $id;
                    foreach ($this->request->data['RGroceryMixMatchPackage']['quantity'] as $key1 => $value1) {
                        $this->RGroceryMixMatchPackage->create();
                        $product_save['RGroceryMixMatchPackage']['quantity'] = $value1;
                        $product_save['RGroceryMixMatchPackage']['numeric_field'] = $this->data['RGroceryMixMatchPackage']['numeric_field'][$key1];
                        $product_save['RGroceryMixMatchPackage']['price'] = $this->data['RGroceryMixMatchPackage']['price'][$key1];
                        $product_save['RGroceryMixMatchPackage']['numeric'] = $this->data['RGroceryMixMatchPackage']['numeric'][$key1];
                        if (!$this->RGroceryMixMatchPackage->save($product_save)) {
                            throw new Exception();
                        }
                    }
                }
                if ($datasource->commit()) {
                    $this->Session->setFlash(__('The Mix Match Inforamtion has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                }
            } catch (Exception $ex) {
                // numeric for department Id Exception
                if (is_numeric($ex->getMessage())) {
                    $product_no = $ex->getMessage();
                    $this->Session->setFlash(__("Invalid Item/Plu For Items /UPC Participate  No. $product_no"), 'default', array('class' => 'error'));
                } else {
                    $this->Session->setFlash(__('The Mix Match Inforamtion could not be saved. Please, try again.'), 'default', array('class' => 'error'));
                }
                $datasource->rollback();
            }
        } else {
            $options = array('conditions' => array('RGroceryMixMatch.' . $this->RGroceryMixMatch->primaryKey => $id));
            $this->request->data = $this->RGroceryMixMatch->find('first', $options);
        }
        $rGroceryDepartment = $this->RGroceryDepartment->find('list', array('conditions' => array(/*'RGroceryDepartment.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryDepartment.store_id' => $_SESSION['store_id'])));
        $this->set(compact('rGroceryDepartment'));
    }

    public function admin_delete($id = null) {
           $this->Setredirect();
        $this->loadModel('RGroceryMixMatchParticipate');
        $this->loadModel('RGroceryMixMatchPackage');
        $this->RGroceryMixMatch->id = $id;
        if (!$this->RGroceryMixMatch->exists()) {
            throw new NotFoundException(__('Invalid r grocery mix match'));
        }
        if ($this->RGroceryMixMatch->delete()) {
            $this->RGroceryMixMatchParticipate->deleteAll(array('RGroceryMixMatchParticipate.r_grocery_mix_match_id' => $id), false);
            $this->RGroceryMixMatchPackage->deleteAll(array('RGroceryMixMatchPackage.r_grocery_mix_match_id' => $id), false);
            $this->Session->setFlash(__('The r grocery mix match has been deleted.'));
        } else {
            $this->Session->setFlash(__('The r grocery mix match could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function autocomplete($departmentId) {
        $this->autoRender = false;
        $this->loadModel('RGroceryItem');
        $term = $_GET['term'];
        $item = $this->RGroceryItem->find('all', array('conditions' => array(/*'RGroceryItem.corporation_id' => $_SESSION['corporation_id'],*/'RGroceryItem.r_grocery_department_id' => $departmentId, 'RGroceryItem.store_id' => $_SESSION['store_id'], 'RGroceryItem.plu_no LIKE' => "$term%"), 'fields' => array('RGroceryItem.plu_no', 'RGroceryItem.description')));
        $i = 0;
        $data = array();
        foreach ($item as $data_value) {
            $data[] = array(
                'label' => $data_value['RGroceryItem']['plu_no'],
                'value' => $data_value['RGroceryItem']['plu_no'],
                'desc' => $data_value['RGroceryItem']['description'],
            );
            //  $data[$i]['dec'] = $data_value['RGroceryItem']['description'];
            $i++;
        }
   
        echo json_encode($data);
    }

}
