<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class RGroceryPromotionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
          $this->Setredirect();
        $this->RGroceryPromotion->recursive = 0;
        $this->paginate = array('conditions' => array(/*'RGroceryPromotion.corporation_id' => $_SESSION['corporation_id'],*/ 'RGroceryPromotion.store_id' => $_SESSION['store_id']), "limit" => 15, "order" => "RGroceryPromotion.id DESC");
        $this->set('rGroceryPromotion', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
          $this->Setredirect();
        if (!$this->RGroceryPromotion->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery Promotion'));
        }
        $options = array('conditions' => array('RGroceryPromotion.' . $this->RGroceryPromotion->primaryKey => $id));
        $this->set('rGroceryPromotion', $this->RGroceryPromotion->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
          $this->Setredirect();
        if ($this->request->is('post')) {

         //   $this->request->data['RGroceryPromotion']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RGroceryPromotion']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RGroceryPromotion']['str_disc_period'] = $this->Default->Getdate($this->data['RGroceryPromotion']['str_disc_period']);
            $this->request->data['RGroceryPromotion']['end_disc_period'] = $this->Default->Getdate($this->data['RGroceryPromotion']['end_disc_period']);
            $this->RGroceryPromotion->create();
            if ($this->RGroceryPromotion->save($this->request->data)) {
                $this->Session->setFlash(__('The R Grocery Promotion has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('R Grocery Promotion Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
         
            }
        }
        $this->loadModel('RGroceryItem');
        $rGroceryPromotion = $this->RGroceryItem->find('list',array('fields'=>array('RGroceryItem.plu_no','RGroceryItem.plu_no'),'group'=>'RGroceryItem.plu_no'));
        $this->set(compact('rGroceryPromotion'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
          $this->Setredirect();
        if (!$this->RGroceryPromotion->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery Promotion'));
        }
        if ($this->request->is(array('post', 'put'))) {
        //    $this->request->data['RGroceryPromotion']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RGroceryPromotion']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RGroceryPromotion']['str_disc_period'] = $this->Default->Getdate($this->data['RGroceryPromotion']['str_disc_period']);
            $this->request->data['RGroceryPromotion']['end_disc_period'] = $this->Default->Getdate($this->data['RGroceryPromotion']['end_disc_period']);
            if ($this->RGroceryPromotion->save($this->request->data)) {
                $this->Session->setFlash(__('The R Grocery Promotion has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('R Grocery Promotion Could Not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('RGroceryPromotion.' . $this->RGroceryPromotion->primaryKey => $id));
            $this->request->data = $this->RGroceryPromotion->find('first', $options);
        }
          $this->loadModel('RGroceryItem');
        $rGroceryPromotion = $this->RGroceryItem->find('list',array('fields'=>array('RGroceryItem.plu_no','RGroceryItem.plu_no'),'group'=>'RGroceryItem.plu_no'));
        $this->set(compact('rGroceryPromotion'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        
        $this->RGroceryPromotion->id = $id;
        if (!$this->RGroceryPromotion->exists()) {
            throw new NotFoundException(__('Invalid r grocery Promotion'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RGroceryPromotion->delete()) {
            $this->Session->setFlash(__('The R Grocery Promotion has been deleted.'));
        } else {
            $this->Session->setFlash(__('R Grocery Promotion Could Not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
