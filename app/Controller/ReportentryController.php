<?php

ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class ReportentryController extends AppController {

    //    public $components = array('Paginator', 'Mpdf.Mpdf');
    public $components = array('Paginator', 'RequestHandler');

    public function admin_index() {

    }
	public function admin_daily_sales($id=null) {
		$this->loadModel('TpReports');
		$this->loadModel('Rproduct');
		$this->set('daily', 'active');
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{
		if($id){
			if($this->TpReports->delete($id)){
				$this->Session->setFlash(__('Deleted successfully.'));
					 return $this->redirect(array('action' => 'daily_sales'));
			}
		}
		
		if($this->request->is('post')){
			if($this->request->data['TpReports']['createdate']){
				$newdate = explode('-',$this->request->data['TpReports']['createdate']);
				$this->request->data['TpReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			$this->request->data['TpReports']['company_id'] = $this->Session->read('Auth.User.company_id');
			$this->request->data['TpReports']['store_id'] = $this->Session->read('stores_id');
			
			$this->request->data['TpReports']['created_at'] = date('Y-m-d');
			$this->request->data['TpReports']['updated_at'] = date('Y-m-d');
			if($this->TpReports->save($this->data)){
					 $this->Session->setFlash(__('Submited new data successfully.'));
					 return $this->redirect(array('action' => 'daily_sales'));
			}else{
				 $this->Session->setFlash(__('Could not save.'));
			}
		}
		
			
			$options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

				$list = $this->TpReports->find('all', $options);
				$this->set('list',$list);
				
			$options = array('conditions' => array('Rproduct.company_id'=> $this->Session->read('Auth.User.company_id'),'Rproduct.store_id'=>$this->Session->read('stores_id')), 'order'=> array('Rproduct.id'=>'desc'),'fields'=>array('Rproduct.name','Rproduct.name')); 
							

				$plist = $this->Rproduct->find('list', $options);
				$this->set('plist',$plist);
		}
    }
		public function admin_edit_daily_sales($id=null) {
				$this->loadModel('TpReports');
			$options = array('conditions' => array('id'=>$id,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

				$list = $this->TpReports->find('first', $options);
				$this->set('data',$list);
				
				if($this->request->is('post')){
			if($this->request->data['TpReports']['createdate']){
				$newdate = explode('-',$this->request->data['TpReports']['createdate']);
				$this->request->data['TpReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			
			$this->request->data['TpReports']['updated_at'] = date('Y-m-d');
			if($this->TpReports->save($this->data)){
					 $this->Session->setFlash(__('Data Saved successfully.'));
					 return $this->redirect(array('action' => 'daily_sales'));
			}else{
				 $this->Session->setFlash(__('Could not save.'));
			}
		}
		
		
		}
		
	public function admin_tank_report($id=null) {
		$this->set('treport', 'active');
		$this->loadModel('TankReports');
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{
			if($id){
			if($this->TankReports->delete($id)){
				$this->Session->setFlash(__('Deleted successfully.'));
					 return $this->redirect(array('action' => 'tank_report'));
			}
		
		}
		if($this->request->is('post')){
			if($this->request->data['TankReports']['createdate']){
				$newdate = explode('-',$this->request->data['TankReports']['createdate']);
				$this->request->data['TankReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			$this->request->data['TankReports']['company_id'] = $this->Session->read('Auth.User.company_id');
			$this->request->data['TankReports']['store_id'] = $this->Session->read('stores_id');
			
			$this->request->data['TankReports']['created_at'] = date('Y-m-d');
			$this->request->data['TankReports']['updated_at'] = date('Y-m-d');
			if($this->TankReports->save($this->data)){
					 $this->Session->setFlash(__('Submited new data successfully.'));
					 return $this->redirect(array('action' => 'tank_report'));
			}else{
				 $this->Session->setFlash(__('Could not save.'));
			}
		}
		$options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

				$list = $this->TankReports->find('all', $options);
				$this->set('list',$list);
		}
    }
	public function admin_edit_tank_report($id=null) { 
		$this->set('treport', 'active');
		$this->loadModel('TankReports');
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{
			
		
		if($this->request->is('post')){
			
			if($this->request->data['TankReports']['createdate']){
				$newdate = explode('-',$this->request->data['TankReports']['createdate']);
				$this->request->data['TankReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			$this->request->data['TankReports']['updated_at'] = date('Y-m-d');
			if($this->TankReports->save($this->data)){
					 $this->Session->setFlash(__('Data saved successfully.'));
					 return $this->redirect(array('action' => 'tank_report'));
			}else{
				 $this->Session->setFlash(__('Could not save.'));
			}
		}
		
		$options = array('conditions' => array('id'=>$id,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

				$list = $this->TankReports->find('first', $options);
				$this->set('data',$list);
		}
    }
	public function admin_mop($id=null) {
		$this->set('mop', 'active');
		$this->loadModel('MopReports');
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{
			
				if($id){
				if($this->MopReports->delete($id)){
					$this->Session->setFlash(__('Deleted successfully.'));
						 return $this->redirect(array('action' => 'mop'));
				}
				}
			if($this->request->is('post')){
				
				if($this->request->data['MopReports']['createdate']){
					$newdate = explode('-',$this->request->data['MopReports']['createdate']);
					$this->request->data['MopReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
				}
				$this->request->data['MopReports']['company_id'] = $this->Session->read('Auth.User.company_id');
				$this->request->data['MopReports']['store_id'] = $this->Session->read('stores_id');
				
				$this->request->data['MopReports']['created_at'] = date('Y-m-d');
				$this->request->data['MopReports']['updated_at'] = date('Y-m-d');
				if($this->MopReports->save($this->data)){
						 $this->Session->setFlash(__('Submited new data successfully.'));
						 return $this->redirect(array('action' => 'mop'));
				}else{
					 $this->Session->setFlash(__('Could not save.'));
				}
			}
			$options = array('conditions' => array('company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

					$list = $this->MopReports->find('all', $options);
					$this->set('list',$list);
			}
		
	}
	public function admin_edit_mop($id=null) {
		$this->set('mop', 'active');
		$this->loadModel('MopReports');
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 
				//	return $this->redirect(array('action' => 'daily_sales'));
		}else{
		if($this->request->is('post')){
			if($this->request->data['MopReports']['createdate']){
				$newdate = explode('-',$this->request->data['MopReports']['createdate']);
				$this->request->data['MopReports']['createdate'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			$this->request->data['MopReports']['updated_at'] = date('Y-m-d');
			if($this->MopReports->save($this->data)){
					 $this->Session->setFlash(__('Data saved successfully.'));
					 return $this->redirect(array('action' => 'mop'));
			}else{
				 $this->Session->setFlash(__('Could not save.'));
			}
		}
		$options = array('conditions' => array('id'=>$id,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc')); 

				$list = $this->MopReports->find('first', $options);
				$this->set('data',$list);
		}
    }
	
	
public function admin_filterdata()
	{
		$this->loadModel('TankReports');
		$this->loadModel('MopReports');
		$this->loadModel('TpReports');
		/* TpReports */
		//$this->autoRender = false;
		
		if($this->data['datav']){
		

		 $newdate = explode('-',$this->data['datav']);
				$datev = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			$this->set('tpdate', $datev);
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TpReports->find('all', $options);
		$this->set('treport', $list);
		
		/* MopReports */
		
				$this->set('mpdate', $datev);
			
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->MopReports->find('all', $options);
		$this->set('mreports',$list);
		
		/* TankReports */
		
				
			$this->set('tnpdate', $datev);
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TankReports->find('all', $options);
		$this->set('tnreports', $list);
	
		}else{
			
			$this->set('tnpdate', '');
			$this->set('mpdate', '');
			$this->set('tpdate', '');
		
		}
	}
	
	/* gas dashboard1 filtering */
	
	public function admin_filterdata1()
	{
		$this->loadModel('TankReports');
		$this->loadModel('MopReports');
		$this->loadModel('TpReports');
		/* TpReports */
		//$this->autoRender = false;
		
		if($this->data['datav']){
		

		 $newdate = explode('-',$this->data['datav']);
				$datev = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			$this->set('tpdate', $datev);
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TpReports->find('all', $options);
		$this->set('treport', $list);
		
		/* MopReports */
		
				$this->set('mpdate', $datev);
			
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->MopReports->find('all', $options);
		$this->set('mreports',$list);
		
		/* TankReports */
		
				
			$this->set('tnpdate', $datev);
	
		$options = array('conditions' => array('createdate'=>$datev,'company_id'=> $this->Session->read('Auth.User.company_id'),'store_id'=>$this->Session->read('stores_id')), 'order'=> array('id'=>'desc'));
		$list = $this->TankReports->find('all', $options);
		$this->set('tnreports', $list);
	
		}else{
			
			$this->set('tnpdate', '');
			$this->set('mpdate', '');
			$this->set('tpdate', '');
		
		}
	}
}
