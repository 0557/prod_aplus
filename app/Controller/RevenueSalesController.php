<?php
error_reporting(0);
App::uses('AppController', 'Controller');
 
 
class RevenueSalesController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session');
	
    public function admin_index() {	
		
		$this->Setredirect();   
		//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';die;		
		$this->loadModel('RevenueSale');		
		$this->loadModel('RubyHeader');		
		$this->loadModel('RubyUprodt');			
			
			$first_day_this_month= new DateTime('first day of this month');
			$first_day=$first_day_this_month->format('m/d/Y');
			$last_day_this_month= new DateTime('last day of this month');
			$last_day=$last_day_this_month->format('m/d/Y');
			$full_date =$first_day.' - '.$last_day;
			$fdate=$first_day_this_month->format('Y-m-d');
			$tdate=$last_day_this_month->format('Y-m-d');				
			
			$this->Session->write('full_date',$full_date);	
			$this->Session->write('from_date',$fdate);	
			$this->Session->write('to_date',$tdate);	
		    $conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
			
			if($this->request->is('post')){	
				//echo '<pre>';print_r($this->request);die;
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['RGroceryInvoice']['full_date']!= "" ) {			
				
				$full_date=$this->request->data['RGroceryInvoice']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$fdate);	
				$this->Session->write('to_date',$tdate);				   	
						
				} else {
				$this->Session->delete('from_date');
				$this->Session->delete('from_date');
				$this->Session->delete('to_date');		
				}				
				
						
			}	
			
			$full_date=$this->Session->read('full_date');
			$from_date=$this->Session->read('from_date');
			$to_date=$this->Session->read('to_date');
			//echo $from_date=$this->Session->read('from_date');echo '<br>';
			//echo $to_date=$this->Session->read('to_date');die;
					
			if(isset($from_date) && $from_date!=''){
				$conditions['RubyHeader.ending_date_time >=']=$from_date;				
			}
			
			if(isset($to_date) && $to_date!=''){
				$conditions['RubyHeader.ending_date_time <=']=$to_date;				
			}	
		$this->set('full_date',$full_date);	
		
		$this->RevenueSale->virtualFields['total_inside_fuel_sales'] = 'SUM(RevenueSale.total_inside_fuel_sales)';
		$this->RevenueSale->virtualFields['total_outside_fuel_sales'] = 'SUM(RevenueSale.total_outside_fuel_sales)';
		$this->RevenueSale->virtualFields['total_merchandise_sales'] = 'SUM(RevenueSale.total_merchandise_sales)';
		$RevenueSales = $this->RevenueSale->find('all',array('conditions' => $conditions));
		//echo '<pre>';print_r($RevenueSales); die;	
        $this->set('revenuesales', $RevenueSales);
		
		
		$this->loadModel('RubyTottaxe');  		
		$this->RubyTottaxe->virtualFields['total_taxes'] = 'SUM(RubyTottaxe.total_taxes)';
		$this->RubyTottaxe->virtualFields['total_taxable_sales'] = 'SUM(RubyTottaxe.total_taxable_sales)';
		
		$OtherrevenueSales = $this->RubyTottaxe->find('all',array('conditions' => $conditions));		
		//pr($OtherrevenueSales);die;
        $this->set('otherrevenuesales', $OtherrevenueSales);
		
		
		
		$this->loadModel('RubyMemo1');  	
		$RubyMemo1=$this->RubyMemo1->find('all',array('conditions' => $conditions));		
		//pr($RubyMemo1);die;
        $this->set('RubyMemo1', $RubyMemo1);
		
		
		$this->loadModel('RubyPmtout');  	
		$RubyPmtout=$this->RubyPmtout->find('all',array('conditions' => $conditions));		
		//pr($RubyMemo1);die;
        $this->set('RubyPmtout', $RubyPmtout);
		
		
		
		$this->loadModel('RubyTotsum');  	
		$RubyTotsum=$this->RubyTotsum->find('all',array('conditions' => $conditions));		
		//pr($RubyTotsum);die;
        $this->set('RubyTotsum', $RubyTotsum);
		
		$this->loadModel('RubyTottaxe');  	
		$RubyTottaxe=$this->RubyTottaxe->find('all',array('conditions' => $conditions));		
		//pr($RubyTottaxe);die;
        $this->set('RubyTottaxe', $RubyTottaxe);
		
		$this->loadModel('RubyMemo3');  	
		$RubyMemo3=$this->RubyMemo3->find('all',array('conditions' => $conditions));		
	    //pr($RubyMemo3);die;
        $this->set('RubyMemo3',$RubyMemo3);
		
		$this->loadModel('RubyTotnet');  	
		$RubyTotnet=$this->RubyTotnet->find('all',array('conditions' => $conditions));		
	    //pr($RubyTotnet);die;
        $this->set('RubyTotnet',$RubyTotnet);
		
		$arrayofmopname =array('CASH','CREDIT','FOODSTAMP','LOTTERY');
		$conditions1= array(
					'RubyTotmop.mop_name' => $arrayofmopname,
				);	
		$conditionstotmop=array_merge($conditions,$conditions1);	
		$this->loadModel('RubyTotmop');  	
		$RubyTotmop=$this->RubyTotmop->find('all',array('conditions' => $conditionstotmop));		
	    //pr($RubyTotmop);die;
        $this->set('RubyTotmop',$RubyTotmop);		
		
		$arrayofcardname =array('MASTERCARD','VISA','AMEX','DISCOVER','WEX','VOYAGER');
		$conditions2= array(
					'RubyTotcard.card_name' => $arrayofcardname,
				);	
		$conditionstotcards=array_merge($conditions,$conditions2);	
		$this->loadModel('RubyTotcard');  	
		$RubyTotcard=$this->RubyTotcard->find('all',array('conditions' => $conditionstotcards));		
	    //pr($RubyTotcard);die;
        $this->set('RubyTotcard',$RubyTotcard);
		
		
		
		
		$storeId = $this->getStoreId();			
		$prodList = $this->setFilterProductNumber($storeId);		
		
		if(!empty($prodList)) {
			$conditionsrpl['RubyUprodt.fuel_product_number'] = array_keys($prodList);
		} else {
			$conditionsrpl['RubyUprodt.fuel_product_number'] = array();
		}	
			
		$conditionsr1 = array('RubyUprodt.fmop1' => '1' );		
		$conditionsmoneyflow1=array_merge($conditions,$conditionsrpl,$conditionsr1);	
		//pr($conditionsmoneyflow1);
		$RubyUprodt1 = $this->RubyUprodt->find('all', array(
			'conditions' => $conditionsmoneyflow1
		));		
		//pr($RubyUprodt1);
		$this->set('rubyUprodts1', $RubyUprodt1);
		
		
		
		
		$conditionsr2= array('RubyUprodt.fmop1' => '2' );		
		$conditionsmoneyflow2=array_merge($conditions,$conditionsrpl,$conditionsr2);	
		//pr($conditionsmoneyflow2);
		$RubyUprodt2= $this->RubyUprodt->find('all', array(
			'conditions' => $conditionsmoneyflow2
		));		
		//pr($RubyUprodt2); die;
		$this->set('rubyUprodts2', $RubyUprodt2);
		
		
		
		//echo $this->Session->read('pos_type');die;
		if($this->Session->read('pos_type')=='ruby2'){				
			
			$this->loadModel('Ruby2Summary');				
	    		
			$summary2_conditions= array('Ruby2Summary.store_id' =>$this->Session->read('stores_id'));	
			
			if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$summary2_conditions= array('Ruby2Summary.store_id' =>$this->Session->read('stores_id'),'Ruby2Summary.period_end_date >=' =>$from_date, 'Ruby2Summary.period_end_date <=' =>$to_date);	
			}
		
			
			//print_r($summary2_conditions);die;
		    $Ruby2Summary = $this->Ruby2Summary->find('all',array('conditions' => $summary2_conditions));
		    //echo '<pre>';print_r($Ruby2Summary); die;	
            $this->set('ruby2summary',$Ruby2Summary);		
		 
		
		/*$this->Ruby2Summary->virtualFields['fuel_sales'] = 'SUM(Ruby2Summary.fuel_sales)';
		$this->Ruby2Summary->virtualFields['merch_sales'] = 'SUM(Ruby2Summary.merch_sales)';
		$this->Ruby2Summary->virtualFields['total_payment_out'] = 'SUM(Ruby2Summary.total_payment_out)';
		$this->Ruby2Summary->virtualFields['total_payment_in'] = 'SUM(Ruby2Summary.total_payment_in)';
		$this->Ruby2Summary->virtualFields['total_taxes'] = 'SUM(Ruby2Summary.total_taxes)';
		$this->Ruby2Summary->virtualFields['item_count'] = 'SUM(Ruby2Summary.item_count)';
		$this->Ruby2Summary->virtualFields['customer_count'] = 'SUM(Ruby2Summary.customer_count)';
		$this->Ruby2Summary->virtualFields['safe_drop_count'] = 'SUM(Ruby2Summary.safe_drop_count)';
		$this->Ruby2Summary->virtualFields['safe_drop_amount'] = 'SUM(Ruby2Summary.safe_drop_amount)';
		$this->Ruby2Summary->virtualFields['void_line_count'] = 'SUM(Ruby2Summary.void_line_count)';
		$this->Ruby2Summary->virtualFields['void_line_amount'] = 'SUM(Ruby2Summary.void_line_amount)';	
		$this->Ruby2Summary->virtualFields['start_insidegrand'] = 'SUM(Ruby2Summary.start_insidegrand)';
		$this->Ruby2Summary->virtualFields['end_insidegrand'] = 'SUM(Ruby2Summary.end_insidegrand)';
		$this->Ruby2Summary->virtualFields['diff_insidegrand'] = 'SUM(Ruby2Summary.diff_insidegrand)';		
		$this->Ruby2Summary->virtualFields['start_insidesales'] = 'SUM(Ruby2Summary.start_insidesales)';
		$this->Ruby2Summary->virtualFields['end_insidesales'] = 'SUM(Ruby2Summary.end_insidesales)';
		$this->Ruby2Summary->virtualFields['diff_insidesales'] = 'SUM(Ruby2Summary.diff_insidesales)';		
		$this->Ruby2Summary->virtualFields['start_outsidegrand'] = 'SUM(Ruby2Summary.start_outsidegrand)';
		$this->Ruby2Summary->virtualFields['end_outsidegrand'] = 'SUM(Ruby2Summary.end_outsidegrand)';
		$this->Ruby2Summary->virtualFields['diff_outsidegrand'] = 'SUM(Ruby2Summary.diff_outsidegrand)';	
		$this->Ruby2Summary->virtualFields['start_outsidesales'] = 'SUM(Ruby2Summary.start_outsidesales)';
		$this->Ruby2Summary->virtualFields['end_outsidesales'] = 'SUM(Ruby2Summary.end_outsidesales)';
		$this->Ruby2Summary->virtualFields['diff_outsidesales'] = 'SUM(Ruby2Summary.diff_outsidesales)';	
		$this->Ruby2Summary->virtualFields['start_overallgrand'] = 'SUM(Ruby2Summary.start_overallgrand)';
		$this->Ruby2Summary->virtualFields['end_overallgrand'] = 'SUM(Ruby2Summary.end_overallgrand)';
		$this->Ruby2Summary->virtualFields['diff_overallgrand'] = 'SUM(Ruby2Summary.diff_overallgrand)';	
		$this->Ruby2Summary->virtualFields['start_overallsales'] = 'SUM(Ruby2Summary.start_overallsales)';
		$this->Ruby2Summary->virtualFields['end_overallsales'] = 'SUM(Ruby2Summary.end_overallsales)';
		$this->Ruby2Summary->virtualFields['diff_overallsales'] = 'SUM(Ruby2Summary.diff_overallsales)';*/		
				
		$Ruby2SummaryTotal = $this->Ruby2Summary->find('all',array('conditions' => $summary2_conditions));
		//echo '<pre>';print_r($Ruby2SummaryTotal);	die;
        $this->set('ruby2summarytotal',$Ruby2SummaryTotal[0]);	
		
		
				
		
		
		$this->loadModel('Ruby2Tax');
		/*$ruby2_tax_data = $this->Ruby2Tax->find('first',array('conditions'=>array('store_id' =>$this->Session->read('stores_id')),'order'=>array('Ruby2Tax.id DESC')));
		//echo '<pre>';print_r($ruby2_tax_data);die;
		$tax2_end_date= $ruby2_tax_data['Ruby2Tax']['period_end_date'];*/			
		$tax2_conditions= array('Ruby2Tax.tax_sysid' =>1,'Ruby2Tax.store_id' =>$this->Session->read('stores_id'));
		
		if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$tax2_conditions= array('Ruby2Tax.tax_sysid' =>1,'Ruby2Tax.store_id' =>$this->Session->read('stores_id'),'Ruby2Tax.period_end_date >=' =>$from_date, 'Ruby2Tax.period_end_date <=' =>$to_date);	
			}	
		
		        
		//$this->Ruby2Tax->virtualFields['tax_sysid'] = 'SUM(Ruby2Tax.tax_sysid)';
		//$this->Ruby2Tax->virtualFields['name'] = 'SUM(Ruby2Tax.name)';
		/*$this->Ruby2Tax->virtualFields['tax_rate'] = 'SUM(Ruby2Tax.tax_rate)';
		$this->Ruby2Tax->virtualFields['actual_tax_rate'] = 'SUM(Ruby2Tax.actual_tax_rate)';	*/	
		$this->Ruby2Tax->virtualFields['taxable_sales'] = 'SUM(Ruby2Tax.taxable_sales)';
		$this->Ruby2Tax->virtualFields['non_taxable_sales'] = 'SUM(Ruby2Tax.non_taxable_sales)';		
		$this->Ruby2Tax->virtualFields['sales_tax'] = 'SUM(Ruby2Tax.sales_tax)';
		$this->Ruby2Tax->virtualFields['refund_tax'] = 'SUM(Ruby2Tax.refund_tax)';
		$this->Ruby2Tax->virtualFields['net_tax'] = 'SUM(Ruby2Tax.net_tax)';	
		$Ruby2Tax = $this->Ruby2Tax->find('all',array('conditions' => $tax2_conditions));
		//echo '<pre>';print_r($Ruby2Tax); die;	
        $this->set('ruby2tax',$Ruby2Tax[0]);				
		
		$this->loadModel('Ruby2Network');	
		$network2_conditions= array('Ruby2Network.store_id' =>$this->Session->read('stores_id'));	
			
		if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$network2_conditions= array('Ruby2Network.store_id' =>$this->Session->read('stores_id'),'Ruby2Network.period_end_date >=' =>$from_date, 'Ruby2Network.period_end_date <=' =>$to_date);	
		}	
		
		$Ruby2Network = $this->Ruby2Network->find('all',array('conditions' => $network2_conditions,'group'=>'Ruby2Network.card_name'));
		//echo '<pre>';print_r($Ruby2Network); die;	
        $this->set('ruby2network',$Ruby2Network);	
		if(!$this->request->is('post')){	
		$this->set('full_date',$full_date);	
		}
		}
	
    }
	
		public function admin_create_pdf() {
		//echo '<pre>';print_r($this->request);die;
		$this->loadModel('RevenueSale');		
		$this->loadModel('RubyHeader');		
		$this->loadModel('RubyUprodt');	

		    $conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
		
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				$full_date=$this->request->data['RGroceryInvoice']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$fdate=$date[0];
				$formarr=explode('/',$fdate);
				$from_date=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$tdate=$date[1];
				$toarr=explode('/',$tdate);
				$to_date=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
						

					
			if(isset($from_date) && $from_date!=''){
				$conditions['RubyHeader.ending_date_time >=']=$from_date;				
			}
			
			if(isset($to_date) && $to_date!=''){
				$conditions['RubyHeader.ending_date_time <=']=$to_date;				
			}
		
		$this->set('full_date',$full_date);
		$this->RevenueSale->virtualFields['total_inside_fuel_sales'] = 'SUM(RevenueSale.total_inside_fuel_sales)';
		$this->RevenueSale->virtualFields['total_outside_fuel_sales'] = 'SUM(RevenueSale.total_outside_fuel_sales)';
		$this->RevenueSale->virtualFields['total_merchandise_sales'] = 'SUM(RevenueSale.total_merchandise_sales)';
		$RevenueSales = $this->RevenueSale->find('all',array('conditions' => $conditions));
        $this->set('revenuesales', $RevenueSales);
		
		
		$this->loadModel('RubyTottaxe');  		
		$this->RubyTottaxe->virtualFields['total_taxes'] = 'SUM(RubyTottaxe.total_taxes)';
		$this->RubyTottaxe->virtualFields['total_taxable_sales'] = 'SUM(RubyTottaxe.total_taxable_sales)';
		
		$OtherrevenueSales = $this->RubyTottaxe->find('all',array('conditions' => $conditions));		
        $this->set('otherrevenuesales', $OtherrevenueSales);
		
		
		
		$this->loadModel('RubyMemo1');  	
		$RubyMemo1=$this->RubyMemo1->find('all',array('conditions' => $conditions));		
        $this->set('RubyMemo1', $RubyMemo1);
		
		
		$this->loadModel('RubyPmtout');  	
		$RubyPmtout=$this->RubyPmtout->find('all',array('conditions' => $conditions));		
        $this->set('RubyPmtout', $RubyPmtout);
		
		
		
		$this->loadModel('RubyTotsum');  	
		$RubyTotsum=$this->RubyTotsum->find('all',array('conditions' => $conditions));		
        $this->set('RubyTotsum', $RubyTotsum);
		
		$this->loadModel('RubyTottaxe');  	
		$RubyTottaxe=$this->RubyTottaxe->find('all',array('conditions' => $conditions));		
        $this->set('RubyTottaxe', $RubyTottaxe);
		
		$this->loadModel('RubyMemo3');  	
		$RubyMemo3=$this->RubyMemo3->find('all',array('conditions' => $conditions));		
        $this->set('RubyMemo3',$RubyMemo3);
		
		$this->loadModel('RubyTotnet');  	
		$RubyTotnet=$this->RubyTotnet->find('all',array('conditions' => $conditions));		
        $this->set('RubyTotnet',$RubyTotnet);
		
		$arrayofmopname =array('CASH','CREDIT','FOODSTAMP','LOTTERY');
		$conditions1= array(
					'RubyTotmop.mop_name' => $arrayofmopname,
				);	
		$conditionstotmop=array_merge($conditions,$conditions1);	
		$this->loadModel('RubyTotmop');  	
		$RubyTotmop=$this->RubyTotmop->find('all',array('conditions' => $conditionstotmop));		
        $this->set('RubyTotmop',$RubyTotmop);		
		
		$arrayofcardname =array('MASTERCARD','VISA','AMEX','DISCOVER','WEX','VOYAGER');
		$conditions2= array(
					'RubyTotcard.card_name' => $arrayofcardname,
				);	
		$conditionstotcards=array_merge($conditions,$conditions2);	
		$this->loadModel('RubyTotcard');  	
		$RubyTotcard=$this->RubyTotcard->find('all',array('conditions' => $conditionstotcards));		
        $this->set('RubyTotcard',$RubyTotcard);
		
		
		
		
		$storeId = $this->getStoreId();			
		$prodList = $this->setFilterProductNumber($storeId);		
		
		if(!empty($prodList)) {
			$conditionsrpl['RubyUprodt.fuel_product_number'] = array_keys($prodList);
		} else {
			$conditionsrpl['RubyUprodt.fuel_product_number'] = array();
		}	
			
		$conditionsr1 = array('RubyUprodt.fmop1' => '1' );		
		$conditionsmoneyflow1=array_merge($conditions,$conditionsrpl,$conditionsr1);	
		$RubyUprodt1 = $this->RubyUprodt->find('all', array(
			'conditions' => $conditionsmoneyflow1
		));		
		$this->set('rubyUprodts1', $RubyUprodt1);
		
		
		
		
		$conditionsr2= array('RubyUprodt.fmop1' => '2' );		
		$conditionsmoneyflow2=array_merge($conditions,$conditionsrpl,$conditionsr2);	
		$RubyUprodt2= $this->RubyUprodt->find('all', array(
			'conditions' => $conditionsmoneyflow2
		));		
		$this->set('rubyUprodts2', $RubyUprodt2);
		
		
		
		
		if($this->Session->read('pos_type')=='ruby2'){				
			
			$this->loadModel('Ruby2Summary');	
			
			if($this->request->is('post')){	
			if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$summary2_conditions= array('Ruby2Summary.store_id' =>$this->Session->read('stores_id'),'Ruby2Summary.period_end_date >=' =>$from_date, 'Ruby2Summary.period_end_date <=' =>$to_date);	
			}
			}
			
			//print_r($summary2_conditions);die;
		    $Ruby2Summary = $this->Ruby2Summary->find('all',array('conditions' => $summary2_conditions));
		    //echo '<pre>';print_r($Ruby2Summary); die;	
            $this->set('ruby2summary',$Ruby2Summary);		
		 
		
		$this->Ruby2Summary->virtualFields['fuel_sales'] = 'SUM(Ruby2Summary.fuel_sales)';
		$this->Ruby2Summary->virtualFields['merch_sales'] = 'SUM(Ruby2Summary.merch_sales)';
		$this->Ruby2Summary->virtualFields['total_payment_out'] = 'SUM(Ruby2Summary.total_payment_out)';
		$this->Ruby2Summary->virtualFields['total_payment_in'] = 'SUM(Ruby2Summary.total_payment_in)';
		$this->Ruby2Summary->virtualFields['total_taxes'] = 'SUM(Ruby2Summary.total_taxes)';
		$this->Ruby2Summary->virtualFields['item_count'] = 'SUM(Ruby2Summary.item_count)';
		$this->Ruby2Summary->virtualFields['customer_count'] = 'SUM(Ruby2Summary.customer_count)';
		$this->Ruby2Summary->virtualFields['safe_drop_count'] = 'SUM(Ruby2Summary.safe_drop_count)';
		$this->Ruby2Summary->virtualFields['safe_drop_amount'] = 'SUM(Ruby2Summary.safe_drop_amount)';
		$this->Ruby2Summary->virtualFields['void_line_count'] = 'SUM(Ruby2Summary.void_line_count)';
		$this->Ruby2Summary->virtualFields['void_line_amount'] = 'SUM(Ruby2Summary.void_line_amount)';	
		$this->Ruby2Summary->virtualFields['start_insidegrand'] = 'SUM(Ruby2Summary.start_insidegrand)';
		$this->Ruby2Summary->virtualFields['end_insidegrand'] = 'SUM(Ruby2Summary.end_insidegrand)';
		$this->Ruby2Summary->virtualFields['diff_insidegrand'] = 'SUM(Ruby2Summary.diff_insidegrand)';		
		$this->Ruby2Summary->virtualFields['start_insidesales'] = 'SUM(Ruby2Summary.start_insidesales)';
		$this->Ruby2Summary->virtualFields['end_insidesales'] = 'SUM(Ruby2Summary.end_insidesales)';
		$this->Ruby2Summary->virtualFields['diff_insidesales'] = 'SUM(Ruby2Summary.diff_insidesales)';		
		$this->Ruby2Summary->virtualFields['start_outsidegrand'] = 'SUM(Ruby2Summary.start_outsidegrand)';
		$this->Ruby2Summary->virtualFields['end_outsidegrand'] = 'SUM(Ruby2Summary.end_outsidegrand)';
		$this->Ruby2Summary->virtualFields['diff_outsidegrand'] = 'SUM(Ruby2Summary.diff_outsidegrand)';	
		$this->Ruby2Summary->virtualFields['start_outsidesales'] = 'SUM(Ruby2Summary.start_outsidesales)';
		$this->Ruby2Summary->virtualFields['end_outsidesales'] = 'SUM(Ruby2Summary.end_outsidesales)';
		$this->Ruby2Summary->virtualFields['diff_outsidesales'] = 'SUM(Ruby2Summary.diff_outsidesales)';	
		$this->Ruby2Summary->virtualFields['start_overallgrand'] = 'SUM(Ruby2Summary.start_overallgrand)';
		$this->Ruby2Summary->virtualFields['end_overallgrand'] = 'SUM(Ruby2Summary.end_overallgrand)';
		$this->Ruby2Summary->virtualFields['diff_overallgrand'] = 'SUM(Ruby2Summary.diff_overallgrand)';	
		$this->Ruby2Summary->virtualFields['start_overallsales'] = 'SUM(Ruby2Summary.start_overallsales)';
		$this->Ruby2Summary->virtualFields['end_overallsales'] = 'SUM(Ruby2Summary.end_overallsales)';
		$this->Ruby2Summary->virtualFields['diff_overallsales'] = 'SUM(Ruby2Summary.diff_overallsales)';		
				
		$Ruby2SummaryTotal = $this->Ruby2Summary->find('all',array('conditions' => $summary2_conditions));
		//echo '<pre>';print_r($Ruby2SummaryTotal);	die;
        $this->set('ruby2summarytotal',$Ruby2SummaryTotal[0]);	
		
		
				
		
		
		$this->loadModel('Ruby2Tax');
		
		if($this->request->is('post')){	
		if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$tax2_conditions= array('Ruby2Tax.tax_sysid' =>1,'Ruby2Tax.store_id' =>$this->Session->read('stores_id'),'Ruby2Tax.period_end_date >=' =>$from_date, 'Ruby2Tax.period_end_date <=' =>$to_date);	
			}
		}
		        
		//print_r($tax2_conditions);die;
		$this->Ruby2Tax->virtualFields['taxable_sales'] = 'SUM(Ruby2Tax.taxable_sales)';
		$this->Ruby2Tax->virtualFields['non_taxable_sales'] = 'SUM(Ruby2Tax.non_taxable_sales)';		
		$this->Ruby2Tax->virtualFields['sales_tax'] = 'SUM(Ruby2Tax.sales_tax)';
		$this->Ruby2Tax->virtualFields['refund_tax'] = 'SUM(Ruby2Tax.refund_tax)';
		$this->Ruby2Tax->virtualFields['net_tax'] = 'SUM(Ruby2Tax.net_tax)';	
		$Ruby2Tax = $this->Ruby2Tax->find('all',array('conditions' => $tax2_conditions));
		//echo '<pre>';print_r($Ruby2Tax); die;	
        $this->set('ruby2tax',$Ruby2Tax[0]);
				
		
		$this->loadModel('Ruby2Network');
		
		if($this->request->is('post')){	
		if(isset($from_date) && $from_date!='' && isset($to_date) && $to_date!=''){
			$network2_conditions= array('Ruby2Network.store_id' =>$this->Session->read('stores_id'),'Ruby2Network.period_end_date >=' =>$from_date, 'Ruby2Network.period_end_date <=' =>$to_date);	
			}	
		}
		
		$Ruby2Network = $this->Ruby2Network->find('all',array('conditions' => $network2_conditions));
		//echo '<pre>';print_r($Ruby2Network); die;	
        $this->set('ruby2network',$Ruby2Network);	
		
		}
		
	
			$this->set('from_date', $from_date);
			$this->set('to_date', $to_date);
			$strname=$this->RubyUprodt->query("SELECT * FROM `stores` where id=".$storeId."");
		
			$storename=$strname[0]['stores']['name'];
			$this->set('storename', $storename);
			$this->layout = '/pdf/default1';
			$this->render('/Pdf/revenuesales');
		
    
			
			}
	
	
	
	
	public function admin_reset() {
	
		$this->Session->delete('PostSearch');
		$this->Session->delete('full_date');
		$this->Session->delete('from_date');
		$this->Session->delete('to_date');
		$redirect_url = array('controller' => 'revenue_sales', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}

	
	
	  public function admin_delete($id = null) {
        $this->Setredirect();       
        $this->RmsItem->id = $id;
        if (!$this->RmsItem->exists()) {
            throw new NotFoundException(__('Invalid Rms item'));
        }      
        if ($this->RmsItem->delete()) {           
          
            $this->Session->setFlash(__('The  Rms item has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Rms item could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
	
	
	

	
	
	
	
	
}
