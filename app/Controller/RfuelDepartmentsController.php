<?php
error_reporting(0);
App::uses('AppController', 'Controller');

/**
 * FuelDepartments Controller
 *
 * @property FuelDepartment $FuelDepartment
 * @property PaginatorComponent $Paginator
 */
class RfuelDepartmentsController extends AppController {

    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Setredirect();
        $this->set('rdepartments', 'active');
        $this->paginate = array('conditions' => array(/*'RfuelDepartment.corporation_id' => $_SESSION['corporation_id'],*/ 'RfuelDepartment.store_id' => $_SESSION['store_id']), "limit" => 5, "order" => "RfuelDepartment.id DESC");
        $this->set('rfuelDepartments', $this->Paginator->paginate());
    }
	
	public function admin_dashboard1()
	{			
		//$this->loadModel('TankReports');
		//$this->loadModel('MopReports');
		$this->loadModel('RubyUtiert');
		$this->loadModel('RubyHeader');
		//$this->loadModel('RubyFprod');
		$this->loadModel('RubyUprodt');
		$this->loadModel('RubyUtankt');
        $this->loadModel('RubyFtank');
		$this->loadModel('Houseacccustsale');
		//echo $this->Session->read('stores_id');
		
		if($this->request->is('post')){
		//echo '<pre>';  print_r($this->request->data);exit;
		$date = $this->request->data['expiry'];
		//echo $date;die;
		$conditions = array('Houseacccustsale.store_id' => $this->Session->read('stores_id'), 'Houseacccustsale.company_id' => $this->Session->read('Auth.User.company_id'),'Houseacccustsale.report_date' => $date);
		
		$Salereports= $this->Houseacccustsale->find('all',array('conditions' =>$conditions, "order" => "Houseacccustsale.id DESC"));	
		//echo '<pre>';  print_r($Salereports);exit;
		$this->set('Salereports', $Salereports);
		}
		
		if(!$this->Session->read('stores_id')){
			$this->Session->setFlash(__('Please select  store.')); 	
		}else{ 	
		$options = array('conditions'=>array('RubyHeader.store_id'=>$this->Session->read('stores_id')),'order'=> array('RubyHeader.id'=>'desc'),'limit'=>1);			
			if(@$this->data['expiry']){			
				$endate = $this->data['expiry'];			
			}else{           
				if($data = $this->RubyUtiert->find('first', $options)){				
					$endate = 	$data['RubyHeader']['ending_date_time'];
				}else{
					$endate =date('Y-m-d');				
				}
			}			

			/*TierReports */			
		$prodList = $this->setFilterProductNumber($this->Session->read('stores_id'));	
		//pr($prodList);die;
		$conditions = array();	
		$conditions = array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
		if($endate){ 			
		$conditions['RubyHeader.ending_date_time =']=$endate;
		}
		if(!empty($prodList)) {
			$conditions['RubyUprodt.fuel_product_number'] = array_keys($prodList);
		} else {
			$conditions['RubyUprodt.fuel_product_number'] = array();
		}
		$this->set(compact('prodList'));	
		//pr($conditions);die;
		$list=$this->RubyUprodt->find('all',array('conditions' => $conditions));	
		//pr($list);die;	
		$this->set('rubyUprodts', $list);
		
	
		
		/*MopReports */
		$mopoptions = array('joins' => array(
					array(
						'table' => 'ruby_headers',
						'alias' => 'rubyheaders',
						'type' => 'inner',
						'foreignKey' => false,
						'conditions'=> array('rubyheaders.id = RubyUprodt.ruby_header_id')
					)),'conditions'=>array('rubyheaders.ending_date_time'=>$endate,'rubyheaders.store_id'=>$this->Session->read('stores_id'),'RubyUprodt.fuel_product_number'=>array_keys($prodList)),'order'=> array('RubyUprodt.id'=>'asc'));	
		$list = $this->RubyUprodt->find('all', $mopoptions);
		//pr($list); die;
		$this->set('mreports',$list);				
		/* TankReports */	
		$tankoptions = array('joins' => array(
							array(
								'table' => 'ruby_headers',
								'alias' => 'rubyheaders',
								'type' => 'inner',
								'foreignKey' => false,
								'conditions'=> array('rubyheaders.id = RubyUtankt.ruby_header_id')
							),
							array(
								'table' => 'ruby_ftanks',	
								'alias' => 'rubyftanks',						
								'type' => 'inner',
								'foreignKey' => false,
								'conditions'=> array('rubyftanks.tank_number = RubyUtankt.tank_number')
							)
							),'conditions'=>array('rubyheaders.ending_date_time'=>$endate,'rubyheaders.store_id'=>$this->Session->read('stores_id'),'rubyftanks.store_id'=>$this->Session->read('stores_id'),'rubyftanks.status'=>'1'),'order'=> array('RubyUtankt.id'=>'asc'));						
		$list = $this->RubyUtankt->find('all', $tankoptions);    
		    
		$this->set('tnreports', $list);		
		$this->set('endate', $endate);
		}
	}
	
	

    public function admin_view($id = null) {
        $this->Setredirect();
        if (!$this->RfuelDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        $options = array('conditions' => array('RfuelDepartment.' . $this->RfuelDepartment->primaryKey => $id));
        $this->set('rfuelDepartment', $this->RfuelDepartment->find('first', $options));
    }

    public function admin_add() {
        $this->Setredirect();
        $this->set('rdepartments', 'active');
        if ($this->request->is('post')) {
            $this->RfuelDepartment->create();

            $this->request->data['RfuelDepartment']['store_id'] = $_SESSION['store_id'];
          $this->request->data['RfuelDepartment']['company_id'] = $this->Session->read('Auth.User.company_id');

            if ($this->RfuelDepartment->save($this->request->data)) {
                $this->Flash->success(__('The fuel department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The fuel department could not be saved. Please, try again.'));
            }
        }
        $this->loadmodel('Store');
        $stores = $this->Store->find('list', array('conditions' => array('Store.company_id' => $this->Session->read('Auth.User.company_id'))));
        $this->set('stores', $stores);
    }

    public function admin_edit($id = null) {
        $this->Setredirect();
        $this->set('rdepartments', 'active');
        if (!$this->RfuelDepartment->exists($id)) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        if ($this->request->is(array('post', 'put'))) {
           // $this->request->data['RfuelDepartment']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RfuelDepartment']['store_id'] = $_SESSION['store_id'];
            if ($this->RfuelDepartment->save($this->request->data)) {
                $this->Flash->success(__('The fuel department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The fuel department could not be saved. Please, try again.'));
            }
        }
        $options = array('conditions' => array('RfuelDepartment.' . $this->RfuelDepartment->primaryKey => $id));
        $this->request->data = $this->RfuelDepartment->find('first', $options);

        $this->loadmodel('Store');
        $stores = $this->Store->find('list', array('conditions' => array('Store.company_id' => $this->Session->read('Auth.User.company_id'))));
        $this->set('stores', $stores);
    }

    public function admin_delete($id = null) {
        $this->Setredirect();
        $this->RfuelDepartment->id = $id;
        if (!$this->RfuelDepartment->exists()) {
            throw new NotFoundException(__('Invalid fuel department'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->RfuelDepartment->delete()) {
            $this->Flash->success(__('The fuel department has been deleted.'));
        } else {
            $this->Flash->error(__('The fuel department could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
public function admin_settings(){
}
}
