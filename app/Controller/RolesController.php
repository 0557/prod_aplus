<?php
App::uses('AppController', 'Controller');
/**
 * Roles Controller
 *
 * @property Role $Role
 */
class RolesController extends AppController {
 public $uses=array("Role","SitePermission");

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	   if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user("package_id"),'roles','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            $this->Role->recursive = 0;
		$limit=(isset($this->params['named']['showperpage']))?$this->params['named']['showperpage']:Configure::read('site.admin_paging_limit');
		$conditions=array();		
		if(isset($this->params['named']['keyword']) && $this->params['named']['keyword']!=''){
		  $conditions['Role.name LIKE ']='%'.$this->params['named']['keyword'].'%';
		}
		  
		if(!empty($this->request->data)){
		  if(isset($this->request->data['showperpage']) && $this->request->data['showperpage']!=''){
		    $limit=$this->request->data['showperpage'];
		    $this->params['named']=array("showperpage"=>$limit);
		  }
		  if(isset($this->request->data['keyword']) && $this->request->data['keyword']!=''){
		    $this->params['named']=array("keyword"=>$this->request->data['keyword']);
			$conditions['Role.name LIKE ']='%'.$this->request->data['keyword'].'%';
		  }
		}
		
		
		$this->paginate=array("conditions"=>$conditions,"limit"=>$limit,"order"=>"Role.created DESC");
		
		
		$this->set('roles', $this->paginate());
		$this->set(compact('limit'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user("package_id"),'roles','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
		$this->set('role', $this->Role->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user("package_id"),'roles','is_add')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if ($this->request->is('post')) {
			$this->Role->create();
			if ($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('The role has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The role could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user("package_id"),'roles','is_edit')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('The role has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The role could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id),'recursive'=>"-1");
			$this->request->data = $this->Role->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user("package_id"),'roles','is_delete')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
            }
            $this->Role->id = $id;
		if (!$this->Role->exists()) {
			throw new NotFoundException(__('Invalid role'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Role->delete()) {
			$this->Session->setFlash(__('Role deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Role was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
