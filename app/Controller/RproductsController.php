<?php

App::uses('AppController', 'Controller');

/**
 * WholesaleProducts Controller
 *
 * @property WholesaleProduct $WholesaleProduct
 * @property PaginatorComponent $Paginator
 */
class RproductsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->set('r_products', 'active');

        $this->paginate = array('conditions' => array(/*'Rproduct.corporation_id' => $_SESSION['corporation_id'], */'Rproduct.store_id' => $_SESSION['store_id']), "limit" => 5, "order" => "Rproduct.id DESC");

        $this->set('wholesaleProducts', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->set('r_products', 'active');
        if (!$this->Rproduct->exists($id)) {
            throw new NotFoundException(__('Invalid fuel product'));
        }
        $options = array('conditions' => array('Rproduct.' . $this->Rproduct->primaryKey => $id));
        $this->set('wholesaleProduct', $this->Rproduct->find('first', $options));
    }

    public function admin_add() {
        $this->Setredirect();
		   $this->loadmodel('RfuelDepartment');
        $this->set('r_products', 'active');
        if ($this->request->is('post')) {
            $this->Rproduct->create();
          //  $this->request->data['Rproduct']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['Rproduct']['store_id'] = $_SESSION['store_id'];
			$this->request->data['Rproduct']['company_id'] = $this->Session->read('Auth.User.company_id');

            $this->request->data['Rproduct']['Piping_date'] = (isset($this->data['Rproduct']['Piping_date']) && $this->data['Rproduct']['Piping_date'] != '') ? $this->Default->Getdate($this->data['Rproduct']['Piping_date']) : 0;
            $this->request->data['Rproduct']['price'] = (isset($this->request->data['Rproduct']['price']) && $this->request->data['Rproduct']['price'] != '') ? $this->request->data['Rproduct']['price'] : 0;
            $this->request->data['Rproduct']['qty'] = (isset($this->request->data['Rproduct']['qty']) && $this->request->data['Rproduct']['qty'] != '') ? $this->request->data['Rproduct']['qty'] : 0;
            $this->request->data['Rproduct']['opening_book_amount'] = $this->request->data['Rproduct']['price'] * $this->request->data['Rproduct']['qty'];

            if ($this->Rproduct->save($this->request->data)) {
                $this->Session->setFlash(__('The fuel product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fuel product could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        }
        $fuelDepartments = $this->RfuelDepartment->find('list', array('conditions' => array("RfuelDepartment.company_id"=>$this->Session->read('Auth.User.company_id'), "RfuelDepartment.store_id" => $_SESSION['store_id'])));
        $customers = $this->Rproduct->Customer->find('list', array('conditions' => array("Customer.company_id"=>$this->Session->read('Auth.User.company_id'), "Customer.store_id" => $_SESSION['store_id'],'Customer.type'=>'Fuel')));

        $stores = $this->Rproduct->Store->find('list', array('conditions' => array('Store.company_id'=>$this->Session->read('Auth.User.company_id'))));

        $this->set(compact('fuelDepartments', 'customers', 'stores'));
    }

    public function admin_edit($id = null) {
        $this->Setredirect();
        $this->set('r_products', 'active');
		   $this->loadmodel('RfuelDepartment');
        if (!$this->Rproduct->exists($id)) {
            throw new NotFoundException(__('Invalid fuel product'));
        }
        if ($this->request->is(array('post', 'put'))) {
         //   $this->request->data['Rproduct']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['Rproduct']['store_id'] = $_SESSION['store_id'];
            $this->request->data['Rproduct']['Piping_date'] = (isset($this->data['Rproduct']['Piping_date']) && $this->data['Rproduct']['Piping_date'] != '') ? $this->Default->Getdate($this->data['Rproduct']['Piping_date']) : 0;
            $this->request->data['Rproduct']['price'] = (isset($this->request->data['Rproduct']['price']) && $this->request->data['Rproduct']['price'] != '') ? $this->request->data['Rproduct']['price'] : 0;
            $this->request->data['Rproduct']['qty'] = (isset($this->request->data['Rproduct']['qty']) && $this->request->data['Rproduct']['qty'] != '') ? $this->request->data['Rproduct']['qty'] : 0;
            $this->request->data['Rproduct']['opening_book_amount'] = $this->request->data['Rproduct']['price'] * $this->request->data['Rproduct']['qty'];

            if ($this->Rproduct->save($this->request->data)) {
                $this->Session->setFlash(__('The fuel product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fuel product could not be saved. Please, try again.'), 'default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('Rproduct.' . $this->Rproduct->primaryKey => $id));
            $this->request->data = $this->Rproduct->find('first', $options);
        }
        $fuelDepartments = $this->RfuelDepartment->find('list', array('conditions' => array("RfuelDepartment.company_id"=>$this->Session->read('Auth.User.company_id'), "RfuelDepartment.store_id" => $_SESSION['store_id'])));
        $customers = $this->Rproduct->Customer->find('list', array('conditions' => array("Customer.company_id" => $this->Session->read('Auth.User.company_id'), "Customer.store_id" => $_SESSION['store_id'])));
        $stores = $this->Rproduct->Store->find('list', array('conditions' => array('Store.company_id'=>$this->Session->read('Auth.User.company_id'))));
        $this->set(compact('fuelDepartments', 'customers', 'stores'));
    }

    public function admin_delete($id = null) {
        $this->Setredirect();
        $this->Rproduct->id = $id;
        if (!$this->Rproduct->exists()) {
            throw new NotFoundException(__('Invalid fuel product'));
        }
        if ($this->Rproduct->delete()) {
            $this->Session->setFlash(__('The fuel product has been deleted.'));
        } else {
            $this->Session->setFlash(__('The fuel product could not be deleted. Please, try again.'), 'default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
