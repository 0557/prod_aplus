<?php

ob_start();
App::uses('AppController', 'Controller');

class RsalesInvoicesController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        die('Enter Wrong Url');
        $this->Setredirect();
        $this->set('rsales_invoice', 'active');
        $this->paginate = array(
            'conditions' => array('or' => array(
                    array('RsalesInvoice.corporation_id' => $_SESSION['corporation_id'], 'RsalesInvoice.store_id' => $_SESSION['store_id']),
                    array('RsalesInvoice.customer_id not' => Null))), "limit" => 5, "order" => "RsalesInvoice.id DESC");

        $this->set('rsalesInvoice', $this->paginate());
    }

    public function admin_view($id = null) {

        die('Enter Wrong Url');
        $this->set('rsales_invoice', 'active');
        if (!$this->RsalesInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid sales invoice'));
        }

        $options = array('conditions' => array('RsalesInvoice.' . $this->RsalesInvoice->primaryKey => $id));
        $this->set('salesInvoice', $this->RsalesInvoice->find('first', $options));
        $product_id = $this->RsalesInvoice->find('first', array('conditions' => array('RsalesInvoice.id' => $id), 'fields' => array('product_id')));

        $product_id_arr = explode(',', $product_id['RsalesInvoice']['product_id']);

        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('all', array('conditions' => array('WholesaleProduct.id' => $product_id_arr)));

        $this->set('product', $product);
    }

    public function admin_add() {
        die('Enter Wrong Url');
        $this->Setredirect();
        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('Corporation');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('rsales_invoice', 'active');
        if ($this->request->is('post')) {

            $this->RsalesInvoice->create();
            $this->request->data['RsalesInvoice']['invoice_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['invoice_date']);
            $this->request->data['RsalesInvoice']['ship_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['ship_date']);
            $this->request->data['RsalesInvoice']['due_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['due_date']);
            if (!empty($this->request->data['RsalesInvoice']['customer_id'])) {
                $this->request->data['RsalesInvoice']['corporation_id'] = $_SESSION['corporation_id'];
                $this->request->data['RsalesInvoice']['store_id'] = $_SESSION['store_id'];
            }


            if (!empty($this->request->data['FuelProduct']['product_id']) && (!empty($this->request->data['FuelProduct']['product_id']))) {
                if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                    $this->request->data['RsalesInvoice']['send_email'] = '1';
                }
                $datasource = $this->RsalesInvoice->getDataSource();
                try {
                    $datasource->begin();
                    if (!$this->RsalesInvoice->save($this->request->data)) {
                        throw new Exception();
                    }
                    $getlast_id = $this->RsalesInvoice->getLastInsertId();

                    $tax_save = array();
                    $tax_save['TaxeZone']['rsale_invoice_id'] = $getlast_id;

                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                            if (!$this->TaxeZone->save($tax_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {
                        $product_save = array();
                        $product_save['FuelProduct']['type'] = 'RSale Invoice';
                        $product_save['FuelProduct']['rfuel_sale_invoice_id'] = $getlast_id;
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key => $value) {
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key];
                            if (!$this->FuelProduct->save($product_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    if ($datasource->commit()) {

                        $fileName = $this->__CreatePdf($this->data, $getlast_id);
                        $this->RsalesInvoice->id = $getlast_id;
                        $sales['RsalesInvoice']['invoice_file'] = $fileName;

                        if ($this->RsalesInvoice->save($sales)) {
                            $customer_email = '';
                            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                                if (!empty($this->request->data['RsalesInvoice']['customer_id'])) {

                                    $customer_id = $this->request->data['RsalesInvoice']['customer_id'];
                                    $customer = $this->Customer->find('first', array('conditions' => array('Customer.id' => $customer_id), 'fields' => array('email'), 'recursive' => 0));
                                    $customer_email = $customer['Customer']['email'];
                                }
                            }
                            if (!empty($customer_email)) {

                                $this->loadModel('EmailTemplate');
                                $email = $this->EmailTemplate->selectTemplate('send_invoice');

                                $this->loadModel('Setting');
                                //       $toEmail = $this->Setting->field('Setting.value', array('Setting.name' => 'site.contactus_email'));
                                $emailFindReplace = array(
                                    '##SITE_LINK##' => Router::url('/', true),
                                    '##SITE_NAME##' => Configure::read('site.name'),
                                    '##SUPPORT_EMAIL##' => Configure::read('site.contactus_email'),
                                    '##WEBSITE_URL##' => Router::url('/', true),
                                    '##FROM_EMAIL##' => 'info@24phpsupport.com',
                                    '##CONTACT_URL##' => Router::url(array(
                                        'controller' => 'homes',
                                        'action' => 'contact',
                                        'admin' => false
                                            ), true),
                                    '##SITE_LOGO##' => Router::url(array(
                                        'controller' => 'img',
                                        'action' => '/',
                                        'logo-big.png',
                                        'admin' => false
                                            ), true),
                                );

                                if (@file_exists(WWW_ROOT . 'uploads/sale_invoice/' . $fileName)) {
                                    $this->Email->attachments = array(WWW_ROOT . 'uploads/sale_invoice/' . $fileName);
                                }
                                $this->Email->from = 'info@24phpsupport.com';
                                $this->Email->replyTo = ($email['reply_to_email'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to') : $email['reply_to_email'];
                                $this->Email->to = 'vishnsingh007@gmail.com';
                                $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                                $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                                $this->Email->send(strtr($email['description'], $emailFindReplace));
                            }
                        }
                        $this->Session->setFlash(__('The fuel Sales Invoice has been saved.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } catch (Exception $e) {
                    $this->Session->setFlash(__('The fuel Sales Invoice could not be saved. Please, try again.'));
                    $datasource->rollback();
                }
            } else {
                $error = 'Select Atleast One Product';
                $this->set('error', $error);
                $this->Session->setFlash(__($error));
            }
        }
        $product = $this->WholesaleProduct->find('list');
        $wholesale_customer = $this->Customer->find('list', array('conditions' => array(/* 'retail_type' => 'wholesale', */ 'customer_type' => 'Customer')));

        $corporation = $this->Corporation->find('list', array('conditions' => array('id' => $_SESSION['corporation_id'])));
        $usa_state = $this->State->find('list', array('conditions' => array('country_id' => 223)));
        $store = $this->RsalesInvoice->Store->find('list', array('conditions' => array('Store.id' => $_SESSION['store_id'])));
        $this->set(compact('usa_state', 'store', 'product', 'wholesale_customer', 'corporation', 'store'));
    }

    public function admin_add1() {
        die('Enter Wrong Url');

        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('rsales_invoice', 'active');
        if ($this->request->is('post')) {
//            pr($this->data);
//            die;
            $this->RsalesInvoice->create();
            $this->request->data['RsalesInvoice']['invoice_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['invoice_date']);
            $this->request->data['RsalesInvoice']['ship_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['ship_date']);
            $this->request->data['RsalesInvoice']['due_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['due_date']);

            if (!empty($this->request->data['FuelProduct']['product_id']) && (!empty($this->request->data['FuelProduct']['product_id']))) {

                if (!empty($this->request->data['FuelProduct']['product_id'])) {

                    if ($this->RsalesInvoice->save($this->request->data)) {

                        $getlast_id = $this->RsalesInvoice->getLastInsertId();
                        $tax_save = array();
                        $product_save = array();
                        $tax_save['TaxeZone']['rsale_invoice_id'] = $getlast_id;

                        if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                            foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                                $this->TaxeZone->create();
                                $tax_save['TaxeZone']['tax_zone'] = $value;
                                $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                                $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                                $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                                $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                                $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                                $this->TaxeZone->save($tax_save);
                            }
                        }

                        if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {
                            $product_save['FuelProduct']['type'] = 'RSale Invoice';
                            $product_save['FuelProduct']['rfuel_sale_invoice_id'] = $getlast_id;
                            foreach ($this->request->data['FuelProduct']['product_id'] as $key => $value) {
                                $this->FuelProduct->create();
                                $product_save['FuelProduct']['product_id'] = $value;
                                $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key];
                                $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key];
                                $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key];
                                $this->FuelProduct->save($product_save);
                            }
                        }

//                        if (isset($this->request->data['SalesInvoice']['customer_id']) && !empty($this->request->data['SalesInvoice']['customer_id'])) {
//                            if (isset($this->request->data['checkbox']) && ($this->request->data['checkbox'] == 'on')) {
//                             
//                                ini_set("pcre.backtrack_limit", "200000000");
//                                ini_set("pcre.recursion_limit", "200000000");
//                                ini_set("memory_limit", "18000M");
//                                
//                                App::import('Vendor', 'Mpdf', array('file' => 'Mpdf' . DS . 'Mpdf.php')); 
//                                $pdf = new mPDF();
//
//$pdf->WriteHTML('hello world');
//
//$pdf->Output('name');
//exit;
//
////                                $this->Mpdf->init();
////                                $this->Mpdf->setFooter('{PAGENO}');
////                                $this->Mpdf->ignore_invalid_utf8 = true;
////                                $this->Mpdf->ignore_table_widths = true;
////                                $this->Mpdf->ignore_table_percents = true;
////                                $this->Mpdf->ignorefollowingspaces = true;
////                                $this->Mpdf->SetDefaultFont('opensans');
////                                $this->Mpdf->setFilename($isExist['Survey']['survey_title'] . ".pdf");
////
////                                // setting output to I, D, F, S
////                                $this->Mpdf->setOutput('D');
//                                   die;
//                            }
//                        }
                        $email = new CakeEmail();
                        $email->attachments(WWW_ROOT . '/uploads/sale_invoice');
                        $email->to = $customer_email;
                        $email->subject = 'Invoice is send on your email';
                        $email->replyTo = 'info@24phpsupport.com';
                        $email->from = 'info@24phpsupport.com';
                        $email->emailFormat = 'html';
                        if ($email->send()) {
                            die('Email Sent!');
                        } else {
                            die('Failed to send email');
                        }
                        $this->Session->setFlash(__('The fuel Sales Invoice has been saved.'));
                        return $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The fuel Sales Invoice could not be saved. Please, try again.'));
                    }
                }
            } else {
                $error = 'Select Atleast One Product';
                $this->set('error', $error);
                $this->Session->setFlash(__($error));
            }
        }
        $product = $this->WholesaleProduct->find('list');
        $wholesale_customer = $this->Customer->find('list', array('conditions' => array(/* 'retail_type' => 'wholesale', */ 'customer_type' => 'Customer')));
        // $store = $this->SalesInvoice->Store->find('list');
        $corporation = $this->RsalesInvoice->Corporation->find('list');
        $usa_state = $this->State->find('list', array('conditions' => array('country_id' => 223)));
        $this->set(compact('usa_state', 'store', 'product', 'wholesale_customer', 'corporation'));
    }

    public function admin_edit($id = null) {
        die('Enter Wrong Url');
        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('Corporation');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('rsales_invoice', 'active');
        if (!$this->RsalesInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel Sales invoice'));
        }

        if ($this->request->is(array('post', 'put'))) {
            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                $this->request->data['RsalesInvoice']['send_email'] = '1';
            }
            $customer_email = '';
            if (!empty($this->request->data['RsalesInvoice']['customer_id'])) {
                $this->request->data['RsalesInvoice']['corporation_id'] = '';
                $this->request->data['RsalesInvoice']['store_id'] = '';
            }
            $customer_id = $this->request->data['RsalesInvoice']['customer_id'];
            $customer = $this->Customer->find('first', array('conditions' => array('Customer.id' => $customer_id), 'fields' => array('email'), 'recursive' => 0));
            $customer_email = $customer['Customer']['email'];

            if (!empty($this->request->data['RsalesInvoice']['corporation_id'])) {
                $this->request->data['RsalesInvoice']['customer_id'] = '';
            }

            $this->request->data['RsalesInvoice']['invoice_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['invoice_date']);
            $this->request->data['RsalesInvoice']['ship_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['ship_date']);
            $this->request->data['RsalesInvoice']['due_date'] = $this->Default->Getdate($this->data['RsalesInvoice']['due_date']);
            if (empty($this->request->data['RsalesInvoice']['customer_id'])) {
                $this->request->data['RsalesInvoice']['corporation_id'] = $_SESSION['corporation_id'];
                $this->request->data['RsalesInvoice']['store_id'] = $_SESSION['store_id'];
            }
            if (!empty($this->request->data['FuelProduct']['product_id']) && (!empty($this->request->data['FuelProduct']['product_id']))) {
                $datasource = $this->RsalesInvoice->getDataSource();
                try {
                    $datasource->begin();
                    if (!$this->RsalesInvoice->save($this->request->data)) {
                        throw new Exception();
                    }
                    $this->TaxeZone->deleteAll(array('TaxeZone.rsale_invoice_id' => $id));
                    $getlast_id = $this->RsalesInvoice->getLastInsertId();
                    $tax_save = array();
                    $tax_save['TaxeZone']['rsale_invoice_id'] = $id;
                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                            if (!$this->TaxeZone->save($tax_save)) {
                                throw new Exception();
                            }
                        }
                    }

                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {

                        $this->FuelProduct->deleteAll(array('FuelProduct.fuel_sale_invoice_id' => $id), false);
                        $product_save = array();
                        $product_save['FuelProduct']['type'] = 'RSale Invoice';
                        $product_save['FuelProduct']['rfuel_sale_invoice_id'] = $id;
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key1 => $value1) {
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value1;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key1];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key1];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key1];
                            if (!$this->FuelProduct->save($product_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    if ($datasource->commit()) {

                        $uploadPath = WWW_ROOT . 'uploads/sale_invoice/' . $this->data['RsalesInvoice']['invoice_file'];
                        @unlink($uploadPath);

                        $fileName = $this->__CreatePdf($this->data, $id);

                        $this->RsalesInvoice->id = $id;
                        $sales['RsalesInvoice']['invoice_file'] = $fileName;
                        if ($this->RsalesInvoice->save($sales)) {

                            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {

                                if (!empty($customer_email)) {

                                    $this->loadModel('EmailTemplate');
                                    $email = $this->EmailTemplate->selectTemplate('send_invoice');

                                    $this->loadModel('Setting');
                                    //       $toEmail = $this->Setting->field('Setting.value', array('Setting.name' => 'site.contactus_email'));
                                    $emailFindReplace = array(
                                        '##SITE_LINK##' => Router::url('/', true),
                                        '##SITE_NAME##' => Configure::read('site.name'),
                                        '##SUPPORT_EMAIL##' => Configure::read('site.contactus_email'),
                                        '##WEBSITE_URL##' => Router::url('/', true),
                                        '##FROM_EMAIL##' => 'info@24phpsupport.com',
                                        '##CONTACT_URL##' => Router::url(array(
                                            'controller' => 'homes',
                                            'action' => 'contact',
                                            'admin' => false
                                                ), true),
                                        '##SITE_LOGO##' => Router::url(array(
                                            'controller' => 'img',
                                            'action' => '/',
                                            'logo-big.png',
                                            'admin' => false
                                                ), true),
                                    );

                                    if (@file_exists(WWW_ROOT . 'uploads/sale_invoice/' . $fileName)) {

                                        $this->Email->attachments = array(WWW_ROOT . 'uploads/sale_invoice/' . $fileName);
                                    }
                                    $this->Email->from = 'info@24phpsupport.com';
                                    $this->Email->replyTo = ($email['reply_to_email'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to') : $email['reply_to_email'];
                                    $this->Email->to = $customer_email;
                                    $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                                    $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';

                                    $this->Email->send(strtr($email['description'], $emailFindReplace));
                                }
                            }

                            $this->Session->setFlash(__('The fuel Sales Invoice has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                        }
                    }
                } catch (Exception $e) {
                    $this->Session->setFlash(__('The fuel Sales Invoice could not be saved. Please, try again.'));
                    $datasource->rollback();
                }
            } else {
                $this->Session->setFlash(__('Select Atleast One Product'));
            }
        } else {
            $options = array('conditions' => array('RsalesInvoice.' . $this->RsalesInvoice->primaryKey => $id));
            $this->request->data = $this->RsalesInvoice->find('first', $options);
            //  pr($this->request->data); die;
        }
        $product = $this->WholesaleProduct->find('list');
        $wholesale_customer = $this->Customer->find('list', array('conditions' => array(/* 'retail_type' => 'wholesale', */ 'customer_type' => 'Customer')));

        $corporation = $this->Corporation->find('list', array('conditions' => array('id' => $_SESSION['corporation_id'])));

        $store = $this->RsalesInvoice->Store->find('list', array('conditions' => array('Store.id' => $_SESSION['store_id'])));
        //$corporation = $this->RsalesInvoice->Corporation->find('list',array('conditions'=>array('Corporation.id'=>$_SESSION['corporation_id'])));
        //$store = $this->RsalesInvoice->Store->find('list',array('conditions'=>array('Corporation.id'=>$_SESSION['store_id'])));
        $usa_state = $this->State->find('list', array('conditions' => array('country_id' => 223)));
        $this->set(compact('usa_state', 'store', 'product', 'wholesale_customer', 'corporation', 'store'));
    }

    public function admin_delete($id = null) {
        die('Enter Wrong Url');
        $this->RsalesInvoice->id = $id;
        if (!$this->RsalesInvoice->exists()) {
            throw new NotFoundException(__('Invalid sales invoice'));
        }

        if ($this->RsalesInvoice->delete()) {
            $this->Flash->success(__('The sales invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The sales invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function products($id = null) {
        die('Enter Wrong Url');
        if (empty($id)) {
            return;
        }

        $this->loadmodel('WholesaleProduct');

        $products = $this->WholesaleProduct->find('list');
        $product = $this->WholesaleProduct->find('first', array('conditions' => array('WholesaleProduct.id' => $id)));

        $this->set(compact('products', 'product', 'id'));
    }

    public function admin_filter_store($corporation_id = null) {
        die('Enter Wrong Url');
        $this->autoRender = false;
        $this->loadModel('Store');
        $store_name = $this->Store->find('list', array('conditions' => array('Store.corporation_id' => $corporation_id)));
        return json_encode($store_name);
    }

    protected function __CreatePdf($data, $invoice_number) {
        die('Enter Wrong Url');
        $this->autoRender = false;

        App::import('Vendor', 'Mpdf', array('file' => 'Mpdf' . DS . 'mpdf.php'));

        $payable = Configure::read('checkspayable');
        $logo = Configure::read('logo');
        // echo $logo; die;
        //    pr($data); die;



        $middile_description_product = '';
        if (isset($data['FuelProduct']['product_id']) && !empty($data['FuelProduct']['product_id'])) {
            $this->loadModel('WholesaleProduct');
            foreach ($data['FuelProduct']['product_id'] as $key => $value) {
                $product = $this->WholesaleProduct->field('name', array('WholesaleProduct.id' => $value));

                $middile_description_product .= '<tr>
                                                    <td style="border-right: 1px solid; width:655px; padding: 0px 2px;font-size: 16.5667px;">' . $product . '</td>
                                                    <td style="border-right: 1px solid;width:154px;padding: 0px 2px;font-size: 16.5667px; text-align:right;" >' . $data['FuelProduct']['gallons_delivered'][$key] . '</td>
                                                    <td style="border-right: 1px solid;width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">' . $data['FuelProduct']['cost_per_gallon'][$key] . '</td>
                                                    <td style="border-right: 1px solid; width:154px; padding: 0px 2px;font-size: 16.5667px;text-align:right;">' . $data['FuelProduct']['net_ammount'][$key] . '</td>
                                                </tr>';
            }
        }

        $middile_description_taxes = '';
        if (isset($data['TaxeZone']['tax_zone']) && !empty($data['TaxeZone']['tax_zone'])) {
            foreach ($data['TaxeZone']['tax_zone'] as $key1 => $value1) {

                $middile_description_taxes .= ' <tr >
                                            <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">' . $data['TaxeZone']['tax_decription'][$key1] . '</td>
                                            <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['tax_on_gallon_qty'][$key1] . '</td>
                                            <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['rate_on_gallon'][$key1] . '</td>
                                            <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['tax_net_amount_gallon'][$key1] . '</td>
                                          </tr>';
            }
        }


        $footer = '<table width="700px" style="border-top:0;" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
       <td style="width:410px;border-right:0px solid;padding: 0px 2px;">&nbsp;</td>
        <td style="border-bottom:1px solid;border-left:1px solid;font-size:16px;padding-left:2px;margin:0;font-weight:bold;">Subtotal</h2></td>
        <td style="border-bottom:1px solid;border-right:1px solid;font-size:16.5667px;padding-left:2px;width:140px;text-align:right;">$' . $data['RsalesInvoice']['gross_amount'] . '</td>
      </tr>
      <tr>
       <td style="width:410px;border-right:0px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="border-bottom: 1px solid;border-left:1px solid;font-size:16px;padding-left: 2px;margin:0;font-weight:bold;">Payments/Credits</h2></td>
        <td style="border-bottom: 1px solid;border-right:1px solid;font-size: 16.5667px;padding-left: 2px;width:140px;text-align:right;">$0.00</td>
      </tr>
      <tr>
        <td style="width:410px;border-right:0px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="border-bottom:1px solid;border-left:1px solid;font-size:16px;padding-left:2px;margin:0;font-weight:bold;">Balance Due</h2></td>
        <td style="border-bottom:1px solid;font-size: 16.5667px;border-right:1px solid;padding-left: 2px;width:140px;text-align:right;font-weight:bold;"><strong>$' . $data['RsalesInvoice']['gross_amount'] . '</strong></td>
      </tr>
    </tbody>
  </table>';

        // PUT YOUR HTML IN A VARIABLE

        $my_html = '<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div style="width:700px; margin:75px auto 0">
  <div>
    <div style=" float:left; width:204px; text-align:center;"> <img style="width:auto; height:100px" src="' . Router::url('/') . 'app/webroot/uploads/settings/' . $logo . '" />
      <p style="font-size:19.9667px;text-align:left;
margin: 0;"></p>
      <p style="font-size: 18px;text-align:left;
margin: 0;">' . Configure::read('address') . '</p>
      <h1 style="font-size:18px;text-align:left;
margin: 15px 0; ">Bill To:</h1>
      <p style="font-size:18px;text-align:left;
margin: 0;">' . $data['RsalesInvoice']['bill_to'] . '
   </p>
      <p style="font-size:18px;text-align:left;
margin: 0;">' . $data['RsalesInvoice']['billing_address'] . '</p>
      <p style="font-size:18px;text-align:left;
margin: 0;"></p>
    </div>
    <div style="float:right; width:425px">
    <table style="width:425px;">
   <tr>
        <td style="font-size:19px;font-weight:bold;">Invoice Date</td>
        <td style="font-size:19px;font-weight:bold;">Invoice#</td>
        <td rowspan="2" style="font-size:36px;font-weight:bold;">Invoice</td>
        </tr>
    <tr>
        <td style="font-size:16px;">' . date('d/m/Y', strtotime($data['RsalesInvoice']['invoice_date'])) . '</td>
        <td style="font-size:16px;">' . $data['RsalesInvoice']['incoice_type'] . '</td>
        </tr>   
    </table>

      <div style="border:3px solid #7f7f7f;
border-radius: 3px;
clear: both;
float: right;
overflow: auto;
padding:5px;margin-top:15px;
width: 350px; display:inline;">
<table style="width:350px;">
    <tr>
        <td style="font-size:24px;font-weight:bold;padding:15px;">PLEASE PAY</td>
        <td  style="font-size:24px;padding:15px;">$' . $data['RsalesInvoice']['gross_amount'] . '</td>
        </tr>   
    </table>
      </div>
      <h1 style="clear:both;float:right;width:380px;font-size:16px; font-weight:normal;">Make checks payable to:<span style="float: right;
font-size:16px; font-weight:bold;">' . $payable . '</span></h1>
      <div style="clear: both;
float: right;
width:380px;">
        <h1 style="font-size:19.9667px;
margin:0 0 15px;">Ship To:</h1>
        <p style="font-size:19.9667px;
margin: 0;">' . $data['RsalesInvoice']['ship_to'] . '<br />
          ' . $data['RsalesInvoice']['shiping_address'] . '<br />
          </p>
      </div>
    </div>
  </div>
  <div  style="border-top: 5px dotted gray;
clear: both;
display: inline-block;
margin: 25px 0;
padding-top: 20px;
width: 700px;">
    <div style="float: left; width:191px">
      <h3 style="font-size:18.9667px;
margin: 0;">' . Configure::read('wholesalename') . '</h3>
      <p style="font-size:18.9667px;
margin: 0;">
       
       ' . Configure::read('address') . '</p>
    </div>
    <div style="float: right; width: 461px;">
      <h3 style="font-size: 15px;
font-weight: normal;
margin: 0;
text-align: right;">PLEASE DETACH AND RETURN TOP PORTION WITH PAYMENT</h3>
      <table style="margin:24px 0 0;" width="461px;" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">FEIN</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">BOL #</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Ship Date</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Due Date</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Terms</td>
        </tr>
        <tr>
          <td style="font-size:16.5667px;margin-top: 9px;padding: 8px 0;text-align: center;">' . $data['SalesInvoice']['ship_via'] . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . $data['RsalesInvoice']['bol'] . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding: 8px 0;text-align:center;">' . date('d/m/Y', strtotime($data['RsalesInvoice']['ship_date'])) . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . date('d/m/Y', strtotime($data['RsalesInvoice']['due_date'])) . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . $data['RsalesInvoice']['terms'] . '</td>
        </tr>
      </table>
    </div>
  </div>
  <table cellpadding="0" cellspacing="0" width="700px" style="text-align:left; border:1px solid;">
    <thead>
      <tr>
        <th style="width:655px; border-right: 1px solid;padding: 0px 2px; border-bottom: 1px solid;"><h2 style="font-size:16.5667px;
margin: 0; text-align:center;">Description</h2></th>
        <th style="border-right: 1px solid;text-align:center;font-size:16.5667px;margin:0; border-bottom: 1px solid;text-align:center;">Qty</th>
        <th style="border-right: 1px solid;text-align:center;font-size:16.5667px;margin: 0; border-bottom: 1px solid;text-align:center;">Rate</th>
        <th style="border-bottom: 1px solid;text-align:center;font-size:16.5667px;margin: 0;">Amount</h2></th>
      </tr>
    </thead>
    <tbody>
        ' . $middile_description_product . '
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      ' . $middile_description_taxes . '
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 0px 2px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
    </tbody>
  </table>

    ' . $footer . '
  <br />
  <br />
  <table style="clear:both; width:355px" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td style=" font-size: 16.5667px; padding-bottom: 10px;"><strong>Billing Inqueries? Call</strong></td>
        <td style=" font-size:16.5667px; padding-bottom: 10px;"><strong>' . Configure::read('phonenumber') . '</strong></td>
      </tr>
      <tr>
        <td style=" font-size: 16.5667px;"><strong>Fax</strong></td>
        <td style=" font-size:16.5667px;"><strong>856-632-7743</strong></td>
      </tr>
    </tbody>
  </table>
</div>
</body></html>';
//

        ini_set("pcre.backtrack_limit", "200000000");
        ini_set("pcre.recursion_limit", "200000000");
        ini_set("memory_limit", "18000M");

        Configure::write('debug', 0);
        $mpdf = new mPDF();
        // $mpdf=new mPDF('utf-8',array(750,600));
        $mpdf->WriteHTML($my_html);
        $filename = 'RSale_invoice' . time() . '.pdf';
        $mpdf->Output($filename, 'F');
        //  echo $filename; die;
        return $filename;

//ob_end_clean();
//        $this->Mpdf->init();
//           $this->Mpdf->setFooter('{PAGENO}');
//        $this->Mpdf->ignore_invalid_utf8 = true;
//        $this->Mpdf->ignore_table_widths = true;
//        $this->Mpdf->ignore_table_percents = true;
//        $mpdf->allow_output_buffering = true;
//        $this->Mpdf->ignorefollowingspaces = true;
//        $this->Mpdf->WriteHTML($my_html);
//        $this->Mpdf->SetDefaultFont('opensans');
//        $filename = 'Sale_invoice' . time() . '.pdf';
//        $this->Mpdf->setFilename($filename);
//
//        // setting output to I, D, F, S
//        // $this->Mpdf->setOutput('D');
//        $mpdf->Output('D');
    }

}
