<?php 
error_reporting(0);
App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class RstoresController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
	
	public function admin_index() {		   		
			$this->Setredirect();
			$this->loadModel('RGroceryItem');
			$this->loadModel('Store');
			$company_id=$this->Session->read('Auth.User.company_id');
			$storedata = $this->Store->find('list',array('fields' => array('id', 'name'), 'conditions'=>array('Store.company_id'=>$company_id)));
			//pr($storedata); die;
		$this->set('storedata', $storedata);
		if($this->request->is('post')){
			$expstoreid=$this->request->data['RGroceryItem']['exp_store_id'];
			$expdepartment=$this->request->data['exp_dep_id'];
				$conditions = array(
					'RGroceryItem.r_grocery_department_id' => $expdepartment,
					'RGroceryItem.store_id' => $expstoreid
				);
			$rdata = $this->RGroceryItem->find('all',array('conditions'=>$conditions));
			//pr($rdata); die;
			$update=array();
			foreach($rdata as $rgroc){
				//pr($rgroc); die;
				
				$update['RGroceryItem']['store_id'] =$this->request->data['RGroceryItem']['imp_store_id'];
				$update['RGroceryItem']['r_grocery_department_id'] = $this->request->data['imp_dep_id'];
				$update['RGroceryItem']['plu_no'] = $rgroc['RGroceryItem']['plu_no'];
				$update['RGroceryItem']['price'] = $rgroc['RGroceryItem']['price'];
				$update['RGroceryItem']['plu_tax'] = $rgroc['RGroceryItem']['plu_tax'];
				$update['RGroceryItem']['description'] = $rgroc['RGroceryItem']['description'];
				$update['RGroceryItem']['company_id'] = $this->Session->read('Auth.User.company_id');
				$update['RGroceryItem']['created_at'] = date('Y-m-d');
				//pr($update);
				$this->RGroceryItem->create();
				$this->RGroceryItem->save($update);
				
				$this->Session->setFlash(__('The R Grocery Item has been saved.'));
				}
			}
			
		}
		
		
	public function admin_getdep(){
		$this->loadModel('RubyDepartment');
		$storeid=$this->request->data['store_id'];		
		$rGroceryDepartments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'), 'conditions'=>array('RubyDepartment.store_id'=>$storeid)));		
		//echo '<pre>';print_r($rGroceryDepartments);die;
		$r_grocery_departments=array(''=>'Select Department');
		
		foreach($rGroceryDepartments as $key => $value){
	    $r_grocery_departments[$key]=$value;
		}
		//echo '<pre>';print_r($r_grocery_departments);die;
		$this->set('rGroceryDepartments', $r_grocery_departments);	
	}
   	public function admin_getdep1(){
		$this->loadModel('RubyDepartment');
		$storeid=$this->request->data['store_id'];
		$rGroceryDepartments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'), 'conditions'=>array('RubyDepartment.store_id'=>$storeid)));
		$r_grocery_departments=array(''=>'Select Department');
		foreach($rGroceryDepartments as $key => $value){
	    $r_grocery_departments[$key]=$value;
		}		
		$this->set('rGroceryDepartments', $r_grocery_departments);	
	}

}
