<?php
error_reporting(0);
App::uses('AppController', 'Controller');
class Ruby1DailyreportingsController extends AppController {

	public $components = array('Paginator', 'Flash', 'Session');
	public function index() {
		$this->Rbank->recursive = 0;
	}


	public function admin_index($id=null) {	
		
		$this->Setredirect();
		$this->RubyDailyreporting->recursive = 0;
		if($id){
			if($this->RubyDailyreporting->delete($id)){
					$this->Session->setFlash(__("Deleted"),'flash_notification');
				return $this->redirect(array('controller' => 'Ruby1Dailyreportings','action' => 'index'));
				
			}
		}		
		$list = $this->RubyDailyreporting->find('all',array('conditions'=>array('RubyDailyreporting.store_id'=>$this->Session->read('stores_id'))));
		$this->set('reports',$list);	
		
	}
	public function admin_add() {		
		//$this->Setredirect();
		$prodList = $this->setFilterProductNumber($this->Session->read('stores_id'));	
		//pr($prodList);die;
		$this->set('prodList',$prodList);	
		$this->RubyDailyreporting->recursive = 0;	
		
		
	}
	public function admin_edit($id=null) {
		//$this->Setredirect();
		$this->RubyDailyreporting->recursive = 0;
		
		$list = $this->RubyDailyreporting->find('first',array('conditions'=>array('RubyDailyreporting.store_id'=>$this->Session->read('stores_id'),'id'=>$id)));
		$this->set('reports',$list);
	}
	
	public function admin_add_reporting() 
	 {  
	   $this->Setredirect();       
		$this->layout='admin';
		$storeId = $_SESSION['store_id'];
		//echo '<pre>';print_r($_REQUEST);exit;
		if (!empty($this->request->data))
		{
		if($_REQUEST['input_hidden_field_obj1']!=''){	
		$jsondata1=$_REQUEST['input_hidden_field_obj1'];
		$obj1 =json_decode($jsondata1, true);	
		$obj1 = array_filter($obj1);
		foreach($obj1 as $ob1){
			$insertQuery1 = ("INSERT INTO r_bank_accounts(bank_id,withdrawal_type,withdrawal_check_no,withdrawal_amount,withdrawal_memo,type) VALUES ('".$ob1['account_name']."','".$ob1['withdrawal_type']."','".$ob1['withdrawal_check_no']."','".$ob1['withdrawal_amount']."','".$ob1['withdrawal_memo']."','withdrawl')");
			$this->RubyDailyreporting->query($insertQuery1);
			}
		}
		
		if($_REQUEST['input_hidden_field_obj']!='')	{
			$jsondata=$_REQUEST['input_hidden_field_obj'];
			$obj =json_decode($jsondata, true);	
			$obj = array_filter($obj);
			foreach($obj as $ob){
			$insertQuery = ("INSERT INTO r_bank_accounts(bank_id,deposit_cash_amount,deposit_check_amount,deposit_total_amount,deposit_memo,type) VALUES ('".$ob['account_name']."','".$ob['deposit_cash_amount']."','".$ob['deposit_check_amount']."','".$ob['deposit_total_amount']."','".$ob['deposit_memo']."','deposit')");
			$this->RubyDailyreporting->query($insertQuery);
			}
		}
		
		
		$this->request->data['RubyDailyreporting']['store_id']=$storeId;
		//$this->request->data['RubyDailyreporting']['Grocey_Tax']=$this->request->data['RubyDailyreporting']['Grocey_Tax'];
		$this->request->data['RubyDailyreporting']['z_reading']=$this->request->data['z_reading'];
		$this->request->data['RubyDailyreporting']['Total_In']=$this->request->data['Total_In'];
		$this->request->data['RubyDailyreporting']['Total_Out']=$this->request->data['Total_Out'];
		$this->request->data['RubyDailyreporting']['Short_Over']=$this->request->data['Short_Over'];
		$this->request->data['RubyDailyreporting']['create_date']=date('Y-m-d');
		
		
				if ($this->RubyDailyreporting->save($this->request->data))
				{   
				$this->Session->setFlash(__("You have successfully added new Note"),'flash_notification');
				return $this->redirect(array('controller' => 'ruby_dailyreportings','action' => 'index'));				
		    	}
		}        
		else 
		{
			//$this->request->data = $this->Job->find('first',array('conditions'=>array('OtherBuses.id'=>$id)));
		}
    }
	public function admin_edit_reporting() 
	 {  
	   $this->Setredirect();       
		$this->layout='admin';
		$storeId = $_SESSION['store_id'];
		//echo '<pre>';print_r($_REQUEST);exit;
		if (!empty($this->request->data))
		{
		//	print_r($this->request->data);exit;
				if ($this->RubyDailyreporting->save($this->request->data))
				{   
		$this->Session->setFlash(__("You have successfully added new Note"),'flash_notification');
				return $this->redirect(array('controller' => 'Ruby1Dailyreportings','action' => 'index'));			
		    	}
		}        
		else 
		{
			//$this->request->data = $this->Job->find('first',array('conditions'=>array('OtherBuses.id'=>$id)));
		}
    }
    public function admin_daily_caleneder () 
    {
    	//$this->set('ruby_dailyreportings', 'active');
        //$this->layout='admin';
      	//$this->Setredirect();
		$this->RubyDailyreporting->recursive = 0;
    }
	
	public function get_daily_reporting_jsondata () 
	{
	     $this->RubyDailyreporting->recursive = 0;
	     $this->layout=false;
		  $this->autoRender=false;
		  $storeId = $_SESSION['store_id'];
    }


public function admin_getvalues(){
	 
	//  $this->autoRender=false;
	$this->loadModel('RubyHeader');
	$this->loadModel('RubyUtiert');
	$this->loadModel('RubyUprodt');  
	$this->RubyHeader->recursive = -1;
	$this->RubyUtiert->recursive = -1;
	$this->loadModel('Bank');
	$bank = $this->Bank->find('list',array('fields' => array('Bank.id', 'Bank.name'),'order' => array('Bank.id' => 'desc')));
	$this->set('bank',$bank);	
	
	 
	 if($this->request->data['adate']){
				$newdate = explode('-',$this->request->data['adate']);
				$date = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
				//$options=array('conditions' => array(date('RubyHeader.ending_date_time')=>$date,'RubyHeader.store_id'=>$this->Session->read('stores_id')));
			$headerid = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.ending_date_time'=>$date,'RubyHeader.store_id'=>$this->Session->read('stores_id'),'RubyHeader.requested_period_number'=>'1'),'order'=>array('RubyHeader.id'=>'desc')));
			
					
			
			if(!empty($headerid)){				
				//echo $headerid['RubyHeader']['id'];
				$headeridn=$headerid['RubyHeader']['id'];
				
				$this->loadModel('RubyDailyreporting');  
				$this->RubyDailyreporting->virtualFields['max_z_reading'] = 'MAX(RubyDailyreporting.z_reading)';	
				$RubyDailyreporting=$this->RubyDailyreporting->find('all',array('conditions' =>array('RubyDailyreporting.store_id'=>$this->Session->read('stores_id'))));		
				$max_z_reading=$RubyDailyreporting[0]['RubyDailyreporting']['max_z_reading'];
				$this->set('max_z_reading', $max_z_reading);				
				
				$this->loadModel('RubyTottaxe');  	
				$RubyTottaxe=$this->RubyTottaxe->find('all',array('conditions' =>array('RubyTottaxe.ruby_header_id'=> $headeridn )));		
				//pr($RubyTottaxe);die;
				$this->set('RubyTottaxe', $RubyTottaxe);
				
				
		        $this->loadModel('RubyMemo3');  	
		        $RubyMemo3=$this->RubyMemo3->find('all',array('conditions' =>array('RubyMemo3.ruby_header_id'=> $headeridn )));		
	            //pr($RubyMemo3);die;
                $this->set('RubyMemo3',$RubyMemo3);
				
								
				$this->loadModel('RubyTottaxe');  		
				$this->RubyTottaxe->virtualFields['total_taxes'] = 'SUM(RubyTottaxe.total_taxes)';
				$this->RubyTottaxe->virtualFields['total_taxable_sales'] = 'SUM(RubyTottaxe.total_taxable_sales)';
				$OtherrevenueSales = $this->RubyTottaxe->find('all',array('conditions' =>array('RubyTottaxe.ruby_header_id'=> $headeridn )));		
				//pr($OtherrevenueSales);die;
				$this->set('otherrevenuesales', $OtherrevenueSales);
				
				
				$this->loadModel('RubyTotmop');  	
				$RubyTotmop=$this->RubyTotmop->find('all',array('conditions' =>array('RubyTotmop.ruby_header_id'=> $headeridn,'RubyTotmop.mop_name'=>array('CREDIT','CASH','CHECK','LOTTERY'))));		
			    //pr($RubyTotmop);die;
				$this->set('RubyTotmop',$RubyTotmop);				
				
				$prodList = $this->setFilterProductNumber($this->Session->read('stores_id'));
				$this->set(compact('prodList'));				
				//$prodList = array(1=> 'Diesel', 4 => 'Regular', 5 => 'Premium', 6 => 'Plus'); //pr($prodList);
				$conditions22 = array('RubyHeader.ending_date_time ='=>$date,'RubyHeader.store_id'=>$this->Session->read('stores_id'));
				
				if(!empty($prodList)) {
				$conditions22['fuel_product_number'] = array_keys($prodList);
				} else {
				$conditions22['fuel_product_number'] = array();
				}
				//pr($conditions22);
				$rubyUprodts = $this->RubyUprodt->find('all',array('conditions'=>$conditions22,'order'=>array('RubyUprodt.id ASC')));
				//pr($rubyUprodts);die;
				$this->set('rubyUprodts', $rubyUprodts);
				
				
				/*$data=array();
				$volume = array('Regular','Premium','Plus','Diesel'); pr($volume);
				foreach($volume as $key=>$value){
			
							$fvolume = $this->RubyUtiert->find('first',array( "joins" => array(
							array(
								"table" => "ruby_fprods",
								"alias" => "ruby_fprod",
								"type" => "LEFT",
								"conditions" => array(
									"ruby_fprod.fuel_product_number = RubyUtiert.fuel_product_number"
								)
							)),'conditions'=>array('ruby_fprod.display_name'=>$value,'ruby_fprod.store_id'=>$this->Session->read('stores_id'),'RubyUtiert.price_tier_number'=>1,'RubyUtiert.ruby_header_id'=>@$headerid['RubyHeader']['id']),'order'=>array('RubyUtiert.id'=>'desc'),'fields'=>array('RubyUtiert.fuel_volume,RubyUtiert.fuel_value')));
							//pr($fvolume);
							if(!empty($fvolume)){
							$resu =$fvolume['RubyUtiert']['fuel_volume'].'*'.$fvolume['RubyUtiert']['fuel_value'];
						    //pr($resu);
				            $this->set($value,$resu);
							}
				}*/
				
							
				}else{			
				$headerid = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.ending_date_time'=>$date,'RubyHeader.store_id'=>$this->Session->read('stores_id'),'RubyHeader.requested_period_number'=>'2'),'order'=>array('RubyHeader.id'=>'desc')));
		        $headeridn = $headerid['RubyHeader']['id']; 
				
				$this->loadModel('RubyDailyreporting');  
				$this->RubyDailyreporting->virtualFields['max_z_reading'] = 'MAX(RubyDailyreporting.z_reading)';	
				$RubyDailyreporting=$this->RubyDailyreporting->find('all',array('conditions' =>array('RubyDailyreporting.store_id'=>$this->Session->read('stores_id'))));		
				$max_z_reading=$RubyDailyreporting[0]['RubyDailyreporting']['max_z_reading'];
				$this->set('max_z_reading', $max_z_reading);	
				
				$this->loadModel('RubyTottaxe');  	
				$RubyTottaxe=$this->RubyTottaxe->find('all',array('conditions' =>array('RubyTottaxe.ruby_header_id'=> $headeridn )));		
				//pr($RubyTottaxe);die;
				$this->set('RubyTottaxe', $RubyTottaxe);
				
				
		        $this->loadModel('RubyMemo3');  	
		        $RubyMemo3=$this->RubyMemo3->find('all',array('conditions' =>array('RubyMemo3.ruby_header_id'=> $headeridn )));		
	            //pr($RubyMemo3);die;
                $this->set('RubyMemo3',$RubyMemo3);
				
								
				$this->loadModel('RubyTottaxe');  		
				$this->RubyTottaxe->virtualFields['total_taxes'] = 'SUM(RubyTottaxe.total_taxes)';
				$this->RubyTottaxe->virtualFields['total_taxable_sales'] = 'SUM(RubyTottaxe.total_taxable_sales)';
				$OtherrevenueSales = $this->RubyTottaxe->find('all',array('conditions' =>array('RubyTottaxe.ruby_header_id'=> $headeridn )));		
				//pr($OtherrevenueSales);die;
				$this->set('otherrevenuesales', $OtherrevenueSales);
				
				
				$this->loadModel('RubyTotmop');  	
				$RubyTotmop=$this->RubyTotmop->find('all',array('conditions' =>array('RubyTotmop.ruby_header_id'=> $headeridn,'RubyTotmop.mop_name'=>array('CREDIT','CASH','CHECK','LOTTERY'))));		
				//pr($RubyTotmop);die;
				$this->set('RubyTotmop',$RubyTotmop);
				
						
				
				$prodList = $this->setFilterProductNumber($this->Session->read('stores_id'));
				$this->set(compact('prodList'));				
				//$prodList = array(1=> 'Diesel', 4 => 'Regular', 5 => 'Premium', 6 => 'Plus'); //pr($prodList);
				$conditions22 = array('RubyHeader.ending_date_time ='=>$date,'RubyHeader.store_id'=>$this->Session->read('stores_id'));
				
				if(!empty($prodList)) {
				$conditions22['fuel_product_number'] = array_keys($prodList);
				} else {
				$conditions22['fuel_product_number'] = array();
				}
				//pr($conditions22);
				$rubyUprodts = $this->RubyUprodt->find('all',array('conditions'=>$conditions22,'order'=>array('RubyUprodt.id ASC')));
				//pr($rubyUprodts);die;
				$this->set('rubyUprodts', $rubyUprodts);
				
				
				
				/*$volume = array('Regular','Premium','Plus','Diesel');
				$data=array();
				foreach($volume as $key=>$value){
			
							$fvolume = $this->RubyUtiert->find('first',array( "joins" => array(
							array(
								"table" => "ruby_fprods",
								"alias" => "ruby_fprod",
								"type" => "LEFT",
								"conditions" => array(
									"ruby_fprod.fuel_product_number = RubyUtiert.fuel_product_number"
								)
							)),'conditions'=>array('ruby_fprod.display_name'=>$value,'ruby_fprod.store_id'=>$this->Session->read('stores_id'),'RubyUtiert.price_tier_number'=>1,'RubyUtiert.ruby_header_id'=>@$headerid['RubyHeader']['id']),'order'=>array('RubyUtiert.id'=>'desc'),'fields'=>array('RubyUtiert.fuel_volume,RubyUtiert.fuel_value')));
							// pr($fvolume);
							if(!empty($fvolume)){
							$resu =$fvolume['RubyUtiert']['fuel_volume'].'*'.$fvolume['RubyUtiert']['fuel_value'];
							//pr($resu);
				            $this->set($value,$resu);
							}
							
							
				}*/
				
						
			  }
			}
}


}
