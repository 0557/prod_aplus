<?php
App::uses('AppController', 'Controller');
/**
 * RubyDcrstats Controller
 *
 * @property RubyDcrstat $RubyDcrstat
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyDcrstatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyDcrstat->recursive = 0;
		$this->set('rubyDcrstats', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyDcrstat->exists($id)) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		$options = array('conditions' => array('RubyDcrstat.' . $this->RubyDcrstat->primaryKey => $id));
		$this->set('rubyDcrstat', $this->RubyDcrstat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyDcrstat->create();
			if ($this->RubyDcrstat->save($this->request->data)) {
				$this->Flash->success(__('The ruby dcrstat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby dcrstat could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyDcrstat->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyDcrstat->exists($id)) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyDcrstat->save($this->request->data)) {
				$this->Flash->success(__('The ruby dcrstat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby dcrstat could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyDcrstat.' . $this->RubyDcrstat->primaryKey => $id));
			$this->request->data = $this->RubyDcrstat->find('first', $options);
		}
		$rubyHeaders = $this->RubyDcrstat->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyDcrstat->id = $id;
		if (!$this->RubyDcrstat->exists()) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyDcrstat->delete()) {
			$this->Flash->success(__('The ruby dcrstat has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby dcrstat could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$this->RubyDcrstat->recursive = 0;
		$storeId = $_SESSION['store_id'];
		
		$this->paginate = array('conditions' => array('RubyHeader.store_id' => $storeId), 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			)
		);
		$this->set('rubyDcrstats', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyDcrstat->exists($id)) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		$options = array('conditions' => array('RubyDcrstat.' . $this->RubyDcrstat->primaryKey => $id));
		$this->set('rubyDcrstat', $this->RubyDcrstat->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyDcrstat->create();
			if ($this->RubyDcrstat->save($this->request->data)) {
				$this->Flash->success(__('The ruby dcrstat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby dcrstat could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyDcrstat->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyDcrstat->exists($id)) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyDcrstat->save($this->request->data)) {
				$this->Flash->success(__('The ruby dcrstat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby dcrstat could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyDcrstat.' . $this->RubyDcrstat->primaryKey => $id));
			$this->request->data = $this->RubyDcrstat->find('first', $options);
		}
		$rubyHeaders = $this->RubyDcrstat->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyDcrstat->id = $id;
		if (!$this->RubyDcrstat->exists()) {
			throw new NotFoundException(__('Invalid ruby dcrstat'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyDcrstat->delete()) {
			$this->Flash->success(__('The ruby dcrstat has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby dcrstat could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
