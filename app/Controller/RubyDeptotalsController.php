<?php
App::uses('AppController', 'Controller');
error_reporting(0);
/**
 * RubyDeptotals Controller
 *
 * @property RubyDeptotal $RubyDeptotal
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyDeptotalsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyDeptotal->recursive = 0;
		$this->Render=false;
			
		/*$from_date = $this->data['from_date'];*/
		$to_date = $this->data['to_date'];
		//$department_number = $this->data['department_number'];
		
		$storeId = $this->getStoreId();
		$conditions = array('RubyDeptotal.store_id' => $storeId);
		
		/*if ($from_date) {
			$from_dates = explode('-', $from_date);//14-12-2015
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			$conditions['RubyDeptotal.beginning_date_time'] = $from_date;
		}*/
		
		
		if ($to_date)  { 
			$to_dates = explode('-', $to_date);//14-12-2015
			$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
			$conditions['RubyDeptotal.ending_date_time']   = $to_date;
		}	
		
	//	if ($department_number) $conditions['RubyDeptotal.department_number']   = $department_number;
		
		//print_r($conditions);exit;
		
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyDeptotal.ending_date_time' => 'desc'
			)
		);
		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));
		

		$this->set('rdepartments', $rdepartments);
		
	
		
		$this->set('rubyDeptotals', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyDeptotal->exists($id)) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		$options = array('conditions' => array('RubyDeptotal.' . $this->RubyDeptotal->primaryKey => $id));
		$this->set('rubyDeptotal', $this->RubyDeptotal->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyDeptotal->create();
			if ($this->RubyDeptotal->save($this->request->data)) {
				$this->Flash->success(__('The ruby deptotal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby deptotal could not be saved. Please, try again.'));
			}
		}
		$stores = $this->RubyDeptotal->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyDeptotal->exists($id)) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyDeptotal->save($this->request->data)) {
				$this->Flash->success(__('The ruby deptotal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby deptotal could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyDeptotal.' . $this->RubyDeptotal->primaryKey => $id));
			$this->request->data = $this->RubyDeptotal->find('first', $options);
		}
		$stores = $this->RubyDeptotal->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyDeptotal->id = $id;
		if (!$this->RubyDeptotal->exists()) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyDeptotal->delete()) {
			$this->Flash->success(__('The ruby deptotal has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby deptotal could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->loadmodel('RubyDeptotal');
		$this->RubyDeptotal->recursive = 0;
		
		$storeId = $this->getStoreId();
		$conditions = array('RubyDeptotal.store_id' => $storeId);
		$first = $this->RubyDeptotal->find('first',array('conditions'=>$conditions, 'order'=>array('RubyDeptotal.id'=>'desc')));
		if(!empty($first)){
			
		
		if ($first['RubyDeptotal']['beginning_date_time']) {
		//	$from_dates = explode('-', $first['RubyDeptotal']['beginning_date_time']);//14-12-2015
		//	$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			$conditions['RubyDeptotal.beginning_date_time'] = $first['RubyDeptotal']['beginning_date_time'];
		}
		
		
		if ($first['RubyDeptotal']['ending_date_time'])  { 
		//	$to_dates = explode('-', $first['RubyDeptotal']['ending_date_time']);//14-12-2015
		//	$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
			$conditions['RubyDeptotal.ending_date_time']   = $first['RubyDeptotal']['ending_date_time'];
		}	
		
		
		
		$this->set('startdate',$first['RubyDeptotal']['beginning_date_time']);
		$this->set('enddate',$first['RubyDeptotal']['ending_date_time']);
		}
		//print_r($conditions);die;
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyDeptotal.ending_date_time' => 'desc'
			)
		);
		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
			
	$total = $this->RubyDeptotal->find('all', array('fields' => array('sum(RubyDeptotal.net_sales)   AS ctotal'), 'conditions'=>$conditions));
	
		$this->set('total', $total);
		
		$this->set('rubyDeptotals', $this->Paginator->paginate());
	}

	public function admin_dashboard() {
		$this->loadmodel('RubyDeptotal');
		$this->RubyDeptotal->recursive = 0;
		
		$storeId = $this->getStoreId();
		$conditions = array('RubyDeptotal.store_id' => $storeId);
	if ($this->request->is('post')) {
	
		$to_dates = explode('-', $this->request->data['from']);//14-12-2015
		$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
		$conditions['RubyDeptotal.ending_date_time'] = $to_date;
		$this->set('enddate',$to_date);
		
	}else{
	//	print_r($conditions);
	$first = $this->RubyDeptotal->find('first',array('conditions'=>$conditions, 'order'=>array('RubyDeptotal.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyDeptotal']['ending_date_time'])  { 
		//	$to_dates = explode('-', $first['RubyDeptotal']['ending_date_time']);//14-12-2015
		//	$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
			$conditions['RubyDeptotal.ending_date_time =']   = $first['RubyDeptotal']['ending_date_time'];
		}	
	//	echo $first['RubyDeptotal']['ending_date_time'];
		$this->set('enddate',$first['RubyDeptotal']['ending_date_time']);
		}		
	}
	//print_r($conditions);
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyDeptotal.ending_date_time' => 'desc'
			)
		);
		//print_r($this->Paginator->paginate());
		$this->set('rubyDeptotals', $this->Paginator->paginate());
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyDeptotal->exists($id)) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		$options = array('conditions' => array('RubyDeptotal.' . $this->RubyDeptotal->primaryKey => $id));
		$this->set('rubyDeptotal', $this->RubyDeptotal->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyDeptotal->create();
			if ($this->RubyDeptotal->save($this->request->data)) {
				$this->Flash->success(__('The ruby deptotal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby deptotal could not be saved. Please, try again.'));
			}
		}
		$stores = $this->RubyDeptotal->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyDeptotal->exists($id)) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyDeptotal->save($this->request->data)) {
				$this->Flash->success(__('The ruby deptotal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby deptotal could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyDeptotal.' . $this->RubyDeptotal->primaryKey => $id));
			$this->request->data = $this->RubyDeptotal->find('first', $options);
		}
		$stores = $this->RubyDeptotal->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyDeptotal->id = $id;
		if (!$this->RubyDeptotal->exists()) {
			throw new NotFoundException(__('Invalid ruby deptotal'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyDeptotal->delete()) {
			$this->Flash->success(__('The ruby deptotal has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby deptotal could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function admin_dapartment_sales(){
		
	}
	public function admin_sales_by_hour(){
		
	}
	public function admin_credit_card_summary(){
		
	}
}
