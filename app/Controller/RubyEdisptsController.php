<?php
App::uses('AppController', 'Controller');
/**
 * RubyEdispts Controller
 *
 * @property RubyEdispt $RubyEdispt
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyEdisptsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyEdispt->recursive = 0;
		$this->set('rubyEdispts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyEdispt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		$options = array('conditions' => array('RubyEdispt.' . $this->RubyEdispt->primaryKey => $id));
		$this->set('rubyEdispt', $this->RubyEdispt->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyEdispt->create();
			if ($this->RubyEdispt->save($this->request->data)) {
				$this->Flash->success(__('The ruby edispt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby edispt could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyEdispt->RubyHeader->find('list');
		$fuelProducts = $this->RubyEdispt->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyEdispt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyEdispt->save($this->request->data)) {
				$this->Flash->success(__('The ruby edispt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby edispt could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyEdispt.' . $this->RubyEdispt->primaryKey => $id));
			$this->request->data = $this->RubyEdispt->find('first', $options);
		}
		$rubyHeaders = $this->RubyEdispt->RubyHeader->find('list');
		$fuelProducts = $this->RubyEdispt->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyEdispt->id = $id;
		if (!$this->RubyEdispt->exists()) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyEdispt->delete()) {
			$this->Flash->success(__('The ruby edispt has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby edispt could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$this->RubyEdispt->recursive = 0;
		$storeId = $_SESSION['store_id'];
		$this->paginate = array('conditions' => array('RubyHeader.store_id' => $storeId), 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			)
		);
		$this->set('rubyEdispts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyEdispt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		$options = array('conditions' => array('RubyEdispt.' . $this->RubyEdispt->primaryKey => $id));
		$this->set('rubyEdispt', $this->RubyEdispt->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyEdispt->create();
			if ($this->RubyEdispt->save($this->request->data)) {
				$this->Flash->success(__('The ruby edispt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby edispt could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyEdispt->RubyHeader->find('list');
		$fuelProducts = $this->RubyEdispt->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyEdispt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyEdispt->save($this->request->data)) {
				$this->Flash->success(__('The ruby edispt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby edispt could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyEdispt.' . $this->RubyEdispt->primaryKey => $id));
			$this->request->data = $this->RubyEdispt->find('first', $options);
		}
		$rubyHeaders = $this->RubyEdispt->RubyHeader->find('list');
		$fuelProducts = $this->RubyEdispt->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyEdispt->id = $id;
		if (!$this->RubyEdispt->exists()) {
			throw new NotFoundException(__('Invalid ruby edispt'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyEdispt->delete()) {
			$this->Flash->success(__('The ruby edispt has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby edispt could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
