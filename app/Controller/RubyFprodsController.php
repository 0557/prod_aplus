<?php
App::uses('AppController', 'Controller');
/**
 * RubyFprods Controller
 *
 * @property RubyFprod $RubyFprod
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyFprodsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyFprod->recursive = 0;
		
		$this->set('rubyFprods', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyFprod->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		$options = array('conditions' => array('RubyFprod.' . $this->RubyFprod->primaryKey => $id));
		$this->set('rubyFprod', $this->RubyFprod->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyFprod->create();			
			if ($this->RubyFprod->save($this->request->data)) {
				$this->Flash->success(__('The ruby fprod has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fprod could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyFprod->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
			$this->loadModel('RubyFprod');
		$this->loadModel('RubyHeader');
		if (!$this->RubyFprod->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyFprod->save($this->request->data)) {
				$this->Flash->success(__('The ruby fprod has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fprod could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyFprod.' . $this->RubyFprod->primaryKey => $id));
			$this->request->data = $this->RubyFprod->find('first', $options);
		}
		$rubyHeaders = $this->RubyFprod->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyFprod->id = $id;
		if (!$this->RubyFprod->exists()) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFprod->delete()) {
			$this->Flash->success(__('The ruby fprod has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby fprod could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$this->RubyFprod->recursive = 0;
		$this->paginate = array('conditions' => array('store_id'=>$this->Session->read('stores_id')),'order'=>array('sequence','desc'));
		$this->set('rubyFprods', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyFprod->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		$options = array('conditions' => array('RubyFprod.' . $this->RubyFprod->primaryKey => $id));
		$this->set('rubyFprod', $this->RubyFprod->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
			$this->loadModel('RubyFprod');
		$this->loadModel('RubyHeader');
		if ($this->request->is('post')) {
			$this->RubyFprod->create();			
			//print_r($this->request->data);die;
			if($this->request->data['RubyFprod']['fuel_product_number']!='' && $this->request->data['RubyFprod']['fuel_product_name']!='' && $this->request->data['RubyFprod']['display_name']!=''){
			$this->request->data['RubyFprod']['store_id'] = $this->Session->read('stores_id');
			if ($this->RubyFprod->save($this->request->data)) {
				//$id = $this->RubyFprod->getLastInsertId();
				
				//$this->request->data['RubyFprod']['id'] = $id;
				//$this->request->data['RubyFprod']['fuel_product_number']=$id;
				//$this->RubyFprod->save($this->request->data);				
				$this->Session->setFlash(__("The ruby fprod has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {				
				$this->Session->setFlash(__("The ruby fprod could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));
			}
		  }else{			   
				$this->Session->setFlash(__("Please fillup all fields."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));
		  }
		}
		$rubyHeaders = $this->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

	public function admin_edit($id = null) {
		$this->loadModel('RubyHeader');
		$this->loadModel('RubyFprod');
		if (!$this->RubyFprod->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyFprod->save($this->request->data)) {			
				
				$this->Session->setFlash(__("The ruby fprod has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'ruby_fprods','action' => 'edit',$id));	
		
				
			} else {				
				
				$this->Session->setFlash(__("The ruby fprod could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('controller' => 'ruby_fprods','action' => 'edit',$id));	
			}
		} else {
			$options = array('conditions' => array('RubyFprod.' . $this->RubyFprod->primaryKey => $id));
			$this->request->data = $this->RubyFprod->find('first', $options);
		}
		$rubyHeaders = $this->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyFprod->id = $id;
		if (!$this->RubyFprod->exists()) {
			throw new NotFoundException(__('Invalid ruby fprod'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFprod->delete()) {		
			$this->Session->setFlash(__("The ruby fprod has been deleted."), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__("The ruby fprod could not be deleted. Please, try again."), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));	
			
		}
		return $this->redirect(array('action' => 'index'));
	}
}
