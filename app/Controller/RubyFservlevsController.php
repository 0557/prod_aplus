<?php
App::uses('AppController', 'Controller');
/**
 * RubyFservlevs Controller
 *
 * @property RubyFservlev $RubyFservlev
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyFservlevsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyFservlev->recursive = 0;
		$this->set('rubyFservlevs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyFservlev->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		$options = array('conditions' => array('RubyFservlev.' . $this->RubyFservlev->primaryKey => $id));
		$this->set('rubyFservlev', $this->RubyFservlev->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyFservlev->create();
			if ($this->RubyFservlev->save($this->request->data)) {
				$this->Flash->success(__('The ruby fservlev has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fservlev could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyFservlev->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyFservlev->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyFservlev->save($this->request->data)) {
				$this->Flash->success(__('The ruby fservlev has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fservlev could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyFservlev.' . $this->RubyFservlev->primaryKey => $id));
			$this->request->data = $this->RubyFservlev->find('first', $options);
		}
		$rubyHeaders = $this->RubyFservlev->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyFservlev->id = $id;
		if (!$this->RubyFservlev->exists()) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFservlev->delete()) {
			$this->Flash->success(__('The ruby fservlev has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby fservlev could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RubyFservlev->recursive = 0;
		$this->set('rubyFservlevs', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyFservlev->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		$options = array('conditions' => array('RubyFservlev.' . $this->RubyFservlev->primaryKey => $id));
		$this->set('rubyFservlev', $this->RubyFservlev->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyFservlev->create();
			if ($this->RubyFservlev->save($this->request->data)) {
				$this->Flash->success(__('The ruby fservlev has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fservlev could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyFservlev->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyFservlev->exists($id)) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyFservlev->save($this->request->data)) {
				$this->Flash->success(__('The ruby fservlev has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby fservlev could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyFservlev.' . $this->RubyFservlev->primaryKey => $id));
			$this->request->data = $this->RubyFservlev->find('first', $options);
		}
		$rubyHeaders = $this->RubyFservlev->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyFservlev->id = $id;
		if (!$this->RubyFservlev->exists()) {
			throw new NotFoundException(__('Invalid ruby fservlev'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFservlev->delete()) {
			$this->Flash->success(__('The ruby fservlev has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby fservlev could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
