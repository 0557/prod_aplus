<?php
App::uses('AppController', 'Controller');
/**
 * RubyFtanks Controller
 *
 * @property RubyFtank $RubyFtank
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyFtanksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyFtank->recursive = 0;
		$this->set('rubyFtanks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyFtank->exists($id)) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
		$options = array('conditions' => array('RubyFtank.' . $this->RubyFtank->primaryKey => $id));
		$this->set('rubyFtank', $this->RubyFtank->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyFtank->create();
			if ($this->RubyFtank->save($this->request->data)) {
				$this->Flash->success(__('The ruby ftank has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby ftank could not be saved. Please, try again.'));
			}
		}
		$stores = $this->RubyFtank->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyFtank->exists($id)) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyFtank->save($this->request->data)) {
				$this->Flash->success(__('The ruby ftank has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby ftank could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyFtank.' . $this->RubyFtank->primaryKey => $id));
			$this->request->data = $this->RubyFtank->find('first', $options);
		}
		$stores = $this->RubyFtank->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyFtank->id = $id;
		if (!$this->RubyFtank->exists()) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFtank->delete()) {
			$this->Flash->success(__('The ruby ftank has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby ftank could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {		
		//$this->RubyFtank->recursive = 0;		
		//$this->set('rubyFtanks', $this->Paginator->paginate());
		$store_id=$this->Session->read('stores_id');
		$this->Paginator->settings = array(
			'conditions' =>array('RubyFtank.store_id' =>$store_id ),
			'limit' => 100,
			'order'=>array('RubyFtank.id'=>'asc')
		);
		$data= $this->Paginator->paginate('RubyFtank');	
		//echo '<pre>';print_r($data);die;     
		$this->set('rubyFtanks',$data);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyFtank->exists($id)) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
		$options = array('conditions' => array('RubyFtank.' . $this->RubyFtank->primaryKey => $id));
		$this->set('rubyFtank', $this->RubyFtank->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
			
			$this->RubyFtank->create();
			
			if($this->request->data['RubyFtank']['tank_number']!='' && $this->request->data['RubyFtank']['name']!=''){
			
			$this->request->data['RubyFtank']['store_id']=$this->Session->read('stores_id');		
			//print_r($this->request->data);die;
			if ($this->RubyFtank->save($this->request->data)) {			
				
				$this->Session->setFlash(__("The ruby ftank has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));			
				
			} else {			
				$this->Session->setFlash(__("The ruby ftank could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));				
			}
		  
		   }else{			   
				$this->Session->setFlash(__("Please fillup all fields."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));
		  }
			
			
			
		}
		
	}

	public function admin_edit($id = null) {
		if (!$this->RubyFtank->exists($id)) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
	      if ($this->request->is('post')) {			        
				//print_r($this->request->data);die;	
				$this->request->data['RubyFtank']['id'] =$id;   
			if ($this->RubyFtank->save($this->request->data)) {			
				
				$this->Session->setFlash(__("The ruby ftank has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'ruby_ftanks','action' => 'edit',$id));			
				
			} else {			
				$this->Session->setFlash(__("The ruby ftank could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('controller' => 'ruby_ftanks','action' => 'edit',$id));			
			}		
						
						
		} else {			
			//echo 'not posted';die;
			$options = array('conditions' => array('RubyFtank.' . $this->RubyFtank->primaryKey => $id));
			$this->request->data = $this->RubyFtank->find('first', $options);
		}
		
	}


	public function admin_delete($id = null) {
		$this->RubyFtank->id = $id;
		if (!$this->RubyFtank->exists()) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyFtank->delete()) {		
			$this->Session->setFlash(__("The ruby ftank has been deleted."), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		} else {	
			$this->Session->setFlash(__("The ruby ftank could not be deleted. Please, try again."), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));	
		}
		return $this->redirect(array('action' => 'index'));
	}
}
