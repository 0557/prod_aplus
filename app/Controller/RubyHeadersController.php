<?php
App::uses('AppController', 'Controller');
/**
 * RubyHeaders Controller
 *
 * @property RubyHeader $RubyHeader
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyHeadersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyHeader->recursive = 0;
		$this->set('rubyHeaders', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyHeader->exists($id)) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		$options = array('conditions' => array('RubyHeader.' . $this->RubyHeader->primaryKey => $id));
		$this->set('rubyHeader', $this->RubyHeader->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyHeader->create();
			if ($this->RubyHeader->save($this->request->data)) {
				$this->Flash->success(__('The ruby header has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby header could not be saved. Please, try again.'));
			}
		}
		$stores = $this->RubyHeader->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyHeader->exists($id)) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyHeader->save($this->request->data)) {
				$this->Flash->success(__('The ruby header has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby header could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyHeader.' . $this->RubyHeader->primaryKey => $id));
			$this->request->data = $this->RubyHeader->find('first', $options);
		}
		$stores = $this->RubyHeader->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyHeader->id = $id;
		if (!$this->RubyHeader->exists()) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyHeader->delete()) {
			$this->Flash->success(__('The ruby header has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby header could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RubyHeader->recursive = 0;
		$this->set('rubyHeaders', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyHeader->exists($id)) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		$options = array('conditions' => array('RubyHeader.' . $this->RubyHeader->primaryKey => $id));
		$this->set('rubyHeader', $this->RubyHeader->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyHeader->create();
			if ($this->RubyHeader->save($this->request->data)) {
				$this->Flash->success(__('The ruby header has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby header could not be saved. Please, try again.'));
			}
		}
		$stores = $this->RubyHeader->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyHeader->exists($id)) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyHeader->save($this->request->data)) {
				$this->Flash->success(__('The ruby header has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby header could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyHeader.' . $this->RubyHeader->primaryKey => $id));
			$this->request->data = $this->RubyHeader->find('first', $options);
		}
		$stores = $this->RubyHeader->Store->find('list');
		$this->set(compact('stores'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyHeader->id = $id;
		if (!$this->RubyHeader->exists()) {
			throw new NotFoundException(__('Invalid ruby header'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyHeader->delete()) {
			$this->Flash->success(__('The ruby header has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby header could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
