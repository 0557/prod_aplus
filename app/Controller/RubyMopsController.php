<?php
App::uses('AppController', 'Controller');
/**
 * RubyMops Controller
 *
 * @property RubyMop $RubyMop
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyMopsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyMop->recursive = 0;
		$this->set('rubyMops', $this->Paginator->paginate());
	}
	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
		$this->set('rubyMop', $this->RubyMop->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyMop->create();
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
			$this->request->data = $this->RubyMop->find('first', $options);
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyMop->id = $id;
		if (!$this->RubyMop->exists()) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyMop->delete()) {
			$this->Flash->success(__('The ruby mop has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby mop could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {		
		//$this->RubyMop->recursive = 0;
		//$this->set('rubyMops', $this->Paginator->paginate());	
		$store_id=$this->Session->read('stores_id');
		$this->Paginator->settings = array(
			'conditions' =>array('RubyMop.store_id' =>$store_id ),
			'limit' => 100,
			'order'=>array('RubyMop.id'=>'asc')
		);
		$data= $this->Paginator->paginate('RubyMop');	
		$this->set('rubyMops',$data);	
	}
	
	

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
		$this->set('rubyMop', $this->RubyMop->find('first', $options));
	}




      public function admin_add() {
		if ($this->request->is('post')) {
			
			
			$this->RubyMop->create();
			
			if($this->request->data['RubyMop']['number']!='' && $this->request->data['RubyMop']['name']!=''){
			
			$this->request->data['RubyMop']['store_id']=$this->Session->read('stores_id');		
			//print_r($this->request->data);die;
			if ($this->RubyMop->save($this->request->data)) {			
				
				$this->Session->setFlash(__("The ruby mop has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));			
				
			} else {			
				$this->Session->setFlash(__("The ruby mop could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));				
			}
		  
		   }else{			   
				$this->Session->setFlash(__("Please fillup all fields."), 'default', array('class' => 'error'));
				$this->redirect(array('action' => 'add'));
		  }
			
			
			
		}
		
	}

	public function admin_edit($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby ftank'));
		}
	      if ($this->request->is('post')) {			        
				//print_r($this->request->data);die;	
				$this->request->data['RubyMop']['id'] =$id;   
			if ($this->RubyMop->save($this->request->data)) {			
				
				$this->Session->setFlash(__("The ruby mop has been saved."), 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'ruby_mops','action' => 'edit',$id));			
				
			} else {			
				$this->Session->setFlash(__("The ruby mop could not be saved. Please, try again."), 'default', array('class' => 'error'));
				$this->redirect(array('controller' => 'ruby_mops','action' => 'edit',$id));			
			}		
						
						
		} else {			
			//echo 'not posted';die;
			$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
			$this->request->data = $this->RubyMop->find('first', $options);
		}
		
	}










	public function admin_delete($id = null) {
		$this->RubyMop->id = $id;
		if (!$this->RubyMop->exists()) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyMop->delete()) {			
			$this->Session->setFlash(__("The ruby mop has been deleted."), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));	
			
		} else {			
			$this->Session->setFlash(__("The ruby mop could not be deleted. Please, try again."), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));	
		}
		return $this->redirect(array('action' => 'index'));
	}
}
