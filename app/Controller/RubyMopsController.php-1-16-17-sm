<?php
App::uses('AppController', 'Controller');
/**
 * RubyMops Controller
 *
 * @property RubyMop $RubyMop
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyMopsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyMop->recursive = 0;
		$this->set('rubyMops', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
		$this->set('rubyMop', $this->RubyMop->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyMop->create();
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
			$this->request->data = $this->RubyMop->find('first', $options);
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyMop->id = $id;
		if (!$this->RubyMop->exists()) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyMop->delete()) {
			$this->Flash->success(__('The ruby mop has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby mop could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RubyMop->recursive = 0;
		$this->set('rubyMops', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
		$this->set('rubyMop', $this->RubyMop->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyMop->create();
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyMop->exists($id)) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyMop->save($this->request->data)) {
				$this->Flash->success(__('The ruby mop has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby mop could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyMop.' . $this->RubyMop->primaryKey => $id));
			$this->request->data = $this->RubyMop->find('first', $options);
		}
		$rubyHeaders = $this->RubyMop->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyMop->id = $id;
		if (!$this->RubyMop->exists()) {
			throw new NotFoundException(__('Invalid ruby mop'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyMop->delete()) {
			$this->Flash->success(__('The ruby mop has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby mop could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
