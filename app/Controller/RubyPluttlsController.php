<?php
error_reporting(0);
App::uses('AppController', 'Controller');
/**
 * RubyPluttls Controller
 *
 * @property RubyPluttl $RubyPluttl
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyPluttlsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function admin_dashboard() {
		$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->getStoreId();
		$conditions = array('RubyPluttl.store_id' => $storeId);
	if ($this->request->is('post')) {

					$conditions['RubyHeader.store_id'] = $storeId;
				$to_dates = explode('-', $this->request->data['from']);//14-12-2015
				$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
				$conditions['RubyHeader.ending_date_time'] = $to_date;
				$this->set('enddate',$to_date);

//print_r($this->request->data['ruby_deptotals']['department']);
			if($this->request->data['ruby_deptotals']['department']){
		//	echo $this->request->data['department'];
	$conditions['r_grocery_item.r_grocery_department_id'] = $this->request->data['ruby_deptotals']['department'];
	$conditions['r_grocery_item.store_id'] = $storeId;
			
			$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc')));
			$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
						//	echo $storeId; 
							//print_r($fvolume);exit;
		}else{
		
		
		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc')));
							
							
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
		}
	
	
	
	
	

	}else{

	$first = $this->RubyPluttl->find('first',array('conditions'=>array('RubyPluttl.store_id'=>$storeId), 'order'=>array('RubyPluttl.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyPluttl']['ruby_header_id'])  {

		$rubyfind = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.id'=>$first['RubyPluttl']['ruby_header_id'],'RubyHeader.store_id' => $storeId), 'order'=>array('RubyHeader.id'=>'desc')));
		
		$conditions['RubyHeader.ending_date_time']=$rubyfind['RubyHeader']['ending_date_time'];
		}	
//	echo $rubyfind['RubyHeader']['ending_date_time'];
//	echo $rubyfind['RubyHeader']['id'];
		$this->set('enddate',$rubyfind['RubyHeader']['ending_date_time']);
		}		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc')));
							
							
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('RubyPluttl.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));

	}


		$this->set('somevalue', $sumevalue);
		$this->set('pluttls', $fvolume);
		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
	}
	
public function admin_top_sales(){
				//$this->layout="blank"; 
				$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
		$this->loadmodel('RubyDepartment');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->getStoreId();
				
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));
		
		$conditions = array('RubyPluttl.store_id' => $storeId);
		
	if ($this->request->is('post')) { 
	$vari = explode('-', $this->request->data['from']);
	
	//$conditions['RubyHeader.store_id'] = $storeId;
		$to_dates = explode('/', $vari[0]);//14-12-2015
		$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];

		$conditions['RubyHeader.ending_date_time >='] = str_replace(' ','',$to_date);
		
		$to_dates1 = explode('/', $vari[1]);//14-12-2015
		$to_date1 = $to_dates1[2].'-'.$to_dates1[0].'-'.$to_dates1[1];
		$conditions['RubyHeader.ending_date_time <='] =  str_replace(' ','',$to_date1);
		$this->set('enddate1',str_replace(' ','',$to_date).' to '.str_replace(' ','',$to_date1));
		if($this->request->data['department']){
		//	echo $this->request->data['department'];
	$conditions['r_grocery_item.r_grocery_department_id'] = $this->request->data['department'];
	$conditions['r_grocery_item.store_id'] = $storeId;
			
			$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "LEFT",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no'));
						//	echo $storeId; 
							//print_r($fvolume);exit;
		}else{
			
		
				$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no'));
							
							}
	//	print_r($fvolume);
		$this->set('pluttls', $fvolume);
	}else{

	$first = $this->RubyPluttl->find('first',array('conditions'=>array('RubyPluttl.store_id'=>$storeId), 'order'=>array('RubyPluttl.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyPluttl']['ruby_header_id'])  {

		$rubyfind = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.id'=>$first['RubyPluttl']['ruby_header_id'],'RubyHeader.store_id' => $storeId), 'order'=>array('RubyHeader.id'=>'desc')));
		
		$conditions['RubyHeader.ending_date_time']=$rubyfind['RubyHeader']['ending_date_time'];
		}	
//	echo $rubyfind['RubyHeader']['ending_date_time'];
//	echo $rubyfind['RubyHeader']['id'];
		$this->set('enddate',$rubyfind['RubyHeader']['ending_date_time']);
		}
 
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no','limit'=>10));
	//	print_r($fvolume);
		$this->set('pluttls', $fvolume);		
	}
//	print_r($conditions);exit;

		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
				
}

public function admin_low_sales(){
				//$this->layout="blank"; 
				$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
		$this->loadmodel('RubyDepartment');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->getStoreId();
				
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));
		
		$conditions = array('RubyPluttl.store_id' => $storeId);
		
	if ($this->request->is('post')) { 
	$vari = explode('-', $this->request->data['from']);
	
	//$conditions['RubyHeader.store_id'] = $storeId;
		$to_dates = explode('/', $vari[0]);//14-12-2015
		$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];

		$conditions['RubyHeader.ending_date_time >='] = str_replace(' ','',$to_date);
		
		$to_dates1 = explode('/', $vari[1]);//14-12-2015
		$to_date1 = $to_dates1[2].'-'.$to_dates1[0].'-'.$to_dates1[1];
		$conditions['RubyHeader.ending_date_time <='] =  str_replace(' ','',$to_date1);
		$this->set('enddate1',str_replace(' ','',$to_date).' to '.str_replace(' ','',$to_date1));
		if($this->request->data['department']){
		//	echo $this->request->data['department'];
	$conditions['r_grocery_item.r_grocery_department_id'] = $this->request->data['department'];
	$conditions['r_grocery_item.store_id'] = $storeId;
			
			$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "LEFT",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'asc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no'));
						//	echo $storeId; 
							//print_r($fvolume);exit;
		}else{
			
		
				$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'asc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no'));
							
							}
	//	print_r($fvolume);
		$this->set('pluttls', $fvolume);
	}else{

	$first = $this->RubyPluttl->find('first',array('conditions'=>array('RubyPluttl.store_id'=>$storeId), 'order'=>array('RubyPluttl.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyPluttl']['ruby_header_id'])  {

		$rubyfind = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.id'=>$first['RubyPluttl']['ruby_header_id'],'RubyHeader.store_id' => $storeId), 'order'=>array('RubyHeader.id'=>'desc')));
		
		$conditions['RubyHeader.ending_date_time']=$rubyfind['RubyHeader']['ending_date_time'];
		}	
//	echo $rubyfind['RubyHeader']['ending_date_time'];
//	echo $rubyfind['RubyHeader']['id'];
		$this->set('enddate',$rubyfind['RubyHeader']['ending_date_time']);
		}
 
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'asc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no','limit'=>10));
	//	print_r($fvolume);
		$this->set('pluttls', $fvolume);		
	}
//	print_r($conditions);exit;

		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
				
}

	function admin_summary(){
						//$this->layout="blank"; 
				$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
		$this->loadmodel('RubyDepartment');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->getStoreId();
				
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));
		
		$conditions = array('RubyPluttl.store_id' => $storeId);
		
	if ($this->request->is('post')) { 
	$vari = explode('-', $this->request->data['from']);
	
	//$conditions['RubyHeader.store_id'] = $storeId;
		$to_dates = explode('/', $vari[0]);//14-12-2015
		$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];

		$conditions['RubyHeader.ending_date_time >='] = str_replace(' ','',$to_date);
		
		$to_dates1 = explode('/', $vari[1]);//14-12-2015
		$to_date1 = $to_dates1[2].'-'.$to_dates1[0].'-'.$to_dates1[1];
		$conditions['RubyHeader.ending_date_time <='] =  str_replace(' ','',$to_date1);
		$this->set('enddate1',str_replace(' ','',$to_date).' to '.str_replace(' ','',$to_date1));
		
				$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "LEFT",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('no_of_items_sold'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold','RubyPluttl.plu_no','RubyPluttl.selling_price','RubyPluttl.base_seling_price','RubyPluttl.no_of_customer'),'group'=>'RubyPluttl.plu_no'));
							
							
	//	print_r($fvolume);
		$this->set('pluttls', $fvolume);
	}
//	print_r($conditions);exit;

		
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
	}

	
public function admin_profit() {
		$this->loadmodel('RubyPluttl');
		$this->loadmodel('RubyHeader');
		$this->loadmodel('PurchasePack');
		$this->RubyPluttl->recursive = 0;
		$this->RubyHeader->recursive = -1;
		
		$storeId = $this->getStoreId();
		$conditions = array('RubyPluttl.store_id' => $storeId);
	if ($this->request->is('post')) {

					$conditions['RubyHeader.store_id'] = $storeId;
				$to_dates = explode('-', $this->request->data['from']);//14-12-2015
				$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
				$conditions['RubyHeader.ending_date_time'] = $to_date;
				$this->set('enddate',$to_date);

//print_r($this->request->data['ruby_deptotals']['department']);
			if($this->request->data['ruby_deptotals']['department']){  
		//	echo $this->request->data['department'];
	$conditions['r_grocery_item.r_grocery_department_id'] = $this->request->data['ruby_deptotals']['department'];
	$conditions['r_grocery_item.store_id'] = $storeId;
			
			$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'fields'=>array('RubyPluttl.*','PurchasePack.Margin'),'order'=>array('PurchasePack.id'=>'desc')));
			$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							),array(
								"table" => "r_grocery_items",
								"alias" => "r_grocery_item",
								"type" => "INNER",
								"conditions" => array(
									"r_grocery_item.plu_no = RubyPluttl.plu_no"
								)
							)),'conditions'=>$conditions,'order'=>array('PurchasePack.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
						//	echo $storeId; 
							//print_r($fvolume);exit; 
		}else{
		
		
		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'fields'=>array('RubyPluttl.*','PurchasePack.Margin'),'order'=>array('PurchasePack.id'=>'desc')));
							
							
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('PurchasePack.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));
		}
	
	
	
	
	

	}else{

	$first = $this->RubyPluttl->find('first',array('conditions'=>array('RubyPluttl.store_id'=>$storeId), 'order'=>array('RubyPluttl.id'=>'desc')));
		if(!empty($first)){
		if ($first['RubyPluttl']['ruby_header_id'])  {

		$rubyfind = $this->RubyHeader->find('first',array('conditions'=>array('RubyHeader.id'=>$first['RubyPluttl']['ruby_header_id'],'RubyHeader.store_id' => $storeId), 'order'=>array('RubyHeader.id'=>'desc')));
		
		$conditions['RubyHeader.ending_date_time']=$rubyfind['RubyHeader']['ending_date_time'];
		}	
//	echo $rubyfind['RubyHeader']['ending_date_time'];
//	echo $rubyfind['RubyHeader']['id'];
		$this->set('enddate',$rubyfind['RubyHeader']['ending_date_time']);
		}		
		$fvolume = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'fields'=>array('RubyPluttl.*','PurchasePack.Margin'),'order'=>array('PurchasePack.id'=>'desc')));
					/*
$log = $this->RubyPluttl->getDataSource()->getLog(false, false);
debug($log);									
print_r($fvolume);*/
		$sumevalue = $this->RubyPluttl->find('all',array( "joins" => array(
							array(
								"table" => "ruby_headers",
								"alias" => "RubyHeader",
								"type" => "INNER",
								"conditions" => array(
									"RubyHeader.id = RubyPluttl.ruby_header_id",
									"RubyHeader.store_id" => $storeId
								)
							),array(
								"table" => "purchase_packs",
								"alias" => "PurchasePack",
								"type" => "INNER",
								"conditions" => array(
									"PurchasePack.Item_Scan_Code = RubyPluttl.plu_no",
									"PurchasePack.store_id" => $storeId
								)
							)),'conditions'=>$conditions,'order'=>array('PurchasePack.id'=>'desc'),'fields'=>array('sum(RubyPluttl.no_of_items_sold) as no_of_items_sold')));

	}


		$this->set('somevalue', $sumevalue);
		$this->set('pluttls', $fvolume);
		$this->loadmodel('RubyDepartment');
		$rdepartments =  $this->RubyDepartment->find('list', array('conditions' => array('store_id' => $storeId), 'fields' => array('number', 'name')));

		$this->set('rdepartments', $rdepartments);
	}	
	
}
