<?php
error_reporting(0);
App::uses('AppController', 'Controller');
 
 
class RubyTotsumsController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session');
	
    public function admin_index() {
		
		$this->Setredirect();		
		$this->loadModel('RubyUprodt');	
			
		$ruby_header_data = $this->RubyUprodt->find('first',array('conditions'=>array('store_id' =>$this->Session->read('stores_id')),'order'=>array('RubyUprodt.id DESC')));
		//pr($ruby_header_data);die;
		    $end_date= $ruby_header_data['RubyHeader']['ending_date_time'];
			$this->Session->write('end_date',$end_date);		
		    $conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
			if($this->request->is('post')){			
				if($this->request->data['end_date'] != "") {					   	
						$this->Session->write('end_date',$this->request->data['end_date']);	
				} else {
						$this->Session->delete('end_date');
				}		
			}	
			$end_date=$this->Session->read('end_date');
					
			if(isset($end_date) && $end_date!=''){
				$conditions['RubyHeader.ending_date_time']=$end_date;				
			}	
			
		$this->set('end_date',$end_date);
		
		$this->loadModel('RubyTotsum');  	
		$RubyTotsum=$this->RubyTotsum->find('all',array('conditions' => $conditions));		
		//pr($RubyTotsum);die;
        $this->set('RubyTotsum', $RubyTotsum);
	
    }
	
	
	public function admin_reset() {
		$this->Session->delete('end_date');
		$redirect_url = array('controller' => 'ruby_totsums', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}

	

	
	
	

	
	
	
	
	
}
