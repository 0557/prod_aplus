<?php
App::uses('AppController', 'Controller');
/**
 * RubyUautots Controller
 *
 * @property RubyUautot $RubyUautot
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyUautotsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyUautot->recursive = 0;
		$this->set('rubyUautots', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyUautot->exists($id)) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		$options = array('conditions' => array('RubyUautot.' . $this->RubyUautot->primaryKey => $id));
		$this->set('rubyUautot', $this->RubyUautot->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyUautot->create();
			if ($this->RubyUautot->save($this->request->data)) {
				$this->Flash->success(__('The ruby uautot has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby uautot could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUautot->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyUautot->exists($id)) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUautot->save($this->request->data)) {
				$this->Flash->success(__('The ruby uautot has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby uautot could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUautot.' . $this->RubyUautot->primaryKey => $id));
			$this->request->data = $this->RubyUautot->find('first', $options);
		}
		$rubyHeaders = $this->RubyUautot->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyUautot->id = $id;
		if (!$this->RubyUautot->exists()) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUautot->delete()) {
			$this->Flash->success(__('The ruby uautot has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby uautot could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$storeId = $_SESSION['store_id'];
		
		$this->paginate = array('conditions' => array('RubyHeader.store_id' => $storeId), 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			)
		);
		$this->RubyUautot->recursive = 0;
		$this->set('rubyUautots', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyUautot->exists($id)) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		$options = array('conditions' => array('RubyUautot.' . $this->RubyUautot->primaryKey => $id));
		$this->set('rubyUautot', $this->RubyUautot->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyUautot->create();
			if ($this->RubyUautot->save($this->request->data)) {
				$this->Flash->success(__('The ruby uautot has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby uautot could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUautot->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyUautot->exists($id)) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUautot->save($this->request->data)) {
				$this->Flash->success(__('The ruby uautot has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby uautot could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUautot.' . $this->RubyUautot->primaryKey => $id));
			$this->request->data = $this->RubyUautot->find('first', $options);
		}
		$rubyHeaders = $this->RubyUautot->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyUautot->id = $id;
		if (!$this->RubyUautot->exists()) {
			throw new NotFoundException(__('Invalid ruby uautot'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUautot->delete()) {
			$this->Flash->success(__('The ruby uautot has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby uautot could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
