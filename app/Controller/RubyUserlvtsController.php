<?php
App::uses('AppController', 'Controller');
/**
 * RubyUserlvts Controller
 *
 * @property RubyUserlvt $RubyUserlvt
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyUserlvtsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RubyUserlvt->recursive = 0;
		$this->set('rubyUserlvts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyUserlvt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		$options = array('conditions' => array('RubyUserlvt.' . $this->RubyUserlvt->primaryKey => $id));
		$this->set('rubyUserlvt', $this->RubyUserlvt->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyUserlvt->create();
			if ($this->RubyUserlvt->save($this->request->data)) {
				$this->Flash->success(__('The ruby userlvt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby userlvt could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUserlvt->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyUserlvt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUserlvt->save($this->request->data)) {
				$this->Flash->success(__('The ruby userlvt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby userlvt could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUserlvt.' . $this->RubyUserlvt->primaryKey => $id));
			$this->request->data = $this->RubyUserlvt->find('first', $options);
		}
		$rubyHeaders = $this->RubyUserlvt->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyUserlvt->id = $id;
		if (!$this->RubyUserlvt->exists()) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUserlvt->delete()) {
			$this->Flash->success(__('The ruby userlvt has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby userlvt could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$this->RubyUserlvt->recursive = 0;
		$storeId = $_SESSION['store_id'];
		$this->paginate = array('conditions' => array('RubyHeader.store_id' => $storeId), 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			)
		);
		$this->set('rubyUserlvts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyUserlvt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		$options = array('conditions' => array('RubyUserlvt.' . $this->RubyUserlvt->primaryKey => $id));
		$this->set('rubyUserlvt', $this->RubyUserlvt->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RubyUserlvt->create();
			if ($this->RubyUserlvt->save($this->request->data)) {
				$this->Flash->success(__('The ruby userlvt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby userlvt could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUserlvt->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyUserlvt->exists($id)) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUserlvt->save($this->request->data)) {
				$this->Flash->success(__('The ruby userlvt has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby userlvt could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUserlvt.' . $this->RubyUserlvt->primaryKey => $id));
			$this->request->data = $this->RubyUserlvt->find('first', $options);
		}
		$rubyHeaders = $this->RubyUserlvt->RubyHeader->find('list');
		$this->set(compact('rubyHeaders'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyUserlvt->id = $id;
		if (!$this->RubyUserlvt->exists()) {
			throw new NotFoundException(__('Invalid ruby userlvt'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUserlvt->delete()) {
			$this->Flash->success(__('The ruby userlvt has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby userlvt could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
