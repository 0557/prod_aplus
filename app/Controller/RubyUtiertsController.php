<?php
App::uses('AppController', 'Controller');
/**
 * RubyUtierts Controller
 *
 * @property RubyUtiert $RubyUtiert
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyUtiertsController extends AppController {

	public $components = array('Paginator', 'Flash', 'Session','ExportXls');
	
	public function admin_index() {
		$this->Setredirect();
		$this->RubyUtiert->recursive = 0;
		$storeId = $_SESSION['store_id'];
		
		$conditions = array('RubyHeader.store_id' => $storeId);	
		
		$full_date = '';
		$form_date='';
		$to_date='';
		
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['RubyUtiert']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['RubyUtiert']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
			
				
				if(isset($fdate) && $fdate!=''){				
						
				$conditions['RubyHeader.ending_date_time >='] = $fdate;			
				}
				if(isset($tdate) && $tdate!=''){						
				
				$conditions['RubyHeader.ending_date_time <=']  = $tdate;
				}
				//echo '<pre>';print_r($conditions);die;
				
				}	
      
        }else{
			    $current_date=date('Y-m-d');
	            $conditions['RubyHeader.ending_date_time >='] = $current_date;	
				$conditions['RubyHeader.ending_date_time <='] = $current_date;
		}
		
		//echo '<pre>';print_r($conditions);die;
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyUtiert.id' => 'desc'
			),
			'limit' => 10000
		);		
		//echo '<pre>';print_r($this->Paginator->paginate());die;		
		
		$this->set('full_date',$full_date);	
		$this->set('rubyUtierts', $this->Paginator->paginate());
	}
	
	public function admin_export()
	{
	            $this->RubyUtiert->recursive = 0;
	            $storeId = $_SESSION['store_id'];		
		        $conditions = array('RubyHeader.store_id' => $storeId);		
	
	            $date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['RubyUtiert']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['RubyUtiert']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
			
				
				if(isset($fdate) && $fdate!=''){				
						
				$conditions['RubyHeader.ending_date_time >='] = $fdate;			
				}
				if(isset($tdate) && $tdate!=''){						
				
				$conditions['RubyHeader.ending_date_time <=']  = $tdate;
				}
				
				}	
				//echo '<pre>';print_r($conditions);die;
				
	            //$rubyUtierts = $this->RubyUtiert->find('all',array('conditions' => $conditions,'order' => array('RubyHeader.ending_date_time' => 'desc')));
				//echo '<pre>';print_r($rubyUtierts);die;
				
				$this->paginate = array('conditions' => $conditions, 
				'order' => array(
				'RubyUtiert.id' => 'desc'
				),
				'limit' => 10000
				);		
				//echo '<pre>';print_r($this->Paginator->paginate());die;
				$rubyUtierts =$this->Paginator->paginate();				
				//echo '<pre>';print_r($rubyUtierts);die;
				 
				 
	$nwrubyutr=array();
	foreach ($rubyUtierts as $rubyUtiert)
	{
	$RubyFprod= ClassRegistry::init('RubyFprod')->find('first',array('conditions'=>array('RubyFprod.fuel_product_number' => $rubyUtiert['RubyUtiert']['fuel_product_number'])));
	//echo '<pre>';print_r($RubyFprod);
	$prdtlst=array('Regular','Plus','Premium','Diesel');
	//echo '|'.trim($RubyFprod['RubyFprod']['display_name']).'|';echo '<br>';
	//print_r($prdtlst);
	if(in_array(trim($RubyFprod['RubyFprod']['display_name']),$prdtlst))	
	{	
	$sbarray=array('date'=>$rubyUtiert['RubyHeader']['ending_date_time'],'product'=>trim($RubyFprod['RubyFprod']['display_name']),'gal'=>$rubyUtiert['RubyUtiert']['fuel_volume'],'prof'=>$rubyUtiert['RubyUtiert']['fuel_value']);
	array_push($nwrubyutr,$sbarray);
	}
	}
	
	//echo '<pre>';print_r($nwrubyutr);
	$date_array=array();
	foreach($nwrubyutr as $nwrubyutr_row)
	{
	$date=$nwrubyutr_row['date'];	
	array_push($date_array,$date);
	}
	$date_array=array_unique($date_array);
	//echo '<pre>';print_r($date_array);
	$nwdate_array=array();
	foreach($date_array as $key=>$val)
	{
	array_push($nwdate_array,$val);	
	}
	//echo '<pre>';print_r($nwdate_array);
	
	$date_count=count($nwdate_array);
	//echo $date_count;die;	
	
	$cstmz_rubyutr=array();
	
	for($i=0;$i<$date_count;$i++)
	{
	$reg_gal_tot=0;
	$reg_prof_tot=0;
	
	$sup_gal_tot=0;
	$sup_prof_tot=0;
	
	$diesel_gal_tot=0;
	$diesel_prof_tot=0;
	
	$plus_gal_tot=0;
	$plus_prof_tot=0;	
		
	foreach($nwrubyutr as $nwrubyutr_row)	
	{	
	if($nwrubyutr_row['product']=='Regular' && $nwrubyutr_row['date']==$nwdate_array[$i]){
	$reg_gal_tot=$reg_gal_tot+$nwrubyutr_row['gal'];
	$reg_prof_tot=$reg_prof_tot+$nwrubyutr_row['prof'];
	}
	
	if($nwrubyutr_row['product']=='Premium' && $nwrubyutr_row['date']==$nwdate_array[$i]){
	$sup_gal_tot=$sup_gal_tot+$nwrubyutr_row['gal'];
	$sup_prof_tot=$sup_prof_tot+$nwrubyutr_row['prof'];
	}
	if($nwrubyutr_row['product']=='Diesel' && $nwrubyutr_row['date']==$nwdate_array[$i]){
	$diesel_gal_tot=$diesel_gal_tot+$nwrubyutr_row['gal'];
	$diesel_prof_tot=$diesel_prof_tot+$nwrubyutr_row['prof'];
	}
	if($nwrubyutr_row['product']=='Plus' && $nwrubyutr_row['date']==$nwdate_array[$i]){
	$plus_gal_tot=$plus_gal_tot+$nwrubyutr_row['gal'];
	$plus_prof_tot=$plus_prof_tot+$nwrubyutr_row['prof'];
	}	
	}	
	$sub_rubyutr=array('date'=>$nwdate_array[$i],'reg_gal_tot'=>$reg_gal_tot,'reg_prof_tot'=>$reg_prof_tot,'plus_gal_tot'=>$plus_gal_tot,'plus_prof_tot'=>$plus_prof_tot,'sup_gal_tot'=>$sup_gal_tot,'sup_prof_tot'=>$sup_prof_tot,'diesel_gal_tot'=>$diesel_gal_tot,'diesel_prof_tot'=>$diesel_prof_tot);
	array_push($cstmz_rubyutr,$sub_rubyutr);	
	}
				
				
		//echo '<pre>';print_r($cstmz_rubyutr);die;		
	$arr = array();		
	$reg_gal_total=0;
	$reg_prof_total=0;
	$plus_gal_total=0;
	$plus_prof_total=0;
	$sup_gal_total=0;
	$sup_prof_total=0;
	$diesel_gal_total=0;
	$diesel_prof_total=0;
	$tot_tot_gallon=0;
	$tot_tot_value=0;
	foreach($cstmz_rubyutr as $cstmz_rubyutr_row)
	{
	/*$reg_gal_total=$reg_gal_total+$cstmz_rubyutr_row['reg_gal_tot'];
	$reg_prof_total=$reg_prof_total+$cstmz_rubyutr_row['reg_prof_tot'];
	$plus_gal_total=$plus_gal_total+$cstmz_rubyutr_row['plus_gal_tot'];
	$plus_prof_total=$plus_prof_total+$cstmz_rubyutr_row['plus_prof_tot'];
	$sup_gal_total=$sup_gal_total+$cstmz_rubyutr_row['sup_gal_tot'];
	$sup_prof_total=$sup_prof_total+$cstmz_rubyutr_row['sup_prof_tot'];
	$diesel_gal_total=$diesel_gal_total+$cstmz_rubyutr_row['diesel_gal_tot'];
	$diesel_prof_total=$diesel_prof_total+$cstmz_rubyutr_row['diesel_prof_tot'];*/
	$total_gallon=$cstmz_rubyutr_row['reg_gal_tot']+ $cstmz_rubyutr_row['plus_gal_tot']+$cstmz_rubyutr_row['sup_gal_tot']+$cstmz_rubyutr_row['diesel_gal_tot'];
	$total_value=$cstmz_rubyutr_row['reg_prof_tot']+$cstmz_rubyutr_row['plus_prof_tot']+$cstmz_rubyutr_row['sup_prof_tot']+$cstmz_rubyutr_row['diesel_prof_tot'];
	/*$tot_tot_gallon=$tot_tot_gallon+$total_gallon;
	$tot_tot_value=$tot_tot_value+$total_value;	*/
	
	$res1['date']=$cstmz_rubyutr_row['date'];
	$res1['reg_gal_tot']=number_format((float)($cstmz_rubyutr_row['reg_gal_tot']), 2, '.', '');
	$res1['reg_prof_tot']=number_format((float)($cstmz_rubyutr_row['reg_prof_tot']), 2, '.', '');
	$res1['plus_gal_tot']=number_format((float)($cstmz_rubyutr_row['plus_gal_tot']), 2, '.', '');
	$res1['plus_prof_tot']=number_format((float)($cstmz_rubyutr_row['plus_prof_tot']), 2, '.', '');
	$res1['sup_gal_tot']=number_format((float)($cstmz_rubyutr_row['sup_gal_tot']), 2, '.', '');
	$res1['sup_prof_tot']=number_format((float)($cstmz_rubyutr_row['sup_prof_tot']), 2, '.', '');
	$res1['diesel_gal_tot']=number_format((float)($cstmz_rubyutr_row['diesel_gal_tot']), 2, '.', '');
	$res1['diesel_prof_tot']=number_format((float)($cstmz_rubyutr_row['diesel_prof_tot']), 2, '.', '');
	$res1['total_gallon']=number_format((float)($total_gallon), 2, '.', '');
	$res1['total_value']=number_format((float)($total_value), 2, '.', '');
	array_push($arr,$res1);		
	}		  
	$fileName = "ruby_utierts_reports".time().".xls";			
	$headerRow = array('Date','Regular Gal Sold','Regular Value','Plus Gal Sold','Plus Value','Super Gal Sold','Super Value','Diesel Gal Sold','Diesel Value','Total Gallon','Total Value');
	$this->ExportXls->export($fileName, $headerRow, $arr);
	}

	public function index() {		
		$this->Render=false;	
		
		
		$date= $this->data['date'];
		$date=explode(' - ',$date);
		$from_date =$date[0];
		$to_date =$date[1];
		//$from_date = $this->data['from_date'];
		//$to_date = $this->data['to_date'];
		
				
		$storeId = $_SESSION['store_id'];		
		$conditions = array('RubyHeader.store_id' => $storeId);	
		if ($from_date) {
			$from_dates = explode('/', $from_date);//14-12-2015
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			$conditions['RubyHeader.ending_date_time >='] = $from_date;
		}		
		if ($to_date)  { 
			$to_dates = explode('/', $to_date);//14-12-2015
			$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
			$conditions['RubyHeader.ending_date_time <=']   = $to_date;
		}		
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			),
			'limit' => 10000
		);		 
		$this->set('rubyUtierts', $this->Paginator->paginate());        
	}

	public function view($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
		$this->set('rubyUtiert', $this->RubyUtiert->find('first', $options));
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->RubyUtiert->create();
			$this->request->data['RubyUtiert']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['RubyUtiert']['created'] = date('Y-m-d');
			
			$from_date = $this->request->data['RubyUtiert']['beginning_date_time'];
			
			$from_dates = explode('-', $from_date);//14-12-2015
			
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			
			$this->request->data['RubyUtiert']['beginning_date_time'] = $from_date;
			
			
			
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
			$this->request->data = $this->RubyUtiert->find('first', $options);
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}


	public function delete($id = null) {
		$this->RubyUtiert->id = $id;
		if (!$this->RubyUtiert->exists()) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUtiert->delete()) {
			$this->Flash->success(__('The ruby utiert has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby utiert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	

	public function admin_view($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
		$this->set('rubyUtiert', $this->RubyUtiert->find('first', $options));
	}

	public function admin_add() {
		$this->loadModel('RubyFprod');
		$this->loadModel('RubyHeader');
			if ($this->request->is('post')) {
			$this->RubyUtiert->create();
			$this->request->data['RubyHeader']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['RubyHeader']['sequence_number'] = $this->request->data['RubyUtiert']['sequence_number'];
			$this->request->data['RubyHeader']['created'] = date('Y-m-d');
			
			$from_date = $this->request->data['RubyUtiert']['beginning_date_time'];
			
			$from_dates = explode('-', $from_date);//14-12-2015
			
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			
			$this->request->data['RubyHeader']['beginning_date_time'] = $from_date;
			$this->request->data['RubyHeader']['ending_date_time'] = date('Y-m-d',strtotime($from_date . "+1 days"));
			
			if ($this->RubyHeader->save($this->request->data)) {
			$id = $this->RubyHeader->getLastInsertId();
			$this->request->data['RubyUtiert']['ruby_header_id']=$id;
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
			}
		}
		$fuelProducts = $this->RubyFprod->find('list',array('conditions'=>array('RubyFprod.store_id'=>$this->Session->read('stores_id')),'fields'=>array('RubyFprod.fuel_product_number','RubyFprod.display_name'),'group'=>'RubyFprod.fuel_product_number'));
		
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		//$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
		
		$conditions = array('RubyHeader.store_id' => $this->Session->read('stores_id'));
		$uproads = $this->RubyUtiert->find('first',array('conditions'=>$conditions,'order'=>array('RubyUtiert.id DESC')));
	//	print_r($uproads);
		$this->set('datevalue',$uproads);
		
	}

	public function admin_edit($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
			$this->request->data = $this->RubyUtiert->find('first', $options);
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

	public function admin_delete($id = null) {
		$this->RubyUtiert->id = $id;
		if (!$this->RubyUtiert->exists()) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUtiert->delete()) {
			$this->Flash->success(__('The ruby utiert has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby utiert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function admin_reset() {	
	
		$redirect_url = array('controller' => 'ruby_utierts', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}
}
