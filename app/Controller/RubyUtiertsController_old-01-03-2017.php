<?php
App::uses('AppController', 'Controller');
/**
 * RubyUtierts Controller
 *
 * @property RubyUtiert $RubyUtiert
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RubyUtiertsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->Render=false;
		
		$from_date = $this->data['from_date'];
		$to_date = $this->data['to_date'];
	
		
		
		
		$storeId = $this->getStoreId();
		
		$conditions = array('RubyHeader.store_id' => $storeId);
		
		
		if ($from_date) {
			$from_dates = explode('-', $from_date);//14-12-2015
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			$conditions['RubyHeader.beginning_date_time >='] = $from_date;
		}
		
		if ($to_date)  { 
			$to_dates = explode('-', $to_date);//14-12-2015
			$to_date = $to_dates[2].'-'.$to_dates[0].'-'.$to_dates[1];
			$conditions['RubyHeader.ending_date_time <=']   = $to_date;
		}	
		
		
		
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			),
			'limit' => 10000
		);
		 
		$this->set('rubyUtierts', $this->Paginator->paginate());
        
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
		$this->set('rubyUtiert', $this->RubyUtiert->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RubyUtiert->create();
			$this->request->data['RubyUtiert']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['RubyUtiert']['created'] = date('Y-m-d');
			
			$from_date = $this->request->data['RubyUtiert']['beginning_date_time'];
			
			$from_dates = explode('-', $from_date);//14-12-2015
			
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			
			$this->request->data['RubyUtiert']['beginning_date_time'] = $from_date;
			
			
			
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
			$this->request->data = $this->RubyUtiert->find('first', $options);
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RubyUtiert->id = $id;
		if (!$this->RubyUtiert->exists()) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUtiert->delete()) {
			$this->Flash->success(__('The ruby utiert has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby utiert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Setredirect();
		$this->RubyUtiert->recursive = 0;
		$storeId = $_SESSION['store_id'];
		
		$this->paginate = array('conditions' => array('RubyHeader.store_id' => $storeId), 
			'order' => array(
				'RubyHeader.ending_date_time' => 'desc'
			)
		);
		$this->set('rubyUtierts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
		$this->set('rubyUtiert', $this->RubyUtiert->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->loadModel('RubyFprod');
		$this->loadModel('RubyHeader');
			if ($this->request->is('post')) {
			$this->RubyUtiert->create();
			$this->request->data['RubyHeader']['store_id'] = $this->Session->read('stores_id');
			$this->request->data['RubyHeader']['sequence_number'] = $this->request->data['RubyUtiert']['sequence_number'];
			$this->request->data['RubyHeader']['created'] = date('Y-m-d');
			
			$from_date = $this->request->data['RubyUtiert']['beginning_date_time'];
			
			$from_dates = explode('-', $from_date);//14-12-2015
			
			$from_date = $from_dates[2].'-'.$from_dates[0].'-'.$from_dates[1];//ymd
			
			$this->request->data['RubyHeader']['beginning_date_time'] = $from_date;
			$this->request->data['RubyHeader']['ending_date_time'] = date('Y-m-d',strtotime($from_date . "+1 days"));
			
			if ($this->RubyHeader->save($this->request->data)) {
			$id = $this->RubyHeader->getLastInsertId();
			$this->request->data['RubyUtiert']['ruby_header_id']=$id;
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
			}
		}
		$fuelProducts = $this->RubyFprod->find('list',array('conditions'=>array('RubyFprod.store_id'=>$this->Session->read('stores_id')),'fields'=>array('RubyFprod.fuel_product_number','RubyFprod.display_name'),'group'=>'RubyFprod.fuel_product_number'));
		
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		//$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
		
		$conditions = array('RubyHeader.store_id' => $this->Session->read('stores_id'));
		$uproads = $this->RubyUtiert->find('first',array('conditions'=>$conditions,'order'=>array('RubyUtiert.id DESC')));
	//	print_r($uproads);
		$this->set('datevalue',$uproads);
		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RubyUtiert->exists($id)) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RubyUtiert->save($this->request->data)) {
				$this->Flash->success(__('The ruby utiert has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The ruby utiert could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RubyUtiert.' . $this->RubyUtiert->primaryKey => $id));
			$this->request->data = $this->RubyUtiert->find('first', $options);
		}
		$rubyHeaders = $this->RubyUtiert->RubyHeader->find('list');
		$fuelProducts = $this->RubyUtiert->FuelProduct->find('list');
		$this->set(compact('rubyHeaders', 'fuelProducts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RubyUtiert->id = $id;
		if (!$this->RubyUtiert->exists()) {
			throw new NotFoundException(__('Invalid ruby utiert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RubyUtiert->delete()) {
			$this->Flash->success(__('The ruby utiert has been deleted.'));
		} else {
			$this->Flash->error(__('The ruby utiert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
