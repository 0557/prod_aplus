<?php

App::uses('AppController', 'Controller');

/**
 * WholesaleProducts Controller
 *
 * @property WholesaleProduct $WholesaleProduct
 * @property PaginatorComponent $Paginator
 */
class RwholesaleProductsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->Setredirect();
        $this->set('rwholesale_products', 'active');
        $this->paginate = array('conditions' => array(/*'RwholesaleProduct.corporation_id' => $_SESSION['corporation_id'],*/ 'RwholesaleProduct.store_id' => $_SESSION['store_id']), "limit" => 5, "order" => "RwholesaleProduct.id DESC");

        $this->set('wholesaleProducts', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->Setredirect();
        $this->set('rwholesale_products', 'active');
        if (!$this->RwholesaleProduct->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        $options = array('conditions' => array('RwholesaleProduct.' . $this->RwholesaleProduct->primaryKey => $id));
        $this->set('wholesaleProduct', $this->RwholesaleProduct->find('first', $options));
    }

    public function admin_add() {
        $this->Setredirect();
        $this->loadmodel('RfuelProduct');
        $this->set('rwholesale_products', 'active');
        if ($this->request->is('post')) {
            $this->RwholesaleProduct->create();
           // $this->request->data['RwholesaleProduct']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RwholesaleProduct']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RwholesaleProduct']['Piping_date'] = (isset($this->data['RwholesaleProduct']['Piping_date']) && $this->data['RwholesaleProduct']['Piping_date'] != '') ? $this->Default->Getdate($this->data['RwholesaleProduct']['Piping_date']) : 0;
            $this->request->data['RwholesaleProduct']['price'] = (isset($this->request->data['RwholesaleProduct']['price']) && $this->request->data['RwholesaleProduct']['price'] != '') ? $this->request->data['RwholesaleProduct']['price'] : 0;
            $this->request->data['RwholesaleProduct']['qty'] = (isset($this->request->data['RwholesaleProduct']['qty']) && $this->request->data['RwholesaleProduct']['qty'] != '') ? $this->request->data['RwholesaleProduct']['qty'] : 0;
            $this->request->data['RwholesaleProduct']['opening_book_amount'] = $this->request->data['RwholesaleProduct']['price'] * $this->request->data['RwholesaleProduct']['qty'];
            if ($this->RwholesaleProduct->save($this->request->data)) {
                $getlast_id = $this->RwholesaleProduct->getLastInsertId();
                $product_save = array();
                $product_save['RfuelProduct']['rwholesale_product_id'] = $getlast_id;
                $product_save['RfuelProduct']['base_product_first'] = $this->data['RfuelProduct']['base_product_first'];
                $product_save['RfuelProduct']['first_per'] = $this->data['RfuelProduct']['first_per'];
                $product_save['RfuelProduct']['base_product_second'] = $this->data['RfuelProduct']['base_product_second'];
                $product_save['RfuelProduct']['second_per'] = $this->data['RfuelProduct']['second_per'];
                $this->RfuelProduct->save($product_save);
                $this->Session->setFlash(__('The fuel wholesale product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fuel wholesale product could not be saved. Please, try again.'));
            }
        }
        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('list');
        $fuelDepartments = $this->RwholesaleProduct->FuelDepartment->find('list');
        $customers = $this->RwholesaleProduct->Customer->find('list', array('conditions' => array("Customer.customer_type" => "Vendor", 'Customer.retail_type' => 'corporation'/*, 'Customer.corporation_id' => $_SESSION['corporation_id']*/)));
        $stores = $this->RwholesaleProduct->Store->find('list'/*, array('conditions' => array('Store.corporation_id' => $_SESSION['corporation_id']))*/);
        $this->set(compact('fuelDepartments', 'customers', 'stores', 'product'));
    }

    public function admin_edit($id = null) {
        $this->Setredirect();
        $this->loadmodel('RfuelProduct');
        $this->set('rwholesale_products', 'active');
        if (!$this->RwholesaleProduct->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        if ($this->request->is(array('post', 'put'))) {
           // $this->request->data['RwholesaleProduct']['corporation_id'] = $_SESSION['corporation_id'];
            $this->request->data['RwholesaleProduct']['store_id'] = $_SESSION['store_id'];
            $this->request->data['RwholesaleProduct']['Piping_date'] = (isset($this->data['RwholesaleProduct']['Piping_date']) && $this->data['RwholesaleProduct']['Piping_date'] != '') ? $this->Default->Getdate($this->data['RwholesaleProduct']['Piping_date']) : 0;
            $this->request->data['RwholesaleProduct']['price'] = (isset($this->request->data['RwholesaleProduct']['price']) && $this->request->data['RwholesaleProduct']['price'] != '') ? $this->request->data['RwholesaleProduct']['price'] : 0;
            $this->request->data['RwholesaleProduct']['qty'] = (isset($this->request->data['RwholesaleProduct']['qty']) && $this->request->data['RwholesaleProduct']['qty'] != '') ? $this->request->data['RwholesaleProduct']['qty'] : 0;
            $this->request->data['RwholesaleProduct']['opening_book_amount'] = $this->request->data['RwholesaleProduct']['price'] * $this->request->data['RwholesaleProduct']['qty'];
            if ($this->RwholesaleProduct->save($this->request->data)) {
                $this->RfuelProduct->deleteAll(array('RfuelProduct.rwholesale_product_id' => $id), false);
                $product_save = array();
                $product_save['RfuelProduct']['rwholesale_product_id'] = $id;
                $this->RfuelProduct->create();

                $product_save['RfuelProduct']['base_product_first'] = $this->data['RfuelProduct']['base_product_first'];
                $product_save['RfuelProduct']['first_per'] = $this->data['RfuelProduct']['first_per'];
                $product_save['RfuelProduct']['base_product_second'] = $this->data['RfuelProduct']['base_product_second'];
                $product_save['RfuelProduct']['second_per'] = $this->data['RfuelProduct']['second_per'];

                $this->RfuelProduct->save($product_save);

                $this->Session->setFlash(__('The  fuel Product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fuel wholesale product could not be saved. Please, try again.'));
            }
        }
        $options = array('conditions' => array('RwholesaleProduct.' . $this->RwholesaleProduct->primaryKey => $id));
        $this->request->data = $this->RwholesaleProduct->find('first', $options);
        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('list');
        $fuelDepartments = $this->RwholesaleProduct->FuelDepartment->find('list');
        $customers = $this->RwholesaleProduct->Customer->find('list', array('conditions' => array("Customer.customer_type" => "Vendor", 'Customer.retail_type' => 'corporation'/*, 'Customer.corporation_id' => $_SESSION['corporation_id']*/)));
        $stores = $this->RwholesaleProduct->Store->find('list'/*, array('conditions' => array('Store.corporation_id' => $_SESSION['corporation_id']))*/);
        $this->set(compact('fuelDepartments', 'customers', 'stores', 'product'));
    }

    public function admin_delete($id = null) {
        $this->RwholesaleProduct->id = $id;
        if (!$this->RwholesaleProduct->exists()) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        if ($this->RwholesaleProduct->delete()) {
            $this->Session->setFlash(__('The wholesale product has been deleted.'));
        } else {
            $this->Session->setFlash(__('The wholesale product could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
