<?php
ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class SalesInvoicesController extends AppController {

  //    public $components = array('Paginator', 'Mpdf.Mpdf');
    public $components = array('Paginator','RequestHandler');

    public function admin_index() {
		

        $this->set('sales_invoice', 'active');
		$this->Paginator->settings = array(				
				'limit' => 10,
				"order" => "SalesInvoice.id DESC"
			);		 
		$this->set('salesInvoices', $this->paginate());
       // echo'<pre>';print_r($sales);exit;
      
    }
	
	public function admin_invoice() {		
        $this->set('sales_invoice', 'active');
		$this->Paginator->settings = array(
				'conditions' => array('SalesInvoice.store_id'=>$this->Session->read('stores_id')),
				'limit' => 10,
				"order" => "SalesInvoice.id DESC"
			);		 
		$this->set('salesInvoices', $this->paginate());
       // echo'<pre>';print_r($sales);exit;
      
    }

    public function admin_view($id = null) {
        $this->set('sales_invoice', 'active');
        if (!$this->SalesInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid sales invoice'));
        }

        $options = array('conditions' => array('SalesInvoice.' . $this->SalesInvoice->primaryKey => $id));
        $this->set('salesInvoice', $this->SalesInvoice->find('first', $options));
        $product_id = $this->SalesInvoice->find('first', array('conditions' => array('SalesInvoice.id' => $id), 'fields' => array('product_id')));
        
        $productall = $this->SalesInvoice->find('first', array('conditions' => array('SalesInvoice.id' => $id))); 
       // echo'<pre>';print_r($productall);exit;

        $product_id_arr = explode(',', $product_id['SalesInvoice']['product_id']);


        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('all', array('conditions' => array('WholesaleProduct.id' => $product_id_arr)));

        $this->set('product', $product);
    }
	
public function view_pdf($id = null) {
    $this->Post->id = $id;
    if (!$this->Post->exists()) {
        throw new NotFoundException(__('Invalid post'));
    }
    // increase memory limit in PHP 
    ini_set('memory_limit', '512M');
    $this->set('post', $this->Post->read(null, $id));
}
    public function admin_add() {

        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('sales_invoice', 'active');
        if ($this->request->is('post')) {

 $this->SalesInvoice->create();
            $this->request->data['SalesInvoice']['invoice_date'] = $this->Default->Getdate($this->data['SalesInvoice']['invoice_date']);
            $this->request->data['SalesInvoice']['ship_date'] = $this->Default->Getdate($this->data['SalesInvoice']['ship_date']);
            $this->request->data['SalesInvoice']['due_date'] = $this->Default->Getdate($this->data['SalesInvoice']['due_date']);

            if (!empty($this->request->data['FuelProduct']['product_id']) && (!empty($this->request->data['FuelProduct']['product_id']))) {
                if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                    $this->request->data['SalesInvoice']['send_email'] = '1';
                }
                $datasource = $this->SalesInvoice->getDataSource();
                try {
                    $datasource->begin();
					
					/*
					$this->SalesInvoice->save($this->request->data);
				$data = 	$this->SalesInvoice->query("SELECT * FROM sales_invoices;");
//print_r($data);      
        //  echo $this->SalesInvoice->id;
  $getlast_id = $this->SalesInvoice->getLastInsertId();
 // echo $getlast_id;
 	exit;*/
				if (!$this->SalesInvoice->save($this->request->data)) {
				throw new Exception();
                 
                            }
							
                    $getlast_id = $this->SalesInvoice->getLastInsertId();

                    $tax_save = array();
                    $tax_save['TaxeZone']['sale_invoice_id'] = $getlast_id;

                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                          
                            if (!$this->TaxeZone->save($tax_save)) {
                         
						 throw new Exception();
						
                            }
								
						
                        }
                    }
					
                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {
                        $product_save = array();
                        $product_save['FuelProduct']['type'] = 'Sale Invoice';
                        $product_save['FuelProduct']['fuel_sale_invoice_id'] = $getlast_id;
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key => $value) {
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key];
                              
                            if (!$this->FuelProduct->save($product_save)) {
                              throw new Exception();
							      }
								   
                        }
                    }
                    if ($datasource->commit()) {
						$fileName = $this->__CreatePdf($this->data, $getlast_id);
                        $this->SalesInvoice->id = $getlast_id;
                        $sales['SalesInvoice']['invoice_file'] = $fileName;

                        if ($this->SalesInvoice->save($sales)) {
							
                            $customer_email = '';
                            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                                if (!empty($this->request->data['SalesInvoice']['customer_id'])) {

                                    $customer_id = $this->request->data['SalesInvoice']['customer_id'];
                                    $customer = $this->Customer->find('first', array('conditions' => array('Customer.id' => $customer_id), 'fields' => array('email'), 'recursive' => 0));
                                    $customer_email = $customer['Customer']['email'];
                                }
                            }
							//echo 'okkkkkkkkk';die;
                            if (!empty($customer_email)) {
                                
                                $this->loadModel('EmailTemplate');
                                $email = $this->EmailTemplate->selectTemplate('send_invoice');
                                
                                $this->loadModel('Setting');
                                //       $toEmail = $this->Setting->field('Setting.value', array('Setting.name' => 'site.contactus_email'));
                                $emailFindReplace = array(
                                    '##SITE_LINK##' => Router::url('/', true),
                                    '##SITE_NAME##' => Configure::read('site.name'),
                                    '##SUPPORT_EMAIL##' => Configure::read('site.contactus_email'),
                                    '##WEBSITE_URL##' => Router::url('/', true),
                                    '##FROM_EMAIL##' => 'info@24phpsupport.com',
                                    '##CONTACT_URL##' => Router::url(array(
                                        'controller' => 'homes',
                                        'action' => 'contact',
                                        'admin' => false
                                            ), true),
                                    '##SITE_LOGO##' => Router::url(array(
                                        'controller' => 'img',
                                        'action' => '/',
                                        'logo-big.png',
                                        'admin' => false
                                            ), true),
                                );

                                if (@file_exists(WWW_ROOT . 'uploads/sale_invoice/' . $fileName)) {
                                    $this->Email->attachments = array(WWW_ROOT . 'uploads/sale_invoice/' . $fileName);
                                }
                                $this->Email->from = 'info@24phpsupport.com';
                                $this->Email->replyTo = ($email['reply_to_email'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to') : $email['reply_to_email'];
                                $this->Email->to = 'sarangpthk@gmail.com';//vishnsingh007@gmail.com
                                $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                                $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                                $this->Email->send(strtr($email['description'], $emailFindReplace));
                            }
                        }
				
                        $this->Session->setFlash(__('The fuel Sales Invoice has been saved.'));
						
                   return $this->redirect(array('action' => 'index'));
                    }
					
                } catch (Exception $e) {
                    $this->Session->setFlash(__('The fuel Sales Invoice could not be saved. Please, try again.'));
                    $datasource->rollback();
                }
            } else {
                $error = 'Select Atleast One Product';
                $this->set('error', $error);
                $this->Session->setFlash(__($error));
            }
        }
        $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
        $wholesale_customer = $this->Customer->find('list', array('conditions' => array('retail_type' => 'wholesale', 'customer_type' => 'Customer')));
        $corporation = $this->SalesInvoice->Corporation->find('list');
     //   $usa_state = $this->State->find('list', array('conditions' => array('country_id' => 1)));
        $this->set(compact('usa_state', 'store', 'product', 'wholesale_customer', 'corporation'));
    }

    public function admin_edit($id = null) {

        $this->loadmodel('WholesaleProduct');
        $this->loadmodel('Customer');
        $this->loadmodel('FuelProduct');
        $this->loadmodel('State');
        $this->loadmodel('TaxeZone');
        $this->set('sales_invoice', 'active');
        if (!$this->SalesInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid fuel Sales invoice'));
        }

        if ($this->request->is(array('post', 'put'))) {


            //  pr($this->data); die;
            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {
                $this->request->data['SalesInvoice']['send_email'] = '1';
            }
            $customer_email = '';
            if (!empty($this->request->data['SalesInvoice']['customer_id'])) {
                $this->request->data['SalesInvoice']['corporation_id'] = '';
                $this->request->data['SalesInvoice']['store_id'] = '';
                $customer_id = $this->request->data['SalesInvoice']['customer_id'];
                $customer = $this->Customer->find('first', array('conditions' => array('Customer.id' => $customer_id), 'fields' => array('email'), 'recursive' => 0));
                $customer_email = $customer['Customer']['email'];
            }
            if (!empty($this->request->data['SalesInvoice']['corporation_id'])) {
                $this->request->data['SalesInvoice']['customer_id'] = '';
            }

            $this->request->data['SalesInvoice']['invoice_date'] = $this->Default->Getdate($this->data['SalesInvoice']['invoice_date']);
            $this->request->data['SalesInvoice']['ship_date'] = $this->Default->Getdate($this->data['SalesInvoice']['ship_date']);
            $this->request->data['SalesInvoice']['due_date'] = $this->Default->Getdate($this->data['SalesInvoice']['due_date']);

            if (!empty($this->request->data['FuelProduct']['product_id']) && (!empty($this->request->data['FuelProduct']['product_id']))) {
                $datasource = $this->SalesInvoice->getDataSource();
                try {
                    $datasource->begin();
                    if (!$this->SalesInvoice->save($this->request->data)) {
                        throw new Exception();
                    }
                    $this->TaxeZone->deleteAll(array('TaxeZone.sale_invoice_id' => $id));
                    $getlast_id = $this->SalesInvoice->getLastInsertId();
                    $tax_save = array();
                    $tax_save['TaxeZone']['sale_invoice_id'] = $id;
                    if (isset($this->data['TaxeZone']['tax_zone']) && !empty($this->data['TaxeZone']['tax_zone'])) {
                        foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $value) {
                            $this->TaxeZone->create();
                            $tax_save['TaxeZone']['tax_zone'] = $value;
                            $tax_save['TaxeZone']['state_id'] = $this->data['TaxeZone']['state_id'][$key];
                            $tax_save['TaxeZone']['tax_decription'] = $this->data['TaxeZone']['tax_decription'][$key];
                            $tax_save['TaxeZone']['tax_on_gallon_qty'] = $this->data['TaxeZone']['tax_on_gallon_qty'][$key];
                            $tax_save['TaxeZone']['rate_on_gallon'] = $this->data['TaxeZone']['rate_on_gallon'][$key];
                            $tax_save['TaxeZone']['tax_net_amount_gallon'] = $this->data['TaxeZone']['tax_net_amount_gallon'][$key];
                            if (!$this->TaxeZone->save($tax_save)) {
                                throw new Exception();
                            }
                        }
                    }

                    if (isset($this->data['FuelProduct']['product_id']) && !empty($this->data['FuelProduct']['product_id'])) {

                        $this->FuelProduct->deleteAll(array('FuelProduct.fuel_sale_invoice_id' => $id), false);
                        $product_save = array();
                        $product_save['FuelProduct']['type'] = 'Sale Invoice';
                        $product_save['FuelProduct']['fuel_sale_invoice_id'] = $id;
                        foreach ($this->request->data['FuelProduct']['product_id'] as $key1 => $value1) {
                            $this->FuelProduct->create();
                            $product_save['FuelProduct']['product_id'] = $value1;
                            $product_save['FuelProduct']['gallons_delivered'] = $this->data['FuelProduct']['gallons_delivered'][$key1];
                            $product_save['FuelProduct']['cost_per_gallon'] = $this->data['FuelProduct']['cost_per_gallon'][$key1];
                            $product_save['FuelProduct']['net_ammount'] = $this->data['FuelProduct']['net_ammount'][$key1];
                            if (!$this->FuelProduct->save($product_save)) {
                                throw new Exception();
                            }
                        }
                    }
                    if ($datasource->commit()) {

                        $uploadPath = WWW_ROOT . 'uploads/sale_invoice/' . $this->data['SalesInvoice']['invoice_file'];
                        @unlink($uploadPath);
                        $fileName = $this->__CreatePdf($this->data, $id);

                        $this->SalesInvoice->id = $id;
                        $sales['SalesInvoice']['invoice_file'] = $fileName;
                        if ($this->SalesInvoice->save($sales)) {
//                            if (!empty($this->request->data['SalesInvoice']['corporation_id']) && !empty($this->request->data['SalesInvoice']['corporation_id'])) {
//                       //         $this->__saveFuelPurchase($this->data, $fileName, $id);
//                            }

                            if (isset($this->data['checkbox']) && !empty($this->data['checkbox']) && ($this->data['checkbox'] == 'on')) {

                                if (!empty($customer_email)) {

                                    $this->loadModel('EmailTemplate');
                                    $email = $this->EmailTemplate->selectTemplate('send_invoice');

                                    $this->loadModel('Setting');
                                    //       $toEmail = $this->Setting->field('Setting.value', array('Setting.name' => 'site.contactus_email'));
                                    $emailFindReplace = array(
                                        '##SITE_LINK##' => Router::url('/', true),
                                        '##SITE_NAME##' => Configure::read('site.name'),
                                        '##SUPPORT_EMAIL##' => Configure::read('site.contactus_email'),
                                        '##WEBSITE_URL##' => Router::url('/', true),
                                        '##FROM_EMAIL##' => 'info@24phpsupport.com',
                                        '##CONTACT_URL##' => Router::url(array(
                                            'controller' => 'homes',
                                            'action' => 'contact',
                                            'admin' => false
                                                ), true),
                                        '##SITE_LOGO##' => Router::url(array(
                                            'controller' => 'img',
                                            'action' => '/',
                                            'logo-big.png',
                                            'admin' => false
                                                ), true),
                                    );

                                    if (@file_exists(WWW_ROOT . 'uploads/sale_invoice/' . $fileName)) {
                                        $this->Email->attachments = array(WWW_ROOT . 'uploads/sale_invoice/' . $fileName);
                                    }
                                    $this->Email->from = 'info@24phpsupport.com';
                                    $this->Email->replyTo = ($email['reply_to_email'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to') : $email['reply_to_email'];
                                    $this->Email->to = $customer_email;
                                    $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                                    $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';

                                    $this->Email->send(strtr($email['description'], $emailFindReplace));
                                }
                            }

                            $this->Session->setFlash(__('The fuel Sales Invoice has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                        }
                    }
                } catch (Exception $e) {
                    $this->Session->setFlash(__('The fuel Sales Invoice could not be saved. Please, try again.'));
                    $datasource->rollback();
                }
            } else {
                $this->Session->setFlash(__('Select Atleast One Product'));
            }
        } else {
            $options = array('conditions' => array('SalesInvoice.' . $this->SalesInvoice->primaryKey => $id));
            $this->request->data = $this->SalesInvoice->find('first', $options);
            //  pr($this->request->data); die;
        }
        $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
        //$product = $this->WholesaleProduct->find('list');
        $wholesale_customer = $this->Customer->find('list', array('conditions' => array('retail_type' => 'wholesale', 'customer_type' => 'Customer')));
        if (isset($this->request->data['SalesInvoice']['corporation_id']) && !empty($this->request->data['SalesInvoice']['corporation_id'])) {
            $store = $this->SalesInvoice->Store->find('list', array('conditions' => array('Store.corporation_id' => $this->request->data['SalesInvoice']['corporation_id'])));
        }

        $corporation = $this->SalesInvoice->Corporation->find('list');
       // $usa_state = $this->State->find('list', array('conditions' => array('country_id' => 1)));
        $this->set(compact('usa_state', 'store', 'product', 'wholesale_customer', 'corporation', 'store'));
    }

    public function admin_delete($id = null) {

        $this->SalesInvoice->id = $id;
        if (!$this->SalesInvoice->exists()) {
            throw new NotFoundException(__('Invalid sales invoice'));
        }

        if ($this->SalesInvoice->delete()) {
            $this->Flash->success(__('The sales invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The sales invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function products($id = null) {
        if (empty($id)) {
            return;
        }

        $this->loadmodel('WholesaleProduct');
        $product = $this->WholesaleProduct->find('list', array('conditions' => array('status' => 'Enable')));
        //$products = $this->WholesaleProduct->find('list');
        $product = $this->WholesaleProduct->find('first', array('conditions' => array('WholesaleProduct.id' => $id)));

        $this->set(compact('products', 'product', 'id'));
    }

    public function admin_filter_store($corporation_id = null) {
        $this->autoRender = false;
        $this->loadModel('Store');
        $store_name = $this->Store->find('list', array('conditions' => array('Store.corporation_id' => $corporation_id)));
        return json_encode($store_name);
    }

    protected function __CreatePdf($data, $invoice_number) {
        $this->autoRender = false;

        App::import('Vendor', 'Mpdf', array('file' => 'Mpdf' . DS . 'mpdf.php'));

        $payable = Configure::read('checkspayable');
        $logo = Configure::read('logo');
        // echo $logo; die;
        //    pr($data); die;



        $middile_description_product = '';
        if (isset($data['FuelProduct']['product_id']) && !empty($data['FuelProduct']['product_id'])) {
            $this->loadModel('WholesaleProduct');
			
            foreach ($data['FuelProduct']['product_id'] as $key => $value) {
                $product = $this->WholesaleProduct->field('name', array('WholesaleProduct.id' => $value));

                $middile_description_product .= '<tr>
                                                    <td style="border-right: 1px solid; width:655px; padding:3px 5px;font-size: 16.5667px;">' . $product . '</td>
                                                    <td style="border-right: 1px solid;width:154px;padding: 3px 5px;font-size: 16.5667px; text-align:right;" >' . $data['FuelProduct']['gallons_delivered'][$key] . '</td>
                                                    <td style="border-right: 1px solid;width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">' . $data['FuelProduct']['cost_per_gallon'][$key] . '</td>
                                                    <td style="border-right: 1px solid; width:154px; padding: 3px 5px;font-size: 16.5667px;text-align:right;">' . $data['FuelProduct']['net_ammount'][$key] . '</td>
                                                </tr>';
            }
        }

        $middile_description_taxes = '';
        if (isset($data['TaxeZone']['tax_zone']) && !empty($data['TaxeZone']['tax_zone'])) {
            foreach ($data['TaxeZone']['tax_zone'] as $key1 => $value1) {

                $middile_description_taxes .= ' <tr >
                                            <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">' . $data['TaxeZone']['tax_decription'][$key1] . '</td>
                                            <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['tax_on_gallon_qty'][$key1] . '</td>
                                            <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['rate_on_gallon'][$key1] . '</td>
                                            <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">' . $data['TaxeZone']['tax_net_amount_gallon'][$key1] . '</td>
                                          </tr>';
            }
        }


        $footer = '<table width="700px" style="border-top:0;" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
       <td style="width:410px;border-right:0px solid;padding: 3px 5px;">&nbsp;</td>
        <td style="border-bottom:1px solid;border-left:1px solid;font-size:16px;padding-left:2px;margin:0;font-weight:bold;">Subtotal</h2></td>
        <td style="border-bottom:1px solid;border-right:1px solid;font-size:16.5667px;padding-left:2px;width:140px;text-align:right;">$' . $data['SalesInvoice']['gross_amount'] . '</td>
      </tr>
      <tr>
       <td style="width:410px;border-right:0px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="border-bottom: 1px solid;border-left:1px solid;font-size:16px;padding-left: 2px;margin:0;font-weight:bold;">Payments/Credits</h2></td>
        <td style="border-bottom: 1px solid;border-right:1px solid;font-size: 16.5667px;padding-left: 2px;width:140px;text-align:right;">$0.00</td>
      </tr>
      <tr>
        <td style="width:410px;border-right:0px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="border-bottom:1px solid;border-left:1px solid;font-size:16px;padding-left:2px;margin:0;font-weight:bold;">Balance Due</h2></td>
        <td style="border-bottom:1px solid;font-size: 16.5667px;border-right:1px solid;padding-left: 2px;width:140px;text-align:right;font-weight:bold;"><strong>$' . $data['SalesInvoice']['gross_amount'] . '</strong></td>
      </tr>
    </tbody>
  </table>';
  
        // PUT YOUR HTML IN A VARIABLE

        $my_html = '<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div style="width:700px; margin:75px auto 0">
  <div>
    <div style=" float:left; width:204px; text-align:center;"> <img style="width:auto; height:100px" src="' . Router::url('/') . 'app/webroot/uploads/settings/' . $logo . '" />
      <p style="font-size:19.9667px;text-align:left;
margin: 0;"></p>
      <p style="font-size: 18px;text-align:left;
margin: 0;">' . Configure::read('address') . '</p>
      <h1 style="font-size:18px;text-align:left;
margin: 15px 0; ">Bill To:</h1>
      <p style="font-size:18px;text-align:left;
margin: 0;">' . $data['SalesInvoice']['bill_to'] . '
   </p>
      <p style="font-size:18px;text-align:left;
margin: 0;">' . $data['SalesInvoice']['billing_address'] . '</p>
      <p style="font-size:18px;text-align:left;
margin: 0;"></p>
    </div>
    <div style="float:right; width:425px">
    <table style="width:425px;">
   <tr>
        <td style="font-size:19px;font-weight:bold;">Invoice Date</td>
        <td style="font-size:19px;font-weight:bold;">Invoice#</td>
        <td rowspan="2" style="font-size:36px;font-weight:bold;">Invoice</td>
        </tr>
    <tr>
        <td style="font-size:16px;">' . date('d/m/Y', strtotime($data['SalesInvoice']['invoice_date'])) . '</td>
        <td style="font-size:16px;">' . $data['SalesInvoice']['incoice_type'] . '</td>
        </tr>   
    </table>

      <div style="border:3px solid #7f7f7f;
border-radius: 3px;
clear: both;
float: right;
overflow: auto;
padding:5px;margin-top:15px;
width: 350px; display:inline;">
<table style="width:350px;">
    <tr>
        <td style="font-size:24px;font-weight:bold;padding:15px;">PLEASE PAY</td>
        <td  style="font-size:24px;padding:15px;">$' . $data['SalesInvoice']['gross_amount'] . '</td>
        </tr>   
    </table>
      </div>
      <h1 style="clear:both;float:right;width:380px;font-size:16px; font-weight:normal;">Make checks payable to:<span style="float: right;
font-size:16px; font-weight:bold;">' . $payable . '</span></h1>
      <div style="clear: both;
float: right;
width:380px;">
        <h1 style="font-size:19.9667px;
margin:0 0 15px;">Ship To:</h1>
        <p style="font-size:19.9667px;
margin: 0;">' . $data['SalesInvoice']['ship_to'] . '<br />
          ' . $data['SalesInvoice']['shiping_address'] . '<br />
          </p>
      </div>
    </div>
  </div>
  <div  style="border-top: 5px dotted gray;
clear: both;
display: inline-block;
margin: 25px 0;
padding-top: 20px;
width: 700px;">
    <div style="float: left; width:191px">
      <h3 style="font-size:16px;
margin: 0;">' . Configure::read('wholesalename') . '</h3>
      <p style="font-size:16px;
margin: 0;">
       
       ' . Configure::read('address') . '</p>
    </div>
    <div style="float: right; width: 461px;">
      <h3 style="font-size: 15px;
font-weight: normal;
margin: 0;
text-align: right;">PLEASE DETACH AND RETURN TOP PORTION WITH PAYMENT</h3>
      <table style="margin:24px 0 0;" width="461px;" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Ship Via</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">BOL #</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Ship Date</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Due Date</td>
          <td style="font-size:16.5667px;
margin-top: 9px;
padding: 8px 0;
text-align: center;">Terms</td>
        </tr>
        <tr>
          <td style="font-size:16.5667px;margin-top: 9px;padding: 8px 0;text-align: center;">' . $data['SalesInvoice']['ship_via'] . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . $data['SalesInvoice']['bol'] . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding: 8px 0;text-align:center;">' . date('d/m/Y', strtotime($data['SalesInvoice']['ship_date'])) . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . date('d/m/Y', strtotime($data['SalesInvoice']['due_date'])) . '</td>
          <td style="font-size:16.5667px;margin-top:9px;padding:8px 0;text-align: center;">' . $data['SalesInvoice']['terms'] . '</td>
        </tr>
      </table>
    </div>
  </div>
  <table cellpadding="0" cellspacing="0" width="700px" style="text-align:left; border:1px solid;">
    <thead>
      <tr>
        <th style="width:655px; border-right: 1px solid;padding: 3px 5px; border-bottom: 1px solid;"><h2 style="font-size:16.5667px;
margin: 0; text-align:center;">Description</h2></th>
        <th style="border-right: 1px solid;text-align:center;font-size:16.5667px;margin:0; border-bottom: 1px solid;text-align:center;">Qty</th>
        <th style="border-right: 1px solid;text-align:center;font-size:16.5667px;margin: 0; border-bottom: 1px solid;text-align:center;">Rate</th>
        <th style="border-bottom: 1px solid;text-align:center;font-size:16.5667px;margin: 0;">Amount</h2></th>
      </tr>
    </thead>
    <tbody>
        ' . $middile_description_product . '
      <tr>
        <td style="width:655px;border-right: 1px solid;padding:2px 6px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding:2px 6px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding:2px 6px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding:2px 6px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      ' . $middile_description_taxes . '
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:655px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;border-right: 1px solid;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
        <td style="width:154px;padding: 3px 5px;font-size: 16.5667px;text-align:right;">&nbsp;</td>
      </tr>
    </tbody>
  </table>

    ' . $footer . '
  <br />
  <br />
  <table style="clear:both; width:355px" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td style=" font-size: 16.5667px; padding-bottom: 10px;"><strong>Billing Inqueries? Call</strong></td>
        <td style=" font-size:16.5667px; padding-bottom: 10px;"><strong>' . Configure::read('phonenumber') . '</strong></td>
      </tr>
      <tr>
        <td style=" font-size: 16.5667px;"><strong>condition:</strong></td>
        <td style=" font-size:16.5667px;"><strong>' . Configure::read('Terms And Condition') . '</strong></td>
      </tr>
    </tbody>
  </table>
</div>
</body></html>';
//    echo $my_html;
        ini_set("pcre.backtrack_limit", "200000000");
        ini_set("pcre.recursion_limit", "200000000");
        ini_set("memory_limit", "18000M");
		
		Configure::write('debug', 0);
        $mpdf = new mPDF();
        $mpdf->WriteHTML($my_html);
        $filename = 'Sale_invoice' . time() . '.pdf';
        $mpdf->Output($filename, 'F');
                 // echo $filename; die;
       
		//$filename = 'test.pdf';
		return $filename;
    }

    protected function __saveFuelPurchase($data, $fielname, $sale_invoice_id) {
        //  Configure::write('debug', 2);
        $this->loadModel('PurchaseInvoice');

        //    echo "<pre>";  print_r($this->data); echo $fielname ."<br>"; echo $sale_invoice_id; 
        $this->PurchaseInvoice->validator()->remove('comments', 'notBlank');
        $this->PurchaseInvoice->validator()->remove('files', 'upload-file');

        $purchase_invoice = array();
        $purchase_invoice['PurchaseInvoice']['corporation_id'] = $data['SalesInvoice']['corporation_id'];
        $purchase_invoice['PurchaseInvoice']['sale_invoice_id'] = $sale_invoice_id;
        $purchase_invoice['PurchaseInvoice']['store_id'] = $data['SalesInvoice']['store_id'];
        $purchase_invoice['PurchaseInvoice']['mop'] = $data['SalesInvoice']['mop'];
        $purchase_invoice['PurchaseInvoice']['gross_amount'] = $data['SalesInvoice']['gross_amount'];
        $purchase_invoice['PurchaseInvoice']['taxes'] = $data['SalesInvoice']['taxes'];
        $purchase_invoice['PurchaseInvoice']['max_qnty'] = $data['SalesInvoice']['max_qnty'];
        $purchase_invoice['PurchaseInvoice']['total_invoice'] = $data['SalesInvoice']['total_invoice'];
        $purchase_invoice['PurchaseInvoice']['bol'] = $data['SalesInvoice']['bol'];
        $purchase_invoice['PurchaseInvoice']['load_date'] = $data['SalesInvoice']['ship_date'];
        $purchase_invoice['PurchaseInvoice']['receving_date'] = date('Y-m-d H:m:i');
        $purchase_invoice['PurchaseInvoice']['po'] = $data['SalesInvoice']['po'];
        $purchase_invoice['PurchaseInvoice']['status'] = $data['SalesInvoice']['status'];
        $purchase_invoice['PurchaseInvoice']['files'] = $fielname;

        $purchase_invoice_id = $this->PurchaseInvoice->field('PurchaseInvoice.id', array('PurchaseInvoice.sale_invoice_id' => $sale_invoice_id));
        if (isset($purchase_invoice_id) && !empty($purchase_invoice_id)) {
            $purchase_invoice['PurchaseInvoice']['id'] = $purchase_invoice_id;
        }
        if ($this->PurchaseInvoice->save($purchase_invoice)) {
            $movefrompath = WWW_ROOT . 'uploads/sale_invoice/' . $fielname;
            $movetopath = WWW_ROOT . 'uploads/purchaseinvoice/' . $fielname;
            copy($movefrompath, $movetopath);
        }
        return 0;
    }

    public function admin_taxzone_state() {

        $tax_zone = strtolower($this->data['tax_zone']);

        $this->autoRender = false;
        $this->loadModel('WholesaleTax');
        $tax_states = $this->WholesaleTax->find('all', array("conditions" => array("WholesaleTax.tax_zone" => $tax_zone), 'fields' => array('DISTINCT State.name', 'State.id')));
        $taxstates = array();
        $i = 0;
        foreach ($tax_states as $value) {
            $taxstates[$i]['name'] = $value['State']['name'];
            $taxstates[$i]['id'] = $value['State']['id'];
            $i++;
        }

        return json_encode($taxstates);
    }

    public function admin_tax_description($state_id, $tax_zone) {

        $this->autoRender = false;
        $this->loadModel('WholesaleTax');
        $item = array();
        $item = $this->WholesaleTax->find('list', array('conditions' => array('WholesaleTax.state_id' => $state_id, 'WholesaleTax.tax_zone' => $tax_zone), 'fields' => array('WholesaleTax.id', 'WholesaleTax.description')));
      //  pr($item); die;
        echo json_encode($item);
    }
    
    public function admin_tax_rate() {

        $this->autoRender = false;
        $this->loadModel('WholesaleTax');
        $description = $this->data['tax_desctiption'];
        $rate = '';
        $item = $this->WholesaleTax->find('first', array('conditions' => array('WholesaleTax.description' => $description), 'fields' => array('WholesaleTax.rate')));
        $rate = $item['WholesaleTax']['rate'];
        echo json_encode($rate);
    }

}
