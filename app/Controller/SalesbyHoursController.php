<?php
App::uses('AppController', 'Controller');
 
 
class SalesbyHoursController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session');
	
	
	public function admin_index() {
		
		$this->Setredirect();
		$this->loadModel('SalesbyHour');		
		$this->loadModel('RubyHeader');		
		$ruby_header_data= $this->RubyHeader->find('all', array(
		'conditions' => array('store_id' =>$this->Session->read('stores_id')), 
		'fields' => array('MAX(RubyHeader.ending_date_time) AS ending_date_time')));
		//pr($ruby_header_data);die;
		$end_date=$ruby_header_data[0][0]['ending_date_time'];			
		$conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));


			if($this->request->is('post')){			
			    //pr($this->request->data); die;		
				if($this->request->data['end_date'] != "") {					   	
						$this->Session->write('PostSearch', 'PostSearch');
						$this->Session->write('end_date',$this->request->data['end_date']);	
				} else {
						$this->Session->delete('PostSearch');
						$this->Session->delete('end_date');
				}		
			}
			if ($this->Session->check('PostSearch')) {
				$PostSearch = $this->Session->read('PostSearch');
				$end_date = $this->Session->read('end_date');				
			}
			
			if(isset($end_date) && $end_date!=''){
				$conditions[] = array(
					'RubyHeader.ending_date_time' => $end_date,
				);				
				//pr($conditions);die;
			}	
		$this->set('end_date', $end_date);

		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'SalesbyHour.hours' => 'desc'
			)
		);
		//pr($this->Paginator->paginate()); die;
		$this->set('SalesbyHours', $this->Paginator->paginate());
		
					
$total = $this->SalesbyHour->find('all', array('fields' => array('sum(SalesbyHour.hours)   AS hours','sum(SalesbyHour.number_of_fuel_only_customers)   AS number_of_fuel_only_customers','sum(SalesbyHour.number_fuel_and_merchandise)   AS number_fuel_and_merchandise','sum(SalesbyHour.number_merchandise_customers)   AS number_merchandise_customers','sum(SalesbyHour.number_of_items_this_hour)   AS number_of_items_this_hour',
'sum(SalesbyHour.value_of_fuel_only_sales)   AS value_of_fuel_only_sales','sum(SalesbyHour.value_of_fuel_merchandise_sales)   AS value_of_fuel_merchandise_sales',
'sum(SalesbyHour.value_of_merchandise_sales)   AS value_of_merchandise_sales'), 'conditions'=>$conditions));
	//pr($total); die;
		$this->set('total', $total);
	}
	
	
	
	
	
	
	
	
	
	
   /* public function admin_index() {
		$this->Setredirect();
		$this->loadModel('SalesbyHour');		
		$this->loadModel('RubyHeader');		
		$ruby_header_data= $this->RubyHeader->find('all', array(
    'conditions' => array('store_id' =>$this->Session->read('stores_id')), 
    'fields' => array('MAX(RubyHeader.ending_date_time) AS ending_date_time')));
		//pr($ruby_header_data);die;
		    $end_date=$ruby_header_data[0][0]['ending_date_time'];			
		    $conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));	
			if($this->request->is('post')){			
			    //pr($this->request->data); die;		
				if($this->request->data['end_date'] != "") {					   	
						$this->Session->write('PostSearch', 'PostSearch');
						$this->Session->write('end_date',$this->request->data['end_date']);	
				} else {
						$this->Session->delete('PostSearch');
						$this->Session->delete('end_date');
				}		
			}
			if ($this->Session->check('PostSearch')) {
				$PostSearch = $this->Session->read('PostSearch');
				$end_date = $this->Session->read('end_date');				
			}
			
			if(isset($end_date) && $end_date!=''){
				$conditions[] = array(
					'RubyHeader.ending_date_time' => $end_date,
				);				
				//pr($conditions);die;
			}	
		$this->set('end_date', $end_date);
		$this->SalesbyHour->virtualFields['number_of_fuel_only_customers'] = 'SUM(SalesbyHour.number_of_fuel_only_customers)';
		$this->SalesbyHour->virtualFields['number_fuel_and_merchandise'] = 'SUM(SalesbyHour.number_fuel_and_merchandise)';
		$this->SalesbyHour->virtualFields['number_merchandise_customers'] = 'SUM(SalesbyHour.number_merchandise_customers)';
		$this->SalesbyHour->virtualFields['number_of_items_this_hour'] = 'SUM(SalesbyHour.number_of_items_this_hour)';		
		$this->SalesbyHour->virtualFields['value_of_fuel_only_sales'] = 'SUM(SalesbyHour.value_of_fuel_only_sales)';
		$this->SalesbyHour->virtualFields['value_of_fuel_merchandise_sales'] = 'SUM(SalesbyHour.value_of_fuel_merchandise_sales)';
		$this->SalesbyHour->virtualFields['value_of_merchandise_sales'] = 'SUM(SalesbyHour.value_of_merchandise_sales)';		
		$SalesbyHour = $this->SalesbyHour->find('all',array('conditions' => $conditions));
		//pr($RevenueSales); die;	
        $this->set('SalesbyHour', $SalesbyHour);
		
    }*/
	
	
	public function admin_reset() {
	
		$this->Session->delete('PostSearch');
		$this->Session->delete('end_date');
		$redirect_url = array('controller' => 'salesby_hours', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}

	
	
	
	
	
	

	
	
	
	
	
}
