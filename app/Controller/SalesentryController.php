<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class SalesentryController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
		  $this->loadModel('RGroceryItem');
           $this->Setredirect();
        $this->RGroceryItem->recursive = 0;
		
		$rgroceryitems  = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.company_id'=>$this->Session->read('Auth.User.company_id'),'RGroceryItem.store_id' => $this->Session->read('stores_id')), "order" => "RGroceryItem.id DESC"));
		if($rgroceryitems['RGroceryItem']['updated_at']){
			$rGroceryItemsda = date("m-d-Y", strtotime($rgroceryitems['RGroceryItem']['updated_at']));
	        $this->set('rGroceryItems', $rGroceryItemsda);
		}else{
		$this->set('rGroceryItems', '');
		}
		
	/*	   $rGroceryDepartments = $this->RGroceryItem->find('list',array('joins' => array(
    array(
        'table' => 'r_grocery_departments',
        'alias' => 'RGroceryDepartment',
        'type' => 'inner',
        'foreignKey' => false,
        'conditions'=> array('RGroceryDepartment.id = RGroceryItem.r_grocery_department_id')
    )),'conditions'=>array('RGroceryItem.company_id'=>$this->Session->read('Auth.User.company_id'),'RGroceryItem.store_id'=>$this->Session->read('stores_id')),'group'=>'r_grocery_department_id', 'fields'=>array('RGroceryItem.r_grocery_department_id','RGroceryDepartment.name')));
	$this->set('departmentlist',$rGroceryDepartments);
	
	*/
/*$rGroceryDepartments=array();
foreach($rGroceryDepartmentslist as $dept){
	$rGroceryDepartments[] = array($dept['RGroceryItem']['r_grocery_department_id'],$dept['RGroceryDepartment']['name']);
}*/

	
	 if ($this->request->is('post')) {

         //'RGroceryItem.r_grocery_department_id'=>$this->request->data['RGroceryItem']['department'],
			$findid = $this->RGroceryItem->find('first',array('conditions'=>array('RGroceryItem.plu_no'=>$this->request->data['RGroceryItem']['plu'],'RGroceryItem.description'=>$this->request->data['RGroceryItem']['desc'],'RGroceryItem.store_id'=>$this->Session->read('stores_id'),'RGroceryItem.company_id'=>$this->Session->read('Auth.User.company_id'))));
            
			if($this->request->data['RGroceryItem']['updated_at']){
				$newdate = explode('-',$this->request->data['RGroceryItem']['updated_at']);
				$this->request->data['RGroceryItem']['updated_at'] = $newdate[2].'-'.$newdate[0].'-'.$newdate[1];
			}
			
            if(!empty($findid)){
				$this->request->data['RGroceryItem']['id'] = $findid['RGroceryItem']['id'];
				$this->request->data['RGroceryItem']['purchase'] = $findid['RGroceryItem']['purchase'];
				$this->request->data['RGroceryItem']['sales_inventory'] = $this->request->data['RGroceryItem']['sales_inventory'] + $findid['RGroceryItem']['sales_inventory'];
				 $this->RGroceryItem->create();
				// print_r($this->request->data);exit;
						if ($this->RGroceryItem->save($this->request->data)) {
							  $this->Session->setFlash(__('Data has been saved.'));
							return $this->redirect(array('action' => 'index'));
					}
			}else{
				  $this->Session->setFlash(__('Please Enter correct PLU No.'));
				  
							return $this->redirect(array('action' => 'index'));
			}
            
          
           
	 }
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
          $this->Setredirect();
        if (!$this->RGroceryItem->exists($id)) {
            throw new NotFoundException(__('Invalid r grocery item'));
        }
        $options = array('conditions' => array('RGroceryItem.' . $this->RGroceryItem->primaryKey => $id));
        $this->set('rGroceryItem', $this->RGroceryItem->find('first', $options));
    }

	public function admin_getitems(){
		
	
		  $this->Setredirect();
        $this->RGroceryItem->recursive = 0;
		
       
			$this->paginate = array('conditions' => array_filter(array('RGroceryItem.company_id'=>$this->Session->read('Auth.User.company_id'),'RGroceryItem.store_id' => $this->Session->read('stores_id'),'r_grocery_department_id'=>$this->request->data['id'],'plu_no'=>$this->request->data['pul'])),  "order" => "RGroceryItem.id DESC");
		
        $this->set('rGroceryItems', $this->Paginator->paginate());
		
	}
		public function admin_getbyitems(){
		
	
		  $this->Setredirect();
        $this->RGroceryItem->recursive = 0;
	
       
			$this->paginate = array('conditions' => array_filter(array('RGroceryItem.company_id'=>$this->Session->read('Auth.User.company_id'),'RGroceryItem.store_id' => $this->Session->read('stores_id'),'r_grocery_department_id'=>$this->request->data['id'],'plu_no'=>$this->request->data['pul'])),  "order" => "RGroceryItem.id DESC");
		
        $this->set('rGroceryItems', $this->Paginator->paginate());
		
	}
	
	public function admin_getdesc(){
		$this->loadModel('RGroceryItem');
 $this->autoRender = false; 

	$first = $this->RGroceryItem->find('first',array('conditions' => array('RGroceryItem.plu_no'=>$this->request->data['pul']),  "order" => "RGroceryItem.id DESC"));
	if(!empty($first)){
        echo $first['RGroceryItem']['description'];
		}else{
			exit;
		}
		
	}
	
}
