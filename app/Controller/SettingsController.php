<?php

App::uses('AppController', 'Controller');

/**
 * Settings Controller
 *
 * @property Setting $Setting
 */
class SettingsController extends AppController {

    public $uses = array("Setting", "SitePermission");

    function admin_index() {
      /* if (!$this->SitePermission->CheckPermission($this->Auth->user("role_id"), 'settings', 'is_read')) {
            $this->Session->setFlash(__('You are not authorised to access that location'));
            $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        }*/

        if ($this->request->is('post') || $this->request->is('put')) {
            // Save settings

            foreach ($this->request->data['Setting'] as $id => $value) {
                if ($value['type'] == 'file') {
                    if (isset($value['name']['name']) && $value['name']['name'] != '') {

                        move_uploaded_file($value['name']['tmp_name'], WWW_ROOT . 'uploads/settings/' . $value['name']['name']);
                        $settings['Setting']['id'] = $id;
                        $settings['Setting']['value'] = $value['name']['name'];
                    } else {
                        unset($value['name']);
                    }
                } else {
                    $settings['Setting']['id'] = $id;
                    $settings['Setting']['value'] = $value['name'];
                }
                $this->Setting->save($settings['Setting']);
            }
            $this->Session->setFlash(__('Config settings updated'), 'default', array("class" => "success"));
        }

        $settingcategories = $this->Setting->SettingCategory->find('all', array(
            'conditions' => array("SettingCategory.status" => "1"),
            'order' => array(
                'SettingCategory.id' => 'asc'
            ),
            'recursive' => 0
        ));

        foreach ($settingcategories as $settingcat) {
            $settingsarr = $this->Setting->find('all', array(
                'conditions' => array("Setting.setting_category_id" => $settingcat['SettingCategory']['id'], "Setting.status" => "1"),
                'order' => array(
                    'Setting.order' => 'asc'
                ),
                'recursive' => 0
            ));
            $settings[$settingcat['SettingCategory']['id']] = $settingsarr;
        }

        $this->set(compact('settings', 'settingcategories'));
    }

}
