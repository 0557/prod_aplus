<?php

App::uses('AppController', 'Controller');

/**
 * Corporations Controller
 *
 * @property Corporation $Corporation
 * @property PaginatorComponent $Paginator
 */
class ShiftendsController extends AppController {

    public $components = array('Paginator');

    public function admin_index() {
        $conditions = array();
        if (isset($this->params['named']['product_id']) && $this->params['named']['product_id'] != '') {
            $conditions = array(
                'OR' => array(
                    'Shiftend.product_id LIKE ' => '%' . $this->params['named']['product_id'] . '%',
                )
            );
        }
        if (!empty($this->request->data)) {
            if (isset($this->request->data['Shiftend']['product_id']) && $this->request->data['Shiftend']['product_id'] != '') {
                // $this->params['named'] = array("product_id" => $this->request->data['Shiftend']['product_id']);
                $conditions[] = array(
                    'OR' => array(
                        'Shiftend.product_id LIKE ' => '%' . $this->request->data['Shiftend']['product_id'] . '%',
                    )
                );
            }
            if (isset($this->request->data['Shiftend']['date']) && $this->request->data['Shiftend']['date'] != '') {
                //   $this->params['named'] = array("date" => $this->request->data['Shiftend']['date']);
                $conditions[] = array(
                    'OR' => array(
                        'Shiftend.date' => '' . date('Y-m-d', strtotime($this->request->data['Shiftend']['date'])) . '',
                    )
                );
            }
            if (isset($this->request->data['Shiftend']['shift_id']) && $this->request->data['Shiftend']['shift_id'] != '') {
              //  $this->params['named'] = array("shift" => $this->request->data['Shiftend']['shift_id']);
                $conditions[] = array(
                    'OR' => array(
                        'Shiftend.shift_id' => '' . $this->request->data['Shiftend']['shift_id'] . '',
                    )
                );
            }
        }
        $this->Setredirect();
        $this->set('shiftens', 'active');
        $this->set('title_for_layout', 'Shift End');

        $this->paginate = array('conditions' => $conditions, "limit" => 10, "order" => "Shiftend.id DESC");

        $this->set('shiftend', $this->paginate());
        $this->loadModel('RubyFprod');
        $mixer_product = $this->RubyFprod->find('list',array('conditions'=>array('RubyFprod.store_id'=>$_SESSION['store_id']), 'fields'=>array('fuel_product_number','display_name')));
        $this->set(compact('mixer_product'));
    }

    public function admin_view($id = null) {

        $this->set('shiftens', 'active');
        $this->set('title_for_layout', 'Shift End');

        //$id = base64_decode($id);
        if (!$this->Shiftend->exists($id)) {
            throw new NotFoundException(__('Invalid corporation'));
        }
        $options = array('conditions' => array('Shiftend.' . $this->Shiftend->primaryKey => $id));

        $this->set('shiftend', $this->Shiftend->find('first', $options));
    }

    public function admin_add() {

        $this->set('shiftens', 'active');

        $this->set('title_for_layout', 'Shift End');

        if ($this->request->is('post')) {
            $this->Shiftend->create();
            $this->request->data['Shiftend']['company_id'] = $this->Session->read('Auth.User.company_id');
            $this->request->data['Shiftend']['store_id'] = $this->Session->read('stores_id');
            if(!empty($this->data['Shiftend']['date'])){
            $this->request->data['Shiftend']['date'] = date("Y-m-d", strtotime($this->data['Shiftend']['date']));
            }else{
                $this->request->data['Shiftend']['date'] = date("Y-m-d");
            }
	        if ($this->Shiftend->save($this->request->data)) {
                $this->Session->setFlash(__('The shifted has been saved.'));
                $this->redirect(array('controller' => 'shiftends', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The shiftends could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('RubyFprod');
        $mixer_product = $this->RubyFprod->find('list',array('conditions'=>array('RubyFprod.store_id'=>$_SESSION['store_id']), 'fields'=>array('fuel_product_number','display_name')));

        $this->set(compact('mixer_product'));
    }

    public function admin_edit($id = null) {
        $this->set('shiftens', 'active');

        $this->set('title_for_layout', 'Eidt Shift End');


        if (!$this->Shiftend->exists($id)) {
            throw new NotFoundException(__('Invalid Shiftend'));
        }
        if ($this->request->is(array('post', 'put'))) {
             if(!empty($this->data['Shiftend']['date'])){
            $this->request->data['Shiftend']['date'] = date("Y-m-d", strtotime($this->data['Shiftend']['date']));
            }else{
                $this->request->data['Shiftend']['date'] = date("Y-m-d");
            }
		//	print_r($this->request->data);exit;
            if ($this->Shiftend->save($this->request->data)) {
                $this->Session->setFlash(__('The Shiftend has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Shiftend could not be saved. Please, try again.'));
            }
        }
        $options = array('conditions' => array('Shiftend.' . $this->Shiftend->primaryKey => $id));
        $this->request->data = $this->Shiftend->find('first', $options);
        $this->loadModel('RubyFprod');
        $mixer_product = $this->RubyFprod->find('list',array('conditions'=>array('RubyFprod.store_id'=>$_SESSION['store_id']), 'fields'=>array('fuel_product_number','display_name')));
        $this->set(compact('mixer_product'));
    }

    public function admin_delete($id = null) {

        $this->Shiftend->id = $id;
        if (!$this->Shiftend->exists()) {
            throw new NotFoundException(__('Invalid Shiftend'));
        }
        if ($this->Shiftend->delete()) {
            $this->Session->setFlash(__('The Shiftend has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Shiftend could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
