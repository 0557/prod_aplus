<?php
App::uses('AppController', 'Controller');
/**
 * SitePermissions Controller
 *
 * @property SitePermission $SitePermission
 */
class SitePermissionsController extends AppController {

public $uses=array("SitePermission");

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	 if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'site_permissions','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
            $this->SitePermission->recursive = 0;
		$limit=(isset($this->params['named']['showperpage']))?$this->params['named']['showperpage']:Configure::read('site.admin_paging_limit');
		$conditions=array();		
		if(isset($this->params['named']['keyword']) && $this->params['named']['keyword']!=''){
		  $conditions=array('OR' => array(
						'Role.name LIKE ' => '%'.$this->params['named']['keyword'].'%',
					    'Manager.name LIKE ' => '%'.$this->params['named']['keyword'].'%',
						)
		                           );
		   
		}
		  
		if(!empty($this->request->data)){
		  if(isset($this->request->data['showperpage']) && $this->request->data['showperpage']!=''){
		    $limit=$this->request->data['showperpage'];
		    $this->params['named']=array("showperpage"=>$limit);
		  }
		  if(isset($this->request->data['keyword']) && $this->request->data['keyword']!=''){
		    $this->params['named']=array("keyword"=>$this->request->data['keyword']);
			  $conditions=array('OR' => array(
							  'Role.name LIKE ' => '%'.$this->params['named']['keyword'].'%',
							  'Manager.name LIKE ' => '%'.$this->params['named']['keyword'].'%',
							 )
		                                    );
		  }
		}
		
		
		$this->paginate=array("conditions"=>$conditions,"limit"=>$limit,"order"=>"SitePermission.created DESC");
		
		
		$this->set('sitePermissions', $this->paginate());
		$this->set(compact('limit'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	 if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'site_permissions','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
            if (!$this->SitePermission->exists($id)) {
			throw new NotFoundException(__('Invalid site permission'));
		}
		$options = array('conditions' => array('SitePermission.' . $this->SitePermission->primaryKey => $id));
		$this->set('sitePermission', $this->SitePermission->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	  if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'site_permissions','is_add')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
            if ($this->request->is('post')) {
			$this->SitePermission->create();
			
			
			if ($this->SitePermission->save($this->request->data)) {
				$this->Session->setFlash(__('The site permission has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The site permission could not be saved. Please, try again.'));
			}
		}
		$roles = $this->SitePermission->Role->find('list');
		$managers = $this->SitePermission->Manager->find('list');
		$this->set(compact('roles', 'managers'));
			$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	 if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'site_permissions','is_edit')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
            if (!$this->SitePermission->exists($id)) {
			throw new NotFoundException(__('Invalid site permission'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
		
		
			if ($this->SitePermission->save($this->request->data)) {
				$this->Session->setFlash(__('The site permission has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The site permission could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SitePermission.' . $this->SitePermission->primaryKey => $id));
			$this->request->data = $this->SitePermission->find('first', $options);
		}
		$roles = $this->SitePermission->Role->find('list');
		$managers = $this->SitePermission->Manager->find('list');
		$this->set(compact('roles', 'managers'));
		
			$jsIncludes=array('admin/chosen.jquery.min.js','admin/jquery.toggle.buttons.js','admin/jquery.reveal.js','admin/jquery.validationEngine.js','admin/jquery.validationEngine-en.js');
		$cssIncludes=array('admin/chosen.css','admin/bootstrap-toggle-buttons.css','admin/validationEngine.jquery.css');
		$this->set(compact('jsIncludes','cssIncludes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	  if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),'site_permissions','is_delete')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
            $this->SitePermission->id = $id;
		if (!$this->SitePermission->exists()) {
			throw new NotFoundException(__('Invalid site permission'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->SitePermission->delete()) {
			$this->Session->setFlash(__('Site permission deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Site permission was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	/*
	 * checking permission assigned to all managers  
	*/
	public function admin_manager_permission($user_id=null){
	
	 if(!$this->SitePermission->CheckPermission($this->Auth->user("role_id"),$this->Auth->user('id'),'site_permissions','is_read')) {
              $this->Session->setFlash(__('You are not authorised to access that location'));
	      $this->redirect(array('controller'=>'users','action' => 'dashboard'));
          }
	 
	 if ($this->request->is('post')) {
	
	 
	 	if(isset($this->request->data['Permission']) && count($this->request->data['Permission'])>0){
		
		   $managers_data = $this->SitePermission->Manager->find('list',array("recursive"=>"-1"));
		   
		   $this->SitePermission->deleteAll(array('SitePermission.user_id' => $user_id), false);
		  		   
		   foreach($managers_data as $manager_key=>$manager_val){
		   
		     $is_read=(isset($this->request->data['Permission']['is_read'][$manager_key]) && $this->request->data['Permission']['is_read'][$manager_key]!='')?$this->request->data['Permission']['is_read'][$manager_key]:'';
			 $is_add=(isset($this->request->data['Permission']['is_add'][$manager_key]) && $this->request->data['Permission']['is_add'][$manager_key]!='')?$this->request->data['Permission']['is_add'][$manager_key]:'';
			 $is_edit=(isset($this->request->data['Permission']['is_edit'][$manager_key]) && $this->request->data['Permission']['is_edit'][$manager_key]!='')?$this->request->data['Permission']['is_edit'][$manager_key]:'';
			 $is_delete=(isset($this->request->data['Permission']['is_delete'][$manager_key]) && $this->request->data['Permission']['is_delete'][$manager_key]!='')?$this->request->data['Permission']['is_delete'][$manager_key]:'';
			 
			 if(!empty($is_read) || !empty($is_add) || !empty($is_edit) || !empty($is_delete)) {
			   
				 $permission_data=array();
				 $permission_data['SitePermission']['manager_id']=$manager_key;
				 $permission_data['SitePermission']['user_id']=$user_id;
				 $permission_data['SitePermission']['status']='1';
				 $permission_data['SitePermission']['is_read']=$is_read;
				 $permission_data['SitePermission']['is_add']=$is_add;
				 $permission_data['SitePermission']['is_edit']=$is_edit;
				 $permission_data['SitePermission']['is_delete']=$is_delete;
				
				 $this->SitePermission->create();
				 $this->SitePermission->save($permission_data);
				 
			}
			 
			 
		   }
		 $this->Session->setFlash(__('The site permission has been saved'));  
		 
			
		}
	}
			
	
	$managers = $this->SitePermission->Manager->find('all',array("conditions"=>array("Manager.status"=>"1")));
	
	$this->SitePermission->unbindModel(array('belongsTo' => array('User','Manager')), true);	
	$update_manager_data = $this->SitePermission->find('all',array('conditions'=>array('SitePermission.user_id'=>$user_id)));
	
	$users_permissions=array();
	if(count($update_manager_data)>0){
	  foreach($update_manager_data as $update_managers){
	    $users_permissions[$update_managers['SitePermission']['manager_id']]['is_read']=$update_managers['SitePermission']['is_read'];
		$users_permissions[$update_managers['SitePermission']['manager_id']]['is_add']=$update_managers['SitePermission']['is_add'];
		$users_permissions[$update_managers['SitePermission']['manager_id']]['is_edit']=$update_managers['SitePermission']['is_edit'];
		$users_permissions[$update_managers['SitePermission']['manager_id']]['is_delete']=$update_managers['SitePermission']['is_delete'];
	  }
	}
	
	//pr($users_permissions); exit;
	$this->set(compact('managers','user_id','users_permissions'));
	}
	
}
