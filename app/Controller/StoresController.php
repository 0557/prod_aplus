<?php

App::uses('AppController', 'Controller');

/**
 * Stores Controller
 *
 * @property Store $Store
 * @property PaginatorComponent $Paginator
 */
class StoresController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {

        $this->set('storess', 'active');
        $this->paginate = array("limit" => 10, "order" => "Corporation.id DESC");
        $this->set('stores', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->set('storess', 'active');
        $id = base64_decode($id);

        if (!$this->Store->exists($id)) {
            throw new NotFoundException(__('Invalid store'));
        }
        $options = array('conditions' => array('Store.' . $this->Store->primaryKey => $id));
        $this->set('store', $this->Store->find('first', $options));
    }

    public function admin_add() {
        $this->set('storess', 'active');
        if ($this->request->is('post')) {
            $this->Store->create();
          
		  //print_r($this->request->data['Store']);
			//set company id based on corporation_id
			if (isset($this->request->data['Store']['corporation_id'])) {
				$this->request->data['Store']['company_id'] = $this->Store->Corporation->field('company_id', array('id' => $this->request->data['Store']['corporation_id']));
			}
			
            if ($this->Store->save($this->request->data)) {
                $this->Session->setFlash(__('The store has been saved.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The store could not be saved. Please, try again.'));
            }
        }
        $corporations = $this->Store->Corporation->find('list');
        $countries = $this->Store->Country->find('list', array("conditions" => array("Country.status" => "1"), 'fields' => array('Country.id', 'Country.name')));
        $this->set(compact('corporations', 'countries', 'states', 'cities'));
    }

    public function admin_edit($id = null) {
        $this->set('storess', 'active');
        $id = base64_decode($id);

        if (!$this->Store->exists($id)) {
            throw new NotFoundException(__('Invalid store'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Store->save($this->request->data)) {

                $this->Session->setFlash(__('The store has been saved.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The store could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Store.' . $this->Store->primaryKey => $id));
            $this->request->data = $this->Store->find('first', $options);
        }
        $corporations = $this->Store->Corporation->find('list');
        $countries = $this->Store->Country->find('list', array("conditions" => array("Country.status" => "1"), 'fields' => array('Country.id', 'Country.name')));
        $states = $this->Store->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
        if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
            $cities = $this->Store->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
        }
        if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
            $Zipcode = $this->Store->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code') ));
        }
        $this->set(compact('corporations', 'countries', 'states', 'cities','Zipcode'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->set('store', 'active');
        $id = base64_decode($id);
        $this->Store->id = $id;
        if (!$this->Store->exists()) {
            throw new NotFoundException(__('Invalid store'));
        }
        if ($this->Store->delete()) {
            $this->Session->setFlash(__('The store has been deleted.'));
        } else {
            $this->Session->setFlash(__('The store could not be deleted. Please, try again.'));
        }
        $this->redirect(array('action' => 'index'));
    }

    public function admin_states($country_id = null) {
        $this->autoRender = false;

        $this->loadModel('State');
        $states = $this->State->find('list', array("conditions" => array("State.country_id" => $country_id), 'fields' => array('State.id', 'State.name'), "recursive" => "-1"));
        return json_encode($states);
    }

    public function admin_cities($state_id = null) {
        $this->autoRender = false;
        $this->loadModel('City');
        $cities = $this->City->find('list', array("conditions" => array("City.state_id" => $state_id), 'fields' => array('City.id', 'City.name'), "recursive" => "-1"));
        return json_encode($cities);
    }

    public function admin_zipcode($city = null) {
     
        $this->autoRender = false;
        $this->loadModel('ZipCode');
        $zipCode = $this->ZipCode->find('list', array("conditions" => array("ZipCode.city" => $city), 'fields' => array('ZipCode.id', 'ZipCode.zip_code'), "recursive" => "-1"));
        return json_encode($zipCode);
    }

}
