<?php

App::uses('AppController', 'Controller');

/**
 * RGroceryItems Controller
 *
 * @property RGroceryItem $RGroceryItem
 * @property PaginatorComponent $Paginator
 */
class StoresettingsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
      * admin_index method
     *
     * @return void
     */
    public function admin_index() {
		//error_reporting(0);
		  $this->loadModel('GlobalCategorie');
		 $this->loadModel('Globalupcs');
		 $this->loadModel('RGroceryItem');
		 $this->loadModel('RubyDepartment');
	
		
		$rGroceryDepartments = $this->RubyDepartment->find('list',array('fields' => array('number', 'name'), 'conditions'=>array('RubyDepartment.store_id'=>
		$this->Session->read('stores_id'))));
	//	pr($rGroceryDepartments); die;
		
		$this->set('departmentlist',$rGroceryDepartments);
		
		
		$GlobalCategorie = $this->GlobalCategorie->find('list',array('fields' => array('GlobalCategorie.g_category', 'GlobalCategorie.g_category'), 'group'=>'GlobalCategorie.g_category'));

		$this->set('GlobalCategorielist',$GlobalCategorie);
		
		
	   if ($this->request->is('post')) {
		   if($this->request->data['RGroceryItem']['cat']){
			   
			  $this->set('Globalupcslist', $this->Globalupcs->find('all',array('conditions'=>array('Globalupcs.category'=>$this->request->data['RGroceryItem']['cat']))));
			  
	 		 $this->set('categoryname',$this->request->data['RGroceryItem']['cat']);
		   }
   		}

    }


	public function admin_newitem() {
		$this->autoRender = false;
		$this->response->type('json');
		 $this->loadModel('Globalupcs');
		//pr($_POST); die;
		 if ($this->request->is('post')) {
			 $retArr=array();
			$index= $this->Session->read('index')+1;
			 
			 $data = array(
    			'Globalupcs' => array(
					 'plu_no' => $this->request->data['plu'],
					'category' => $this->request->data['category'],
					'cost' => $this->request->data['price'],
					'starting_inventory' => $this->request->data['current_inventory'],
					'description' => $this->request->data['description']
					
				 )
			);
			//pr($data);die;
			$fine=$this->Globalupcs->save($data);
			if($fine){
				$id=$this->Globalupcs->getInsertID();
				
				$retArr=array(
					'id'=>$id,
					'index'=>$index,
					'plu_no' => $this->request->data['plu'],
					'category' => $this->request->data['category'],
					'cost' => $this->request->data['price'],
					'starting_inventory' => $this->request->data['current_inventory'],
					'description' => $this->request->data['description'],
					'tax' => $this->request->data['tax']
				);
				
				$json = json_encode($retArr);
				$this->response->body($json);
			}
		 }
		}
		

	public function admin_getnewplu() {
	
		$this->autoRender = false;
		$this->response->type('json');
		$this->loadModel('Globalupcs');
		$rowcount = $this->Globalupcs->find('count', array('conditions' => array('Globalupcs.plu_no' => $_POST['pul'])));
		
		if($rowcount){
			$fetch = $this->Globalupcs->find('all', array(
				'conditions' => array('Globalupcs.plu_no' => $_POST['pul']),
				'order' => array('Globalupcs.id DESC'),
				'limit' => 1
			));
		//pr($fetch[0]['RGroceryItem']);
		$json = json_encode($fetch[0]['Globalupcs']);
		}else{
			$json=json_encode(0);
		}
		
		
		//pr($json);
   		$this->response->body($json);
		
	}

		
	public function admin_newplu() {
	
	$this->Session->delete('oldval');
	$this->Session->delete('index');
	$this->Session->delete('ScoreCardCriteria');
	$this->loadModel('GlobalCategorie');
	$GlobalCategorie = $this->GlobalCategorie->find('list',array('fields' => array('GlobalCategorie.g_category', 'GlobalCategorie.g_category'), 'group'=>'GlobalCategorie.g_category'));

	$this->set('GlobalCategorielist',$GlobalCategorie);
	
	$this->layout = '/fancy';
	$this->render('/Fancy/storeplu');
	
	}
	
	public function admin_addplu() {
		    $posttype = $_POST['posttype'];
			$index = $_POST['index'];
			$this->Session->write('index',$index);
			$oval=array();
			$a[$index] = $_POST;
			if($this->Session->read('oldval')) {
				$oval = $this->Session->read('oldval');
			} else {
				if ($posttype=='new') {
					$oval = array();
				} elseif($posttype=='submit') {
					$oval = $a;
				}
			}
			
			if ($posttype=='new') {
				$oval = array_merge($oval, $a);
			} 	
				$this->Session->write('oldval', $oval);
				
				$this->Session->write('ScoreCardCriteria', $this->Session->read('oldval') );
				//pr($this->Session->read('ScoreCardCriteria'));
		}

    public function admin_edit($id=null) {
		 $this->loadModel('Globalupcs');

	  $this->set('Globalupcslist', $this->Globalupcs->find('first',array('conditions'=>array('Globalupcs.id'=>$id))));

	   if ($this->request->is('post')) { 
	   if($this->Globalupcs->save($this->request->data)){
                $this->Session->setFlash(__('The data has been saved.'));
                return $this->redirect(array('action' => 'index'));
		   }
	   }

    }

	 public function admin_import_item() {
					$this->loadmodel('RGroceryItem');
					 $this->loadModel('Globalupcs');
					$this->autoRender = false;
/*						print_r($this->request->data['ids']);
						print_r(@$this->request->data['inventory']);
						print_r(@$this->request->data['cost']);
						print_r(@$this->request->data['taxtype']);
	*/				$size = sizeof($this->request->data['ids']);
					$yes = 0;
			//		print_r($this->request->data['ids']);
					for($i = 0; $i < $size; $i++ ){
$itemid = explode('*',$this->request->data['ids'][$i]);
//print_r($itemid);
							  $Globalupcslist = $this->Globalupcs->find('first',array('conditions'=>array('Globalupcs.id'=>$itemid[0])));
	
	$newid = $itemid[1];
	$findplu = $this->RGroceryItem->find('first',array('conditions'=>array('RGroceryItem.plu_no'=>$Globalupcslist['Globalupcs']['plu_no'],'RGroceryItem.store_id'=>$this->Session->read('stores_id'))));

//echo $newid;
//echo $this->request->data['inventory'][$newid];
//print_r($this->request->data['inventory']);
	//					 exit;
						 if(@$findplu['RGroceryItem']['plu_no']){
							 
							 }else{
								 
						$this->request->data['RGroceryItem']['department_name'] = $this->request->data['dpartname'];		 
						$this->request->data['RGroceryItem']['r_grocery_department_id'] = $this->request->data['dpartid'];		 
						$this->request->data['RGroceryItem']['plu_no'] = $Globalupcslist['Globalupcs']['plu_no'];		 
						$this->request->data['RGroceryItem']['description'] = $Globalupcslist['Globalupcs']['description'];		 
						$this->request->data['RGroceryItem']['price'] = $this->request->data['cost'][$newid];		 
						$this->request->data['RGroceryItem']['current_inventory'] = $this->request->data['inventory'][$newid];
						
						//$Globalupcslist['Globalupcs']['starting_inventory'];
						
						$this->request->data['RGroceryItem']['plu_sold'] = 1;
						$this->request->data['RGroceryItem']['plu_open'] = 0;
						$this->request->data['RGroceryItem']['plu_return'] = 1;
						$this->request->data['RGroceryItem']['import_flag'] = 1;
						$this->request->data['RGroceryItem']['created_at'] = date('Y-m-d');
						$this->request->data['RGroceryItem']['store_id'] = $this->Session->read('stores_id');
						$this->request->data['RGroceryItem']['company_id'] = $this->Session->read('Auth.User.company_id');
						$this->request->data['RGroceryItem']['plu_tax'] = $this->request->data['taxtype'][$newid];
						//		print_r($this->data);exit;
								if($this->RGroceryItem->save($this->data)){
												$yes=1;
													}
		 }

							
					}
					
					if($yes==1){
							echo 'Selected items imported';
							}else{
								echo 'Selected items not imported';
								}

			}

}
