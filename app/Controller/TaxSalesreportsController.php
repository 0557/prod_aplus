<?php
App::uses('AppController', 'Controller');
 
 
class TaxSalesreportsController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session' ,'ExportXls');
	
	
	public function admin_index() {		
		$this->Setredirect();
			
		if($this->Session->read('pos_type')=='ruby2'){	
				$this->loadModel('Ruby2Tax');
				$full_date='';
				$from_date='';
				$to_date='';
				$ruby2_end_date='';
				$ruby2_tax= $this->Ruby2Tax->find('all', array(
				'conditions' => array('store_id' =>$this->Session->read('stores_id')), 
				'fields' => array('MAX(Ruby2Tax.period_end_date) AS period_end_date')));
				//print_r($ruby2_tax[0][0]['period_end_date']);die;
				$ruby2_end_date=$ruby2_tax[0][0]['period_end_date'];
				if($ruby2_end_date!=''){
				$y_date=substr($ruby2_end_date,0,4);
				$m_date=substr($ruby2_end_date,5,2);
				$d_date=substr($ruby2_end_date,8,2);
				$ruby2_modified_end_date=$m_date.'/'.$d_date.'/'.$y_date;		
				//print_r($ruby2_modified_end_date);die;
				$full_date=$ruby2_modified_end_date.' - '.$ruby2_modified_end_date;				
				$this->Session->write('full_date',$full_date);	
				$this->Session->write('from_date',$ruby2_end_date);	
				$this->Session->write('to_date',$ruby2_end_date);		
				}
				
				$ruby2_conditions= array('Ruby2Tax.store_id'=>$this->Session->read('stores_id'), 'Ruby2Tax.tax_sysid'=>'1');
		
		}else{
				$this->loadModel('RubyHeader');
				$this->loadModel('TaxSalesreport');	
				$ruby_header_data= $this->RubyHeader->find('all', array(
				'conditions' => array('store_id' =>$this->Session->read('stores_id')), 
				'fields' => array('MAX(RubyHeader.ending_date_time) AS ending_date_time')));
				//pr($ruby_header_data);die;
				$end_date=$ruby_header_data[0][0]['ending_date_time'];			
				$conditions= array('RubyHeader.store_id' =>$this->Session->read('stores_id'));
		}
		
				
		


			if($this->request->is('post')){			
			    //pr($this->request->data); die;
				if($this->Session->read('pos_type')=='ruby2'){					      
						$date=array(); 
						$formarr=array(); 
						$toarr=array(); 
						if($this->request->data['Ruby2Tax']['full_date']!= "" ) {			
						
						$full_date=$this->request->data['Ruby2Tax']['full_date'];
						if(isset($full_date) && $full_date!=''){
						$date=explode(' - ',$full_date);
						$form_date=$date[0];
						$formarr=explode('/',$form_date);
						$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
						
						$to_date=$date[1];
						$toarr=explode('/',$to_date);
						$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
						}
						$this->Session->write('full_date',$full_date);	
						$this->Session->write('from_date',$fdate);	
						$this->Session->write('to_date',$tdate);				   	
								
						} else {
						$this->Session->delete('from_date');
						$this->Session->delete('from_date');
						$this->Session->delete('to_date');		
						}	
				}else{				
						if($this->request->data['end_date'] != "") {					   	
								$this->Session->write('PostSearch', 'PostSearch');
								$this->Session->write('end_date',$this->request->data['end_date']);	
						} else {
								$this->Session->delete('PostSearch');
								$this->Session->delete('end_date');
						}	
				}
			}
			
			
			
			
				if($this->Session->read('pos_type')=='ruby2'){	
						$full_date=$this->Session->read('full_date');
						$from_date=$this->Session->read('from_date');
						$to_date=$this->Session->read('to_date');			
								
						if(isset($from_date) && $from_date!=''){
							$ruby2_conditions['Ruby2Tax.period_end_date >=']=$from_date;				
						}
						
						if(isset($to_date) && $to_date!=''){
							$ruby2_conditions['Ruby2Tax.period_end_date <=']=$to_date;				
						}	
						
			      }else{
						if ($this->Session->check('PostSearch')) {
						$PostSearch = $this->Session->read('PostSearch');
						$end_date = $this->Session->read('end_date');				
						}
						
						if(isset($end_date) && $end_date!=''){
						$conditions[] = array(
						'RubyHeader.ending_date_time' => $end_date,
						);			
						}
							
			       }
			
			
			
		if($this->Session->read('pos_type')=='ruby2'){	
		
		/*$this->Ruby2Tax = new Ruby2Tax;
		$this->paginate['Ruby2Tax'] = array( 'limit' => 5, 'page' => 1 , 'order' => 'Ruby2Tax.id' );
		$this->set( 'Ruby2Taxs', $this->paginate('Ruby2Tax') );
		echo '<pre>'; print_r($this->paginate('Ruby2Tax')); die;*/	

		
		$this->set('full_date', $full_date);
		$ruby2_tax_all= $this->Ruby2Tax->find('all', array(
		'conditions' =>$ruby2_conditions));		
		$this->set('Ruby2Taxs',$ruby2_tax_all);	
		
		}else{			
		$this->set('end_date', $end_date);
		$this->paginate = array('conditions' => $conditions, 
			'order' => array(
				'TaxSalesreport.id' => 'desc'
			)
		);
		//echo '<pre>'; print_r($this->Paginator->paginate()); //die;
		$this->set('TaxSalesreports', $this->Paginator->paginate());
		}
	}
	
	
	 public function admin_taxsale() {
		 //echo 'okkkkk';die;
		 if ($this->request->is('post')) {
			 $this->loadModel('TaxSalesreport');	
			 //print_r($this->request->data['TaxSalesreport']['end_date']); die;
			 if($this->request->data['TaxSalesreport']['end_date']){				
				 
				  //$TaxSalesreport= $this->TaxSalesreport->find('all', array('fields' => array('TaxSalesreport.tax_table_index', 'TaxSalesreport.total_taxes','TaxSalesreport.total_taxable_sales','TaxSalesreport.total_exempt_taxes','TaxSalesreport.total_tax_exempt_sales'),'limit' =>10));
				 
				 $TaxSalesreport= $this->TaxSalesreport->find('all',array('fields' => array('TaxSalesreport.tax_table_index', 'TaxSalesreport.total_taxes','TaxSalesreport.total_taxable_sales','TaxSalesreport.total_exempt_taxes','TaxSalesreport.total_tax_exempt_sales'),'conditions'=>array('RubyHeader.ending_date_time'=>$this->request->data['TaxSalesreport']['end_date'],'RubyHeader.store_id'=>$this->Session->read('stores_id'))));
				 
								
				 //echo '<pre>';print_r($TaxSalesreport); die;
				 $TaxSalesreportcount=count($TaxSalesreport);
				 if($TaxSalesreportcount>0){
				 	
					/*$this->set('data', $TaxSalesreport);
					$filename=time()."_tax_sales_report.pdf";
					$this->set('filename', $filename);
					$this->layout = '/pdf/default1';
					$this->render('/Pdf/tax_sales_report_pdf_view');*/				
			
			//echo '<pre>'; print_r($TaxSalesreport);die;
			$result=array();
			foreach($TaxSalesreport as $taxsalesreport)
			{				
			array_push($result, $taxsalesreport['TaxSalesreport']);					
			}			
			//echo '<pre>'; print_r($result);die;
			$fileName = "Tax_sales_report_".time().".xls";
			
			$headerRow = array('Tax Table Index','Total Taxes','Total Taxable Sales','Total Exempt Taxes','Total Taxe Exempt Sales');
			$this->ExportXls->export($fileName, $headerRow,$result);			
					
					
					$this->Session->setFlash(__('Excel successfully created.'));
				}else {				 
					$this->Session->setFlash(__('There are no items to create excel.'), 'default', array('class' => 'error'));
				}
				 
			 }
			 
		 } 
		 
		 $this->redirect(array('controller' => 'r_grocery_barcodes', 'action' => 'index'));
		
		 
	 }
	 
	 
	 public function admin_ruby2tax() {
		 //echo 'okkkkk';die;
		 if ($this->request->is('post')) {
			 $this->loadModel('Ruby2Tax');				 
			            //print_r($this->request->data); die;			 
			            $date=array(); 
						$formarr=array(); 
						$toarr=array(); 							
						$full_date=$this->request->data['TaxSalesreport']['full_date'];						
						//echo $full_date;die;
						if(isset($full_date) && $full_date!=''){
						$date=explode(' - ',$full_date);
						$form_date=$date[0];
						$formarr=explode('/',$form_date);
						$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
						
						$to_date=$date[1];
						$toarr=explode('/',$to_date);
						$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
						}
						
						//echo $fdate;	      
						//echo $tdate;die;	
			 
			 //print_r($this->request->data['TaxSalesreport']['full_date']); die;
			 if($this->request->data['TaxSalesreport']['full_date']){				
				 
		$Ruby2Tax= $this->Ruby2Tax->find('all', array('fields' => array('Ruby2Tax.period_end_date', 'Ruby2Tax.taxable_sales','Ruby2Tax.non_taxable_sales','Ruby2Tax.sales_tax','Ruby2Tax.refund_tax','Ruby2Tax.net_tax'),'conditions'=>array('Ruby2Tax.period_end_date >='=>$fdate,'Ruby2Tax.period_end_date <='=>$tdate,'Ruby2Tax.store_id'=>$this->Session->read('stores_id'),'Ruby2Tax.tax_sysid'=>'1')));
		
						
				 //echo '<pre>';print_r($Ruby2Tax); die;
				 $Ruby2Taxcount=count($Ruby2Tax);
				 if($Ruby2Taxcount>0){			 	
			
			//echo '<pre>'; print_r($Ruby2Tax);die;
			$result=array();
			$tot_taxable_sales='';
			$tot_non_taxable_sales='';
			$tot_sales_tax='';
			$tot_refund_tax='';
			$tot_net_tax='';
			foreach($Ruby2Tax as $ruby2tax)
			{		
			$tot_taxable_sales=$tot_taxable_sales+$ruby2tax['Ruby2Tax']['taxable_sales'];
			$tot_non_taxable_sales=$tot_non_taxable_sales+$ruby2tax['Ruby2Tax']['non_taxable_sales'];
			$tot_sales_tax=$tot_sales_tax+$ruby2tax['Ruby2Tax']['sales_tax'];
			$tot_refund_tax=$tot_refund_tax+$ruby2tax['Ruby2Tax']['refund_tax'];
			$tot_net_tax=$tot_net_tax+$ruby2tax['Ruby2Tax']['net_tax'];	
			array_push($result, $ruby2tax['Ruby2Tax']);					
			}	
			$tatal_array=array('Total',$tot_taxable_sales,$tot_non_taxable_sales,$tot_sales_tax,$tot_refund_tax,$tot_net_tax);
			array_push($result,$tatal_array);	
					
			//echo '<pre>'; print_r($result);die;
			$fileName = "Ruby2_tax_report_".time().".xls";			
			$headerRow = array('Date','Taxable Sales','Non Taxable Sales','Sales Tax','Refund Tax','Net Tax');
			$this->ExportXls->export($fileName, $headerRow,$result);	
					
					
					//$this->Session->setFlash(__('Excel successfully created.'));
				}else {				 
					//$this->Session->setFlash(__('There are no items to create excel.'), 'default', array('class' => 'error'));
				}
				 
			 }
			 
		 } 
		 
		 //$this->redirect(array('controller' => 'r_grocery_barcodes', 'action' => 'index'));
		
		 
	 }
	
	
	public function admin_reset() {	
		$this->Session->delete('PostSearch');			
		$this->Session->delete('end_date');
		$redirect_url = array('controller' => 'tax_salesreports', 'action' => 'index')	;
		return $this->redirect( $redirect_url );
		
	}

	
	
	
	
	
	

	
	
	
	
	
}
