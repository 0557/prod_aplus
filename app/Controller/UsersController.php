<?php
error_reporting(0);
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

    var $uses = array("User","Corporation", "SitePermission", "EmailTemplate","RubyHeader","RubyFprod","RubyUprodt");

    public function admin_login() {
		
        //pr($this->request->data);
        $this->layout = 'admin_login';
        $this->User->recursive = 1;
        $this->set('title_for_layout', 'Admin Login');
        if ($this->request->is('post')) {
			
			
			
		$type=$this->request['data']['User']['type'];
		
		if($type=='company') {
		$this->Auth->authenticate['Form'] = array('fields' => array('username' => 'email'), 'scope' => array('User.status' => 1));   
		} 
		
		if ($type=='store') {
		$arr=array();	
		$username = $this->request->data['User']['email'];
		$arr=explode("-",$username);
		$str_id=$arr[1];
		$find_by_storeid = $this->User->find('first', array(
		'conditions' => array('User.store_id' => $str_id, 'User.status' => 1),
		'fields' => array('store_id', 'email')
		)
		);
		
		$store_id = $find_by_storeid['User']['store_id']; 
		$email = $find_by_storeid['User']['email']; 
		
		
		if ($email!="" && $store_id!=0) {
		$this->request->data['User']['email'] = $email;
		}
		
		}
		
		$this->Auth->authenticate['Form'] = array('fields' => array('username' => 'email'), 'scope' => array('User.status' => 1));
			

            
            if ($this->Auth->login()) {                
				//Set company & corporation in user auth
				$this->loadModel('Store');
				$storeId = $this->Session->read('Auth.User.store_id');
				
				if ($storeId > 0) {
					$storeData = $this->Store->findById($storeId);
					$this->Session->write('Auth.User.StoreInfo', $storeData);
					
					//Tmp uses of Session code to maintain previous flow:
					$_SESSION['corporation_id'] =  $storeData['Corporation']['id'];
					$_SESSION['store_id'] 		=  $storeData['Store']['id'];
						$this->Session->write('stores_id', $storeData['Store']['id']);
				}
				
                $this->Session->setFlash(__('Logged in successfully'));
                return $this->redirect(array("action" => "index"));
            } else {
                //pr($this->Auth->authError); die;
                $this->Session->setFlash(__('Invalid Username and Password'));
                $this->Session->setFlash($this->Auth->authError, 'default', array(), 'auth');
                $this->redirect($this->Auth->loginAction);
            }
        } 
    }
    
	public function admin_logout() {
	  header('pragma: no-cache'); 
	  header('Cache-Control: no-cache, must-revalidate'); 
	  $this->response->disableCache();        
	  $this->Session->delete('Auth.User');
	  $this->Session->delete('User');
	  $this->Session->destroy();
	  $this->Cookie->destroy();
	  return $this->redirect($this->Auth->logout());
    }
    
    public function admin_index(){
        $this->set('dash','active');
        $this->set('title_for_layout', 'Admin Dashboard');
		$this->loadModel('RubyHeader');		
		$this->loadModel('PollingDashboard');
		$this->loadModel('RubyDeptotal');
		$this->loadModel('RubyDailyreporting');
		$this->loadModel('RGroceryInvoice');
		$this->loadModel('PurchaseInvoice');
		$this->loadModel('Corporation');
		$store_id=$this->Session->read('stores_id');		
		$company_id=$this->Session->read('Auth.User.company_id');
		
		
		$first_day_this_month= new DateTime('first day of this month');
		$first_day=$first_day_this_month->format('m/d/Y');
		$last_day_this_month= new DateTime('last day of this month');
		$last_day=$last_day_this_month->format('m/d/Y');
		$full_date =$first_day.' - '.$last_day;
		$fdate=$first_day_this_month->format('Y-m-d');
		$tdate=$last_day_this_month->format('Y-m-d');		
		
		
		$gas_conditions= array('RubyDeptotal.department_number' =>9998);
		$merchandise_conditions= array('RubyDeptotal.department_number !=' =>9998);	
		$daily_reporting_conditions= array('RubyDailyreporting.company_id' =>$this->Session->read('Auth.User.company_id'));	
		$grocery_conditions=array('RGroceryInvoice.company_id' => $this->Session->read('Auth.User.company_id'));
		$purchase_conditions=array('PurchaseInvoice.company_id'=>$this->Session->read('Auth.User.company_id'));
		
		
		if($store_id!=''){
		$gas_conditions['RubyDeptotal.store_id']=$store_id;		
		$merchandise_conditions['RubyDeptotal.store_id']=$store_id;	
		$daily_reporting_conditions['RubyDailyreporting.store_id']=$store_id;	
		$grocery_conditions['RGroceryInvoice.store_id']=$store_id;
		$purchase_conditions['PurchaseInvoice.store_id']=$store_id;		
		}
		
				
        if ($this->request->is('post')) {
                //echo '<pre>';print_r($this->request->data);die;       
				$date=array(); 
				$formarr=array(); 
				$toarr=array(); 
				if($this->request->data['User']['full_date']!= "" ) {			
				//echo 'ok';die;
				$full_date=$this->request->data['User']['full_date'];
				if(isset($full_date) && $full_date!=''){
				$date=explode(' - ',$full_date);
				$form_date=$date[0];
				$formarr=explode('/',$form_date);
				$fdate=$formarr[2]."-".$formarr[0]."-".$formarr[1]; 
				
				$to_date=$date[1];
				$toarr=explode('/',$to_date);
				$tdate=$toarr[2]."-".$toarr[0]."-".$toarr[1]; 
				}
			   
			}		

		
	     }
		 
		 
	   $this->set('full_date',$full_date);	

	   $gas_conditions['RubyDeptotal.ending_date_time >=']=$fdate;	
	   $gas_conditions['RubyDeptotal.ending_date_time <=']=$tdate;	

	   $merchandise_conditions['RubyDeptotal.ending_date_time >=']=$fdate;	
	   $merchandise_conditions['RubyDeptotal.ending_date_time <=']=$tdate;	
	   
	   $daily_reporting_conditions['RubyDailyreporting.reporting_date >=']=$fdate;	
	   $daily_reporting_conditions['RubyDailyreporting.reporting_date <=']=$tdate;
	   
	   $grocery_conditions['DATE(RGroceryInvoice.invoice_date) >=']=$fdate;	
	   $grocery_conditions['DATE(RGroceryInvoice.invoice_date) <=']=$tdate;
	   
	   $purchase_conditions['DATE(PurchaseInvoice.receving_date) >=']=$fdate;	
	   $purchase_conditions['DATE(PurchaseInvoice.receving_date) <=']=$tdate;
		 
		 
		
		$this->RubyDeptotal->virtualFields['total_gas_sales'] = 'SUM(RubyDeptotal.net_sales)';
		$gassales = $this->RubyDeptotal->find('all',array('conditions' => $gas_conditions));
		//echo '<pre>';print_r($gassales); die;	
		$total_gas_sales=$gassales[0]['RubyDeptotal']['total_gas_sales'];
		$this->set('total_gas_sales',$total_gas_sales);		
		
		
		$this->RubyDeptotal->virtualFields['total_merchandise_sales'] = 'SUM(RubyDeptotal.net_sales)';
		$merchetsales = $this->RubyDeptotal->find('all',array('conditions' => $merchandise_conditions));
		//echo '<pre>';print_r($merchetsales); die;	
		$total_merchandise_sales=$merchetsales[0]['RubyDeptotal']['total_merchandise_sales'];
		$this->set('total_merchandise_sales', $total_merchandise_sales);	
		
		
		$this->RubyDailyreporting->virtualFields['total_lottery_sales'] = 'SUM(RubyDailyreporting.Scratch_off_sales)';
		$this->RubyDailyreporting->virtualFields['total_atm_amount'] = 'SUM(RubyDailyreporting.Put_In_ATM)';
		$daily_reportings= $this->RubyDailyreporting->find('all',array('conditions' => $daily_reporting_conditions));
		//echo '<pre>';print_r($daily_reportings); die;	
		$daily_reportings=$daily_reportings[0]['RubyDailyreporting'];
		$this->set('daily_reportings', $daily_reportings);	
		
		
		$this->RGroceryInvoice->virtualFields['total_gross_amount'] = 'SUM(RGroceryInvoice.gross_amount)';		
		$grocery_invoices= $this->RGroceryInvoice->find('all',array('conditions' => $grocery_conditions));
		//echo '<pre>';print_r($grocery_invoices); die;	
		$grocery_invoices=$grocery_invoices[0]['RGroceryInvoice'];
		$this->set('grocery_invoices', $grocery_invoices);	
		
		
		
		$this->PurchaseInvoice->virtualFields['total_gross_amount'] = 'SUM(PurchaseInvoice.gross_amount)';		
		$purchase_invoices= $this->PurchaseInvoice->find('all',array('conditions' => $purchase_conditions));
		//echo '<pre>';print_r($purchase_invoices); die;	
		$purchase_invoices=$purchase_invoices[0]['PurchaseInvoice'];
		$this->set('purchase_invoices', $purchase_invoices);	
		
		
		
		/*echo "SELECT stores.* ,ruby_headers.beginning_date_time, ruby_headers.ending_date_time FROM `stores` JOIN `ruby_headers` ON `stores`.id=`ruby_headers`.store_id WHERE `stores`.company_id=".$company_id." group by`stores`.id "; die;*/
		
		$storedata = $this->RubyHeader->query("SELECT stores.* ,ruby_headers.beginning_date_time, ruby_headers.ending_date_time FROM `stores` LEFT JOIN `ruby_headers` ON `stores`.id=`ruby_headers`.store_id WHERE `stores`.company_id=".$company_id." group by`stores`.id ");
	//	pr($storedata); die;
		$this->set('storedata', $storedata);	
		
		
    }
	public function admin_add($id=null) {
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Add User');
		$this->loadModel('Store');
		$this->loadModel('User');
		if($id){
		if($this->User->delete($id)){
			
				$this->Session->setFlash(__('user has been deleted.'));
						return $this->redirect(array('action' => 'add'));
					}
				}
		$stores = $this->Store->find('list');
		$users = $this->User->find('all',array('conditions' => array('company_id' => $this->Session->read('Auth.User.company_id'))));
		$this->set('usersdata', $users);
		$this->set(compact('stores'));
	}
	
	public function admin_new_add() {
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Add User');
		
		$this->loadModel('Store');
		
			$this->loadModel('User');
	
		if ($this->request->is('post')) {
			$storename = $this->Store->find('first',array('conditions' => array('Store.id' => $this->request->data['User']['store_id'])));
			$this->User->unbindValidation('remove',array('first_name','last_name','phone','confirm_password'));
			$user_data=array();
			$user_data['User']['email']=$this->request->data['User']['email'];
			$user_data['User']['company_id']= $this->Session->read('Auth.User.company_id');
			$user_data['User']['password']= $this->request->data['User']['password'];
			$user_data['User']['username']= $this->request->data['User']['username'];
			$user_data['User']['store_id']= $this->request->data['User']['store_id'];
			$user_data['User']['store_name']= $storename['Store']['name'];
			$user_data['User']['role_id']=6;
			$user_data['User']['status']=1;
			$this->User->create();
			$this->User->save($user_data);	
		
			$this->Session->setFlash(__('The company has been saved'));
			return $this->redirect(array('action' => 'add'));
		} else {
			$this->Flash->error(__('The company could not be saved. Please, try again.'));
		}
		
	
		$stores = $this->Store->find('list');
	
		$this->set(compact('stores'));
	}
				
	public function admin_edit($id=null) {
		
		$this->set('companies', 'active');
        $this->set('title_for_layout', 'Add User');
		
		$this->loadModel('Store');
		
			$this->loadModel('User');
	
		if ($this->request->is('post')) {
			//$storename = $this->Store->find('first',array('conditions' => array('Store.id' => $this->request->data['User']['store_id'])));
			$this->User->unbindValidation('remove',array('first_name','last_name','phone','confirm_password'));
			$user_data=array();
			//$user_data['User']['email']=$this->request->data['User']['email'];
			//$user_data['User']['company_id']= $this->Session->read('Auth.User.company_id');
			$user_data['User']['password']= $this->request->data['User']['oldpassword'];
			$user_data['User']['username']= $this->request->data['User']['username'];
			//$user_data['User']['store_id']= $this->request->data['User']['store_id'];
			$user_data['User']['id']= $this->request->data['User']['id'];
			//$user_data['User']['store_name']= $storename['Store']['name'];
			//$user_data['User']['role_id']=6;
			//$user_data['User']['status']=1;
			$this->User->create();
			$this->User->save($user_data);	
	
				$this->Session->setFlash(__('The company has been saved'));
			return $this->redirect(array('action' => 'add'));
		} 
		$userdetails = $this->User->find('first',array('conditions' => array('User.id' => $id)));
		$stores = $this->Store->find('list');
		$this->set(compact('stores'));
			$this->set('userdetails', $userdetails);
			
	}
	
	public function testdata() {
		 $this->layout = 'rest_dafault';
		 $this->set('title_for_layout', 'Test Data');
	}
				
}
