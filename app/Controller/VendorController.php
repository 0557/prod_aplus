<?php

App::uses('AppController', 'Controller');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class VendorController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

      public function admin_index($id=null,$vtype=null) {
		  $this->set('grocery', 'active');
			$this->loadModel('Customer');
        $title_for_layout = 'Vendors';
		if($id=='grocery'){
		$this->set('grocerytab', 'active');
		$type="grocery";
		}
	else{
		$this->set('purchase_invoice', 'active');
		$type="Fuel";
	}
        $this->set('title_for_layout', $title_for_layout);
    
		if($id){
			if($this->Customer->delete($id)){
				 $this->Session->setFlash(__('The Vendor has been deleted.'));
                return $this->redirect(array('action' => 'index/'.$vtype));
			}
		}
        $where = array('conditions' => array('Customer.type'=>$type,'Customer.company_id' => $this->Session->read('Auth.User.company_id'),'Customer.store_id' => $this->Session->read('stores_id')), "order" => "Customer.id DESC");
		
        $this->set('vendor', $this->Customer->find('all',$where));
    }

   

    public function admin_edit($id = null,$type=null) {
		$this->loadModel('Customer');
        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
           if($type=='grocery'){
		$this->set('grocerytab', 'active');
		}
	else{
		$this->set('purchase_invoice', 'active');
		}
	
        if ($this->request->is(array('post', 'put'))) {

            if ($this->request->data['Customer']['retail_type'] == 'wholesale') {
                $this->request->data['Customer']['corporation_id'] = '';
                $this->request->data['Customer']['store_id'] = '';
             
            }
            // pr($this->request->data);
            // die('i mm here');
            if ($this->Customer->save($this->request->data)) {
                $this->Session->setFlash(__('The Vendor has been saved.'));
                return $this->redirect(array('action' => 'index/'.$type));
            } else {
                $this->Session->error(__('The Vendor could not be saved. Please, try again.'));
            }
        }

        $options = array('conditions' => array('Customer.id' => $id));
        $this->request->data = $this->Customer->find('first', $options);
/*
        $countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));
        $states = $this->Customer->State->find('list', array('conditions' => array('State.country_id' => $this->request->data['Country']['id'])));
        if (isset($this->request->data['City']['id']) && !empty($this->request->data['City']['id'])) {
            $cities = $this->Customer->City->find('list', array('conditions' => array('City.state_id' => $this->request->data['State']['id'])));
        }
        if (isset($this->request->data['ZipCode']['city']) && !empty($this->request->data['ZipCode']['city'])) {
            $Zipcode = $this->Customer->ZipCode->find('list', array('conditions' => array('ZipCode.city' => $this->request->data['ZipCode']['city']), 'fields' => array('ZipCode.id', 'ZipCode.zip_code')));
        }*/
    }
    
   

    public function admin_add($id=null) {
		
      $this->loadModel('Customer');
      $this->loadModel('Store');
        $title_for_layout = 'Vendors';
        $this->set('title_for_layout', $title_for_layout);
      if($id=='grocery'){
		$this->set('grocerytab', 'active');
		$type="grocery";
		}
	else{
		$this->set('purchase_invoice', 'active');
		$type="Fuel";
	}
		
			if(!$this->Session->read('stores_id')){ 
				$this->Session->setFlash(__('Please select store.')); 
				return $this->redirect(array('action' => 'index'));

			}else{
				        if ($this->request->is('post')) {
            $this->Customer->create();
			
            $this->request->data['Customer']['customer_type'] = 'Vendor';
            $this->request->data['Customer']['type'] = $type;
			
			$company = $this->Store->find('first', array('conditions' => array('Store.id'=>$this->Session->read('stores_id'))));
			
			
   $this->request->data['Customer']['company_id'] = $company['Store']['company_id'];
   $this->request->data['Customer']['store_id'] = $this->Session->read('stores_id');
   
            if ($this->Customer->save($this->request->data)) {

                $this->Session->setFlash(__('The Vendor has been saved.'));
                return $this->redirect(array('action' => 'index/'.$type));
            } else {
                $this->Session->setFlash(__('The Vendor could not be saved. Please, try again.'));
            }
        }
		
		$countries = $this->Customer->Country->find('list', array("conditions" => array("Country.status" => "1")));

        $corporations = $this->Customer->Corporation->find('list');
        $stores = array();
        $this->set(compact('stores', 'countries', 'corporations'));
			}


    }


}
