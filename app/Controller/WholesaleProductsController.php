<?php

App::uses('AppController', 'Controller');

/**
 * WholesaleProducts Controller
 *
 * @property WholesaleProduct $WholesaleProduct
 * @property PaginatorComponent $Paginator
 */
class WholesaleProductsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->set('wholesale_products', 'active');
        $this->WholesaleProduct->recursive = 0;
        $this->set('wholesaleProducts', $this->Paginator->paginate());
    }

    public function admin_view($id = null) {
        $this->set('wholesale_products', 'active');
        if (!$this->WholesaleProduct->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        $options = array('conditions' => array('WholesaleProduct.' . $this->WholesaleProduct->primaryKey => $id));
        $this->set('wholesaleProduct', $this->WholesaleProduct->find('first', $options));
    }

    public function admin_add() {
        $this->set('wholesale_products', 'active');
        if ($this->request->is('post')) {
            $this->WholesaleProduct->create();

            $this->request->data['WholesaleProduct']['Piping_date'] = $this->Default->Getdate($this->data['WholesaleProduct']['Piping_date']);
            $this->request->data['WholesaleProduct']['price'] = (isset($this->request->data['WholesaleProduct']['price']) && $this->request->data['WholesaleProduct']['price'] != '') ? $this->request->data['WholesaleProduct']['price'] : 0;
            $this->request->data['WholesaleProduct']['qty'] = (isset($this->request->data['WholesaleProduct']['qty']) && $this->request->data['WholesaleProduct']['qty'] != '') ? $this->request->data['WholesaleProduct']['qty'] : 0;
            $this->request->data['WholesaleProduct']['opening_book_amount'] = $this->request->data['WholesaleProduct']['price'] * $this->request->data['WholesaleProduct']['qty'];
			 $this->request->data['WholesaleProduct']['company_id']=$this->Session->read('Auth.User.company_id');

            if ($this->WholesaleProduct->save($this->request->data)) {
                $this->Session->setFlash(__('The wholesale product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The wholesale product could not be saved. Please, try again.'));
            }
        }
        $fuelDepartments = $this->WholesaleProduct->FuelDepartment->find('list');
        $customers = $this->WholesaleProduct->Customer->find('list', array('conditions' => array("Customer.customer_type" => "Vendor", "retail_type" => "wholesale")));
        $this->set(compact('fuelDepartments', 'customers'));
    }

    public function admin_edit($id = null) {
        $this->set('wholesale_products', 'active');
        if (!$this->WholesaleProduct->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['WholesaleProduct']['Piping_date'] = $this->Default->Getdate($this->data['WholesaleProduct']['Piping_date']);
            $this->request->data['WholesaleProduct']['price'] = (isset($this->request->data['WholesaleProduct']['price']) && $this->request->data['WholesaleProduct']['price'] != '') ? $this->request->data['WholesaleProduct']['price'] : 0;
            $this->request->data['WholesaleProduct']['qty'] = (isset($this->request->data['WholesaleProduct']['qty']) && $this->request->data['WholesaleProduct']['qty'] != '') ? $this->request->data['WholesaleProduct']['qty'] : 0;
            $this->request->data['WholesaleProduct']['opening_book_amount'] = $this->request->data['WholesaleProduct']['price'] * $this->request->data['WholesaleProduct']['qty'];

            if ($this->WholesaleProduct->save($this->request->data)) {
                $this->Session->setFlash(__('The wholesale product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The wholesale product could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WholesaleProduct.' . $this->WholesaleProduct->primaryKey => $id));
            $this->request->data = $this->WholesaleProduct->find('first', $options);
        }
        $fuelDepartments = $this->WholesaleProduct->FuelDepartment->find('list');
        $customers = $this->WholesaleProduct->Customer->find('list', array('conditions' => array("Customer.customer_type" => "Vendor", "retail_type" => "wholesale")));
        $this->set(compact('fuelDepartments', 'customers'));
    }

    public function admin_delete($id = null) {
        $this->WholesaleProduct->id = $id;
        if (!$this->WholesaleProduct->exists()) {
            throw new NotFoundException(__('Invalid wholesale product'));
        }
        //$this->request->allowMethod('post', 'delete');
        if ($this->WholesaleProduct->delete()) {
            $this->Session->setFlash(__('The wholesale product has been deleted.'));
        } else {
            $this->Session->setFlash(__('The wholesale product could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
