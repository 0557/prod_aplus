<?php

App::uses('AppController', 'Controller');

/**
 * WholesaleTaxes Controller
 *
 * @property WholesaleTax $WholesaleTax
 * @property PaginatorComponent $Paginator
 */
class WholesaleTaxesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function admin_index() {
        $this->WholesaleTax->recursive = 0;
        $this->set('wholesaleTaxes', $this->Paginator->paginate());
    }

    public function admin_view($id = null) {
        if (!$this->WholesaleTax->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale tax'));
        }
        $options = array('conditions' => array('WholesaleTax.' . $this->WholesaleTax->primaryKey => $id));
        $this->set('wholesaleTax', $this->WholesaleTax->find('first', $options));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->WholesaleTax->create();
            if ($this->WholesaleTax->save($this->request->data)) {
                $this->Flash->success(__('The wholesale tax has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The wholesale tax could not be saved. Please, try again.'));
            }
        }
        $states = $this->WholesaleTax->State->find('list');
        $this->set(compact('states'));
    }

    public function admin_edit($id = null) {
        if (!$this->WholesaleTax->exists($id)) {
            throw new NotFoundException(__('Invalid wholesale tax'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->WholesaleTax->save($this->request->data)) {
                $this->Flash->success(__('The wholesale tax has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The wholesale tax could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WholesaleTax.' . $this->WholesaleTax->primaryKey => $id));
            $this->request->data = $this->WholesaleTax->find('first', $options);
        }
        $states = $this->WholesaleTax->State->find('list');
        $this->set(compact('states'));
    }

    public function admin_delete($id = null) {
        $this->WholesaleTax->id = $id;
        if (!$this->WholesaleTax->exists()) {
            throw new NotFoundException(__('Invalid wholesale tax'));
        }
        // $this->request->allowMethod('post', 'delete');
        if ($this->WholesaleTax->delete()) {
            $this->Flash->success(__('The wholesale tax has been deleted.'));
        } else {
            $this->Flash->error(__('The wholesale tax could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
