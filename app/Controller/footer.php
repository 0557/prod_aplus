<?php
$CI = & get_instance();

$baseurl = $CI->config->item("base_url");
?>


<!--page footer-->
<footer id="main-footer" class="site-footer clearfix">
    <div class="container">
        <div class="row">


            <!--about widget-->
            <div class="col-md-9">
                <section  class="widget animated fadeInLeft">
                    <h3 class="title">About</h3>
                    <div class="textwidget">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                            nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis                            nisl ut aliquip ex ea commodo consequat.<a href="<?php echo $baseurl; ?>index.php/welcome/about" class="read">Read More</a></p>														
                    </div>
                </section>
            </div>


            <!--general services-->
            

            <div class="clearfix visible-sm"></div>

            <!--recent posts-->
            
            <!--subscription form-->
            <div class="col-md-3">
                <section  class="widget animated fadeInLeft">
                    <h3 class="title">Contact US</h3>
                   
                     
                      <p>5-23-25, Main Road' MumBai 230016</p>
                     
                       <p>E-Mail:Info@hospital.com</p>
                       <p>98 48 09 84 80</p>
                    

                </section>
            </div>
        </div>
        <div class="footer-bottom animated fadeInDown clearfix">
            <div class="row">
                <div class="col-sm-9">
                    <p>Copyright&copy; 2016. All Rights Reserved by Hospital</p>
                </div>
				<div class="col-sm-2">
                    <p><a href="<?php echo $baseurl; ?>index.php/welcome/terms" class="terms">Terms and Conditions</a></p>
                </div>
                <!--footer social icons-->
                <div class="col-sm-1 clearfix">
                    <ul class="footer-social-nav">
                        <li><a target="_blank" href="https://twitter.com/info34771997"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/Myspindoctor-438565106335259/?skip_nax_wizard=true"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#top" id="scroll-top"></a>

<script type='text/javascript' id='quick-js'></script>    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery-1.11.1.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/bootstrap.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.swipebox.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.isotope.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.appear.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.ui.core.min.js'></script><script type="text/javascript" src="<?php echo $baseurl; ?>js/moment.min2.js"></script>        <script type="text/javascript" src="<?php echo $baseurl; ?>js/datepicker/daterangepicker.js"></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.ui.datepicker.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.form.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.autosize.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.meanmenu.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery.velocity.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/respond.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/jquery-migrate-1.2.1.min.js'></script>
<script type='text/javascript' src='<?php echo $baseurl ; ?>front/js/custom.js'></script>
<script> $(document).ready(function () {	
	   $( "#date" ).datepicker({ 				
	   dateFormat: 'DD, d MM, yy',		
minDate: 0,	   
	   }).change(function() {				
	   functioncall();						
	   });		
	   });						
	   function getdoctors(elem){		
	   var elems =  elem.value;  $.ajax({
		   url: "<?php echo $baseurl; ?>" + "index.php/admin/getdoctorslist",	
		   type: 'POST',	
		   data: {option: elems},	
		   success: function (data)		{
			   $(".doctorsl").html(data);		
			   }	 
			   });
			   }
function getphonenum(elem){	
var elem = elem.value;		
	  $.ajax({		
	  url: "<?php echo $baseurl; ?>" + "index.php/admin/getphoness",		
	  type: 'POST',	
	  data: {option: elem},	
	  success: function (data)	
	  {	
	  $("#phones").val(data);	
	  getfees(elem);	
	  getdoctorlocation(elem);	
	  }	    
	  });
	  }
	  function getfees(elem){	
	  $.ajax({	
	  url: "<?php echo $baseurl; ?>" + "index.php/admin/getfees",	
	  type: 'POST',	
	  data: {option: elem},
	  success: function (data)		{	
	  $("#experience").val(data);		
	  }	   
	  });
	  }	
	  function getdoctorlocation(elem){	
	  $.ajax({	
	  url: "<?php echo $baseurl; ?>" + "index.php/admin/getdoctorwithlocation",	
	  type: 'POST',			
	  data: {d_id:elem},		
	  success: function (data)				{	
	  
				$(".locations").html(data);		
				}	  
				});		
				
				}		
				function functioncall(){	
				var elem = document.getElementById('date').value;	
				var stop = elem.indexOf(',');		      
				var dayv = elem.substring(0, stop);
				$('#assignday').val(dayv);	
				var location =  $('.locations').val();	
				var bdate =  document.getElementById('date').value;
				var doctorsl = $('.doctorsl').val();	
				if(location==""){		
				alert('please select location');	
				$('#date').val('');	}else{	
				$.ajax({	
				url: "<?php echo $baseurl; ?>" + "index.php/admin/getdayswithlocation",
				type: 'POST',		
				data: {days: dayv,location: location,bdate:bdate,doctorsl:doctorsl},
				success: function (data)		{	
	   		    $(".timeslot").html(data);	
				}	   
				});	}		
				}			
				</script>    
				<script>     
				$(document).ready(function () {		
				var j=1;			              
				$('.xcxc').click(function () {    
                $('#descr').val($('#editor').html());     
				});					
				$('#addmore').click(function(){																var newtext = '<div class="rm'+j+'"><hr/><div class="form-group col-md-6"><label for="middle-name" class="control-label col-md-4">Location </label><div class="col-md-8"><select  id="loca" name="location[]" class="form-control col-md-7 col-xs-12"  placeholder="Location" required="required"><option value="Delhi">Delhi</option><option value="Hyderabad">Hyderabad</option><option value="Bangalore">Bangalore</option><option value="Chennai">Chennai</option></select></div></div><div class="form-group col-md-6"><label for="middle-name" class="control-label col-md-4">Day </label><div class="col-md-8"><input type="text" name="day[]" id="dayvales" value="" placeholder="sunday,monday" class="form-control"/></div></div><div class="form-group col-md-6"><label for="middle-name" class="control-label col-md-4">Timings </label><div class="col-md-8"><div class="col-md-4 col-sm-4 col-xs-12"><input type="text" id="from'+j+'" onchange="validatetime(this.id)" class="form-control col-md-7 col-xs-12" name="from[]" placeholder="00:00:am"  required style="margin-left:-13px; width:74px;"></div><div class="col-md-2 col-sm-2 col-xs-12" style="margin-top:9px;"> To</div><div class="col-md-4 col-sm-4 col-xs-12"><input type="text" id="fromto'+j+'" name="fromto[]" class="form-control col-md-7 col-xs-12"  placeholder="00:00:am"  onchange="validatetime1(this.id)"  required style="width:80px;  margin-left: 19px;"></div></div></div><div class="form-group"><div class="col-md-11" style="text-align:right"><button type="button" class="btn btn-warning addmoreq" id="rm'+j+'" onclick="removelocation(this.id);">Remove</button></div></div></div>';											$('#Locations').append(newtext);								j++;				    					        });																	$("#Treatment, #numbervalid").keypress(function (i) {     if (i.which != 8 && i.which != 0 && (i.which < 48 || i.which > 57)) 	 {        $("#errmsg").html("Digits Only").show().fadeOut("slow");               return false;    }   });			});				function removelocation(id){				$('.'+id).remove();				}								            $(function () {                function initToolbarBootstrapBindings() {                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',                'Times New Roman', 'Verdana'],                        fontTarget = $('[title=Font]').siblings('.dropdown-menu');                    $.each(fonts, function (idx, fontName) {                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));                    });                    $('a[title]').tooltip({                        container: 'body'                    });                    $('.dropdown-menu input').click(function () {                            return false;                        })                        .change(function () {                            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');                        })                        .keydown('esc', function () {                            this.value = '';                            $(this).change();                        });                    $('[data-role=magic-overlay]').each(function () {                        var overlay = $(this),                            target = $(overlay.data('target'));                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());                    });                    if ("onwebkitspeechchange" in document.createElement("input")) {                        var editorOffset = $('#editor').offset();                        $('#voiceBtn').css('position', 'absolute').offset({                            top: editorOffset.top,                            left: editorOffset.left + $('#editor').innerWidth() - 35                        });                    } else {                        $('#voiceBtn').hide();                    }                };                function showErrorAlert(reason, detail) {                    var msg = '';                    if (reason === 'unsupported-file-type') {                        msg = "Unsupported format " + detail;                    } else {                        console.log("error uploading file", reason, detail);                    }                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +                        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#notifications');                };                initToolbarBootstrapBindings();                $('#editor').wysiwyg({                    fileUploadError: showErrorAlert                });                window.prettyPrint && prettyPrint();            });        </script>		 <script type="text/javascript">                        $(document).ready(function () {                            $('#day').daterangepicker({                                singleDatePicker: true, changeMonth: true,									changeYear: true,                                calender_style: "picker_4"                            }, function (start, end, label) {                                console.log(start.toISOString(), end.toISOString(), label);                            });							                            $('#fromv').daterangepicker({                                singleDatePicker: true,								 changeMonth: true,									
				changeYear: true,         
				calender_style: "picker_4" 
				}, function (start, end, label) {                              
				console.log(start.toISOString(), end.toISOString(), label);                          
				});							                                               
				});						function validatetime(id){
					var from = document.getElementById(id).value;
					re = /^(\d{1,2}):(\d{2})([APap][mM])?$/; 
					if(!from.match(re)) {		
					$('#'+id).val("");	
					alert('please follow time format 00:00am/pm');	
					}	    
					var res = from.charAt(3);
					if(res>=6){	$('#'+id).val("");	
					alert('Please Enter Valide From Time (Ex: 10:55am/pm)');
					}	
					var res1  = from.charAt(0)+from.charAt(1);
					var res2 = from.charAt(5)+from.charAt(6);	
					if((res1 > 12) && ((res2=='am')||((res2=='AM')))){
						$('#'+id).val("");	
						alert('Please Enter Valide From Time (Ex: 13:55pm)');		
						}
						}
						function validatetime1(id){
							var from = document.getElementById(id).value; 
							re = /^(\d{1,2}):(\d{2})([APap][mM])?$/;   
							if(!from.match(re)) {		
							$('#'+id).val("");	
							alert('please follow time format 00:00am/pm');	
							}	  	  
				var res = from.charAt(3);
				if(res>=6){	$('#'+id).val("");	
				alert('Please Enter Valide To Time (Ex: 10:55am/pm)');	
				}	
				var res1  = from.charAt(0)+from.charAt(1);
				var res2 = from.charAt(5)+from.charAt(6);
				if((res1 > 12) && ((res2=='am')||((res2=='AM')))){
					$('#'+id).val("");
					
				alert('Please Enter Valide From Time (Ex: 13:55pm)');	
				
				}}function removerecord(id){
						$.ajax({
							url: "<?php echo $baseurl; ?>" + "index.php/admin/deletelocation",		
					type: 'POST',	
					data: {option: id},	
					success: function (data)
					{	
					location.reload();	
					}	  
					});
				}            
				$('#experience').bind('keypress', function (event) {		
				var regex = new RegExp("^[0-9.\b]+$");  
				var key = String.fromCharCode(!event.charCode ? event.which : event.charCode); 
				if (!regex.test(key)) {       
				event.preventDefault();       
				return false;    }	});
				
				<!-- home page find doctor script-->
				function getlocationlistf(elem){
					
					var special = elem.value;
					$.ajax({
						url: "<?php echo $baseurl; ?>" + "index.php/admin/getlocationwithspeciality",	type: 'POST',	
						data: {option: special},	
						success: function (data)
						{	
						$('.findlocations').html(data);
						}	  
						});
				}
				
				function validatesearch(){
					var sname = $('.slocation').val();
					var lname = $('.findlocations').val();
					if(sname==''){
						alert('please select speciality');
						return false;
					}
				}
				</script>
</body>
</html>