<?php
App::uses('AppModel', 'Model');
/**
 * Company Model
 *
 * @property Competitor $Competitor
 * @property Corporation $Corporation
 * @property Customer $Customer
 * @property Rproduct $Rproduct
 * @property Shiftend $Shiftend
 * @property Store $Store
 * @property User $User
 */
class Company extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Name is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'notBlank' => array(
				'rule' => array('email'),
				'message' => 'Please enter valid email!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            'rule1' => array(
                'rule' => array(
                    '_checkemail',
                    'email'
                ),
                'message' => 'Email address is already in use.',
            )
		),
		'password' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Password is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Competitor' => array(
			'className' => 'Competitor',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Corporation' => array(
			'className' => 'Corporation',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Rproduct' => array(
			'className' => 'Rproduct',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Shiftend' => array(
			'className' => 'Shiftend',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'company_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	function _checkemail($field1 = array(), $field2 = null) {

        if (isset($this->data['Company']['user_id']) && ($this->data['Company']['user_id'] != '')) {
            $conditions['User.id <>'] = $this->data[$this->name]['user_id'];
        }
		
        $conditions['User.email'] = $this->data[$this->name][$field2];
		
        $user = $this->User->find('count', array(
            'conditions' => $conditions,
            'recursive' => -1
        ));

        if ($user > 0)
            return false;

        return true;
    }
	
	public function geruseremail($company_id){
	  $user = $this->User->find('first', array(
            'conditions' => array("User.role_id"=>"5","User.company_id"=>$company_id),
            'recursive' => -1
        ));
		return (isset($user['User']['email']) && $user['User']['email']!='')?$user['User']['email']:'';
		
	}
	public function geruserid($company_id){
	  $user = $this->User->find('first', array(
            'conditions' => array("User.role_id"=>"5","User.company_id"=>$company_id),
            'recursive' => -1
        ));
		return (isset($user['User']['id']) && $user['User']['id']!='')?$user['User']['id']:'';
		
	}

}
