<?php
App::uses('AppModel', 'Model');
/**
 * SalesInvoice Model
 *
 * @property Supplier $Supplier
 * @property Product $Product
 */
class ComplianceNotification extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
 public $validate = array(
		'notification_days' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Only Numeric Field is Allowed',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'expiry_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select Expiry Date',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'license_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select License Name',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'repeat_year' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select Repeat Year',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'supplier_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
            'subject' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Subject of Email',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sender_email' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Sender Email Id',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'store_emails' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Store Email Id',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'message' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
	);


}
