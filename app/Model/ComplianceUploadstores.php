<?php
App::uses('AppModel', 'Model');
/**
 * SalesInvoice Model
 *
 * @property Supplier $Supplier
 * @property Product $Product
 */
class ComplianceUploadstores extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
 
public $validate = array(
	
            'license_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select License Name',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	
		'message' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
