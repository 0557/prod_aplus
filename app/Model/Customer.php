<?php

App::uses('AppModel', 'Model');

/**
 * Customer Model
 *
 * @property Store $Store
 * @property State $State
 * @property City $City
 */
class Customer extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
//	    'retail_type' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'This is required!',
//            ),
//        ),
        'retail_type' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This is required!',
            ),
        ),
        
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Name is Required Field',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'address' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Address is Required Field',
//            ),
//        ),
//        'state_id' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Please Select A State',
//            ),
//        ),
//        'city_id' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Please Select A City',
//            ),
//        ),
//        'zip_code' => array(
//            'numeric' => array(
//                'rule' => array('numeric'),
//                'message' => 'Zip Code is must be numeric',
//            ),
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Zip Code is Required Field',
//            ),
//        ),
//        'fax' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Fax is Required Field',
//            ),
//        ),
//        'email' => array(
//            'email' => array(
//                'rule' => array('email'),
//                'message' => 'Enter a Valid Email',
//            ),
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Email is Required Field',
//            ),
//            'isUnique' => array(
//                'rule' => array('isUnique'),
//                'message' => 'Email address already in use',
//				'on' => 'create',
//            )
//        ),
//        'contact_person' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Contact Person is Required Field',
//         
//            ),
//        ),
//        'gl' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'GL is Required Field',
//         
//            ),
//        ),
//        'terms' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Terms are Required Field',
//            ),
//        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
	    'Corporation' => array(
            'className' => 'Corporation',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Store' => array(
            'className' => 'Store',
            'foreignKey' => 'store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ZipCode' => array(
            'className' => 'ZipCode',
            'foreignKey' => 'zip_code',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
