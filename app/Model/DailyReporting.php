<?php
//

App::uses('AppModel', 'Model');
/**
 * SalesInvoice Model
 *
 * @property Supplier $Supplier
 * @property Product $Product
 */
class DailyReporting extends AppModel {
	public $useTable ='ruby_dailyreportings';
}
