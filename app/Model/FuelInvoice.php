<?php
App::uses('AppModel', 'Model');
/**
 * FuelInvoice Model
 *
 */
class FuelInvoice extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'bol' => array(
			'notBlank' => array(
				'rule' => array('notBlank','alphaNumeric'),
				'message' => 'Enter BOL#',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//		
		
		
		'mop' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select Mop',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
      
		
		'po' => array(
			'notBlank' => array(
				'rule' => array('notBlank','alphaNumeric'),
				'message' => 'Enter PO#',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	
//    
	);
	
	public $belongsTo = array(
		
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'supplier_id',
			'conditions' => array('retail_type' => 'wholesale', 'customer_type' => 'Vendor'),
			'fields' => '',
			'order' => ''
		),

	);
        
        public $hasMany = array(
        
		'TaxeZone' => array(
			'className' => 'TaxeZone',
			'foreignKey' => 'fuel_invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'FuelProduct' => array(
			'className' => 'FuelProduct',
			'foreignKey' => 'fuel_purchase_invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		));
        
        function beforeValidate($options = array()) {

        if (isset($this->data[$this->alias]["files"]["name"]) && $this->data[$this->alias]["files"]["name"] == '') {
            unset($this->data[$this->alias]["files"]);
        }
		
    }
	
}
