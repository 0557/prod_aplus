<?php
App::uses('AppModel', 'Model');
/**
 * SalesInvoice Model
 *
 * @property Supplier $Supplier
 * @property Product $Product
 */
class GamePacks extends AppModel {

public $useTable ='game_packs';

}
