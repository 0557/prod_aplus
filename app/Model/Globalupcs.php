<?php

App::uses('AppModel', 'Model');

/**
 * RGroceryItem Model
 *
 * @property RGroceryDepartment $RGroceryDepartment
 */
class Globalupcs extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
      
        'plu_no' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Value is Required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Only AlphaNumeric Value is Allowed.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'description' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'notNumeric' => array(
               'rule' => array('notNumeric'),
               // 'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
                'message' => 'Only AlphaNumeric Value is Allowed.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */


    public function onlypositive($check) {

        $value = array_keys($check);
        $value = $value[0];
        if (!isset($this->data[$this->name][$value])) {
            throw new CakeException(__('Can\'t compare to the non-existing field "%s" of model %s.'));
        }
        if ($this->data[$this->name][$value] < 0) {
            return false;
        }
        return true;
    }
    
    public function notNumeric($check) {

        $value = array_keys($check);
        $value = $value[0];
        if (!isset($this->data[$this->name][$value])) {
            throw new CakeException(__('Can\'t compare to the non-existing field "%s" of model %s.'));
        }
        if (is_numeric($this->data[$this->name][$value])) {
            return false;
        }
        return true;
    }

}
