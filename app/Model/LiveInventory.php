<?php
App::uses('AppModel', 'Model');

class LiveInventory extends AppModel {

    public $useTable ='ruby2_livepos';

    public $belongsTo = array(		
		'RGroceryItem' => array(
			'className' => 'RGroceryItem',
			'foreignKey' =>false,
			'conditions' => array('RGroceryItem.plu_no = LiveInventory.upc'),
			'fields' => '',
			'order' => ''
		)
	);
	
	


}
