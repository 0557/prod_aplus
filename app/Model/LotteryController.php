<?php

ob_start();
App::uses('AppController', 'Controller');

/**
 * SalesInvoices Controller
 *
 * @property SalesInvoice $SalesInvoice
 * @property PaginatorComponent $Paginator
 */
class LotteryController extends AppController {

    //   public $components = array('Paginator', 'Mpdf.Mpdf');
    public $components = array('Paginator');

    public function admin_index() {

	if(!$_SESSION['store_id']) $this->Session->setFlash(__('Please select  store.')); 
	if($_SESSION['store_id'] !="") $this->set('display', 'yes');
    
	}
	    public function admin_game_import() {
	$this->loadmodel('Games');
	$this->set('import_game', 'active');
	$games = $this->Games->find('all',array('conditions' => array('status' => 0)));
	$this->set('Games', $games);
    }
	
	 public function admin_import_game() {
		
				$this->loadmodel('ImportedGames');
				
				$this->loadmodel('Games');
					$this->autoRender = false;
					$size = sizeof($_POST['ids']);
					
$yes = 0;
				
					for($i = 0; $i < $size; $i++ ){
						
					$getgame = $this->Games->find('first', array('conditions' => array('id' => $_POST['ids'][$i])));
						if($getgame['Games']['id']){
						$this->ImportedGames->create();
						$dat = array('games'=>$_POST['ids'][$i],'state'=>$_POST['state'],'status'=>'Ready to sale','store'=>$_POST['store'],'game_name'=>$getgame['Games']['game_name'],'value'=>$getgame['Games']['value'],'tickets_pack'=>$getgame['Games']['tickets_pack'],'start_ticket'=>$getgame['Games']['start_ticket'],'end_ticket'=>$getgame['Games']['end_ticket'],'game_no'=>$getgame['Games']['game_no'],'available'=>$getgame['Games']['tickets_pack'],'created_at'=>date('Y-m-d'));	
						if($this->ImportedGames->save($dat)){
							$yes=1;
							
							

						}
						
						
						}
							
					}
					
					if($yes==1){
						  $this->Session->setFlash(__('Selected games imported successfully.'));
							
						
					}else{
						  $this->Session->setFlash(__('Selected games already imported successfully.'));
						
						
					}
											
				
			}
			
	public function admin_getgamesbystate(){
		$this->loadmodel('Games');
		$this->loadmodel('ImportedGames');
					$this->autoRender = false;
		$getgame = $this->Games->find('all', array('conditions' => array('state' => $_POST['ids'])));
		    if (isset($getgame) && !empty($getgame)) { 

                                             foreach ($getgame as $data) { 
                                               echo '<tr>
                                                   <td><input type="checkbox" name="import" id="import" value="'.$data["Games"]["id"].'"/>&nbsp;</td>
                                                      
                                                      <td>'.$data["Games"]["game_no"].'</td>
                                                    <td>'.$data["Games"]["game_name"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["value"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["tickets_pack"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["start_ticket"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["end_ticket"].'&nbsp;</td>';
												
												$ready = $this->ImportedGames->find('first', array('conditions' => array('state' => $_POST['ids'],'game_no' => $data["Games"]["game_no"], 'store' => $_SESSION['store_id'])));
												
												if($ready['ImportedGames']['id']){
													
														echo '<td><button class="btn default btn-xs red-stripe">Ready to sale</button></td>';
												}else{
													echo '<td><button class="btn warning">Not Ready Yet</button></td>';
												}
                                                echo '</tr>';
											 } 
                                       } else { 
									 
                                           echo ' <tr>
                                                <td colspan="10">  No games found </td>
                                            </tr>';
                                        } 
		
	}	
public function admin_getgames(){
		$this->loadmodel('Games');
					$this->autoRender = false;
		$getgame = $this->Games->find('all', array('conditions' => array('state' => $_POST['ids'])));
		    if (isset($getgame) && !empty($getgame)) { 

                                             foreach ($getgame as $data) { 
                                               echo '<tr>
                                                      
                                                      <td>'.$data["Games"]["game_no"].'</td>
                                                    <td>'.$data["Games"]["game_name"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["value"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["tickets_pack"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["start_ticket"].'&nbsp;</td>
                                                    <td>'.$data["Games"]["end_ticket"].'&nbsp;</td>
													<td>
													<a href="'.Router::url('/').'admin/lottery/edit_game/'.$data["Games"]["id"].'" class="btn default btn-xs red-stripe">Edit</a> <button class="btn default btn-xs red-stripe" id="'.$data["Games"]["id"].'" onclick = "removegame(this.id);">Delete</button></td>
                                                </tr>';
                                             } 
                                       } else { 
									    
                                           echo ' <tr>
                                                <td colspan="10">  No games found </td>
                                            </tr>';
                                        } 
		
	}	
    public function admin_games() {
	$this->loadmodel('ImportedGames');
	$this->set('listofgames', 'active');
	$games = $this->ImportedGames->find('all',array('conditions' => array( 'store' => $_SESSION['store_id'],'status' => 'Ready to sale')));
	$this->set('Games', $games);
    }	
	public function admin_delete_game($id, $gid){
		$this->loadmodel('Games');
		$this->loadmodel('ImportedGames');
	
		if($this->ImportedGames->delete($gid)){
			
			$this->Session->setFlash(__('Imported games has been deleted.'));
			return $this->redirect(array('action' => 'games'));
			
		}
					
		
		
	}	
	
	function admin_delete_cpack($id){
		$this->loadmodel('GamePacks');
		if($this->GamePacks->delete($id)){
			
			$this->Session->setFlash(__('Confirmed Pack  has been deleted.'));
			return $this->redirect(array('action' => 'confirm_pack'));
			
		}
	}
	
		
	function admin_delete_apack($id){
		$this->loadmodel('GamePacks');
	if($this->GamePacks->updateAll(array('status'=>"'confirm'") , array('id' => $id))){
			$this->Session->setFlash(__('Activated Pack  has been deleted.'));
			return $this->redirect(array('action' => 'activate_pack'));
		}
	}
	
	public function admin_edit_game($id){
		$this->loadmodel('ImportedGames');
	if(isset($_POST['submit'])){
//		  $data = array('game_name'=>$this->request->data('game_name'),'value'=>$this->request->data('value'),'tickets_pack'=>$this->request->data('tickets_pack'),'state'=>$this->request->data('state'),'start_ticket'=>$this->request->data('start_ticket'),'end_ticket'=>$this->request->data('end_ticket'),'updated_at'=>date('Y-m-d'));
		  $data = array('game_name'=> "'".$_POST['game_name']."'",'value'=> "'".$_POST['value']."'",'tickets_pack'=> "'".$_POST['tickets_pack']."'",'state'=>"'".$_POST['state']."'",'start_ticket'=>"'".$_POST['start_ticket']."'",'end_ticket'=>"'".$_POST['end_ticket']."'",'updated_at'=>date('Y-m-d'));
	
	if($this->ImportedGames->updateAll($data , array('id' => $id))){
		
						$this->Session->setFlash(__('Updated successfully.'));
						return $this->redirect(array('action' => 'games'));
					
				}
	}else{
		$this->set('listofgames', 'active');
	$getgame = $this->ImportedGames->find('first', array('conditions' => array('id' => $id)));
	$this->set('editgame', $getgame);
	}
		
	
	}
	public function admin_new_game(){
		$this->loadmodel('ImportedGames');
	if(isset($_POST['submit'])){
		  $dat = array('game_no'=>$this->request->data('game_no'),'status'=>'Ready to sale', 'store' => $_SESSION['store_id'],'game_name'=>$this->request->data('game_name'),'value'=>$this->request->data('value'),'tickets_pack'=>$this->request->data('tickets_pack'),'state'=>$this->request->data('state'),'start_ticket'=>$this->request->data('start_ticket'),'end_ticket'=>$this->request->data('end_ticket'),'craeted_at'=>date('Y-m-d'));
	$this->ImportedGames->create();
//	$dat = array('game_no'=>$_POST['game_no'],'game_name'=>$_POST['corporation'],'state'=>$_POST['state'],'store'=>$_POST['store'],'game_name'=>$getgame['Games']['game_name'],'value'=>$getgame['Games']['value'],'tickets_pack'=>$getgame['Games']['tickets_pack'],'start_ticket'=>$getgame['Games']['start_ticket'],'end_ticket'=>$getgame['Games']['end_ticket'],'game_no'=>$getgame['Games']['game_no'],'created_at'=>date('Y-m-d'));	

				if($this->ImportedGames->save($dat)){
						$this->Session->setFlash(__('Inserted successfully.'));
						return $this->redirect(array('action' => 'games'));
					
				}
	}else{
		$this->set('listofgames', 'active');
	}
		
	
	}
	
	public function admin_confirm_pack(){
	
		$this->set('confirmpack', 'active');
			$this->loadmodel('ImportedGames');
			$this->loadmodel('GamePacks');

	$games = $this->ImportedGames->find('all',array('conditions' => array('status' => 'Ready to sale','store' => $_SESSION['store_id'])));
			$this->set('games', $games);
			
			if(isset($_POST['submit'])){
				
					$gamedata = $this->ImportedGames->find('first',array('conditions' => array('status' => 'Ready to sale','game_no' => $this->request->data['game_no'],'store' => $_SESSION['store_id'])));

				
					$valueas = $gamedata['ImportedGames']['value'];
					if(substr($gamedata['ImportedGames']['value'],0,1) == '$'){
						$valueas = substr($gamedata['ImportedGames']['value'],1) ;
					}
					$face_value = $valueas * $gamedata['ImportedGames']['tickets_pack'];
				
				$profit = (5*$face_value)/100;

	
				$valueas = $gamedata['ImportedGames']['tickets_pack'];
	
					
					$net_value = $face_value - $profit;
					
					$newDate = date("Y-m-d", strtotime($this->request->data('packdate')));
					
					$dat = array('game_no'=>$this->request->data('game_no'),
					'gamename'=>$gamedata['ImportedGames']['game_name'],
					'ticket_value'=>$gamedata['ImportedGames']['value'],
					'ticket_per_pack'=>$gamedata['ImportedGames']['tickets_pack'],
					'start_tkt'=>$gamedata['ImportedGames']['start_ticket'],
					'end_tkt'=>$gamedata['ImportedGames']['end_ticket'],
					'net_value'=>$net_value,'face_value'=>$face_value,
					'scan_ticket_code'=>$this->request->data('scan_ticket_code'),
					'pack_no'=>$this->request->data('pack_no'),
					'packdate'=>$newDate,
					'available'=>$gamedata['ImportedGames']['tickets_pack'],
					
					'cdate' => date('Y-m-d'),
					'status'=>'confirm',
					'store_id' => $_SESSION['store_id']);
				$datav = array('game_no'=>$this->request->data('game_no'),'pack_no'=>$this->request->data('pack_no'),'store_id' => $_SESSION['store_id']);
			
				$games = $this->GamePacks->find('first', array('conditions' => $datav));

			
	
				if($games['GamePacks']['pack_no']!=""){
						$this->Session->setFlash(__('This pack no '.$this->request->data('pack_no').'existed already'));
				}else{
					
					if($this->GamePacks->save($dat)){
				$this->Session->setFlash(__('Confirm pack added successfully.'));
						
					}else{
				$this->Session->setFlash(__('Pack not confirmed, there was something wrong.'));
						
					}
				}
			}
			
			$confirm_packs = $this->GamePacks->find('all',array('conditions' => array('status' => 'confirm','store_id' => $_SESSION['store_id'])));
			$this->set('confirm_packs', $confirm_packs);
			
			$active_packs = $this->GamePacks->find('all',array('conditions' => array('status' => 'active','store_id' => $_SESSION['store_id'])));
			$this->set('active_packs', $active_packs);
			
			
		
			
			

		}
	
	public function admin_activate_pack(){
		
		$this->set('activatepack', 'active');
			$this->loadmodel('GamePacks');

			$games = $this->GamePacks->find('all', array('conditions' => array('status' => 'confirm','store_id' => $_SESSION['store_id']),'group'=>'game_no'));
		
			$this->set('games', $games);
			
				if(isset($_POST['submit'])){
					
					$upd = array('status' => '"active"','bin_no'=>$this->request->data('bin_no'));
					
						if($this->GamePacks->updateAll($upd, array('game_no'=>$this->request->data('game_no'),'pack_no'=>$this->request->data('pack_no')))){
		
						$this->Session->setFlash(__('Activated successfully.'));
						
				}

				}
			
		$active_packs = $this->GamePacks->find('all',array('conditions' => array('status' => 'active','store_id' => $_SESSION['store_id'])));
		$this->set('active_packs', $active_packs);
		
			$confirm_packs = $this->GamePacks->find('all',array('conditions' => array('status' => 'confirm','store_id' => $_SESSION['store_id'])));
			$this->set('confirm_packs', $confirm_packs);
			
	}
	
	public function admin_daily_reading(){
		
	
	}	
	public function admin_daily_report(){
			$this->set('dailyreading', 'active');
			$this->loadmodel('GamePacks');
			$this->loadmodel('ImportedGames');
			$this->loadmodel('lottery_daily_readings');

			$games = $this->GamePacks->find('all', array('conditions' => array('status' => 'active','store_id' => $_SESSION['store_id']),'group'=>'game_no'));
		
			$this->set('games', $games);
			
			
			
				  $olddat = array('game_no'=>$this->request->data('game_no'),'pack_no'=>$this->request->data('pack_no'),'store_id' => $_SESSION['store_id']);
				  
				  
				  $olddata = array('store_id' => $_SESSION['store_id']);
				  
				  
		    $oldgames = $this->GamePacks->find('first', array('conditions' => $olddata));
			
		$next = $oldgames['GamePacks']['next_sold'];
	
		if(($next=="0000-00-00") || ($next=="")){
			$next = date('Y-m-d');
		}
		
				if(isset($_POST['submit'])){
					
					$upd = array('game_no' => $this->request->data('game_no'),'bin_no'=>$this->request->data('bin_no'),'pack_no'=>$this->request->data('pack_no'),'prev'=>$this->request->data('prtkt'),'today'=>$this->request->data('totkt'),'tdate'=>$next, 'store_id' => $_SESSION['store_id'],'status' => 'counting');
					
						if($this->lottery_daily_readings->save($upd)){
							$idss = $this->lottery_daily_readings->getInsertID();
							
						$availa = $this->request->data('prtkt') - $this->request->data('totkt');
						$this->Session->setFlash(__('Readin Enter successfully.'));
						
						$this->GamePacks->updateAll(array('available'=>$availa), $olddat);
						
						if($availa==0){
							$this->GamePacks->updateAll(array('status'=>'"sold out"'), $olddat);
							
							$this->lottery_daily_readings->updateAll(array('status'=>'"sold out"'), array('id'=>$idss));
						}
						
				}

				}
		//	$readings = $this->lottery_daily_readings->find('all', array('conditions' => array('tdate' =>$next)));
		
echo 'SELECT * FROM `lottery_daily_readings` AS e INNER JOIN `game_packs` AS u ON e.game_no = u.game_no and e.tdate = "'.$next.'" and e.store_id = '.$_SESSION['store_id'];

		$readings = $this->lottery_daily_readings->query('SELECT * FROM `lottery_daily_readings` AS e INNER JOIN `game_packs` AS u ON e.game_no = u.game_no and e.tdate = "'.$next.'" and e.store_id = '.$_SESSION['store_id']);
		echo sizeof($readings); 
		
		$this->set('readings', $readings);
		
		
	}
	
	public function admin_finish_daily_report(){
		$this->loadmodel('GamePacks');
		  $olddata = array('store_id' => $_SESSION['store_id']);
		$oldgames = $this->GamePacks->find('first', array('conditions' => $olddata));
			
		$next = $oldgames['GamePacks']['next_sold'];
		$newnext = date('Y-m-d', strtotime($next. ' + 1 days'));
		
		$this->GamePacks->updateAll(array('next_sold'=>$newnext), $olddat);
		$this->GamePacks->updateAll(array('available'=>$availa), $olddat);
			$this->Session->setFlash(__('Daily reading finished successfully.'));
		return $this->redirect(array('action' => 'daily_reading'));
	}
	
	public function admin_getpacks(){
				$this->loadmodel('GamePacks');
				$this->autoRender = false;
			  $dat = array('game_no'=>$this->request->data('game_no'),'status'=>'confirm', 'store_id' => $_SESSION['store_id']);
			
			$games = $this->GamePacks->find('all', array('conditions' => $dat));
			//	print_r($games);
			echo '<option value="" selected></option>';
			foreach($games as $games){
			echo '<option value="'.$games['GamePacks']['pack_no'].'">'.$games['GamePacks']['pack_no'].'</option>';
				
			}
		
	}
		public function admin_getpacksactive(){
				$this->loadmodel('GamePacks');
				$this->autoRender = false;
			  $dat = array('game_no'=>$this->request->data('game_no'),'status'=>'active', 'store_id' => $_SESSION['store_id']);
			
			$games = $this->GamePacks->find('all', array('conditions' => $dat));
			//	print_r($games);
			echo '<option value="" selected></option>';
			foreach($games as $games){
			echo '<option value="'.$games['GamePacks']['pack_no'].'">'.$games['GamePacks']['pack_no'].'</option>';
				
			}
		
	}
	public function admin_getgamedata(){
				$this->loadmodel('GamePacks');
				$this->loadmodel('ImportedGames');
				$this->autoRender = false;
		  $dat = array('scan_ticket_code'=>$this->request->data('scan'), 'store_id' => $_SESSION['store_id']);
		  
		  $games = $this->GamePacks->find('first', array('conditions' => $dat));
		  $result['gno'] = $games['GamePacks']['game_no'];
		  $result['pack'] = $games['GamePacks']['pack_no'];

		  $olddat = array('game_no'=>$games['GamePacks']['game_no'], 'store' => $_SESSION['store_id']);
				  
			$oldgames = $this->ImportedGames->find('first', array('conditions' => $olddat));
			$result['prev']  = $oldgames['ImportedGames']['available'];
		
		
		  return json_encode($result);
	}
	
	public function admin_gettcikets(){
		$this->loadmodel('LotteryDailyReading');
		$this->loadmodel('GamePacks');
		$this->autoRender = false;
		
				 $olddat = array('game_no'=>$this->request->data('game_no'),'pack_no'=>$this->request->data('pack_no'), 'store_id' => $_SESSION['store_id']);
				  
			$oldgames = $this->GamePacks->find('first', array('conditions' => $olddat));
				  
		
		echo	$prev = $oldgames['GamePacks']['available'];
		
	}
	
		public function admin_reports(){
		
		$this->set('reports', 'active');
	}
	public function admin_lotto_settlements(){
		
		$this->set('lottosettlements', 'active');
	}
	public function admin_pack_history(){
		
		$this->set('packhistory', 'active');
	}
	public function admin_return_pack(){
		
		$this->set('returnpack', 'active');
	}
	public function admin_settle_pack(){
		
		$this->set('settlepack', 'active');
	}
}
