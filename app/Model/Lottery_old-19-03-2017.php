<?php
App::uses('AppModel', 'Model');

class Lottery extends AppModel {

public $useTable ='lottery_daily_readings';

public $belongsTo = array(		
		'GamePacks' => array(
			'className' => 'GamePacks',
			'foreignKey' =>false,
			'conditions' => array('Lottery.game_no = GamePacks.game_no','Lottery.store_id = GamePacks.store_id'),
			'fields' => '',
			'order' => ''
		)
	);
	



}
