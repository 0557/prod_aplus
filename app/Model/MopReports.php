<?php

App::uses('AppModel', 'Model');

/**
 * Corporation Model
 *
 * @property Federal $Federal
 * @property StateWh $StateWh
 * @property Country $Country
 * @property State $State
 * @property City $City
 * @property Store $Store
 */
class MopReports extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'Product_Name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Product Name is Required!',
                'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
		'Cash_Amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'Credit_Amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'Cheque_Amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'Sequence_No' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
    );

}
