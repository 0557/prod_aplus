<?php

App::uses('AppModel', 'Model');

/**
 * Customer Model
 *
 * @property Store $Store
 * @property State $State
 * @property City $City
 */
class Payment extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
  	'Invoices' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),'Creditcards' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),'Creditcardfeeadjustment' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),'Cashcardcredits' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),'Cashcardcommissionadjustment' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),'Othercharges' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Enter degits only!',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
	   
        'Store' => array(
            'className' => 'Store',
            'foreignKey' => 'store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
