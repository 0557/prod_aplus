<?php
App::uses('AppModel', 'Model');
/**
 * PurchaseInvoice Model
 *
 */
class PurchaseInvoice extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
    
	
	public $belongsTo = array(
		
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'supplier_id',
			'conditions' => array('type' => 'fuel', 'customer_type' => 'Vendor'),
			'fields' => '',
			'order' => ''
		),
            'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);
        
        public $hasMany = array(
		
		'FuelProduct' => array(
			'className' => 'FuelProduct',
			'foreignKey' => 'r_purchase_invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		));
        
    /*    function beforeValidate($options = array()) {

        if (isset($this->data[$this->alias]["files"]["name"]) && $this->data[$this->alias]["files"]["name"] == '') {
            unset($this->data[$this->alias]["files"]);
        }
		
    }*/
}
