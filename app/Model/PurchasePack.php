<?php

App::uses('AppModel', 'Model');

/**
 * Customer Model
 *
 * @property Store $Store
 * @property State $State
 * @property City $City
 */
class PurchasePack extends AppModel {

	
    public $belongsTo = array(
	    'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'vendor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
