<?php

App::uses('AppModel', 'Model');

/**
 * Customer Model
 *
 * @property Store $Store
 * @property State $State
 * @property City $City
 */
class PurchasePacks extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
      
        'Units_Per_Case' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Value is Required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Only AlphaNumeric Value is Allowed.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
       
        'Case_Cost' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Value is Required.',
                'allowEmpty' => true,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

	
    public $belongsTo = array(
	    'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'vendor',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
