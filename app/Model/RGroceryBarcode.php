<?php

App::uses('AppModel', 'Model');

/**
 * RGroceryBarcode Model
 *
 * @property RGroceryDepartment $RGroceryDepartment
 */
class RGroceryBarcode extends AppModel {

    public $name = 'RGroceryItem';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(/*
		'RubyDepartment' => array(
            'className' => 'RubyDepartment',
            'conditions' => array(' `RGroceryItem`.`r_grocery_department_id` = `RubyDepartment`.`number`'),
            'fields' => '',
            'order' => ''
        )*/
    );

}
