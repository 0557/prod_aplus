<?php

App::uses('AppModel', 'Model');

/**
 * RGroceryDepartment Model
 *
 * @property Corporation $Corporation
 * @property Store $Store
 */
class RGroceryDepartment extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'corporation_id' => array(
            'numeric' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'store_id' => array(
            'numeric' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'department' => array(
            'numeric' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'numeric2' => array(
                'rule' => array('maxLength', 4),
                'message' => 'Department No. Must be between(0-9999)'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'onlyPositive' => array(
                'rule' => array('onlypositive'),
                'message' => 'Department No. Must be Positive Number '
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'numeric' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'description' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'min_amount' => array(
             'onlyPositive' => array(
                'rule' => array('onlypositive'),
                'message' => 'Min. Amount. Must be Positive Number '
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'lessThanMyOtherField' => array(
                'rule' => array('comparisonWithField', '<=', 'max_amount'),
                'message' => 'Min Amount Must Be less Than Max Amount',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            
        ),
        'max_amount' => array(
             'onlyPositive' => array(
                'rule' => array('onlypositive'),
                'message' => 'Max. Amount. Must be Positive Number '
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'fee' => array(
            'Limit' => array(
                'rule' => array('maxLength', 2),
                'message' => 'Fee/Charge Must be between(0-99)',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'onlyPositive' => array(
                'rule' => array('onlypositive'),
                'message' => 'fee. Must be Positive Number '
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ), 
        
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Corporation' => array(
            'className' => 'Corporation',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Store' => array(
            'className' => 'Store',
            'foreignKey' => 'store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function comparisonWithField($validationFields = array(), $operator = null, $compareFieldName = '') {
        if (!isset($this->data[$this->name][$compareFieldName])) {
            throw new CakeException(sprintf(__('Can\'t compare to the non-existing field "%s" of model %s.'), $compareFieldName, $this->name));
        }
        $compareTo = $this->data[$this->name][$compareFieldName];
        foreach ($validationFields as $key => $value) {
            if (!Validation::comparison($value, $operator, $compareTo)) {
                return false;
            }
        }
        return true;
    }
   
    public function onlypositive($check) {

        $value = array_keys($check);
        $value = $value[0];
        if (!isset($this->data[$this->name][$value])) {
            throw new CakeException(sprintf(__('Can\'t compare to the non-existing field "%s" of model %s.'), $compareFieldName, $this->name));
        }
        if($this->data[$this->name][$value] < 0 ){
            return false;
        }
        return true;
    }
    
     public function beforeSave($options = array()) {
     
        if ($this->data['RGroceryDepartment']['taxable'] == '0') {
             unset($this->data['RGroceryDepartment']['tax1']);
             unset($this->data['RGroceryDepartment']['tax2']);
             unset($this->data['RGroceryDepartment']['tax3']);
             unset($this->data['RGroceryDepartment']['tax4']);
        } 
        return true;
    }

}
