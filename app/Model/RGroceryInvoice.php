<?php

App::uses('AppModel', 'Model');

/**
 * RGroceryInvoice Model
 *
 * @property Corporation $Corporation
 * @property Store $Store
 * @property Customer $Customer
 */
class RGroceryInvoice extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
       /* 'corporation_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'store_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'invoice_date' => array(
            'datetime' => array(
                'rule' => array('datetime'),
            'message' => 'Please select datetime',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
        'po' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Value is Required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'invoice' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This is Required Field.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'alphaNumeric' => array(
                'rule' => array('alphaNumeric'),
                'message' => 'Alpha Numeric is Allowed.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'mop' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'status' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
      'vendor_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        /*  'terms' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
      'files' => array(
            'extension' => array(
                'rule' => array('extension', array('xlsx','csv')),
                'message' => 'Please upload a valid File. Only xlsx ,csv',
                'allowEmpty' => true,
                'on' => 'create',
            ),
            'upload-file' => array(
                'rule' => array('uploadImages', 'rgroceryinvoice'),
                'message' => 'Error uploading file',
            )
        ),*/

    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
      /*  'Corporation' => array(
            'className' => 'Corporation',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),*/
        'Store' => array(
            'className' => 'Store',
            'foreignKey' => 'store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'vendor_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )/*,
		'PurchasePack' => array(
            'className' => 'PurchasePack',
            'foreignKey' => false,
            'conditions' => array(
                 'RGroceryInvoice.invoice = PurchasePack.invoice',
				 'RGroceryInvoice.vendor_id = PurchasePack.vendor',
             )
    	),*/
    );
    public $hasMany = array(
		

		
		/*
        'RGroceryInvoiceProduct' => array(
            'className' => 'RGroceryInvoiceProduct',
            'foreignKey' => 'r_grocery_invoice_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
    	),*/
	
		
	
	);

   /* public function beforeValidate($options = array()) {

        if (empty($this->data['RGroceryInvoice']['files']['name'])) {
            unset($this->data['RGroceryInvoice']['files']);
        }
        return true;
    }*/

}
