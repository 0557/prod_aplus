<?php

App::uses('AppModel', 'Model');

/**
 * RGroceryPromotion Model
 *
 * @property Corporation $Corporation
 * @property Store $Store
 */
class RGroceryPromotion extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'corporation_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'store_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'plu_no' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'str_disc_period' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'end_disc_period' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'greaterThanOtherField' => array(
                'rule' => array('comparisonWithField', '<=', 'str_disc_period'),
                'message' => 'End Discount Must be Greater Than Beginning Discount Period',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'disc_amount' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Value is Allowed',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'disc_amount_percent' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'This field is required.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Corporation' => array(
            'className' => 'Corporation',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Store' => array(
            'className' => 'Store',
            'foreignKey' => 'store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function comparisonWithField($validationFields = array(), $operator = null, $compareFieldName = '') {
       
        if (!isset($this->data[$this->name][$compareFieldName])) {
            throw new CakeException(sprintf(__('Can\'t compare to the non-existing field "%s" of model %s.'), $compareFieldName, $this->name));
        }
        $compareTo = strtotime($this->data[$this->name][$compareFieldName]);
        $first_field =  strtotime($validationFields['end_disc_period']);
        if($first_field <= $compareTo ){
            return false;
        }
        return true;
    }

}

