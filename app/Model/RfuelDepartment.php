<?php
App::uses('AppModel', 'Model');
/**
 * FuelDepartment Model
 *
 * @property WholesaleProduct $WholesaleProduct
 */
class RfuelDepartment extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 * 
 * 
 */
        public $belongsTo = array(
            'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);
        
        
        
        
//	public $hasMany = array(
//		'WholesaleProduct' => array(
//			'className' => 'WholesaleProduct',
//			'foreignKey' => 'fuel_department_id',
//			'dependent' => false,
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'exclusive' => '',
//			'finderQuery' => '',
//			'counterQuery' => ''
//		)
//	);

}
