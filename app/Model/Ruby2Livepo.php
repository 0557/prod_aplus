<?php
App::uses('AppModel', 'Model');
/**
 * Ruby2Livepo Model
 *
 * @property Store $Store
 */
class Ruby2Livepo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $name = 'ruby2_vposjournals';
	public $displayField = 'name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
        public function getdata($store_id = null)
        {
            //$sql = "SELECT a.*, b.name FROM ruby2_livepos a inner join stores b on a.store_id = b.id";
            if($store_id != null)
            {
                $sql = 'select a.name, b.* 
     from stores a,ruby2_livepos b 
     where b.store_id=a.id and b.store_id ='.$store_id;
                $data = $this->query($sql);
            return $data;
            }
            
            
        }
}
