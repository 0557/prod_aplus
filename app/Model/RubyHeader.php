<?php
App::uses('AppModel', 'Model');
/**
 * RubyHeader Model
 *
 * @property Store $Store
 * @property RubyFprod $RubyFprod
 * @property RubyUprodt $RubyUprodt
 * @property RubyUtankt $RubyUtankt
 */
class RubyHeader extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'RubyFprod' => array(
			'className' => 'RubyFprod',
			'foreignKey' => 'ruby_header_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'RubyUprodt' => array(
			'className' => 'RubyUprodt',
			'foreignKey' => 'ruby_header_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'RubyUtankt' => array(
			'className' => 'RubyUtankt',
			'foreignKey' => 'ruby_header_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
