<?php

App::uses('AppModel', 'Model');

class RubyMemo1 extends AppModel {

	public $useTable = 'ruby_memo1s';
    
	
	
	public $belongsTo = array(		
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' =>'',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	
	
}
