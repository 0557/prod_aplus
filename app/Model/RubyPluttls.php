<?php
App::uses('AppModel', 'Model');
/**
 * RubyDeptotal Model
 *
 * @property Store $Store
 */
class RubyPluttl extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
