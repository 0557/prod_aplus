<?php

App::uses('AppModel', 'Model');

class RubyTotmop extends AppModel {

	public $useTable = 'ruby_totmops';
    
	
	
	public $belongsTo = array(		
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' =>'',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	
	
}
