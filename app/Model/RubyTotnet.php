<?php

App::uses('AppModel', 'Model');

class RubyTotnet extends AppModel {

	public $useTable = 'ruby_totnets';
    
	
	
	public $belongsTo = array(		
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' =>'',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	
	
}
