<?php

App::uses('AppModel', 'Model');

class RubyTottaxe extends AppModel {

	public $useTable = 'ruby_tottaxes';
    
	
	public $belongsTo = array(		
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	
	
}
