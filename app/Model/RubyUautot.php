<?php
App::uses('AppModel', 'Model');
/**
 * RubyUautot Model
 *
 * @property RubyHeader $RubyHeader
 */
class RubyUautot extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
