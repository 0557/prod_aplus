<?php
App::uses('AppModel', 'Model');
/**
 * RubyUserlvt Model
 *
 * @property RubyHeader $RubyHeader
 */
class RubyUserlvt extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
