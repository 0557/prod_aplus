<?php
App::uses('AppModel', 'Model');
/**
 * RubyUtiert Model
 *
 * @property RubyHeader $RubyHeader
 * @property FuelProduct $FuelProduct
 */
class RubyUtiert extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'RubyHeader' => array(
			'className' => 'RubyHeader',
			'foreignKey' => 'ruby_header_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FuelProduct' => array(
			'className' => 'FuelProduct',
			'foreignKey' => 'fuel_product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
