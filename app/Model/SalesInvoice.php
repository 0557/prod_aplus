<?php
App::uses('AppModel', 'Model');
/**
 * SalesInvoice Model
 *
 * @property Supplier $Supplier
 * @property Product $Product
 */
class SalesInvoice extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
//		'Supplier' => array(
//			'className' => 'Supplier',
//			'foreignKey' => 'supplier_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		),
//		'Product' => array(
//			'className' => 'Product',
//			'foreignKey' => 'product_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		),
            'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			//'conditions' => 'TaxeZone.state_id',
			'fields' => '',
			'order' => ''
		),
            'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			//'conditions' => 'TaxeZone.state_id',
			'fields' => '',
			'order' => ''
		),
            'Corporation' => array(
			'className' => 'Corporation',
			'foreignKey' => 'corporation_id',
			//'conditions' => 'TaxeZone.state_id',
			'fields' => '',
			'order' => ''
		),
               
	);
        
  
        public $hasMany = array(
		'TaxeZone' => array(
			'className' => 'TaxeZone',
			'foreignKey' => 'sale_invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'FuelProduct' => array(
			'className' => 'FuelProduct',
			'foreignKey' => 'fuel_sale_invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		));
    
}
