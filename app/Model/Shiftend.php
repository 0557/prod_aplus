<?php

App::uses('AppModel', 'Model');

/**
 * Corporation Model
 *
 * @property Federal $Federal
 * @property StateWh $StateWh
 * @property Country $Country
 * @property State $State
 * @property City $City
 * @property Store $Store
 */
class Shiftend extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Name is Required!',
                'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'federalId' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Federal Id is Required Field',
//                'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'payroll_account' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Payroll Account is Required Field',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'email' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Email is Required Field.',
//            ),
//            'email' => array(
//                'rule' => array('email'),
//                'message' => 'Please Enter Email in correct Format',
//            ),
//            'isUnique' => array(
//                'rule' => array('isUnique'),
//                'message' => 'Email address already in use',
//            )
//        ),
//        'phone' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Phone is Required Field.',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'fax' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Fax is Required Field',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'quick_book_path' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Quick Book path is Required Field',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'state_wh_id' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'State Wh Id is required Field',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'address' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Address is Required Fields',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'country_id' => array(
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Please Select A Country',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
////        'state_id' => array(
////            'numeric' => array(
////                'rule' => array('numeric'),
////            'message' => 'Your custom message here',
////            //'allowEmpty' => false,
////            //'required' => false,
////            //'last' => false, // Stop validation after this rule
////            //'on' => 'create', // Limit validation to 'create' or 'update' operations
////            ),
////        ),
//        'city_id' => array(
//            'numeric' => array(
//                'rule' => array('numeric'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'zipcode' => array(
//            'numeric' => array(
//                'rule' => array('numeric'),
//                'message' => 'Enter Zip Code in coorect Format',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//            'notBlank' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Zip Code is Required Field',
//            ),
//        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    
    public $belongsTo = array(
       'Store' => array(
			'className' => 'Store',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
//        'Corporation' => array(
//			'className' => 'Store',
//			'foreignKey' => 'corporation_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		),

    );

    /**
     * hasMany associations
     *
     * @var array
     */
//    public $hasMany = array(
//        'Store' => array(
//            'className' => 'Store',
//            'foreignKey' => 'rwholesale_product_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
//    );

}
