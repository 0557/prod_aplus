<?php
App::uses('AppModel', 'Model');
/**
 * Store Model
 *
 * @property Corporation $Corporation
 * @property Country $Country
 * @property State $State
 * @property City $City
 */
class Store extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'corporation_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Corporation is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Name is Required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            
//		'business_type' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Buisness Type is Required Field',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'Address' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Address Required Field',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'country_id' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Please Select Your Country',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'state_id' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Please Select Your State',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'city_id' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//				//'message' => 'Your custom message here',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'zip_code' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Zip Code is Required field',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		'sales_tax' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Sales Tax is Required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//		'fax' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Fax is Required Field',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//		'phone' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//				'message' => 'Phone no. must be numeric',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//                    'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'Phone is Required Field',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
            'email' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Email is Required!',
            ),
            'email' => array(
                'rule' => array('email'),
                'message' => 'Please Enter Email in correct Format',
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Email address already in use',
            )
        ),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Corporation' => array(
			'className' => 'Corporation',
			'foreignKey' => 'corporation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ZipCode' => array(
			'className' => 'ZipCode',
			'foreignKey' => 'zip_code',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
