<?php

App::uses('AppModel', 'Model');

/**
 * Corporation Model
 *
 * @property Federal $Federal
 * @property StateWh $StateWh
 * @property Country $Country
 * @property State $State
 * @property City $City
 * @property Store $Store
 */
class TankReports extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'Tank_Name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Product Name is Required!',
                'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
		'Volume' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'createdate' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'Amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
		  'Sequence_No' => array(
            'numeric' => array(
                'rule' => array('numeric'),
              'message' => 'Enter Numerics only',
          //'allowEmpty' => false,
          //'required' => false,
         //'last' => false, // Stop validation after this rule
       //'on' => 'create', // Limit validation to 'create' or 'update' operations
          ),
		  ),
    );

}
