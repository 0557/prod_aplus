<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Role $Role
 * @property ClientSetting $ClientSetting
 * @property ClientStore $ClientStore
 */
class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter First name.'
            ),
        ),
        'last_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter Last Name.'
            ),
        ),
        'phone' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter phone no.'
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide a valid email address',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Email address already in use',
            )
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter password.',
                'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'confirm_password' => array(
            'equaltofield' => array(
            'rule' => array('equaltofield','password'),
            'message' => 'Password and confirm password does not match.',
            'on' => 'create', // Limit validation to 'create' or 'update' operations
            )
        ),
//        'title' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//                'message' => 'Please enter title.'
//            ),
//        ),
//            'intervention' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//                'message' => 'Please enter intervention.'
//            ),
//        ),
    );




    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array('Role', 'Country', 'State');
    public $hasMany = array(
        'Userlog' => array(
            'className' => 'Userlog',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function beforeSave($options=array()) {

        if (!empty($this->data['User']['password'])) {
			//echo $this->data['User']['password'];
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		//	echo $this->data['User']['password'];
		//	exit;
        } else {
            unset($this->data['User']['password']);
        }
        return true;
    }
    
    function equaltofield($check,$otherfield)
    {
        //get name of field
        $fname = '';
        foreach ($check as $key => $value){
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
 } 

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}
