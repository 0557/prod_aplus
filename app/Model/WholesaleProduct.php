<?php
App::uses('AppModel', 'Model');
/**
 * WholesaleProduct Model
 *
 * @property FuelDepartment $FuelDepartment
 * @property Customer $Customer
 */
class WholesaleProduct extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fuel_department_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//            'tank_capacity' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//				'message' => 'This is required!',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//            'tank_type' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'This is required!',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//            'piping' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'This is required!',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//            'Piping_date' => array(
//			'notBlank' => array(
//				'rule' => array('notBlank'),
//				'message' => 'This is required!',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		'price' => array(
			'decimal' => array(
				'rule' => array('decimal'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tax_class' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'vendor_price' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'qty' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'store_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This is required!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FuelDepartment' => array(
			'className' => 'FuelDepartment',
			'foreignKey' => 'fuel_department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			
			'fields' => '',
			'order' => ''
		)
	);
}
