<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Payment successful</title>
		<meta http-equiv="refresh" content="5;URL='<?php //echo Configure::read('Worldpay.complete-url'); ?>'">
		<style>
		body {
			font-family: 'Arial', sans-serif;
			background: #EFEFEF;
		}
		</style>
	</head>
	<body>
	    <section id="products">
<div class="bx-wrapper" style="max-width: 5000px;">
<div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 510px;">
		<h1><?php echo $this->Html->image(
			Configure::read('App.logo'),
			array('alt' => Configure::read('App.name'), 'fullBase' => true)
		) ?></h1>
		<p>Payment has been successfull. You will be redirected to the website in 5 sec, or click <?php echo $this->Html->link('here', Configure::read('Worldpay.complete-url')); ?> to continue</p>
		<?php
	echo $this->Worldpay->form(array(
		'cartId' => 10, // your cart / order id
		'amount' => 102.00, // total amount to be paid
	)); ?>
</div>
</div>
	    </section>	</body>
</html>
