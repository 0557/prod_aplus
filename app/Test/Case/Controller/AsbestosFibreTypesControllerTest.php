<?php
App::uses('AsbestosFibreTypesController', 'Controller');

/**
 * AsbestosFibreTypesController Test Case
 *
 */
class AsbestosFibreTypesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.asbestos_fibre_type',
		'app.properties_asbestos_to_fibre_type'
	);

}
