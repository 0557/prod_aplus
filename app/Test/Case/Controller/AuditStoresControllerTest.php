<?php
App::uses('AuditStoresController', 'Controller');

/**
 * AuditStoresController Test Case
 *
 */
class AuditStoresControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_store',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit',
		'app.audit_location',
		'app.audit_operations_region',
		'app.audit_division',
		'app.audit_request',
		'app.audit_to_audit_question',
		'app.audit_video',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_question_answer'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
