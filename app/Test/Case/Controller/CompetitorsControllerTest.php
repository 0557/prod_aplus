<?php
App::uses('CompetitorsController', 'Controller');

/**
 * CompetitorsController Test Case
 */
class CompetitorsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.competitor',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail',
		'app.vacinationtodetail'
	);

}
