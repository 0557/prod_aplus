<?php
App::uses('DiseasesController', 'Controller');

/**
 * DiseasesController Test Case
 *
 */
class DiseasesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.disease',
		'app.disease_medician',
		'app.disease_precaution',
		'app.site_permission',
		'app.role',
		'app.user',
		'app.agelimit',
		'app.vacinationdetail',
		'app.month',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail',
		'app.userlog',
		'app.manager'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testDeleteall method
 *
 * @return void
 */
	public function testDeleteall() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

/**
 * testAdminDeleteall method
 *
 * @return void
 */
	public function testAdminDeleteall() {
	}

}
