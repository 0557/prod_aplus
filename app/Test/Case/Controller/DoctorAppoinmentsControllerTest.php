<?php
App::uses('DoctorAppoinmentsController', 'Controller');

/**
 * DoctorAppoinmentsController Test Case
 *
 */
class DoctorAppoinmentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doctor_appoinment',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.hospital_category',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail',
		'app.disease',
		'app.disease_precaution',
		'app.vacinationdetail_to_disease',
		'app.userlog',
		'app.doctor_hospital',
		'app.week'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

/**
 * testAdminDeleteall method
 *
 * @return void
 */
	public function testAdminDeleteall() {
	}

}
