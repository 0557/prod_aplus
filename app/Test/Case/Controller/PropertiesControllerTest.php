<?php
App::uses('PropertiesController', 'Controller');

/**
 * PropertiesController Test Case
 *
 */
class PropertiesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.property_asbestos_survey',
		'app.property_asbesto_survey',
		'app.property_asbesto',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.property_floorplan',
		'app.property_photo',
		'app.property_room'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
