<?php
App::uses('TransactionHistoriesController', 'Controller');

/**
 * TransactionHistoriesController Test Case
 *
 */
class TransactionHistoriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.transaction_history',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.lead',
		'app.lead_type',
		'app.event'
	);

}
