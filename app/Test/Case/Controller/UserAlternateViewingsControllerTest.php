<?php
App::uses('UserAlternateViewingsController', 'Controller');

/**
 * UserAlternateViewingsController Test Case
 *
 */
class UserAlternateViewingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_alternate_viewing',
		'app.property',
		'app.propertytype',
		'app.trainstation',
		'app.tubeline',
		'app.property_photo',
		'app.property_video',
		'app.property_floorplan',
		'app.property_room',
		'app.property_room_photo',
		'app.user_viewing_schdule',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_viewing',
		'app.user_viewing_agegroup',
		'app.user_viewing_schdules_to_user',
		'app.amendity',
		'app.property_rooms_to_amendity',
		'app.bathroomtype',
		'app.property_rooms_to_bathroomtype',
		'app.bedroomclass',
		'app.property_rooms_to_bedroomclass',
		'app.feature',
		'app.property_rooms_to_feature',
		'app.properties_to_feature'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
