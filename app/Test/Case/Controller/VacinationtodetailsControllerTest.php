<?php
App::uses('VacinationtodetailsController', 'Controller');

/**
 * VacinationtodetailsController Test Case
 *
 */
class VacinationtodetailsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacinationtodetail',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.vacinationrequiredetail',
		'app.userlog'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testDeleteall method
 *
 * @return void
 */
	public function testDeleteall() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

/**
 * testAdminDeleteall method
 *
 * @return void
 */
	public function testAdminDeleteall() {
	}

}
