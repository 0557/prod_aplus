<?php
App::uses('WholesaleProductsController', 'Controller');

/**
 * WholesaleProductsController Test Case
 */
class WholesaleProductsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wholesale_product',
		'app.fuel_department',
		'app.customer',
		'app.corporation',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail',
		'app.vacinationtodetail',
		'app.store'
	);

}
