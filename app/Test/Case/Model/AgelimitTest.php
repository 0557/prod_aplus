<?php
App::uses('Agelimit', 'Model');

/**
 * Agelimit Test Case
 *
 */
class AgelimitTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.agelimit',
		'app.disease_medician',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Agelimit = ClassRegistry::init('Agelimit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Agelimit);

		parent::tearDown();
	}

}
