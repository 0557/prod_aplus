<?php
App::uses('AgentResource', 'Model');

/**
 * AgentResource Test Case
 *
 */
class AgentResourceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.agent_resource'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AgentResource = ClassRegistry::init('AgentResource');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AgentResource);

		parent::tearDown();
	}

}
