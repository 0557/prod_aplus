<?php
App::uses('Amendity', 'Model');

/**
 * Amendity Test Case
 *
 */
class AmendityTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.amendity'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Amendity = ClassRegistry::init('Amendity');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Amendity);

		parent::tearDown();
	}

}
