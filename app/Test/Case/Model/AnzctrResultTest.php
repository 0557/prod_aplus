<?php
App::uses('AnzctrResult', 'Model');

/**
 * AnzctrResult Test Case
 *
 */
class AnzctrResultTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.anzctr_result',
		'app.anzctr_condition',
		'app.anzctr_contact',
		'app.primaryoutcome',
		'app.secondaryoutcome'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AnzctrResult = ClassRegistry::init('AnzctrResult');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AnzctrResult);

		parent::tearDown();
	}

}
