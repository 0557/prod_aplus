<?php
App::uses('ApiCredential', 'Model');

/**
 * ApiCredential Test Case
 *
 */
class ApiCredentialTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.api_credential',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.package',
		'app.manager'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ApiCredential = ClassRegistry::init('ApiCredential');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ApiCredential);

		parent::tearDown();
	}

}
