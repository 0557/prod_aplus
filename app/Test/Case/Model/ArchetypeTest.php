<?php
App::uses('Archetype', 'Model');

/**
 * Archetype Test Case
 *
 */
class ArchetypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.archetype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Archetype = ClassRegistry::init('Archetype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Archetype);

		parent::tearDown();
	}

}
