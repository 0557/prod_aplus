<?php
App::uses('AsbestosFibreType', 'Model');

/**
 * AsbestosFibreType Test Case
 *
 */
class AsbestosFibreTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.asbestos_fibre_type',
		'app.properties_asbestos_to_fibre_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AsbestosFibreType = ClassRegistry::init('AsbestosFibreType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AsbestosFibreType);

		parent::tearDown();
	}

}
