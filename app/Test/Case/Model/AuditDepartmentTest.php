<?php
App::uses('AuditDepartment', 'Model');

/**
 * AuditDepartment Test Case
 *
 */
class AuditDepartmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_to_audit_question'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditDepartment = ClassRegistry::init('AuditDepartment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditDepartment);

		parent::tearDown();
	}

}
