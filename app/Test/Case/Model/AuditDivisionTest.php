<?php
App::uses('AuditDivision', 'Model');

/**
 * AuditDivision Test Case
 *
 */
class AuditDivisionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_division',
		'app.audit_operations_region',
		'app.audit_location',
		'app.audit',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_to_audit_question',
		'app.audit_request',
		'app.audit_store',
		'app.audit_video'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditDivision = ClassRegistry::init('AuditDivision');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditDivision);

		parent::tearDown();
	}

}
