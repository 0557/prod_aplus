<?php
App::uses('AuditLocation', 'Model');

/**
 * AuditLocation Test Case
 *
 */
class AuditLocationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_location',
		'app.audit',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_to_audit_question',
		'app.audit_request',
		'app.audit_store',
		'app.audit_video',
		'app.audit_operations_region'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditLocation = ClassRegistry::init('AuditLocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditLocation);

		parent::tearDown();
	}

}
