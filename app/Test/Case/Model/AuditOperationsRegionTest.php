<?php
App::uses('AuditOperationsRegion', 'Model');

/**
 * AuditOperationsRegion Test Case
 *
 */
class AuditOperationsRegionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_operations_region',
		'app.audit_location',
		'app.audit',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_to_audit_question',
		'app.audit_request',
		'app.audit_store',
		'app.audit_video',
		'app.audit_division'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditOperationsRegion = ClassRegistry::init('AuditOperationsRegion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditOperationsRegion);

		parent::tearDown();
	}

}
