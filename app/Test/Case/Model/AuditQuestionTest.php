<?php
App::uses('AuditQuestion', 'Model');

/**
 * AuditQuestion Test Case
 *
 */
class AuditQuestionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_question',
		'app.audit_department',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit',
		'app.audit_operations_region',
		'app.audit_division',
		'app.audit_store',
		'app.audit_location',
		'app.audit_request',
		'app.client_store',
		'app.store_group',
		'app.audit_information',
		'app.audit_question_answer',
		'app.audit_to_audit_question',
		'app.audit_video',
		'app.audit_question_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditQuestion = ClassRegistry::init('AuditQuestion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditQuestion);

		parent::tearDown();
	}

}
