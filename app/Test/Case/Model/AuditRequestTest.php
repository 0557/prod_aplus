<?php
App::uses('AuditRequest', 'Model');

/**
 * AuditRequest Test Case
 *
 */
class AuditRequestTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_request',
		'app.audit',
		'app.audit_to_audit_question',
		'app.audit_video',
		'app.client_store',
		'app.audit_information',
		'app.audit_question_answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditRequest = ClassRegistry::init('AuditRequest');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditRequest);

		parent::tearDown();
	}

}
