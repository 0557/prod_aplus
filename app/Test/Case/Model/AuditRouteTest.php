<?php
App::uses('AuditRoute', 'Model');

/**
 * AuditRoute Test Case
 *
 */
class AuditRouteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_route',
		'app.audit_route_to_audit'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditRoute = ClassRegistry::init('AuditRoute');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditRoute);

		parent::tearDown();
	}

}
