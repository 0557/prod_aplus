<?php
App::uses('AuditStore', 'Model');

/**
 * AuditStore Test Case
 *
 */
class AuditStoreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit_store',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit',
		'app.audit_location',
		'app.audit_operations_region',
		'app.audit_division',
		'app.audit_request',
		'app.audit_to_audit_question',
		'app.audit_video',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_question_answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuditStore = ClassRegistry::init('AuditStore');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuditStore);

		parent::tearDown();
	}

}
