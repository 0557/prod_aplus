<?php
App::uses('Audit', 'Model');

/**
 * Audit Test Case
 *
 */
class AuditTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.audit',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.client_information',
		'app.audit_department',
		'app.audit_information',
		'app.audit_question',
		'app.audit_to_audit_question',
		'app.audit_request',
		'app.audit_store',
		'app.audit_location',
		'app.audit_video'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Audit = ClassRegistry::init('Audit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Audit);

		parent::tearDown();
	}

}
