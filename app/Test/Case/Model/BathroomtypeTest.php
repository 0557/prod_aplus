<?php
App::uses('Bathroomtype', 'Model');

/**
 * Bathroomtype Test Case
 *
 */
class BathroomtypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bathroomtype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bathroomtype = ClassRegistry::init('Bathroomtype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bathroomtype);

		parent::tearDown();
	}

}
