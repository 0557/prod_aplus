<?php
App::uses('BctrResult', 'Model');

/**
 * BctrResult Test Case
 *
 */
class BctrResultTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bctr_result',
		'app.trial',
		'app.bctr_contact_person',
		'app.bctr_contact',
		'app.bctr_health_condition',
		'app.bctr_keyword',
		'app.bctr_outcome_translation',
		'app.bctr_primary_outcome',
		'app.bctr_recruitment_country',
		'app.bctr_recruitment',
		'app.bctr_secondary_id',
		'app.bctr_secondary_outcome',
		'app.bctr_source_support',
		'app.bctr_sponsors_and_support',
		'app.bctr_study',
		'app.bctr_text_translation',
		'app.bctr_translation',
		'app.secondary_id'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BctrResult = ClassRegistry::init('BctrResult');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BctrResult);

		parent::tearDown();
	}

}
