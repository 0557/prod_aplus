<?php
App::uses('Bedroomclass', 'Model');

/**
 * Bedroomclass Test Case
 *
 */
class BedroomclassTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bedroomclass'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bedroomclass = ClassRegistry::init('Bedroomclass');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bedroomclass);

		parent::tearDown();
	}

}
