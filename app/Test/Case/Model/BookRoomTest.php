<?php
App::uses('BookRoom', 'Model');

/**
 * BookRoom Test Case
 *
 */
class BookRoomTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.book_room',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_application_form',
		'app.tubeline',
		'app.trainstation',
		'app.country',
		'app.property',
		'app.propertytype',
		'app.locality',
		'app.property_photo',
		'app.property_floorplan',
		'app.property_room',
		'app.property_room_photo',
		'app.user_viewing_schdule',
		'app.viewing',
		'app.bathroomtype',
		'app.property_rooms_to_bathroomtype',
		'app.bedroomclass',
		'app.property_rooms_to_bedroomclass',
		'app.feature',
		'app.property_rooms_to_feature',
		'app.property_video',
		'app.properties_to_feature',
		'app.state',
		'app.user_partner'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BookRoom = ClassRegistry::init('BookRoom');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BookRoom);

		parent::tearDown();
	}

}
