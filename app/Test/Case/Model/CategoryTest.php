<?php
App::uses('Category', 'Model');

/**
 * Category Test Case
 *
 */
class CategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.category',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.coffee_shop',
		'app.country',
		'app.state',
		'app.coffee_shop_image',
		'app.attribute',
		'app.attributes_to_menu',
		'app.menu',
		'app.order_detail',
		'app.categories_to_menu',
		'app.order',
		'app.timing',
		'app.timing_detail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Category = ClassRegistry::init('Category');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Category);

		parent::tearDown();
	}

}
