<?php
App::uses('ClientStore', 'Model');

/**
 * ClientStore Test Case
 *
 */
class ClientStoreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client_store',
		'app.user',
		'app.role',
		'app.client_setting',
		'app.store_group',
		'app.audit_information',
		'app.audit_request',
		'app.audit',
		'app.audit_to_audit_question',
		'app.audit_video',
		'app.audit_question_answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClientStore = ClassRegistry::init('ClientStore');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClientStore);

		parent::tearDown();
	}

}
