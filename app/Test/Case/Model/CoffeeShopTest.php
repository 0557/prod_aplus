<?php
App::uses('CoffeeShop', 'Model');

/**
 * CoffeeShop Test Case
 *
 */
class CoffeeShopTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coffee_shop',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.country',
		'app.state',
		'app.coffee_shop_image',
		'app.menu',
		'app.order'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CoffeeShop = ClassRegistry::init('CoffeeShop');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CoffeeShop);

		parent::tearDown();
	}

}
