<?php
App::uses('Company', 'Model');

/**
 * Company Test Case
 *
 */
class CompanyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.company',
		'app.archetype',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.property',
		'app.property_photo',
		'app.property_asbesto',
		'app.property_room',
		'app.property_asbesto_photo',
		'app.property_asbesto_survey',
		'app.remedial_work',
		'app.asbestos_fibre_type',
		'app.properties_asbesto_to_fibre_type',
		'app.property_floorplan',
		'app.properties_to_archetype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Company = ClassRegistry::init('Company');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Company);

		parent::tearDown();
	}

}
