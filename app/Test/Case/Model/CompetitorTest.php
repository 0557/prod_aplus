<?php
App::uses('Competitor', 'Model');

/**
 * Competitor Test Case
 */
class CompetitorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.competitor',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail',
		'app.vacinationtodetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Competitor = ClassRegistry::init('Competitor');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Competitor);

		parent::tearDown();
	}

}
