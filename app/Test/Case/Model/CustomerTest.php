<?php
App::uses('Customer', 'Model');

/**
 * Customer Test Case
 */
class CustomerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.customer',
		'app.store',
		'app.corporation',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail',
		'app.vacinationtodetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Customer = ClassRegistry::init('Customer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Customer);

		parent::tearDown();
	}

}
