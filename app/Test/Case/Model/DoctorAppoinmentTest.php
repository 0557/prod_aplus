<?php
App::uses('DoctorAppoinment', 'Model');

/**
 * DoctorAppoinment Test Case
 *
 */
class DoctorAppoinmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doctor_appoinment',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.hospital_category',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail',
		'app.disease',
		'app.disease_precaution',
		'app.vacinationdetail_to_disease',
		'app.userlog',
		'app.doctor_hospital',
		'app.week'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DoctorAppoinment = ClassRegistry::init('DoctorAppoinment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DoctorAppoinment);

		parent::tearDown();
	}

}
