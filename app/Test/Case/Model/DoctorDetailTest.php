<?php
App::uses('DoctorDetail', 'Model');

/**
 * DoctorDetail Test Case
 *
 */
class DoctorDetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doctor_detail',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.hospital_category',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail',
		'app.disease',
		'app.disease_precaution',
		'app.vacinationdetail_to_disease',
		'app.userlog',
		'app.specialty'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DoctorDetail = ClassRegistry::init('DoctorDetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DoctorDetail);

		parent::tearDown();
	}

}
