<?php
App::uses('Featurearea', 'Model');

/**
 * Featurearea Test Case
 *
 */
class FeatureareaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.featurearea'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Featurearea = ClassRegistry::init('Featurearea');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Featurearea);

		parent::tearDown();
	}

}
