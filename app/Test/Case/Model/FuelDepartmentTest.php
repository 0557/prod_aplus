<?php
App::uses('FuelDepartment', 'Model');

/**
 * FuelDepartment Test Case
 */
class FuelDepartmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fuel_department',
		'app.wholesale_product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FuelDepartment = ClassRegistry::init('FuelDepartment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FuelDepartment);

		parent::tearDown();
	}

}
