<?php
App::uses('FuelInvoice', 'Model');

/**
 * FuelInvoice Test Case
 */
class FuelInvoiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fuel_invoice'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FuelInvoice = ClassRegistry::init('FuelInvoice');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FuelInvoice);

		parent::tearDown();
	}

}
