<?php
App::uses('FuelProduct', 'Model');

/**
 * FuelProduct Test Case
 */
class FuelProductTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fuel_product',
		'app.fuel_purchase_invoice',
		'app.fuel_sale_invoice',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FuelProduct = ClassRegistry::init('FuelProduct');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FuelProduct);

		parent::tearDown();
	}

}
