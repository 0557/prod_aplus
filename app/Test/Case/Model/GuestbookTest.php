<?php
App::uses('Guestbook', 'Model');

/**
 * Guestbook Test Case
 *
 */
class GuestbookTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.guestbook',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.package',
		'app.manager',
		'app.property',
		'app.country',
		'app.state',
		'app.payment_history',
		'app.property_availability',
		'app.property_feature',
		'app.property_other_rate',
		'app.property_photo',
		'app.reservation_enquiry',
		'app.shortlist'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Guestbook = ClassRegistry::init('Guestbook');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Guestbook);

		parent::tearDown();
	}

}
