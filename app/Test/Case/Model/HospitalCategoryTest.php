<?php
App::uses('HospitalCategory', 'Model');

/**
 * HospitalCategory Test Case
 *
 */
class HospitalCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.hospital_category',
		'app.hospital'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->HospitalCategory = ClassRegistry::init('HospitalCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HospitalCategory);

		parent::tearDown();
	}

}
