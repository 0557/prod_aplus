<?php
App::uses('Hospital', 'Model');

/**
 * Hospital Test Case
 *
 */
class HospitalTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.hospital',
		'app.hospital_category',
		'app.country',
		'app.city',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.disease',
		'app.disease_precaution',
		'app.vacinationdetail_to_disease',
		'app.userlog',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Hospital = ClassRegistry::init('Hospital');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Hospital);

		parent::tearDown();
	}

}
