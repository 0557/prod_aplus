<?php
App::uses('IctrpResult', 'Model');

/**
 * IctrpResult Test Case
 *
 */
class IctrpResultTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ictrp_result',
		'app.ictrp_condition',
		'app.ictrp_contact',
		'app.ictrp_intervention',
		'app.ictrp_primary_outcome',
		'app.ictrp_secondary_outcome',
		'app.ictrp_secondary_sponsor'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->IctrpResult = ClassRegistry::init('IctrpResult');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->IctrpResult);

		parent::tearDown();
	}

}
