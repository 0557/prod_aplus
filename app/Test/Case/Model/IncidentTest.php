<?php
App::uses('Incident', 'Model');

/**
 * Incident Test Case
 *
 */
class IncidentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.incident',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.company',
		'app.archetype',
		'app.property_asbesto_survey',
		'app.property_room',
		'app.property_asbesto',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.asbestos_fibre_type',
		'app.properties_asbesto_to_fibre_type',
		'app.property_floorplan_tag',
		'app.property_floorplan',
		'app.property_photo',
		'app.properties_to_archetype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Incident = ClassRegistry::init('Incident');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Incident);

		parent::tearDown();
	}

}
