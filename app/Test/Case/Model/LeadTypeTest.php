<?php
App::uses('LeadType', 'Model');

/**
 * LeadType Test Case
 *
 */
class LeadTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.lead_type',
		'app.lead'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LeadType = ClassRegistry::init('LeadType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LeadType);

		parent::tearDown();
	}

}
