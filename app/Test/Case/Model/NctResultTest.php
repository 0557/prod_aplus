<?php
App::uses('NctResult', 'Model');

/**
 * NctResult Test Case
 *
 */
class NctResultTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.nct_result',
		'app.org_study',
		'app.nct',
		'app.nct_arm_group',
		'app.nct_eligibilitiy',
		'app.nct_intervention',
		'app.nct_location',
		'app.primaryoutcome',
		'app.secondaryoutcome'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NctResult = ClassRegistry::init('NctResult');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NctResult);

		parent::tearDown();
	}

}
