<?php
App::uses('NewsLetter', 'Model');

/**
 * NewsLetter Test Case
 *
 */
class NewsLetterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.news_letter'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NewsLetter = ClassRegistry::init('NewsLetter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NewsLetter);

		parent::tearDown();
	}

}
