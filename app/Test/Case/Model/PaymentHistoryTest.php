<?php
App::uses('PaymentHistory', 'Model');

/**
 * PaymentHistory Test Case
 *
 */
class PaymentHistoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payment_history',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.package',
		'app.manager',
		'app.country',
		'app.state',
		'app.property',
		'app.currency',
		'app.property_photos',
		'app.guestbook',
		'app.property_availability',
		'app.property_other_rate',
		'app.property_photo',
		'app.reservation_enquiry',
		'app.reservation_enquiry_message',
		'app.shortlist',
		'app.feature',
		'app.properties_to_feature'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PaymentHistory = ClassRegistry::init('PaymentHistory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PaymentHistory);

		parent::tearDown();
	}

}
