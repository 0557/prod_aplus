<?php
App::uses('PropertyAsbestoSurvey', 'Model');

/**
 * PropertyAsbestoSurvey Test Case
 *
 */
class PropertyAsbestoSurveyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_asbesto_survey',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.property_asbestos_survey',
		'app.property_asbesto',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.property_floorplan',
		'app.property_photo',
		'app.property_room'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyAsbestoSurvey = ClassRegistry::init('PropertyAsbestoSurvey');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyAsbestoSurvey);

		parent::tearDown();
	}

}
