<?php
App::uses('PropertyAsbesto', 'Model');

/**
 * PropertyAsbesto Test Case
 *
 */
class PropertyAsbestoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_asbesto',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.company',
		'app.archetype',
		'app.incident',
		'app.property_asbesto_survey',
		'app.property_asbesto_survey_to_property_asbesto',
		'app.property_room',
		'app.property_floorplan_tag',
		'app.property_floorplan',
		'app.property_asbesto_survey_to_property_room',
		'app.remedial_work',
		'app.property_photo',
		'app.properties_to_archetype',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_photo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyAsbesto = ClassRegistry::init('PropertyAsbesto');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyAsbesto);

		parent::tearDown();
	}

}
