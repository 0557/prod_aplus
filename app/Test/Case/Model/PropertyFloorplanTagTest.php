<?php
App::uses('PropertyFloorplanTag', 'Model');

/**
 * PropertyFloorplanTag Test Case
 *
 */
class PropertyFloorplanTagTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_floorplan_tag',
		'app.property_room',
		'app.property_floorplan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyFloorplanTag = ClassRegistry::init('PropertyFloorplanTag');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyFloorplanTag);

		parent::tearDown();
	}

}
