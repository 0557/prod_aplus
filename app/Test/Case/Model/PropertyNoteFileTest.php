<?php
App::uses('PropertyNoteFile', 'Model');

/**
 * PropertyNoteFile Test Case
 *
 */
class PropertyNoteFileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_note_file',
		'app.property_note'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyNoteFile = ClassRegistry::init('PropertyNoteFile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyNoteFile);

		parent::tearDown();
	}

}
