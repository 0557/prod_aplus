<?php
App::uses('PropertyNote', 'Model');

/**
 * PropertyNote Test Case
 *
 */
class PropertyNoteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_note',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.company',
		'app.archetype',
		'app.incident',
		'app.property_asbesto',
		'app.property_room',
		'app.property_floorplan_tag',
		'app.property_floorplan',
		'app.property_asbesto_survey',
		'app.property_asbesto_survey_to_property_room',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.asbestos_fibre_type',
		'app.properties_asbesto_to_fibre_type',
		'app.user_property',
		'app.property_photo',
		'app.properties_to_archetype',
		'app.property_note_file'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyNote = ClassRegistry::init('PropertyNote');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyNote);

		parent::tearDown();
	}

}
