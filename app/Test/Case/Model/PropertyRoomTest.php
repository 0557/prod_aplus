<?php
App::uses('PropertyRoom', 'Model');

/**
 * PropertyRoom Test Case
 *
 */
class PropertyRoomTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_room',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.company',
		'app.archetype',
		'app.property_asbesto_survey',
		'app.property_asbesto',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.asbestos_fibre_type',
		'app.properties_asbesto_to_fibre_type',
		'app.property_photo',
		'app.property_floorplan',
		'app.properties_to_archetype',
		'app.property_floorplan_tag'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyRoom = ClassRegistry::init('PropertyRoom');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyRoom);

		parent::tearDown();
	}

}
