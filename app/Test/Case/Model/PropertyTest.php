<?php
App::uses('Property', 'Model');

/**
 * Property Test Case
 *
 */
class PropertyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.property_asbestos_survey',
		'app.property_asbesto_survey',
		'app.property_asbesto',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.property_floorplan',
		'app.property_photo',
		'app.property_room'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Property = ClassRegistry::init('Property');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Property);

		parent::tearDown();
	}

}
