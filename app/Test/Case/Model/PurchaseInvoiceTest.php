<?php
App::uses('PurchaseInvoice', 'Model');

/**
 * PurchaseInvoice Test Case
 */
class PurchaseInvoiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.purchase_invoice'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PurchaseInvoice = ClassRegistry::init('PurchaseInvoice');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PurchaseInvoice);

		parent::tearDown();
	}

}
