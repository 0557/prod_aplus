<?php
App::uses('RemedialWork', 'Model');

/**
 * RemedialWork Test Case
 *
 */
class RemedialWorkTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.remedial_work',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.property_asbestos_survey',
		'app.property_asbesto',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_photo',
		'app.property_asbesto_survey',
		'app.property_floorplan',
		'app.property_photo',
		'app.property_room'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RemedialWork = ClassRegistry::init('RemedialWork');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RemedialWork);

		parent::tearDown();
	}

}
