<?php
App::uses('ReservationEnquiry', 'Model');

/**
 * ReservationEnquiry Test Case
 *
 */
class ReservationEnquiryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.reservation_enquiry',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.package',
		'app.manager',
		'app.country',
		'app.state',
		'app.guestbook',
		'app.payment_history',
		'app.property_availability',
		'app.property_feature',
		'app.property_other_rate',
		'app.property_photo',
		'app.shortlist',
		'app.reservation_enquiry_message'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ReservationEnquiry = ClassRegistry::init('ReservationEnquiry');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ReservationEnquiry);

		parent::tearDown();
	}

}
