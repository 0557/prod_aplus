<?php
App::uses('SalesInvoice', 'Model');

/**
 * SalesInvoice Test Case
 */
class SalesInvoiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sales_invoice',
		'app.supplier',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SalesInvoice = ClassRegistry::init('SalesInvoice');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SalesInvoice);

		parent::tearDown();
	}

}
