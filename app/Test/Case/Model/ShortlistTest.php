<?php
App::uses('Shortlist', 'Model');

/**
 * Shortlist Test Case
 *
 */
class ShortlistTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.shortlist',
		'app.property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.package',
		'app.manager',
		'app.country',
		'app.state',
		'app.guestbook',
		'app.payment_history',
		'app.property_availability',
		'app.property_feature',
		'app.property_other_rate',
		'app.property_photo',
		'app.reservation_enquiry',
		'app.reservation_enquiry_message'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Shortlist = ClassRegistry::init('Shortlist');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Shortlist);

		parent::tearDown();
	}

}
