<?php
App::uses('SitePermission', 'Model');

/**
 * SitePermission Test Case
 *
 */
class SitePermissionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.site_permission',
		'app.role',
		'app.user',
		'app.package',
		'app.manager'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SitePermission = ClassRegistry::init('SitePermission');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SitePermission);

		parent::tearDown();
	}

}
