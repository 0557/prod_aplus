<?php
App::uses('Specification', 'Model');

/**
 * Specification Test Case
 *
 */
class SpecificationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.specification'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Specification = ClassRegistry::init('Specification');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Specification);

		parent::tearDown();
	}

}
