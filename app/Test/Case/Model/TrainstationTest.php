<?php
App::uses('Trainstation', 'Model');

/**
 * Trainstation Test Case
 *
 */
class TrainstationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.trainstation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Trainstation = ClassRegistry::init('Trainstation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Trainstation);

		parent::tearDown();
	}

}
