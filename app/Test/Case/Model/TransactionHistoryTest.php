<?php
App::uses('TransactionHistory', 'Model');

/**
 * TransactionHistory Test Case
 *
 */
class TransactionHistoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.transaction_history',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.lead',
		'app.lead_type',
		'app.event'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TransactionHistory = ClassRegistry::init('TransactionHistory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TransactionHistory);

		parent::tearDown();
	}

}
