<?php
App::uses('Tubeline', 'Model');

/**
 * Tubeline Test Case
 *
 */
class TubelineTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tubeline'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tubeline = ClassRegistry::init('Tubeline');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tubeline);

		parent::tearDown();
	}

}
