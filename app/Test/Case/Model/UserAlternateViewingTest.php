<?php
App::uses('UserAlternateViewing', 'Model');

/**
 * UserAlternateViewing Test Case
 *
 */
class UserAlternateViewingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_alternate_viewing',
		'app.property',
		'app.propertytype',
		'app.trainstation',
		'app.tubeline',
		'app.property_photo',
		'app.property_video',
		'app.property_floorplan',
		'app.property_room',
		'app.property_room_photo',
		'app.user_viewing_schdule',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_viewing',
		'app.user_viewing_agegroup',
		'app.user_viewing_schdules_to_user',
		'app.amendity',
		'app.property_rooms_to_amendity',
		'app.bathroomtype',
		'app.property_rooms_to_bathroomtype',
		'app.bedroomclass',
		'app.property_rooms_to_bedroomclass',
		'app.feature',
		'app.property_rooms_to_feature',
		'app.properties_to_feature'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserAlternateViewing = ClassRegistry::init('UserAlternateViewing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserAlternateViewing);

		parent::tearDown();
	}

}
