<?php
App::uses('UserApplicationForm', 'Model');

/**
 * UserApplicationForm Test Case
 *
 */
class UserApplicationFormTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_application_form',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserApplicationForm = ClassRegistry::init('UserApplicationForm');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserApplicationForm);

		parent::tearDown();
	}

}
