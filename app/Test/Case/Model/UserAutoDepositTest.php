<?php
App::uses('UserAutoDeposit', 'Model');

/**
 * UserAutoDeposit Test Case
 *
 */
class UserAutoDepositTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_auto_deposit',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_creditcard'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserAutoDeposit = ClassRegistry::init('UserAutoDeposit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserAutoDeposit);

		parent::tearDown();
	}

}
