<?php
App::uses('UserConsultation', 'Model');

/**
 * UserConsultation Test Case
 *
 */
class UserConsultationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_consultation',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.vacinationdetail',
		'app.vacinationtodetail',
		'app.userlog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserConsultation = ClassRegistry::init('UserConsultation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserConsultation);

		parent::tearDown();
	}

}
