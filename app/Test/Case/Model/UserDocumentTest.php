<?php
App::uses('UserDocument', 'Model');

/**
 * UserDocument Test Case
 *
 */
class UserDocumentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_document',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.vacinationdetail',
		'app.vacinationtodetail',
		'app.userlog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserDocument = ClassRegistry::init('UserDocument');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserDocument);

		parent::tearDown();
	}

}
