<?php
App::uses('UserLeadProfile', 'Model');

/**
 * UserLeadProfile Test Case
 *
 */
class UserLeadProfileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_lead_profile',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.lead_type',
		'app.lead',
		'app.event',
		'app.transaction_history',
		'app.user_lead_geographic_area'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserLeadProfile = ClassRegistry::init('UserLeadProfile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserLeadProfile);

		parent::tearDown();
	}

}
