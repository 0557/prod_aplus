<?php
App::uses('UserProperty', 'Model');

/**
 * UserProperty Test Case
 *
 */
class UserPropertyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_property',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.company',
		'app.archetype',
		'app.incident',
		'app.property',
		'app.property_photo',
		'app.property_asbesto_survey',
		'app.property_asbesto',
		'app.property_room',
		'app.property_floorplan_tag',
		'app.property_floorplan',
		'app.property_asbesto_photo',
		'app.remedial_work',
		'app.asbestos_fibre_type',
		'app.properties_asbesto_to_fibre_type',
		'app.property_asbesto_survey_to_property_room',
		'app.property_note',
		'app.properties_to_archetype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserProperty = ClassRegistry::init('UserProperty');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserProperty);

		parent::tearDown();
	}

}
