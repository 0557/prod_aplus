<?php
App::uses('UserViewingSchdule', 'Model');

/**
 * UserViewingSchdule Test Case
 *
 */
class UserViewingSchduleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_viewing_schdule',
		'app.user_viewing',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_viewing_agegroup',
		'app.property',
		'app.country',
		'app.state',
		'app.property_photo',
		'app.property_floorplan',
		'app.property_room',
		'app.property_room_photo',
		'app.viewing',
		'app.amendity',
		'app.property_rooms_to_amendity',
		'app.bathroomtype',
		'app.property_rooms_to_bathroomtype',
		'app.bedroomclass',
		'app.property_rooms_to_bedroomclass',
		'app.feature',
		'app.property_rooms_to_feature',
		'app.property_video',
		'app.properties_to_feature'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserViewingSchdule = ClassRegistry::init('UserViewingSchdule');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserViewingSchdule);

		parent::tearDown();
	}

}
