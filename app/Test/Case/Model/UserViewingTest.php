<?php
App::uses('UserViewing', 'Model');

/**
 * UserViewing Test Case
 *
 */
class UserViewingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_viewing',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_viewing_agegroup',
		'app.user_viewing_schdule'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserViewing = ClassRegistry::init('UserViewing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserViewing);

		parent::tearDown();
	}

}
