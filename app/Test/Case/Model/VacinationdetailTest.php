<?php
App::uses('Vacinationdetail', 'Model');

/**
 * Vacinationdetail Test Case
 *
 */
class VacinationdetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacinationdetail',
		'app.agelimit',
		'app.disease_medician',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.month',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.vacinationtodetail',
		'app.vacinationrequiredetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Vacinationdetail = ClassRegistry::init('Vacinationdetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Vacinationdetail);

		parent::tearDown();
	}

}
