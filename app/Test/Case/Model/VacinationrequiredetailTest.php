<?php
App::uses('Vacinationrequiredetail', 'Model');

/**
 * Vacinationrequiredetail Test Case
 *
 */
class VacinationrequiredetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacinationrequiredetail',
		'app.agelimit',
		'app.disease_medician',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.vacinationdetail',
		'app.month',
		'app.disease',
		'app.disease_precaution',
		'app.vacinationdetail_to_disease',
		'app.vacinationtodetail',
		'app.userlog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Vacinationrequiredetail = ClassRegistry::init('Vacinationrequiredetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Vacinationrequiredetail);

		parent::tearDown();
	}

}
