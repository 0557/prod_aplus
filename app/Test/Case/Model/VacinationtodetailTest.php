<?php
App::uses('Vacinationtodetail', 'Model');

/**
 * Vacinationtodetail Test Case
 *
 */
class VacinationtodetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacinationtodetail',
		'app.country',
		'app.city',
		'app.state',
		'app.hospital',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.agelimit',
		'app.disease_medician',
		'app.vacinationdetail',
		'app.month',
		'app.vacinationrequiredetail',
		'app.userlog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Vacinationtodetail = ClassRegistry::init('Vacinationtodetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Vacinationtodetail);

		parent::tearDown();
	}

}
