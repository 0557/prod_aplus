<?php
App::uses('Viewing', 'Model');

/**
 * Viewing Test Case
 *
 */
class ViewingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.viewing',
		'app.property',
		'app.propertytype',
		'app.trainstation',
		'app.tubeline',
		'app.locality',
		'app.property_photo',
		'app.property_floorplan',
		'app.property_room',
		'app.property_room_photo',
		'app.user_viewing_schdule',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.user_viewing',
		'app.user_viewing_agegroup',
		'app.user_viewing_schdules_to_user',
		'app.bathroomtype',
		'app.property_rooms_to_bathroomtype',
		'app.bedroomclass',
		'app.property_rooms_to_bedroomclass',
		'app.feature',
		'app.property_rooms_to_feature',
		'app.property_video',
		'app.properties_to_feature'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Viewing = ClassRegistry::init('Viewing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Viewing);

		parent::tearDown();
	}

}
