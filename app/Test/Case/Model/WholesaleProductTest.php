<?php
App::uses('WholesaleProduct', 'Model');

/**
 * WholesaleProduct Test Case
 */
class WholesaleProductTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wholesale_product',
		'app.fuel_department',
		'app.customer',
		'app.corporation',
		'app.country',
		'app.city',
		'app.hospital',
		'app.state',
		'app.user',
		'app.role',
		'app.site_permission',
		'app.manager',
		'app.userlog',
		'app.vacinationdetail',
		'app.vacinationtodetail',
		'app.store'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WholesaleProduct = ClassRegistry::init('WholesaleProduct');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WholesaleProduct);

		parent::tearDown();
	}

}
