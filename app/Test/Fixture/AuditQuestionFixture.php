<?php
/**
 * AuditQuestionFixture
 *
 */
class AuditQuestionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'audit_department_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'comment' => 'this is primary key of users tables which roles is clients'),
		'question_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'questions' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'setorder' => array('type' => 'integer', 'null' => false, 'default' => null),
		'weight' => array('type' => 'integer', 'null' => false, 'default' => null),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'audit_departments_fk' => array('column' => 'audit_department_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'audit_department_id' => 1,
			'user_id' => 1,
			'question_type' => 'Lorem ipsum dolor sit amet',
			'questions' => 'Lorem ipsum dolor sit amet',
			'setorder' => 1,
			'weight' => 1,
			'status' => 1,
			'created' => '2014-05-01 13:45:45',
			'modified' => '2014-05-01 13:45:45'
		),
	);

}
