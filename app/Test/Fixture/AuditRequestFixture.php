<?php
/**
 * AuditRequestFixture
 *
 */
class AuditRequestFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'audit_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'client_store_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'clientid' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key of users table those roles is Client'),
		'auditorid' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key of users table those role is as Auditor'),
		'reviewerid' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key of users table those roles is reviewers'),
		'comments' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'clientid' => array('column' => 'clientid', 'unique' => 0),
			'audit_id' => array('column' => 'audit_id', 'unique' => 0),
			'auditorid' => array('column' => 'auditorid', 'unique' => 0),
			'reviewerid' => array('column' => 'reviewerid', 'unique' => 0),
			'client_store_id' => array('column' => 'client_store_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'audit_id' => 1,
			'client_store_id' => 1,
			'clientid' => 1,
			'auditorid' => 1,
			'reviewerid' => 1,
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2014-04-18 18:01:38',
			'modified' => '2014-04-18 18:01:38'
		),
	);

}
