<?php
/**
 * BookRoomFixture
 *
 */
class BookRoomFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'property_room_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'tenant_type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'payment_in_installments' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'request_move_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'request_move_time' => array('type' => 'integer', 'null' => false, 'default' => null),
		'duration_of_stay' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'alternative_email' => array('type' => 'integer', 'null' => false, 'default' => null),
		'alternative_phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'dob' => array('type' => 'date', 'null' => false, 'default' => null),
		'insurance_number' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'nationality' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'short_description' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'monthly_salary' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => 10),
		'job_title' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'company_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_relationship' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_address' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'need_cleaning' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'cleaning_time' => array('type' => 'time', 'null' => false, 'default' => '00:00:00'),
		'declaration' => array('type' => 'integer', 'null' => false, 'default' => null),
		'questions' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'property_id' => 1,
			'property_room_id' => 1,
			'tenant_type' => 'Lorem ipsum dolor sit amet',
			'payment_in_installments' => 1,
			'request_move_date' => '2013-07-08',
			'request_move_time' => 1,
			'duration_of_stay' => 1,
			'alternative_email' => 1,
			'alternative_phone' => 'Lorem ipsum dolor sit amet',
			'dob' => '2013-07-08',
			'insurance_number' => 1,
			'nationality' => 'Lorem ipsum dolor sit amet',
			'short_description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'monthly_salary' => 1,
			'job_title' => 'Lorem ipsum dolor sit amet',
			'company_name' => 'Lorem ipsum dolor sit amet',
			'emergency_name' => 'Lorem ipsum dolor sit amet',
			'emergency_relationship' => 'Lorem ipsum dolor sit amet',
			'emergency_phone' => 'Lorem ipsum dolor ',
			'emergency_address' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'landlord_name' => 'Lorem ipsum dolor sit amet',
			'landlord_phone' => 'Lorem ipsum dolor ',
			'landlord_email' => 'Lorem ipsum dolor sit amet',
			'need_cleaning' => 1,
			'cleaning_time' => '17:26:30',
			'declaration' => 1,
			'questions' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'status' => 1,
			'created' => '2013-07-08 17:26:30',
			'modified' => '2013-07-08 17:26:30'
		),
	);

}
