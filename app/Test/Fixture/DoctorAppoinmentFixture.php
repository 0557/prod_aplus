<?php
/**
 * DoctorAppoinmentFixture
 *
 */
class DoctorAppoinmentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'doctorid' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'doctor_hospital_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'week_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'date' => array('type' => 'date', 'null' => false, 'default' => null),
		'timing' => array('type' => 'time', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'doctorid' => 1,
			'user_id' => 1,
			'doctor_hospital_id' => 1,
			'week_id' => 1,
			'date' => '2015-07-27',
			'timing' => '11:10:39',
			'created' => '2015-07-27 11:10:39',
			'modified' => '2015-07-27 11:10:39'
		),
	);

}
