<?php
/**
 * FuelInvoice Fixture
 */
class FuelInvoiceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'bol' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'consignee' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'receving_data' => array('type' => 'date', 'null' => false, 'default' => null),
		'load_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'supplier' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'terminal' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'carrier' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'driver' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'po' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'export_supplier' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'export_terminal' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'purachase_order_number' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'comments' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'files' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'bol' => 1,
			'consignee' => 1,
			'receving_data' => '2015-09-28',
			'load_date' => '2015-09-28',
			'supplier' => 'Lorem ipsum dolor sit amet',
			'terminal' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'carrier' => 'Lorem ipsum dolor sit amet',
			'driver' => 1,
			'po' => 1,
			'id' => 1,
			'export_supplier' => 'Lorem ipsum dolor sit amet',
			'export_terminal' => 'Lorem ipsum dolor sit amet',
			'purachase_order_number' => 1,
			'status' => 1,
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'files' => 'Lorem ipsum dolor sit amet'
		),
	);

}
