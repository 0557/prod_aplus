<?php
/**
 * FuelProduct Fixture
 */
class FuelProductFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'fuel_purchase_invoice_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'fuel_sale_invoice_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'product_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'gallons_delivered' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cost_per_gallon' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'net_ammount' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'fuel_purchase_invoice_id' => 1,
			'fuel_sale_invoice_id' => 1,
			'product_id' => 1,
			'type' => 'Lorem ipsum dolor sit amet',
			'gallons_delivered' => 'Lorem ipsum dolor sit amet',
			'cost_per_gallon' => 1,
			'net_ammount' => 1,
			'created' => '2015-10-10 11:25:56'
		),
	);

}
