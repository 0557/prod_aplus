<?php
/**
 * IctrpResultFixture
 *
 */
class IctrpResultFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'TrialID' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Last_Refreshed_on' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Public_title' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Scientific_title' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Acronym' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Primary_sponsor' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Date_registration' => array('type' => 'date', 'null' => false, 'default' => null),
		'Study_type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Source_Register' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'web_address' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Recruitment_Status' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_records' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Inclusion_agemin' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Inclusion_agemax' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Inclusion_gender' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Date_enrollement' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Target_size' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'Phase' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Countries' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Inclusion_Criteria' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Exclusion_Criteria' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Secondary_ID' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Source_Support' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'TrialID' => 'Lorem ipsum dolor sit amet',
			'Last_Refreshed_on' => 'Lorem ipsum dolor sit amet',
			'Public_title' => 'Lorem ipsum dolor sit amet',
			'Scientific_title' => 'Lorem ipsum dolor sit amet',
			'Acronym' => 'Lorem ipsum dolor sit amet',
			'Primary_sponsor' => 'Lorem ipsum dolor sit amet',
			'Date_registration' => '2015-09-16',
			'Study_type' => 'Lorem ipsum dolor sit amet',
			'Source_Register' => 'Lorem ipsum dolor sit amet',
			'web_address' => 'Lorem ipsum dolor sit amet',
			'Recruitment_Status' => 'Lorem ipsum dolor sit amet',
			'other_records' => 'Lorem ipsum dolor sit amet',
			'Inclusion_agemin' => 'Lorem ipsum dolor sit amet',
			'Inclusion_agemax' => 'Lorem ipsum dolor sit amet',
			'Inclusion_gender' => 'Lorem ipsum dolor sit amet',
			'Date_enrollement' => 'Lorem ipsum dolor sit amet',
			'Target_size' => 1,
			'Phase' => 'Lorem ipsum dolor sit amet',
			'Countries' => 'Lorem ipsum dolor sit amet',
			'Inclusion_Criteria' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'Exclusion_Criteria' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'Secondary_ID' => 'Lorem ipsum dolor sit amet',
			'Source_Support' => 'Lorem ipsum dolor sit amet'
		),
	);

}
