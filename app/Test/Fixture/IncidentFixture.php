<?php
/**
 * IncidentFixture
 *
 */
class IncidentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key for property table'),
		'company_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key for companies table'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is client id and primary key for users table'),
		'property_asbesto_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary key for property asbestos table'),
		'consultantid' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is consultant unique id and primary key for users table'),
		'date_occured' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'date_reported' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'incidient_summary' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'image' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'exposure' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'riddor_reportable' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'individuals_involved' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'individuals_notified_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'airborne_fibre_level' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2', 'comment' => '(number e.g. 0.01) label \'f/cm�\''),
		'fibre_level_assessment_detail' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '0=>Pending, 1=>Approved'),
		'approvedby' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is for consulant user and priamry key for users table'),
		'approved_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'property_id' => array('column' => 'property_id', 'unique' => 0),
			'company_id' => array('column' => 'company_id', 'unique' => 0),
			'user_id' => array('column' => 'user_id', 'unique' => 0),
			'property_asbesto_id' => array('column' => 'property_asbesto_id', 'unique' => 0),
			'consultantid' => array('column' => 'consultantid', 'unique' => 0),
			'approvedby' => array('column' => 'approvedby', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_id' => 1,
			'company_id' => 1,
			'user_id' => 1,
			'property_asbesto_id' => 1,
			'consultantid' => 1,
			'date_occured' => '2013-10-19 11:06:47',
			'date_reported' => '2013-10-19 11:06:47',
			'incidient_summary' => 'Lorem ipsum dolor sit amet',
			'image' => 'Lorem ipsum dolor sit amet',
			'exposure' => 1,
			'riddor_reportable' => 1,
			'individuals_involved' => 'Lorem ipsum dolor sit amet',
			'individuals_notified_date' => '2013-10-19',
			'airborne_fibre_level' => 1,
			'fibre_level_assessment_detail' => 'Lorem ipsum dolor sit amet',
			'status' => 1,
			'approvedby' => 1,
			'approved_date' => '2013-10-19',
			'created' => '2013-10-19 11:06:47',
			'modified' => '2013-10-19 11:06:47'
		),
	);

}
