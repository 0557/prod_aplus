<?php
/**
 * PropertyAsbestoSurveyFixture
 *
 */
class PropertyAsbestoSurveyFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary id for properties table'),
		'property_asbesto_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary id for property_asbestos table'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'this is primary id for user table and used for SURVEYOR details'),
		'survey_reference' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'survey_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'property_asbesto_surveys_properties_asbestos_fk' => array('column' => 'property_asbesto_id', 'unique' => 0),
			'property_asbesto_surveys_properties_fk' => array('column' => 'property_id', 'unique' => 0),
			'property_asbesto_surveys_users_fk' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_id' => 1,
			'property_asbesto_id' => 1,
			'user_id' => 1,
			'survey_reference' => 'Lorem ipsum dolor sit amet',
			'survey_date' => '2013-09-27',
			'status' => 1,
			'created' => '2013-09-27 15:39:55',
			'modified' => '2013-09-27 15:39:55'
		),
	);

}
