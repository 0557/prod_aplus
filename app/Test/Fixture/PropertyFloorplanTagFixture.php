<?php
/**
 * PropertyFloorplanTagFixture
 *
 */
class PropertyFloorplanTagFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'property_room_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'property_floorplan_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'width' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3),
		'height' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3),
		'top_pos' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3),
		'left' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_room_id' => 1,
			'property_floorplan_id' => 1,
			'width' => 1,
			'height' => 1,
			'top_pos' => 1,
			'left' => 1,
			'created' => '2013-10-08 16:27:38',
			'modified' => '2013-10-08 16:27:38'
		),
	);

}
