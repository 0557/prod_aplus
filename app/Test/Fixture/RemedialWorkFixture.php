<?php
/**
 * RemedialWorkFixture
 *
 */
class RemedialWorkFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'property_asbesto_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'contractor_company_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'analyst_company_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'contractor_supervisor_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'analyst_staff_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'project_manager_company_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'remedial_works_start date' => array('type' => 'date', 'null' => false, 'default' => null),
		'remedial_works_completion_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'air_monitoring_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'waste_consignment_note' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'air_monitoring_certificates' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'project_manager_completion_doc' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'quotation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'plan_of_work' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'notification' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'remedial_works_properties_fk' => array('column' => 'property_id', 'unique' => 0),
			'remedial_works_property_asbestos_fk' => array('column' => 'property_asbesto_id', 'unique' => 0),
			'remedial_works_users_fk' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_id' => 1,
			'property_asbesto_id' => 1,
			'user_id' => 1,
			'contractor_company_name' => 'Lorem ipsum dolor sit amet',
			'analyst_company_name' => 'Lorem ipsum dolor sit amet',
			'contractor_supervisor_name' => 'Lorem ipsum dolor sit amet',
			'analyst_staff_name' => 'Lorem ipsum dolor sit amet',
			'project_manager_company_name' => 'Lorem ipsum dolor sit amet',
			'remedial_works_start date' => '2013-09-27',
			'remedial_works_completion_date' => '2013-09-27',
			'air_monitoring_type' => 'Lorem ipsum dolor sit amet',
			'waste_consignment_note' => 'Lorem ipsum dolor sit amet',
			'air_monitoring_certificates' => 'Lorem ipsum dolor sit amet',
			'project_manager_completion_doc' => 'Lorem ipsum dolor sit amet',
			'quotation' => 'Lorem ipsum dolor sit amet',
			'plan_of_work' => 'Lorem ipsum dolor sit amet',
			'notification' => 'Lorem ipsum dolor sit amet',
			'created' => '2013-09-27 15:41:28',
			'modified' => '2013-09-27 15:41:28'
		),
	);

}
