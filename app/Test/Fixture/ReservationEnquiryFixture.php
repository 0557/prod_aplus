<?php
/**
 * ReservationEnquiryFixture
 *
 */
class ReservationEnquiryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'enquiry_to' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'Enquiry to contain the unique id for the owner(User table unique id)'),
		'enquiry_from' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index', 'comment' => 'Enquiry from contain the unique id for the traveller(User table unique id)'),
		'check_in' => array('type' => 'date', 'null' => false, 'default' => null),
		'check_out' => array('type' => 'date', 'null' => false, 'default' => null),
		'no_of_adults' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'no_of_childrens' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'message' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'attach_contract' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1=>Contact is attached by the owner, 0=>contract is not attached'),
		'total_amount_pay' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2', 'comment' => 'this amount is set by the owner for intial deposit payment'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'property_id' => array('column' => array('property_id', 'enquiry_to', 'enquiry_from'), 'unique' => 0),
			'enquiry_to' => array('column' => 'enquiry_to', 'unique' => 0),
			'enquiry_from' => array('column' => 'enquiry_from', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_id' => 1,
			'enquiry_to' => 1,
			'enquiry_from' => 1,
			'check_in' => '2013-03-22',
			'check_out' => '2013-03-22',
			'no_of_adults' => 1,
			'no_of_childrens' => 1,
			'message' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'attach_contract' => 1,
			'total_amount_pay' => 1,
			'created' => '2013-03-22 16:58:46',
			'modified' => '2013-03-22 16:58:46'
		),
	);

}
