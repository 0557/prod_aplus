<?php
/**
 * SalesInvoice Fixture
 */
class SalesInvoiceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'bol' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'po' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'purchase_order_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'load_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'receving_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'supplier_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'terminal' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'export_suplier' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'export_terminal' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'carrier' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'driver' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'invoice' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'product_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'max_qnty' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'total_invoice' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mop' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'comments' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'store_bill' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'customer_bill' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'store_ship' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'customer_ship' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'invoice_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'ship_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'terms' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'due_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'bol' => 1,
			'po' => 1,
			'purchase_order_no' => 1,
			'load_date' => '2015-10-06 10:45:16',
			'receving_date' => '2015-10-06 10:45:16',
			'supplier_id' => 1,
			'terminal' => 'Lorem ipsum dolor sit amet',
			'export_suplier' => 'Lorem ipsum dolor sit amet',
			'export_terminal' => 'Lorem ipsum dolor sit amet',
			'carrier' => 'Lorem ipsum dolor sit amet',
			'driver' => 'Lorem ipsum dolor sit amet',
			'invoice' => 'Lorem ipsum dolor sit amet',
			'product_id' => 'Lorem ipsum dolor sit amet',
			'max_qnty' => 'Lorem ipsum dolor sit amet',
			'total_invoice' => 'Lorem ipsum dolor sit amet',
			'mop' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit amet',
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => 1444128316,
			'store_bill' => 'Lorem ipsum dolor sit amet',
			'customer_bill' => 'Lorem ipsum dolor sit amet',
			'store_ship' => 'Lorem ipsum dolor sit amet',
			'customer_ship' => 'Lorem ipsum dolor sit amet',
			'invoice_date' => '2015-10-06 10:45:16',
			'ship_date' => '2015-10-06 10:45:16',
			'terms' => 'Lorem ipsum dolor sit amet',
			'due_date' => '2015-10-06 10:45:16'
		),
	);

}
