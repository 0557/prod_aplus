<?php
/**
 * SitePermissionFixture
 *
 */
class SitePermissionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'package_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'manager_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'is_read' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'is_add' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'is_edit' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'is_delete' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'role_id' => 1,
			'package_id' => 1,
			'manager_id' => 1,
			'is_read' => 1,
			'is_add' => 1,
			'is_edit' => 1,
			'is_delete' => 1,
			'created' => '2013-02-18 13:52:07',
			'modified' => '2013-02-18 13:52:07'
		),
	);

}
