<?php
/**
 * UserApplicationFormFixture
 *
 */
class UserApplicationFormFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'view_the_room' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1=>Yes, 0=>No'),
		'view_explain' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => 'if view_the_room is 0 then this field is used', 'charset' => 'latin1'),
		'date_of_birth' => array('type' => 'date', 'null' => false, 'default' => null),
		'current_address' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'living_time' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'employment_company' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'employment_job' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'employment_salary' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 70, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'employer_postcode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_relationship' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'emergency_email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'reference' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 70, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'landlord_rental_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2'),
		'need_cleaning' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'declaration' => array('type' => 'integer', 'null' => false, 'default' => null),
		'moving_in' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'comment' => 'this fields is used to calculate the deposit amount according to the user provided details'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'view_the_room' => 1,
			'view_explain' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'date_of_birth' => '2013-06-10',
			'current_address' => 'Lorem ipsum dolor sit amet',
			'living_time' => 'Lorem ipsum dolor sit amet',
			'employment_company' => 'Lorem ipsum dolor sit amet',
			'employment_job' => 'Lorem ipsum dolor sit amet',
			'employment_salary' => 'Lorem ipsum dolor sit amet',
			'employer_postcode' => 'Lorem ipsum dolor ',
			'emergency_name' => 'Lorem ipsum dolor sit amet',
			'emergency_relationship' => 'Lorem ipsum dolor sit amet',
			'emergency_phone' => 'Lorem ipsum dolor ',
			'emergency_email' => 'Lorem ipsum dolor sit amet',
			'reference' => 'Lorem ipsum dolor sit amet',
			'landlord_name' => 'Lorem ipsum dolor sit amet',
			'landlord_email' => 'Lorem ipsum dolor sit amet',
			'landlord_phone' => 'Lorem ipsum dolor ',
			'landlord_rental_amount' => 1,
			'need_cleaning' => 1,
			'declaration' => 1,
			'moving_in' => 1,
			'created' => '2013-06-10 12:06:47',
			'modified' => '2013-06-10 12:06:47'
		),
	);

}
