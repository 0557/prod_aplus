<?php
/**
 * UserAutoDepositFixture
 *
 */
class UserAutoDepositFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'user_creditcard_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'deposit_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2'),
		'min_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2', 'comment' => 'this is minimum amount after that auto refil request is on'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 0),
			'user_creditcard_id' => array('column' => 'user_creditcard_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'user_creditcard_id' => 1,
			'deposit_amount' => 1,
			'min_amount' => 1,
			'created' => '2014-05-28 19:56:34',
			'modified' => '2014-05-28 19:56:34'
		),
	);

}
