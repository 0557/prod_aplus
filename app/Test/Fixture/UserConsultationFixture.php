<?php
/**
 * UserConsultationFixture
 *
 */
class UserConsultationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'doctor_name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'consultation_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'consultation_time' => array('type' => 'time', 'null' => false, 'default' => null),
		'resume' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'report' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'doctor_name' => 'Lorem ipsum dolor sit amet',
			'consultation_date' => '2015-07-28',
			'consultation_time' => '10:48:38',
			'resume' => 'Lorem ipsum dolor sit amet',
			'report' => 'Lorem ipsum dolor sit amet',
			'created' => '2015-07-28 10:48:38',
			'modified' => '2015-07-28 10:48:38'
		),
	);

}
