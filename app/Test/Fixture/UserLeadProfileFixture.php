<?php
/**
 * UserLeadProfileFixture
 *
 */
class UserLeadProfileFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'lead_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'subleadtypeid' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'lead_profile_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'daily_lead_limit' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'weekly_lead_limit' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'unlimited_lead' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'lead_email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 0),
			'lead_type_id' => array('column' => 'lead_type_id', 'unique' => 0),
			'subleadtypeid' => array('column' => 'subleadtypeid', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'lead_type_id' => 1,
			'subleadtypeid' => 1,
			'lead_profile_name' => 'Lorem ipsum dolor sit amet',
			'daily_lead_limit' => 1,
			'weekly_lead_limit' => 1,
			'unlimited_lead' => 1,
			'lead_email' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-06-18 19:35:28',
			'modified' => '2014-06-18 19:35:28'
		),
	);

}
