<?php
/**
 * UserViewingFixture
 *
 */
class UserViewingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'arrive_date' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => 'if viewing user is not from the UK then it fields use'),
		'budget' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'view_for' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'comment' => 'This isfor user who viewing the rooms is Couple, Single or Group', 'charset' => 'latin1'),
		'how_many_people' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'comment' => 'This fields is used when view_for is group'),
		'how_many_rooms' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'comment' => 'This fields is used when view_for is group'),
		'required_move_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'duration_of_stay' => array('type' => 'date', 'null' => false, 'default' => null),
		'pets' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1=>yes,0=>no'),
		'pets_details' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => 'this field is used when pets is checked as yes', 'charset' => 'latin1'),
		'dss' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '1=>yes,0=>no'),
		'comments' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_swedish_ci', 'comment' => 'status is Urgently Seeking, Seeking, Just Curious, Found a Room', 'charset' => 'latin1'),
		'how_did_you_here_about_us' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'aboutyourselfs' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'arrive_date' => '2013-06-11',
			'budget' => 'Lorem ipsum dolor sit amet',
			'view_for' => 'Lorem ipsum dolor ',
			'how_many_people' => 1,
			'how_many_rooms' => 1,
			'required_move_date' => '2013-06-11',
			'duration_of_stay' => '2013-06-11',
			'pets' => 1,
			'pets_details' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'dss' => 1,
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'status' => 'Lorem ipsum dolor sit amet',
			'how_did_you_here_about_us' => 'Lorem ipsum dolor sit amet',
			'aboutyourselfs' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2013-06-11 11:40:34',
			'modified' => '2013-06-11 11:40:34'
		),
	);

}
