<?php
/**
 * UserViewingSchduleFixture
 *
 */
class UserViewingSchduleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_viewing_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'property_room_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'date_of_viewing' => array('type' => 'date', 'null' => false, 'default' => null),
		'start_time' => array('type' => 'time', 'null' => false, 'default' => null),
		'end_time' => array('type' => 'time', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_viewing_id' => 1,
			'property_id' => 1,
			'property_room_id' => 1,
			'date_of_viewing' => '2013-06-11',
			'start_time' => '11:41:49',
			'end_time' => '11:41:49',
			'created' => '2013-06-11 11:41:49',
			'modified' => '2013-06-11 11:41:49'
		),
	);

}
