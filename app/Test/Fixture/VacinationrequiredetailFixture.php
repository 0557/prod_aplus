<?php
/**
 * VacinationrequiredetailFixture
 *
 */
class VacinationrequiredetailFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 21, 'unsigned' => false, 'key' => 'primary'),
		'agelimit_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vacinationtodetail_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vacination_date' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 60, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'agelimit_id' => 1,
			'vacinationtodetail_id' => 1,
			'vacination_date' => 'Lorem ipsum dolor sit amet'
		),
	);

}
