<?php
/**
 * WholesaleProduct Fixture
 */
class WholesaleProductFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'fuel_department_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'customer_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'description' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'price' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'vendor_price' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'tax_class' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'qty' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'min_qty' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'max_qty' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'stock_availability' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'opening_book_amount' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'tank_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'fuel_department_id' => 1,
			'customer_id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'price' => '',
			'vendor_price' => '',
			'tax_class' => 'Lorem ipsum dolor sit amet',
			'qty' => 1,
			'min_qty' => 1,
			'max_qty' => 1,
			'stock_availability' => 1,
			'opening_book_amount' => '',
			'tank_no' => 1,
			'created' => '2015-09-26 12:58:31',
			'modified' => '2015-09-26 12:58:31'
		),
	);

}
