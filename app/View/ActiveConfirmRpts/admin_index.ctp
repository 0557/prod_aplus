<div class="page-content-wrapper">
    <div class="portlet box blue">
        <div class="page-content portlet-body">       
            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Active/Confirm Games Report
                    </h3>
                </div>
            </div>
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">                          
                            <div class="confirm_heading clearfix">
                                <h5><strong>Please enter date range for this report  </strong>
                                    <span>  
                                        <input type="button" class="btn btn-warning pull-right" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">
                                    </span>
                                </h5>
                            </div>                          
                            <?php echo $this->Form->create('ActiveConfirmRpt', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                            <div class="confim_form">                              
                                <div class="row">                                
                                    <div class="col-md-3 col-xs-6">
                                        <label>Confirm date</label>                                      
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>
                                    </div>
                                   <?php /*?> <div class="col-md-2 ">
                                        <label>Status</label>
                                        <?php 
										echo $this->Form->input('status', array('options' => array('all' => 'All','active' => 'Active', 'confirm' => 'Confirm'),'empty'=>'Select Status','class' => 'form-control', 'label' => false)); 
										?>
                                    </div> <?php */?>                            
                                    <div class="col-md-4 ">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                        <label>&nbsp;</label>
                                        <a style=" margin-top:24px;" href="<?php echo Router::url('/'); ?>admin/active_confirm_rpts/reset" class="btn btn-warning ">Reset</a> 

                                    </div>

                                </div>
                            </div>
                            <?php echo $this->form->end(); ?>                          
                        </div>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable tabbable-custom tabbable-full-width">

                            <div class="tab-content">

                                <div id="tab_1_5" class="tab-pane1">

                                    <?php //echo '<pre>'; print_r($LotterySalesReport); die;		 ?>

                                    <table id="example" class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>                                              
                                                <th><?php echo $this->Paginator->sort('Game Number'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Game'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Confirm Date'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Status'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Active Date'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Ticket per Pack'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Face Value'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Net Value'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($games) && !empty($games)) { 
											   $total_facevalue=0;
											   $total_netvalue=0;                                
                                                foreach ($games as $data) {
													$total_facevalue=$total_facevalue+$data["ActiveConfirmRpt"]["face_value"];
													$total_netvalue=$total_netvalue+$data["ActiveConfirmRpt"]["net_value"];
                                                    echo '<tr>											      
                                                    <td>'.$data["ActiveConfirmRpt"]["game_no"].'</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["pack_no"].'&nbsp;</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["gamename"].'&nbsp;</td>
											<td>'.date('Y-m-d',strtotime($data["ActiveConfirmRpt"]["confirm_date_time"])).'</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["status"].'&nbsp;</td>  
									       <td>'.date('Y-m-d',strtotime($data["ActiveConfirmRpt"]["activate_date_time"])).'</td>                                                    <td>'.$data["ActiveConfirmRpt"]["ticket_value"].'&nbsp;</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["ticket_per_pack"].'&nbsp;</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["face_value"].'&nbsp;</td>
                                                    <td>'.$data["ActiveConfirmRpt"]["net_value"].'&nbsp;</td>
                                                </tr>';
                                                }
                                            } else {
                                                echo ' <tr>
                                                <td colspan="9">No results found! </td>
                                            </tr>';
                                            }
                                            ?>
                                        </tbody>
                                        
                                         <thead>
                                            <tr>
                                                <th>Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th><?php echo $total_facevalue; ?></th>
                                                <th><?php echo $total_netvalue; ?></th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                    <div class="margin-top-20">
                                        <ul class="pagination">
                                            <li>
                  <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                            </li>
                  <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                            <li> 
                  <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!--end tabbable-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <style>
            .current{
                background: rgb(238, 238, 238) none repeat scroll 0 0;
                border-color: rgb(221, 221, 221);
                color: rgb(51, 51, 51);
                border: 1px solid rgb(221, 221, 221);
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .view_file{
                width: 30px;
                border: medium none;
                padding: 10px;
                color: #FFF;		
                font-size: 13px;		
            }



        </style>

    </div>
</div>
<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
    <script>
                $(document).ready(function () {
        $('#config-demo').val('');
        });</script>
    <?php
}
?>
<script>
                    var tableToExcel = (function () {
                            var uri = 'data:application/vnd.ms-excel;base64,'
                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                                                , base64 = function (s) {
                                                return window.btoa(unescape(encodeURIComponent(s)))
                                                }
                                        , format= function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (table, name) {
                        if (!table.nodeType)
                            table = document.getElementById(table)
                        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
                        window.location.href = uri + base64(format(template, ctx))
                    }
                })()
</script>