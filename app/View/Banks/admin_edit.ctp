<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php echo $this->Form->create('Bank', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
     <div class="page-content portlet-body" >
               <div class="page-content portlet-body" >

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Bank
                    </div>
                </div></div></div>

        <div class="form-body">
            

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                 Bank Name:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php
									if(isset($edit_bank)){
								    $bank_name=$edit_bank['Bank']['name'];
									}else{
									$bank_name='';	
									}
									
									 echo $this->Form->input('name', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Bank Name','value'=>$bank_name)); ?>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                
             
          


        </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>            
            <button type="button" onclick="javascript:history.back(1)"
            class="btn default">Cancel</button>
        </div>

    </div>
    <?php echo $this->form->end(); ?>
     </div>
</div>



