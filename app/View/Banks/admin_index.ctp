<?php //print_r($Banks);
//echo $this->Session->flash();
?>
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
        
        <div class="row">
            <div class="col-md-12">
            
                <h3 class="page-title">
                      Bank<span class="btn green fileinput-button" style="margin-right:10px;">
                      <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>Add New</span>', array('controller' => 'banks','action' => 'admin_add'), array('escape' => false)); ?> 
                    </span>&nbsp;&nbsp;&nbsp;&nbsp;					
                </h3>
            </div>
        </div>
     
        <div class="row">
		       
         <?php			 
		 if($this->Session->read('search')!=""){
			 $search=$this->Session->read('search');
		 }else{
			 $search="";
		 }		 
		 ?>
           <?php echo $this->Form->create('Bank', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
		 
		 
		<div class="col-md-3" id="getplunos">
			<label>Search</label>
				<?php echo $this->Form->input('search', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'search','placeholder'=>'Filter by Name','value'=>$search)); ?>
			<br/>
		</div>
        
        
        <div class="col-md-1"  >
		  <label style="display:block;">&nbsp;</label>
		<?php echo $this->Form->input('search', array('class' => 'btn btn-success','type'=>'button', 'label' => false, 'required' => false,)); ?>
        </div>  
        <div class="col-md-1"  >
         <label style="display:block;">&nbsp;</label>
         <a href="<?php echo Router::url('/');?>admin/banks/reset" class="btn btn-warning " >Reset</a>   
		         
		</div>        
        <?php echo $this->form->end(); ?>
     
		</div>
		
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">
<div id="loading-image" style="display:none">
           <h3 style="text-align:center"> Loading...</h3>
            </div>
                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover" id="getdata">
                                    <thead>
                                        <tr>
                                            <?php /*?><th class="no-sort"> <?php echo $this->Form->input('all', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'id' =>'selecctall')); ?> 
											
											<input type="checkbox" name="all" id="selecctall"/>
                                            </th><?php */?>
                                            <th><?php echo $this->Paginator->sort('name', 'Name'); ?></th>                                            <th class="no-sort"> 
                                            Actions                                             
                                            <span class="delete_span">
                                            <?php echo $this->Form->create('Bank', array('action' => 'delete_multiple', 'role' => 'form', 'type' => 'file', 'id' => 'multiple_delete_form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
            <?php echo $this->Form->input('deleted_items', array('class' => 'form-control', 'type'=>'hidden', 'label' => false, 'id' => 'deleted_items')); ?>
            
             <?php /*?><?php echo $this->Form->postLink(__('<img src="'.Router::url('/').'img/delete_multiple.png"/>'), array( $data['Bank']['id']), array('escape' => false,'class' => 'newicon2 red-stripe delete_multiple' ,'title'=>'Multiple Delete')); ?><?php */?>
             
            <?php echo $this->form->end(); ?>
                                            </span>
                                            
                                
                                            
                                            
                                             </th>
                     
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
										
										//print_r($Banks);die;
										if (isset($Banks) && !empty($Banks)) { ?>
                                            <?php foreach ($Banks as $bank_row) { ?>
                                                <tr>
                                                    <?php /*?><td > <?php echo $this->Form->input('id[]', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'value' => $data['Bank']['id'])); ?>
													
													<input type="checkbox" class="delete_checkbox" name="id[]"  value="<?php echo $bank_row['Bank']['id']; ?>"/>
													</td><?php */?>
                                                    
													<td><?php echo $bank_row['Bank']['name']; ?>&nbsp;</td>                             			 
													<td></td>																			                                                    <td class="actionicons">
       
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('controller' => 'banks','action' => 'edit', $bank_row['Bank']['id']), array('escape' => false,'class' => 'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Form->postLink(__('<img src="'.Router::url('/').'img/delete.png"/>'), array('controller' => 'banks','action' => 'delete', $bank_row['Bank']['id']), array('escape' => false,'class' => 'newicon red-stripe delete')); 
														?>                      
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10"> No results found! </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                           <div class="margin-top-20">
						<?php 
						echo $this->Paginator->counter(array(
                        		'format' => 'Showing {:start} to {:end} of {:count}'
                        	));                           

						?>
                        </div>
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>                     
                    </div>
                </div>
            </div>       
        </div>
    
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>
</div>

<script type="text/javascript">			
    jQuery( document ).ready(function() {
		jQuery('.delete_checkbox').click(updateTextArea);
		jQuery('#selecctall').click(updateTextArea);
		
		updateTextArea();
    });
	

	jQuery( ".delete_multiple" ).click(function() {
		if(jQuery('#deleted_items').val() == "") {
			alert ("Please select atleast one checkbox for delete.");	
			return false;
		}
		
		del =confirm("Are you sure to delete permanently?");
		if(del!=true)
		{
		return false;
		}
					
		
    	jQuery( "#multiple_delete_form" ).submit();
    });
</script>
<style>
#getdata th.actionicons, #getdata td.actionicons {
    width: 153px !important;
}
#loading-image {
    background: #fff none repeat scroll 0 0;
    bottom: 0;
    left: 0;
    opacity: 0.8;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 9999;
}
#loading {
    width: 50px;
    height: 57px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
.btn-success{opacity:1 !important; padding:5px 15px!important;  position:relative !important;}
.portlet .portlet .row h3.page-title{ display:none;}
.portlet .portlet .row #BankAdminIndexForm{display:none;}
.portlet .portlet ul.pagination{display:block !important;}
.portlet .portlet .margin-top-20{display:block !important;}
.table-responsive {
    overflow-x: initial;
}
</style>