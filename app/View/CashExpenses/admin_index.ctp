<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">        


            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Cash Expense
                        <?php /*?><span class="btn green fileinput-button" style="margin-right:10px;">
                            <a href="<?php echo Router::url('/') ?>admin/cash_expenses/add"><i class="fa fa-plus"></i>  <span>Add New</span></a> 
                        </span>
<?php */?>
                    </h3>

                </div>
            </div>
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                         
                            <div class="confirm_heading clearfix">
                                <h5><strong>Please enter date range for this report  </strong>
                                    <span>  
                                        <input type="button" class="btn btn-warning pull-right" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">
                                    </span>
                                </h5>

                            </div>                         
                            
                            <?php echo $this->Form->create('CashExpense', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                           
                                <div class="row">                                
                                    <div class="col-md-3 col-xs-6">
                                        <label>Report date</label>                               
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>
                                    </div>
                                                                
                                    <div class="col-md-4 ">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                        <label>&nbsp;</label>
                                        <a style=" margin-top:24px;" href="<?php echo Router::url('/'); ?>admin/cash_expenses/reset" class="btn btn-warning ">Reset</a> 

                                    </div>

                                </div>
                               
                            </div>
                            <?php echo $this->form->end(); ?>                          
                        </div>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable tabbable-custom tabbable-full-width">

                            <div class="tab-content">

                                <div id="tab_1_5" class="tab-pane1">

                                    <?php //echo '<pre>'; print_r($LotterySalesReport); die;		 ?>

                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Report Date</th>
                                                    <th>Category</th>
                                                    <th>Payment Mode</th>
                                                    <th>Cheque Number</th>
                                                    <th>Amount</th>
                                                    <th>Payee</th>
                                                    <th>Memo</th>                                                  
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($cashexpenses) && !empty($cashexpenses)) { ?>
                                                    <?php
                                                    //echo '<pre>'; print_r($Salereport);die;
                                                    $total_amount = 0;
                                                    foreach ($cashexpenses as $data) {
                                                        
                                                        $amount = $data['CashExpense']['amount'];
                                                        $total_amount = $total_amount + $amount;
                                                        ?>
                                                        <tr>
                                                        <td><?php echo date('Y-m-d',strtotime($data['CashExpense']['report_date'])); ?></td>
                                                        <td>
														<?php 														
														$userObj = ClassRegistry::init('OtherCategories');
														$category= $userObj->find('list',array('conditions'=>array('OtherCategories.id' => $data['CashExpense']['category']),'fields'=>array('OtherCategories.category'))); 
											            print_r($category[$data['CashExpense']['category']]);
														 ?></td>  
                                                        <td><?php echo $data['CashExpense']['payment_mode']; ?></td>  
                                                        <td><?php echo $data['CashExpense']['cheque_number']; ?></td> 
                                                        <td><?php echo '$ ' . $amount; ?></td>  
                                                        <td><?php echo $data['CashExpense']['payee']; ?></td> 
                                                        <td><?php echo $data['CashExpense']['memo']; ?></td> 
                                                  
                                                            <td>
                                                              <?php /*?>  <?php
                                                                echo $this->Html->link("<i class='fa fa-edit'></i>", array('controller' => 'cash_expenses', 'action' => 'edit/' . $data['CashExpense']['id']), array('escape' => false, 'class' => 'btn default btn-xs red-stripe edit'));
                                                                ?>  <?php */?>       
                                                                <?php echo $this->Html->link("<i class='fa fa-trash'></i>", array('controller' => 'cash_expenses', 'action' => 'delete', $data['CashExpense']['id']), array('escape' => false, 'class' => 'btn default btn-xs red-stripe delete', 'confirm' => 'Are you sure you want to delete?'));
                                                                ?>                                                                                                                 
                                                            </td> 
                                                        </tr>
                                                    <?php } ?>
                                                <thead>
                                                    <tr>
                                                        <th>Total</th>
                                                        <th></th>  
                                                        <th></th> 
                                                        <th></th>  
                                                        <th><?php echo '$ '.$total_amount; ?></th>  
                                                        <th></th> 
                                                        <th></th> 
                                                        <th></th>                                       
                                                    </tr>
                                                </thead>  
                                                <?php
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="8">No result founds!</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="margin-top-20">
                                        <ul class="pagination">
                                            <li>
                                                <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                            </li>
                                            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                            <li> 
                                                <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!--end tabbable-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <style>
            .current{
                background: rgb(238, 238, 238) none repeat scroll 0 0;
                border-color: rgb(221, 221, 221);
                color: rgb(51, 51, 51);
                border: 1px solid rgb(221, 221, 221);
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .view_file{
                width: 30px;
                border: medium none;
                padding: 10px;
                color: #FFF;		
                font-size: 13px;		
            }



        </style>

    </div>
</div>
<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
        <script>
            $(document).ready(function () {
                $('#config-demo').val('');
            });
        </script>
    <?php
}
?>
<script>             
    var tableToExcel = (function () {
                        var uri = 'data:application/vnd.ms-excel;base64,'
                            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                                        , base64 = function (s) {
                            return window.btoa(unescape(encodeURIComponent(s)))
                        }
                        , format = function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (table, name) {
                        if (!table.nodeType)
                            table = document.getElementById(table)
                        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
                        window.location.href = uri + base64(format(template, ctx))
                    }
                })()
</script>