<script type="text/javascript">
jQuery( document ).ready(function() {  
	// validate the form when it is submitted
	$("#CompanyAdminAddForm").validate();
	
		$("#CompanyName").rules("add", {
			required:true,
			messages: {
				required: "Please enter Company Name"
			}
		});
		$("#CompanyEmail").rules("add", {
			required:true,
			messages: {
				required: "Please enter Email"
			}
		});
		
	   
   
 });  
</script>
<?php echo $this->Html->script(array('admin/custom')); ?>
 <?php echo $this->Form->create('Company', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Company
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div>
                  <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
                </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Company Name:<span class="star">* </span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Company Name'));
									echo $this->Form->input('id', array('class' => 'form-control'));
									echo $this->Form->input('user_id', array('type' => 'hidden'));
									
									?>
                                    
                                </div>
                            </div>
							<div class="row static-info">
                                <div class="col-md-5 name">
                                  Company Address:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Company Address')); ?>
                                    
                                </div>
                            </div>
							<div class="row static-info">
                                <div class="col-md-5 name">
                                  Company Phone:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Company Phone')); ?>
                                    
                                </div>
                            </div>

                           
                          
                          
                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
						
						  <div class="row static-info">
                                <div class="col-md-5 name">
                                   Email:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email')); ?>

                                </div>
                            </div>
                          <div class="row static-info">
                                <div class="col-md-5 name">
                                    Password:<span class="star">* </span>			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('password', array('class' => 'form-control',  'required' => 'false', 'label' => false, 'placeholder' => 'Enter Password')); ?>

                                </div>
                            </div>
                          
                        
                        </div></div></div>
               
            </div>
            
            


            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
<?php echo $this->form->end(); ?>


    
  



