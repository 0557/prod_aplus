<?php
app::import('Model','Company');
$com_obj=new Company();
app::import('Model', 'SitePermission');
$permission_obj = new SitePermission();
?>
<style>
    .search-form-default {
        background: rgb(240, 246, 250) none repeat scroll 0 0;
        margin-bottom: 25px;
        padding: 12px 14px;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Companies  
					  <?php if ($permission_obj->CheckPermission($UsersDetails['id'], 'companies', 'is_add')) { ?>
					<span class="btn green fileinput-button">
                    <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('controller' => 'companies', 'action' => 'admin_add'), array('escape' => false)); ?>
                    </span>
                   <?php } ?>
				   
                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                           
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('name')); ?>  </th>
                                            <th>Email   </th>
                                            <th><?php echo __($this->Paginator->sort('phone')); ?>  </th>
                                           
                                            <th><?php echo __($this->Paginator->sort('created')); ?>   </th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($companies) && !empty($companies)) { ?>
                                            <?php foreach ($companies as $company){ ?>
                                                <tr>
												    
													<td><?php echo h($company['Company']['name']); ?>&nbsp;</td>
													<td><?php echo $com_obj->geruseremail($company['Company']['id']); ?>&nbsp;</td>
													<td><?php echo h($company['Company']['phone']); ?>&nbsp;</td>
													
													<td><?php echo h($company['Company']['created']); ?>&nbsp;</td>
													
													
                                                  
                                                    <td>
													 
													  
                                                         <?php if ($permission_obj->CheckPermission($UsersDetails['id'], 'companies', 'is_read')) { ?>
														<?php echo $this->Html->link("<i class='fa fa-eye'></i>",array('controller'=>'companies','action'=>'view',$company['Company']['id']) ,array('escape' => false,'class'=>'btn default btn-xs red-stripe view')); ?>
														<?php } ?>
														 <?php if ($permission_obj->CheckPermission($UsersDetails['id'], 'companies', 'is_edit')) { ?>
                                                        <?php echo $this->Html->link("<i class='fa fa-pencil'></i>",array('controller'=>'companies','action'=>'edit',$company['Company']['id']) ,array('escape' => false,'class'=>'btn default btn-xs red-stripe edit')); ?>
														<?php } ?>
														 <?php if ($permission_obj->CheckPermission($UsersDetails['id'], 'companies', 'is_delete')) { ?>
                                                        <?php echo $this->Html->link("<i class='fa fa-trash'></i>",array('controller'=>'companies','action'=>'delete',$company['Company']['id']) ,array('escape' => false,'class'=>'btn default btn-xs red-stripe delete')); ?>
														<?php } ?>
                                                        
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php }else { ?>
                                            no result found
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                             <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
border-color: rgb(221, 221, 221);
color: rgb(51, 51, 51);
border: 1px solid rgb(221, 221, 221);
float: left;
line-height: 1.42857;
margin-left: -1px;
padding: 6px 12px;
position: relative;
text-decoration: none;
    }
</style>