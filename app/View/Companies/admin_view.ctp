<?php
app::import('Model','Company');
$com_obj=new Company();

?>
<div class="page-content">

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Company Info - <?php echo $company['Company']['name']; ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                  
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                   
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Company Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $company['Company']['name']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Address:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $company['Company']['address']; ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Phone:  
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $company['Company']['phone']; ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Email:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $com_obj->geruseremail($company['Company']['id']); ?>

                                                        </div>
                                                    </div>
                                                    
                                                    


                                                </div>
                                            </div>
                                        </div>
                                        



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
    <!-- END PAGE CONTENT-->
