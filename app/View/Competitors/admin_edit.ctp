<?php echo $this->Html->script(array('admin/custom')); ?>
<?php if(!isset($ZipCode) && empty($ZipCode)){
    $ZipCode = '';
} ?>
<div class="page-content">
  

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-9 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Competitor
                    </div>
              
                </div>
                <div class="portlet-body form">
                    <?php echo $this->Form->create('Competitor', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                    <!--<form role="form">-->
                    <div class="form-body">
                        <div class="form-group">
                            <label for="CompetitorName">Name</label>
                            <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Name')); ?>
                            <?php echo $this->Form->input('id'); ?>
                            <span class="help-block">

                            </span>
                        </div>
                        <div class="form-group">
                        <div class="form-group">
                            <label>Base Store</label>
                            <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'empty' => 'Select Store')); ?>
                            <span class="help-block">

                            </span>
                        </div>
                        <div class="form-group">
                            <label>Business Type</label>
                            <?php $buisness_type = Configure::read('buisness_type'); ?>
                            <?php echo $this->Form->input('business_type', array('class' => 'form-control', 'type' => 'select','options'=>$buisness_type ,'required' => 'false', 'id' => 'exampleInputPayroll', 'label' => false, 'placeholder' => 'Enter Payroll Account')); ?>
                            <!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter text">-->
                            <span class="help-block">

                            </span>
                        </div>
                        <div class="form-group">
                            <label for="CompetitorAddress">Address</label>
                            <?php echo $this->Form->input('address', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Address')); ?>
                            <!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter text">-->
                            <span class="help-block">

                            </span>
                        </div>
                      
                        <div class="form-group">
                            <label>Country</label>
                            <?php echo $this->Form->input('country_id', array('type' => 'select', 'class' => 'form-control','id'=>'AjaxCountry' , 'label' => false, 'required' => 'false', "empty" => "Select Country", 'options' => $countries)); ?>
                            <?php // echo $this->Form->input('country_id',array('class'=>'form-control','type'=>'email' ,'id'=>'exampleInputPayroll', 'label'=>false ,'placeholder'=>'Enter Corporation Name')); ?>
                        </div>
                        <div class="form-group">
                            <label>State</label>
                            <div id="state_div">
                                <?php echo $this->Form->input('state_id', array("label" => false, "div" => false, "class" => "form-control",'id'=>'AjaxState' , "label" => false, "empty" => "Select State", 'required' => 'false')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <div id="city_div">
                                <?php echo $this->Form->input('city_id', array("label" => false, "div" => false, "class" => "form-control",'id' =>'AjaxCity' , "label" => false, "empty" => "Select City", 'required' => 'false')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="CompetitorZipCode">Zip Code</label>
                            <?php echo $this->Form->input('zip_code', array('class' => 'form-control' ,'id' => 'AjaxZipCode','type'=>'select' ,'options'=>$ZipCode ,'required' => false, 'label' => false, 'empty' => 'Select Zip Code')); ?>
                            <span class="help-block">

                            </span>
                        </div>
                        <div class="form-group">
                            <label for="CompetitorSummary">Summary</label>
                            <?php echo $this->Form->input('summary', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Summary')); ?>
                            <span class="help-block">

                            </span>
                        </div>
                       
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <?php echo $this->form->end(); ?>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>

    </div>

    <!-- END PAGE CONTENT-->
</div>
