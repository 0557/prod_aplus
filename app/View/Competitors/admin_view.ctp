<?php //  pr($competitor);  ?>
<div class="page-content">

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Competitors Info - <?php echo $competitor['Competitor']['name']; ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <!--<h2 class="margin-bottom-20"> View  Info - FuelInvoice : <?php echo $wholesaleProduct['WholesaleProduct']['id']; ?> </h2>-->

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                    <!--														<div class="caption">
                                                                                                                                                                            <i class="fa fa-cogs"></i>Product Information
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="actions">
                                                                                                                                                                            <a href="#" class="btn default btn-sm">
                                                                                                                                                                                    <i class="fa fa-pencil"></i> Edit
                                                                                                                                                                            </a>
                                                                                                                                                                    </div>-->
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Competitor Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Competitor']['name']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Corporation:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Corporation']['name']; ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Store Name:  
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Store']['name']; ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Rating:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Competitor']['rating']; ?>

                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                 
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Address: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Competitor']['address']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Country: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['Country']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            State:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['State']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            City:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['City']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Zip Code::
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $competitor['ZipCode']['zip_code']; ?>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
    <!-- END PAGE CONTENT-->
