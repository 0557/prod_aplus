<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">License Name List 	<a href="<?php echo Router::url('/');?>admin/compliance/new_licenselist/" class="btn  notfinew default btn-xs red-stripe"  ><i class="fa fa-plus"></i> Add New</a>
</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
     <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
					<div class="row"><br/>
				</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                     <th><?php echo __($this->Paginator->sort('License Name'));?>  </th>
                                                 <th><?php echo $this->Paginator->sort('Action'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							   
						
							$ms = "'Are you sure want delete it?'";
                                             foreach ($list as $data) { 
                                               echo '<tr>
                                                      
                                                      <td>'.$data["Licenselist"]["l_name"].'</td>
                                                 	<td>   
													<a href="'.Router::url('/').'admin/compliance/edit_licenselist/'.$data["Licenselist"]["id"].'" class="btn default btn-xs red-stripe"><i class="fa fa-pencil"></i></a>
													<a href="'.Router::url('/').'admin/compliance/licenselist/'.$data["Licenselist"]["id"].'"  onClick="return confirm('.$ms.');" class="btn default btn-xs red-stripe" ><i class="fa fa-trash"></i></a>
												';
												echo '</td>
                                               
												</tr>';
												
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
        </div>
   

    </div>
    </div>
<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker();
	
	$("#expiry").attr("readonly", false); 
	});

	</script>