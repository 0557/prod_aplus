<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Notification
</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
           
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
          	<?php echo $this->Form->create('ComplianceNotification', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				   <div class="col-xs-6 col-md-2">
                <label>License Name </label>
				<?php	
				echo $this->Form->input('license_name', array('type'=>'select','class' => 'form-control','required' => true, 'label'=>false, 'options'=>array('Air Pollution License'=>'Air Pollution License','Business License'=>'Business License','Cigerette Dealer License'=>'Cigerette Dealer License','Cigarette license'=>'Cigarette license','Commercial Activity License'=>'Commercial Activity License','Dumpster'=>'Dumpster','EPA'=>'EPA','Food Establishment License'=>'Food Establishment License','Food Establishment License QR Code'=>'Food Establishment License QR Code','Gas License'=>'Gas License','Hazardous Materials'=>'Hazardous Materials','Hazardous Materials QR Code'=>'Hazardous Materials QR Code','Lottery License'=>'Lottery License','Motor Vehicle Repair'=>'Motor Vehicle Repair','Motor Vehicle Repair  QR Code'=>'Motor Vehicle Repair  QR Code','Sales Tax License'=>'Sales Tax License','Sales & Scanners'=>'Sales & Scanners','Sales & Scanners QR Code'=>'Sales & Scanners QR Code','Storage Tank Registration'=>'Storage Tank Registration','Tank License'=>'Tank License','Tobacco Retailer Permit'=>'Tobacco Retailer Permit','Other License'=>'Other License'), 'empty'=>''));?>
              </div>

                   <div class="col-md-2 col-xs-6">
                    <label>License Expire Date </label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('expiry_date', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>

                  
                  <div class="col-md-2 col-xs-6">
                    <label>Notify Before Days</label>
                    <!-- important for developer please place your clander code on this input-->
                   
					<?php echo $this->Form->input('notification_days', array('id' => 'notification_days', 'class' => 'form-control','type' => 'select', 'label' => false,'options'=>array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30'), 'required' => true, 'empty'=>'')); ?>
                  </div>
                  <div class="col-md-2 col-xs-6">
                    <label>Repeat Year </label>
                    <!-- important for developer please place your clander code on this input-->
					
					<?php	
				echo $this->Form->input('repeat_year', array('type'=>'select','class' => 'form-control','required' => false, 'label'=>false, 'options'=>array('Y'=>'Yes','N'=>'No'), 'empty'=>''));?>
				
                  </div>
                </div>
			   <div class="row"><br/>
				</div>
			       <div class="row">    
				   <div class="col-xs-6 col-md-2">
                <label>Email Subject Line </label>
				   
					<?php echo $this->Form->input('subject', array('id' => 'subject', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
              </div>
              <div class="col-xs-6 col-md-2">
                <label>Sender Email Id </label>
 
				<?php echo $this->Form->input('sender_email', array('id' => 'sender_email', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
				
              </div>
			     <div class="col-xs-6 col-md-4">
                <label>Store Email Id </label>
				<?php echo $this->Form->input('store_emails', array('id' => 'store_emails', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
              </div>
				</div>
				</div>
				 <div class="row"><br/>
				</div>
              <div class="row">
				
				     <div class="col-xs-6 col-md-8">
                <label>Email Body Line </label>
             
				<?php echo $this->Form->input('message', array('id' => 'store_emails', 'class' => 'form-control','type' => 'textarea', 'label' => false, 'required' => true)); ?>
              </div>
				</div>
				
				 <div class="row"><br/>
				</div>

              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" id="sunmit" name ="submit" class="btn default updatebtn" id="confirmpackid">Submit</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
          <?php echo $this->form->end(); ?>
            <!-- form ends -->
          </div>
		  
		
            <!--end tabbable-->
        </div>
		
        </div>
        </div>
   

    </div>
	<script type="text/javascript" charset="utf-8">
    $(document).ready(function (e) {
    //$('#expiry').Zebra_DatePicker();
    $("#expiry").attr("readonly", false); 
    });
    </script>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script>
      $( function() {
        $( "#expiry" ).datepicker({ dateFormat: 'dd-mm-yy' });
      } );
      </script>