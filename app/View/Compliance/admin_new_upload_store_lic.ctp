<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Upload License

</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
           
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
             	<?php echo $this->Form->create('ComplianceUploadstores', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'enctype'=>'multipart/form-data' ,'div' => false, 'required' => false))); ?>
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				   <div class="col-xs-6 col-md-2">
                <label>License Name </label>
					<?php	
				echo $this->Form->input('license_name', array('type'=>'select','class' => 'form-control','required' => true, 'label'=>false, 'options'=>array('Air Pollution License'=>'Air Pollution License','Business License'=>'Business License','Cigerette Dealer License'=>'Cigerette Dealer License','Cigarette license'=>'Cigarette license','Commercial Activity License'=>'Commercial Activity License','Dumpster'=>'Dumpster','EPA'=>'EPA','Food Establishment License'=>'Food Establishment License','Food Establishment License QR Code'=>'Food Establishment License QR Code','Gas License'=>'Gas License','Hazardous Materials'=>'Hazardous Materials','Hazardous Materials QR Code'=>'Hazardous Materials QR Code','Lottery License'=>'Lottery License','Motor Vehicle Repair'=>'Motor Vehicle Repair','Motor Vehicle Repair  QR Code'=>'Motor Vehicle Repair  QR Code','Sales Tax License'=>'Sales Tax License','Sales & Scanners'=>'Sales & Scanners','Sales & Scanners QR Code'=>'Sales & Scanners QR Code','Storage Tank Registration'=>'Storage Tank Registration','Tank License'=>'Tank License','Tobacco Retailer Permit'=>'Tobacco Retailer Permit','Other License'=>'Other License'), 'empty'=>''));?>
              </div>

                   <div class="col-md-2 col-xs-6">
                    <label>Upload Files  </label>
                    <!-- important for developer please place your clander code on this input-->
					<?php echo $this->Form->input('store_file', array('id' => 'store_file', 'type' => 'file', 'label' => false, 'required' => true)); ?>(pdf, doc , excelsheet) 
				
                  </div>

                 
                </div>
            
				 <div class="row"><br/>
				</div>
              <div class="row">
				
				     <div class="col-xs-6 col-md-6">
                <label>Comments  </label>
               <?php echo $this->Form->input('message', array('id' => 'store_emails', 'class' => 'form-control','type' => 'textarea', 'label' => false, 'required' => true)); ?>
              </div>
				</div>
				
								 <div class="row"><br/>
				</div>

              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Submit</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
               <?php echo $this->form->end(); ?>
            <!-- form ends -->
          </div>
        </div>
      </div>
      </div>
 
 
<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#expiry').Zebra_DatePicker();
	});
	</script>