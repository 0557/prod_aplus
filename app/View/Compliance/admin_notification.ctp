<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Notification
		  
		  	<a href="<?php echo Router::url('/');?>admin/compliance/new_notification/" class="btn  notfinew default btn-xs red-stripe"  ><i class="fa fa-plus"></i> Add New</a>
</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
           
            </div>
            
          </div>
		  
		   <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
					<div class="row"><br/>
				</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                     <th><?php echo __($this->Paginator->sort('License Name'));?>  </th>
                                            <th><?php echo $this->Paginator->sort('Store'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Expiry'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Repeat Year'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Notify Before Days'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('Sender Email'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('Store Emails'); ?></th>
                                                 <th><?php echo $this->Paginator->sort('Action'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							   
						
							$ms = "'Are you sure want delete it?'";
                                             foreach ($list as $data) { 
                                               echo '<tr>
                                                      
                                                      <td>'.$data["ComplianceNotification"]["license_name"].'</td>
                                                    <td>'.$data["ComplianceNotification"]["store_name"].'&nbsp;</td>
                                                    <td>'.$data["ComplianceNotification"]["expiry_date"].'&nbsp;</td>
                                                    <td>'.$data["ComplianceNotification"]["repeat_year"].'&nbsp;</td>
                                                    <td>'.$data["ComplianceNotification"]["notification_days"].'&nbsp;</td>
                                                    <td>'.$data["ComplianceNotification"]["sender_email"].'&nbsp;</td>
                                                    <td>'.$data["ComplianceNotification"]["store_emails"].'&nbsp;</td>
                                                 	<td>   
													<a href="'.Router::url('/').'admin/compliance/edit_notification/'.$data["ComplianceNotification"]["id"].'" class="newicon red-stripe"><img src="'.Router::url('/').'img/edit.png"/></a>
													<a href="'.Router::url('/').'admin/compliance/notification/'.$data["ComplianceNotification"]["id"].'"  onClick="return confirm('.$ms.');" class="newicon red-stripe" ><img src="'.Router::url('/').'img/delete.png"/></a>
												';
												echo '</td>
                                               
												</tr>';
												
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
		
        </div>
   

    </div>
    </div>
<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker();
	

	});

	</script>