<script type="text/javascript">
jQuery( document ).ready(function() {  
	// validate the form when it is submitted
	$("#CorporationAdminAddForm").validate();
	
		$("#CorporationName").rules("add", {
			required:true,
			messages: {
				required: "Please enter Name"
			}
		});
	   
   
 });  
</script>
<?php echo $this->Html->script(array('admin/custom')); ?>
 <?php echo $this->Form->create('Corporation', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Corporation
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div>
                  <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
                </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Corporation Name:<span class="star">* </span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Corporation Name')); ?>
                                    
                                </div>
                            </div>

                           
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Email:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email')); ?>

                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Phone:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('phone', array('class' => 'form-control',  'required' => 'false', 'label' => false, 'placeholder' => 'Enter Phone')); ?>

                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fax:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('fax', array('class' => 'form-control',  'required' => 'false', 'label' => false, 'placeholder' => 'Enter Fax')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Quick Book Path:	 
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('quick_book_path', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Quick Book Path')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    State WH ID:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('stateWhId', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter State WH ID')); ?>

                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Company:
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('company_id', array('class' => 'form-control',  'label' => false, 'required' => 'false', 'placeholder' => 'Company')); ?>
								</div>
                            </div>

							<div class="row static-info">
                                <div class="col-md-5 name">
                                  Address:
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('address', array('class' => 'form-control',  'label' => false, 'required' => 'false', 'placeholder' => 'Enter Address')); ?>
								</div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Country:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('country_id', array('type' => 'select', 'id'=>'AjaxCountry', 'class' => 'form-control', 'label' => false, 'required' => 'false', "empty" => "Select Country", 'options' => $countries)); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    State:
                                </div>
                                <div class="col-md-7 value">
                                     <div id="Ajax_State">
                                <?php echo $this->Form->input('state_id', array("label" => false, "div" => false,  'id'=>'AjaxState',  "class" => "form-control", "label" => false, "empty" => "Select State", 'required' => 'false')); ?>
                            </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  City:
                                </div>
                                <div class="col-md-7 value">
                                   <div id="Ajax_City">
                                <?php echo $this->Form->input('city_id', array("label" => false, "div" => false, "class" => "form-control",  'id'=>'AjaxCity',  "label" => false, "empty" => "Select City", 'required' => 'false')); ?>
                            </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Zip Code:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div id="Ajax_ZipCode">
                                      <?php echo $this->Form->input('zip_code', array('class' => 'form-control', 'id'=>'AjaxZipCode',  'type'=>'select',  'required' => false, 'label' => false,"empty" => "Select Zipcode")); ?>
                                    </div>
                                    </div>
                            </div>
                           
   
                        
                        </div></div></div>
               
            </div>
            
            


            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
<?php echo $this->form->end(); ?>


    
  



