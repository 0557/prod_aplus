<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.dynamic.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.gauge.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.core.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.effects.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.vprogress.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>

<style>
.bar-cont {
	width: 200px;
	float: right;
}
.bar-cont1 {
	width: 100%;
	margin: 0;
}
.bar-regular {
	background: #3366cc none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-plus {
	background: #dc3912 none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-super {
	background: #ff9600 none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-diesel {
	background: #19981f none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-reg {
	width: 90px;
	float: left;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif
}
.percentage {
	font-family: arial;
	font-weight: 600;
	text-align: right;
	width: 74%;
}
.gas-dashboard {
	margin: 10px 0 40px;
	overflow: hidden;
	width: 300px;
	float: left;
}
.gas-dashboard1 {
	margin: 10px 0 40px;
	overflow: hidden;
	width: 300px;
	text-align: right;
	float: right
}
.graph-bar {
	width: 100%;
	position: relative;
	overflow: hidden;
	margin: 40px 0 75px 0;
	background: #e0e6f0;
	padding: 29px 0 0 20px;
}
.gas-control {
	background: #fff;
	border: 1px solid #ddd;
	overflow: hidden;
	margin: 0 0 30px 20px;
}
.gas-control1 {
	background: #dff0d8;
	padding: 10px;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif
}
.triangle {
	float: left;
	overflow: hidden;
	padding: 9px 0 0 11px;
	width: 34px;
}
.triangle-text {
	float: left;
	font-family: arial;
	font-size: 12px;
	overflow: hidden;
	padding: 12px 0 0;
	width: 115px;
}
.main-cont {
	width: 1170px;
	margin: 0 auto;
}
.col-left {
	width: 50%;
	position: relative;
	overflow: hidden;
	float: left;
}

.backoffice-chart{width:900px;
 height:350px;
 float: left; 
 }
 
 
 
 
@media only screen and (max-width: 1160px)
  
   {
   	.main-cont {
    margin: 0 auto;
    width: 100%;
}

}


@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.bar-cont {
    float: right;
    width: 127px;
}
.bar-cont1 {
    margin: 0 0 4px;
    overflow: hidden !important;
    width: 100%;
}

.bar-regular {
    background: #3366cc none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}

.bar-reg {
    float: left;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    width: 49px;
}
.bar-plus {
    background: #dc3912 none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}
.bar-super {
    background: #ff9600 none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}
.bar-diesel {
    background: #19981f none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}

}




</style>
<div class="row">
<div class="col-xs-12">
<h3 class="page-title">Dashboard</h3>
</div>
</div>

<div class="row" style="margin-bottom:20px;">
<!-- graph -->
<div class="col-xs-8">
	<div style=" border-radius: 10px; border:2px solid #dbe1e8; width:100%; display:block; text-align: center; padding:30px;
  background: #ffffff;">
 <div class="percentage">Sales By Hours || Customer Visited</div>
    <canvas id="cvs4" width="600" height="300">[No canvas support]</canvas>
    <script>
    var line = new RGraph.Line({
        id: 'cvs4',
        data: [8,9,15,16,21,23, 14, 4,13,19,18,11],
        options: {
            numxticks: 11,
            numyticks: 5,
            backgroundGridVlines: false,
            backgroundGridBorder: false,
            colors: ['red'],
            linewidth: 2,
            gutterLeft: 40,
            gutterRight: 20,
            gutterBottom: 50,
            labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            shadow: false,
            noxaxis: true,
            textSize: 16,
            textColor: '#999',
            textAngle: 0,
            spline: true,
            hmargin: 5,
            clearto: 'white',
            tickmarks: function (obj, data, value, index, x, y, color, prevX, prevY)
            {
                var co = obj.context,
                    ra = 4;
                        
                // Clear a white space
                RGraph.path2(co, 'b a % % % 0 6.28 false f white', x, y, ra + 6);

                // Draw the center of the tickmark
                RGraph.path2(co, 'b a % % % 0 6.28 false c f #f00', x, y, ra);
                
                // Draw the outer ring
                RGraph.path2(co, 'b a % % % 0 6.28 false', x, y, ra + 1);
                RGraph.path2(co, 'a % % % 6.28 0 true c f rgba(255,200,200,0.5)', x, y, ra + 5);
            }
        }
    }).trace2({frames: 60});
</script> 
  </div>
  <!--canvas 4 ends-->
</div>
<!-- graph ends -->
<!-- side bar -->
<div class="col-xs-4">
	<div style=" border-radius: 10px; border:2px solid #dbe1e8; width:100%; display:block; text-align: center; padding:0;
  background: #ffffff;">
  <h3 class="page-title" style="margin-top:0; border-radius: 10px;">Select Corporation and then Store</h3>
  <ul>
  
  </ul>
  </div>
</div>
<!-- sidebar ends -->
</div>






<div class="row" style="">
<div class="col-xs-12">
<h3 class="page-title">Critical Control Points</h3>
</div>
</div>
<div class="row">
<div class="col-xs-12"><ul class="menu-btn">
                 
                <li class="<?php echo (isset($corpss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Corporation", array('controller' => 'corporations', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($storess)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($vendorss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link(" <i class='fa fa-tags'></i>Vendors", array('controller' => 'customers', 'action' => 'vendor'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($customerss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-sitemap'></i>Customers", array('controller' => 'customers', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Competitors", array('controller' => 'competitors', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                      <?php 
$userSessionData = $this->Session->read('Auth.User');
$role = $userSessionData['Role']['alias'];
$store = $this->Session->read('Auth.User.StoreInfo');
if ($role != 'store_admin' && empty($store)) {
?>
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Add User", array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
                </li>
<?php } ?>

            </ul></div></div>



