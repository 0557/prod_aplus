<style>
    .search-form-default {
        background: rgb(240, 246, 250) none repeat scroll 0 0;
        margin-bottom: 25px;
        padding: 12px 14px;
    }
</style>
<!--
<div class="row">
<div class="col-xs-12"><ul class="menu-btn">
                 
                <li class="<?php echo (isset($corpss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Corporation", array('controller' => 'corporations', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($storess)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($vendorss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link(" <i class='fa fa-tags'></i>Vendors", array('controller' => 'customers', 'action' => 'vendor'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($customerss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-sitemap'></i>Customers", array('controller' => 'customers', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Competitors", array('controller' => 'competitors', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Add User", array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
                </li>


            </ul></div></div>-->
<div class="page-content-wrapper">
    <div class="portlet box blue">
               <div class="page-content portlet-body">


        <!-- BEGIN PAGE HEADER-->
      
		<div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Corporation  
					<?php if ('administrator' == $this->Session->read('Auth.User.Role.alias')) { ?>
						<span class="btn green fileinput-button">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('controller' => 'corporations', 'action' => 'admin_add'), array('escape' => false)); ?>
						</span>
					<?php } ?>
                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
		
		
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                           
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('name')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('address')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('phone')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('email','Email')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('zip_code')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('City.name','City')); ?>  </th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($corporations) && !empty($corporations)) { ?>
                                            <?php foreach ($corporations as $data) { ?> 
                                                <tr>
                                                    <td> <?php echo $data['Corporation']['name']; ?> </td>
                                                    <td> <?php echo $data['Corporation']['address']; ?> </td>
                                                    <td>  <?php echo $data['Corporation']['phone']; ?>  </td>
                                                    <td>  <?php echo $data['Corporation']['email']; ?>  </td>
                                                    <td> <?php echo $data['ZipCode']['zip_code']; ?> </td>
                                                    <td> <?php echo $data['City']['name']; ?>  </td>
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>',array('controller'=>'corporations','action'=>'view',base64_encode($data['Corporation']['id'])) ,array('escape' => false,'class'=>'newicon red-stripe view')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('controller'=>'corporations','action'=>'edit',base64_encode($data['Corporation']['id'])) ,array('escape' => false,'class'=>'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>',array('controller'=>'corporations','action'=>'delete',base64_encode($data['Corporation']['id'])) ,array('escape' => false,'class'=>'newicon red-stripe delete')); ?>
                                                        
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php }else { ?>
                                            no result found
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                             <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
border-color: rgb(221, 221, 221);
color: rgb(51, 51, 51);
border: 1px solid rgb(221, 221, 221);
float: left;
line-height: 1.42857;
margin-left: -1px;
padding: 6px 12px;
position: relative;
text-decoration: none;
    }
</style>