<div class="corporations index">
	<h2><?php echo __('Corporations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('federal_id'); ?></th>
			<th><?php echo $this->Paginator->sort('payroll_acount'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('fax'); ?></th>
			<th><?php echo $this->Paginator->sort('quick_book_path'); ?></th>
			<th><?php echo $this->Paginator->sort('state_wh_id'); ?></th>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('state_id'); ?></th>
			<th><?php echo $this->Paginator->sort('city_id'); ?></th>
			<th><?php echo $this->Paginator->sort('zipcode'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($corporations as $corporation): ?>
	<tr>
		<td><?php echo h($corporation['Corporation']['id']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['name']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['federal_id']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['payroll_acount']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['email']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['phone']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['fax']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['quick_book_path']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['state_wh_id']); ?>&nbsp;</td>
		<td><?php echo h($corporation['Corporation']['address']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($corporation['Country']['name'], array('controller' => 'countries', 'action' => 'view', $corporation['Country']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($corporation['State']['name'], array('controller' => 'states', 'action' => 'view', $corporation['State']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($corporation['City']['name'], array('controller' => 'cities', 'action' => 'view', $corporation['City']['id'])); ?>
		</td>
		<td><?php echo h($corporation['Corporation']['zipcode']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $corporation['Corporation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $corporation['Corporation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $corporation['Corporation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $corporation['Corporation']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Corporation'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
