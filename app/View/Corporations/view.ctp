<div class="corporations view">
<h2><?php echo __('Corporation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Federal Id'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['federal_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payroll Acount'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['payroll_acount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quick Book Path'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['quick_book_path']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State Wh Id'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['state_wh_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($corporation['Country']['name'], array('controller' => 'countries', 'action' => 'view', $corporation['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo $this->Html->link($corporation['State']['name'], array('controller' => 'states', 'action' => 'view', $corporation['State']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($corporation['City']['name'], array('controller' => 'cities', 'action' => 'view', $corporation['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zipcode'); ?></dt>
		<dd>
			<?php echo h($corporation['Corporation']['zipcode']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Corporation'), array('action' => 'edit', $corporation['Corporation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Corporation'), array('action' => 'delete', $corporation['Corporation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $corporation['Corporation']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Corporations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Corporation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Stores'); ?></h3>
	<?php if (!empty($corporation['Store'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Corporation Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Business Type'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('State Id'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Zip Code'); ?></th>
		<th><?php echo __('Sales Tax'); ?></th>
		<th><?php echo __('Fax'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($corporation['Store'] as $store): ?>
		<tr>
			<td><?php echo $store['id']; ?></td>
			<td><?php echo $store['corporation_id']; ?></td>
			<td><?php echo $store['name']; ?></td>
			<td><?php echo $store['business_type']; ?></td>
			<td><?php echo $store['Address']; ?></td>
			<td><?php echo $store['country_id']; ?></td>
			<td><?php echo $store['state_id']; ?></td>
			<td><?php echo $store['city_id']; ?></td>
			<td><?php echo $store['zip_code']; ?></td>
			<td><?php echo $store['sales_tax']; ?></td>
			<td><?php echo $store['fax']; ?></td>
			<td><?php echo $store['phone']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'stores', 'action' => 'view', $store['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'stores', 'action' => 'edit', $store['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'stores', 'action' => 'delete', $store['id']), array('confirm' => __('Are you sure you want to delete # %s?', $store['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
