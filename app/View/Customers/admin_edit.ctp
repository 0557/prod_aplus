<?php echo $this->Html->script(array('admin/custom')); ?>

 <?php $option = Configure::read('bool'); ?>
<?php $type = Configure::read('type'); ?>
 <?php echo $this->Form->create('Customer', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php if(!isset($ZipCode) && empty($ZipCode)){
    $ZipCode = '';
} ?>
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Customer
                    </div>
                   
                </div></div></div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Customer Type:
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('retail_type', array('class' => 'form-control', 'label' => false  , 'required' => 'false', 'empty' => 'Select Vendor Type',"options"=>array("wholesale"=>"wholesale","corporation"=>"corporation"),"onchange"=>"SelectType(this.value);")); ?>
                                <?php 	   echo $this->Form->input('id'); ?>
                                </div>
                            </div>
                            <div id="is_corporation_div" <?php if($this->request->data['Customer']['retail_type']=='wholesale' || $this->request->data['Customer']['retail_type']==''){ ?>  style="display:none;" <?php } else { ?> <?php } ?>>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                Corporation:
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('corporation_id', array('class' => 'form-control', 'label' => false  , 'required' => 'false', 'empty' => 'Select Corporation')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Store:
                                </div>
                                <div class="col-md-7 value">
                                      <div id="store_div"><?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false  , 'required' => 'false', 'empty' => 'Select Store')); ?></div>
                                </div>
                            </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Name:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false',  'placeholder' => 'Enter Name')); ?>

                                </div>
                            </div>
                           
                            

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Contact Person:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('contact_person', array('class' => 'form-control' ,  'required' => false, 'label' => false, 'placeholder' => 'Enter Contact Person')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    GL [Name in accounting]:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('gl', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'id' => '', 'label' => false, 'placeholder' => 'Enter GL')); ?>

                                </div>
                            </div>
                            
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terms:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('terms', array('class' => 'form-control'  , 'required' => false, 'label' => false, 'placeholder' => 'Enter Terms')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Email:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   E D I Parser:	 
                                </div>
                                <div class="col-md-7 value">
                                       <?php echo $this->Form->input('edi_parser', array('class' => 'form-control' ,'options'=>$option , 'required' => false, 'label' => false)); ?>

                                </div>
                            </div>
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Type:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('type', array('class' => 'form-control' ,'options'=>$type , 'required' => false, 'label' => false)); ?>

                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Address:
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('address', array('class' => 'form-control',  'label' => false, 'required' => 'false', 'placeholder' => 'Enter Address')); ?>
        </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Country:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('country_id', array('type' => 'select', 'id'=>'AjaxCountry' ,'class' => 'form-control', 'label' => false, 'required' => 'false', "empty" => "Select Country", 'options' => $countries)); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    State:
                                </div>
                                <div class="col-md-7 value">
                                     <div id="Ajax_State">
                                <?php echo $this->Form->input('state_id', array("label" => false ,  "div" => false, "class" => "form-control", "label" => false,'id'=>'AjaxState' ,"empty" => "Select State", 'required' => 'false')); ?>
                            </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  City:
                                </div>
                                <div class="col-md-7 value">
                                   <div id="Ajax_City">
                                <?php echo $this->Form->input('city_id', array("label" => false,  'id'=>'AjaxCity',"div" => false, "class" => "form-control", "label" => false, "empty" => "Select City", 'required' => 'false')); ?>
                            </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Zip Code:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div id="Ajax_ZipCode">
                                      <?php echo $this->Form->input('zip_code', array('class' => 'form-control', 'type'=>'select' ,'empty'=>'Select Zip Code' , 'options'=>$ZipCode , 'id'=>'AjaxZipCode' ,  'required' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Phone:	 
                                </div>
                                <div class="col-md-7 value">
                                       <?php echo $this->Form->input('phone', array('class' => 'form-control' , 'required' => false, 'label' => false, 'placeholder' => 'Enter Phone')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fax:	 
                                </div>
                                <div class="col-md-7 value">
                                       <?php echo $this->Form->input('fax', array('class' => 'form-control' , 'required' => false, 'label' => false, 'placeholder' => 'Enter Fax')); ?>
                                </div>
                            </div>
                           
   
                        
                        </div></div></div>
               
            </div>
            
            


            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
<!--        <button type="reset" class="btn default">Reset</button>-->
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
<?php echo $this->form->end(); ?>


  <script type="text/javascript">
function SelectType(value){
  if(value=='corporation'){
    jQuery("#is_corporation_div").show();
  }else{
    jQuery("#is_corporation_div").hide();
  }
}
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
      
		
		$('#CustomerCorporationId').change(function () {
            var corporation_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/customers/stores",
                data: 'corporation_id=' + corporation_id,
                success: function (data) {
                    $("#store_div").html(data);
                }
            });

        });
		
		
       
    });
</script>




    
  



