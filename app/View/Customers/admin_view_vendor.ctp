<div class="page-content">

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Vendor  Info - <?php echo $customer['Customer']['name']; ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">


                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                  
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Vender Type:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['retail_type']; ?>

                                                        </div>
                                                    </div>
                                                    <div id="is_corporation_div"  <?php if ($customer['Customer']['retail_type'] == 'wholesale' || $customer['Customer']['retail_type'] == '') { ?>  style="display:none;" <?php } else { ?> <?php } ?>>

                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Corporation:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <?php echo $customer['Corporation']['name']; ?>

                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Store:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <?php echo $customer['Store']['name']; ?>

                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Name: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['name']; ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Contact Person:  
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['contact_person']; ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Terms:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['terms']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Email:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['email']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            E D I Parser:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['edi_parser']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Vendor Type:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['type']; ?>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                 
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Address: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['address']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Country: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Country']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            State:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['State']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            City:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['City']['name']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Zip Code:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['ZipCode']['zip_code']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Phone:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['phone']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Fax:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $customer['Customer']['fax']; ?>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
</div>
    <!-- END PAGE CONTENT-->
