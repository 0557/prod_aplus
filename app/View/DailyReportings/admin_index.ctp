<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">

            <div class="row">
                <div class="portlet-body col-md-12">
                    <?php
                    //echo 'ok';die;			
                    echo $this->Form->create('DailyReporting', array('action' => 'index', 'role' => 'form', 'type' => 'file'));
                    ?>	
                    <!--<form action="salesreport" method="post">-->
                    <div class=" col-md-4">
                        <?php 
						//echo $this->Form->input('update_date', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => $update_date)); 
                        include('multidatepicker.ctp'); 
						?>    
                    </div>	   
                    <div class=" col-md-1">
                        <button name="getdata" class="btn btn-success" type="submit">Submit</button>
                    </div>
                    <div class=" col-md-1">
                       <a href="<?php echo Router::url('/');?>admin/daily_reportings/reset" class="btn btn-warning ">Reset</a> 
                    </div>		
                    <?php echo $this->form->end();
					if ($this->Session->check('PostSearch'))
					{	
					?> 
                    <form action=" <?php echo Router::url('/');?>admin/daily_reportings/export" class="form-horizontal" id="" method="post" accept-charset="utf-8">                      
                    <div class="col-md-2">                   
                    <?php
					echo $this->Form->input('full_date', array('id' =>'full_date','type' => 'hidden','value' => $full_date)); 
					echo $this->Form->button('Report', array('type'=>'submit', 'class' => 'btn green','id' => 'exportid'));
					?>                                   
                    </div>
                    </form>
                    <?php
					}
					?>
                           
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Coupon Report
                    </h3>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom tabbable-full-width">

                        <div class="tab-content">

                            <div id="tab_1_5" class="tab-pane1">

                                <?php //echo '<pre>'; print_r($DailyReportingReport); die;		 ?>

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>Report Date</th>
                                                <th>Coupon Count</th>
                                                <th>Coupon Amount</th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($DailyReportingReport) && !empty($DailyReportingReport)) { ?>
                                                <?php
                                                //echo '<pre>'; print_r($DailyReportingReport);die;
												$total_coupon=0;
												$total_coupon_amount=0;
                                                foreach ($DailyReportingReport as $data) {
                                                    $total_coupon += $data['DailyReporting']['coupon_count'];
													$total_coupon_amount += $data['DailyReporting']['coupon_amount'];
                                                    ?>
                                                    <tr>
                                                        <td><?php echo h($data['DailyReporting']['reporting_date']); ?></td>  
                                                        <td><?php echo h($data['DailyReporting']['coupon_count']); ?></td>                                                    
                                                        <td><?php echo h($data['DailyReporting']['coupon_amount']); ?></td>
                                                        
                                                    </tr>
                                                <?php } ?>
                                                <thead>
                                                <tr>
                                                <th>Total Sales</th>
                                                <th><?php  echo $total_coupon; ?></th>  
                                                <th><?php  echo $total_coupon_amount; ?></th>                                            
                                                </tr>
                                                </thead>  
                                                <?php 
												} 
												else
												{
												?>
                                                <tr>
                                                    <td colspan="4">No result founds!</td>
                                                </tr>
                                                <?php
												}
												?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="margin-top-20">
                                    <ul class="pagination">
                                        <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                        </li>
                                            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                        <li> 
<?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end tab-pane-->
                        </div>
                    </div>
                </div>
                <!--end tabbable-->
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <style>
        .current{
            background: rgb(238, 238, 238) none repeat scroll 0 0;
            border-color: rgb(221, 221, 221);
            color: rgb(51, 51, 51);
            border: 1px solid rgb(221, 221, 221);
            float: left;
            line-height: 1.42857;
            margin-left: -1px;
            padding: 6px 12px;
            position: relative;
            text-decoration: none;
        }
    </style>

</div>
<script type="text/javascript">
    $('[data-toggle=modal]').on('click', function (e) {
        var $target = $($(this).data('target'));
        $target.data('triggered', true);
        setTimeout(function () {
            if ($target.data('triggered')) {
                $target.modal('show')
                        .data('triggered', false); // prevents multiple clicks from reopening
            }
            ;
        }, 1000); // milliseconds
        return false;
    });
</script>
 <?php 
if($full_date=='' || $full_date=='// - //')
{
?>        
<script>
$(document).ready(function(){
$('#config-demo').val('');		
});
</script>
<?php 
}
?>