<?php  // pr($this->params); // die; ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image"><?php echo $this->Html->image('avatar1_small.jpg', array('alt' => ''));  ?> </div>
      <div class="pull-left info">
        <p> <?php echo $UsersDetails['username']; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="start <?php echo (isset($dash) && !empty($dash)) ? 'active' : ''; ?> "> 
        <?php echo $this->Html->link('<i class="fa fa-dashboard"></i> Dashborad', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> 
        </li>
      <li> <a href="admin.html"> <i class="fa fa-gavel"></i> <span>Admin</span> </a> </li>
      <li> <a href="#"> <i class="fa fa-globe"></i> <span>Wholesale</span> </a> </li>
      <li> <a href="#"> <i class="fa fa-glass"></i> <span>R-Fuel</span> </a> </li>
      <li> <a href="#"> <i class="fa fa-glass"></i> <span>R-Grocery</span> </a> </li>
      <li> <a href="#"> <i class="fa fa-glass"></i> <span>Report</span> </a> </li>
    </ul>
  </section>
  <!-- /.sidebar --> 
</aside>
<!-- /.right-side -->
