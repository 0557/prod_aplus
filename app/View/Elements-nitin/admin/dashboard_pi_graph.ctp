<?php
//print_r($graphaData);
if(!empty($graphaData)) {  ?>
	<div class="col-left">
	  <div class="gas-dashboard"><h3>Sale Per Type</h3></div>
	 
		<canvas style="float: left; width: 82%; margin:0 0 20px 0;" height="250" width="700" id="cvs3">[No canvas support]</canvas>
		
		 <script>
		 {
			var pie = new RGraph.Pie({
				id: 'cvs3',
				data: <?php echo json_encode(array_values($graphaData)); ?>,
				options: {
					shadow: true,
                    shadowOffsetx: 0,
                    shadowOffsety: 5,
                    shadowColor: '#aaa',
                    radius: 80,
                    variant: 'donut3d', 
                    labels: ['Plus', 'Super', 'Regular', 'Diesel'],
                    colors: ['#DC3912', '#FF9600', '#3366CC', '#19981F'],
                    labelsSticks: true,
                    labelsSticksLength: 15,
					     gutterTop: 35,
                    gutterLeft: 0,
                    gutterRight: 5,
                    gutterBottom: 10,
					
				}
			}).fadeIn();
			};
		 </script> 
	  
	</div>
<?php } ?>