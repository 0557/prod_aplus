<?php
app::import("Model","SitePermission");
$permission_obj=new SitePermission();
?>
<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>&nbsp;</li>
				
				
				 <li class="start <?php if($this->params['controller']=='users' && $this->params['action']=='admin_dashboard') { ?>active<?php } ?> "> 
				  <?php echo $this->Html->link('<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span></span>', array('controller' => 'dashboards', 'action' => 'index'),array("escape"=>false)); ?>
				 </li>
				<li class="has-sub <?php if(in_array($this->params['controller'], array('leads')) && (in_array($this->params['action'],array("index","add","edit","view","export")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-bookmark-empty"></i> <span class="title">Leads</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <li>
					  <?php echo $this->Html->link('View Leads', array('controller' => 'leads', 'action' => 'index',"admin"=>false),array("escape"=>false)); ?>
					  </li>
					  <li>
					  <?php echo $this->Html->link('Add Leads', array('controller' => 'leads', 'action' => 'add',"admin"=>false),array("escape"=>false)); ?>
					  </li>
                      <li>
					  <?php echo $this->Html->link('Export Leads', array('controller' => 'leads', 'action' => 'export',"admin"=>false),array("escape"=>false)); ?>
					  </li>					 
					</ul>
				  </li>                
                <li class="has-sub <?php if(in_array($this->params['controller'], array('LeadProfiles')) && (in_array($this->params['action'],array("index","add","edit","view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-bookmark-empty"></i> <span class="title">Lead Profiles</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <li>
					  <?php echo $this->Html->link('View Lead Profiles', array('controller' => 'LeadProfiles', 'action' => 'index',"admin"=>false),array("escape"=>false)); ?>
					  </li>
					  <li>
					  <?php echo $this->Html->link('Add Lead Profiles', array('controller' => 'LeadProfiles', 'action' => 'add',"admin"=>false),array("escape"=>false)); ?>
					  </li>
                     			 
					</ul>
				  </li>
                  <li class="has-sub <?php if(in_array($this->params['controller'], array('transactionHistories')) && (in_array($this->params['action'],array("index","view","export")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-bookmark-empty"></i> <span class="title">Billing Histories</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <li>
					  <?php echo $this->Html->link('View Billing Histories', array('controller' => 'transactionHistories', 'action' => 'index',"admin"=>false),array("escape"=>false)); ?>
					  </li>
					  <li>
					  <?php echo $this->Html->link('Export Billing Histories', array('controller' => 'transactionHistories', 'action' => 'export',"admin"=>false),array("escape"=>false)); ?>
					  </li>
					
					</ul>
				  </li>
				   <li class="has-sub <?php if(in_array($this->params['controller'], array('transactionHistories')) && (in_array($this->params['action'],array("report")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-bookmark-empty"></i> <span class="title">Reports</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <li>
					  <?php echo $this->Html->link('View Report', array('controller' => 'transaction_histories', 'action' => 'report',"admin"=>false),array("escape"=>false)); ?>
					  </li>
					
					
					</ul>
				  </li>
                <li class="has-sub <?php if(in_array($this->params['controller'], array('Users')) && (in_array($this->params['action'],array("change_password")))) { ?>active<?php } ?>">
                  <?php echo $this->Html->link('<i class="icon-bookmark-empty"></i> <span class="title">Change Password</span>', array('controller' => 'users', 'action' => 'change_password',"admin"=>false),array("escape"=>false)); ?>                  
				  </li>
				<?php /* 
				  <li class="has-sub "> <a href="javascript:;"> <i class="icon-table"></i> <span class="title">Reports</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <li ><a href="#">Leads Distributed</a></li>
					  <li ><a href="#">Billing Summary</a></li>
					  <li ><a href="#">Search Leads</a></li>
					</ul>
				  </li>
				  
				<li class="start <?php if($this->params['controller']=='settings' && $this->params['action']=='admin_index') { ?>active<?php } ?> "> 
				  <?php echo $this->Html->link('<i class="icon-bookmark-empty"></i> 
					<span class="title">General Settings</span>
					<span></span>', array('controller' => 'settings', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
				 </li><?php */?>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
