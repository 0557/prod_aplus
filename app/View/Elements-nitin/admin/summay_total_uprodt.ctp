<?php
$_summaryTotal = array();
if(!empty($rubyUprodts)) {
	foreach($rubyUprodts as $_k => $_value) {
		$_ukey = @$prodList[$_value['RubyUprodt']['fuel_product_number']];
		
		if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
			$_summaryTotal[$_ukey]['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
			$_summaryTotal[$_ukey]['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
		} else {
			$_summaryTotal[$_ukey]['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
			$_summaryTotal[$_ukey]['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
		}
		
		if (isset($_summaryTotal['Total']['fuel_volume'])) {
			$_summaryTotal['Total']['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
			$_summaryTotal['Total']['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
		} else {
			$_summaryTotal['Total']['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
			$_summaryTotal['Total']['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
		}
	}
}

?>
<?php if(!empty($rubyUprodts) && !empty($_summaryTotal)) { ?>
	<?php foreach($prodList as $key => $value){ ?> 
		<div class="col-md-2">
			<h4><?php echo $value;?></h4>
			<input type="text" class="form-control" value="<?php echo $_summaryTotal[$value]['fuel_volume']; ?>" readonly /> <br/>
			<input type="text" class="form-control" value="$<?php echo $_summaryTotal[$value]['fuel_value']; ?>" readonly />
		</div>	
			
	<?php  } ?>
				
	<div class="col-md-2">
		<h4>Total </h4>
		<input type="text" class="form-control" value="<?php echo $_summaryTotal['Total']['fuel_volume']; ?>" readonly /> <br/>
		<input type="text" class="form-control" value="$<?php echo $_summaryTotal['Total']['fuel_value']; ?>" readonly />
	</div>
<?php  } ?>