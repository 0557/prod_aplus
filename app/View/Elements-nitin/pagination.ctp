<?php
$this->Paginator->options(array(
    'update' => '#update_table',
    'evalScripts' => true
));
?> 


<div class="row-fluid">
         <div class="span6">
             <div class="dataTables_info" id="sample_1_info">
              <?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
			  ?> 
			  </div>
                            </div>

                            <div class="span6">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    <ul>
                                        <?php
		echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'prev disabled', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
		echo $this->Paginator->next(__('Next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'next disabled', 'disabledTag' => 'a'));
	?>
 
                                    </ul>

                                </div>
                            </div>
                        </div>


<?php echo $this->Js->writeBuffer(); ?>