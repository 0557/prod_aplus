<div class="col-md-3">
	<label>From:</label>
	<input type="text" class="form-control" placeholder="Select From Date" id="from">
</div>

<div class="col-md-3">
	<label>To:</label>
	<input type="text" class="form-control" placeholder="Select To Date" id="to">
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable();
		$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});
	});
</script>
