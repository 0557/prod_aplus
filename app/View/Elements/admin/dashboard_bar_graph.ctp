
  <div class="gas-dashboard"></div>
   <div class="gas-dashboard1"></div>
   <div class="graph-bar">Graph Gas Gallon sold by Month
		<canvas style="float: left; width: 82%; margin:0 0 20px 0;" height="250" width="700" id="cvs2">[No canvas support]</canvas>
		<div class="bar-cont">
			<div>All Values * 100 Gallon</div>
			<?php foreach($prodList as $cat) { ?>
			  <div class="bar-cont1" style="overflow: hidden;">
				<div class="bar-<?php  echo strtolower($cat); ?>"></div>
				<div class="bar-reg"><?php echo $cat; ?></div>
			  </div>
			<?php  } ?>
		</div>
		
		
	
	<!--bar content div ends--> 
	<?php
	
		//if point 1 then add another dummy 
		if( isset($graphaData['y']) && count($graphaData['y']) ==  1 ) {
			$_graphaData['y'][] = array(0,0,0,0);
			$_graphaData['y'][] = $graphaData['y'][0];
			$graphaData = $_graphaData;
		}	
	?>
	<script>
		window.onload = function () {
		 var bar = new RGraph.Bar({
			id: 'cvs2',
			data: <?php echo json_encode(@$graphaData['y']); ?>,
			options: {
				backgroundGridVlines: false,
				backgroundGridBorder: false,
				noyaxis: true,
				labels2:[<?php echo json_encode(array('Nov','dec')); ?>],
				labels:['Nov', 'Dec'],
				colors: ['#DC3912', '#FF9600', '#3366CC', '#19981F'],
				hmargin: 15,
				strokestyle: 'white',
				gutterLeft: 50,
				linewidth: 1,
				shadowColor: '#ccc',
				shadowOffsetx: 0,
				shadowOffsety: 0,
				textSize: 8,
				shadowBlur: 10
				
			}
		}).wave({frames:30})
		};
	</script> 
  </div>