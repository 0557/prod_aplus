<style>
    .skin-black .navbar
    {
        background: #dcdadd!important;
        //color: #454648!important;
    }
    .navbar ul .select label
    {
        color:#4d4b4e;
    }
    .dropdown-toggle > span, .dropdown-toggle > .fa
    {
        color:#4d4b4e;
    }
    .navbar-right
    {
        background: #e2e0e3!important;
    }
</style>
<?php $option = classregistry::init('Corporation')->find('list'); ?> 
<?php if (isset($_SESSION['corporation_id']) || !empty($_SESSION['corporation_id'])) { ?>
    <?php $filter_store = classregistry::init('Store')->find('list', array('conditions' => array('Store.corporation_id' => $_SESSION['corporation_id']))); ?>
<?php } elseif (isset($_SESSION['corporation_id']) || empty($_SESSION['corporation_id'])) { ?>
    <?php $filter_store = ''; ?>
<?php } ?>

<!-- header logo: style can be found in header.less -->
<header class="header"> 
 <a href="#" class="logo"> 
<?php echo $this->Html->image('logo-main.png', array('alt' => 'logo', 'onclick' => 'javascript:history.back(1)')); ?></a> 
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation"> 
    <!-- Sidebar toggle button--> 
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
    
	<a href="<?php echo $this->webroot?>admin/ruby2_livepos" class="logo" style="width:115px;background-color:#dcdadd;"><img src="<?php echo $this->webroot?>images/live_data.png" width="48px;"></a>
	
	<div class="navbar-right">
      <ul class="nav navbar-nav">
      <!-- select dropdown -->
	<?php echo $this->element('admin/header_store_selector'); ?>
      
      
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i> <span>
        <?php echo $UsersDetails['username']; ?></i></span> </a>
          <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
            <li class="dropdown-header text-center">Account</li>
            <li> 
            <?php echo $this->Html->link('<i class="fa fa-building fa-fw pull-right"></i> Corporation', array('controller' => 'corporations', 'action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link("<i class='fa fa-university fa-fw pull-right'></i> Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
            </li>
            <li class="divider"></li>
            <li> <?php echo $this->Html->link('<i class="fa fa-ban fa-fw pull-right"></i> Log Out', array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?> </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
<script type="text/javascript">
  
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
		$('.sidebar-toggle').click(function(){
			$('.left-side,.logo').toggle();
		$('.right-side,.navbar-static-top').toggleClass('right-sidetoggele');
		
    });
		
        // binds form submission and fields to the validation engine
        $('#corporation').click(function() {
            if (this.checked) {
                $("div").removeClass("corporation_store_add");
            }
        })

        $('#corporationName').change(function() {
            var corporation_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/corporations/filter_store/" + corporation_id,
                data: 'corporation_id=' + corporation_id,
                success: function(data) {
                    $(".store_div").html(data);
                    location.reload();

                }
            });

        });

        jQuery(document).delegate('#storeName', 'change', function() {
            var store_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/corporations/SetstoreId/" + store_id,
                data: 'store_id=' + store_id,
                success: function(data) {
                    location.reload();
                }
            });




        });
    });
</script>
<style>
.right-sidetoggele {
    margin-left: 0;
}
.header .navbar.right-sidetoggele {
    margin-left: 0;
}
</style>