<?php 
$option = classregistry::init('Corporation')->find('list'); 
$corporationId = $this->Session->read('corporation_id');
$storeId = $this->Session->read('storeId');
$corporationList = classregistry::init('Corporation')->find('list');
$storeModel = classregistry::init('Store');
$storeOptList = array();
if(!empty($corporationList)) {
	foreach($corporationList as $_coid => $coname) {
		$storeOptList[$coname] = $storeModel->find('list', array('conditions' => array('corporation_id' => $_coid)));	
	}
}
?> 
<?php 
$userSessionData = $this->Session->read('Auth.User');
$role = $userSessionData['Role']['alias'];
$store = $this->Session->read('Auth.User.StoreInfo');
if ($role == 'store_admin' && !empty($store)) { ?>
      <li class="select">
		<label>Corporation: <?php echo $store['Corporation']['name']. '| Store: ' . $store['Store']['name']; ?></label>
      </li>
      <?php } else  { ?>
		 <li class="select">
			<label>Select Store</label>
			<?php echo $this->Form->input('Store.id', array('type' => 'select', 'id' => 'storeName', 'div' => false, 'label' => false, 'empty' => 'Select STORE', 'selected'=> $this->Session->read('store_id'), 'required' => 'false', 'options' => $storeOptList)); ?>
		</li>
	  <?php } ?>
        
<script type="text/javascript">
    jQuery(document).ready(function() {
        // binds form submission and fields to the validation engine
        $('#corporation').click(function() {
            if (this.checked) {
                $("div").removeClass("corporation_store_add");
            }
        })

        $('#corporationName').change(function() {
            var corporation_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/corporations/filter_store/" + corporation_id,
                data: 'corporation_id=' + corporation_id,
                success: function(data) {
                    $(".store_div").html(data);
                    location.reload();

                }
            });

        });

        jQuery(document).delegate('#storeName', 'change', function() {
            var store_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/corporations/SetstoreId/" + store_id,
                data: 'store_id=' + store_id,
                success: function(data) {
                    location.reload();
                }
            });




        });
    });
</script>