<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				
				 <?php echo $this->Html->link($this->Html->image('logo-big.png', array('alt' =>"Cypress Leads")), array('controller' => 'dashboards', 'action' =>'index'),array("escape"=>false)); ?>
				
				
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<?php echo $this->Html->link($this->Html->image('menu-toggler.png', array('alt' =>"")), 'javascript:void(0);',array("escape"=>false,"class"=>"btn-navbar collapsed","data-toggle"=>"collapse","data-target"=>".nav-collapse")); ?>
				
				     
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<!-- BEGIN TOP NAVIGATION MENU -->					
				<ul class="nav pull-right">
			
					<li class="dropdown user">
					<?php echo $this->Html->link($this->Html->image('avatar_small.png', array('alt' =>"")).' <span class="username">'.$UsersDetails['first_name'].'</span>
						<i class="icon-angle-down"></i>', 'javascript:void(0);',array("escape"=>false,"class"=>"dropdown-toggle","data-toggle"=>"dropdown")); ?>
					<ul class="dropdown-menu">
							<li><?php echo $this->Html->link('<i class="icon-user"></i> My Profile', array('controller' => 'users', 'action' => 'manageprofile',"admin"=>false),array("escape"=>false)); ?></li>
                            <li class="divider"></li>
                            <li>
                            	<?php echo $this->Html->link('<i class="icon-user"></i> Change Password', array('controller' => 'users', 'action' => 'change_password',"admin"=>false),array("escape"=>false)); ?>
                            </li>
							<?php /*?><li><a href="#"><i class="icon-user"></i> Privacy Policy</a></li><?php */?>
							
							<li class="divider"></li>
							<li>
							<?php echo $this->Html->link('<i class="icon-key"></i> Log Out', array('controller' => 'users', 'action' => 'logout'),array("escape"=>false)); ?>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>