<?php

app::import("Model","SitePermission");
$permission_obj=new SitePermission();
//pr($UsersDetails);

?>
<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>&nbsp;</li>
				
				
				 <li class="start <?php if($this->params['controller']=='users' && $this->params['action']=='admin_dashboard') { ?>active<?php } ?> "> 
				  <?php echo $this->Html->link('<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span></span>', array('controller' => 'users', 'action' => 'dashboard',"admin"=>true),array("escape"=>false)); ?>
				 </li>
				 
				  
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'users','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'users','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('users')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">Users</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'users','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View Users', array('controller' => 'users', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'users','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add User', array('controller' => 'users', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				  
				 
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_documents','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'user_documents','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('user_documents')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">User Documents</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_documents','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View User Documents', array('controller' => 'user_documents', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_documents','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add User Document', array('controller' => 'user_documents', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_consultations','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'user_consultations','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('user_consultations')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">User Consultations</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_consultations','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View User Consultations', array('controller' => 'user_consultations', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'user_consultations','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add User Consultation', array('controller' => 'user_consultations', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				    <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'memberships','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'memberships','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('memberships')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">Memberships</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'memberships','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View Memberships', array('controller' => 'memberships', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'memberships','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add Membership', array('controller' => 'memberships', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				  
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'specifications','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'specifications','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('specifications')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">Specifications</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'specifications','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View Specifications', array('controller' => 'specifications', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'specifications','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add Specification', array('controller' => 'specifications', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'hospitalmanagers','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'hospitalmanagers','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('hospitalmanagers')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">Hospital Manager</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'hospitalmanagers','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View Hospital', array('controller' => 'hospital_managers', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'hospitalmanagers','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add Hospital', array('controller' => 'hospital_managers', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
					  
 				  
				   <?php  if($permission_obj->CheckPermission($UsersDetails['role_id'],'pages','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'pages','is_add')) { ?>
				  <li class="has-sub <?php if(in_array($this->params['controller'], array('pages')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-file"></i> <span class="title">Pages</span> <span class="arrow "></span> </a>
					<ul class="sub">
					  <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'pages','is_read')) { ?>		
					  <li>
					  <?php echo $this->Html->link('View Pages', array('controller' => 'pages', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					 <?php } ?>
					 <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'pages','is_add')) { ?>	 
					 <li>
					  <?php echo $this->Html->link('Add Pages', array('controller' => 'pages', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					  <?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				    
					<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_read') || $permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_add')) { ?>
					
				    <li class="has-sub <?php if(in_array($this->params['controller'], array('emailTemplates')) && (in_array($this->params['action'],array("admin_index","admin_add","admin_edit","admin_view")))) { ?>active<?php } ?>"> <a href="javascript:void(0);"> <i class="icon-repeat"></i> <span class="title">Email Templates</span> <span class="arrow "></span> </a>
					<ul class="sub">
                    <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_read')) { ?>							 
					 <li>
					  <?php echo $this->Html->link('View Email Templates', array('controller' => 'emailTemplates', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					<?php } ?>
					<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_add')) { ?>
					  <li>
					  <?php echo $this->Html->link('Add Email Templates', array('controller' => 'emailTemplates', 'action' => 'add',"admin"=>true),array("escape"=>false)); ?>
					  </li>
					<?php } ?>
					</ul>
				  </li>
				  <?php } ?>
				
				  
				
				<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'settings','is_read')) { ?>  
				<li class="has-sub <?php if($this->params['controller']=='settings' && $this->params['action']=='admin_index') { ?>active<?php } ?> "> 
				  <?php echo $this->Html->link('<i class="icon-bookmark-empty"></i> 
					<span class="title">General Settings</span>
					<span></span>', array('controller' => 'settings', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>
				 </li>
				<?php } ?>
				<?php //echo $this->params['action'];?>
                <li class="has-sub <?php if(in_array($this->params['controller'], array('users')) && (in_array($this->params['action'],array("admin_change_password")))) { ?> active<?php } ?>">
                  <?php echo $this->Html->link('<i class="icon-time"></i> <span class="title">Change Password</span>', array('controller' => 'users', 'action' => 'change_password',"admin"=>true),array("escape"=>false)); ?>                  
				  </li>
                
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
