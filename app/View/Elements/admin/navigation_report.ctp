<li class="<?php echo (($this->params['controller'] ==  'ruby_uprodts' || $this->params['controller'] == 'ruby_utankts' || $this->params['controller'] == 'ruby_uautots' || $this->params['controller'] == 'ruby_uhosets' || $this->params['controller'] == 'ruby_userlvts' || $this->params['controller'] == 'ruby_utierts' || $this->params['controller'] == 'ruby_edispts' || $this->params['controller'] == 'ruby_dcrstats'
|| $this->params['controller'] == 'ruby_deptotals' || $this->params['controller'] == 'ruby_dailyreportings' 	)) ? 'active' : ''; ?>">
	<a href="javascript:;">
		<i class="fa fa-shopping-cart"></i>
		<span class="title">
			Report
		</span>
		<span class="arrow "></span>
	</a>
	
	<ul class="sub-menu">
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uprodts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Daily Sales", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utankts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sale By Tank No", array('controller' => 'ruby_utankts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uautots')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sales by MOP", array('controller' => 'ruby_uautots', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uhosets')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sales HOSC", array('controller' => 'ruby_uhosets', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_userlvts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sale By Service Level", array('controller' => 'ruby_userlvts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utierts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sale By Tier Product", array('controller' => 'ruby_utierts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_edispts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Dispenser Sales", array('controller' => 'ruby_edispts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dcrstats')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Sale By DCR No", array('controller' => 'ruby_dcrstats', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_deptotals')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dailyreportings')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Daily Reporting", array('controller' => 'ruby_dailyreportings', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
	</ul>
	
</li>

