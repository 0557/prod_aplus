<aside class="left-side sidebar-offcanvas"  style="border-right:1px solid #d9d9d9">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
 <div class="user-panel">
     <!-- <div class="pull-left image"> <?php echo $this->Html->image('avatar1_small.jpg', array('alt' => '')); ?> </div>-->
      <div class="pull-left info">
        <p><?php echo $UsersDetails['username']; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
		<p>Version 2.3</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
    <li class="start <?php echo (isset($dash) && !empty($dash)) ? 'active' : ''; ?> ">
            <?php echo $this->Html->link('<i class="fa fa-home"></i><span class="title">
                                    Dashboard
                                </span><span class="selected">
                                </span>', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>

        </li>
        <li class="has-sub <?php echo ((isset($storess)) || isset($corpss) || isset($vendorss) || isset($customerss) || isset($companies) || isset($competitorss)) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    Admin
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
			     <li class="<?php echo (isset($companies)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Companies", array('controller' => 'companies', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($corpss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Corporation", array('controller' => 'corporations', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($storess)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($vendorss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link(" <i class='fa fa-tags'></i>Vendors", array('controller' => 'customers', 'action' => 'vendor'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($customerss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-sitemap'></i>Customers", array('controller' => 'customers', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Competitors", array('controller' => 'competitors', 'action' => 'index'), array('escape' => false)); ?>
                </li>
            </ul>
        </li>
        
        <li class="has-sub <?php echo ((isset($purchase_invoice)) || (isset($rdepartments)) || (isset($shiftens)) || (isset($r_products)) || (isset($rsales_invoice)) || (isset($rwholesale_products)) ) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    R-Fuel
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo (isset($rdepartments)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Department", array('controller' => 'rfuel_departments', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($r_products)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Product", array('controller' => 'rproducts', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($rwholesale_products)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Mixes", array('controller' => 'rwholesale_products', 'action' => 'index'), array('escape' => false)); ?>
                </li>

                <li class="<?php echo (isset($purchase_invoice)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Purchase", array('controller' => 'purchase_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>



                <li class="<?php echo (isset($shiftens)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Shift End", array('controller' => 'shiftends', 'action' => 'index'), array('escape' => false)); ?>
                </li>
<!--                <li class="<?php echo (isset($rsales_invoice)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Sales", array('controller' => 'rsales_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>-->


            </ul>
        </li>


        <li class="has-sub <?php echo (($this->params['controller'] ==  'r_grocery_departments' ||  $this->params['controller'] ==  'r_grocery_items' || $this->params['controller'] ==  'r_grocery_mix_matches' ||  $this->params['controller'] ==  'r_grocery_promotions' ||  $this->params['controller'] ==  'r_grocery_invoices')) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    R-Grocery
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_departments')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Department", array('controller' => 'r_grocery_departments', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_items')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Grocery Item", array('controller' => 'r_grocery_items', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_promotions')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Promotions", array('controller' => 'r_grocery_promotions', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_invoices')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Purchase Invoice", array('controller' => 'r_grocery_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_mix_matches')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Mix And Match", array('controller' => 'r_grocery_mix_matches', 'action' => 'index'), array('escape' => false)); ?>
                </li>
            </ul>
        </li>




     
    </ul>
  </section>
  <!-- /.sidebar --> 
</aside>

<?php  /* // pr($this->params); // die; ?>
<div class="page-sidebar navbar-collapse collapse">
    <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
        <li class="sidebar-toggler-wrapper">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone">
            </div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- <form class="sidebar-search"  method="POST">
                 <div class="form-container">
                     <div class="input-box">
                         <a href="javascript:;" class="remove">
                         </a>
                         <input type="text" placeholder="Search..."/>
                         <input type="button" class="submit" value=" "/>
                     </div>
                 </div>
             </form>-->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start <?php echo (isset($dash) && !empty($dash)) ? 'active' : ''; ?> ">
            <?php echo $this->Html->link('<i class="fa fa-home"></i><span class="title">
                                    Dashboard
                                </span><span class="selected">
                                </span>', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>

        </li>
        <li class="<?php echo ((isset($storess)) || isset($corpss) || isset($vendorss) || isset($customerss) || isset($companies) || isset($competitorss)) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    Admin
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
			     <li class="<?php echo (isset($companies)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Companies", array('controller' => 'companies', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($corpss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Corporation", array('controller' => 'corporations', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($storess)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($vendorss)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link(" <i class='fa fa-tags'></i>Vendors", array('controller' => 'customers', 'action' => 'vendor'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($customerss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-sitemap'></i>Customers", array('controller' => 'customers', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($competitorss)) ? 'active' : '' ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Competitors", array('controller' => 'competitors', 'action' => 'index'), array('escape' => false)); ?>
                </li>
            </ul>
        </li>
        
        <li class="<?php echo ((isset($purchase_invoice)) || (isset($rdepartments)) || (isset($shiftens)) || (isset($r_products)) || (isset($rsales_invoice)) || (isset($rwholesale_products)) ) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    R-Fuel
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo (isset($rdepartments)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Department", array('controller' => 'rfuel_departments', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($r_products)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Product", array('controller' => 'rproducts', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($rwholesale_products)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Mixes", array('controller' => 'rwholesale_products', 'action' => 'index'), array('escape' => false)); ?>
                </li>

                <li class="<?php echo (isset($purchase_invoice)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Purchase", array('controller' => 'purchase_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>



                <li class="<?php echo (isset($shiftens)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Shift End", array('controller' => 'shiftends', 'action' => 'index'), array('escape' => false)); ?>
                </li>
<!--                <li class="<?php echo (isset($rsales_invoice)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Fuel Sales", array('controller' => 'rsales_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>-->


            </ul>
        </li>


        <li class="<?php echo (($this->params['controller'] ==  'r_grocery_departments' ||  $this->params['controller'] ==  'r_grocery_items' || $this->params['controller'] ==  'r_grocery_mix_matches' ||  $this->params['controller'] ==  'r_grocery_promotions' ||  $this->params['controller'] ==  'r_grocery_invoices')) ? 'active' : ''; ?>">
            <a href="javascript:;">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">
                    R-Grocery
                </span>
                <span class="arrow ">
                </span>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_departments')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Department", array('controller' => 'r_grocery_departments', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_items')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Grocery Item", array('controller' => 'r_grocery_items', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_promotions')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Promotions", array('controller' => 'r_grocery_promotions', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_invoices')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Purchase Invoice", array('controller' => 'r_grocery_invoices', 'action' => 'index'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (($this->params['controller'] ==  'r_grocery_mix_matches')) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>Mix And Match", array('controller' => 'r_grocery_mix_matches', 'action' => 'index'), array('escape' => false)); ?>
                </li>
            </ul>
        </li>




    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<?php */ ?>
<script>
$(document).ready(function(){
	$('.nolink').click(function(e){
		
    $(this).attr('src',$(this).attr('href'));
    e.preventDefault();
});
	
	$('.has-sub').click(function(){
		$(this).addClass('active');
		$(this).children('ul').toggle();
		$('.has-sub').not(this).children('ul').hide();
		$('.has-sub').not(this).children('ul').hide();
		$('.has-sub').not(this).removeClass('active');
	});
	
	$('.third').click(function(){
		
		$(this).parent('ul').toggle();
		$(this).children('ul').toggle();
		$('.third').not(this).children('ul').hide();

	});
});
</script>
