
<?php // pr($this->params); // die; ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas" style="border-right:1px solid #d9d9d9">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <!--<div class="pull-left image"> <?php echo $this->Html->image('avatar1_small.jpg', array('alt' => '')); ?> </div>-->
            <div class="pull-left info">
                <p><?php echo $UsersDetails['username']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="start <?php echo (isset($dash) && !empty($dash)) ? 'active' : ''; ?> ">
                <?php echo $this->Html->link('<i class="fa fa-dashboard"></i><span class="title">
          Dashboard
          </span><span class="selected">
          </span>', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>


            </li>
            <!--  <li class="has-sub">
               <a href="" class="nolink"><i class='fa fa-gavel'></i> <span>Admin</span></a>
                <ul class="">
                     
                       <li>
            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Dashboard", array('controller' => 'corporations', 'action' => 'dashboard'), array('escape' => false)); ?>
                    </li>
                  
                    <li>
            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Stores", array('controller' => 'stores', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
            <?php echo $this->Html->link(" <i class='fa fa-tags'></i>Vendors", array('controller' => 'customers', 'action' => 'vendor'), array('escape' => false)); ?>
                    </li>
                    <li>
            <?php echo $this->Html->link("<i class='fa fa-sitemap'></i>Customers", array('controller' => 'customers', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
            <?php echo $this->Html->link("<i class='fa fa-file-o'></i>Competitors", array('controller' => 'competitors', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    
                 
    
    
                </ul>
                            
          </li>-->
            <li>
                <?php echo $this->Html->link("<i class='fa fa-gavel'></i> Daily Reporting", array('controller' => 'ruby_dailyreportings', 'action' => 'index'), array('escape' => false)); ?>
            </li>
     <!--  <li class="<?php echo (isset($compliance)) ? 'active' : ''; ?>">
            <?php echo $this->Html->link("<i class='fa fa-gavel'></i><span>Compliance </span>", array('controller' => 'compliance ', 'action' => 'index'), array('escape' => false)); ?>
  </li>-->
            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-gavel'></i> <span>Compliance</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-gavel'></i> <span>Compliance </span>", array('controller' => 'compliance ', 'action' => 'index'), array('escape' => false)); ?>

                <ul> 

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-plus'></i> New lic", array('controller' => 'compliance', 'action' => 'licenselist'), array('escape' => false)); ?>
                    </li>


                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Notification", array('controller' => 'compliance', 'action' => 'notification'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Upload Store Lic", array('controller' => 'compliance', 'action' => 'upload_store_lic'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-file'></i> Report", array('controller' => 'compliance', 'action' => 'report'), array('escape' => false)); ?>
                    </li>
                </ul>

            </li>


            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-gavel'></i> <span>Gas</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  <span>Gas</span>", array('controller' => 'rfuel_departments', 'action' => 'dashboard'), array('escape' => false)); ?>

                <ul>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  <span>Dashboard</span>", array('controller' => 'rfuel_departments', 'action' => 'dashboard'), array('escape' => false)); ?>
                    </li>


                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Delivery", array('controller' => 'purchase_invoices', 'action' => 'index'), array('escape' => false)); ?>
                    </li>


                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Payment", array('controller' => 'payment', 'action' => 'index'), array('escape' => false)); ?>
                    </li>

                    <li class="third">
                        <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Inventory Adjustment</span></a>

                        <ul>
                            <li class="<?php echo (isset($daily)) ? 'active' : ''; ?>">
                                <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> Tier /Pr<br/> (Dailys Sales Report )", array('controller' => 'reportentry', 'action' => 'daily_sales'), array('escape' => false)); ?>
                            </li>
                            <li class="<?php echo (isset($treport)) ? 'active' : ''; ?>">
                                <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> Tank Report", array('controller' => 'reportentry', 'action' => 'tank_report'), array('escape' => false)); ?>
                            </li>
                            <li class="<?php echo (isset($mop)) ? 'active' : ''; ?>">
                                <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> MOP", array('controller' => 'reportentry', 'action' => 'mop'), array('escape' => false)); ?>
                            </li>
                        </ul>

                    </li>

<!--                <li class="<?php echo (isset($rsales_invoice)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Fuel Sales", array('controller' => 'rsales_invoices', 'action' => 'index'), array('escape' => false)); ?>
</li>-->
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Order", array('controller' => 'sales_invoices', 'action' => 'orders'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Vendor", array('controller' => 'vendor', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li class="third">
                        <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Settings</span></a>



                        <ul>

                            <li class="<?php echo (isset($rdepartments)) ? 'active' : ''; ?>">
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>   Fuel Department", array('controller' => 'rfuel_departments', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li class="<?php echo (isset($r_products)) ? 'active' : ''; ?>">
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>   Fuel Product", array('controller' => 'rproducts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                        </ul>


                    </li>
                    <li class="third">
                        <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Gas Report</span></a>
                        <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i><span> POS Data</span>", array('controller' => 'ruby_uprodts', 'action' => 'dashboard'), array('escape' => false)); ?>
                        <ul>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> Sales by MOP", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-pie-chart'></i> Sale By Tank No", array('controller' => 'ruby_utankts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

<!--	<li class="<?php echo (($this->params['controller'] == 'ruby_uautots')) ? 'active' : ''; ?>">
                            <?php echo $this->Html->link("<i class='fa fa-line-chart'></i> Sales by MOP", array('controller' => 'ruby_uautots', 'action' => 'index'), array('escape' => false)); ?>
</li> -->

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i> Sales HOSC", array('controller' => 'ruby_uhosets', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-calculator'></i> Sale By Service Level", array('controller' => 'ruby_userlvts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-clipboard'></i> Sale By Tier Product", array('controller' => 'ruby_utierts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-file-o'></i> Dispenser Sales", array('controller' => 'ruby_edispts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-th-large'></i> Sale By DCR No", array('controller' => 'ruby_dcrstats', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-th-list'></i> Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Gas Reconcilation", array('controller' => 'shiftends', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                </ul>

            </li>
            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-gavel'></i> <span>Other Entries</span></a>
                <ul>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Expenses", array('controller' => 'other_expenses', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Income", array('controller' => 'other_income', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Profit Withdrawals", array('controller' => 'other_withdraws', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> General Accounts", array('controller' => 'other_entries', 'action' => 'general_accounts'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Reports", array('controller' => 'other_entries', 'action' => 'reports'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Settings", array('controller' => 'other_entries', 'action' => 'settings'), array('escape' => false)); ?>
                    </li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-gavel'></i> <span>Grocery</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i> Grocery ", array('controller' => 'grocery', 'action' => 'index'), array('escape' => false)); ?>
                <ul>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Dashboard", array('controller' => 'ruby_deptotals', 'action' => 'dashboard'), array('escape' => false)); ?>
                    </li>
                    <li class="third"> <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Purchase</span></a>


                        <ul>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Invoices", array('controller' => '', 'action' => 'r_grocery_invoices'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Vendor", array('controller' => 'vendor', 'action' => 'index/grocery'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Purchase Packs", array('controller' => 'grocery', 'action' => 'purchase_packs'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Purchase Report", array('controller' => 'grocery', 'action' => 'purchase_report'), array('escape' => false)); ?>
                            </li>
                        </ul>


                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Inventory Adjustments", array('controller' => 'r_grocery_items', 'action' => 'inventory_adjustment'), array('escape' => false)); ?>
                    </li>

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Track Non scanned Item", array('action' => 'track_non_scanned_item'), array('escape' => false)); ?>
                        <ul>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Products", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Purchases", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Sales", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Actual Readings", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Adjustments", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Inventory Reports", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                        </ul>
                    </li>

                    <li class="third"> <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Reports</span></a>


                        <ul>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Inventory Summary", array('controller' => 'ruby_pluttls', 'action' => 'summary'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Starting Inventory", array('controller' => 'r_grocery_items', 'action' => 'department_report'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Profit", array('controller' => 'ruby_pluttls', 'action' => 'profit'), array('escape' => false)); ?>
                            </li>
                            <!--	<li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Profit", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                                    <li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Sales", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                            
                                    <li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Purchase", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                                    <li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Key Ratios", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                                    <li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Inventory", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                                    <li>
                            <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Inventory Reports", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>-->
                        </ul>

                    </li>
                    <li class="<?php echo (($this->params['controller'] == 'r_grocery_promotions')) ? 'active' : ''; ?>">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Promotions", array('controller' => 'r_grocery_promotions', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li class="<?php echo (($this->params['controller'] == 'r_grocery_mix_matches')) ? 'active' : ''; ?>">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Mix And Match", array('controller' => 'r_grocery_mix_matches', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Settings", array('controller' => 'r_grocery_departments', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                </ul>
            </li>

            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Pricebook</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i> <span>Pricebook</span> ", array('controller' => 'r_grocery_departments', 'action' => 'dashboard'), array('escape' => false)); ?>
                <ul>

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Products", array('controller' => 'products', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Dashboard", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Items", array('controller' => 'r_grocery_items', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Bulk Update ", array('controller' => 'r_grocery_items', 'action' => 'bulkupdate'), array('escape' => false)); ?>
                    </li>	

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Promotions", array('controller' => 'r_grocery_promotions', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Monitoring", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Item Reconcilation", array('controller' => 'ItemReconcilations', 'action' => 'index'), array('escape' => false)); ?>
                    </li>

                    <li class="third">
                        <a href="" class="nolink"><i class='fa fa-shopping-cart'></i> <span>Inventory by items</span></a>


                        <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Inventory by items",  array('controller' => 'r_grocery_items', 'action' => 'inventory_by_items'), array('escape' => false)); ?>
                        <ul>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Item inventory report", array('controller' => 'r_grocery_items', 'action' => 'inventory_by_items'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Item inventory adjustment", array('controller' => 'r_grocery_items', 'action' => 'inventory_adjustment'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Item-to-case openings ", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Price Group", array('controller' => 'r_grocery_mix_matches', 'action' => 'index'), array('escape' => false)); ?>
                    </li>

                    <li class="<?php echo (($this->params['controller'] == 'r_grocery_departments')) ? 'active' : ''; ?>">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Department", array('controller' => 'r_grocery_departments', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li class="">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i> Send to POS ", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li class="">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Gas Price", array('controller' => 'gasprice', 'action' => 'index'), array('escape' => false)); ?>
                    </li>



                </ul>
            </li>

            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Lottery/lotto</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i> Lottery/lotto ", array('controller' => 'lottery', 'action' => 'index'), array('escape' => false)); ?>
                <ul>

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i> Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i> Confirm Pack", array('controller' => 'lottery', 'action' => 'confirm_pack'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i> Activate Pack", array('controller' => 'lottery', 'action' => 'activate_pack'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>   Daily Reading", array('controller' => 'lottery', 'action' => 'daily_reading'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Add New Games ", array('controller' => 'lottery', 'action' => 'games'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Active/Confirm Report ", array('controller' => 'active_confirm_rpts', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Pack History ", array('controller' => 'pack_historys', 'action' => 'index'), array('escape' => false)); ?>
                    </li>
                    <!--<li>
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i> Settle Pack", array('controller' => 'lottery', 'action' => 'settle_pack'), array('escape' => false)); ?>
    </li>
                    <li>
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('controller' => 'lottery', 'action' => 'return_pack'), array('escape' => false)); ?>
    </li>
                    <li>
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('controller' => 'lottery', 'action' => 'pack_history'), array('escape' => false)); ?>
    </li>-->
                    <!--<li>
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('controller' => 'lottery', 'action' => 'lotto_settlements'), array('escape' => false)); ?>
    </li>-->
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('controller' => 'lottery', 'action' => 'reports'), array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  On Hand Tickets ", array('controller' => 'lottery', 'action' => 'onhand_tickets'), array('escape' => false)); ?>
                    </li>
                </ul>
            </li>




            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>POS Data</span></a>
                <ul>

                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-th-large'></i> Dashboard", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                    </li>

                    <li class="third">

                        <a href="" class="nolink"><i class='fa fa-bullhorn'></i> <span>Reports</span></a>
                        <?php //echo $this->Html->link("<i class='fa fa-bullhorn'></i><span> POS Data</span>", array('controller' => 'ruby_uprodts', 'action' => 'dashboard'), array('escape' => false)); ?>
                        <ul>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> Z-Report", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-pie-chart'></i> Department Sales by Date", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
                            </li>


                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i> Gas Sales", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-calculator'></i> Item Sale", array('controller' => 'ruby_pluttls', 'action' => 'dashboard'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-clipboard'></i> Sales Comparison", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-file-o'></i> Item Sales by Date", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Sales by Hour", array('controller' => 'ruby_deptotals', 'action' => 'sales_by_hour'), array('escape' => false)); ?>

                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-th-list'></i> Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Fuel Sales by Credit Card", array('controller' => 'ruby_deptotals', 'action' => 'credit_card_summary'), array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Tax Sales Report", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Top Sales Report", array('controller' => 'ruby_pluttls', 'action' => 'top_sales'), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Lowest Sales Report", array('controller' => 'ruby_pluttls', 'action' => 'low_sales'), array('escape' => false)); ?>
                            </li>

                            <li>
                                <?php echo $this->Html->link("<i class='fa fa-columns'></i> Department Sales", array('controller' => 'ruby_deptotals', 'action' => 'dapartment_sales'), array('escape' => false)); ?>

                            </li>


                        </ul>


                    </li>
                    <li>
                        <?php echo $this->Html->link("<i class='fa fa-columns'></i> Settings", array('controller' => 'next_version', 'action' => 'index'), array('escape' => false)); ?>

                    </li>

                </ul>
            </li>

            <li class="has-sub">
                <a href="" class="nolink"><i class='fa fa-cogs'></i> <span>Store settings</span></a>
                <?php //echo $this->Html->link("<i class='fa fa-cogs'></i> Store settings", array('controller' => 'storesettings', 'action' => 'index'), array('escape' => false)); ?>

                <ul>
                    <li class="">
                        <?php echo $this->Html->link("<i class='fa fa-bullhorn'></i>  Setup/ Syn Pricebook", array('controller' => 'storesettings', 'action' => 'index'), array('escape' => false)); ?>
                    </li> 
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar --> 
</aside>
<script>
    $(document).ready(function () {
        $('.nolink').click(function (e) {

            $(this).attr('src', $(this).attr('href'));
            e.preventDefault();
        });

        $('.has-sub').click(function () {
            $(this).addClass('active');
            $(this).children('ul').toggle();
            $('.has-sub').not(this).children('ul').hide();
            $('.has-sub').not(this).children('ul').hide();
            $('.has-sub').not(this).removeClass('active');
        });

        $('.third').click(function () {

            $(this).parent('ul').toggle();
            $(this).children('ul').toggle();
            $('.third').not(this).children('ul').hide();

        });
    });
</script>