<div class="col-md-3">
	<label>From:</label>
	<input type="text" class="form-control" placeholder="Select From Date" id="from">
</div>

<div class="col-md-3">
	<label>To:</label>
	<input type="text" class="form-control" placeholder="Select To Date" id="to">
</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable();
		//$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		//$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});
	});
</script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#from" ).datepicker({ dateFormat: 'mm-dd-yy' });
	$( "#to" ).datepicker({ dateFormat: 'mm-dd-yy' });	
  } );
  </script>
