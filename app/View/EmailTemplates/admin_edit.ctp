<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN STYLE CUSTOMIZER -->

				<!-- END BEGIN STYLE CUSTOMIZER -->
				<h3 class="page-title">
					Edit Email Template <small>Here you can edit your email template information</small>
				</h3>
				<ul class="breadcrumb">
					<li><i class="icon-home"></i> <?php echo $this->Html->link('Email Template', array('controller' => 'emailTemplates', 'action' => 'index',"admin"=>true),array("escape"=>false)); ?>

						<span class="icon-angle-right"></span></li>

					<li>Edit Email Template</li>
					
					
				</ul>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<div class="tabbable tabbable-custom boxless">

					<div class="tab-content">
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4>
									<i class="icon-reorder"></i>Edit Email Template
								</h4>

							</div>

							<div class="portlet-body form">


								<!-- BEGIN FORM-->
							<?php 
									echo $this->Form->create('EmailTemplate',array('type' => 'file','class'=>'user_form')); 
									echo $this->Form->input('id');
									
							?>
								<div class="alert alert-error hide">
									<button class="close" data-dismiss="alert"></button>
									<span>Please fill all the required fields.</span>
								</div>
								
								<div class="row-fluid">
									
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="firstName">Title</label>
											<div class="controls">
											<?php echo $this->Form->input('title', array("label" => false, "div" => false, "class" => "m-wrap span12 validate[required] inp-2","data-errormessage-value-missing" => "title is required!")); ?>
												<span class="help-block">Here you can add Title.</span>
											</div>
										</div>
									</div>
									
									<div class="span6 ">
										<div class="control-group">
										
											<label class="control-label" for="lastName">Subject</label>
											<div class="controls">
												
												<?php echo $this->Form->input('subject', array("label" => false, "div" => false, "class" => "m-wrap span12  inp-2")); ?>
												<span class="help-block">Here you can add Subject.</span>
											</div>
										</div>
									</div>


								</div>
								
								<div class="row-fluid">
									
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="firstName">From</label>
											<div class="controls">
											<?php echo $this->Form->input('from', array("label" => false, "div" => false, "class" => "m-wrap span12  inp-2")); ?>
												<span class="help-block">Here you can add From.</span>
											</div>
										</div>
									</div>
									
									<div class="span6 ">
										<div class="control-group">
										
											<label class="control-label" for="lastName">Reply To Email</label>
											<div class="controls">
												
												<?php echo $this->Form->input('reply_to_email', array("label" => false, "div" => false, "class" => "m-wrap span12  inp-2")); ?>
												<span class="help-block">Here you can add Reply To Email.</span>
											</div>
										</div>
									</div>


								</div>
								
								<div class="row-fluid">
								
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="firstName">Status</label>
											<div class="controls">
											<div class="basic-toggle-button">
											<?php echo $this->Form->input('status', array("label" => false, "div" => false, "class" => "toggle")); ?>
											</div><br />
												<span class="help-block">Here you can add Status.</span>
											</div>
										</div>
									</div>
									
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="firstName">Html</label>
											<div class="controls">
											<div class="basic-toggle-button">
											<?php echo $this->Form->input('is_html', array("label" => false, "div" => false, "class" => "toggle")); ?>
											</div><br />
												<span class="help-block">Here you can add Html On/Off.</span>
											</div>
										</div>
									</div>
									
									
								</div>
								
								<div class="row-fluid">

									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="firstName">Description</label>
											<div class="controls">
											<?php echo $this->Form->input('description', array("label" => false, "div" => false, "class" => "m-wrap span12 ckeditor inp-2")); ?>
												<span class="help-block">Here you can add Description.</span>
											</div>
										</div>
									</div>
										
								</div>
								
								<!--/row-->
								
								<div class="form-actions">
									<button type="submit" class="btn blue">
										<i class="icon-ok"></i> Save
									</button>

									<button type="button" onclick="javascript:history.back(1)"
										class="btn">Cancel</button>
								</div>
								<?php echo $this->Form->end(); ?>
								<!-- END FORM-->
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?> 
<script type="text/javascript">
 jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#EmailTemplateAdminEditForm").validationEngine({promptPosition : "bottomLeft"});
 });



	
</script>
