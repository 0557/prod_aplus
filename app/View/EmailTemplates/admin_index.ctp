<?php app::import("Model","SitePermission");
$permission_obj=new SitePermission();
//pr($users);die;

?>

<div
	class="page-content">
	
	<div class="container-fluid">
	
		<div class="row-fluid">
			<div class="span12">
			
				<h3 class="page-title">
					View Email Templates <small>Here you can manage your all the Email Templates
						listings</small>
				</h3>
				<ul class="breadcrumb">
					<li><i class="icon-home"></i> <?php echo $this->Html->link('Home', array('controller' => 'users', 'action' => 'dashboard',"admin"=>true),array("escape"=>false)); ?>
						<i class="icon-angle-right"></i>
					</li>
					<li>View Email Templates
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box light-grey">
					<div class="portlet-body">
						<div class="clearfix">
							<div class="btn-group pull-right">
							<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'EmailTemplates','is_add')) { ?>
								<span id="sample_editable_1_new" class="btn green"> <?php echo $this->Html->link('Add New Email Template <i class="icon-plus"></i>', array('controller' => 'emailTemplates', 'action' => 'admin_add',"admin"=>true),array("escape"=>false)); ?>
								</span>
						
								
								<?php } ?>
							</div>
						</div>
						<?php echo $this->Form->create('EmailTemplate'); ?>

						<div class="row-fluid">
							<div class="span6">
								<div id="sample_1_length" class="dataTables_length">
									<label> <select name="showperpage"
										onchange="$('form:first').submit();" size="1"
										aria-controls="sample_1" class="m-wrap xsmall">
										<?php foreach (Configure::read('Admin.showperpage') as $showpage): ?>
											<option value="<?php echo $showpage; ?>"
											<?php if ($showpage == $limit) { ?> selected="selected"
											<?php } ?>>
												<?php echo $showpage; ?>
											</option>
											<?php endforeach; ?>
									</select> records per page</label>
								</div>
							</div>
							<div class="span6">
								<div class="dataTables_filter" id="sample_1_filter">
									<label>Search: <input type="text" name="keyword"
										class="m-wrap medium"
										value="<?php echo (isset($this->params['named']['keyword'])) ? $this->params['named']['keyword'] : ''; ?>">
 <button type="submit" class="btn blue"> <i class="icon-ok"></i> Search </button>
									</label>
								</div>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
						<table class="table table-striped table-bordered table-hover"
							id="sample_1">
							<thead>
								<tr>
									<th style="width: 8px;"><input type="checkbox"
										class="group-checkable" data-set="#sample_1 .checkboxes" /></th>

									
									<th><?php echo $this->Paginator->sort('title'); ?></th>
									<th><?php echo $this->Paginator->sort('subject');?></th>
                                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
									 <th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
							<?php   $len = count($emailTemplates); if($len >0):
							foreach ($emailTemplates as $emailTemplate):
							?>
							
								<tr class="odd gradeX">
									
									<td><input type="checkbox" class="checkboxes"
										value="<?php echo h($emailTemplate['EmailTemplate']['id']); ?>" /></td>
									
									<td><?php echo ucfirst(($emailTemplate['EmailTemplate']['title'])); ?></td>
									<td><?php echo ucfirst(($emailTemplate['EmailTemplate']['subject'])); ?></td>
									
									<td class="center hidden-480"><?php if ($emailTemplate['EmailTemplate']['status'] == '1'): ?>
                                            <span class="label label-success">Active</span>
<?php else: ?>
                                                <span class="label">Inactive</span>
<?php endif; ?></td>
									<td class="hidden-480"><?php echo date(Configure::read('site.admin_date_format'), strtotime($emailTemplate['EmailTemplate']['created'])); ?></td>
									
									<td class="center">
								<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_read')) { ?>

									<?php echo $this->Html->link('View', array('controller' => 'emailTemplates', 'action' => 'view',"admin"=>true, $emailTemplate['EmailTemplate']['id']), array("escape" => false, "class" => "btn mini green-stripe view")); ?>
									<?php } ?>
									<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_edit')) { ?>
									<?php echo $this->Html->link('Edit', array('action' => 'edit', $emailTemplate['EmailTemplate']['id']), array("escape" => false, "class" => "btn mini green-stripe edit")); ?>
									<?php } ?> <?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_delete')) { ?>
									<?php echo $this->Form->postLink('Delete', array('action' => 'delete', $emailTemplate['EmailTemplate']['id']), array("escape" => false, "class" => "btn mini red-stripe delete"), __('Are you sure you want to delete # %s?',$emailTemplate['EmailTemplate']['id'])); ?>
									<?php } ?> 
									</td>
								</tr>
								<?php endforeach; ?>
								<?php else :?>
								<tr odd gradeX>
									<td colspan="7">No records to display</td>
								</tr>
								<?php endif; ?>

							</tbody>
						</table>

						<div class="row-fluid">
							<div class="span6">
								<div class="dataTables_info" id="sample_1_info">
								<?php
								echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>
								</div>
							</div>
							<div class="span6">
								<div class="dataTables_paginate paging_bootstrap pagination">
									<ul>
									<?php
									echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'prev disabled','disabledTag' => 'a'));
									echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'next disabled','disabledTag' => 'a'));
									?>
									</ul>

								</div>
							</div>
						</div>
						<div class="clearfix">
							<div class="btn-group ">
							<?php if($permission_obj->CheckPermission($UsersDetails['role_id'],'emailTemplates','is_delete')) { ?>
								<span id="sample_editable_1_new" class="btn green"> <?php echo $this->Html->link('Mark as Delete', 'javascript:void(0);',array("escape"=>false,"onclick"=>"MarkDelete();")); ?>
								</span>
								<?php } ?>	
							</div>
						</div>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<script type="text/javascript">
function MarkDelete(){
  var checked=0;
  jQuery("#action_row").val('delete');
  var audits	=	'';
  $('.checkboxes:checked').each(function(){
	 checked=1;			
	 audits += 	this.value+',';	 
  });
 var auditsval	=	audits.slice(0, -1);
  //alert(auditsval);
  if(checked==1){
	  var con	=	confirm('Are you sure you want to delete ?');
	  if(con){
		  jQuery.ajax({
				type: "POST",
				url: CommanPath.basePath+'admin/emailTemplates/deleteall',
				data:'ids='+auditsval,
				cache: false,
				success: function(data){
				  window.location.reload();
				} 
			});
	  }
	
  }else{
	alert("Please select atleast one to delete");  
  }
  
}
</script>