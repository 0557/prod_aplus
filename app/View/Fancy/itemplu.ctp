<style>
.noplu{ padding-left:40px;}

</style>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> 
                    </div>
                </div></div></div>
		


        <div class="form-body col-md-12">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                       <!-- <form action="<?php echo Router::url('/') ?>admin/r_grocery_items/addplu" method="post">-->
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU No:	 
                                </div>
                                <div class="col-md-7 ">
                                <input type="text" id="pul" name="plu_no" class="form-control " placeholder="Enter PLU No"  value=""/>
                                <input type="hidden" id="id" name="id" class="form-control"   value=""/>
                                </div>
                                
                                <div style="float: right;   margin-top: -36px;  margin-left: 10px;  position: absolute;    right: 0;" id="downarr"><i class="fa fa-arrow-down" aria-hidden="true" style="cursor:pointer;" id="changetax"></i></div>
                                
                            </div>

							<div class="row static-info">
                                <div class="col-md-5 name">
                                    Description:	 
                                </div>
                                <div class="col-md-7 value">
                                <input type="text" id="description" name="" class="form-control"  readonly="readonly" />
                                </div>
                            </div>
                            
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    On Hand:	 
                                </div>
                                <div class="col-md-7 value">
                                <input type="text" id="onhand" name="" class="form-control"  readonly="readonly" />
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    On Self:	 
                                </div>
                                <div class="col-md-7 value">
                                <input type="text" id="on_self" name="on_self" class="form-control" placeholder="Enter Value" />
                                <input type="hidden" id="index" name="index" class="form-control" value="<?php echo $this->Session->read('index') ?>" />
                                  
                                </div>
                            </div>

                        
                        </div>
                    </div>
                </div>
                
                <!-- END FORM-->
            </div>
        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit"  id="submit"  class="btn blue closeclick">Submit</button>
            <button type="submit" id="new" class="btn default closeclick">Save & New</button>
            <!--<button type="button" class="btn default">Cancel</button>-->
        </div>
         <!--</form>   -->
         
         <div class="noplu">
       
   			 <span style="color:#060">List of PLU successfully added to queue. </span> <br/>
             <div id="hello"> </div>
    		<!--<a href="#" onclick="parent.jQuery.fancybox.close();" class="btn btn-info upl" >Submit</a>-->
		</div>
    </div>
    <?php echo $this->form->end(); ?>
</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
	 //alert("hello");
	 var tplu = "";
	 jQuery('.noplu').hide();
		//jQuery('#pul').change(function(){
	jQuery('#downarr').on('click', function(e) {	
		var pul = $('#pul').val();
		
		jQuery.ajax({
				dataType: "json",
				url: '<?php echo Router::url('/') ?>admin/ItemReconcilations/getnewplu',
                type: 'post',
                data: {pul:pul},
                success:function(data){
					//alert(JSON.stringify(data));
					//alert(data.RGroceryItem.id);
						
					var obj = jQuery.parseJSON( JSON.stringify(data) );
					jQuery('#id').val(obj.id);
					jQuery('#on_self').val(obj.on_self);
					jQuery('#description').val(obj.description);
					
					var salesinventory=obj.salesinventory;
					
					if(salesinventory='null'){
						salesinventory=0;
					}
					
					//alert(obj.salesinventory);
					//alert(obj.purchase);
					
					// var handvalue =  ((parseFloat(obj.current_inventory) + parseFloat(obj.purchase)) - parseFloat(salesinventory));
					jQuery('#onhand').val(obj.hand_value);
					//alert(handvalue);
					
                }
            });
		});
		
		jQuery('.closeclick').click(function(){
				
				
				var id = jQuery('#id').val();
				var plu = $('#pul').val();
				var index =	jQuery('#index').val();
				var on_self =	jQuery('#on_self').val();
				var posttype = jQuery(this).attr("id"); 
			//	alert(on_self);
				if(tplu != '') {
					tplu = tplu + ', ' + plu;
				} else {
					tplu = plu;	
				}
				

				if(index==''){
					var index = 0;
					jQuery('#index').val(index);
				}else{
					var index = parseInt(index) + 1;
					jQuery('#index').val(index);
				}
				

				jQuery.ajax({
					url: '<?php echo Router::url('/') ?>admin/ItemReconcilations/addplu',
					type: 'post',
					data: {id : id, on_self : on_self, posttype : posttype,index:index},
					success:function(data){
						
						//alert(data);
						jQuery('.noplu').show();
						jQuery('#hello').html(tplu);
					
					if(posttype=='new'){
							jQuery('#pul').val("");
							jQuery('#id').val("");
							jQuery('#on_self').val("");
							jQuery('#description').val("");
							jQuery('#onhand').val("");
						}
					
						if(posttype=='submit'){
							parent.jQuery.fancybox.close();
						}
					}
           		 });
			
			});
		

    });

</script>
