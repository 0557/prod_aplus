<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-9 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Fuel Department
                    </div>
                    <div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php echo $this->Form->create('FuelDepartment', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                    <!--<form role="form">-->
                    <div class="form-body">
                       
                        
                        <div class="form-group">
                            <label for="exampleInputName">Department Name</label>
                            <?php 
							echo $this->Form->input('id');
							
							echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'id' => 'exampleInputName', 'required' => 'false', 'placeholder' => 'Enter Department Name')); ?>

                            <span class="help-block">

                            </span>
                        </div>
                      
                        

                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <?php echo $this->form->end(); ?>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>

    </div>

    <!-- END PAGE CONTENT-->
</div>
</div>
