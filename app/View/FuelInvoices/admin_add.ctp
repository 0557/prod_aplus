<script type="text/javascript">
jQuery( document ).ready(function() {  
	// validate the form when it is submitted
	$("#FuelInvoiceAdminAddForm").validate(
	{
	rules: {
				"data[FuelProduct][product_id][]": {
					required:true
				},
				"data[FuelInvoice][mop]": {
					required:true
				},
				
			},
			messages: {
				"data[FuelProduct][product_id][]" : "Please select Product.",
				"data[FuelInvoice][mop]" : "Please select MOP.",
				
			}
	}
	);
	
		$("#FuelInvoiceBol").rules("add", {
			required:true,
			messages: {
				required: "Please enter BOL#"
			}
		});
		$("#FuelInvoicePo").rules("add", {
			required:true,
			messages: {
				required: "Please enter PO#."
		}
	});
	$("#FuelInvoiceFiles").rules("add", {
			required:false,
			 extension:"csv|xls|xlsx|pdf|doc|jpg|png",
			messages: {
				required:"Please select a file",
				extension: "select only csv/xls/xlsx/pdf/doc/jpg/png."
		}
	});
	   
   
 });  
</script>

<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers','admin/bootstrap-fileinput')); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker')); ?>      

 <?php echo $this->Form->create('FuelInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
 <?php $tax_zone = array('federal' => 'Federal', 'county' => 'County', 'state' => 'State', 'municipality' => 'Municipality') ?>
<div class="portlet box blue">
               <div class="page-content portlet-body fuel-scroll" >


<div class="row">
<div class="col-xs-12">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
  
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Fuel Purchase INVOICE
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div>
                 <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
                </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Order Details
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body extra_tt">
                           <!-- <div class="row static-info">
                                <div class="col-md-5 name">
                                   BOL#
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>

                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>-->
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   BOL#: <span class="star">* </span>		 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter BOL#')); ?>

                                </div>
                            </div>
                            
<!--                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bol:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter  Bol')); ?>

                                </div>
                            </div>-->

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Load Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('load_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16','value' => date("d/m/Y - h:i") )); ?>
<!--												<input type="text" size="16" readonly class="form-control">-->
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Receving Date:
                                </div>
                                <div class="col-md-7 value">
                                
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('receving_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16','value' =>date("d/m/Y - h:i"))); ?>

                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>

                       <!-- <div class="row static-info">
                                <div class="col-md-5 name">
                                  PO#:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>

                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>-->

                           <div class="row static-info">
                                <div class="col-md-5 name">
                                  PO#: <span class="star">* </span>		 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter PO#')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Carrier:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('carrier', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Carrier')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   FEIN:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('ship_via', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter FEIN')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                 Invoice#:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('invoice', array('class' => 'form-control', 'label' => false, 'required' => 'false','placeholder'=>'Enter Invoice#')); ?>
                                </div>
                            </div>   
                            
                          
                            <div class="credit_span"><span class="span_1">MOP :<span class="star">* </span>  </span><?php echo $this->Form->input('FuelInvoice.mop', array('class' => 'select_list_mop form-control', 'onchange'=>"mop_function(this.value)" ,'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'check'))); ?></div><span class="span_2" id="MOP">
            
                            </span>
         
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Status:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>

                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Supplier Information
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Supplier:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('supplier_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $wholesale_supplier, 'empty' => 'Select Supplier')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Export Supplier:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('export_supplier', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Export Suplier')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terminal:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Terminal')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Export Terminal:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('export_terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Export Terminal')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Driver:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('driver', array('label' => false, 'required' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Driver')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Comments: 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->textarea('comments', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Comments')); ?>
                                </div>
                            </div>
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Upload:
                                    
                                    <?php //echo $this->Form->file('FuelInvoice.files'); ?> 
                                </div>
                                
                                
                                
                            <div class="col-md-7">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															 Select file
														</span>
														<span class="fileinput-exists">
															 Change
														</span>
														
                                                                                                            
				<?php echo $this->Form->file('FuelInvoice.files', array('div'=>false,'label' => false, 'required' => 'false')); ?>
                                                                                                            
													</span>
													<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
														 Remove
													</a>
												</div>
											</div>
										</div>
                            </div>
                            
                            
                            
                            
                         
                            
                        
                        </div></div></div>
               
            </div>




            <!-- END FORM-->
        </div>







        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Fuel Purchase Products
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>
                                        Products <span class="star">* </span> 	
                                    </th>
                                    <th>
                                        <!-- Vendor Price-->
                                        Gallons Delivered
                                    </th>
                                    <th>
                                        <!-- Tax Class-->
                                        Cost Per Gallons

                                    <th>
                                        Net Amount
                                    </th>

                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more">
                                <?php $total = 0;
								//print_r($this->request->data);								
								?>                                 
                               <?php if (isset($this->request->data['FuelProduct']['product_id']) && !empty($this->request->data['FuelProduct']['product_id'])) {								
							   ?>
                                    <?php foreach ($this->request->data['FuelProduct']['product_id'] as $key => $data) { $nameAndId = $this->Custom->GetProductName($data); ?>
                                        <tr>
                                            
                                            <td><?php echo  $nameAndId['WholesaleProduct']['name'];  echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'id' => false, 'value' =>  $this->request->data['FuelProduct']['product_id'][$key],  'label' => false, 'type' => 'hidden',  'required' => false)); ?>
                                            </td>
                                            
                                            <td class="max_open" ><?php echo $this->Form->input('FuelProduct.gallons_delivered.', array('label' => false, 'value' =>  $this->request->data['FuelProduct']['gallons_delivered'][$key], 'class' => 'form-control', 'onkeyup' => 'quantity_amount()', 'type' => 'text', 'placeholder' => '0')); ?>
                                            </td>
                                            <td class="enter_product_quantity"><?php echo $this->Form->input('FuelProduct.cost_per_gallon.', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'id' => '', 'value' => $this->request->data['FuelProduct']['cost_per_gallon'][$key], 'onkeyup'=>'quantity_amount()', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
                                            <td class="net_amount"><?php echo $this->Form->input('FuelProduct.net_ammount.', array('label' => false, 'class' => 'form-control', 'value' => $this->request->data['FuelProduct']['net_ammount'][$key], 'type' => 'text','readonly'=>true)); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                        <?php
                                    }
                                } else {								
                                    ?>                                
                                <tr>
                                    <td><?php 									
									//print_r($product);die;									
									echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Product', 'required' => false, "options" => $product)); ?></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <button type="button" class="btn blue" onclick="AddProduct();" style="float:right;">Add More</button>
                    
                    <br/>
                    <br/>
                     <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Tax Zone</th>
                                    <th>USA State</th>
                                    <th>Tax Description</th>   
                                    <th>Tax on Gallon Delivered(Qty)</th>
                                    <th>Rate on Gallon</th>
                                    <th>Net Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="gallon_add_more">
                                <?php $total = 0; ?>
                                <?php if (isset($this->request->data['TaxeZone']['tax_zone']) && !empty($this->request->data['TaxeZone']['tax_zone'])) { ?>
                                    <?php foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $data) { ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $data, 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?></td>
                                            <?php
                                            $request_state =array();
                                            if (isset($data) && !empty($data)) {
                                                $request_state = $this->Custom->getTaxState($data);
                                            }
                                            ?>

                                            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $this->request->data['TaxeZone']['state_id'][$key], 'empty' => 'Select State', 'required' => false, 'options' => $request_state)); ?></td>
                                             <?php
                                             $request_description = array();
                                            if (isset( $this->request->data['TaxeZone']['tax_decription'][$key]) && !empty( $this->request->data['TaxeZone']['tax_decription'][$key])) {
                                                $request_description = $this->Custom->getTaxDescription($this->request->data['TaxeZone']['state_id'][$key],$data);
                                            }
                                            ?>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));', 'options'=>$request_description, 'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'selected' => $this->request->data['TaxeZone']['tax_decription'][$key], 'required' => 'false')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'class' => 'form-control SalesInvoiceTaxOnGallonQty', 'onkeyup' => 'percentage1($(this));', 'value' => $this->request->data['TaxeZone']['tax_on_gallon_qty'][$key], 'type' => 'text', 'placeholder' => '0')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide ', 'type' => 'text', 'label' => false, 'onkeyup' => 'percentage($(this));', 'value' => $this->request->data['TaxeZone']['rate_on_gallon'][$key], 'id' => 'rate_on_gallon', 'required' => 'false')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'class' => 'form-control value_after_tax', 'value' => $this->request->data['TaxeZone']['tax_net_amount_gallon'][$key], 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?></td>
                                        <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select State', 'required' => false)); ?></td>
                                        <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));',  'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
                                        <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'onkeyup' => 'percentage1($(this));', 'class' => 'form-control SalesInvoiceTaxOnGallonQty first_class second_class', 'type' => 'text', 'placeholder' => '0')); ?></td>
                                        <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide', 'type' => 'text', 'label' => false, 'id' => 'rate_on_gallon', 'onkeyup' => 'percentage($(this));', 'required' => 'false', 'value' => '0', 'placeholder' => '0')); ?></td>
                                        <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'class' => 'form-control value_after_tax', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
                                        <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                    </tr>
<?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" class="btn blue" onclick="gallonAddProduct();" style="float:right;">Add More</button>
                </div>
            </div>

        </div>

        <ul class="list-unstyled amounts total_last">
            <li class="last_content_first field-space col-md-4 col-xs-6">
                <span class="span_1">Gallon Delivered Total </span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $this->Form->input('max_qnty', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_two col-md-4 col-xs-6">
                <span class="span_1">Net Amount Total</span><span class="span_2" id="Net_Amount_Total"><?php echo $this->Form->input('total_invoice', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
            </li>
            <li class="last_content_three col-md-4 col-xs-6">
                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['FuelInvoice']['taxes'])) ? $this->request->data['FuelInvoice']['taxes']: '0' , 'onblur'=>"getvalue()" ,'class' => 'form-control', 'type' => 'text', 'placeholder' => '0','onkeyup'=>'getvalue()')); ?></span>
            </li>
            <li class="last_content_four col-md-4 col-xs-6">
                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('gross_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
            </li>
            <li class="last_content_four col-md-4 col-xs-6">
                <span class="span_1">Discount</span><span class="span_2" id="discount"><?php echo $this->Form->input('discount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0','onkeyup'=>'total_amount()')); ?></span>
            </li>
            <li class="last_content_four col-md-4 col-xs-6">
                <span class="span_1">Total Amount</span><span class="span_2" id="total_amount"><?php echo $this->Form->input('total_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
            </li>            

            <!--       <li>
                       <span class="span_1">MOP</span><span class="span_2" id="MOP">
<?php echo $this->Form->input('FuelInvoice.mop', array('class' => 'form-control exampleInputName', 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'check'))); ?>
                    </span>
                    </li>-->

        </ul>

    </div>
    <div class="form-actions fuel-button" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'empty' => 'Select Product', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', /* 'multiple' => true, */ /* 'multiple' => 'checkbox', */ "options" => $product)); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>
<!-- END SAMPLE FORM PORTLET-->
<table style="display:none">
    <tbody id="gallon_AddMore">
        <tr>
            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select State', 'required' => false)); ?></td>

            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));',  'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'value' => 0, 'class' => 'form-control SalesInvoiceTaxOnGallonQty  abcd', 'onkeyup' => 'percentage1($(this));', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide', 'onkeyup' => 'percentage($(this));', 'type' => 'text', 'label' => false, 'id' => '', 'required' => 'false', 'value' => '0', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'value' => 0, 'class' => 'form-control value_after_tax', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>


<!-- END PAGE CONTENT-->
<!--</div>-->
<script type="text/javascript">
function total_amount() {   
    var discount=$('#FuelInvoiceDiscount').val();	
	if(discount!='' && !$.isNumeric(discount)){
		alert('Please enter numeric number!');	
		$('#FuelInvoiceDiscount').removeAttr('value');
		exit;
	}	
    
 
    var discount = parseFloat(document.getElementById("FuelInvoiceDiscount").value);
    var gross = (document.getElementById("FuelInvoiceGrossAmount").value);
    (document.getElementById("FuelInvoiceTotalAmount")).value = (gross-discount);
};
</script>

<script>
    
    function mop_function(value){
       $('#MOP').text(value);
    }
    
    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }
    $(document).ready(function () {
        // initiate layout and plugins
        //App.init();
        ComponentsPickers.init();
        getvalue();
        

    });    
    
    $(document).delegate('.product_ajax', 'change', function () {
        var category = jQuery(this).val();            
        $('.product_ajax').removeClass('active');
        $(this).addClass('active');
        var avg = jQuery.ajax({
            url: '<?php echo Router::url('/'); ?>fuel_invoices/products/' + category,
            processData: false,
            type: 'POST',
            success: function (data) {
				//alert(data);
                $('.product_ajax.active').parents('tr').html(data);
                getvalue();
            },
        });


    });

    function getvalue() {
		
        var Gross_Amount = 0;
        var max_qnty = 0;
        var max_total = 0;
        var tax = 0;
//    $('td.max_qnty').each(function(){        
//        max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).text());        
//    });
//
//    $('td.max_open').each(function(){        
//        max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());        
//    });

        $('td.net_amount').each(function () {
            max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).find('input').val());
            //max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).text());
        });
        //alert(max_qnty);
        if(max_qnty == null || max_qnty == '' || isNaN(max_qnty)){
                
            max_qnty = 0;
        }

        $('td.max_open').each(function() {
            max_total = parseFloat(max_total) + parseFloat(jQuery(this).find('input').val());
           
            if(max_total == null || max_total == '' || isNaN(max_total)){
                
            max_total = 0;
        }
            //max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());
        });
        tax = $('#FuelInvoiceTaxes').val();
        if(tax == null || tax == ''){
            tax = 0;
        }
        Gross_Amount = parseFloat(max_qnty + parseFloat(tax));


        $('#FuelInvoiceMaxQnty').val(max_total);

        $('#FuelInvoiceTotalInvoice').val(max_qnty);
        $('#FuelInvoiceGrossAmount').val(Gross_Amount);
       
    }

    function quantity_amount() {
		
		var gallons_delivered=$('#FuelProductGallonsDelivered').val();	
		if(gallons_delivered!='' && !$.isNumeric(gallons_delivered)){
		alert('Please enter numeric number!');	
		$('#FuelProductGallonsDelivered').removeAttr('value');
		exit;
		}
		
		var cost_per_gallon=$('#FuelProductCostPerGallon').val();	
	if(cost_per_gallon!='' && !$.isNumeric(cost_per_gallon)){
		alert('Please enter numeric number!');	
		$('#FuelProductCostPerGallon').removeAttr('value');
		exit;
	}
		
		
        var product_amount = 0;
        var total_amt_final = 0;
        var max_open = 0;
       
        $('td.max_open').each(function () {
           max_open = parseFloat(jQuery(this).find('input').val());
            if(max_open == null || max_open == '' || isNaN(max_open) ){
            max_open = 0;
        }
           
            //var max_open = parseFloat(jQuery(this).text());
            var max_open_qty = parseFloat(jQuery(this).next('td').find('input').val());
            var total_amt = parseFloat(max_open * max_open_qty);
           // alert(total_amt);
            if(total_amt == null || total_amt == '' || isNaN(total_amt) ){
            total_amt = 0;
        }
        
        
            jQuery(this).next('td').next('td').find('input').val(total_amt);
            total_amt_final = total_amt_final + total_amt;
            //alert(total_amt_final);

            product_amount = parseFloat(max_open) + parseFloat(jQuery(this).text());
            //alert(product_amount);
        });
     //   $('#FuelInvoiceMaxQnty').val(max_total);
        $('#FuelInvoiceTotalInvoice').val(total_amt_final);
         getvalue();

    }

    $(document).delegate('.Delete_product', 'click', function () {
        $(this).parents('tr').remove();
        getvalue();
    });

</script>
<script>
    function percentage_final() {
        var total_pr = 0;
        $('.value_after_tax').each(function () {
            total_pr = total_pr + parseFloat($(this).val());
        });
        if (!isNaN(total_pr)) {
            $('#FuelInvoiceTaxes').val(total_pr);
        }
        getvalue();
    }

    function percentage(currentval) {
		var rate_on_gallon=$('#rate_on_gallon').val();	
		if(rate_on_gallon!='' && !$.isNumeric(rate_on_gallon)){
		alert('Please enter numeric number!');	
		$('#rate_on_gallon').removeAttr('value');
		exit;
		}
		
        $('.FuelInvoiceTaxOnGallonQty').removeClass('first_class');
        $('.FuelInvoiceTaxOnGallonQty').removeClass('second_class');
        var value = currentval.val();

        var total = currentval.parents('tr').find('td:nth-child(4)').find('input').val();
        // var total = $('.SalesInvoiceTaxOnGallonQty').val();
        var farr = parseFloat(total) * value;

        var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(farr.toFixed(2));
        percentage_final();

    }

    function percentage1(currentval) {
		var tax_gallon_qty=$('#TaxeZoneTaxOnGallonQty').val();	
		if(tax_gallon_qty!='' && !$.isNumeric(tax_gallon_qty)){
		alert('Please enter numeric number!');	
		$('#TaxeZoneTaxOnGallonQty').removeAttr('value');
		exit;
		}			
		
        $('.FuelInvoiceTaxOnGallonQty').removeClass('first_class');
        $('.FuelInvoiceTaxOnGallonQty').removeClass('second_class');
        var value = currentval.val();

        var total = currentval.parents('tr').find('td:nth-child(5)').find('input').val();
        // var total = $('.SalesInvoiceTaxOnGallonQty').val();
        var farr = parseFloat(value) * total;
          // alert(farr);
        var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(farr.toFixed(4));
        percentage_final();

    }

    function TaxZone(currentval) {
        var value = currentval.val();
        var target = currentval.parents('tr').find('td:nth-child(2)');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: SITEURL + 'admin/sales_invoices/taxzone_state/',
            data: 'tax_zone=' + value,
           /* beforeSend: function () {
                App.blockUI({
                    target: target,
                    iconOnly: true
                });
            },*/
            success: function (data) {
                //App.unblockUI(target);
                var html = '<option value="">Select State</option>';
                $.each(data, function (key, value) {
                    html += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $(target).find('select').html(html);
            }
        });

    }


    function TaxDescription(currentval) {
        var target = currentval.parents('tr').find('td:nth-child(3)');
        var state_id = currentval.val();
        var tax_zone = currentval.parents('td').prev('td').find('select').val();
        if (tax_zone != '' && tax_zone != '') {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SITEURL + 'admin/sales_invoices/tax_description/'+state_id+'/'+tax_zone,
              /*  beforeSend: function () {
                    App.blockUI({
                        target: target,
                        iconOnly: true
                    });
                },*/
                success: function (data) {
                   // App.unblockUI(target);
                    console.log(data);
                    var html = '<option value="">Select Description</option>';
                    $.each(data, function (key, value) {
                        html += '<option value=' + value + '>' + value + '</option>';
                    });
                    $(target).find('select').html(html);
                }
            });
        }
    }
    
    function TaxRate(currentval) {
        
        var target = currentval.parents('tr').find('td:nth-child(5)');
        var taxDescription = currentval.find('option:selected').text();
        var tax_zone = currentval.parents('td').prev('td').find('select').val();
        if (tax_zone != '' && tax_zone != '') {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SITEURL + 'admin/sales_invoices/tax_rate',
                data: 'tax_desctiption=' + taxDescription,
               /* beforeSend: function () {
                    App.blockUI({
                        target: target,
                        iconOnly: true
                    });
                },*/
                success: function (data) {
                   // App.unblockUI(target);
                    $(target).find('input').val(data);
                     var taxqty =$('#TaxeZoneTaxOnGallonQty').val();
                     var rategallon =$('#rate_on_gallon').val();                  
					
					var TaxeZoneTaxNetAmountGallon = parseFloat(taxqty) * parseFloat(rategallon);
					var TaxeZoneTaxNetAmountGallonValue = TaxeZoneTaxNetAmountGallon.toFixed(4);
					
					$('#TaxeZoneTaxNetAmountGallon').val(TaxeZoneTaxNetAmountGallonValue);
					$('#FuelInvoiceTaxes').val(TaxeZoneTaxNetAmountGallonValue);
					getvalue();
				
                }
            });
        }
    }
    function mop_function(value) {
        $('#MOP').text(value);
    }

    function gallonAddProduct() {
        $("#gallon_add_more").append($("#gallon_AddMore").html());
        var max_qntyc = parseFloat(jQuery('#SalesInvoiceMaxQnty').val());

        $('.abcd').attr('value', max_qntyc);
        $('.FuelInvoiceTaxOnGallonQty').removeClass('abcd');
        // $('.SalesInvoiceTaxOnGallonQty').attr('value', max_qntyc);
        getvalue();
    }

    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }

    $(document).ready(function () {
        // initiate layout and plugins

        ComponentsPickers.init();
        getvalue();
        percentage_final();
        //quantity_amount();
    });


    $('#same_as_address').change(function () {

        if ($(this).prop('checked')) {
            $('#SalesInvoiceShipTo').val($('#SalesInvoiceBillTo').val());
            $('#SalesInvoiceShipTo').prop('readonly', true);
            $('#SalesInvoiceShipingAddress').val($('#SalesInvoiceBillingAddress').val());
            $('#SalesInvoiceShipingAddress').prop('readonly', true);
        }
        else {
            $('#SalesInvoiceShipTo').val('');
            $('#SalesInvoiceShipTo').prop('readonly', false);
            $('#SalesInvoiceShipingAddress').val('');
            $('#SalesInvoiceShipingAddress').prop('readonly', false);
        }
    });


    function getvalue() {		
	var taxes=$('#FuelInvoiceTaxes').val();	
	if(taxes!='' && !$.isNumeric(taxes)){
		alert('Please enter numeric number!');	
		$('#FuelInvoiceTaxes').removeAttr('value');
		exit;
	}	
		
        var Gross_Amount = 0;
        var max_qnty = 0;
        var max_total = 0;
        var tax = 0;


        $('td.net_amount').each(function () {
            max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).find('input').val());
        });

        if (max_qnty == null || max_qnty == '' || isNaN(max_qnty)) {

            max_qnty = 0;
        }

        $('td.max_open').each(function () {
            max_total = parseFloat(max_total) + parseFloat(jQuery(this).find('input').val());
            //   alert(max_total);
            if (max_total == null || max_total == '' || isNaN(max_total)) {

                max_total = 0;
            }
            //max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());
        });
        tax = $('#FuelInvoiceTaxes').val();
        if (tax == null || tax == '') {
            tax = 0;
        }
        Gross_Amount = parseFloat(max_qnty + parseFloat(tax));

        $('.first_class').val(max_total);

        $('#FuelInvoiceMaxQnty').val(max_total);

        $('#FuelInvoiceTotalInvoice').val(max_qnty);
        $('#FuelInvoiceGrossAmount').val(Gross_Amount);
        
        discount =$('#FuelInvoiceDiscount').val();
        if (discount == null || discount == '') {
            discount = 0;
        }
        gross =$('#FuelInvoiceGrossAmount').val(); 
        
         TotalAmount = parseFloat(gross - parseFloat(discount));
     
      $('#FuelInvoiceTotalAmount').val(TotalAmount.toFixed(4));

    }

    

    $(document).delegate('.Delete_product', 'click', function () {
        $(this).parents('tr').remove();
        getvalue();
    }
    );

    $('#SalesInvoiceStoreId , #SalesInvoiceCorporationId ').change(function () {
        var bill_to = $(this).find("option:selected").text();
        if (bill_to != 'Select Corporation' && bill_to != 'Select Store') {
            $('#SalesInvoiceBillTo').val(bill_to);
            $('#SalesInvoiceCustomerId').prop('disabled', 'disabled');
        } else {
            $('#SalesInvoiceCustomerId').prop('disabled', false);
        }
    });

    $('#SalesInvoiceCustomerId').change(function () {

        var bill_to = $(this).find("option:selected").text();
        if (bill_to != 'Select Customer') {
            $('#SalesInvoiceBillTo').val(bill_to);
            $("#SalesInvoiceStoreId").prop('disabled', 'disabled');
            $("#SalesInvoiceCorporationId").prop('disabled', 'disabled');
        } else {
            $("#SalesInvoiceStoreId").prop('disabled', false);
            $("#SalesInvoiceCorporationId").prop('disabled', false);
        }

    });

    $('#SalesInvoiceCorporationId').change(function () {
        var corporation_id = $(this).val();
        $('#SalesInvoiceCustomerId').val('');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "<?php echo Router::url('/') ?>admin/sales_invoices/filter_store/" + corporation_id,
            data: 'corporation_id=' + corporation_id,
            success: function (data) {
                var html = '<option value="">Select Store</option>';
                $.each(data, function (key, value) {
                    html += '<option value=' + key + '>' + value + '</option>';
                });
                $('#SalesInvoiceStoreId').html(html);
            }
        });

    });

</script>
