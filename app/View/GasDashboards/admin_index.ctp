<!--<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" > -->
         
       <div class="row" style=" margin-left:-7px">
            <div class="col-md-12">               
                <h3 class="page-title" >
                    Gas Dashboard		
                </h3>
            
            </div>
        </div> 
	 
        <?php 
		$storeId=$this->Session->read('stores_id');	
		$sale=array();
		for($m=1;$m<=12;$m++){	

		
		$pluttls1= ClassRegistry::init('PollingDashboard')->query("SELECT SUM(`PollingDashboard`.`fuel_value`) as flu FROM `ruby_uprodts` AS `PollingDashboard` LEFT JOIN `ruby_headers` AS `RubyHeader` ON (`PollingDashboard`.`ruby_header_id` = `RubyHeader`.`id`) WHERE `RubyHeader`.`store_id` = ".$storeId." AND MONTH(`RubyHeader`.`ending_date_time`) = ".$m." AND YEAR(`RubyHeader`.`ending_date_time`) = YEAR(CURDATE()) AND `fuel_product_number` IN (1, 4, 5, 6)"); 
	
		if($pluttls1[0][0]['flu']==''){
			$pluttls1[0][0]['flu']=0;
		}
			$total_dlllar_soldaa=$pluttls1[0][0]['flu'];
			

				 array_push($sale,$total_dlllar_soldaa);  
		}
		
		
				
		$pro=array();
		
		for($m=1;$m<=12;$m++){		
		 $fetch= ClassRegistry::init('PollingDashboard')->query("SELECT SUM(total_invoice) as cost_of_product from `purchase_invoices`   WHERE MONTH(receving_date)=".$m." AND YEAR(receving_date)=YEAR(CURDATE())  ");   
		
		if($fetch[0][0]['cost_of_product']==''){
			$fetch[0][0]['cost_of_product']=0;
		}
		
		
		array_push($pro,$fetch[0][0]['cost_of_product']); 
		}
	
		//die;
		 ?>
        
          <div id="chart_div" style="width:100%; height: 400px; margin:0 auto; margin-left:8px">          
        
      <!--  <img src="<?php echo $this->webroot; ?>images/graph.png"/ > -->
        
        </div>
        
            
     <!-- </div>
   </div>
</div>-->
<style>
.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }
button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": false
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
		$('#end_date').Zebra_DatePicker({format:"Y-m-d"});
	});
</script>


 <?php 
$full_date = $this->Session->read('full_date'); 
if($full_date=='')
{
?>        
<script>
$(document).ready(function(){
$('#config-demo').val('');		
});
</script>
<?php 
}
?>   
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load("current", {'packages':["bar"]});
	google.charts.setOnLoadCallback(drawChart);
	
      function drawChart() {
         var data = google.visualization.arrayToDataTable([
          ['Months','Purchase at cost','Sales at retail'],        
          ['Jan', <?php echo $pro[0] ; ?>, <?php echo $sale[0] ; ?>],
          ['Feb', <?php echo $pro[1] ; ?>,<?php echo $sale[1] ; ?>],
          ['Mar', <?php echo $pro[2] ; ?>,<?php echo $sale[2] ; ?>],
		  ['Apr', <?php echo $pro[3] ; ?>,<?php echo $sale[3] ; ?>],
          ['May', <?php echo $pro[4] ; ?>,<?php echo $sale[4] ; ?>],
          ['Jun', <?php echo $pro[5] ; ?>,<?php echo $sale[5] ; ?>],
          ['Jul', <?php echo $pro[6] ; ?>,<?php echo $sale[6] ; ?>],
		  ['Aug', <?php echo $pro[7] ; ?>,<?php echo $sale[7] ; ?>],
          ['Sep', <?php echo $pro[8] ; ?>,<?php echo $sale[8] ; ?>],
          ['Oct', <?php echo $pro[9] ; ?>,<?php echo $sale[9] ; ?>],
          ['Nov',<?php echo  $pro[10] ; ?>,<?php echo $sale[10] ; ?>],
		  ['Dec', <?php echo $pro[11] ; ?>,<?php echo $sale[11] ; ?>],
        ]);
		
		/* function drawChart() {
         var data = google.visualization.arrayToDataTable([
          ['Months', 'Purchase at cost', 'Sales at retail'],        
          ['Dec', 0,0],
          ['Jan', 0,0],
          ['Feb', 0,0],
          ['Mar', 0,0],
		  ['Apr', 0,0],
          ['May', 0,0],
          ['Jun', 0,15.2],
          ['Jul', 12428.97,3031.7],
		  ['Aug', 7597.33,13697.13],
          ['Sep', 11300.08,11975.36],
          ['Oct', 9034.39,8747.93],
          ['Nov', 7621.81,4660.09],
        ]);
		*/
		
		
		
		var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);


        var options = {
          chart: {
            title: 'Gas Overview',
            subtitle: 'Gas sales at retail and purchase at cost',
			width:900
          },     
		 };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
    