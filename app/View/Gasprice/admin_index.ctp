<div class="portlet box blue">
               <div class="page-content portlet-body" >
<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Change Gas Price
							</div>
							
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Grade'); ?></th>
								<th><?php echo $this->Paginator->sort('Weighted cost '); ?></th>
								<th><?php echo $this->Paginator->sort('Current set cash price'); ?></th>
								<th><?php echo $this->Paginator->sort('New cash price'); ?></th>
								<th><?php echo $this->Paginator->sort('Last credit price'); ?></th>
								<th><?php echo $this->Paginator->sort('New credit price'); ?></th>
								
						</tr>
						</thead>
							<tbody><tr><td class="col-md-2">
                                    Regular
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
											
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Plus
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Super
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Premium
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Diesel
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Kerosene
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											<tr><td class="col-md-2">
                                    Off Road
                                  </td>
								  <td class="col-md-1">$0.000</td>
								  <td class="col-md-1">$0.000</td>
								  <td>
								  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
										</td><td class="col-md-1">$0.000</td><td>  <div class="input-icon">
											<i class="fa fa-dollar"></i>
											<input type="text"  class="form-control">
										</div>
											</td>
											</tr>
											</tbody>
						
						</table>
						
											 
                            </div>
                          
						  <?php echo $this->Form->input('Send Price Changes to Register', array('class' => 'btn green ', 'required' => 'false', 'label' => false,'type'=>'submit', 'value' => 'Send price changes to register')); ?>
            </div>
						</div>
						</div>
						</div>
						