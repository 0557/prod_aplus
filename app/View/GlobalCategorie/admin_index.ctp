<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Global Category
							</div>
							
						</div>
						<div class="portlet-body">
								<?php echo $this->Form->create('GlobalCategorie', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                     
					   <div class="row">
		  <div class="col-md-12">
	 <div class="col-md-4">
		  <label>Enter Global Category Name</label>
		<?php if($id){
			echo $this->Form->input('id', array('class' => 'form-control','type'=>'hidden', 'label' => false, 'required' => true, 'id' =>'id','value'=>$id));
			echo $this->Form->input('g_category', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'name','value'=>$name));
			}else{
				echo $this->Form->input('g_category', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'pul','placeholder'=>'Category'));
				}
		  ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('submit', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
							
            </div> <?php echo $this->form->end(); ?>
						</div>

<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Global Category List
							</div>
							
						</div>
						<div class="portlet-body">


					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
							
								<th><?php echo $this->Paginator->sort('Category'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody>
						<?php if (isset($glist) && !empty($glist)) { ?>
                                            <?php foreach ($glist as $data) { ?>
						<tr>
										
                                                    <td><?php echo h($data['GlobalCategorie']['g_category']); ?>&nbsp;</td>
                                                 <td>
                                                     
                                                        <?php echo $this->Html->link("<i class='fa fa-pencil'></i>", array('action' => 'index', $data['GlobalCategorie']['id'],$data['GlobalCategorie']['g_category']), array('escape' => false,'class' => 'btn default btn-xs red-stripe edit')); ?>
                                                        <?php echo $this->Form->postLink(__("<i class='fa fa-trash'></i>"), array('action' => 'delete', $data['GlobalCategorie']['id']), array('escape' => false,'class' => 'btn default btn-xs red-stripe delete')); ?>
    <?php echo $this->Form->postLink(__("<i class='fa fa-cloud-upload'></i> export global UPC"), array('action' => 'exportglobalcategory', $data['GlobalCategorie']['g_category']), array('escape' => false,'class' => 'btn default btn-xs red-stripe delete')); ?>
                                                       
                                                    </td>
                                                  
							
						</tr>
					<?php } }?>
						</tbody>
						</table>
                            </div>





						</div>

