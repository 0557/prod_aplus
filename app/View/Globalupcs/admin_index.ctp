<?php
$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name')));
?>
				
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
				  
				<?php echo $this->Form->create('Globalupcs', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'enctype'=>'multipart/form-data' ,'div' => false, 'required' => false))); ?>
				
				
				
			 <div class="portlet-body col-md-1">
			 <label><i class="fa fa-file"></i> File</label> 
</div>			 
			 <div class="portlet-body col-md-3">
			 
			 <?php echo $this->Form->input('files', array('class' => 'textfile' ,'label' => false, 'required' => true, 'type' => 'file')); ?>
			  </div>
	
				 <div class="portlet-body col-md-4">

				 <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Import</button>
</div>				
               
						    <?php echo $this->form->end(); ?>
			<p><a href="http://aplus1.net<?php echo Router::url('/'); ?>files/globalupcsample/060850_samplefile.xlsx" download><i class="fa fa-download"></i> Sample File</a></p>		
			
		</div>
					<!-- END SAMPLE TABLE PORTLET-->
	</div>

<div class="row">&nbsp;</div>

<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Globalupc
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
																<i class="fa fa-calendar"></i> <?php if(isset($enddate)) echo date("m-d-Y", strtotime($enddate));?>													
							</div>
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
							
								<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
								<th><?php echo $this->Paginator->sort('Description'); ?></th>
								<th><?php echo $this->Paginator->sort('Category'); ?></th>
								<th><?php echo $this->Paginator->sort('Cost'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody>
						<?php if (isset($Globalupcslist) && !empty($Globalupcslist)) { ?>
                                            <?php foreach ($Globalupcslist as $data) { ?>
						<tr>
										
                                                    <td><?php echo h($data['Globalupcs']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['description']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['category']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['cost']); ?>&nbsp;</td>
                                                 <td>
                                                     
                                                        <?php echo $this->Html->link("<i class='fa fa-pencil'></i>", array('action' => 'edit', $data['Globalupcs']['id']), array('escape' => false,'class' => 'btn default btn-xs red-stripe edit')); ?>
                                                        <?php echo $this->Form->postLink(__("<i class='fa fa-trash'></i>"), array('action' => 'delete', $data['Globalupcs']['id']), array('escape' => false,'class' => 'btn default btn-xs red-stripe delete')); ?>
                                                       
                                                    </td>
                                                  
							
						</tr>
					<?php } }?>
						</tbody>
						</table>
                            </div>
                           
            </div>
						</div>
						