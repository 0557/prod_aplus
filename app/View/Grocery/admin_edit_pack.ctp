<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Edit Purchase Pack
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <div class="col-md-2 col-xs-6">
                    <label>Vendor</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('id', array('id' => 'vendor', 'class' => 'form-control','type' => 'hidden','label' => false, 'required' => true)); ?>
				
				<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'default'=>$list['PurchasePacks']['vendor'], 'label' => false, 'required' => true)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'value'=>$list['PurchasePacks']['Item_Scan_Code'],'required' => true)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Item_Name', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Item_Name'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Units Per Case</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Units_Per_Case'])); ?>
                  </div>
				  
  				   <div class="col-md-2 col-xs-6">
                    <label>Case Cost</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Case_Cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Case_Cost'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Net Cost</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Net_Cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Net_Cost'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Discount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Discount', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Discount'])); ?>
                  </div>
				  	 <div class="col-md-2 col-xs-6">
                    <label>Rebate</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Rebate', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Rebate'])); ?>
                  </div>
				    <div class="col-md-2 col-xs-6">
                    <label>Case Retail</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Case_Retail', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Case_Retail'])); ?>
                  </div>
				     <div class="col-md-2 col-xs-6">
                    <label>Margin</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Margin', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Margin'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label></label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Submit', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => true)); ?>
                  </div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
		
	</div>
	</div>