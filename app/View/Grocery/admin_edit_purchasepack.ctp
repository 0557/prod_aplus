<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Edit Purchase Pack
							</div>
						
						</div>
<div class="portlet-body">
 <?php echo $this->Html->script('admin/select2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'admin/select2',
	         'admin/select2-metronic',
        ));
        ?> 
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                
                <?php echo $this->Form->input('id', array('id' => '', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['PurchasePacks']['id'], 'required' => true)); ?>
                
                <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['PurchasePacks']['invoice'])); ?>
                	
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['PurchasePacks']['vendor'])); ?>
					
				
				 
                  
                  
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code <span style="color:red">*</span></label>                   
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'pul', 'class' => 'form-control','type' => 'text', 'label' => false, 'value'=>$list['PurchasePacks']['Item_Scan_Code'],'required' => true,'disabled' => true)); ?>
                  </div>
				 
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name <span style="color:red">*</span></label>                  
				
                    <?php 					
					//pr($item);
					echo $this->Form->input('Item_Name', array(
					'id' => 'description', 'class' => 'form-control select2me','type' => 'select','label' => false,'required' => false,
                    'options' =>$item,
                    'empty' => 'Select Item',
					'value'=>$list['PurchasePacks']['Item_Name']
                    ));
					?>                     
                    
                  </div>                
             
                  
				   <div class="col-md-2 col-xs-6">
                    <label>Item#</label>                 
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'value'=>$list['PurchasePacks']['Item#'],'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Case Cost <span style="color:red">*</span></label>                  
					<?php echo $this->Form->input('Case_Cost', array('id' => 'Case_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Case_Cost'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Units Per Case <span style="color:red">*</span></label>                  
                	
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'Units_Per_Case', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['PurchasePacks']['Units_Per_Case'])); ?>
                  </div>                

                  
				   <div class="col-md-2 col-xs-6">
                    <label>Case Retail</label>              
					<?php echo $this->Form->input('Case_Retail', array('id' => 'Case_Retail', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['PurchasePacks']['Case_Retail'])); ?>
                   </div>				
                  
                   <div class="col-md-2 col-xs-6">
                    <label>Purchase Quantity</label>                  	
					<?php echo $this->Form->input('purchase', array('id' => 'purchase', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['PurchasePacks']['purchase'])); ?>
                  </div>
                   
				 
				  <div class="col-md-2 col-xs-6">
                    <label>Gross Amount</label>
                  	
					<?php echo $this->Form->input('gross_amount', array('id' => 'gross_amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['PurchasePacks']['gross_amount'])); ?>
                  </div>
                  
                  
				   <div class="col-md-2 col-xs-6">
                    <label></label>                 
                	
					<?php echo $this->Form->input('Submit', array('id' => '', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => true)); ?>
                  </div>
				<div class="col-md-2 col-xs-6">
				<label>&nbsp;</label>
								<div class="input text">
								<a href="<?php echo Router::url('/');?>admin/grocery/purchase_packs" class="btn btn-danger"> Cancel</a>
							</div>
							</div>
							
							
    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
		
	</div>	<script>
    $(document).ready(function(){
		
	$('#expiry').Zebra_DatePicker({format:'m-d-Y'});
	
	$("#expiry").attr("readonly", false); 

	var Units_Per_Case = 1;
	var Case_Cost = 0;
	
	/*$('#Units_Per_Case').keyup(function(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	$('#unitpercost').val(unicost);
	});
	$('#Case_Cost').keyup(function(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	if(Units_Per_Case=='' || Units_Per_Case==0) {
	Units_Per_Case = 1;
	}
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	$('#unitpercost').val(unicost);
	});
	
	
	$('#Case_Cost,#Case_Retail').keyup(function(){
	
		var Case_Cost  = $('#Case_Cost').val();				
		var Case_Retail  = $('#Case_Retail').val();	
		var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
				$('#Margin').val(margin);
				
	});
	
	$('#Case_Cost,#Units_Per_Case').keyup(function(){

			var Units_Per_Case = $('#Units_Per_Case').val();
			var Case_Cost = $('#Case_Cost').val();
			var unicost = Case_Cost/Units_Per_Case;
			
			if(Units_Per_Case !='' && Case_Cost != ''){
			findretailss();
			}else{
	$('#Case_Retail').val('');
	}
	});*/
	
	/*function findretailss(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	var retail = (unicost)*(Units_Per_Case);
	$('#Case_Retail').val(retail);	
    }*/
	
			 
	
		$('#pul').keyup(function(){
  		var pul = $('#pul').val();
  $.ajax({
    url: '<?php echo Router::url('/') ?>admin/grocery/getdesc',
	type: 'post',
	data: {pul:pul},
	success:function(data){  
     //alert(data);  
     var res = $.parseJSON(data);      
     if(res){      
		var item_id = res.result['id'];
		$('#description').select2().select2('val', item_id)
		$('#gid').val(res.result['id']);
		/*$('#item').val(res.result['Item']);
		$('#inventory').val(res.result['inven']);
		$('#price').val(res.result['price']);	*/
		//alert(res.result['Item']);		
		 /* if(res.result['Item']=='' || res.result['Item']==null){	
			if (item_id!="") {			 
				var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
				$("#editbt").html(a);
				} else {
					$("#editbt").html('');
				}
			}	*/
     }	  
	 else{
      $('#description').select2().select2('val','');
      $('#gid').val('');
     /* $('#item').val('');
      $('#inventory').val('');
      $('#price').val('');    */ 
         }

     }
   }); 

  });	
			 

			 
/*		$('#description').change(function(){
		var id = $('#description').val();
		//alert(id);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/purchasegetplu',
                type: 'post',
                data: {id:id},
                success:function(data){	
				 var res = $.parseJSON(data);	
				// alert(res.result['plu_no']);			 	
				 if(res){
					    var item_id=res.result['id'];
				 		$('#pul').val(res.result['plu_no']);
						$('#gid').val(res.result['id']);
						$('#item').val(res.result['Item']);
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);						
						
						if(res.result['Item']=='' || res.result['Item']==null){	
						if (item_id!="") {			 
						var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
						$("#editbt").html(a);
						} else {
						$("#editbt").html('');
						}
						}	
										
					
				 }else{
				        $('#pul').val('');
						$('#gid').val('');
						$('#item').val('');
						$('#inventory').val('');
						$('#price').val('');
				 
				 }			
                }
            }); 
			var price = $('#price').val();
			var Units_Per_Case = $('#Units_Per_Case').val();
			if(Units_Per_Case==""){
			Units_Per_Case= 0;
			}
				//alert(Units_Per_Case);
				var retail = ((price)*(Units_Per_Case)).toFixed(2);
				$('#Case_Retail').val(retail);
						var Case_Cost  = $('#Case_Cost').val();				
						var Case_Retail  = $('#Case_Retail').val();	
						var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
						$('#Margin').val(margin);

         });  	
		 
		 
		 $('#description').change(function(){
			    var id = $('#description').val();			
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/getitemnamecost',
				type: 'post',
				data: {id:id},
				success:function(data){ 
				var res = $.parseJSON(data);      
				if(res){     
				$('#Case_Cost').val(res.result['Case_Cost']);
				$('#Case_Retail').val(res.result['Case_Retail']);	
				}else{
				$('#Case_Cost').val('');
				$('#Case_Retail').val(''); 
				}
				}
				}); 
				
				});	
		 
		
		$('#item').keyup(function(){	
		        $("#editbt").html('');
				var item = $('#item').val();
		        //alert(item);
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/getplu',
						type: 'post',
						data: {item:item},
						success:function(data){
						//alert(data);
						 var res = $.parseJSON(data);
						if(res){      
						var item_id = res.result['id'];
						$('#pul').val(res.result['plu_no']);
						$('#description').select2().select2('val', item_id);
						$('#gid').val(res.result['id']);						
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);					
						}else{
						$('#pul').val('');
						$('#description').select2().select2('val','')
						$('#gid').val('');						
						$('#inventory').val('');
						$('#price').val('');
						}
						
						
						}
					}); 
	
					
					 });
					 
					 
				$('#item').keyup(function(){
			    var item = $('#item').val();
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/getitemcost',
				type: 'post',
				data: {item:item},
				success:function(data){      
				var res = $.parseJSON(data);      
				if(res){     
				$('#Case_Cost').val(res.result['Case_Cost']);
				$('#Case_Retail').val(res.result['Case_Retail']);	
				}else{
				$('#Case_Cost').val('');
				$('#Case_Retail').val(''); 
				}
				}
				}); 
				
				});	 */				 
					 
					 
					 
	});
	</script>