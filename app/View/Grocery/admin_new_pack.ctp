<div class="portlet box blue">
               <div class="page-content portlet-body" >
	 <?php echo $this->Html->script('admin/select2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'admin/select2',
	         'admin/select2-metronic',
        ));
        ?> 
	<div class="portlet box blue">
							<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Import Excelsheet
							</div>
						
						</div>
						
						<div class="portlet-body ">
				  
				<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file','url' => '/admin/grocery/importpacksfile'), array('inputDefaults' => array('label' => false, 'enctype'=>'multipart/form-data' ,'div' => false, 'required' => false))); ?>
				
				 
				
			 <div class="portlet-body col-md-1">
			 <label><i class="fa fa-file"></i> File</label> 
</div>			 
			 <div class="portlet-body col-md-3">
			 
			 <?php echo $this->Form->input('files', array('class' => 'textfile' ,'label' => false, 'required' => true, 'type' => 'file')); ?>
			  </div>
				 <div class="portlet-body col-md-4">
				 <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Import</button>
</div>				
			<?php echo $this->form->end(); ?>
			<p><a href="http://aplus1.net<?php echo Router::url('/'); ?>files/groceryitemsamplefile/please _follow_coloumn_order.xlsx" download><i class="fa fa-download"></i> Sample File</a></p>		
		</div>
	</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>New Purchase Pack
							</div>
						</div>
						<div class="portlet-body">
	<div class="rubyUprodts index">
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				  <!--<div class="col-md-2 col-xs-6">
                    <label>Vendor</label>
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'empty'=>'', 'label' => false, 'required' => false)); ?>
                  </div>
				    <div class="col-md-2 col-xs-6">
                    <label>Invoice</label>
					 <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'select','empty'=>'', 'label' => false, 'readonly' => false,'required'=>true)); ?>
                   
                  </div>-->
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code</label>
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'pul', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name</label>
                	
					<?php echo $this->Form->input('Item_Name', array('id' => 'description', 'class' => 'form-control select2me','select' => 'text', 'label' => false, 'required' => true,  'options' =>$item,  'empty' => 'Select Item')); ?>
                   
                  </div>
			
				   <div class="col-md-2 col-xs-6">
                    <label>Item#</label>
                	
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				  
				  
  				   <div class="col-md-2 col-xs-6">
                    <label>Case Cost</label>
                	
					<?php echo $this->Form->input('Case_Cost', array('id' => 'Case_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				  	   <div class="col-md-2 col-xs-6">
                    <label>Units Per Case</label>
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'Units_Per_Case', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
			<!--	   <div class="col-md-2 col-xs-6">
                    <label>Net Cost</label>
					<?php echo $this->Form->input('Net_Cost', array('id' => 'Net_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Discount</label>
                	
					<?php echo $this->Form->input('Discount', array('id' => 'Discount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  	 <div class="col-md-2 col-xs-6">
                    <label>Rebate</label>
                	
					<?php echo $this->Form->input('Rebate', array('id' => 'Rebate', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>-->
				    <div class="col-md-2 col-xs-6">
                    <label>Case Retail</label>
                	
					<?php echo $this->Form->input('Case_Retail', array('id' => 'Case_Retail', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div> 
			<!--	     <div class="col-md-2 col-xs-6">
                    <label>Margin</label>
                	
					<?php echo $this->Form->input('Margin', array('id' => 'Margin', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'readonly'=>true)); ?>
                  </div>
				      <div class="col-md-2 col-xs-6">
                    <label>Purchase Quantity</label>
                	
					<?php echo $this->Form->input('purchase', array('id' => 'purchase', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  <div class="col-md-2 col-xs-6">
                    <label>Purchase Date</label>
					<?php echo $this->Form->input('purchase_date', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Unit per cost </label>
					 <?php echo $this->Form->input('Unit_per_cost', array('id' => 'unitpercost', 'class' => 'form-control','type' => 'text', 'label' => false, 'readonly' => true)); ?>
                   
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Unit per Price</label>
					 <?php echo $this->Form->input('pricevalue', array('id' => 'price', 'class' => 'form-control','type' => 'text', 'label' => false, 'readonly' => true)); ?>
                   
                  </div>-->

					 <?php echo $this->Form->input('gid', array('id' => 'gid', 'class' => 'form-control','type' => 'hidden', 'label' => false)); ?>
                  
                  
				 <!--   <div class="col-md-2 col-xs-6">
                    <label>Remaining Inventory</label>
					 <?php echo $this->Form->input('inventory', array('id' => 'inventory', 'class' => 'form-control','type' => 'text', 'label' => false, 'readonly' => true)); ?>
                   
                  </div>
				  <div class="col-md-2 col-xs-6">
                    <label>Gross Amount</label>
                  	
					<?php echo $this->Form->input('gross_amount', array('id' => 'gross_amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>-->
				   <div class="col-md-2 col-xs-6">
                    <label></label>
					 
                	
					<?php echo $this->Form->input('Submit', array('id' => '', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => true)); ?>
                  </div>
				<div class="col-md-2 col-xs-6">
				<label>&nbsp;</label>
								<div class="input text">
								<a href="<?php echo Router::url('/');?>admin/grocery/purchase_packs" class="btn btn-danger"> Cancel</a>
							</div>
							</div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
		
	</div>	
	<script>
    $(document).ready(function(){
	$("form :input").attr("autocomplete", "off");
	$('#expiry').Zebra_DatePicker({format:'m-d-Y'});
	$("#expiry").attr("readonly", false);
	 
		$('#pul').keyup(function(){
			var pul = $('#pul').val();
			$.ajax({
			url: '<?php echo Router::url('/') ?>admin/grocery/getdesc',
			type: 'post',
			data: {pul:pul},
			success:function(data){  
			
			 var res = $.parseJSON(data);      
			 if(res){      
				var item_id = res.result['id'];
				$('#description').select2().select2('val', item_id)
				$('#gid').val(res.result['id']);
			 }	  
			 else{
				  $('#description').select2().select2('val','');
				  $('#gid').val('');
				 }
			 }
			}); 
     });	
	
	
	/*var Units_Per_Case = 1;
	var Case_Cost = 0;*/
	
	/*$('#Units_Per_Case').keyup(function(){
	var price = $('#price').val();
	var Units_Per_Case = $('#Units_Per_Case').val();
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	var retail = (price)*(Units_Per_Case);
	
	$('#unitpercost').val(unicost);
	$('#Case_Retail').val(retail);
	
	
			var Case_Cost  = $('#Case_Cost').val();				
		var Case_Retail  = $('#Case_Retail').val();	
var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
				$('#Margin').val(margin);
	
	});
	$('#Case_Cost').keyup(function(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	if(Units_Per_Case=='' || Units_Per_Case==0) {
	Units_Per_Case = 1;
	}
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	$('#unitpercost').val(unicost);
	});*/
	
	/*$('#Case_Cost,#Case_Retail').keyup(function(){
		var Case_Cost  = $('#Case_Cost').val();				
		var Case_Retail  = $('#Case_Retail').val();	
var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
				$('#Margin').val(margin);
				
	});
	$('#pul').keyup(function(){
		
		var pul = $('#pul').val();
//		alert(pul);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/getdesc',
                type: 'post',
                data: {pul:pul},
                success:function(data){
				 var res = $.parseJSON(data);
				 if(res){
				 		$('#description').val(res.result['desc']);
						$('#gid').val(res.result['id']);
						$('#item').val(res.result['Item']);
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);
					
				 }else{
				 $('#description').val('');
						$('#gid').val('');
						$('#item').val('');
						$('#inventory').val('');
						$('#price').val('');
				 
				 }
				
                }
            }); 
			var price = $('#price').val();
			var Units_Per_Case = $('#Units_Per_Case').val();
			if(Units_Per_Case==""){
			Units_Per_Case= 0;
			}
				//alert(Units_Per_Case);
				var retail = ((price)*(Units_Per_Case)).toFixed(2);
				$('#Case_Retail').val(retail);
						var Case_Cost  = $('#Case_Cost').val();				
						var Case_Retail  = $('#Case_Retail').val();	
						var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
						$('#Margin').val(margin);
						
						 
             });*/

	/*$('#item').keyup(function(){
		var plu = $('#pul').val();
		if((plu == '') || (plu == null)){
				var item = $('#item').val();
		//		alert(item);
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/getplu',
						type: 'post',
						data: {item:item},
						success:function(data){
						//alert(data);
						 var res = $.parseJSON(data);
						$('#pul').val(res.result['plu']);
						$('#description').val(res.result['desc']);
						
						
					
						}
					}); 
	
					}
					 });
					 
	$('#vendor').change(function(){
					 var vendor = $('#vendor').val(); 
					 $('#invoice').html('');
										$.ajax({
											url: '<?php echo Router::url('/') ?>admin/grocery/getinvoice',
											type: 'post',
											data: {vendor:vendor},
											success:function(data){
											var res = $.parseJSON(data);
											//alert(res.length);
											$('#invoice').append('<option value=""></option>');
											  for (var key in res) {
       if (res.hasOwnProperty(key)) {
			$('#invoice').append('<option value="'+res[key]+'">'+res[key]+'</option>');
       }
    }
										//	$('#invoice').val(data);
											}
										}); 
					 });*/
	 });
		
	</script>
