<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Search Your Store's Purchase Packs
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <!--<div class="col-md-2 col-xs-6">
                    <label>Vendor</label>
                  
                	
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'empty'=>'', 'label' => false, 'required' => false)); ?>
                  </div>-->
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Item_Name', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
  			<!--	   <div class="col-md-1 col-xs-6">
                    <label>Min Cost</label>
                	
					<?php echo $this->Form->input('min_cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-1 col-xs-6">
                    <label>Max Cost</label>
                	
					<?php echo $this->Form->input('max_cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  <div class="col-md-1 col-xs-6">
                    <label>Margin</label>
                	
					<?php echo $this->Form->input('Margin', array('id' => 'Margin', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>-->
				  <div class="col-md-1 col-xs-6">
                    <label>Item#</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label></label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
                  </div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	
	<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Your Store's Purchase Packs
							</div>
							<div class="caption pull-right">
								<a href="<?php echo Router::url('/');?>admin/grocery/new_pack" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
							</div>
						
						</div>
						<div class="portlet-body">
		

					<div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                 <!--    <th><?php echo __($this->Paginator->sort('Vendor Id'));?>  </th>-->
                                     <th><?php echo __($this->Paginator->sort('Item scan code'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Item name'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Units per case'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Case cost'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Net cost'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Case retail'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Unit per cost'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Item#'));?>  </th>
                                                 <th><?php echo $this->Paginator->sort('Margin'); ?></th>
                                                 <th style="width:130px "><?php echo $this->Paginator->sort('Actions'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							//   pr($list);
						
							$ms = "'Are you sure want delete it?'";
                                             foreach ($list as $data) { 
											
	//echo "Select description from r_grocery_items where id='".$data["PurchasePacks"]["Item_Name"]."'";die;
											 
											  $results2= ClassRegistry::init('PurchasePacks')->query("Select description from r_grocery_items where id='".$data["PurchasePacks"]["Item_Name"]."'");   
											// pr($results2); 
											 if (isset($results2) && !empty($results2)) { 
											 $item=$results2[0]['r_grocery_items']['description'];
											 }else{
												 $item=$data["PurchasePacks"]["Item_Name"];
											 }
                                               echo '<tr>
                                                     
                                                      <td>'.$data["PurchasePacks"]["Item_Scan_Code"].'</td>
                                                      <td>'. $item.'</td>
                                                      <td>'.$data["PurchasePacks"]["Units_Per_Case"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Case_Cost"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Net_Cost"].'</td>
                                                      <td>'.round($data["PurchasePacks"]["Case_Retail"],2).'</td>
                                                      <td>'.$data["PurchasePacks"]["Unit_per_cost"].'</td>
													   <td>'.$data["PurchasePacks"]["Item#"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Margin"].'</td>
                                                      <td>	<a href="'.Router::url('/').'admin/grocery/edit_purchasepack/'.$data["PurchasePacks"]["id"].'" class="newicon red-stripe"><img src="'.Router::url('/').'img/edit.png"/></a>
													<a href="'.Router::url('/').'admin/grocery/purchase_packs/'.$data["PurchasePacks"]["id"].'"  onClick="return confirm('.$ms.');" class="newicon red-stripe" ><img src="'.Router::url('/').'img/delete.png"/></a></td>
												</tr>';
												
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="11">    
												<h3>No data exists for this search criteria.</h3>
        <h4>Try searching again with different criteria values.</h4> </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        
        </div>
</div>			
</div>			
	</div>
