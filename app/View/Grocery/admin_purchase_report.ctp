<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Search Your Store's Purchase Pack Report
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <div class="col-md-4 col-xs-6">
                    <label>Vendor<?php echo $vendor_id;?></label>                
					<?php 					
					echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'empty'=>'Select Vendor', 'label' => false, 'required' => false,'selected'=>$vendor_id)); ?>
                  </div>
				     <div class="col-md-4 col-xs-6">
                    <label>Invoice</label>
					 <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'select','options'=>$invoice_array,'empty'=>'Select Invoice', 'label' => false, 'readonly' => false,'selected'=>$invoice_id)); 				 
					 
				 ?>
                   
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label></label>                   
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
                  </div>
                  
                  <div class="col-md-2 col-xs-6">
                      <label></label>                    
                     <a href="<?php echo Router::url('/');?>admin/grocery/purchasereportreset" class="form-control btn btn-warning ">Reset</a>                  
                  </div>

    <?php echo $this->form->end(); ?>
	
				</div><?php if(isset($totalinvoice)){?>
				<div class="row"><br/>
				</div>
				<div class="row">
				  <div class="col-md-4 col-xs-6">
					<h4>Invoice Gross Total = 	<?php if($totalinvoice[0][0]['totalinvoice']==0) echo '0'; else echo round($totalinvoice[0][0]['totalinvoice'],2); ?></h4>
				  </div>
				   <div class="col-md-4 col-xs-6">
					<h4>	Purchase Pack Gross Total = <?php if($grosstotal[0][0]['totalgross']==0) echo '0'; else  echo round($grosstotal[0][0]['totalgross'],2); ?></h4>
				  </div>
				   <div class="col-md-4 col-xs-6">
	<h4>	Difference = <?php $ab = round($totalinvoice[0][0]['totalinvoice'],2);
	$bc = round($grosstotal[0][0]['totalgross'],2);
	echo( $ab- $bc); ?></h4>
				  </div>
				</div>
				  <?php } ?>
	</div>
	</div>
	
	<div class="portlet box blue"> 
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Your Store's Purchase Pack Report
								
								
							</div>
							
						</div>
						<div class="portlet-body">
		

					<div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                     <th><?php echo __($this->Paginator->sort('Purchase Date invoice'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Pack Date'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Items'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('description'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Purchase Qty'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Net Amount'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Gross Amount'));?>  </th>
                                  
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							   
						
							$ms = "'Are you sure want delete it?'";
							$Item_Name = '';
                            foreach ($list as $data) { 
											 
							$Item_Id =$data["PurchasePacks"]["Item_Name"];
												
							$RGroceryItemslist = ClassRegistry::init('RGroceryItem')->find('all',array('conditions'=>array('RGroceryItem.id' => $Item_Id)));
												if(isset($RGroceryItemslist[0])){
												$Item_Name = $RGroceryItemslist[0]['RGroceryItem']['description'];
												}				 
							 
                                               echo '<tr>
                                                      <td>';
													  if($data["PurchasePacks"]["purchase_date"]){
													  echo date('M-d-Y',strtotime($data["PurchasePacks"]["purchase_date"]));
													  }
													  echo '</td>
																																						<td>';
													  if($data["PurchasePacks"]["created_at"]){
													  echo date('M-d-Y',strtotime($data["PurchasePacks"]["created_at"]));
													  }
													  echo '</td>
														<td>'.$data["PurchasePacks"]["Item_Scan_Code"].'</td>
                                                      <td>'.$Item_Name.'</td>
                                                      <td>'.$data["PurchasePacks"]["purchase"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Net_Cost"].'</td>
                                                      <td>'.$data["PurchasePacks"]["gross_amount"].'</td>
													  
												</tr>';
												
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="7">    
												<h3>No data exists for this search criteria.</h3>
        <h4>Try searching again with different criteria values.</h4> </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        
        </div>
</div>			
</div>			
	</div>
	<script>
		$(document).ready(function(){
			<?php if($this->Session->read('vendor')=="") { ?>
			$('#PurchasePacksAdminPurchaseReportForm select').val('');
			<?php } ?>
			$('#vendor').change(function(){
			var vendor = $('#vendor').val(); 
			$('#invoice').html('');
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/getinvoice',
				type: 'post',
				data: {vendor:vendor},
					success:function(data){
					var res = $.parseJSON(data);
					//alert(res.length);
					$('#invoice').append('<option value="">Select Invoice</option>');
						for (var key in res) {
							if (res.hasOwnProperty(key)) {
								$('#invoice').append('<option value="'+res[key]+'">'+res[key]+'</option>');
							}
						}
					//	$('#invoice').val(data);
					}
				}); 
			});
		});
    
    </script>