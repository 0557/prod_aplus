<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
              <li class="<?php echo (isset($purchase)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Purchase", array('controller' => '', 'action' => 'r_grocery_invoices'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($inventory)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Inventory Adjustments", array( 'action' => 'inventory'), array('escape' => false)); ?>
                </li>
               
				<li class="<?php echo (isset($track_non_scanned_item)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Track Non scanned Item", array('action' => 'track_non_scanned_item'), array('escape' => false)); ?>
                </li>

				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>