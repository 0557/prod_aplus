					<?php
					//$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));
					?>				
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
						  
					<?php 		
                    echo $this->Form->create('grocery_sales', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false)));
                     ?>
            <div class="confim_form">
                               
                                <div class="row">
                                   
                                    <div class="col-md-3 col-xs-6">
                                        <label>Report date</label>                                      
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                    <label style="display:block;">&nbsp;</label>   
                                    <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Get Report</button>
                                    </div>
                                    
                                    <div class="col-md-1 col-xs-6">
                                    <label style="display:block;">&nbsp;</label> 
                                    <a href="<?php echo Router::url('/');?>admin/grocery_sales/reset" class="btn btn-warning ">Reset</a> 
                                    </div>
                             <?php echo $this->form->end(); 
							 if ($this->Session->check('PostSearch')) {	
							 ?>
                                     
                                    <form action=" <?php echo Router::url('/');?>admin/grocery_sales/export" class="form-horizontal" id="" method="post" accept-charset="utf-8">                      
                                    <div class="col-md-2 col-xs-6"> 
                                    <label style="display:block;">&nbsp;</label>
                                    <?php
                                    echo $this->Form->input('full_date', array('id' =>'full_date','type' => 'hidden','value' => $full_date)); 
                                    echo $this->Form->button('Report', array('type'=>'submit', 'class' => 'btn green','id' => 'exportid'));
                                    ?>                                   
                                    </div>
                                    </form>                                    
                             <?php } ?>
                 
				               </div>
                               
            </div>                   
          
						
							
		</div>
			
	</div>

                    <div class="row">&nbsp;</div>
                  <div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Grocery Sales
						</div>
							
                  <div class="caption pull-right">
															
							</div>
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo 'Date'; ?></th>
								<th><?php echo 'Cigarates'; ?></th>
                                <th><?php echo 'Grocery Non Taxable'; ?></th>
                                <th><?php echo 'Grocery Taxable'; ?></th>
								<th><?php echo 'Tobacco'; ?></th>
                                <th><?php echo 'Total'; ?></th>
                                <th><?php echo 'Sales Tax'; ?></th>
                             
						</tr>
						</thead>
						<tbody>
						<?php				
						if(isset($grocery_sales) && $grocery_sales!='' && count($grocery_sales)>0){
						//echo '<pre>';print_r($grocery_sales);die;
						$date_array=array();
						foreach($grocery_sales as $grocery_sales_row)
						{
						$date=$grocery_sales_row['GrocerySale']['ending_date_time'];	
						array_push($date_array,$date);
						}
						$date_array=array_unique($date_array);
						//echo '<pre>';print_r($date_array);die;
						$nwdate_array=array();
						foreach($date_array as $key=>$val)
						{
						array_push($nwdate_array,$val);	
						}
						//echo '<pre>';print_r($nwdate_array);die;
						
						$date_count=count($nwdate_array);
	                    //echo $date_count;die;	
						$cstmz_grocery_sales=array();
	
						for($i=0;$i<$date_count;$i++)
						{
						foreach($grocery_sales as $grocery_sales_row)	
						{	
						if($grocery_sales_row['GrocerySale']['department_number']=='1' && $grocery_sales_row['GrocerySale']['ending_date_time']==$nwdate_array[$i]){
						$cigarate_net_sales=$grocery_sales_row['GrocerySale']['net_sales'];
						}
						if($grocery_sales_row['GrocerySale']['department_number']=='2' && $grocery_sales_row['GrocerySale']['ending_date_time']==$nwdate_array[$i]){
						$tobaco_net_sales=$grocery_sales_row['GrocerySale']['net_sales'];
						}
						$sub_array=array('date'=>$nwdate_array[$i],'cigarate_net_sales'=>$cigarate_net_sales,'tobaco_net_sales'=>$tobaco_net_sales);
						}	
						array_push($cstmz_grocery_sales,$sub_array);	
						}
						
						//echo '<pre>';print_r($cstmz_grocery_sales);die;
						
						 $tot_cig_sl=0;
						 
						 $tot_grnt_sl=0;
						 $tot_grt_sl=0;
						 
						 
						 $tot_tob_sl=0;
						 
					
						 
						 $tot_total=0;
						 
						 $tot_sl_tx=0;						
					
						 foreach ($cstmz_grocery_sales as $data)
						 {
							 
					     $Ruby2Tax=  ClassRegistry::init('Ruby2Tax')->find('all', array('fields' => array('taxable_sales', 'non_taxable_sales','net_tax'),'conditions'=>array('Ruby2Tax.period_end_date'=>$data['date'],'Ruby2Tax.tax_sysid'=>'1','store_id'=>$this->Session->read('stores_id')))); 
						 
							 
						 //echo '<pre>';print_r($Ruby2Tax[0]['Ruby2Tax']);
							 
						 $Ruby2Summary=ClassRegistry::init('Ruby2Summary')->find('all', array('fields' => array('fuel_sales'),'conditions'=>array('Ruby2Summary.period_end_date'=>$data['date'],'store_id'=>$this->Session->read('stores_id')))); 	
						 
						 
						 
						 //echo '<pre>';print_r($Ruby2Summary[0]['Ruby2Summary']);die;  
						 
						 $cigarate_net_sales=0;
						 if(isset($data['cigarate_net_sales']) && $data['cigarate_net_sales']!=''){
						 $cigarate_net_sales=$data['cigarate_net_sales'];
						 }
						 
						 $tobaco_net_sales=0;
						 if(isset($data['tobaco_net_sales']) && $data['tobaco_net_sales']!=''){
						 $tobaco_net_sales=$data['tobaco_net_sales'];
						 }
						 
						 
						 $fuel_sales=0;
						 if(isset($Ruby2Summary[0]['Ruby2Summary']['fuel_sales']) && $Ruby2Summary[0]['Ruby2Summary']['fuel_sales']!=''){
						 $fuel_sales=$Ruby2Summary[0]['Ruby2Summary']['fuel_sales'];
						 }
						 
						 
						 $ruby2_non_taxable_sales=0;
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales']) && $Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales']!=''){
						 $ruby2_non_taxable_sales=$Ruby2Tax[0]['Ruby2Tax']['non_taxable_sales'];
						 
						 }
						 
						 $non_taxable_sales=$ruby2_non_taxable_sales-$fuel_sales; 
						 
						 
						 
						 $ruby2_taxable_sales=0; 
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['taxable_sales']) && $Ruby2Tax[0]['Ruby2Tax']['taxable_sales']!=''){
						 $ruby2_taxable_sales=$Ruby2Tax[0]['Ruby2Tax']['taxable_sales'];
						 }
						 
						 
						 $taxable_sales=($cigarate_net_sales+$tobaco_net_sales)-($ruby2_taxable_sales);
						 
						 
						 $fuel_sales=0;
						 if(isset($Ruby2Tax[0]['Ruby2Tax']['net_tax']) && $Ruby2Tax[0]['Ruby2Tax']['net_tax']!=''){
						 $fuel_sales=$Ruby2Tax[0]['Ruby2Tax']['net_tax'];
						 }
						
						 
						 $tot_cig_sl=$tot_cig_sl+$cigarate_net_sales;
						 
						 $tot_grnt_sl=$tot_grnt_sl+$non_taxable_sales;
						 $tot_grt_sl=$tot_grt_sl+$taxable_sales;						
						 
						 $tot_tob_sl=$tot_tob_sl+$tobaco_net_sales;
					
						 $tot_sl_tx=$tot_sl_tx+$fuel_sales;					
						 
		                 $total=$cigarate_net_sales+$non_taxable_sales+$taxable_sales+$tobaco_net_sales;
						 $tot_total=$tot_total+$total;						 
						 ?>
						<tr>
                        
							<td><?php echo $data['date'];?>&nbsp;</td>
							<td><?php echo '$ '.$cigarate_net_sales;?>&nbsp;</td>
                            
                            <td><?php echo '$ '.$non_taxable_sales;?>&nbsp;</td>
                            <td><?php echo '$ '.$taxable_sales;?>&nbsp;</td>
                            
							<td><?php echo '$ '.$tobaco_net_sales;?>&nbsp;</td>
                         
                            <td><?php echo '$ '.$total;?>&nbsp;</td>
                            
                            <td><?php echo '$ '.$fuel_sales;?>&nbsp;</td>
						  
                            
						</tr>
					    <?php
						 }
						
						?>                   
                    
                        <thead>
						<tr>
								<th><?php echo 'Total Sales'; ?></th>
								<th><?php echo  '$ '.$tot_cig_sl; ?></th>
                                
                                <th><?php echo  '$ '.$tot_grnt_sl; ?></th>
                                <th><?php echo  '$ '.$tot_grt_sl; ?></th>                                
                                
								<th><?php echo  '$ '.$tot_tob_sl; ?></th>
                                
                                
                                <th><?php echo  '$ '.$tot_total; ?></th>
                                
                                <th><?php echo  '$ '.$tot_sl_tx; ?></th>
                            
						</tr>
						</thead>
                    
                    <?php 
					}
					?>
                    
                    
                    
						</tbody>
						</table>
                     
                        
                     </div>
                           
            </div>
	</div>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({bFilter: false});
	});
</script>