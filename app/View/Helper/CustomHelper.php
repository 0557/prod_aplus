<?php

App::uses('Helper', 'View');
App::uses('Helper', 'Html');

class CustomHelper extends Helper {

    var $helpers = array('Html', 'Session');

    public function GetProductName($product_id) {
        $data = ClassRegistry::init("WholesaleProduct")->find('first', array('conditions' => array("WholesaleProduct.id" => $product_id), 'recursive' => -1, 'fields' => array('WholesaleProduct.id', 'WholesaleProduct.name')));
        return $data;
    }

    public function GroceryItemName($product_id) {

        $data = ClassRegistry::init("RGroceryItem")->find('first', array('conditions' => array("RGroceryItem.id" => $product_id), 'recursive' => -1, 'fields' => array('RGroceryItem.plu_no')));
        return $data['RGroceryItem']['plu_no'];
    }

    public function getTaxState($tax_zone) {
        $tax_states = ClassRegistry::init("WholesaleTax")->find('all', array("conditions" => array("WholesaleTax.tax_zone" => strtolower($tax_zone)), 'fields' => array('State.name', 'State.id')));
        $taxstates = array();
        foreach ($tax_states as $value) {
            $taxstates[$value['State']['id']] = $value['State']['name'];
        }
        return $taxstates;
    }

    public function getTaxDescription($state_id, $tax_zone) {
        $tax_states = ClassRegistry::init("WholesaleTax")->find('list', array("conditions" => array('WholesaleTax.state_id' => $state_id, 'WholesaleTax.tax_zone' => $tax_zone), 'fields' => array('WholesaleTax.description', 'WholesaleTax.description')));
        // $item = $this->WholesaleTax->find('list', array('conditions' => array('WholesaleTax.state_id' => $state_id, 'WholesaleTax.tax_zone' => $tax_zone), 'fields' => array('WholesaleTax.description', 'WholesaleTax.description')));
        return $tax_states;
    }
    public function getdepartment($id) {
        $department = ClassRegistry::init("RGroceryDepartment")->find('first', array("conditions" => array('RGroceryDepartment.id' => $id), 'fields' => array('RGroceryDepartment.name')));
        
        return $department;
    }
    public function getItem($id) {
        $item = ClassRegistry::init("RGroceryItem")->find('first', array("conditions" => array('RGroceryItem.id' => $id), 'fields' => array('RGroceryItem.plu_no')));
        
        return $item;
    }

}
