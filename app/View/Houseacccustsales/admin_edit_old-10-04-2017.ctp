<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php
//echo '<pre>';print_r($Houseacccustsale);die;
echo $this->Form->create('Houseacccustsale', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false)));
?>

<div class="portlet box blue">
    <div class="page-content portlet-body" >


        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Edit House Acc/Cust Sales
                        </div>
                    </div></div></div>

            <div class="form-body col-md-12">
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <div class="portlet yellow box">

                            <div class="portlet-title">

                            </div>
                            <div class="portlet-body extra_tt">


                                <div class="row static-info">

                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Report Date:</strong>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('report_date', array('id' => 'report_date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => true)); ?> 
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Account Name:</strong>	 
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('account_name', array('class' => 'form-control', 'required' => 'true', 'label' => false, 'id' => 'account_name', 'placeholder' => 'Enter Account Name')); ?>

                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Vehical Tag#(Optional):</strong>	 
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('vehical_tag', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'vehical_tag', 'placeholder' => 'Enter Vehical Tag')); ?>

                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row static-info">

                                    

                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Fuel Product Type:</strong>			 
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php
                                            $options = array('Regular' => 'Regular', 'Super' => 'Super', 'Plus' => 'Plus', 'Diesel' => 'Diesel', 'Kerosene' => 'Kerosene', 'Off Road' => 'Off Road',);

                                            echo $this->Form->input('fuel_product_type', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'fuel_product_type', 'type' => 'select', 'options' => $options, 'empty' => 'Select Fuel Type'));
                                            ?>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Gallons Purchased(Optional):</strong>	 
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('gallons_purchased', array('class' => 'form-control', 'id' => 'gallons_purchased', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Gallons Purchased')); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Amount:
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('amount', array('class' => 'form-control', 'id' => 'amount', 'type' => 'text', 'required' => 'true', 'label' => false, 'placeholder' => 'Enter Amount')); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row static-info">

                                    <div class="col-md-6 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Memo:</strong>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('memo', array('class' => 'form-control', 'id' => 'memo', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Memo')); ?>

                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="col-md-5 name">
                                            <strong>Image:</strong>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->input('image', array('class' => 'form-control', 'id' => 'image', 'type' => 'file', 'label' => false, 'required' => 'false')); ?>
                                            <?php echo $this->Form->input('pre_image', array('type' => 'hidden', 'value' => $Houseacccustsale['Houseacccustsale']['image'])); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>             
            </div>


        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit" class="btn blue">Submit</button>
            <button type="reset" class="btn default">Reset</button>
            <button type="button" onclick="javascript:history.back(1)"
                    class="btn default">Cancel</button>

        </div>

    </div>

    <?php echo $this->form->end(); ?>
</div>

</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
                $(function () {
                    $("#report_date").datepicker({dateFormat: 'yy-mm-dd'});
                });
</script>



