<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">        


            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        House Acc/Cust Sales Report
                        <span class="btn green fileinput-button" style="margin-right:10px;">
                            <a href="<?php echo Router::url('/') ?>admin/houseacccustsales/add"><i class="fa fa-plus"></i>  <span>Add New</span></a> 
                        </span> 
                    </h3>

                </div>
            </div>
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">
                                <h5><strong>Please enter date range for this report  </strong></h5>
                            </div>
                            <!-- heading title ends-->
                            <!-- form starts --><?php //echo Router::url('/')   ?>
                            <?php echo $this->Form->create('RubyDailyreporting', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->
                                <div class="row">
                                    <!-- input 1-->
                                    <div class="col-md-2 col-xs-6">
                                        <label>Report date</label>
                                        <!-- important for developer please place your clander code on this input
                                        <input type="text" name="report" id="Date2" value=""  class="form-control" >-->

                                        <?php //echo $this->Form->input('reporting_date', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => date('d-m-Y'))); ?> 
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>

                                    </div>
                                    <!-- input 1 ends-->
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                    </div>
                                    <div class="col-xs-2  col-md-2">
                                        <label>&nbsp;</label>
                                        <a style=" margin-top:24px;" href="<?php echo Router::url('/'); ?>admin/houseacccustsales/reset" class="btn btn-warning ">Reset</a> 
                                    </div>
                                </div>
                                <!-- row one ends -->
                                <!--row for or -->

                                <!-- row for or ends -->
                                <!-- row two start -->

                                <!-- row two ends -->
                            </div>
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable tabbable-custom tabbable-full-width">

                            <div class="tab-content">

                                <div id="tab_1_5" class="tab-pane1">

                                    <?php //echo '<pre>'; print_r($LotterySalesReport); die;		 ?>

                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Report Date</th>
                                                    <th>Account Name</th>
                                                    <th>Fuel Product Type</th>
                                                    <th>Amount</th>
                                                    <th>Memo</th>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($Salereports) && !empty($Salereports)) { ?>
                                                    <?php
                                                    //echo '<pre>'; print_r($Salereport);die;
                                                    $total_amount = 0;
                                                    foreach ($Salereports as $data) {
                                                        $report_date = $data['Houseacccustsale']['report_date'];
                                                        $account_name = $data['Houseacccustsale']['account_name'];
                                                        $fuel_product_type = $data['Houseacccustsale']['fuel_product_type'];
                                                        $memo = $data['Houseacccustsale']['memo'];
                                                        $amount = $data['Houseacccustsale']['amount'];
                                                        $total_amount = $total_amount + $amount;
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $report_date; ?></td>  
                                                            <td><?php echo $account_name; ?></td>  
                                                            <td><?php echo $fuel_product_type; ?></td>  
                                                            <td><?php echo '$ ' . $amount; ?></td>  
                                                            <td><?php echo $memo; ?></td> 
                                                            <td>
                                                                <?php
                                                                if ($data['Houseacccustsale']['image'] != '') {
                                                                    ?>      
                                                                    <a href="<?php echo $this->webroot . 'houseacccustsaledocs/' . $data['Houseacccustsale']['image']; ?>" class="btn-warning view_file" target="_blank"><i class="fa fa-search"></i></a>
                                                                    <?php
                                                                } else {
                                                                    ?>        
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo $this->Html->link("<i class='fa fa-edit'></i>", array('controller' => 'houseacccustsales', 'action' => 'edit/' . $data['Houseacccustsale']['id']), array('escape' => false, 'class' => 'btn default btn-xs red-stripe edit'));
                                                                ?>         
                                                                <?php echo $this->Html->link("<i class='fa fa-trash'></i>", array('controller' => 'houseacccustsales', 'action' => 'delete', $data['Houseacccustsale']['id']), array('escape' => false, 'class' => 'btn default btn-xs red-stripe delete', 'confirm' => 'Are you sure you want to delete?'));
                                                                ?>                                                                                                                 
                                                            </td> 
                                                        </tr>
                                                    <?php } ?>
                                                <thead>
                                                    <tr>
                                                        <th colspan=3>Total</th>
                                                        <th><?php echo '$ ' . $total_amount; ?></th>  
                                                        <th></th>    
                                                        <th></th> 
                                                        <th></th>                                       
                                                    </tr>
                                                </thead>  
                                                <?php
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="7">No result founds!</td>
                                                </tr>
    <?php
}
?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="margin-top-20">
                                        <ul class="pagination">
                                            <li>
<?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                            </li>
                                                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                            <li> 
                                            <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!--end tabbable-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <style>
            .current{
                background: rgb(238, 238, 238) none repeat scroll 0 0;
                border-color: rgb(221, 221, 221);
                color: rgb(51, 51, 51);
                border: 1px solid rgb(221, 221, 221);
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .view_file{
                width: 30px;
                border: medium none;
                padding: 10px;
                color: #FFF;		
                font-size: 13px;		
            }



        </style>

    </div>

<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
        <script>
            $(document).ready(function () {
                $('#config-demo').val('');
            });
        </script>
    <?php
}
?>