<div class="portlet box blue">
               <div class="page-content portlet-body" >
	 <?php echo $this->Html->script('admin/select2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'admin/select2',
	         'admin/select2-metronic',
        ));
        ?> 
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Item Mapping Add
							</div>
						</div>
						<div class="portlet-body">
	<div class="rubyUprodts index">
			 <div class="row">
			  	<?php echo $this->Form->create('ItemMapping', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				  <!--<div class="col-md-2 col-xs-6">
                    <label>Vendor</label>
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'empty'=>'', 'label' => false, 'required' => false)); ?>
                  </div>
				    <div class="col-md-2 col-xs-6">
                    <label>Invoice</label>
					 <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'select','empty'=>'', 'label' => false, 'readonly' => false,'required'=>true)); ?>
                   
                  </div>-->
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code</label>
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'pul', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name</label>
                	
					<?php echo $this->Form->input('Item_Name', array('id' => 'description', 'class' => 'form-control select2me','select' => 'text', 'label' => false, 'required' => true,  'options' =>$item,  'empty' => 'Select Item')); ?>
                   
                  </div>
			
				   <div class="col-md-2 col-xs-6">
                    <label>Item#</label>
                	
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				  
				  
  				   <div class="col-md-2 col-xs-6">
                    <label>Case Cost</label>
                	
					<?php echo $this->Form->input('Case_Cost', array('id' => 'Case_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				  	   <div class="col-md-2 col-xs-6">
                    <label>Units Per Case</label>
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'Units_Per_Case', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
	
				    <div class="col-md-2 col-xs-6">
                    <label>Case Retail</label>
                	
					<?php echo $this->Form->input('Case_Retail', array('id' => 'Case_Retail', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div> 
	

					 <?php echo $this->Form->input('gid', array('id' => 'gid', 'class' => 'form-control','type' => 'hidden', 'label' => false)); ?>
                  
               
				   <div class="col-md-2 col-xs-6">
                    <label></label>
					 
                	
					<?php echo $this->Form->input('Submit', array('id' => '', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => true)); ?>
                  </div>
				<div class="col-md-2 col-xs-6">
				<label>&nbsp;</label>
								<div class="input text">
								<a href="<?php echo Router::url('/');?>admin/ItemMappings/index" class="btn btn-danger"> Cancel</a>
							</div>
							</div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
		
	</div>	
	<script>
    $(document).ready(function(){
	$("form :input").attr("autocomplete", "off");
	$('#expiry').Zebra_DatePicker({format:'m-d-Y'});
	$("#expiry").attr("readonly", false);
	
	 $('#pul').keyup(function(){
  		var pul = $('#pul').val();
			$.ajax({
			url: '<?php echo Router::url('/') ?>admin/grocery/getdesc1',
			type: 'post',
			data: {pul:pul},
			success:function(data){  
			//  alert(data);  
			 var res = $.parseJSON(data);  
			   // alert(res);      
			 if(res){    
				if(res.result['trick']=='yes'){  
				var item_id = res.result['desc'];
					$('#item').val(res.result['Item']);
				$('#Case_Cost').val(res.result['price']);  
				
				}else{
					var item_id = res.result['id'];
				}
				//alert(item_id); 
				$('#description').select2().select2('val', item_id);
				$('#gid').val(res.result['id']);
				$('#Case_Retail').val(res.result['Case_Retail']);
				$('#Units_Per_Case').val(res.result['unitpercase']); 
			
			 }	  
			 else{
			  $('#description').select2().select2('val','');
			  $('#gid').val('');
			  $('#item').val('');
			  $('#inventory').val('');
			  $('#price').val('');  
			  $('#Case_Retail').val('');
			  $('#Units_Per_Case').val('');    
			  }
			
			 }
			}); 
    	});
	
	
		$('#description').change(function(){
		var id = $('#description').val();
		//alert(id);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/purchasegetplu1',
                type: 'post',
                data: {id:id},
                success:function(data){	
			//	alert(data);
				 var res = $.parseJSON(data);	
				// alert(res.result['plu_no']);			 	
				 if(res){
					if(res.result['trick']=='yes'){  
						$('#item').val(res.result['Item']);
						$('#Case_Cost').val(res.result['price']);  
						
					}else{
						
						$('#item').val('');
						$('#Case_Cost').val('');  
					}
				 		$('#pul').val(res.result['plu_no']);
						$('#gid').val(res.result['id']);
						/*$('#item').val(res.result['Item']);
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);		*/		
						$('#Case_Retail').val(res.result['Case_Retail']);		
						$('#Units_Per_Case').val(res.result['unitpercase']); 
						/*if(res.result['Item']=='' || res.result['Item']==null){	
						if (item_id!="") {			 
						var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
						$("#editbt").html(a);
						} else {
						$("#editbt").html('');
						}
						}	*/
										
					
				 }else{
				        $('#pul').val('');
						$('#gid').val('');
						$('#item').val('');
						$('#inventory').val('');
						$('#price').val('');
						 $('#Case_Retail').val('');
			  			$('#Units_Per_Case').val('');    
				 
				 }			
                }
            }); 
			

         });  	
		 
		  $('#item').keyup(function(){	
		        $("#editbt").html('');
				var item = $('#item').val();
		        //alert(item);
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/getplu1',
						type: 'post',
						data: {item:item},
						success:function(data){
						//alert(data);
						 var res = $.parseJSON(data);
						if(res){      
							var item_id = res.result['des'];
							$('#pul').val(res.result['plu']);
							$('#description').select2().select2('val', item_id);
							$('#Units_Per_Case').val(res.result['unirpercase']);						
							$('#Case_Cost').val(res.result['casecost']);
							$('#Case_Retail').val(res.result['caseretails']);					
						}
					}
				}); 
	
					
		});				
	});
		
	</script>
