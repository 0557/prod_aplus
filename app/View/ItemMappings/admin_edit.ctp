<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>Edit Purchase Pack
							</div>
						
						</div>
<div class="portlet-body">
 <?php echo $this->Html->script('admin/select2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'admin/select2',
	         'admin/select2-metronic',
        ));
		//pr($list); die;
        ?> 
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('ItemMapping', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                
                <?php echo $this->Form->input('id', array('id' => '', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['ItemMapping']['id'], 'required' => true)); ?>
                
                <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['ItemMapping']['invoice'])); ?>
                	
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['ItemMapping']['vendor'])); ?>
					
				
				  <?php /*?>
				  <div class="col-md-2 col-xs-6">
                    <label>Vendor  <span style="color:red">*</span></label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('id', array('id' => '', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>$list['ItemMapping']['id'], 'required' => true)); ?>
				
				<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'default'=>$list['ItemMapping']['vendor'], 'label' => false, 'required' => true)); ?>
                  </div>				  
				  <div class="col-md-2 col-xs-6">
                    <label>Invoice <span style="color:red">*</span></label>
					 <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'select','options'=>array($list['ItemMapping']['invoice']=>$list['ItemMapping']['invoice']), 'label' => false, 'readonly' => false,'required'=>true)); ?>
					 
                   
                  </div>
				  <?php */?>
                  
                  
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code <span style="color:red">*</span></label>                   
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'pul', 'class' => 'form-control','type' => 'text', 'label' => false, 'value'=>$list['ItemMapping']['Item_Scan_Code'],'required' => true)); ?>
                  </div>
				 
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name <span style="color:red">*</span></label>                  
				
                    <?php 					
					//pr($item);
					echo $this->Form->input('Item_Name', array(
					'id' => 'description', 'class' => 'form-control select2me','type' => 'select','label' => false,'required' => false,
                    'options' =>$item,
                    'empty' => 'Select Item',
					'value'=>$list['ItemMapping']['Item_Name']
                    ));
					?>                     
                    
                  </div>                
             
                  
				   <div class="col-md-2 col-xs-6">
                    <label>Item#</label>                 
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'value'=>$list['ItemMapping']['Item#'],'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Case Cost <span style="color:red">*</span></label>                  
					<?php echo $this->Form->input('Case_Cost', array('id' => 'Case_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['ItemMapping']['Case_Cost'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Units Per Case <span style="color:red">*</span></label>                  
                	
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'Units_Per_Case', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$list['ItemMapping']['Units_Per_Case'])); ?>
                  </div>                

  				  
				   <?php /*?>
				   <div class="col-md-2 col-xs-6">
                    <label>Net Cost</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Net_Cost', array('id' => 'Net_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['Net_Cost'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Discount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Discount', array('id' => 'Discount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['Discount'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Rebate</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Rebate', array('id' => 'Rebate', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['Rebate'])); ?>
                  </div>
                  <?php */?>
                  
				   <div class="col-md-2 col-xs-6">
                    <label>Case Retail</label>              
					<?php echo $this->Form->input('Case_Retail', array('id' => 'Case_Retail', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['Case_Retail'])); ?>
                   </div>
				
				   <?php /*?>				 				  
				   <div class="col-md-2 col-xs-6">
                    <label>Margin</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Margin', array('id' => 'Margin', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'readonly'=>true,'value'=>$list['ItemMapping']['Margin'])); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Purchase Date</label>
                    <!-- important for developer please place your clander code on this input-->
					<?php 
					if($list['ItemMapping']['purchase_date']){
					$newdate = explode('-',$list['ItemMapping']['purchase_date']);
					$pdate = $newdate[1].'-'.$newdate[2].'-'.$newdate[0];
					echo $this->Form->input('purchase_date', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$pdate));}else{
					echo $this->Form->input('purchase_date', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false));
					}
					 ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Unit per cost </label>
					 <?php echo $this->Form->input('Unit_per_cost', array('id' => 'unitpercost', 'class' => 'form-control','type' => 'text', 'label' => false, 'readonly' => true,'value'=>$list['ItemMapping']['Unit_per_cost'])); ?>
                   
                   </div>
				  <?php */?>
                  
                <?php /*?>   <div class="col-md-2 col-xs-6">
                    <label>Purchase Quantity</label>                  	
					<?php echo $this->Form->input('purchase', array('id' => 'purchase', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['purchase'])); ?>
                  </div>
                   
				 
				  <div class="col-md-2 col-xs-6">
                    <label>Gross Amount</label>
                  	
					<?php echo $this->Form->input('gross_amount', array('id' => 'gross_amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$list['ItemMapping']['gross_amount'])); ?>
                  </div><?php */?>
				   <div class="col-md-2 col-xs-6">
                    <label></label>                 
                	
					<?php echo $this->Form->input('Submit', array('id' => '', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => true)); ?>
                  </div>
				<div class="col-md-2 col-xs-6">
				<label>&nbsp;</label>
								<div class="input text">
								<a href="<?php echo Router::url('/');?>admin/ItemMappings/index" class="btn btn-danger"> Cancel</a>
							</div>
							</div>
							
							
    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
		
	</div>	<script>
    $(document).ready(function(){
		
	$('#expiry').Zebra_DatePicker({format:'m-d-Y'});
	
	$("#expiry").attr("readonly", false); 


	
	 $('#pul').keyup(function(){
  		var pul = $('#pul').val();
			$.ajax({
			url: '<?php echo Router::url('/') ?>admin/grocery/getdesc1',
			type: 'post',
			data: {pul:pul},
			success:function(data){  
			//  alert(data);  
			 var res = $.parseJSON(data);  
			   // alert(res);      
			 if(res){    
				if(res.result['trick']=='yes'){  
				var item_id = res.result['desc'];
					$('#item').val(res.result['Item']);
				$('#Case_Cost').val(res.result['price']);  
				
				}else{
					var item_id = res.result['id'];
				}
				//alert(item_id); 
				$('#description').select2().select2('val', item_id);
				$('#gid').val(res.result['id']);
				$('#Case_Retail').val(res.result['Case_Retail']);
				$('#Units_Per_Case').val(res.result['unitpercase']); 
			
			 }	  
			 else{
			  $('#description').select2().select2('val','');
			  $('#gid').val('');
			  $('#item').val('');
			  $('#inventory').val('');
			  $('#price').val('');  
			  $('#Case_Retail').val('');
			  $('#Units_Per_Case').val('');    
			  }
			
			 }
			}); 
    	});
	
	
		$('#description').change(function(){
		var id = $('#description').val();
		//alert(id);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/purchasegetplu1',
                type: 'post',
                data: {id:id},
                success:function(data){	
			//	alert(data);
				 var res = $.parseJSON(data);	
				// alert(res.result['plu_no']);			 	
				 if(res){
					if(res.result['trick']=='yes'){  
						$('#item').val(res.result['Item']);
						$('#Case_Cost').val(res.result['price']);  
						
					}else{
						
						$('#item').val('');
						$('#Case_Cost').val('');  
					}
				 		$('#pul').val(res.result['plu_no']);
						$('#gid').val(res.result['id']);
						/*$('#item').val(res.result['Item']);
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);		*/		
						$('#Case_Retail').val(res.result['Case_Retail']);		
						$('#Units_Per_Case').val(res.result['unitpercase']); 
						/*if(res.result['Item']=='' || res.result['Item']==null){	
						if (item_id!="") {			 
						var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
						$("#editbt").html(a);
						} else {
						$("#editbt").html('');
						}
						}	*/
										
					
				 }else{
				        $('#pul').val('');
						$('#gid').val('');
						$('#item').val('');
						$('#inventory').val('');
						$('#price').val('');
						 $('#Case_Retail').val('');
			  			$('#Units_Per_Case').val('');    
				 
				 }			
                }
            }); 
			

         });  	
					 
		  $('#item').keyup(function(){	
		        $("#editbt").html('');
				var item = $('#item').val();
		        //alert(item);
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/getplu1',
						type: 'post',
						data: {item:item},
						success:function(data){
						//alert(data);
						 var res = $.parseJSON(data);
						if(res){      
							var item_id = res.result['des'];
							$('#pul').val(res.result['plu']);
							$('#description').select2().select2('val', item_id);
							$('#Units_Per_Case').val(res.result['unirpercase']);						
							$('#Case_Cost').val(res.result['casecost']);
							$('#Case_Retail').val(res.result['caseretails']);					
						}
					}
				}); 
	
					
		});						 
	});
	</script>