<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Search Your Store's Purchase Packs
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('ItemMapping', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <!--<div class="col-md-2 col-xs-6">
                    <label>Vendor</label>
                  
                	
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'select','options'=>$vendor,'empty'=>'', 'label' => false, 'required' => false)); ?>
                  </div>-->
				   <div class="col-md-2 col-xs-6">
                    <label>Item Scan Code</label>
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'Item_Scan_Code', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$Item_Scan_Code)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Item Name</label>
                	
					<?php echo $this->Form->input('Item_Name', array('id' => 'Item_Name', 'class' => 'form-control','type' => 'select', 'label' => false, 'required' => false,'options' =>$itemname,'empty' => 'Select Item','default' => $Item_Name)); ?>
                  </div>
  	
					
				  <div class="col-md-1 col-xs-6">
                    <label>Item#</label>
                	
					<?php echo $this->Form->input('Item', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$Item)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>&nbsp;</label>
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
                  </div>

    <?php echo $this->form->end(); ?>
    <div class="col-md-1">
		  <label>&nbsp;</label>
		
         <a href="<?php echo Router::url('/');?>admin/ItemMappings/reset" class="btn btn-warning ">Reset</a>   

		<br/>
        </div>
				</div>
	</div>
	</div>
	
	<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Your Store's Purchase Packs
							</div>
							<div class="caption pull-right">
								<a href="<?php echo Router::url('/');?>admin/ItemMappings/add" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
							</div>
						
						</div>
						<div class="portlet-body">
		

					<div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                 <!--    <th><?php echo __($this->Paginator->sort('Vendor Id'));?>  </th>-->
                                     <th><?php echo __($this->Paginator->sort('Item scan code'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Item name'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Units per case'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Case cost'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Case retail'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Item#'));?>  </th>
                                     <th style="width:130px "><?php echo $this->Paginator->sort('Actions'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							  // pr($list); die;
						
							$ms = "'Are you sure want delete it?'";
                                             foreach ($list as $data) { 
											
	//echo "Select description from r_grocery_items where id='".$data["PurchasePacks"]["Item_Name"]."'";die;
											 
											  $results2= ClassRegistry::init('ItemMapping')->query("Select description from r_grocery_items where id='".$data["ItemMapping"]["Item_Name"]."'");   
											// pr($results2); 
											 if (isset($results2) && !empty($results2)) { 
											 $item=$results2[0]['r_grocery_items']['description'];
											 }else{
												 $item=$data["ItemMapping"]["Item_Name"];
											 }
                                               echo '<tr>
                                                     
                                                      <td>'.$data["ItemMapping"]["Item_Scan_Code"].'</td>
                                                      <td>'. $item.'</td>
                                                      <td>'.$data["ItemMapping"]["Units_Per_Case"].'</td>
                                                      <td>'.$data["ItemMapping"]["Case_Cost"].'</td>
                                                      <td>'.round($data["ItemMapping"]["Case_Retail"],2).'</td>
													   <td>'.$data["ItemMapping"]["Item#"].'</td>
                                                      <td>	<a href="'.Router::url('/').'admin/ItemMappings/edit/'.$data["ItemMapping"]["id"].'" class="newicon red-stripe"><img src="'.Router::url('/').'img/edit.png"/></a>
													<a href="'.Router::url('/').'admin/ItemMappings/delete/'.$data["ItemMapping"]["id"].'"  onClick="return confirm('.$ms.');" class="newicon red-stripe" ><img src="'.Router::url('/').'img/delete.png"/></a></td>
												</tr>';
											}
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="11">    
												<h3>No data exists for this search criteria.</h3>
       											 <h4>Try searching again with different criteria values.</h4> </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php 
									    echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li' ,'currentClass' => 'active', 'currentTag' => 'a' , 'escape' => false));

									
									//echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        
        </div>
</div>			
</div>			
	</div>
