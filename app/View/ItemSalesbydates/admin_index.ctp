<?php
$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name'), 'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$description =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'description'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$depart =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'r_grocery_department_id'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));
?>
				
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
						  
			<?php echo $this->Form->create('ruby_deptotals', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
	 <div class=" col-md-5">
	<div class=" col-md-4">Update Date </div>
    <div class=" col-md-8">  <?php include('test.ctp'); ?></div>
	</div>
			 <div class=" col-md-3">
			<?php echo $this->Form->input('department', array('id' => 'department', 'class' => 'form-control','type' => 'select', 'options'=>$rdepartments,'empty'=>'All Departments','label' => false,'required' => false)); ?>

	</div>
            <div class="col-md-2">
            <?php echo $this->Form->input('plu_no', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'plu_no','placeholder'=>'Enter Plu no','value'=>$plu_no)); ?>     
            </div>
	 <div class=" col-md-1">

				 <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Get Report</button>
    			
				
	</div>
               
						    <?php
							echo $this->form->end(); 
							$total_selling_price=0;
							foreach ($pluttls as $rubyDeptotalrow){
							$total_selling_price=$total_selling_price+$rubyDeptotalrow['RubyPluttl']['selling_price'];
							}
							?>
						    <p>&nbsp;</p>
							<!--<h4>Total Item Sold = <?php if($somevalue[0][0]['no_of_items_sold']) echo($somevalue[0][0]['no_of_items_sold']); else echo '0'; ?> &nbsp;&nbsp; Total Selling Price = <?php echo $total_selling_price; ?></h4>-->
                            
                           
		</div>
				
	</div>
        <?php
		if($this->Session->read('plu_no')!="") {
			$plu_no = $RGroceryItems[0]['RGroceryItem']['plu_no'];
			$description = $RGroceryItems[0]['RGroceryItem']['description'];
			$department_name = $RGroceryItems[0]['RGroceryItem']['department_name'];
			
			$unit_per_retail = $PurchasePacks[0]['PurchasePack']['item_price'];
			$unit_per_cost = $PurchasePacks[0]['PurchasePack']['Unit_per_cost'];
			$margin = $PurchasePacks[0]['PurchasePack']['Max_Margin'];
			?>
                    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="block-inner clearfix tooltip-examples">
                <table class="table table-striped table-bordered table-advance table-hover" id="">
                    <tbody>
                        <tr>
                            <td class="header-style">Scan Code</td>
                            <td class="txtrht"><?php echo $plu_no; ?></td>  
                            <td class="header-style">Current Price</td> 
                            <td class="txtrht"><?php echo $unit_per_retail; ?></td>   
                        </tr>
                        <tr>
                            <td class="header-style">Item Name</td>
                            <td class="txtrht"><?php echo $description; ?></td>  
                            <td class="header-style">Current Cost</td> 
                            <td class="txtrht"><?php echo $unit_per_cost; ?></td>   
                        </tr>
                        <tr>
                            <td class="header-style">Department</td>
                           <!-- <td class="txtrht"><?php echo $department_name; ?></td>  -->
                            <td class="txtrht"><?php echo $dep_name; ?></td>  
                            <td class="header-style">Margin</td> 
                            <td class="txtrht"><?php echo $margin; ?></td>   
                        </tr>
                    </tbody>
                
                </table>
            </div>
            </div>
        </div>
             <?php
		}
		?>
<div class="row"> </div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Item Sales ByDate
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->

						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<!--<th><?php echo $this->Paginator->sort('Department'); ?></th>-->
								<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
								<th><?php echo $this->Paginator->sort('Item Name'); ?></th>
								<th><?php echo $this->Paginator->sort('Selling Price'); ?></th>
								<th><?php echo $this->Paginator->sort('Current Cost'); ?></th>
								<th><?php echo $this->Paginator->sort('Items Sold'); ?></th>
								<th><?php echo $this->Paginator->sort('dollar Sold'); ?></th>
                                	<th><?php echo $this->Paginator->sort('dollar Cost'); ?></th>
                                <th><?php echo $this->Paginator->sort('Total Profit'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$total_selling_price='';
						$total_current_cost='';
						$total_no_of_items_sold='';
						$total_dlllar_sold='';
						$total_dlllar_cost='';
						$total_total_profit='';		
						
		foreach ($pluttls as $rubyDeptotal): 
		$storeId = $this->Session->read('stores_id');
		$PurchasePack = ClassRegistry::init("PurchasePack")->find('first', array('conditions' => array('PurchasePack.Item_Scan_Code' =>$rubyDeptotal['RubyPluttl']['plu_no'],'PurchasePack.store_id' =>$storeId), 'fields' => array('PurchasePack.id', 'PurchasePack.Unit_per_cost')));
		if($PurchasePack['PurchasePack']['Unit_per_cost']!=""){
		$current_cost=$PurchasePack['PurchasePack']['Unit_per_cost'];
		
		}
		else{
			$current_cost="0.00";
			
		}
		?>
						<tr>
                        <?php 
							$departId = @$depart[$rubyDeptotal['RubyPluttl']['plu_no']];
							 ?>
						<!--<td>&nbsp;</td>-->
							<td id="<?php echo $departId ; ?>" ><?php echo h($rubyDeptotal['RubyPluttl']['plu_no']); ?>&nbsp;</td>
								<td><?php echo @$description[$rubyDeptotal['RubyPluttl']['plu_no']]; ?>&nbsp;</td>
							<td><?php echo h($rubyDeptotal['RubyPluttl']['selling_price']); ?>&nbsp;</td>
                            
							<td><?php echo $current_cost; 
							$total_current_cost=$total_current_cost+$current_cost;
							
							?>&nbsp;</td>
                            
							<td><?php echo h($rubyDeptotal['RubyPluttl']['no_of_items_sold']);
							
							$total_no_of_items_sold=$total_no_of_items_sold+$rubyDeptotal['RubyPluttl']['no_of_items_sold'];
							 ?>&nbsp;</td>
                            
							<td><?php 
							
							$dlllar_sold=$rubyDeptotal['RubyPluttl']['selling_price']*$rubyDeptotal['RubyPluttl']['no_of_items_sold'];
						    echo h(number_format((float)($dlllar_sold), 2, '.', '')); 
							 $total_dlllar_sold=$total_dlllar_sold+$dlllar_sold;		
							 ?>&nbsp;</td>
                            <td><?php 
							
							$dlllar_cost= $current_cost* $rubyDeptotal['RubyPluttl']['no_of_items_sold'];
						    echo h(number_format((float)($dlllar_cost), 2, '.', '')); 
							 $total_dlllar_cost=$total_dlllar_cost+$dlllar_cost;		
							 ?>&nbsp;</td>
                            <td><?php 
							//echo 'Sold = '.$dlllar_sold.'||'; echo 'COST = '.$dlllar_cost.'||';
							$total_profit=$dlllar_sold - $dlllar_cost;
						   echo h(number_format((float)($total_profit), 2, '.', '')); 
						    $total_total_profit=$total_total_profit+$total_profit;
						   
						   ?>&nbsp;</td>
							
						</tr>
					<?php endforeach; ?>
						</tbody>
                        <tfoot>
                    <tr>
                        <td class="header-style"><b style="font-size:16px;">Total</b></td> 
                        <td class="header-style"><b>&nbsp;</b></td> 
                              <td class="header-style"><b>&nbsp;</b></td> 
                        <td class=""><strong style="font-size:16px;">$<?php echo h(number_format((float)($total_current_cost), 2, '.', ''));?></strong></td>  
                   
                        <td class=""><strong style="font-size:16px;">$<?php echo h(number_format((float)($total_no_of_items_sold), 2, '.', ''));?></strong></td> 
                        <td class=""><strong style="font-size:16px;">$<?php echo h(number_format((float)($total_dlllar_sold), 2, '.', ''));?></strong></td> 
                        <td class=""><strong style="font-size:16px;">$<?php echo h(number_format((float)($total_dlllar_cost), 2, '.', ''));?></strong></td> 
                        <td class=""><strong style="font-size:16px;">$<?php echo h(number_format((float)($total_total_profit), 2, '.', ''));?></strong></td> 
                  
                    </tr>
					 </tfoot>
						</table> 
                            </div>
                           
            </div>
						</div>
						
						


<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": true
	  });
		$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});
	});
</script>
<style>
#example th,#example td{
width:100px!important;}
</style>

<style>

.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }

button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>