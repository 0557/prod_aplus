<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" > 
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                   Item Sales By Date		
                </h3>
            
            </div>
        </div>
		<?php echo $this->Form->create('item_salesbydates', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
		<div class="row" style="margin-bottom:20px;">
         <div class="col-md-12">
            <div class="col-md-1">Update Date</div>
            <div class="col-md-3">        
            <?php include('test.ctp'); ?>
            </div>            
            
            <div class="col-md-1">Department</div>
            <div class="col-md-2">
           		<?php echo $this->Form->input('department_id', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'department_id','options'=>$RubyDepartment,'empty'=>'All','default'=>$department_id)); ?>               
            </div>           
            
            <div class=" col-md-1">PLU </div>
            <div class="col-md-2">
            <?php echo $this->Form->input('plu_no', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'plu_no','placeholder'=>'Enter Plu no','value'=>$plu_no)); ?>     
            </div>
            <div class=" col-md-1">
            	<button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <div class=" col-md-1">
           		 <a href="<?php echo Router::url('/');?>admin/item_salesbydates/reset" class="btn btn-warning ">Reset</a> 
            </div> 
            </div>
		</div>
        <?php echo $this->form->end(); ?>	       
        <?php
		if($this->Session->read('plu_no')!="") {
			$plu_no = $RGroceryItems[0]['RGroceryItem']['plu_no'];
			$description = $RGroceryItems[0]['RGroceryItem']['description'];
			$department_name = $RGroceryItems[0]['RGroceryItem']['department_name'];
			
			$unit_per_retail = $PurchasePacks[0]['PurchasePack']['item_price'];
			$unit_per_cost = $PurchasePacks[0]['PurchasePack']['Unit_per_cost'];
			$margin = $PurchasePacks[0]['PurchasePack']['Max_Margin'];
			?>
                    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="block-inner clearfix tooltip-examples">
                <table class="table table-striped table-bordered table-advance table-hover" id="">
                    <tbody>
                        <tr>
                            <td class="header-style">Scan Code</td>
                            <td class="txtrht"><?php echo $plu_no; ?></td>  
                            <td class="header-style">Current Price</td> 
                            <td class="txtrht"><?php echo $unit_per_retail; ?></td>   
                        </tr>
                        <tr>
                            <td class="header-style">Item Name</td>
                            <td class="txtrht"><?php echo $description; ?></td>  
                            <td class="header-style">Current Cost</td> 
                            <td class="txtrht"><?php echo $unit_per_cost; ?></td>   
                        </tr>
                        <tr>
                            <td class="header-style">Department</td>
                           <!-- <td class="txtrht"><?php echo $department_name; ?></td>  -->
                            <td class="txtrht"><?php echo $dep_name; ?></td>  
                            <td class="header-style">Margin</td> 
                            <td class="txtrht"><?php echo $margin; ?></td>   
                        </tr>
                    </tbody>
                
                </table>
            </div>
            </div>
        </div>
             <?php
		}
		?>
        <div class="row">
				<div class="col-md-12">
					<div class="block-inner clearfix tooltip-examples">
					
						<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
                        		<!--<th class="header-style">Date</th>-->
                                <th class="header-style">PLU Number</th>	
    							<th class="header-style">Item Name</th>	
                                <th class="header-style">Selling Price</th>	
                                <th class="header-style">Current Cost</th>	
						        <th class="txtrht header-style">Item Sold</th>
                                <th class="txtrht header-style">Dollar Sold</th>
                                <th class="txtrht header-style">Dollar Cost</th>
                                <th class="txtrht header-style">Total Profit</th>
                                
						</tr>
						</thead>
						<tbody>
						<?php						
						$total_no_of_items_sold='';
						$total_dlllar_sold='';
						$total_dlllar_cost='';
						$total_current_cost='';
						$total_total_profit='';
						
						 foreach ($ItemSalesbydates as $ItemSalesbydate): ?>
						<tr>
                       	  <!-- <td><?php echo h($ItemSalesbydate['RubyHeader']['ending_date_time']); ?>&nbsp;</td>-->
                           <td><?php echo h($ItemSalesbydate['ItemSalesbydate']['plu_no']); ?>&nbsp;</td>
                           <td><?php echo h($ItemSalesbydate['ItemSalesbydate']['item_description']); ?>&nbsp;</td>
                             <td class="txtrht">$<?php echo h($ItemSalesbydate['ItemSalesbydate']['selling_price']); ?>&nbsp;</td>
                             <td class="txtrht">$
						   <?php 
						   $current_cost=$ItemSalesbydate['ItemSalesbydate']['no_of_items_sold']*$unit_per_cost;//$ItemSalesbydate['ItemSalesbydate']['item_price'];
						   echo h(number_format((float)($current_cost), 2, '.', '')); 
						   $total_current_cost=$total_current_cost+$current_cost;	
						   ?>&nbsp;
                           </td>   
                             
                           <td class="txtrht"><?php echo h($ItemSalesbydate['ItemSalesbydate']['no_of_items_sold']); 
						 $total_no_of_items_sold=$total_no_of_items_sold+$ItemSalesbydate['ItemSalesbydate']['no_of_items_sold'];
						   
						   ?>&nbsp;</td>
                           <td class="txtrht">$
						   <?php
						   $dlllar_sold=$ItemSalesbydate['ItemSalesbydate']['no_of_items_sold']*$unit_per_retail;//$ItemSalesbydate['ItemSalesbydate']['selling_price'];
						    echo h(number_format((float)($dlllar_sold), 2, '.', '')); 
					        $total_dlllar_sold=$total_dlllar_sold+$dlllar_sold;							
							?>&nbsp;
                            </td> 
                          
                          
                           <td class="txtrht">$
						   <?php 
						  $dlllar_cost=$ItemSalesbydate['ItemSalesbydate']['no_of_items_sold']*$current_cost;
						   echo h(number_format((float)($dlllar_cost), 2, '.', ''));  
						   $total_dlllar_cost=$total_dlllar_cost+$dlllar_cost;		
						   ?>&nbsp;
                           </td> 
                           
                           <td class="txtrht">$
						   <?php 
						   $total_profit=$current_cost-$dlllar_sold;
						   echo h(number_format((float)($total_profit), 2, '.', ''));
						   $total_total_profit=$total_total_profit+$total_profit;
						   ?>&nbsp;
                           </td> 
          
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="header-style"><b>Total</b></td> 
                        <td class="header-style"><b>&nbsp;</b></td> 
                              <td class="header-style"><b>&nbsp;</b></td> 
                        <td class="txtrht">$<?php echo h(number_format((float)($total_current_cost), 2, '.', ''));?></td>  
                   
                        <td class="txtrht"><?php echo h(number_format((float)($total_no_of_items_sold), 2, '.', ''));?></td> 
                        <td class="txtrht">$<?php echo h(number_format((float)($total_dlllar_sold), 2, '.', ''));?></td> 
                        <td class="txtrht">$<?php echo h(number_format((float)($total_dlllar_cost), 2, '.', ''));?></td> 
                        <td class="txtrht">$<?php echo h(number_format((float)($total_total_profit), 2, '.', ''));?></td> 
                  
                    </tr>
					 </tfoot>	
						</table>
                       
						<p>
						<?php
						echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>
						<div class="paging">
						<?php
							echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
						?>
						</div>
						</div>
					</div>
				</div>
			</div>    
            
       
        
        
        
        
               
         
        
    
   
      
    </div>
</div>


</div>


<style>

.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }

button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "pageLength": 50
	  });
	//$('#end_date').Zebra_DatePicker({format:"Y-m-d"});	
	});
</script>



