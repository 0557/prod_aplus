<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $title_for_layout;  ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        
        <!--<?php   echo $this->Html->css(array(
                                //'admin/font-awesome.min',
                                //'admin/bootstrap.min',
                                //'admin/uniform.default',
                                //'jquery.gritter.css',
                             
                               // 'admin/style-metronic',
                               // 'admin/style',
                               // 'admin/style-responsive',
                                //'admin/plugins',
                                //'admin/default',
                                //'tasks',
                                
                                //'admin/custom'
                    )); ?>  -->
       <?php   echo $this->Html->css(array(
                                'admin/font-awesome.min',
                                'admin/bootstrap.min',
                                'admin/uniform.default',
                                'jquery.gritter.css',
                             
                                'admin/style-metronic',
                                'admin/style1',
                                'admin/plugins',
                                'admin/default',
                                'tasks',
                                
                                'admin/custom'
                    )); ?>  
        <!--<link href="assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>-->
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
   
    <body class="skin-black">
        
       
        <!-- BEGIN HEADER -->
        <div class="header navbar navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
           <?php echo $this->element('admin/header'); ?>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <?php echo $this->element('admin/side-nav'); ?>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                  <?php  echo $this->fetch('content'); ?>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="footer-inner">
                2014 &copy; Metronic by keenthemes.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script> 
        <![endif]-->
        
        <?php
 echo $this->Html->script(array(
                                'admin/jquery-1.10.2.min',
                                'admin/jquery-migrate-1.2.1.min',
                                'jquery-ui-1.10.3.custom.min',
                                'admin/bootstrap.min',
                                'admin/bootstrap-hover-dropdown.min',
                                'admin/jquery.slimscroll.min',
                               
                                'admin/jquery.blockui.min',
                                'admin/jquery.cokie.min',
                                'admin/jquery.uniform.min',
                                'jquery.gritter',
                                'admin/select2.min',
                                'admin/app',
                                'index',
                                'tasks',
                                'jquery.gritter'
                             ));
 
 ?>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="assets/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="assets/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>-->
<!--<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>-->
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<!--<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->

        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                Index.init();
                Index.initIntro();
                Tasks.initDashboardWidget();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>