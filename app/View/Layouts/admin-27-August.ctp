<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $title_for_layout; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
          <?php echo $this->Html->script('admin/jquery-1.10.2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
            'admin/uniform.default',
            'toastr.min',
            /*'admin/style-metronic',*/
            'admin/style',
            /*'admin/style-responsive',*/ 
	         'data_table',
            'zebra_datepicker',	
            'admin/plugins',
            /*'admin/default',*/
            'tasks',
            /*'admin/custom',*/
            /*'admin/template_style'*/
        ));
        ?> 
        <?php echo $this->Html->script('admin/jquery-1.10.2.min'); ?>

        <!--<link href="assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>-->
        <!-- END THEME STYLES -->
        <?php echo $this->Html->meta('icon', 'favicon.ico'); ?>
        <script type="text/javascript">
            var SITEURL = '<?php echo Router::url('/', true); ?>';

        </script>
        <!--<link rel="shortcut icon" href="favicon.ico"/>-->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
     <body class="skin-black">


        <!-- BEGIN HEADER -->
       
            <!-- BEGIN TOP NAVIGATION BAR -->
            <?php echo $this->element('admin/header'); ?>
            <!-- END TOP NAVIGATION BAR -->
      
        <!-- END HEADER -->
         <div class="wrapper row-offcanvas row-offcanvas-left"> 
            <!-- BEGIN SIDEBAR -->
          
			
                <?php		
					if ('company_admin' == $UsersDetails['Role']['alias']) {
						echo $this->element('admin/company_side-nav'); 
					} else if ('store_admin' == $UsersDetails['Role']['alias']) {
						echo $this->element('admin/store_side-nav'); 
					} else {
						echo $this->element('admin/side-nav'); 
					}
				?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <aside class="right-side">
            <section class="content">
            <div class="admin_menu">
            <div class="col-xs-12">
                <?php echo $this->fetch('content'); ?>
            </div>
            </div>
            </section>
            </aside>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="footer">
		<div style="display:none">
			<?php echo $this->element('sql_dump'); ?>
		</div>
		<!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script> 
        <![endif]-->

        <?php
        echo $this->Html->script(array(
            'admin/jquery-migrate-1.2.1.min',
//            'jquery-ui-1.10.3.custom.min',
            'admin/bootstrap.min',
            'admin/bootstrap-hover-dropdown.min',
            'admin/jquery.slimscroll.min',
            'toastr.min.js',
            'admin/jquery.blockui.min',
            'admin/jquery.cokie.min',
            'admin/jquery.uniform.min',
	         'admin/data_table',
            'admin/datatables.min',
            'admin/zebra_datepicker',	
            'admin/app',
            'tasks',
			'admin/jquery.validate.min',
			'admin/additional-methods.min'
        ));
        ?>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!--<script src="assets/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="assets/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="assets/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>-->
        <!--<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>-->
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <!--<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            $(document).ready(function () {
<?php
if ($this->Session->check('Message.flash') == 1) {

    $message = $this->Session->read('Message');
    if (isset($message['flash']['params']['class']) && !empty($message['flash']['params']['class'])) {
        $type_message = $message['flash']['params']['class'];
    }
    ?>
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-center",
                    }
    <?php if (isset($type_message) && !empty($type_message)) { ?>
                        toastr['<?php echo $type_message; ?>']('<?php echo strip_tags($this->Session->flash()); ?>', 'Message');
    <?php } else { ?>
                        toastr['success']('<?php echo strip_tags($this->Session->flash()); ?>', 'Message');
    <?php } ?>

<?php } ?>
            });
        </script>

        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                //  Index.init();
                //   Index.initIntro();
                Tasks.initDashboardWidget();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
