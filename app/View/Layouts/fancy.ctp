<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $title_for_layout;  ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        

       <?php   echo $this->Html->css(array(
                                'admin/font-awesome.min',
                                'admin/bootstrap.min',
                                'admin/uniform.default',
                                'jquery.gritter.css',
                             
                                'admin/style-metronic',
                                'admin/style1',
                                'admin/plugins',
                                'admin/default',
                                'tasks',
                                
                                'admin/custom'
                    )); ?>
                    
                      
        <!--<link href="assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>-->
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
        <style>
		#newplufancy .fancybox-skin{ background-color:#fff;} 
		#newplufancy .form-control{border:1px solid #000;}
		#newplufancy .name{ font-weight:bold; color: #000; padding-bottom:5px;}
        </style>
        
         <?php
			 echo $this->Html->script(array(
                                'admin/jquery-1.10.2.min',
                                'admin/jquery-migrate-1.2.1.min',
                                'jquery-ui-1.10.3.custom.min',
                                'admin/bootstrap.min',
                                'admin/bootstrap-hover-dropdown.min',
                            
                             ));
 
 ?>
        
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
   
    <body class="skin-black" id="newplufancy">

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <div class="page-content-wrapper">
                  <?php  echo $this->fetch('content'); ?>
            </div>
            <!-- END CONTENT -->
        </div>
    
        
       


        
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>