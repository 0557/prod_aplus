<!DOCTYPE HTML>

<html>
    <head>

        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php if (isset($description_for_layout)): ?>
        <meta name="description" content="<?php echo $description_for_layout; ?>" />
        <?php endif;
        if (isset($keywords_for_layout)):
            ?>
        <meta name='keywords' content="<?php echo $keywords_for_layout; ?>" />
        <?php endif; ?>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        <?php
        
        echo $this->Html->meta('icon', 'img_front/favicon.ico');
     //   echo $this->Html->js();
        echo $this->fetch('meta');
        
         
        echo $this->Html->css(array(
                                'custom',
                                'bootstrap',
                                'bootstrap-responsive',
                                'color',
                                'font-awesome.min',
                                'jquery.bxslider',
                                'jquery.mCustomScrollbar',
                                'form',
                                'SpryTabbedPanels'
                    )); ?>
        <link rel="icon" href="img/favicon.ico" type="image/x-icon">
      <?php  //echo $this->Html->css('front/style');
        //echo $this->Html->css('front/responsive');
       
        echo $this->Html->css('admin/jquery.jgrowl'); 

        echo $this->Html->script(array(
                                'html5',
                                'jquery',
                                'admin/jquery.jgrowl',
                                'SpryTabbedPanels',
                             ));
        ?>
        <?php
        $cssfiles = array();
        if (isset($cssIncludes) && $cssIncludes != '') {
            foreach ($cssIncludes as $css) {
                echo $this->Html->css($css);
            }
        }
        $jsfiles = array();
        if (isset($jsIncludes) && $jsIncludes != '') {
            foreach ($jsIncludes as $js) {
                echo $this->Html->script($js);
            }
        }
        ?>

        
        <?php
        
        //echo $this->fetch('css');
        
                echo $this->Html->css('bootstrap.min.css');
                echo $this->Html->css('custom.css');
                echo $this->Html->css('color.css');
                echo $this->Html->css('font-awesome.min.css');
                echo $this->Html->css('form.css');
                echo $this->Html->css('jquery.bxslider.css');
		echo $this->Html->css('bootstrap.css');
                echo $this->Html->css('bootstrap-responsive.css');
                
                
                echo $this->Html->script('html5.js');
                
        
       // echo $this->fetch('script');
        ?>



    </head>


    <body>
        
        <!--Wrapper Start-->

        <div id="wrapper"> 

            <!--header Start -->
                <?php echo $this->element('header'); ?>
            <!-- Header End -->

            <!--Banner Area Start-->
                <?php echo $this->element('banner'); ?>
            <!--Banner Area End--> 
            
            <!--Main Start-->
            <div id="main"> 
                <!--Account Banner Section Start-->
                <?php  echo $this->fetch('content'); ?>
                <!--Footer Start-->
                  <?php echo $this->element('footer'); ?>
                <!--Footer Start End-->

            </div>
                 <script>
            $(document).ready(function () {
<?php
if ($this->Session->check('Message.flash') == 1) {
    
    $message = $this->Session->read('Message');
   
    ?>
                jQuery.jGrowl('<?php echo strip_tags($this->Session->flash()); ?>', {header: 'Message'});
<?php } ?>
            });

        </script>
            <!--Main End--> 

        </div>

        <!--Wrapper End--> 

        <!--Jquery--> 
        <?php
              echo $this->Html->script(array(
                                'bootstrap',
                                'jquery.bxslider.min',
                                'form',
                                'jquery.mCustomScrollbar.concat.min',
                                'custom',
                             ));

        ?>
    
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
    </body>
</html>