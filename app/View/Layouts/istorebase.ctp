<?php
$cakeDescription = __d('cake_dev', 'IstoreOffice');
?>
<!DOCTYPE html>
<html>
<?php echo $this->element('admin/head'); ?>

<body class="skin-blue">
        
          <?php echo $this->Html->css('font-awesome.min');?> 
          
            <?php echo $this->Html->css('ionicons.min');?> 
         
        <!-- header logo: style can be found in header.less -->
        <?php echo $this->element('admin/header'); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">

			<?php echo $this->element('aside'); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				
				<div class="row">
				  <div class="col-md-12">
				  <div class="box-body">
					<h5 class="text-green">
						<?php echo $this->Session->flash(); ?>
					</h5>
				  </div>
				</div>
				</div>
				
				<?php echo $this->fetch('content'); ?>


            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->
        <script type="text/javascript">
	// <![CDATA[
		var basePath = "<?php echo $this->webroot;?>";
	// ]]>
	
		$(function() {
			$("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
			//Datemask2 mm/dd/yyyy
			$("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
			$("[data-mask]").inputmask();
		});
	</script>
	
	<?php 
	 echo $this->Html->script(
			array(
				"bootstrap.min",
				"plugins/input-mask/jquery.inputmask",
				"plugins/input-mask/jquery.inputmask.date.extensions",
				"plugins/input-mask/jquery.inputmask.extensions",
				"AdminLTE/app"
			)
		); 
	?>

     <?php echo $this->element('sql_dump'); ?> </div>
    </body>

</html>
