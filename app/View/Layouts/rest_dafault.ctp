<!DOCTYPE html>
<html lang="en" class="no-js">
     <head>
        <meta charset="utf-8"/>
        <title><?php echo $title_for_layout;  ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>     
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>      
       <?php   echo $this->Html->css(array(
                                'admin/font-awesome.min',
                                'admin/bootstrap.min',
                                'admin/uniform.default',
                                'jquery.gritter.css',
                                'admin/style-metronic',
                                'admin/style1',
                                'admin/plugins',
                                'admin/default',
                                'tasks',
                                'admin/custom'
                    )); 
		?>         
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>  
    <body class="skin-black">       
        <div class="header navbar navbar-fixed-top">            
           <?php echo $this->element('rest_header'); ?>        
        </div>     
        <div class="clearfix">
        </div>      
        <div class="page-container">            
          
            
            <div class="page-content-wrapper">
                  <?php  echo $this->fetch('content'); ?>
            </div>
         
        </div>      
        <div class="footer">
            <div class="footer-inner">
                2014 &copy; Metronic by keenthemes.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>       
        <?php
 echo $this->Html->script(array(
                                'admin/jquery-1.10.2.min',
                                'admin/jquery-migrate-1.2.1.min',
                                'jquery-ui-1.10.3.custom.min',
                                'admin/bootstrap.min',
                                'admin/bootstrap-hover-dropdown.min',
                                'admin/jquery.slimscroll.min',
                               
                                'admin/jquery.blockui.min',
                                'admin/jquery.cokie.min',
                                'admin/jquery.uniform.min',
                                'jquery.gritter',
                                'admin/select2.min',
                                'admin/app',
                                'index',
                                'tasks',
                                'jquery.gritter'
                             ));
 
 ?>

        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                Index.init();
                Index.initIntro();
                Tasks.initDashboardWidget();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>