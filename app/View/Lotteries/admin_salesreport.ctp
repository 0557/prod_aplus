<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">

            <div class="row">
                <div class="portlet-body col-md-12">
                    <?php
                    //echo 'ok';die;			
                    echo $this->Form->create('Lottery', array('action' => 'salesreport', 'role' => 'form', 'type' => 'file'));
                    ?>	
                    <!--<form action="salesreport" method="post">-->
                    <div class=" col-md-4">
                        <?php
                        //echo $this->Form->input('update_date', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => $update_date)); 
                        include('multidatepicker.ctp');
                        ?>    
                    </div>	   
                    <div class=" col-md-1">
                        <button name="getdata" class="btn btn-success" type="submit">Submit</button>
                    </div>
                    <div class=" col-md-2">
                        <a href="<?php echo Router::url('/'); ?>admin/lotteries/reset" class="btn btn-warning ">Reset</a> 
                    </div>		
                    <?php echo $this->form->end(); ?>   
                    <span>  
                        <input type="button" class="btn btn-warning pull-right" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">
                    </span>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Lottery Sales Report
                    </h3>
                </div>
            </div>       
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom tabbable-full-width">

                        <div class="tab-content">

                            <div id="tab_1_5" class="tab-pane1">

                                <?php //echo '<pre>'; print_r($LotterySalesReport); die;		 ?>

                                <div class="table-responsive">
                                    <table  id="example"  class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>Report Date</th>
                                                <th>Game Number</th>
                                                <th>Game Name</th>
                                                <th>Ticket Value</th>
                                                <th># of Tickets Sold Today</th>
                                                <th>$ of Ticket Sold Today</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($LotterySalesReport) && !empty($LotterySalesReport)) { ?>
                                                <?php
                                                //echo '<pre>'; print_r($LotterySalesReport);die;
                                                $total_sold_val = 0;
                                                $total_sold_amount = 0;
                                                $total_sold_ticket = 0;
                                                $total_sold_ticket_amount = 0;
                                                foreach ($LotterySalesReport as $data) {
                                                    $ticket_value = substr($data['GamePacks']['ticket_value'], 1);
                                                    $sold_val = $data['Lottery']['sold'];
                                                    $total_sold_val = $total_sold_val + $sold_val;
                                                    $sold_amount = $ticket_value * $sold_val;
                                                    $total_sold_amount = $total_sold_amount + $sold_amount;

                                                    $today_sold = $data['Lottery']['today_sold'];
                                                    $today_sold_value = ($data['Lottery']['today_sold_value']);

                                                    $total_sold_ticket = $total_sold_ticket + $today_sold;
                                                    $total_sold_ticket_amount = $total_sold_ticket_amount + $today_sold_value;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo h($data['Lottery']['updated']); ?></td>  
                                                        <td><?php echo h($data['GamePacks']['game_no']); ?></td>
                                                        <td><?php echo h($data['GamePacks']['gamename']); ?></td>
                                                        <td><?php echo h($data['GamePacks']['ticket_value']); ?></td>														
        <!--                                                        <td><?php echo h($sold_val); ?></td>
                                                        <td><?php echo h($sold_amount); ?></td>-->
                                                        <td><?php echo h($data['Lottery']['today_sold']); ?></td>
                                                        <td><?php echo h($data['Lottery']['today_sold_value']); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            <thead>
                                                <tr>
                                                    <th colspan=4>Total</th>
                                                    <th><?php echo $total_sold_ticket; ?></th>  
                                                    <th><?php echo '$ ' . $total_sold_ticket_amount; ?></th>                                            
                                                </tr>
                                            </thead>  
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="4">No result founds!</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="margin-top-20">
                                    <ul class="pagination">
                                        <li>
                                            <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                        </li>
                                        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                        <li> 
                                            <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end tab-pane-->
                        </div>
                    </div>
                </div>
                <!--end tabbable-->
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <style>
        .current{
            background: rgb(238, 238, 238) none repeat scroll 0 0;
            border-color: rgb(221, 221, 221);
            color: rgb(51, 51, 51);
            border: 1px solid rgb(221, 221, 221);
            float: left;
            line-height: 1.42857;
            margin-left: -1px;
            padding: 6px 12px;
            position: relative;
            text-decoration: none;
        }
    </style>

</div>
<script type="text/javascript">
            $('[data-toggle=modal]').on('click', function (e) {
    var $target = $($(this).data('target'));
            $target.data('triggered', true);
            setTimeout(function () {
            if ($target.data('triggered')) {
            $target.modal('show')
                    .data('triggered', false); // prevents multiple clicks from reopening
            }
            ;
            }, 1000); // milliseconds
            return false;
    });</script>
<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
    <script>
                $(document).ready(function () {
        $('#config-demo').val('');
        });</script>
    <?php
}
?>
<script>
                    var tableToExcel = (function () {
                            var uri = 'data:application/vnd.ms-excel;base64,'
                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                                                , base64 = function (s) {
                                                return window.btoa(unescape(encodeURIComponent(s)))
                                                }
                                        , format= function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (table, name) {
                        if (!table.nodeType)
                            table = document.getElementById(table)
                        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
                        window.location.href = uri + base64(format(template, ctx))
                    }
                })()
</script>