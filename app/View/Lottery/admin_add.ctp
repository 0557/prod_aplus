<?php

// pr($this->request->data); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers', 'admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker', 'admin/bootstrap-switch.min')); ?>
<?php echo $this->Form->create('SalesInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php $tax_zone = array('federal' => 'Federal', 'county' => 'County', 'state' => 'State', 'municipality' => 'Municipality') ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Fuel Sales INVOICE
                    </div>

                </div>
            </div>
        </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Order Details
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice TO Corporation:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('corporation_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $corporation, 'empty' => 'Select Corporation')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice TO Store:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'empty' => 'Select Store')); ?>
                                </div>
                            </div>
                            <span class="option_customer"> or</span>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice to Customer

                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('customer_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $wholesale_customer, 'empty' => 'Select Customer')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bill to:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('bill_to', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Bill to')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Billing Address:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('billing_address', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Billing Address')); ?>

                                </div>

                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice date:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('invoice_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly','value' => date("d/m/Y - h:i"))); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship Date:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('ship_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly','value' => date("d/m/Y - h:i"))); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Po:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Po')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('incoice_type', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Invoice')); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Customer Information
                            </div>
                            <div class="actions">

                            </div>
                        </div>
                        <div class="portlet-body">


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship to:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('ship_to', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Bill to')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Shipping Address:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('shiping_address', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Shipping Address')); ?>
                                    <input type="checkbox" id="same_as_address" class="same_as_address"><span>Shipping Address same as Billing Address</span>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Due date:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('due_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly','value' => date("d/m/Y - h:i"))); ?>

                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bol:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Bol')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terminal:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Terminal')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terms:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('terms', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter terms')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship Via:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('ship_via', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Ship via')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Status:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Send Email to Customer:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <input class="make-switch" name="checkbox" data-on="primary" data-off="info" type="checkbox"></div>
                                <!--                                <div class="col-md-7 value">
                                <?php // echo $this->Form->input('terms', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter terms')); ?>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Fuel Sales Products
                    </div>
                    <div class="actions">

                    </div>
                </div>

                <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Products</th>
                                    <th>Gallons Delivered</th>
                                    <th>Cost Per Gallons</th>
                                    <th>Net Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more">
                                <?php $total = 0; ?>

                                <?php if (isset($this->request->data['FuelProduct']['product_id']) && !empty($this->request->data['FuelProduct']['product_id'][0])) { ?>
                                    <?php
                                  // echo "<pre>"; print_r($this->request->data['FuelProduct']); die;
                                    foreach ($this->request->data['FuelProduct']['product_id'] as $key => $data) {
                                        if (!empty($data)) {
                                            $nameAndId = $this->Custom->GetProductName($data);
                                            ?>
				<tr>
				    <td><?php
                                                    echo $nameAndId['WholesaleProduct']['name'];
                                                    echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName','value'=> $data, 'id' => false, 'label' => false, 'type' => 'hidden', 'required' => false));
                                                    ?>&nbsp;</td>
				    <td class='max_open'><?php echo $this->Form->input('FuelProduct.gallons_delivered.', array('label' => false, 'value' => $this->request->data['FuelProduct']['gallons_delivered'][$key], 'class' => 'form-control', 'onkeyup' => 'quantity_amount()', 'type' => 'text', 'placeholder' => '0')); ?> </td>
				    <td><?php echo $this->Form->input('FuelProduct.cost_per_gallon.', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'id' => '', 'value' => $this->request->data['FuelProduct']['cost_per_gallon'][$key], 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
				    <td class="net_amount"><?php echo $this->Form->input('FuelProduct.net_ammount.', array('label' => false, 'class' => 'form-control', 'value' => $this->request->data['FuelProduct']['net_ammount'][$key], 'type' => 'text', 'readonly' => true)); ?></td>
				    <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
				</tr>
                                            <?php
                                        }
                                    }
                                } else {
                                    ?>
				<tr>
				    <td><?php echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Product', 'required' => false, "options" => $product)); ?>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
				</tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <button type="button" class="btn blue" onclick="AddProduct();" style="float:right;">Add More</button>
                        </br>
                        </br>
                        <!-- Add sales invoice table   -->
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Tax Zone</th>
                                    <th>USA State</th>
                                    <th>Tax Description</th>
                                    <th>Tax on Gallon Delivered(Qty)</th>
                                    <th>Rate on Gallon</th>
                                    <th>Net Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="gallon_add_more">
                                <?php $total = 0; ?>
                                <?php if (isset($this->request->data['TaxeZone']['tax_zone']) && !empty($this->request->data['TaxeZone']['tax_zone'])) { ?>
                                    <?php foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $data) { ?>
				<tr>
				    <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $data, 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?>&nbsp;</td>
                                            <?php
                                            $request_state =array();
                                            if (isset($data) && !empty($data)) {
                                                $request_state = $this->Custom->getTaxState($data);
                                            }
                                            ?>

				    <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $this->request->data['TaxeZone']['state_id'][$key], 'empty' => 'Select State', 'required' => false, 'options' => $request_state)); ?></td>
                                             <?php
                                             $request_description = array();
                                            if (isset( $this->request->data['TaxeZone']['tax_decription'][$key]) && !empty( $this->request->data['TaxeZone']['tax_decription'][$key])) {
                                                $request_description = $this->Custom->getTaxDescription($this->request->data['TaxeZone']['state_id'][$key],$data);
                                            }
                                            ?>
				    <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));', 'options'=>$request_description, 'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'selected' => $this->request->data['TaxeZone']['tax_decription'][$key], 'required' => 'false')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'class' => 'form-control SalesInvoiceTaxOnGallonQty', 'onkeyup' => 'percentage1($(this));', 'value' => $this->request->data['TaxeZone']['tax_on_gallon_qty'][$key], 'type' => 'text', 'placeholder' => '0')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide ', 'type' => 'text', 'label' => false, 'onkeyup' => 'percentage($(this));', 'value' => $this->request->data['TaxeZone']['rate_on_gallon'][$key], 'id' => 'rate_on_gallon', 'required' => 'false')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'class' => 'form-control value_after_tax', 'value' => $this->request->data['TaxeZone']['tax_net_amount_gallon'][$key], 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
				    <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
				</tr>
                                        <?php
                                    }
                                } else {
                                    ?>
				<tr>
				    <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?>&nbsp;</td>
				    <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select State', 'required' => false)); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));',  'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'onkeyup' => 'percentage1($(this));', 'class' => 'form-control SalesInvoiceTaxOnGallonQty first_class second_class', 'type' => 'text', 'placeholder' => '0')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide', 'type' => 'text', 'label' => false, 'id' => 'rate_on_gallon', 'onkeyup' => 'percentage($(this));', 'required' => 'false', 'value' => '0', 'placeholder' => '0')); ?></td>
				    <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'class' => 'form-control value_after_tax', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
				    <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
				</tr>
<?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" class="btn blue" onclick="gallonAddProduct();" style="float:right;">Add More</button>
                </div>
            </div>

        </div>

        <ul class="list-unstyled amounts total_last">
            <li class="last_content_first">
                <span class="span_1">Gallon Delivered Total</span>
                <span class="span_2" id="Gallon_Delivered_Total">
<?php echo $this->Form->input('max_qnty', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1">Net Amount Total</span>
                <span class="span_2" id="Net_Amount_Total">
<?php echo $this->Form->input('total_invoice', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_three">
                <span class="span_1">Taxes</span>
                <span class="span_2" id="Taxes">
<?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['SalesInvoice']['taxes'])) ? $this->request->data['SalesInvoice']['taxes'] : '0', 'onblur' => "getvalue()", 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0', 'onkeyup' => 'getvalue()')); ?>
                </span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Gross Amount</span>
                <span class="span_2" id="Taxes">
<?php echo $this->Form->input('gross_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
	    <li class="last_content_four">
                <span class="span_1">Discount</span><span class="span_2" id="discount"><?php echo $this->Form->input('discount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0','onkeyup'=>'total_amount()')); ?></span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Total Amount</span><span class="span_2" id="total_amount"><?php echo $this->Form->input('total_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
            </li>
            <li class="last_content_five">
                <div class="credit_span">
                    <span class="span_1">MOP : - </span>
<?php echo $this->Form->input('SalesInvoice.mop', array('class' => 'select_list_mop', 'onchange' => "mop_function(this.value)", 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'check'))); ?>
                </div>
                <span class="span_2" id="MOP">

                </span>
            </li>



        </ul>

    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249);"><?php echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'empty' => 'Select Product', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select',  "options" => $product)); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249);">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249);">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249);">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249);"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>
</table>
<table style="display:none">
    <tbody id="gallon_AddMore">
        <tr>
            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxZone($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Element', 'required' => false, "options" => $tax_zone)); ?>&nbsp;</td>
            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'onchange' => 'TaxDescription($(this));', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select State', 'required' => false)); ?></td>

            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control','onchange' => 'TaxRate($(this));',  'type' => 'select', 'empty' => 'Select Description', 'label' => false, 'id' => '', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'value' => 0, 'class' => 'form-control SalesInvoiceTaxOnGallonQty  abcd', 'onkeyup' => 'percentage1($(this));', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide', 'onkeyup' => 'percentage($(this));', 'type' => 'text', 'label' => false, 'id' => '', 'required' => 'false', 'value' => '0', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'value' => 0, 'class' => 'form-control value_after_tax', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>
</table>
<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->
<script type="text/javascript">

</script>
<script type="text/javascript">
    function total_amount() {
	var discount = parseFloat(document.getElementById("SalesInvoiceDiscount").value);
	var gross = (document.getElementById("SalesInvoiceGrossAmount").value);
	(document.getElementById("SalesInvoiceTotalAmount")).value = (gross - discount).toFixed(2);
    }
    ;
</script>
<script>


    function percentage_final() {
	var total_pr = 0;
	$('.value_after_tax').each(function () {
	    total_pr = total_pr + parseFloat($(this).val());
	});
	if (!isNaN(total_pr)) {
	    $('#SalesInvoiceTaxes').val(total_pr);
	}
	getvalue();
    }

    function percentage(currentval) {
	$('.SalesInvoiceTaxOnGallonQty').removeClass('first_class');
	$('.SalesInvoiceTaxOnGallonQty').removeClass('second_class');
	var value = currentval.val();

	var total = currentval.parents('tr').find('td:nth-child(4)').find('input').val();
	// var total = $('.SalesInvoiceTaxOnGallonQty').val();
	var farr = parseFloat(total) * value / 100;

	var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(farr.toFixed(2));
	percentage_final();

    }

    function percentage1(currentval) {
	$('.SalesInvoiceTaxOnGallonQty').removeClass('first_class');
	$('.SalesInvoiceTaxOnGallonQty').removeClass('second_class');
	var value = currentval.val();

	var total = currentval.parents('tr').find('td:nth-child(5)').find('input').val();
	// var total = $('.SalesInvoiceTaxOnGallonQty').val();
	var farr = parseFloat(value) * total;
	// alert(farr);
	var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(farr.toFixed(4));
	percentage_final();

    }

    function TaxZone(currentval) {
	var value = currentval.val();
	var target = currentval.parents('tr').find('td:nth-child(2)');
	$.ajax({
	    type: "POST",
	    dataType: "json",
	    url: SITEURL + 'admin/sales_invoices/taxzone_state/',
	    data: 'tax_zone=' + value,
	    /*  beforeSend: function () {
	     App.blockUI({
	     target: target,
	     iconOnly: true
	     });
	     },*/
	    success: function (data) {
		//  App.unblockUI(target);
		var html = '<option value="">Select State</option>';
		$.each(data, function (key, value) {
		    html += '<option value=' + value.id + '>' + value.name + '</option>';
		});
		$(target).find('select').html(html);
	    }
	});

    }

    function TaxDescription(currentval) {
	var target = currentval.parents('tr').find('td:nth-child(3)');
	var state_id = currentval.val();
	var tax_zone = currentval.parents('td').prev('td').find('select').val();
	if (tax_zone != '' && tax_zone != '') {
	    $.ajax({
		type: "POST",
		dataType: "json",
		url: SITEURL + 'admin/sales_invoices/tax_description/' + state_id + '/' + tax_zone,
		/*beforeSend: function () {
		 App.blockUI({
		 target: target,
		 iconOnly: true
		 });
		 },*/
		success: function (data) {
		    // App.unblockUI(target);
		    console.log(data);
		    var html = '<option value="">Select Description</option>';
		    $.each(data, function (key, value) {
			html += '<option value=' + value + '>' + value + '</option>';
		    });
		    $(target).find('select').html(html);
		}
	    });
	}
    }

    function TaxRate(currentval) {

	var target = currentval.parents('tr').find('td:nth-child(5)');
	var taxDescription = currentval.find('option:selected').text();
	var tax_zone = currentval.parents('td').prev('td').find('select').val();
	if (tax_zone != '' && tax_zone != '') {
	    $.ajax({
		type: "POST",
		dataType: "json",
		url: SITEURL + 'admin/sales_invoices/tax_rate',
		data: 'tax_desctiption=' + taxDescription,
		/*beforeSend: function () {
		 App.blockUI({
		 target: target,
		 iconOnly: true
		 });
		 },*/
		success: function (data) {
		    //App.unblockUI(target);
		    $(target).find('input').val(data);
		    TaxeZoneTaxNetAmountGallon
		    var taxqty = $('#TaxeZoneTaxOnGallonQty').val();
		    var rategallon = $('#rate_on_gallon').val();
		    parseFloat($('#TaxeZoneTaxNetAmountGallon').val(taxqty * rategallon));
		    parseFloat($('#SalesInvoiceTaxes').val(taxqty * rategallon));

		}
	    });
	}
    }
    function mop_function(value) {
	$('#MOP').text(value);
    }

    function gallonAddProduct() {
	$("#gallon_add_more").append($("#gallon_AddMore").html());
	var max_qntyc = parseFloat(jQuery('#SalesInvoiceMaxQnty').val());

	$('.abcd').attr('value', max_qntyc);
	$('.SalesInvoiceTaxOnGallonQty').removeClass('abcd');
	// $('.SalesInvoiceTaxOnGallonQty').attr('value', max_qntyc);
	getvalue();
    }

    function AddProduct() {
	$("#products_add_more").append($("#AddMore").html());
    }

    $(document).ready(function () {
	// initiate layout and plugins

	ComponentsPickers.init();
	getvalue();
	percentage_final();
	quantity_amount();

    });


    $('#same_as_address').change(function () {

	if ($(this).prop('checked')) {
	    $('#SalesInvoiceShipTo').val($('#SalesInvoiceBillTo').val());
	    $('#SalesInvoiceShipTo').prop('readonly', true);
	    $('#SalesInvoiceShipingAddress').val($('#SalesInvoiceBillingAddress').val());
	    $('#SalesInvoiceShipingAddress').prop('readonly', true);
	}
	else {
	    $('#SalesInvoiceShipTo').val('');
	    $('#SalesInvoiceShipTo').prop('readonly', false);
	    $('#SalesInvoiceShipingAddress').val('');
	    $('#SalesInvoiceShipingAddress').prop('readonly', false);
	}
    });

    $(document).delegate('.product_ajax', 'change', function () {
	var category = jQuery(this).val();

	$('.product_ajax').removeClass('active');
	$(this).addClass('active');
	var avg = jQuery.ajax({
	    url: '<?php echo Router::url('/'); ?>sales_invoices/products/' + category,
	    processData: false,
	    type: 'POST',
	    success: function (data) {
		$('.product_ajax.active').parents('tr').html(data);
		getvalue();
	    },
	});


    });

    function getvalue() {
	var Gross_Amount = 0;
	var max_qnty = 0;
	var max_total = 0;
	var tax = 0;
	discount = 0;


	$('td.net_amount').each(function () {
	    max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).find('input').val());
	});

	if (max_qnty == null || max_qnty == '' || isNaN(max_qnty)) {

	    max_qnty = 0;
	}

	$('td.max_open').each(function () {
	    max_total = parseFloat(max_total) + parseFloat(jQuery(this).find('input').val());
	    //   alert(max_total);
	    if (max_total == null || max_total == '' || isNaN(max_total)) {

		max_total = 0;
	    }
	    // max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());
	});
	tax = $('#SalesInvoiceTaxes').val();
	if (tax == null || tax == '') {
	    tax = 0;
	}
	Gross_Amount = parseFloat(max_qnty + parseFloat(tax));

	$('.first_class').val(max_total);

	$('#SalesInvoiceMaxQnty').val(max_total);

	$('#SalesInvoiceTotalInvoice').val(max_qnty);
	$('#SalesInvoiceGrossAmount').val(Gross_Amount.toFixed(4));

	discount = $('#SalesInvoiceDiscount').val();
	if (discount == null || discount == '') {
	    discount = 0;
	}
	gross = $('#SalesInvoiceGrossAmount').val();

	TotalAmount = parseFloat(gross - parseFloat(discount));

	$('#SalesInvoiceTotalAmount').val(TotalAmount.toFixed(4));


    }

    function quantity_amount() {
	var product_amount = 0;
	var total_amt_final = 0;
	var max_open = 0;

	$('td.max_open').each(function () {
	    max_open = parseFloat(jQuery(this).find('input').val());
	    if (max_open == null || max_open == '' || isNaN(max_open)) {
		max_open = 0;
	    }

	    //var max_open = parseFloat(jQuery(this).text());
	    var max_open_qty = parseFloat(jQuery(this).next('td').find('input').val());
	    var total_amt = parseFloat(max_open * max_open_qty);
	    if (total_amt == null || total_amt == '' || isNaN(total_amt)) {
		total_amt = 0;
	    }
	    jQuery(this).next('td').next('td').find('input').val(total_amt);
	    total_amt_final = total_amt_final + total_amt;


	    product_amount = parseFloat(max_open) + parseFloat(jQuery(this).text());
	    //alert(product_amount);
	});
	//   $('#FuelInvoiceMaxQnty').val(max_total);
	$('#SalesInvoiceTotalInvoice').val(total_amt_final);
	getvalue();

    }

    $(document).delegate('.Delete_product', 'click', function () {
	$(this).parents('tr').remove();
	getvalue();
    }
    );

    $('#SalesInvoiceStoreId , #SalesInvoiceCorporationId ').change(function () {
	var bill_to = $(this).find("option:selected").text();
	if (bill_to != 'Select Corporation' && bill_to != 'Select Store') {
	    $('#SalesInvoiceBillTo').val(bill_to);
	    $('#SalesInvoiceCustomerId').prop('disabled', 'disabled');
	} else {
	    $('#SalesInvoiceCustomerId').prop('disabled', false);
	}
    });

    $('#SalesInvoiceCustomerId').change(function () {

	var bill_to = $(this).find("option:selected").text();
	if (bill_to != 'Select Customer') {
	    $('#SalesInvoiceBillTo').val(bill_to);
	    $("#SalesInvoiceStoreId").prop('disabled', 'disabled');
	    $("#SalesInvoiceCorporationId").prop('disabled', 'disabled');
	} else {
	    $("#SalesInvoiceStoreId").prop('disabled', false);
	    $("#SalesInvoiceCorporationId").prop('disabled', false);
	}

    });

    $('#SalesInvoiceCorporationId').change(function () {
	var corporation_id = $(this).val();
	$('#SalesInvoiceCustomerId').val('');
	$.ajax({
	    type: "POST",
	    dataType: "json",
	    url: "<?php echo Router::url('/') ?>admin/sales_invoices/filter_store/" + corporation_id,
	    data: 'corporation_id=' + corporation_id,
	    success: function (data) {
		var html = '<option value="">Select Store</option>';
		$.each(data, function (key, value) {
		    html += '<option value=' + key + '>' + value + '</option>';
		});
		$('#SalesInvoiceStoreId').html(html);
	    }
	});

    });

</script>
<script>
//    $(function () {
//        $('#gallon_add_more').delegate('input.autocomplete_seacrh', 'focus', function (e) {
//            var state_id = $(this).parents('td').prev('td').find('select').val();
//            var tax_zone = $(this).parents('td').prev('td').prev('td').find('select').val();
//            if (state_id != '' && tax_zone != '') {
//                $(this).autocomplete({
//                    source: '<?php echo Router::url('/') ?>sales_invoices/autocomplete/' + state_id + '/' + tax_zone,
//                    change: function (event, ui) {
//                        $(this).parents('td').next('td').next('td').find('input').val(ui.item.desc);
//                    },
//                    select: function (event, ui) {
//                        $(this).parents('td').next('td').next('td').find('input').val(ui.item.desc);
//                    },
//                    response: function (event, ui) {
//                        // console.log(ui.content.length);
//                        if (!ui.content.length) {
//                            alert('Invalid Tax Description');
//                        }
//                    }
//                });
//            } else {
//                alert('Please Select Tax Zone and State');
//            }
//        });
//
//    });
</script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

	$('#SalesInvoiceDueDate').Zebra_DatePicker();
	$('#SalesInvoiceInvoiceDate').Zebra_DatePicker();
	$('#SalesInvoiceShipDate').Zebra_DatePicker();
    });
</script>
