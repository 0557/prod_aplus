<?php // pr($salesInvoice); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
			   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Confirm lottery delivery</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Enter lottery pack deliveriess</strong></h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
   
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
				<?php echo $this->Form->create('ImportedGames', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
                  <!-- input 1-->
                  <div class="col-md-2 col-xs-6">
                    <label>Date</label>
                    <!-- important for developer please place your clander code on this input-->
					  <?php echo $this->Form->input('packdate', array('id' => 'Date', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => 'false', 'value' => date('d-m-Y'))); ?>
                  </div>
                  <!-- input 1 ends-->
                  <!-- input 2-->
                  <div class="col-md-2 col-xs-6">
                    <label>Scan/Type ticket code</label>
 					 <?php echo $this->Form->input('scan_ticket_code', array('id' => 'scan_ticket_code', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => 'false', 'value' => '')); ?>
                  </div>
  			     <div class="col-xs-1 line" style="text-align:center"> 
				 <strong> Or</strong>
                </div>
				<div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<?php	
				echo $this->Form->input('game_no', array('type'=>'select','class' => 'form-control', 'label'=>false, 'options'=>$games, 'empty'=>'Select Game', 'value' => ''));?>
				  </div>
				  <div class="col-xs-6 col-md-2">
					<label>Pack number</label>
					 <?php echo $this->Form->input('pack_no', array('id' => 'pack_no', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => 'true', 'value' => '')); ?>
				  </div>
                  <!-- input 2 ends-->
                </div>
             
			       <div class="row"><br/>
				</div>
				</div>
              <div class="row">
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
               <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Confirm This Pack</button>
              </div>
              <?php /*?><div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
               <a href="<?php echo Router::url('/').'admin/lottery';?>" class="btn default finishbtn">Finish</a>

				
              </div><?php */?>
              </div>
              <!-- row two ends -->
              </div>
    <?php echo $this->form->end(); ?>
            <!-- form ends -->
          </div>
        </div>
		
      </div>
	  <h1></h1>
	  
	  <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
							
							 <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                      
                                     <th colspan="2"><?php echo __($this->Paginator->sort('Confirm Packs')); ?>  </th>
                                            <th colspan="2"><?php echo $this->Paginator->sort('Active Pcks'); ?></th>
                                     
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
									<tr>
									<td>Total</td>
									<td><?php echo sizeof($confirm_packs); ?></td>
									<td>Total</td>
									<td><?php echo sizeof($active_packs); ?></td>
									</tr>
									<tr>
									<td>Face value</td>
									<td><?php $ctotface = 0;
											foreach($confirm_packs as $face){
											$ctotface = $ctotface +$face['GamePacks']['face_value'];
											} echo  $ctotface ;?></td>
									<td>Face value</td>
									<td><?php 	$atotface = 0;
												foreach($active_packs as $face){
												$atotface = $atotface + 	$face['GamePacks']['face_value'];
												} echo $atotface ;?></td>
									</tr>
									<tr>
									<td>Net Value</td>
									<td><?php $ctotnet = 0;
			foreach($confirm_packs as $face){
			$ctotnet = $ctotnet +$face['GamePacks']['net_value'];
			} 
			echo $ctotnet ;?></td>
									<td>Net Value</td>
										<td><?php $atotnet = 0;
			foreach($active_packs as $face){
			$atotnet = $atotnet + 	$face['GamePacks']['net_value'];
			}
			echo $atotnet ;?></td>
									</tr>
									</tbody>
									</table>							
									
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>                                      
                                            <th><?php echo __($this->Paginator->sort('Game Number')); ?></th>
                                            <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Game'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Ticket per Pack'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Face Value'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Net Value'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Action'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                  <?php      
							      if (isset($confirm_packs) && !empty($confirm_packs)) { 
						                     $ms = "'Are you sure want delete it?'";
											 $total_facevalue=0;
											 $total_netvalue=0;
                                             foreach ($confirm_packs as $data) { 
											 $total_facevalue=$total_facevalue+$data["GamePacks"]["face_value"];
											 $total_netvalue=$total_netvalue+$data["GamePacks"]["net_value"];
                                               echo '<tr>                                                      
                                                    <td>'.$data["GamePacks"]["game_no"].'</td>
                                                    <td>'.$data["GamePacks"]["pack_no"].'&nbsp;</td>
                                                    <td>'.$data["GamePacks"]["gamename"].'&nbsp;</td>
                                                    <td>'.$data["GamePacks"]["ticket_value"].'&nbsp;</td>
                                                    <td>'.$data["GamePacks"]["ticket_per_pack"].'&nbsp;</td>
                                                    <td>'.$data["GamePacks"]["face_value"].'&nbsp;</td>
                                                    <td>'.$data["GamePacks"]["net_value"].'&nbsp;</td>
													<td> <a href="'.Router::url('/').'admin/lottery/delete_cpack/'.$data["GamePacks"]["id"].'" class="newicon red-stripe" onClick="return confirm('.$ms.');"><img src="'.Router::url('/').'img/delete.png"/></a></td>
                                                </tr>';
												
											}
											?>
									 <thead>
                                        <tr>                                      
                                            <th colspan="5" style="text-align:center;">Total</th>                                          
                                            <th><?php echo $total_facevalue; ?></th>
                                            <th><?php echo $total_netvalue; ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                      <?php
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
    </div>
   

    </div>
    </div>
<!--<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {	
	$('#Date').Zebra_DatePicker(	
	);
    });	
</script>-->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#Date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  </script>
  <script type="text/javascript" charset="utf-8">
   $(document).ready(function () {
		/*$('#ImportedGamesGameNo').change(function(){	
			var game_no = $('#ImportedGamesGameNo').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack_no').html(data);
                }
            });		
		});*/
		
			
		$('#scan_ticket_code').change(function(){
			
			var scan = $('#scan_ticket_code').val();
		
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getgamedata',
                type: 'post',
                data: { scan: scan},
                success:function(data){
					var json = jQuery.parseJSON(data)
					if(json['gno']!=null){
					$('#ImportedGamesGameNo').html('<option value="'+json['gno']+'">'+json['gno']+'</option>');
						
					}else{
						//location.reload();
					}
					if(json['pack']!=null){
					
					$('#pack_no').html('<option value="'+json['pack']+'">'+json['pack']+'</option>');
					}else{
					//location.reload();
					}					

                }
            });		
		});
		
		
    });
	

</script>