<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">        
            <div class="row">
                <div class="portlet-body col-md-12">
                    <?php               			
					 echo $this->Form->create( false, array('action' => 'daily_reading', 'role' => 'form', 'type' => 'file'));                     ?>
				       <div class=" col-md-4">
                        <?php                      
                        include('drdngmultidatepicker.ctp');
                        ?>    
                    </div>	   
                    <div class=" col-md-1">
                        <button name="getdata" class="btn btn-success" type="submit">Submit</button>
                    </div>
                    <div class=" col-md-2">
                        <a href="<?php echo Router::url('/'); ?>admin/lottery/dreset" class="btn btn-warning ">Reset</a> 
                    </div>		
                    <?php echo $this->form->end(); ?>   
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Lottery Daily Readings
                    </h3>

                </div>
            </div>

            <div class="content-new">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable tabbable-custom tabbable-full-width">

                            <div class="tab-content">

                                <div id="tab_1_5" class="tab-pane1">

                                    <?php //echo '<pre>'; print_r($LotterySalesReport); die;		 ?>

                                    <table id="example" class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Ticket Sold Count</th>
                                                <th>Ticket Sold Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($DailyReading) && !empty($DailyReading)) { 
											   $total_count=0;
											   $total_amount=0;                                
                                                foreach ($DailyReading as $data) {
												$total_count=$total_count+$data["Lottery"]["today_sold"];
												$total_amount=$total_amount+$data["Lottery"]["today_sold_value"];
                                                 echo '<tr>
											       <td>'.date('Y-m-d',strtotime($data["Lottery"]["updated"])).'</td>
                                                    <td>'.$data["Lottery"]["today_sold"].'</td>
                                                    <td>'.'$ '.$data["Lottery"]["today_sold_value"].'&nbsp;</td>
                                                 </tr>';
                                                }
                                            } else {
                                              echo ' <tr>
                                              <td colspan="3">No results found! </td>
                                              </tr>';
                                            }
                                            ?>
                                        </tbody>
                                        <?php
                                           if (isset($DailyReading) && !empty($DailyReading)) { 
										?>
                                         <thead>
                                            <tr>
                                                <th>Total</th>                                                
                                                <th><?php echo $total_count; ?></th>
                                                <th><?php echo '$ '.$total_amount; ?></th>
                                            </tr>
                                        </thead>
                                        <?php } ?>
                                    </table>
                                    <div class="margin-top-20">
                                        <ul class="pagination">
                                            <li>
                  <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                            </li>
                  <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                            <li> 
                  <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!--end tabbable-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <style>
            .current{
                background: rgb(238, 238, 238) none repeat scroll 0 0;
                border-color: rgb(221, 221, 221);
                color: rgb(51, 51, 51);
                border: 1px solid rgb(221, 221, 221);
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .view_file{
                width: 30px;
                border: medium none;
                padding: 10px;
                color: #FFF;		
                font-size: 13px;		
            }



        </style>

    </div>
</div>