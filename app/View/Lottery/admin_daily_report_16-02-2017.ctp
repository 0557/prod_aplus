<?php // pr($salesInvoice);    ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <div class="page-content">
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-title">Lottery ticket sales </h3>
                    </div>
                </div>
            </div>
            <!-- top header ends -->
            <!-- submit form -->
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">

                            </div>
                            <!-- heading title ends-->
                            <!-- form starts -->
                            <?php echo $this->Form->create('lottery_daily_readings', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Scan/Type ticket code</label>
                                        <?php echo $this->Form->input('scan_ticket_code', array('id' => 'scan_ticket_code', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false)); ?>
                                    </div>
                                    <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>Game number</label>
                                        <?php echo $this->Form->input('game_no', array('type' => 'select', 'class' => 'form-control', 'label' => false, 'options' => $games, 'empty' => '')); ?>
                                    </div>

                                    <div class="col-xs-6 col-md-2">
                                        <label>Pack number</label>
                                        <?php echo $this->Form->input('pack_no', array('type' => 'select', 'class' => 'form-control', 'id' => 'pack', 'label' => false, 'empty' => '')); ?>

                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>Bin number</label>
                                        <?php echo $this->Form->input('bin_no', array('type' => 'select', 'class' => 'form-control', 'required' => false, 'label' => false, 'options' => array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10), 'empty' => '')); ?>
                                    </div>

                                    <!-- input 2 ends-->
                                </div>
                                <!-- row one ends -->
                                <!--row for or -->

                                <!-- row for or ends -->
                                <!-- row two start -->
                                <div class="row"><br/>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Available Tk# on sale</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php echo $this->Form->input('prev', array('id' => 'availa', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'readonly' => 'readonly', 'required' => false)); ?>
                                    </div>

                                    <div class="col-md-2 col-xs-6">
                                        <label>Today Tkt#</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php //echo $this->Form->input('today', array('id' => 'totkt', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => true)); ?>

<!--                                        <input name="data[lottery_daily_readings][today]" id="totkt1" class="form-control" required="required" maxlength="11" type="text">-->
                                        
                                        <select class="form-control" name="data[lottery_daily_readings][today]" id="totkt">

                                        </select>
      <input  type="hidden" id="hidden_today" value="0" name="data[lottery_daily_readings][today_ticket_count]">                                  
      <input  type="hidden" id="start_ticket" value="0" name="data[lottery_daily_readings][start_ticket]">
      <input  type="hidden" id="end_ticket" value="0" name="data[lottery_daily_readings][end_ticket]">
                                    </div>
                                    <div class="col-md-2 col-xs-2" style="visibility: hidden;" id="revdiv">
                                        <input  type="hidden" id="hidden_today_order" value="0" name="data[lottery_daily_readings][today_order]">
                                        <label>Change Order</label><br>
                                        <a href="#" id="reverse" >Reverse</a><img id="revimg" style="height:30px" src="<?php echo $this->webroot?>img/up.png" />
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid" style="margin-top: 24px;">Enter Daily Sales</button>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <a href="<?php echo Router::url('/') . 'admin/lottery/finish_daily_report'; ?>" class="btn default  finishbtn" style="margin-top: 24px;">Finish Daily Sales</a>


                                    </div>
                                </div>
                                <!-- row two ends -->
                            </div>
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <h1></h1>
                <div id="tab_1_5" class="tab-pane1">

                    <div class="table-responsive">


                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>

                                    <th><?php echo __($this->Paginator->sort('Bin Number')); ?>  </th>
                                    <th><?php echo __($this->Paginator->sort('Game Number')); ?>  </th>
                                    <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Game'); ?></th>

                                    <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>


                                    <th><?php echo $this->Paginator->sort('Start Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('End Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Previous Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Current Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Available Tickets#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Status'); ?></th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($readings) && !empty($readings)) {
                                    $ms = "'Are you sure want delete it?'";


                                    foreach ($readings as $data) {
                                        echo '<tr>
                                                      
                                                    <td>' . $data["e"]["bin_no"] . '</td>
                                                    <td>' . $data["e"]["game_no"] . '</td>
                                                    <td>' . $data["e"]["pack_no"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["gamename"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["ticket_value"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["start_tkt"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["end_tkt"] . '&nbsp;</td>
                                                  
                                                    <td>' . $data["e"]["prev"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["today"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["available"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["status"] . '&nbsp;</td>
											    
                                                </tr>';
                                        //echo  '<td> <a href="'.Router::url('/').'admin/lottery/delete_apack/'.$data["GamePacks"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>'
                                    }
                                } else {
                                    echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


        </div>
    </div>


    <script type="text/javascript" charset="utf-8">

        $(document).ready(function () {

            $('#Date').Zebra_DatePicker();


            $('#lottery_daily_readingsGameNo').change(function () {

                var game_no = $('#lottery_daily_readingsGameNo').val();
                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/getpacksactive',
                    type: 'post',
                    data: {game_no: game_no},
                    success: function (data) {

                        $('#pack').html(data);
                        
                    }
                });
            });

            $('#scan_ticket_code').change(function () {

                var scan = $('#scan_ticket_code').val();

                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/getgamedata',
                    type: 'post',
                    data: {scan: scan},
                    success: function (data) {
                        var json = jQuery.parseJSON(data);
                        //alert(json['prev']);
                        if (json['gno'] != null) {
                            $('#lottery_daily_readingsGameNo').html('<option value="' + json['gno'] + '">' + json['gno'] + '</option>');

                        } else {
                            location.reload();
                        }
                        if (json['pack'] != null) {

                            $('#pack').html('<option value="' + json['pack'] + '">' + json['pack'] + '</option>');

                            $('#availa').val(json['prev']);
                        } else {
                            location.reload();

                        }

                    }
                });
            });

            $('#pack').change(function () {

                var gno = $('#lottery_daily_readingsGameNo').val();
                var pack = $('#pack').val();
                
                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/gettcikets',
                    type: 'post',
                    data: {game_no: gno, pack_no: pack},
                    success: function (data) {
//alert(data);
data = JSON.parse(data);

//alert(data.availa);
//alert(data.today);
//alert(data.today_order);
//$.each(data,function(k,v){
//   $.each(data,function(a,b){
//      alert(b.LotteryDailyReading.today);
//   });
//});
                        var start_ticket = parseInt(data.start_ticket);
                        var end_ticket = parseInt(data.end_ticket);
                        var avail = parseInt(data.availa);
                        var today = parseInt(data.today);
                        var today_order = parseInt(data.today_order);
                        var prev = parseInt(data.prev);
                        var start_ticket = parseInt(data.start_ticket);
                        var end_ticket = parseInt(data.end_ticket);
                        var ticket_per_pack = parseInt(data.ticket_per_pack);
                        //alert(avail+"--"+today+"--"+today_order+"--"+prev);
                        
                        $('#availa').val(data.availa);
                        if(isNaN(today_order))
                        {
                            $('#hidden_today_order').val(0);
                        }
                        else
                        {
                            $('#hidden_today_order').val(data.today_order);
                        }
                        
                        $('#start_ticket').val(start_ticket);
                        $('#end_ticket').val(end_ticket);
                        
                        
                        
                        var tokn = '';
                        
                        if(today == null || isNaN(today))
                        {
                            $('#start_ticket').val(start_ticket);
                            $('#end_ticket').val(end_ticket);
                           for (var i = start_ticket; i <= end_ticket; i++)
                            {
                                tokn = tokn + '<option >' + i + '</option>';
                            } 
                        }
                        else if(today_order == 0 )
                        {
                            //alert(avail+"--"+today+"--"+today_order);
                           tokn = '';
                           
                           for (var i = start_ticket; i <= end_ticket; i++)
                            {
                                tokn = tokn + '<option>' + i + '</option>';
                            } 
                        }
                        else if(today_order == 1 )
                        {//alert(today_order+"--"+tt+"--"+today);
                            tokn = '';
                            
                            for(i=start_ticket; i >= end_ticket; i--)
                            {
                                tokn = tokn + '<option>' + i + '</option>';
                            }
                        }
                        
                        //alert(tokn);
                        
                        
                        $('#totkt').html(tokn);
                        $('#revdiv').css('visibility', 'visible');
                        
                        var totkt_zero_val = $("#totkt").prop('selectedIndex');
                        var totkt_zero_text = $("#totkt option:selected").text();
                        totkt_zero_val = parseInt(totkt_zero_val);
                        totkt_zero_text = parseInt(totkt_zero_text);
                        
        //alert('text-'+totkt_zero_text);
                        if(totkt_zero_text == 0)
                        {
                            //alert('val-'+totkt_zero_val);
                            $('#hidden_today').val(0);
                        }
                        if(totkt_zero_text != 0)
                        {
                            //alert('text-'+totkt_zero_text);
                            $('#hidden_today').val(1);
                        }
                            
                        
                    }
                });


            });

            
            $('#totkt').change(function(){
                
                var hidden_today = $("#totkt").prop('selectedIndex');
                var start_ticket = $("#totkt option:selected").text();
                var end_ticket = $("#totkt option").last().text();
                var today_order = parseInt($("#hidden_today_order").val());
                //alert(hidden_today);
                hidden_today = parseInt(hidden_today) + 1;
                if(today_order == 0)
                {
                    start_ticket = parseInt(start_ticket) + 1;
                }
                else if(today_order == 1)
                {
                    start_ticket = parseInt(start_ticket) - 1;
                }
                $('#start_ticket').val(start_ticket);
                $('#end_ticket').val(end_ticket);
                
                $('#hidden_today').val(hidden_today);
            });

            $('#reverse').click(function () {
                
               var $select = $('#totkt');
               var options = $select.find('option');
                options = [].slice.call(options).reverse();
    //alert(options);
                //$select.empty();
                $.each(options, function (i, el) {
                    $select.append($(el));
                  });
                $('#totkt option').first().attr('selected','selected');
                
                var src_old = $('#revimg').attr('src');
                imgpa = src_old.split('/');
                imgname = imgpa[3];
                
                if(imgname == "up.png")
                {
                    src = '<?php echo $this->webroot?>'+'img/down.png';
                }
                else
                {
                    src = '<?php echo $this->webroot?>'+'img/up.png';
                }
                $('#revimg').attr('src',src);
                
                var ord = $('#hidden_today_order').val();
                if(ord == 0)
                {
                    $('#hidden_today_order').val(1);
                }
                else
                {
                    $('#hidden_today_order').val(0);
                }
            });
        });



    </script>