<?php // pr($salesInvoice);         ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <div class="page-content">
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-title">Lottery ticket sales </h3>
                    </div>
                </div>
            </div>
            <!-- top header ends -->
            <!-- submit form -->
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">

                            </div>
                            <!-- heading title ends-->
                            <!-- form starts -->
                            <?php echo $this->Form->create('lottery_daily_readings', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Scan/Type ticket code</label>
                                        <?php echo $this->Form->input('scan_ticket_code', array('id' => 'scan_ticket_code', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false)); ?>
                                    </div>
                                    <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <label>Select Date</label>
                                        <?php echo $this->Form->input('packdate', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => date('d-m-Y'))); ?>
                                        <input type="button" id="search_report" name="search_report" value="Search" />
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>Bin number</label>
                                        <?php
                                        $options = array();
                                        for ($i = 1; $i <= 40; $i++) {
                                            $options[$i] = $i;
                                        }
                                        ?>
                                        <?php echo $this->Form->input('bin_no', array('type' => 'select', 'class' => 'form-control', 'required' => false, 'label' => false, 'options' => $options, 'empty' => '')); ?>
                                    </div>
                                    
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <label>Game Name</label>
                                        <?php 
                                        echo $this->Form->input('game_no', array('type'=>'hidden','class' => 'form-control', 'label' => false));
                                        echo $this->Form->input('game_name', array('class' => 'form-control', 'label' => false));
                                        //echo $this->Form->input('game_no', array('type' => 'select', 'class' => 'form-control', 'label' => false, 'options' => $games, 'empty' => 'Select Game No.')); ?>
                                    </div>

                                    <div class="col-xs-6 col-md-2">
                                        <label>Pack number</label>
                                        <?php echo $this->Form->input('pack_no', array('type' => 'select', 'class' => 'form-control', 'id' => 'pack', 'label' => false, 'empty' => '')); ?>

                                    </div>
                                    

                                    <!-- input 2 ends-->
                                </div>
                                <!-- row one ends -->
                                <!--row for or -->

                                <!-- row for or ends -->
                                <!-- row two start -->
                                <div class="row"><br/>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Available Tk# on sale</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php echo $this->Form->input('on_hand_tickets', array('id' => 'availa', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'readonly' => 'readonly', 'required' => false)); ?>
                                    </div>

                                    <div class="col-md-2 col-xs-6">
                                        <label>Today Tkt#</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php //echo $this->Form->input('today', array('id' => 'totkt', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => true)); ?>

<!--                                        <input name="data[lottery_daily_readings][today]" id="totkt1" class="form-control" required="required" maxlength="11" type="text">-->

                                        <select class="form-control" name="data[lottery_daily_readings][today_reading]" id="totkt">

                                        </select>
                                        <!--                                        <br>
                                                                                prev_ticket-->
                                        <input  type="hidden" id="prev_ticket" value="0"  name="data[lottery_daily_readings][prev_ticket]">
                                        <!--                                        <br>starting_inventory-->
                                        <input  type="hidden" id="starting_inventory" value="0"  name="data[lottery_daily_readings][starting_inventory]">
                                        <!--                                        <br>ticket_order-->
                                        <input  type="hidden" id="ticket_order" value="0" name="data[lottery_daily_readings][ticket_order]">                                  
                                        <!--                                        <br>start_ticket-->
                                        <input  type="hidden" id="start_ticket"  name="data[lottery_daily_readings][start_ticket]">
                                        <!--                                        <br>end_ticket-->
                                        <input  type="hidden" id="end_ticket"  name="data[lottery_daily_readings][end_ticket]">
                                        <!--                                        <br>updated_start_ticket-->
                                        <input  type="hidden" id="updated_start_ticket"  name="data[lottery_daily_readings][updated_start_ticket]">
                                        <!--                                        <br>updated_end_ticket-->
                                        <input  type="hidden" id="updated_end_ticket"  name="data[lottery_daily_readings][updated_end_ticket]">
                                        <!--                                        <br>ticket_count-->
                                        <input  type="hidden" id="ticket_count" value="0"  name="data[lottery_daily_readings][ticket_count]">
                                        
                                        <input  type="hidden" id="ticket_value" value="0"  name="data[lottery_daily_readings][ticket_value]">
                                    
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <label>Status</label>
                                        <select class="form-control" name="data[lottery_daily_readings][status]" id="status">

                                            <option value="Counting">Counting</option>
                                            <option value="Sold Out">Sold Out</option>
                                            <option value="Return">Return</option>
                                            <option value="Settle">Settle</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid" style="margin-top: 24px;">Enter Daily Sales</button>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <a href="<?php echo Router::url('/') . 'admin/lottery/finish_daily_report'; ?>" class="btn default  finishbtn" style="margin-top: 24px;">Finish Daily Sales</a>


                                    </div>
                                </div>
                                <!-- row two ends -->
                            </div>
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <h1></h1>
                <div id="tab_1_5" class="tab-pane1">

                    <div class="table-responsive">


                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>

                                    <th><?php echo __($this->Paginator->sort('Bin Number')); ?>  </th>
                                    <th><?php echo __($this->Paginator->sort('Game Number')); ?>  </th>
                                    <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Game'); ?></th>

                                    <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>


                                    <th><?php echo $this->Paginator->sort('Start Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('End Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Previous Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Current Ticket#'); ?></th>
<!--                                    <th><?php echo $this->Paginator->sort('Available Tickets#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Sold Tickets#'); ?></th>-->
                                    
                                    <th><?php echo $this->Paginator->sort('Today Sold Tickets#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('$Today Sold Value'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Status'); ?></th>


                                </tr>
                            </thead>
                            <tbody id="reports">
                                <?php
                                if (isset($readings) && !empty($readings)) {
                                    $ms = "'Are you sure want delete it?'";
//pr($readings);

                                    foreach ($readings as $data) {
                                        echo '<tr>
                                                      
                                                    <td>' . $data["e"]["bin_no"] . '</td>
                                                    <td>' . $data["e"]["game_no"] . '</td>
                                                    <td>' . $data["e"]["pack_no"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["gamename"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["ticket_value"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["start_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["end_ticket"] . '&nbsp;</td>
                                                  
                                                    <td>' . $data["e"]["prev_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["today_reading"] . '&nbsp;</td>
                                                    <!--<td>' . $data["u"]["available"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["sold"] . '&nbsp;</td>-->
                                                    
                                                    <td>' . $data["e"]["today_sold"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["today_sold_value"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["status"] . '&nbsp;</td>
											    
                                                </tr>';
                                        //echo  '<td> <a href="'.Router::url('/').'admin/lottery/delete_apack/'.$data["GamePacks"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>'
                                    }
                                } else {
                                    echo ' <tr>
                                                <td colspan="12">  no result found </td>
                                            </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


        </div>
    </div>
    
    <script type="text/javascript" charset="utf-8">
        var webroot = '<?php echo $this->webroot; ?>';
        $(document).ready(function () {

            //$('#Date').Zebra_DatePicker();
            
            $('#search_report').click(function(){
                var date = $('#Date').val();
                //alert('date-'+date);
                $.ajax({
                    type:'POST',
                    url:webroot + 'lottery/get_reports',
                    //dataType:'html',
                    data:{date:date},
                    success:function(data){
                        //alert(data);
                        //$('#reports').html(data);
                        var result = JSON.parse(data);
                        var row = '';
                        $.each(result,function(k,v){
                            //alert(v.e.id);
                            
                            
                            row = row +'<tr>'+
                                                      
                                                   ' <td>' + v.e.bin_no + '</td>'+
                                                    '<td>' + v.e.game_no + '</td>'+
                                                    '<td>' + v.e.pack_no +'&nbsp;</td>'+
                                                    '<td>' + v.u.gamename + '&nbsp;</td>'+
                                                    '<td>' + v.u.ticket_value +  '&nbsp;</td>'+
                                                    '<td>' + v.e.start_ticket +'&nbsp;</td>'+
                                                    '<td>' + v.e.end_ticket + '&nbsp;</td>'+
                                                  
                                                    '<td>' + v.e.prev_ticket + '&nbsp;</td>'+
                                                    '<td>' + v.e.today_reading +'&nbsp;</td>'+
                                                    
                                                    
                                                    '<td>' + v.e.today_sold + '&nbsp;</td>'+
                                                    '<td>' + v.e.today_sold_value +'&nbsp;</td>'+
                                                    '<td>' + v.e.status + '&nbsp;</td>'+
											    
                                                '</tr>';
                            
                        });
                        $('#reports').html(row);
                           
                    }
                });
            });
            

            //$('#lottery_daily_readingsGameNo').change(function () {
            $('#lottery_daily_readingsBinNo').change(function () {
                //$('#lottery_daily_readingsAdminDailyReportForm')[0].reset();
                //var game_no = $('#lottery_daily_readingsGameNo').val();
                var bin_no = $('#lottery_daily_readingsBinNo').val();
                //alert(game_no);
                
                
                     $('#pack').empty();
                     $('#lottery_daily_readingsGameName').val('');
                     $('#totkt').val('');
                     $('#availa').val('');
                     
                if(bin_no == "")
                {
                    //alert(game_no);
                     $('#lottery_daily_readingsAdminDailyReportForm')[0].reset();
                     $('#pack').empty();
                     $('#lottery_daily_readingsGameName').val('');
                     $('#totkt').val('');
                     $('#availa').val('');
                }
                else
                {
                    $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/getpacksactive',
                    type: 'post',
                    data: {bin_no: bin_no},
                    success: function (data) {
//alert(data);

    
                        var result = JSON.parse(data);
                        
                        if(result.game_no == null)
                        {
                            alert("Sorry! Data not available.");
                            return false;
                        }
                        
                        var pack_opts = '';
                        var bin_opts = '';
                        var ticket_order = 0;
                        var start_ticket = result.ticket_data.ImportedGames.start_ticket;
                        var end_ticket = result.ticket_data.ImportedGames.end_ticket;
                        var game_name = result.ticket_data.ImportedGames.game_name;
                        
                        
                        
                        $.each(result.games,function(k,v){
                            //alert(v.GamePacks.bin_no);
                            
                            pack_opts = pack_opts + '<option>'+v.GamePacks.pack_no+'</option>';
                            //bin_opts = bin_opts + '<option>'+v.GamePacks.bin_no+'</option>';
                            ticket_order = v.GamePacks.ticket_order;
                            $('#availa').val(v.GamePacks.available);
                            $('#ticket_value').val(v.GamePacks.ticket_value);
                            
                        });
                        
                        $('#pack').html(pack_opts);
                        //$('#lottery_daily_readingsBinNo').html(bin_opts);
                        $('#starting_inventory').val(result.ticket_data.ImportedGames.tickets_pack);
                        $('#lottery_daily_readingsGameNo').val(result.game_no);
                        $('#lottery_daily_readingsGameName').val(game_name);
                        
                        var tokn = '';
                        if (ticket_order == 0)
                            { //alert(ticket_order + " -- "+result.ticket_data.ImportedGames.end_ticket);
                                $('#start_ticket').val(start_ticket);
                                $('#end_ticket').val(end_ticket);

                                for (var i = start_ticket; i <= end_ticket; i++)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                            if (ticket_order == 1)
                            {
                                $('#start_ticket').val(end_ticket);
                                $('#end_ticket').val(start_ticket);
                                for (var i = end_ticket; i >= start_ticket; i--)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                            
                            $('#totkt').html(tokn);
                        
                    }
                });
                }
                
                
            });

        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#Date").datepicker({dateFormat: 'dd-mm-yy'});
    });
</script>