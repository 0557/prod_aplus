<?php // pr($salesInvoice);                   ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <div class="page-content">
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-title">Lottery ticket sales </h3>
                    </div>
                </div>
            </div>
            <!-- top header ends -->
            <!-- submit form -->
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">

                            </div>
                            <!-- heading title ends-->
                            <!-- form starts -->
                            <?php echo $this->Form->create('lottery_daily_readings', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->

                                <div class="row">
                                    <div class="col-md-2 col-xs-2">
                                        <label>Scan/Type ticket code</label>
                                        <?php echo $this->Form->input('scan_ticket_code', array('id' => 'scan_ticket_code', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false)); ?>
                                    </div>

                                    <div class="col-md-2 col-xs-2" style="width:130px; ">
                                        <label>Select Date</label>
                                        <?php echo $this->Form->input('packdate', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => $pack_date)); ?>

                                    </div>
                                    <div class="col-md-2" style="width:120px;margin-top:4px;">

                                        <br>
                                        <input class="btn default updatebtn" type="button" id="search_report" name="search_report" value="Show Report" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-xs-2" style="text-align:left;margin-top: 10px">                
                                        <strong>___________________ Or __________________</strong>
                                    </div>

                                </div>


                                <div class="row" style="margin-top: 10px">



                                    <div class="col-xs-1 col-md-1">
                                        <label>Bin No</label>
                                        <?php
                                        $options = array();
                                        for ($i = 1; $i <= 40; $i++) {
                                            $options[$i] = $i;
                                        }
                                        ?>
                                        <?php echo $this->Form->input('bin_no', array('type' => 'select', 'class' => 'form-control', 'required' => false, 'label' => false, 'options' => $options, 'empty' => '')); ?>
                                    </div>


                                    <div class="col-xs-2 col-md-2" style="width:17%;">
                                        <label>Game No.</label>
                                        <?php
                                        //echo $this->Form->input('game_no', array('type' => 'hidden', 'class' => 'form-control', 'label' => false));
                                        //echo $this->Form->input('game_name', array('class' => 'form-control', 'label' => false));
                                        //echo $this->Form->input('game_no', array('type' => 'select', 'class' => 'form-control', 'label' => false, 'options' => $games, 'empty' => 'Select Game No.')); 
                                        ?>
                                        <?php echo $this->Form->input('game_no', array('type' => 'select', 'class' => 'form-control', 'id' => 'game_no_name', 'label' => false, 'empty' => '')); ?>                                        
                                    </div>

                                    <div class="col-xs-1 col-md-1" style="width:115px;">
                                        <label>Pack No</label>
                                        <?php echo $this->Form->input('pack_no', array('class' => 'form-control', 'id' => 'pack', 'label' => false,)); ?>

                                    </div>
                                    <!--<div class="col-md-2 col-xs-2" style="width:165px;">
                                        <label>Available Tk# on sale</label>
                                        

                                    <?php echo $this->Form->input('on_hand_tickets', array('id' => 'availa', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'readonly' => 'readonly', 'required' => false)); ?>
                                    </div>-->

                                    <div class="col-xs-1 col-md-1" style="width:100px;">
                                        <label>Prev Tkt#</label>
                                        <input class='form-control'  type="text" id="prev_ticket" value="0"  name="data[lottery_daily_readings][prev_ticket]">
                                    </div>
                                    <div class="col-md-2 col-xs-6"style="width:100px;">
                                        <label>Today Tkt#</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php //echo $this->Form->input('today', array('id' => 'totkt', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => true));  ?>

<!--                                        <input name="data[lottery_daily_readings][today]" id="totkt1" class="form-control" required="required" maxlength="11" type="text">-->

                                        <select class="form-control" name="data[lottery_daily_readings][today_reading]" id="totkt">

                                        </select>
                                        <!--                                        <br>
                                                                                prev_ticket-->

                                        <!--                                        <br>starting_inventory-->

                                        <!--                                        <br>ticket_order-->
                                        <input  type="hidden" id="ticket_order" value="0" name="data[lottery_daily_readings][ticket_order]">                                  
                                        <!--                                        <br>start_ticket-->
                                        <input  type="hidden" id="start_ticket"  name="data[lottery_daily_readings][start_ticket]">
                                        <!--                                        <br>end_ticket-->
                                        <input  type="hidden" id="end_ticket"  name="data[lottery_daily_readings][end_ticket]">
                                        <input  type="hidden" id="ticket_value" value="0"  name="data[lottery_daily_readings][ticket_value]">

                                    </div>

                                    <div class="col-xs-6 col-md-2" style="width:136px">
                                        <label>Status</label>
                                        <select class="form-control" name="data[lottery_daily_readings][status]" id="status">

                                            <option value="Counting">Counting</option>
                                            <option value="Sold Out">Sold Out</option>
                                            <option value="Return">Return</option>
                                            <option value="Settle">Settle</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2" style="width:13%;padding-right:0px" >
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid" style="margin-top: 24px;">Enter Daily Sales</button>
                                    </div>
                                    <div class="col-md-2" style="padding-left:0px">
                                        <label>&nbsp;</label>
                                        <a href="<?php echo Router::url('/') . 'admin/lottery/finish_daily_report'; ?>" class="btn default  finishbtn" style="margin-top: 24px;">Finish Daily Sales</a>


                                    </div>

                                    <!-- input 2 ends-->
                                </div>
                                <!-- row one ends -->
                                <!--row for or -->

                                <!-- row for or ends -->
                                <!-- row two start -->
                                <div class="row"><br/>
                                </div>

                                <div class="row">

                                </div>
                                <!-- row two ends -->
                            </div>
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <h1></h1>

                <div id="tab_1_5" class="tab-pane1">


                    <div class="table-responsive">


                        <table class="table table-striped table-bordered table-advance table-hover">

                            
                                
                            <thead>
                                <tr style="background-color:#f2f2f2; ">
                                    <td colspan="8"></td>
                                    <td >
                                        <div><br></div>
                                        <h4>Tickets Sold</h4>
                                    </td>
                                    <td colspan="3" style="text-align: left">
                                        <h4 id="amount">$100</h4>
                                        <b>Total amount</b>
                                    </td>
<!--                                    <div class="row" style="background-color: #f2f2f2;">
                                        <div class=" col-md-7">

                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div><br></div>
                                                    <h4>Tickets Sold</h4>
                                                </div>
                                                <div class="col-md-3">
                                                    <h4 id="amount">$100</h4>
                                                    <b>Total amount</b>
                                                </div>
                                            </div>
                                        </div>

                                    </div> -->
                                </tr>
                            <tr>

                                <th><?php echo __($this->Paginator->sort('Bin#')); ?>  </th>
                                <th><?php echo __($this->Paginator->sort('Game#')); ?>  </th>
                                <th><?php echo $this->Paginator->sort('Pack#'); ?></th>
                                <th><?php echo $this->Paginator->sort('Game Name'); ?></th>
                                <th><?php echo $this->Paginator->sort('Start Ticket#'); ?></th>
                                <th><?php echo $this->Paginator->sort('End Ticket#'); ?></th>
                                <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>
                                <th><?php echo $this->Paginator->sort('Previous Ticket#'); ?></th>
                                <th><?php echo $this->Paginator->sort('Current Ticket#'); ?></th>
<!--                                    <th><?php echo $this->Paginator->sort('Available Tickets#'); ?></th>
                                <th><?php echo $this->Paginator->sort('Sold Tickets#'); ?></th>-->

                                <th><?php echo $this->Paginator->sort('Today Sold Tickets#'); ?></th>
                                <th><?php echo $this->Paginator->sort('$Today Sold Amount'); ?></th>
                                <th><?php echo $this->Paginator->sort('Pack Status'); ?></th>


                            </tr>
                            </thead>
                            <tbody id="reports">
                                <?php
                                if (isset($readings) && !empty($readings)) {
                                    $ms = "'Are you sure want delete it?'";
//pr($readings);
                                    $total_amount = 0;
                                    foreach ($readings as $data) {
                                        echo '<tr>
                                                      
                                                    <td>' . $data["e"]["bin_no"] . '</td>
                                                    <td>' . $data["e"]["game_no"] . '</td>
                                                    <td>' . $data["e"]["pack_no"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["gamename"] . '&nbsp;</td>
                                                    
                                                    <td>' . $data["e"]["start_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["end_ticket"] . '&nbsp;</td>
                                                  <td>$' . $data["u"]["ticket_value"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["prev_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["today_reading"] . '&nbsp;</td>
                                                    <!--<td>' . $data["u"]["available"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["sold"] . '&nbsp;</td>-->
                                                    
                                                    <td>' . $data["e"]["today_sold"] . '&nbsp;</td>
                                                    <td>$' . $data["e"]["today_sold_value"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["status"] . '&nbsp;</td>
											    
                                                </tr>';
                                        //echo  '<td> <a href="'.Router::url('/').'admin/lottery/delete_apack/'.$data["GamePacks"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>'


                                        $total_amount = $total_amount + $data["e"]["today_sold_value"];
                                    }
                                } else {
                                    echo ' <tr>
                                                <td colspan="12">  no result found </td>
                                            </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php //echo "=============" . $total_amount; ?>

                        <input type="hidden" id="totalamount" value="<?php echo (isset($total_amount)) ? $total_amount : ''; ?>" />
                    </div>

                </div>
            </div>


        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
        var webroot = '<?php echo $this->webroot; ?>';
        $(document).ready(function () {

            //$('#Date').Zebra_DatePicker();
            var totalamount = $('#totalamount').val();
            if (totalamount != '')
            {
                totalamount = '$' + totalamount;
                $('#amount').html(totalamount);
            }
            else
            {
                $('#amount').html('$' + 0);
            }

            $('#search_report').click(function () {
                var date = $('#Date').val();
                $('#reports').html('');
                //alert('date-'+date);
                $.ajax({
                    type: 'POST',
                    url: webroot + 'lottery/get_reports',
                    //dataType:'html',
                    data: {date: date},
                    success: function (data) {
                        //alert(data);
                        //$('#reports').html(data);
                        var result = JSON.parse(data);
                        var row = '';
                        var totamount = 0;

                        $.each(result, function (k, v) {
                            //alert(v.e.id);


                            row = row + '<tr>' +
                                    ' <td>' + v.e.bin_no + '</td>' +
                                    '<td>' + v.e.game_no + '</td>' +
                                    '<td>' + v.e.pack_no + '&nbsp;</td>' +
                                    '<td>' + v.u.gamename + '&nbsp;</td>' +
                                    '<td>' + v.e.start_ticket + '&nbsp;</td>' +
                                    '<td>' + v.e.end_ticket + '&nbsp;</td>' +
                                    '<td>$' + v.u.ticket_value + '&nbsp;</td>' +
                                    '<td>' + v.e.prev_ticket + '&nbsp;</td>' +
                                    '<td>' + v.e.today_reading + '&nbsp;</td>' +
                                    '<td>' + v.e.today_sold + '&nbsp;</td>' +
                                    '<td>$' + v.e.today_sold_value + '&nbsp;</td>' +
                                    '<td>' + v.e.status + '&nbsp;</td>' +
                                    '</tr>';

                            totamount = parseInt(totamount) + parseInt(v.e.today_sold_value);

                        });
                        $('#reports').html(row);

                        if (totamount != '')
                        {
                            totamount = '$' + totamount;
                            $('#amount').html(totamount);
                        }
                        else
                        {
                            $('#amount').html('$' + 0);
                        }

                    }
                });
            });


            //$('#lottery_daily_readingsGameNo').change(function () {
            $('#lottery_daily_readingsBinNo').change(function () {
                //$('#lottery_daily_readingsAdminDailyReportForm')[0].reset();
                //var game_no = $('#lottery_daily_readingsGameNo').val();
                var bin_no = $('#lottery_daily_readingsBinNo').val();
                var date = $('#Date').val();
                //alert(game_no);
                if (date == '')
                {
                    alert('Please select date first.');
                    $('#lottery_daily_readingsAdminDailyReportForm')[0].reset();
                }

                $('#pack').empty();
                $('#lottery_daily_readingsGameName').val('');
                $('#totkt').val('');
                $('#availa').val('');

                if (bin_no == "")
                {
                    //alert(game_no);
                    $('#lottery_daily_readingsAdminDailyReportForm')[0].reset();
                    $('#pack').empty();
                    $('#lottery_daily_readingsGameName').val('');
                    $('#totkt').val('');
                    $('#availa').val('');
                }
                else
                {
                    $.ajax({
                        url: '<?php echo Router::url('/') ?>admin/lottery/getpacksactive1',
                        type: 'post',
                        data: {bin_no: bin_no, date: date},
                        success: function (data) {
//alert(data);
                            var result = JSON.parse(data);
                            var game_no_name = '<option value="">Select Game </option>';
                            var pack_opts = '';
                            var bin_opts = '';
                            var ticket_order = 0;
                            
                            $.each(result.games, function (k, v) {
                                //alert(v.GamePacks.game_no);

                                game_no_name = game_no_name + '<option value="' + v.GamePacks.game_no + '-'+ v.GamePacks.pack_no + '">' + v.GamePacks.game_no + ' - '
                                        + v.GamePacks.gamename + '</option>';
                            });

                            $('#game_no_name').html(game_no_name);
                        }
                    });
                }
            });

            $('#game_no_name').change(function () {
                //alert($(this).val());
                var game_no = $(this).val();
                var bin_no = $('#lottery_daily_readingsBinNo').val();
                var date = $('#Date').val();

                var ticket_order = 0;

                $.ajax({
                    type: 'post',
                    url: '<?php echo Router::url('/') ?>admin/lottery/getTicketData',
                    data: {game_no: game_no, bin_no: bin_no, date: date},
                    success: function (data) {
                        //alert(data);
                        var result = JSON.parse(data);
                        var ticket_order = 0;
                        var start_ticket = result.ticket_data.ImportedGames.start_ticket;
                        var end_ticket = result.ticket_data.ImportedGames.end_ticket;
                        var prev_ticket = result.prev_ticket;
                        ticket_order = result.ticket_order;

                        $('#pack').val(result.pack_no);
                        $('#ticket_order').val(ticket_order);
                        $('#ticket_value').val(result.ticket_value);


                        if (prev_ticket == '' || prev_ticket == null)
                        {
                            $('#prev_ticket').val('-1');
                        }
                        else
                        {
                            $('#prev_ticket').val(prev_ticket);
                        }



                        var tokn = '';
                        if (ticket_order == 0)
                        { //alert(ticket_order + " -- "+result.ticket_data.ImportedGames.end_ticket);
                            $('#start_ticket').val(start_ticket);
                            $('#end_ticket').val(end_ticket);

                            for (var i = start_ticket; i <= end_ticket; i++)
                            {
                                tokn = tokn + '<option >' + i + '</option>';
                            }
                        }
                        if (ticket_order == 1)
                        {
                            $('#start_ticket').val(end_ticket);
                            $('#end_ticket').val(start_ticket);
                            for (var i = end_ticket; i >= start_ticket; i--)
                            {
                                tokn = tokn + '<option >' + i + '</option>';
                            }
                        }

                        $('#totkt').html(tokn);
                    }
                });

            });

        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#Date").datepicker({dateFormat: 'mm-dd-yy'});
        });
    </script>