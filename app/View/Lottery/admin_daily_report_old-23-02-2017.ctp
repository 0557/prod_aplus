<?php // pr($salesInvoice);        ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <div class="page-content">
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-title">Lottery ticket sales </h3>
                    </div>
                </div>
            </div>
            <!-- top header ends -->
            <!-- submit form -->
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">

                            </div>
                            <!-- heading title ends-->
                            <!-- form starts -->
                            <?php echo $this->Form->create('lottery_daily_readings', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Scan/Type ticket code</label>
                                        <?php echo $this->Form->input('scan_ticket_code', array('id' => 'scan_ticket_code', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false)); ?>
                                    </div>
                                    <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>Game number</label>
                                        <?php echo $this->Form->input('game_no', array('type' => 'select', 'class' => 'form-control', 'label' => false, 'options' => $games, 'empty' => '')); ?>
                                    </div>

                                    <div class="col-xs-6 col-md-2">
                                        <label>Pack number</label>
                                        <?php echo $this->Form->input('pack_no', array('type' => 'select', 'class' => 'form-control', 'id' => 'pack', 'label' => false, 'empty' => '')); ?>

                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>Bin number</label>
                                        <?php
                                            $options = array();
                                            for ($i = 1; $i <= 40; $i++) {
                                                $options[$i] = $i;
                                            }
                                        ?>
                                        <?php echo $this->Form->input('bin_no', array('type' => 'select', 'class' => 'form-control', 'required' => false, 'label' => false, 'options' => $options, 'empty' => '')); ?>
                                    </div>

                                    <!-- input 2 ends-->
                                </div>
                                <!-- row one ends -->
                                <!--row for or -->

                                <!-- row for or ends -->
                                <!-- row two start -->
                                <div class="row"><br/>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-xs-6">
                                        <label>Available Tk# on sale</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php echo $this->Form->input('on_hand_tickets', array('id' => 'availa', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'readonly' => 'readonly', 'required' => false)); ?>
                                    </div>

                                    <div class="col-md-2 col-xs-6">
                                        <label>Today Tkt#</label>
                                        <!-- important for developer please place your clander code on this input-->

                                        <?php //echo $this->Form->input('today', array('id' => 'totkt', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => true)); ?>

<!--                                        <input name="data[lottery_daily_readings][today]" id="totkt1" class="form-control" required="required" maxlength="11" type="text">-->

                                        <select class="form-control" name="data[lottery_daily_readings][today_reading]" id="totkt">

                                        </select>
                                        <input  type="hidden" id="prev_ticket" value="0"  name="data[lottery_daily_readings][prev_ticket]">
                                        <input  type="hidden" id="starting_inventory" value="0"  name="data[lottery_daily_readings][starting_inventory]">
                                        <input  type="hidden" id="ticket_order" value="0" name="data[lottery_daily_readings][ticket_order]">                                  
                                        <input  type="hidden" id="start_ticket"  name="data[lottery_daily_readings][start_ticket]">
                                        <input  type="hidden" id="end_ticket"  name="data[lottery_daily_readings][end_ticket]">
                                        <input  type="hidden" id="updated_start_ticket"  name="data[lottery_daily_readings][updated_start_ticket]">
                                        <input  type="hidden" id="updated_end_ticket"  name="data[lottery_daily_readings][updated_end_ticket]">
                                        <input  type="hidden" id="ticket_count" value="0"  name="data[lottery_daily_readings][ticket_count]">
                                    </div>
                                    <!--                                    <div class="col-md-2 col-xs-2" style="visibility: hidden;" id="revdiv">
                                                                            <input  type="hidden" id="hidden_today_order" value="0" name="data[lottery_daily_readings][today_order]">
                                                                            <label>Change Order</label><br>
                                                                            <a href="#" id="reverse" >Reverse</a><img id="revimg" style="height:30px" src="<?php echo $this->webroot ?>img/up.png" />
                                      
                                    </div>-->
                                    <div class="col-xs-6 col-md-2">
                                        <label>Status</label>
                                        <select class="form-control" name="data[lottery_daily_readings][status]" id="status">
                                            <option value="">Select Status</option>
                                            <option value="Counting">Counting</option>
                                            <option value="Sold Out">Sold Out</option>
                                            <option value="Return">Return</option>
                                            <option value="Settle Pack">Settle Pack</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid" style="margin-top: 24px;">Enter Daily Sales</button>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label>&nbsp;</label>
                                        <a href="<?php echo Router::url('/') . 'admin/lottery/finish_daily_report'; ?>" class="btn default  finishbtn" style="margin-top: 24px;">Finish Daily Sales</a>


                                    </div>
                                </div>
                                <!-- row two ends -->
                            </div>
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <h1></h1>
                <div id="tab_1_5" class="tab-pane1">

                    <div class="table-responsive">


                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>

                                    <th><?php echo __($this->Paginator->sort('Bin Number')); ?>  </th>
                                    <th><?php echo __($this->Paginator->sort('Game Number')); ?>  </th>
                                    <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Game'); ?></th>

                                    <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>


                                    <th><?php echo $this->Paginator->sort('Start Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('End Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Previous Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Current Ticket#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Available Tickets#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Sold Tickets#'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Status'); ?></th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($readings) && !empty($readings)) {
                                    $ms = "'Are you sure want delete it?'";


                                    foreach ($readings as $data) {
                                        echo '<tr>
                                                      
                                                    <td>' . $data["e"]["bin_no"] . '</td>
                                                    <td>' . $data["e"]["game_no"] . '</td>
                                                    <td>' . $data["e"]["pack_no"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["gamename"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["ticket_value"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["start_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["end_ticket"] . '&nbsp;</td>
                                                  
                                                    <td>' . $data["e"]["prev_ticket"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["today_reading"] . '&nbsp;</td>
                                                    <td>' . $data["u"]["available"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["sold"] . '&nbsp;</td>
                                                    <td>' . $data["e"]["status"] . '&nbsp;</td>
											    
                                                </tr>';
                                        //echo  '<td> <a href="'.Router::url('/').'admin/lottery/delete_apack/'.$data["GamePacks"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>'
                                    }
                                } else {
                                    echo ' <tr>
                                                <td colspan="12">  no result found </td>
                                            </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


        </div>
    </div>


    <script type="text/javascript" charset="utf-8">

        $(document).ready(function () {

            $('#Date').Zebra_DatePicker();


            $('#lottery_daily_readingsGameNo').change(function () {

                var game_no = $('#lottery_daily_readingsGameNo').val();
                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/getpacksactive',
                    type: 'post',
                    data: {game_no: game_no},
                    success: function (data) {

                        $('#pack').html(data);

                    }
                });
            });



            $('#pack').change(function () {
                $('#ticket_count').val(0);
                var gno = $('#lottery_daily_readingsGameNo').val();
                var pack = $('#pack').val();

                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/gettcikets',
                    type: 'post',
                    data: {game_no: gno, pack_no: pack},
                    success: function (data) {
                        //alert(data);
                        data = JSON.parse(data);

//alert(data.availa);
//alert(data.today);
//alert(data.today_order);
//$.each(data,function(k,v){
//   $.each(data,function(a,b){
//      alert(b.LotteryDailyReading.today);
//   });
//});
                        var start_ticket = parseInt(data.start_ticket);
                        var end_ticket = parseInt(data.end_ticket);
                        var starting_inventory = parseInt(data.starting_inventory);
                        var updated_start_ticket = parseInt(data.updated_start_ticket);
                        var updated_end_ticket = parseInt(data.updated_end_ticket);
                        var avail = parseInt(data.availa);
                        var ticket_order = parseInt(data.ticket_order);
                        var today_reading = parseInt(data.today_reading);

                        $('#starting_inventory').val(starting_inventory);
                        $('#ticket_order').val(ticket_order);
                        if (isNaN(today_reading))
                        {
                            $('#prev_ticket').val(0);
                        }
                        else
                        {
                            $('#prev_ticket').val(today_reading);
                        }

                        //alert(ticket_order);
                        var tokn = '';
                        if (isNaN(updated_start_ticket))
                        {
                            $('#updated_start_ticket').val(0);
                            $('#updated_end_ticket').val(0);
                            if (ticket_order == 0)
                            {
                                $('#start_ticket').val(start_ticket);
                                $('#end_ticket').val(end_ticket);

                                for (var i = start_ticket; i <= end_ticket; i++)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                            if (ticket_order == 1)
                            {
                                $('#start_ticket').val(end_ticket);
                                $('#end_ticket').val(start_ticket);
                                for (var i = end_ticket; i >= start_ticket; i--)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                        }
                        else
                        {
                            if (ticket_order == 0)
                            {
                                $('#start_ticket').val(start_ticket);
                                $('#end_ticket').val(end_ticket);
                                $('#updated_start_ticket').val(updated_start_ticket);
                                $('#updated_end_ticket').val(updated_end_ticket);

                                for (var i = updated_start_ticket; i <= updated_end_ticket; i++)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                            if (ticket_order == 1)
                            {
                                $('#start_ticket').val(end_ticket);
                                $('#end_ticket').val(start_ticket);
                                $('#updated_start_ticket').val(updated_start_ticket);
                                $('#updated_end_ticket').val(updated_end_ticket);

                                for (var i = updated_start_ticket; i >= updated_end_ticket; i--)
                                {
                                    tokn = tokn + '<option >' + i + '</option>';
                                }
                            }
                        }

                        $('#availa').val(data.availa);
                        $('#totkt').html(tokn);


//                        var start_ticket = parseInt($("#totkt option:selected").text());
////alert(ticket_count);
//                        if ((ticket_order == 0) && (start_ticket > 0))
//                        {
//                            $('#ticket_count').val(1);
//                        }
//                        if((ticket_order == 1) && (0 == updated_end_ticket))
//                        {
//                            $('#ticket_count').val(0);
//                        }
//                        else
//                        {
//                            $('#ticket_count').val(1);
//                        }


                    }
                });


            });


            $('#totkt').change(function () {

//                var ticket_order = parseInt($('#ticket_order').val());
//                var ticket_count = $("#totkt").prop('selectedIndex');
//                //alert(ticket_count);
//                
//                var ticket_no = parseInt($("#totkt").text());
//                var avail = parseInt($('#availa').val());
//                if((ticket_order == 1) && (ticket_no == avail))
//                {
//                    $('#ticket_count').val(0);
//                }
//                
//                ticket_count = parseInt(ticket_count) + 1;
//                $('#ticket_count').val(ticket_count);

            });

            $('#reverse').click(function () {

                var $select = $('#totkt');
                var options = $select.find('option');
                options = [].slice.call(options).reverse();
                //alert(options);
                //$select.empty();
                $.each(options, function (i, el) {
                    $select.append($(el));
                });
                $('#totkt option').first().attr('selected', 'selected');

                var src_old = $('#revimg').attr('src');
                imgpa = src_old.split('/');
                imgname = imgpa[3];

                if (imgname == "up.png")
                {
                    src = '<?php echo $this->webroot ?>' + 'img/down.png';
                }
                else
                {
                    src = '<?php echo $this->webroot ?>' + 'img/up.png';
                }
                $('#revimg').attr('src', src);

                var ord = $('#hidden_today_order').val();
                if (ord == 0)
                {
                    $('#hidden_today_order').val(1);
                }
                else
                {
                    $('#hidden_today_order').val(0);
                }
            });

            $('#scan_ticket_code').change(function () {

                var scan = $('#scan_ticket_code').val();

                $.ajax({
                    url: '<?php echo Router::url('/') ?>admin/lottery/getgamedata',
                    type: 'post',
                    data: {scan: scan},
                    success: function (data) {
                        var json = jQuery.parseJSON(data);
                        //alert(json['prev']);
                        if (json['gno'] != null) {
                            $('#lottery_daily_readingsGameNo').html('<option value="' + json['gno'] + '">' + json['gno'] + '</option>');

                        } else {
                            location.reload();
                        }
                        if (json['pack'] != null) {

                            $('#pack').html('<option value="' + json['pack'] + '">' + json['pack'] + '</option>');

                            $('#availa').val(json['prev']);
                        } else {
                            location.reload();

                        }

                    }
                });
            });




        });



    </script>