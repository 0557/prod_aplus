<?php
//if (isset($this->request->data) && !empty($this->request->data)) {
//  pr($this->request->data);
//  die;
// } 
?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker')); ?>       

<?php echo $this->Form->create('SalesInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Fuel Sales INVOICE
                    </div>

                </div>
            </div>
        </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Order Details
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice TO Store:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'options' => $store, 'empty' => 'Select Store')); ?>
<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bill to:			 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('store_bill_to', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Bill to')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship to:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('store_ship_to', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Ship to')); ?>
                                    <input type="checkbox" id="same_as_address" class="same_as_address"><span>Same as Address</span>
                                </div>

                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">

                                    Invoice date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d", strtotime($this->request->data['SalesInvoice']['invoice_date'])); ?>T15:25:00Z">
<?php echo $this->Form->input('invoice_date', array('class' => 'form-control', 'type' => 'text', 'value' => date('d/m/Y - H:m', strtotime($this->request->data['SalesInvoice']['invoice_date'])), 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship date:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date("Y-m-d", strtotime($this->request->data['SalesInvoice']['ship_date'])); ?>T15:25:00Z">
<?php echo $this->Form->input('ship_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'value' => date('d/m/Y - H:m', strtotime($this->request->data['SalesInvoice']['ship_date'])), 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Po:			 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Po')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice:			 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('incoice_type', array('class' => 'form-control', 'type' => 'text', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Invoice')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Status:	 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Customer Information
                            </div>
                            <div class="actions">

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice to Customer
                                    <!--                                    Supplier:-->
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('customer_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $wholesale_customer, 'empty' => 'Select Customer')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bill to:			 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('customer_bill_to', array('class' => 'form-control', 'type' => 'text', 'label' => false,  'required' => 'false', 'placeholder' => 'Bill to')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Ship to:			 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('customer_ship_to', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Ship to')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Due date:
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime extra_date_picker_error" data-date="<?php echo date('Y-m-d', strtotime($this->request->data['SalesInvoice']['due_date'])); ?>T15:25:00Z">
<?php echo $this->Form->input('due_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'value' => date('d/m/Y - H:m', strtotime($this->request->data['SalesInvoice']['due_date'])), 'readonly' => 'readonly')); ?>

                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Bol:	 
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Bol')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terminal:
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Terminal')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terms:
                                </div>
                                <div class="col-md-7 value">
<?php echo $this->Form->input('terms', array('class' => 'form-control', 'type' => 'text', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter terms')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Fuel Sales Products
                    </div>
                    <div class="actions">

                    </div>
                </div>

                <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Products</th>
                                    <th>Gallons Delivered</th>
                                    <th>Cost Per Gallons</th>
                                    <th>Net Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more">
                                <?php $total = 0; ?>
                                <?php
                                if (isset($this->request->data['SalesInvoice']['product_id']) && !empty($this->request->data['SalesInvoice']['product_id'])) {
                                    $productDetail = explode(',', $this->request->data['SalesInvoice']['product_id']);
                                    $productQuantity = explode(',', $this->request->data['SalesInvoice']['product_quantity']);
                                    $gallonsdelivered = explode(',', $this->request->data['SalesInvoice']['gallons_delivered']);
                                    ?>
                                    <?php
                                    foreach ($productDetail as $key => $data) {
                                        $nameAndId = $this->Custom->GetProductName($data);
                                        ?>
                                        <tr>
                                            <td style="background-color: rgb(249, 249, 249);">
                                                <?php echo $this->Form->input('SalesInvoice.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'type' => 'hidden', 'id' => false, 'label' => false, 'div' => false, 'value' => $nameAndId['WholesaleProduct']['id'])); ?>
                                            <?php echo $nameAndId['WholesaleProduct']['name']; ?>&nbsp</td>
        <?php $total = $gallonsdelivered[$key] * $productQuantity[$key]; ?>
                                            <td style="background-color: rgb(249, 249, 249); " class="max_open" ><?php echo $this->Form->input('SalesInvoice.gallons_delivered.', array('label' => false, 'value' => $gallonsdelivered[$key], 'class' => 'form-control', 'onkeyup' => 'quantity_amount()', 'type' => 'text', 'placeholder' => '0')); ?> </td>
                                            <td class="enter_product_quantity"><?php echo $this->Form->input('SalesInvoice.product_quantity.', array('class' => 'form-control', 'type' => 'text', 'value' => $productQuantity[$key], 'label' => false, 'id' => '', 'onkeyup' => 'quantity_amount()', 'required' => 'false', 'placeholder' => 'Enter  product quantity')); ?></td>
                                            <td class="net_amount">   <?php echo $total; ?> </td>
                                            <td style="background-color: rgb(249, 249, 249); "><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>

                                    <?php } ?>  
                                <?php } ?>
                            </tbody>
                        </table>
                        <button type="button" class="btn blue" onclick="AddProduct();" style="float:right;">Add More</button>
                        </br>
                        </br>
                        <!-- Add sales invoice table   -->
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Tax Zone</th>
                                    <th>USA State</th>
                                    <th>Tax Description</th>   
                                    <th>Tax on Gallon Delivered(Qty)</th>
                                    <th>Rate(%)on Gallon</th>
                                    <th>Net Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="gallon_add_more">
                                <?php $total = 0; ?>

                                <?php if (isset($this->request->data['TaxeZone']['tax_zone']) && !empty($this->request->data['TaxeZone']['tax_zone'])) { ?>
                                <?php foreach ($this->request->data['TaxeZone']['tax_zone'] as $key => $data) { ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $data, 'empty' => 'Select Element', 'required' => false, "options" => array('Federal' => 'Federal', 'Country' => 'Country', 'State' => 'State', 'Municipality' => 'Municipality'))); ?>&nbsp;</td>
                                            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $this->request->data['TaxeZone']['state_id'][$key], 'empty' => 'Select State', 'required' => false, "options" => $usa_state)); ?></td>

                                            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'id' => '', 'value' => $this->request->data['TaxeZone']['tax_decription'][$key], 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'value' => 0, 'class' => 'form-control SalesInvoiceTaxOnGallonQty', 'value' => $this->request->data['TaxeZone']['tax_on_gallon_qty'][$key], 'type' => 'text', 'placeholder' => '0')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide ', 'type' => 'text', 'label' => false, 'onkeyup' => 'percentage($(this));', 'value' => $this->request->data['TaxeZone']['rate_on_gallon'][$key], 'id' => 'rate_on_gallon', 'required' => 'false', 'placeholder' => '0')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'value' => 0, 'class' => 'form-control value_after_tax', 'value' => $this->request->data['TaxeZone']['tax_net_amount_gallon'][$key], 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                        <?php
                                    }
                                } else if (isset($this->request->data['TaxeZone']) && !empty($this->request->data['TaxeZone'])) {
                                    ?>
                                            <?php foreach ($this->request->data['TaxeZone'] as $key => $data) { // pr($data);  ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $data, 'empty' => 'Select Element', 'required' => false, "options" => array('Federal' => 'Federal', 'Country' => 'Country', 'State' => 'State', 'Municipality' => 'Municipality'))); ?>&nbsp;</td>
                                            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'selected' => $data['state_id'], 'empty' => 'Select State', 'required' => false, "options" => $usa_state)); ?></td>

                                            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'id' => '', 'value' => $data['tax_decription'], 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'value' => 0, 'class' => 'form-control SalesInvoiceTaxOnGallonQty', 'value' => $data['tax_on_gallon_qty'], 'type' => 'text', 'placeholder' => '0')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide ', 'type' => 'text', 'label' => false, 'onkeyup' => 'percentage($(this));', 'value' => $data['rate_on_gallon'], 'id' => 'rate_on_gallon', 'required' => 'false', 'placeholder' => '0')); ?></td>
                                            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'value' => 0, 'class' => 'form-control value_after_tax', 'value' => $data['tax_net_amount_gallon'], 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" class="btn blue" onclick="gallonAddProduct();" style="float:right;">Add More</button>
                </div>
            </div>

        </div>

        <ul class="list-unstyled amounts total_last">
            <li class="last_content_first">
                <span class="span_1">Gallon Delivered Total</span>
                <span class="span_2" id="Gallon_Delivered_Total">
<?php echo $this->Form->input('max_qnty', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1">Net Amount Total</span>
                <span class="span_2" id="Net_Amount_Total">
<?php echo $this->Form->input('total_invoice', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_three">
                <span class="span_1">Taxes</span>
                <span class="span_2" id="Taxes">
<?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['SalesInvoice']['taxes'])) ? $this->request->data['SalesInvoice']['taxes'] : '0', 'onblur' => "getvalue()", 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0', 'onkeyup' => 'getvalue()')); ?>
                </span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Gross Amount</span>
                <span class="span_2" id="Taxes">
<?php echo $this->Form->input('gross_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_five">
                <div class="credit_span">
                    <span class="span_1">MOP : - </span>
<?php echo $this->Form->input('SalesInvoice.mop', array('class' => 'select_list_mop', 'onchange' => "mop_function(this.value)", 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'check'))); ?>
                </div>
                <span class="span_2" id="MOP">

                </span>
            </li>



        </ul>

    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('SalesInvoice.product_id.', array('class' => 'form-control exampleInputName product_ajax', 'empty' => 'Select Product', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', /* 'multiple' => true, */ /* 'multiple' => 'checkbox', */ "options" => $product)); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>
<table style="display:none">
    <tbody id="gallon_AddMore">
        <tr>
            <td><?php echo $this->Form->input('TaxeZone.tax_zone.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Element', 'required' => false, "options" => array('Federal' => 'Federal', 'Country' => 'Country', 'State' => 'State', 'Municipality' => 'Municipality'))); ?>&nbsp;</td>
            <td><?php echo $this->Form->input('TaxeZone.state_id.', array('class' => 'form-control exampleInputName', 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select State', 'required' => false, "options" => $usa_state)); ?></td>

            <td><?php echo $this->Form->input('TaxeZone.tax_decription.', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'id' => '', 'required' => 'false', 'placeholder' => 'Tax Decription')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_on_gallon_qty.', array('label' => false, 'value' => 0, 'onkeyup' => 'percentage($(this));', 'class' => 'form-control SalesInvoiceTaxOnGallonQty', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.rate_on_gallon.', array('class' => 'form-control rate_tax_decide', 'type' => 'text', 'label' => false, 'id' => '', 'required' => 'false', 'value' => '0', 'placeholder' => '0')); ?></td>
            <td><?php echo $this->Form->input('TaxeZone.tax_net_amount_gallon.', array('label' => false, 'value' => 0, 'class' => 'form-control value_after_tax', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></td>
            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>
<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->

<script>

    function percentage_final() {
        var total_pr = 0;
        $('.value_after_tax').each(function() {
            total_pr = total_pr + parseFloat($(this).val());
        });
        $('#SalesInvoiceTaxes').val(total_pr);
        getvalue();
    }

    function percentage(currentval) {
        var value = currentval.val();
        var total = $('.SalesInvoiceTaxOnGallonQty').val();
        var farr = parseFloat(total) * value / 100;
        var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(farr);
        percentage_final();
    }

    function mop_function(value) {
        $('#MOP').text(value);
    }
    function gallonAddProduct() {
        $("#gallon_add_more").append($("#gallon_AddMore").html());
        var max_qntyc = parseFloat(jQuery('#SalesInvoiceMaxQnty').val());
        //var max_qntyc =  parseFloat(jQuery('#SalesInvoiceMaxQnty').val());
        $('.SalesInvoiceTaxOnGallonQty').attr('value', max_qntyc);
    }

    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }
    $(document).ready(function() {
        // initiate layout and plugins
        //App.init();
        var selected_mop = '<?php echo $this->request->data['SalesInvoice']['mop'] ?>';
        $('#MOP').text(selected_mop);
        ComponentsPickers.init();
        getvalue();
        percentage_final();
    });


    $('#same_as_address').change(function() {
       
        if ($(this).prop('checked')) {
            $('#SalesInvoiceCustomerBillTo').val($('#SalesInvoiceStoreBillTo').val());
            $('#SalesInvoiceCustomerBillTo').prop('readonly', true);
            $('#SalesInvoiceCustomerShipTo').val($('#SalesInvoiceStoreShipTo').val());
            $('#SalesInvoiceCustomerShipTo').prop('readonly', true);
        }
        else {
            $('#SalesInvoiceCustomerBillTo').val('');
            $('#SalesInvoiceCustomerBillTo').prop('readonly', false);
            $('#SalesInvoiceCustomerShipTo').val('');
            $('#SalesInvoiceCustomerShipTo').prop('readonly', false);
        }
    });

    $(document).delegate('.product_ajax', 'change', function() {
        var category = jQuery(this).val();

        $('.product_ajax').removeClass('active');
        $(this).addClass('active');
        var avg = jQuery.ajax({
            url: '<?php echo Router::url('/'); ?>sales_invoices/products/' + category,
            processData: false,
            type: 'POST',
            success: function(data) {
                $('.product_ajax.active').parents('tr').html(data);
                getvalue();
            },
        });


    });

    function getvalue() {
        var Gross_Amount = 0;
        var max_qnty = 0;
        var max_total = 0;
        var tax = 0;


        $('td.net_amount').each(function() {
            max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).text());
        });

        if (max_qnty == null || max_qnty == '' || isNaN(max_qnty)) {

            max_qnty = 0;
        }


//           var tax_rate = parseFloat(jQuery('#rate_on_gallon').find('input').val());
//           alert(tax_rate);
        $('td.max_open').each(function() {
            max_total = parseFloat(max_total) + parseFloat(jQuery(this).find('input').val());

            if (max_total == null || max_total == '' || isNaN(max_total)) {

                max_total = 0;
            }
            //max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());
        });

        tax = $('#SalesInvoiceTaxes').val();
        if (tax == null || tax == '') {
            tax = 0;
        }
        Gross_Amount = parseFloat(max_qnty + parseFloat(tax));

        $('.SalesInvoiceTaxOnGallonQty').val(max_total);

        $('#SalesInvoiceMaxQnty').val(max_total);

        $('#SalesInvoiceTotalInvoice').val(max_qnty);
        $('#SalesInvoiceGrossAmount').val(Gross_Amount);

    }

    function quantity_amount() {
        var product_amount = 0;
        var total_amt_final = 0;
        var max_open = 0;

        $('td.max_open').each(function() {
            max_open = parseFloat(jQuery(this).find('input').val());
            if (max_open == null || max_open == '' || isNaN(max_open)) {
                max_open = 0;
            }

            //var max_open = parseFloat(jQuery(this).text());
            var max_open_qty = parseFloat(jQuery(this).next('td').find('input').val());
            var total_amt = parseFloat(max_open * max_open_qty);
            if (total_amt == null || total_amt == '' || isNaN(total_amt)) {
                total_amt = 0;
            }
            jQuery(this).next('td').next('td').text(total_amt);
            total_amt_final = total_amt_final + total_amt;


            product_amount = parseFloat(max_open) + parseFloat(jQuery(this).text());
            //alert(product_amount);
        });
        //   $('#FuelInvoiceMaxQnty').val(max_total);
        $('#SalesInvoiceTotalInvoice').val(total_amt_final);
        getvalue();

    }

    $(document).delegate('.Delete_product', 'click', function() {
        $(this).parents('tr').remove();
        getvalue();
    });

</script>

