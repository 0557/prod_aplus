<div class="page-content-wrapper">
    <div class="portlet box blue">
        <div class="page-content portlet-body" >

            <!-- BEGIN PAGE HEADER-->

            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <h3>     <i class="fa fa-reorder"></i>Add Game</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">

                    <div class="col-md-9 col-sm-12">
                        <div class="portlet yellow box">


                            <div class="portlet-body extra_tt">
                                <?php
                                
                                
                                
                                
                                
                                ?>
                                <?php echo $this->Form->create('ImportedGames');?>   

                                    <div class="row static-info">
                                        <!--                                        <div class="col-md-5 name">
                                                                                    Game No
                                        
                                                                                </div>-->
                                        <div class="col-md-3 value">
                                            Game Number
                                            
                                            <?php echo $this->Form->input('game_no', array("class"=>"form-control",'label'=>false,'placeholder'=>'Game Number'));?>
                                            <input type="hidden" name="id" value=""  class="form-control" readonly/>
                                        </div>

                                        <div class="col-md-6 value">
                                            Game Name
                                            <?php echo $this->Form->input('game_name', array("class"=>"form-control",'label'=>false,'placeholder'=>'Game Name'));?>
                                        </div>
                                        <div class="col-md-3 value">
                                            Ticket Per  Value
                                            <?php echo $this->Form->input('value', array("id"=>"ticktpervalue","class"=>"form-control",'label'=>false,'placeholder'=>'Ticket Per  Value'));?>
                                        </div>
                                    </div>


                                
                                
                                
                                    <div class="row static-info">
                                        
                                        <div class="col-md-3 value">
                                            Starting Ticket Number:
                                            <?php echo $this->Form->input('start_ticket', array("id"=>"startTickt","class"=>"form-control",'label'=>false,'placeholder'=>'Starting Ticket Number'));?>
                                        </div>
                                        <div class="col-md-3 value">
                                            Ending Ticket Number:
                                            <?php echo $this->Form->input('end_ticket', array("id"=>"endTickt","class"=>"form-control",'label'=>false,'placeholder'=>'Ending Ticket Number'));?>
                                        </div>
                                        <div class="col-md-3 value">
                                            Tickets Per Pack:
                                            <?php echo $this->Form->input('tickets_pack', array("id"=>"ticktPack","class"=>"form-control",'label'=>false,'placeholder'=>'Tickets Per Pack'));?>
                                        </div>
                                        
                                         <?php /*?><div class="col-md-3 value">
                                            Pack Value:
                                            <?php echo $this->Form->input('pack_value', array("id"=>"pack_value","class"=>"form-control",'label'=>false,'placeholder'=>'Pack Value'));?>
                                        </div><?php */?>
                                        
                                        <div class="col-md-3 value">
                                            State
                                            <select id="state" name="data[ImportedGames][state]" class="form-control">
                                                <option value=""> Select State</option>
                                                <option value="TX">TX</option>
                                                <option value="GA">GA</option>
                                                <option value="LA">LA</option>
                                                <option value="AZ">AZ</option>
                                                <option value="AR">AR</option>
                                                <option value="FL">FL</option>
                                                <option value="PA">PA</option>
                                                <option value="TN">TN</option>
                                                <option value="NY">NY</option>
                                                <option value="MO">MO</option>
                                                <option value="VA">VA</option>
                                                <option value="IL">IL</option>
                                                <option value="MA">MA</option>
                                                <option value="SD">SD</option>
                                                <option value="NC">NC</option>
                                                <option value="SC">SC</option>
                                                <option value="CO">CO</option>
                                                <option value="KS">KS</option>
                                                <option value="NJ">NJ</option>
                                                <option value="NE">NE</option>
                                                <option value="IA">IA</option>
                                                <option value="MI">MI</option>
                                                <option value="IN">IN</option>
                                                <option value="OK">OK</option>
                                                <option value="WI">WI</option>
                                                <option value="CA">CA</option>

                                            </select>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                <script>
                                
                                $(document).ready(function(){
									
                                    //alert();
                                    $('#startTickt').keyup(function(){
                                        var st = $(this).val();
                                        var et = $('#endTickt').val();
                                        var tp = (parseInt(et) - parseInt(st))+parseInt(1);
                                        tp = Math.abs(tp);
                                        $('#ticktPack').val(tp);
                                    });
                                    $('#endTickt').keyup(function(){
                                        var et = $(this).val();
                                        var st = $('#startTickt').val();
                                        var tp = (parseInt(et) - parseInt(st))+parseInt(1);
                                        tp = Math.abs(tp);
                                        $('#ticktPack').val(tp);
                                    });
									
									
									var $ticktPack = $("#ticktPack");									
									$ticktPack.data("value", $ticktPack.val());									
									setInterval(function() {
									var data = $ticktPack.data("value"),
									val = $ticktPack.val();									
									if (data !== val) {
									$ticktPack.data("value", val);
									//alert($("#ticktPack").val()); 
									var tp = $("#ticktPack").val();
									var tv = $('#ticktpervalue').val();
									var pv = (parseInt(tp) * parseInt(tv));
									pv = Math.abs(pv);
									//$('#pack_value').val(pv);
									}
									}, 100);							
									
									 $('#ticktPack').keyup(function(){
                                        var tp = $(this).val();
                                        var tv = $('#ticktpervalue').val();
                                        var pv = (parseInt(tp) * parseInt(tv));
                                        pv = Math.abs(pv);
                                       // $('#pack_value').val(pv);
                                    });
									 $('#ticktpervalue').keyup(function(){
                                        var tv = $(this).val();
										if(!$.isNumeric(tv)) {
										alert('Please enter only numeric number.');
										$("#ticktpervalue").val('');																		
										}
										
                                        var tp = $('#ticktPack').val();
                                        var pv = (parseInt(tp) * parseInt(tv));
                                        pv = Math.abs(pv);
                                        //$('#pack_value').val(pv);
                                    });                                  
									
                                });</script>
                                
                                    


                                    <div class="row static-info">
                                        <div class="col-md-5 name">

                                        </div>

                                        <div class="col-md-7 value">
                                            <?php echo $this->Form->submit('Add Lottery Game',array('class'=>'btn btn-info'));?>
                                            <?php echo $this->Form->end();?>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <style>
        .form-control{
            margin:10px;
        }</style>