<div class="page-content-wrapper">
    <div class="portlet box blue">
        <div class="page-content portlet-body" >

            <!-- BEGIN PAGE HEADER-->

            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <h3>     <i class="fa fa-reorder"></i> Edit imported game</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <div class="portlet yellow box">


                            <div class="portlet-body extra_tt">
                                <?php
                                ?>
                                <?php echo $this->Form->create('ImportedGames'); ?>   

                                <div class="row static-info">
                                    <!--                                        <div class="col-md-5 name">
                                                                                Game No
                                    
                                                                            </div>-->
                                    <div class="col-md-2 value">
                                        <label>Game Number</label>

                                        <?php echo $this->Form->input('game_no', array("class" => "form-control", 'label' => false, 'placeholder' => 'Game Number')); ?>
                                        <input type="hidden" name="id" value=""  class="form-control" readonly/>
                                    </div>

                                    <div class="col-md-3 value">
                                        <label>Game Name</label>
                                        <?php echo $this->Form->input('game_name', array("class" => "form-control", 'label' => false, 'placeholder' => 'Game Name')); ?>
                                    </div>
                                    <div class="col-md-2 value">
                                        <label>Ticket Value</label>
                                        <?php echo $this->Form->input('value', array("class" => "form-control", 'label' => false, 'placeholder' => 'Ticket Value')); ?>
                                    </div>
                                    <div class="col-md-2 value">
                                        <label>Starting Ticket Number:</label>
                                        <?php echo $this->Form->input('start_ticket', array("id" => "startTickt", "class" => "form-control", 'label' => false, 'placeholder' => 'Starting Ticket Number')); ?>
                                    </div>
                                    <div class="col-md-2 value">
                                        <label>Ending Ticket Number:</label>
                                        <?php echo $this->Form->input('end_ticket', array("id" => "endTickt", "class" => "form-control", 'label' => false, 'placeholder' => 'Ending Ticket Number')); ?>
                                    </div>
                                </div>





                                <div class="row static-info">

                                    <div class="col-md-2 value">
                                        <label>Tickets Per Pack:</label>
                                        <?php echo $this->Form->input('tickets_pack', array("id" => "ticktPack", "class" => "form-control", 'label' => false, 'placeholder' => 'Tickets Per Pack')); ?>
                                    </div>
                                    <div class="col-md-2 value">
                                        <label>State</label>
                                        <select id="state" name="data[ImportedGames][state]" class="form-control">
                                            <option value=""> Select State</option>
                                            <option value="TX">TX</option>
                                            <option value="GA">GA</option>
                                            <option value="LA">LA</option>
                                            <option value="AZ">AZ</option>
                                            <option value="AR">AR</option>
                                            <option value="FL">FL</option>
                                            <option value="PA">PA</option>
                                            <option value="TN">TN</option>
                                            <option value="NY">NY</option>
                                            <option value="MO">MO</option>
                                            <option value="VA">VA</option>
                                            <option value="IL">IL</option>
                                            <option value="MA">MA</option>
                                            <option value="SD">SD</option>
                                            <option value="NC">NC</option>
                                            <option value="SC">SC</option>
                                            <option value="CO">CO</option>
                                            <option value="KS">KS</option>
                                            <option value="NJ">NJ</option>
                                            <option value="NE">NE</option>
                                            <option value="IA">IA</option>
                                            <option value="MI">MI</option>
                                            <option value="IN">IN</option>
                                            <option value="OK">OK</option>
                                            <option value="WI">WI</option>
                                            <option value="CA">CA</option>

                                        </select>
                                    </div>
                                    <div class="col-md-3 value">
                                        <br><br>
                                        <?php echo $this->Form->submit('Add Lottery Game', array('class' => 'btn btn-info')); ?>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                </div>

                                <script>

                                    $(document).ready(function () {
                                        //alert();
                                        $('#startTickt').change(function () {
                                            var st = $(this).val();
                                            var et = parseInt($('#endTickt').val()) + 1;
                                            var tp = parseInt(et) - parseInt(st);
                                            tp = Math.abs(tp);
                                            $('#ticktPack').val(tp);
                                        });
                                        $('#endTickt').change(function () {
                                            var et = parseInt($(this).val()) + 1;
                                            var st = $('#startTickt').val();
                                            var tp = parseInt(et) - parseInt(st);
                                            tp = Math.abs(tp);
                                            $('#ticktPack').val(tp);
                                        });
                                    });</script>




                                <div class="row static-info">
                                    <div class="col-md-5 name">

                                    </div>

<!--                                    <div class="col-md-7 value">
                                        <?php echo $this->Form->submit('Add Lottery Game', array('class' => 'btn btn-info')); ?>
                                        <?php echo $this->Form->end(); ?>
                                    </div>-->

                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .form-control{
        margin:10px;
    }</style>