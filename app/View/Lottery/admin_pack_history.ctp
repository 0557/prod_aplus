<?php // pr($salesInvoice); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Pack History</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Your store's lottery packs    </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				  <div class="col-md-2 col-xs-6">
                    <label>Date Confirmed</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="confirm" id="confirm" value=""  class="form-control" >
                  </div>

                   <div class="col-md-2 col-xs-6">
                    <label>Date Activated</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="activated" id="activated" value=""  class="form-control" >
                  </div>

                  <div class="col-md-2 col-xs-6">
                    <label>Date sold out</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="sold" id="sold" value=""  class="form-control" >
                  </div>
                  <div class="col-md-2 col-xs-6">
                    <label>Date settled</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="settled" id="settled" value=""  class="form-control" >
                  </div>
                  <div class="col-md-2 col-xs-6">
                    <label>Date returned</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="returned" id="returned" value=""  class="form-control" >
                  </div>
                  <!-- input 1 ends-->
                  <!-- input 2-->
                

                  <!-- input 2 ends-->
                </div>
              <!-- row one ends -->
              <!--row for or -->
             
              <!-- row for or ends -->
              <!-- row two start -->
			   <div class="row"><br/>
				</div>
			       <div class="row">       <div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<select name="game_no" id="game_no" class="form-control" >
				<option value="" selected></option>
				<?php /*foreach($games as $games){
					echo '<option value="'.$games['ImportedGames']['game_no'].'">'.$games['ImportedGames']['game_no'].'</option>';
				} */?>
				</select>
              </div>
              <div class="col-xs-6 col-md-2">
                <label>Game Name</label>
                <input type="text" name="gamename"  class="form-control"  id="pack_no" value="" >
              </div>
			     <div class="col-xs-6 col-md-2">
                <label>Pack number</label>
                <input type="text" name="pack_no"  class="form-control"  id="pack_no" value="" >
              </div>
				</div>
				</div>
				 <div class="row"><br/>
				</div>
              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Search</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>
        </div>
   

    </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	//$('#sold').Zebra_DatePicker();
	//$('#settled').Zebra_DatePicker();
	//$('#returned').Zebra_DatePicker();
	//$('#confirm').Zebra_DatePicker();
	//$('#activated').Zebra_DatePicker();			
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sold" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$( "#settled" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$( "#returned" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$( "#confirm" ).datepicker({ dateFormat: 'dd-mm-yy' });
	$( "#activated" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  </script>