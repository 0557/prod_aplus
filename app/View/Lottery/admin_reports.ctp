<?php // pr($salesInvoice);       ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >  <div class="content-new">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="page-title">Lottery Reports</h3>
                </div>
            </div>
        </div>
        <!-- top header ends -->
        <!-- submit form -->
        <div class="content-new">
            <div class="row">
                <div class="col-xs-12">
                    <div class="lottery_setting clearfix">
                        <!-- hedaing title -->
                        <div class="confirm_heading clearfix">
                            <h5><strong>Please enter date range for this report  </strong></h5>
                        </div>
                        <!-- heading title ends-->
                        <!-- form starts --><?php //echo Router::url('/')  ?>
                        <?php echo $this->Form->create('RubyDailyreporting', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                        <div class="confim_form">
                            <!-- row one -->
                            <div class="row">
                                <!-- input 1-->
                                <div class="col-md-2 col-xs-6">
                                    <label>Report date</label>
                                    <!-- important for developer please place your clander code on this input
                                    <input type="text" name="report" id="Date2" value=""  class="form-control" >-->

                                    <?php //echo $this->Form->input('reporting_date', array('id' => 'Date', 'class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'value' => date('d-m-Y'))); ?> 
                                    <?php
                                    include('multidatepicker.ctp');
                                    ?>

                                </div>
                                <!-- input 1 ends-->
                                <div class="col-xs-6 col-md-2">
                                    <label>&nbsp;</label>
                                    <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                </div>
                                <div class="col-xs-2  col-md-2">
                                    <label>&nbsp;</label>
                                    <a style=" margin-top:24px;" href="<?php echo Router::url('/'); ?>admin/lottery/reset" class="btn btn-warning ">Reset</a> 
                                </div>
                            </div>
                            <!-- row one ends -->
                            <!--row for or -->

                            <!-- row for or ends -->
                            <!-- row two start -->

                            <!-- row two ends -->
                        </div>
                        <?php echo $this->form->end(); ?>
                        <!-- form ends -->
                    </div>
                </div>

            </div>
            <h1></h1>
            <div id="tab_1_5" class="tab-pane1">

                <div class="table-responsive">
                   
                    <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                            <tr>

                                <th><?php echo __($this->Paginator->sort('reporting_date', 'Date')); ?>  </th>
                                <th><?php echo $this->Paginator->sort('Net_Online_Sales', 'Online Sales'); ?></th>
                                <th><?php echo $this->Paginator->sort('Net_online_cashes', 'Online Cashes'); ?></th>
                                <th><?php echo $this->Paginator->sort('Scratch_off_cashes', 'Scratch Off Cashes'); ?></th>
                                <th><?php echo $this->Paginator->sort('Settlement', 'Scratch Off Settlement'); ?></th>
                                <th><?php echo $this->Paginator->sort('Adjustment', 'Ajustment'); ?></th>
                                <th><?php echo $this->Paginator->sort('OScratch_off_credit', 'Scratch Off Credit'); ?></th>
                                <th><?php echo $this->Paginator->sort('Online_credit', 'Online Credit'); ?></th>
                                <th><?php echo $this->Paginator->sort('Lotto_Commission', 'Commission'); ?></th>
                                <th><?php echo $this->Paginator->sort('Lotto_Balance', 'Daily Balance'); ?></th>


                            </tr>
                        </thead>
                        <tbody> <?php pr($reportings);?>
                            <?php
                            if (isset($reportings) && !empty($reportings)) {

                                foreach ($reportings as $reporting):
                                    ?>
                                    <tr>

                                        <td><?php echo h($reporting['RubyDailyreporting']['reporting_date']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Net_Online_Sales']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Net_online_cashes']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Scratch_off_cashes']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Settlement']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Adjustment']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['OScratch_off_credit']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Online_credit']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Lotto_Commission']); ?></td>
                                        <td><?php echo h($reporting['RubyDailyreporting']['Lotto_Balance']); ?></td>

                                    </tr>
                                    <?php
                                endforeach;
                            }
                            else {
                                ?>
                                <tr>
                                    <td colspan="10">No results found.</td>
                                </tr>    
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>


                </div>
                <div class="margin-top-20">
                    <ul class="pagination">
                        <li>
                            <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                        </li>
                        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                        <li> 
                            <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                        </li>
                    </ul>
                </div>      
            </div>
        </div>


    </div>
</div>




<script type="text/javascript" charset="utf-8">
//    $(document).ready(function () {
//        //$('#Date').Zebra_DatePicker();
//        $('#game_no').change(function () {
//            var game_no = $('#game_no').val();
//            $.ajax({
//                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
//                type: 'post',
//                data: {game_no: game_no},
//                success: function (data) {
//                    $('#pack').html(data);
//                }
//            });
//        });
//
//    });
</script>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        //$("#Date").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>-->