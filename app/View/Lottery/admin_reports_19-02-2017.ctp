<?php // pr($salesInvoice); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Reports</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Please enter date range for this report  </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				  <div class="col-md-2 col-xs-6">
                    <label>Report date</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="report" id="Date" value=""  class="form-control" >
                  </div>

                   <div class="col-md-2 col-xs-6">
                    <label>Report type</label>
                    <!-- important for developer please place your clander code on this input-->
                    <select name="rtype" class="form-control" >
					<option value="balance">Lottery/Lotto Balance</option>
					<option value="profit">Lottery/Lotto Profit</option>
					<option></option>
					</select>
                  </div>

                  <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
              </div>
                  <!-- input 2 ends-->
                </div>
         
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>
        </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {
	//$('#Date').Zebra_DatePicker();
		$('#game_no').change(function(){			
			var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
				$('#pack').html(data);
                }
            });		
		});
		
    });	
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#Date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  </script>