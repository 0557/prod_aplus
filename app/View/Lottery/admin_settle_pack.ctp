<?php // pr($salesInvoice); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Settle Pack  <a href="" class="btn default  finishbtn" style="float:right">View past settlements</a> <a href="" class="btn default  finishbtn" style="float:right; margin-right:20px">Settle sold out packs</a></h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->

    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Enter lottery pack settlements    </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
                  <div class="col-md-2 col-xs-6">
                    <label>Date</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="packdate" id="Date" value="<?php echo date('d-m-Y'); ?>"  class="form-control" >
                  </div>
                  <!-- input 1 ends-->
                  <!-- input 2-->
                  <div class="col-md-2 col-xs-6">
                    <label>Scan/Type ticket code</label>
                    <input type="text" name="scan_ticket_code" id="scan_ticket_code" value=""  class="form-control" >
                  </div>
								    <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                </div>
       <div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<select name="game_no" id="game_no" class="form-control" required>
				<option value="" selected></option>
				<?php /*foreach($games as $games){
					echo '<option value="'.$games['ImportedGames']['game_no'].'">'.$games['ImportedGames']['game_no'].'</option>';
				} */?>
				</select>
              </div>
              <div class="col-xs-6 col-md-2">
                <label>Pack number</label>
                <input type="text" name="pack_no"  class="form-control"  id="pack_no" value="" required>
              </div>
                  <!-- input 2 ends-->
                </div>
              <!-- row one ends -->
              <!--row for or -->
             
              <!-- row for or ends -->
              <!-- row two start -->
			       <div class="row"><br/>
				</div>
				</div>
              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Settle This Pack</button>
              </div>
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
               <a href="" class="btn default  finishbtn">Finish</a>

				
              </div>
              </div>
              <!-- row two ends -->
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>

     
	  <div id="tab_1_5" class="tab-pane1">
	  <div style="text-align:center">
	  <h1>&nbsp;</h1>
<h1>No packs have been settled for this date yet.</h2>
You can start by scanning the pack's lottery code or manually entering in the game number and pack number.
</div>
</div>

    </div>
    </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	//$('#Date').Zebra_DatePicker();
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });

</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#Date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  </script>