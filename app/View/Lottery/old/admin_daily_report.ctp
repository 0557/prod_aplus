<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
            <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery ticket sales </h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
          
				<div class="row">
						  <div class="col-md-2 col-xs-6">
                    <label>Scan/Type ticket code</label>
                    <input type="text" name="scan_ticket_code" id="scan_ticket_code" value=""  class="form-control" >
</div>
  <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                </div>
				<div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<select name="game_no" id="game_no" class="form-control" required>
				<option value="" selected></option>
				<?php foreach($games as $games){
					echo '<option value="'.$games['GamePacks']['game_no'].'">'.$games['GamePacks']['game_no'].'</option>';
				} ?>
				</select>
              </div>
                    
              <div class="col-xs-6 col-md-2">
                <label>Pack number</label>
               <select name="pack_no" id="pack" class="form-control"  required>
				<option value="" selected></option>
				</select>
              </div>
			    <div class="col-xs-6 col-md-2">
                <label>Bin number</label>
               <select name="bin_no" id="" class="form-control" >
				<option value="0" selected></option>
				<?php for($b=1;$b<=15; $b++){
					echo '<option value="'.$b.'">'.$b.'</option>';
				}?>
				</select>
              </div>
			     
                  <!-- input 2 ends-->
                </div>
              <!-- row one ends -->
              <!--row for or -->
           
              <!-- row for or ends -->
              <!-- row two start -->
			  			    <div class="row"><br/>
				</div>

              <div class="row">
   <div class="col-md-2 col-xs-6">
                    <label>Available Tk# on sale</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="prtkt" id="availa"  class="form-control" readonly>
                  </div>

				                    <div class="col-md-2 col-xs-6">
                    <label>Today Tkt#</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="totkt"   class="form-control" >
                  </div>

              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid" style="margin-top: 24px;">Enter Daily Sales</button>
              </div>
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
               <a href="<?php echo Router::url('/').'admin/lottery/finish_daily_report';?>" class="btn default  finishbtn" style="margin-top: 24px;">Finish Daily Sales</a>

				
              </div>
              </div>
              <!-- row two ends -->
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>
		
      </div>
	  <h1></h1>
	  <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
						
					
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                      
                                     <th><?php echo __($this->Paginator->sort('Bin Number')); ?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Game Number')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('Pack Number'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Game'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('Ticket Value'); ?></th>
                                           
                                    
                                            <th><?php echo $this->Paginator->sort('Start Ticket#'); ?></th>
                                            <th><?php echo $this->Paginator->sort('End Ticket#'); ?></th>
											        <th><?php echo $this->Paginator->sort('Previous Ticket#'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Current Ticket#'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Status'); ?></th>
                                             
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php   


							   if (isset($readings) && !empty($readings)) { 
						$ms = "'Are you sure want delete it?'";
						

                                             foreach ($readings as $data) { 
                                               echo '<tr>
                                                      
                                                      <td>'.$data["e"]["bin_no"].'</td>
                                                      <td>'.$data["e"]["game_no"].'</td>
                                                    <td>'.$data["e"]["pack_no"].'&nbsp;</td>
                                                    <td>'.$data["u"]["gamename"].'&nbsp;</td>
                                                    <td>'.$data["u"]["ticket_value"].'&nbsp;</td>
                                                    <td>'.$data["u"]["start_tkt"].'&nbsp;</td>
                                                    <td>'.$data["u"]["end_tkt"].'&nbsp;</td>
                                                  
                                                    <td>'.$data["e"]["prev"].'&nbsp;</td>
                                                    <td>'.$data["e"]["today"].'&nbsp;</td>
                                                    <td>'.$data["e"]["status"].'&nbsp;</td>
											    
                                                </tr>';
												//echo  '<td> <a href="'.Router::url('/').'admin/lottery/delete_apack/'.$data["GamePacks"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>'
											}
                                             
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
    </div>
   

    </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#Date').Zebra_DatePicker();
	
	
			
	
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacksactive',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
					
                }
            });		
		});
		
				$('#scan_ticket_code').change(function(){
			
			var scan = $('#scan_ticket_code').val();
		
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getgamedata',
                type: 'post',
                data: { scan: scan},
                success:function(data){
					var json = jQuery.parseJSON(data)
					if(json['gno']!=null){
					$('#game_no').html('<option value="'+json['gno']+'">'+json['gno']+'</option>');
						
					}else{
						location.reload();
					}
					if(json['pack']!=null){
					
					$('#pack').html('<option value="'+json['pack']+'">'+json['pack']+'</option>');
					
					$('#availa').val(json['prev']);
					}else{
												location.reload();

					}					

                }
            });		
		});
		
		$('#pack').change(function(){
		
	var gno = $('#game_no').val();

			var pack = $('#pack').val();
				$.ajax({
					url: '<?php echo Router::url('/') ?>admin/lottery/gettcikets',
					type: 'post',
					data: { game_no: gno,pack_no: pack},
					success:function(data){
						$('#availa').val(data);
					}
            });
			
			
		});
		
    });
	

</script>