<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
            <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->

		 <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                   <h3>     <i class="fa fa-reorder"></i> Edit imported game</h3>
                    </div>

                </div>
            </div>
        </div>
		</div>
<div class="form-body">
            <div class="row">

                <div class="col-md-9 col-sm-12">
                    <div class="portlet yellow box">

                       
                        <div class="portlet-body extra_tt">
                      <form method="post" action="">    

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Game No

                                </div>
                                <div class="col-md-7 value">
                            <input type="text" name="game_no" value="<?php echo $editgame["ImportedGames"]["game_no"]; ?>"  class="form-control" readonly/>
                            <input type="hidden" name="id" value="<?php echo $editgame["ImportedGames"]["id"]; ?>"  class="form-control" readonly/>
                                </div>
                            </div>
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Game Name

                                </div>
                                <div class="col-md-7 value">
                            <input type="text" name="game_name" value="<?php echo $editgame["ImportedGames"]["game_name"]; ?>"  class="form-control"/>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Value
                                </div>
                                <div class="col-md-7 value">
  <input type="text" name="value" value="<?php echo $editgame["ImportedGames"]["value"]; ?>"  class="form-control"/>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Tickets Pack:
                                </div>
                                <div class="col-md-7 value">
                                    <input type="text" name="tickets_pack" value="<?php echo $editgame["ImportedGames"]["tickets_pack"]; ?>"  class="form-control"  />
                                </div>

                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Start Ticket:
                                </div>
                                <div class="col-md-7 value">
                                      <input type="text" name="start_ticket" value="<?php echo $editgame["ImportedGames"]["start_ticket"]; ?>" class="form-control"  />
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    End Ticket:
                                </div>
                             
								<div class="col-md-7 value">
                                      <input type="text" name="end_ticket" value="<?php echo $editgame["ImportedGames"]["end_ticket"]; ?>" class="form-control"  />
                                </div>
                         
                        </div>
						  <div class="row static-info">
                                <div class="col-md-5 name">
                                    State
                                </div>
                                <div class="col-md-7 value">
								 <select id="state" name="state" onchange="getgamesbystate(this);" class="form-control">
					 <option value=""><?php echo $editgame["ImportedGames"]["state"]; ?></option>
					 <option value="TX">TX</option>
					 <option value="GA">GA</option>
					 <option value="LA">LA</option>
					 <option value="AZ">AZ</option>
					 <option value="AR">AR</option>
					 <option value="FL">FL</option>
					 <option value="PA">PA</option>
					 <option value="TN">TN</option>
					 <option value="NY">NY</option>
					 <option value="MO">MO</option>
					 <option value="VA">VA</option>
					 <option value="IL">IL</option>
					 <option value="MA">MA</option>
					 <option value="SD">SD</option>
					 <option value="NC">NC</option>
					 <option value="SC">SC</option>
					 <option value="CO">CO</option>
					 <option value="KS">KS</option>
					 <option value="NJ">NJ</option>
					 <option value="NE">NE</option>
					 <option value="IA">IA</option>
					 <option value="MI">MI</option>
					 <option value="IN">IN</option>
					 <option value="OK">OK</option>
					 <option value="WI">WI</option>
					 <option value="CA">CA</option>
					 
				 </select>
                                           </div>
                            </div>
							
							 
						<div class="row static-info">
                                <div class="col-md-5 name">
                                  
                                </div>
                             
								<div class="col-md-7 value">
                                    <button type="submit" name ="submit" class="btn blue">Update</button>
                                </div>
                         
                        </div>
						</form>
                    </div>
                </div>
               
            </div>
            <!-- END FORM-->
        </div>
	</div>
</div>
<style>
.form-control{
	margin:10px;
}</style>