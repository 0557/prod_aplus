<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
            <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                 Import lottery games 
				 <span class="btn green fileinput-button" id="postimport">
                       <?php echo $this->Html->link("<i class='fa fa-plus'></i> New Game", array( 'action' => 'new_game'), array('escape' => false)); ?>

						</span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                     <th><?php echo __($this->Paginator->sort('Game No')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('Game Name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Value'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Tickets/Pack'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('StartTicket#'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('EndTicket#'); ?></th>
                                            <th><?php echo $this->Paginator->sort('State'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Action'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($Games) && !empty($Games)) { 
		$ms = "'Are you sure want delete it?'";
                                             foreach ($Games as $data) { 
                                               echo '<tr>
                                                      
                                                      <td>'.$data["ImportedGames"]["game_no"].'</td>
                                                    <td>'.$data["ImportedGames"]["game_name"].'&nbsp;</td>
                                                    <td>'.$data["ImportedGames"]["value"].'&nbsp;</td>
                                                    <td>'.$data["ImportedGames"]["tickets_pack"].'&nbsp;</td>
                                                    <td>'.$data["ImportedGames"]["start_ticket"].'&nbsp;</td>
                                                    <td>'.$data["ImportedGames"]["end_ticket"].'&nbsp;</td>
                                                    <td>'.$data["ImportedGames"]["state"].'&nbsp;</td>
													<td>  <button class="btn default btn-xs red-stripe">Ready to sale</button> 
													<a href="'.Router::url('/').'admin/lottery/edit_game/'.$data["ImportedGames"]["id"].'" class="btn default btn-xs red-stripe">Edit</a>
												';
												if($data["ImportedGames"]["games"]==""){
												echo '	<a href="'.Router::url('/').'admin/lottery/delete_game/0/'.$data["ImportedGames"]["id"].'" class="btn default btn-xs red-stripe" onClick="return confirm('.$ms.');">Delete</a></td>
                                                </tr>';
												}else{
												echo '	<a href="'.Router::url('/').'admin/lottery/delete_game/'.$data["ImportedGames"]["games"].'/'.$data["ImportedGames"]["id"].'"  onClick="return confirm('.$ms.');" class="btn default btn-xs red-stripe" >Delete</a> </td>
                                               
												</tr>';
												
											}
                                             } 
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>

<script>
function getgamesbystate(elem){

var state = elem.value;
  $.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getgames',
                type: 'post',
                data: { ids: state},
                success:function(data){
					$('#rwods').html(data);
				
                }
            });
}

</script>