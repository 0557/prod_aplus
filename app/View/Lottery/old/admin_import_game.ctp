<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
            <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                 Import lottery games 
				 <select id="state" onchange="getgamesbystate(this);" class="getgame">
				 <option value="">Select games by state</option>
				 <option value="TX">TX</option>
				 <option value="GA">GA</option>
				 <option value="LA">LA</option>
				 <option value="AZ">AZ</option>
				 <option value="AR">AR</option>
				 <option value="FL">FL</option>
				 <option value="PA">PA</option>
				 <option value="TN">TN</option>
				 <option value="NY">NY</option>
				 <option value="MO">MO</option>
				 <option value="VA">VA</option>
				 <option value="IL">IL</option>
				 <option value="MA">MA</option>
				 <option value="SD">SD</option>
				 <option value="NC">NC</option>
				 <option value="SC">SC</option>
				 <option value="CO">CO</option>
				 <option value="KS">KS</option>
				 <option value="NJ">NJ</option>
				 <option value="NE">NE</option>
				 <option value="IA">IA</option>
				 <option value="MI">MI</option>
				 <option value="IN">IN</option>
				 <option value="OK">OK</option>
				 <option value="WI">WI</option>
				 <option value="CA">CA</option>
				 
				 </select><span class="btn green fileinput-button" id="postimport">
                        Import Selected         
						</span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                             <th> <input type="checkbox" name="checkboxlist" id="selecctall" />  </th>
                                     <th><?php echo __($this->Paginator->sort('Game No')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('Game Name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Value'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Tickets/Pack'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('StartTicket#'); ?></th>
                                           
                                            <th><?php echo $this->Paginator->sort('EndTicket#'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody id="rwods">
                                     
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>

<script>
$(document).ready(function() {

    $('#selecctall').click(function(event) {  
	 $("input:checkbox").prop('checked', $(this).prop("checked"));
       
    });
   
    $('#postimport').click(function(){
	var corpo = document.getElementById('corporationName').value;
	var store = document.getElementById('storeName').value;
	var state = document.getElementById('state').value;
	if(corpo == ""){
	alert('please select corporation and store');
	}else if(store == ""){
	alert('please select store before import games');
	}else if(state == ""){
	alert('please state for import games');
	}else if(!$('input[type="checkbox"]').is(':checked')){
      alert("Please check at least one game.");
     
	}else{
	   var data = new Array();
			$("input[name='import']:checked").each(function(i) {
				data.push($(this).val());
			});


			    $.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/import_game',
                type: 'post',
                data: { ids: data, corporation: corpo, store: store, state: state },
                success:function(data){
					alert(data);
					window.location='<?php echo Router::url('/') ?>admin/lottery/games';
                }
            });
	}
	
  
         

			
        });
		
});

function getgamesbystate(elem){

var state = elem.value;
  $.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getgamesbystate',
                type: 'post',
                data: { ids: state},
                success:function(data){
					$('#rwods').html(data);
                }
            });
}

</script>