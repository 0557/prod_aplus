<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
             <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Lotto Settlements</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
    <div class="content-new" style="text-align:center">
	<h2>No lottery/lotto settlements have been made yet.</h2>
	 <a href="" class="btn default btn-xs red-stripe">Create settlement entery</a>           
	 <a href="" class="btn default btn-xs red-stripe">Configure lottery/lotto</a>          
  </div>
   

    </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#Date').Zebra_DatePicker();
	
	
			
	
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });
	


</script>