<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
             <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Pack History</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
   <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Your store's lottery packs    </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				  <div class="col-md-2 col-xs-6">
                    <label>Date Confirmed</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="confirm" id="confirm" value=""  class="form-control" >
                  </div>

                   <div class="col-md-2 col-xs-6">
                    <label>Date Activated</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="activated" id="activated" value=""  class="form-control" >
                  </div>

                  <div class="col-md-2 col-xs-6">
                    <label>Date sold out</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="sold" id="sold" value=""  class="form-control" >
                  </div>
                  <div class="col-md-2 col-xs-6">
                    <label>Date settled</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="settled" id="settled" value=""  class="form-control" >
                  </div>
                  <div class="col-md-2 col-xs-6">
                    <label>Date returned</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="returned" id="returned" value=""  class="form-control" >
                  </div>
                  <!-- input 1 ends-->
                  <!-- input 2-->
                

                  <!-- input 2 ends-->
                </div>
              <!-- row one ends -->
              <!--row for or -->
             
              <!-- row for or ends -->
              <!-- row two start -->
			   <div class="row"><br/>
				</div>
			       <div class="row">       <div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<select name="game_no" id="game_no" class="form-control" >
				<option value="" selected></option>
				<?php /*foreach($games as $games){
					echo '<option value="'.$games['ImportedGames']['game_no'].'">'.$games['ImportedGames']['game_no'].'</option>';
				} */?>
				</select>
              </div>
              <div class="col-xs-6 col-md-2">
                <label>Game Name</label>
                <input type="text" name="gamename"  class="form-control"  id="pack_no" value="" >
              </div>
			     <div class="col-xs-6 col-md-2">
                <label>Pack number</label>
                <input type="text" name="pack_no"  class="form-control"  id="pack_no" value="" >
              </div>
				</div>
				</div>
				 <div class="row"><br/>
				</div>
              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Search</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>
   

    </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#sold').Zebra_DatePicker();
	$('#settled').Zebra_DatePicker();
	$('#returned').Zebra_DatePicker();
	$('#confirm').Zebra_DatePicker();
	$('#activated').Zebra_DatePicker();
	
	
			
	
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });
	


</script>