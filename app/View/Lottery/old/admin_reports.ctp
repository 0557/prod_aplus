<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
             <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>
<div class="page-content">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery Reports</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Please enter date range for this report  </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
				  <div class="col-md-2 col-xs-6">
                    <label>Report date</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="report" id="Date" value=""  class="form-control" >
                  </div>

                   <div class="col-md-2 col-xs-6">
                    <label>Report type</label>
                    <!-- important for developer please place your clander code on this input-->
                    <select name="rtype" class="form-control" >
					<option value="balance">Lottery/Lotto Balance</option>
					<option value="profit">Lottery/Lotto Profit</option>
					<option></option>
					</select>
                  </div>

                  <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
              </div>
                  <!-- input 2 ends-->
                </div>
         
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#Date').Zebra_DatePicker();
	
	
			
	
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });
	


</script>