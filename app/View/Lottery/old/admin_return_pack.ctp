<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
             <li class="<?php echo (isset($import_game)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>Import Games", array('controller' => 'lottery', 'action' => 'game_import'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($confirmpack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>Confirm Pack", array( 'action' => 'confirm_pack'), array('escape' => false)); ?>
                </li>
                <li class="<?php echo (isset($activatepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Activate Pack", array( 'action' => 'activate_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($dailyreading)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Daily Reading", array('action' => 'daily_reading'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($listofgames)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Games ", array('action' => 'games'), array('escape' => false)); ?>
                </li>
				 <li class="<?php echo (isset($settlepack)) ? 'active' : ''; ?>">
                    <?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Settle Pack", array( 'action' => 'settle_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($returnpack)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-shopping-cart'></i>  Return Pack", array('action' => 'return_pack'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($packhistory)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Pack History ", array('action' => 'pack_history'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($lottosettlements)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>   Lotto Settlements ", array('action' => 'lotto_settlements'), array('escape' => false)); ?>
                </li>
				<li class="<?php echo (isset($reports)) ? 'active' : ''; ?>">
                <?php echo $this->Html->link("<i class='fa fa-gamepad'></i>  Reports ", array('action' => 'reports'), array('escape' => false)); ?>
                </li>
        </ul>
    </div>
</div>

<div class="page-content">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Lottery  Return Pack  <a href="" class="btn default  finishbtn" style="float:right">View past returns</a></h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->

    <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <div class="lottery_setting clearfix">
            <!-- hedaing title -->
            <div class="confirm_heading clearfix">
              <h5><strong>Enter returned lottery packs    </strong> </h5>
            </div>
            <!-- heading title ends-->
            <!-- form starts -->
            <form method="post" action"">
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
                  <!-- input 1-->
                  <div class="col-md-2 col-xs-6">
                    <label>Date</label>
                    <!-- important for developer please place your clander code on this input-->
                    <input type="text" name="packdate" id="Date" value="<?php echo date('d-m-Y'); ?>"  class="form-control" >
                  </div>
                  <!-- input 1 ends-->
                  <!-- input 2-->
                  <div class="col-md-2 col-xs-6">
                    <label>Scan/Type ticket code</label>
                    <input type="text" name="scan_ticket_code" id="scan_ticket_code" value=""  class="form-control" >
                  </div>
								    <div class="col-xs-1 line" style="text-align:center">                <strong> Or</strong>
                </div>
       <div class="col-xs-6 col-md-2">
                <label>Game number</label>
				<select name="game_no" id="game_no" class="form-control" required>
				<option value="" selected></option>
				<?php /*foreach($games as $games){
					echo '<option value="'.$games['ImportedGames']['game_no'].'">'.$games['ImportedGames']['game_no'].'</option>';
				} */?>
				</select>
              </div>
              <div class="col-xs-6 col-md-2">
                <label>Pack number</label>
                <input type="text" name="pack_no"  class="form-control"  id="pack_no" value="" required>
              </div>
                  <!-- input 2 ends-->
                </div>
              <!-- row one ends -->
              <!--row for or -->
             
              <!-- row for or ends -->
              <!-- row two start -->
			       <div class="row"><br/>
				</div>
				</div>
              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" name ="submit" class="btn default updatebtn" id="confirmpackid">Return This Pack</button>
              </div>
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
               <a href="" class="btn default  finishbtn">Finish</a>

				
              </div>
              </div>
              <!-- row two ends -->
              </div>
            </form>
            <!-- form ends -->
          </div>
        </div>

     
	  <div id="tab_1_5" class="tab-pane1">
	  <div style="text-align:center">
	  <h1>&nbsp;</h1>
<h1>No packs have been closed out for this date yet.</h2>
You can start by scanning the pack's lottery code or manually entering in the game number and pack number.
</div>
</div>

    </div>
	


<script type="text/javascript" charset="utf-8">
   $(document).ready(function () {

	$('#Date').Zebra_DatePicker();
	
	
			
	
		
		$('#game_no').change(function(){
			
					var game_no = $('#game_no').val();
			$.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/getpacks',
                type: 'post',
                data: { game_no: game_no},
                success:function(data){
					
					$('#pack').html(data);
                }
            });		
		});
		
    });
	


</script>