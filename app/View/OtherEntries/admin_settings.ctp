<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_0" data-toggle="tab">
									Expense categories
								</a>
							</li>
							<li>
								<a href="#tab_1" data-toggle="tab">
									Income Sources
								</a>
							</li>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Withdrawal Entities
								</a>
							</li>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Other Accounts
								</a>
							</li>
							<li>
								<a href="#tab_4" data-toggle="tab">
									 Local Accounts
								</a>
							</li>
							
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box blue">
									<div class="portlet-title  col-md-12">
										<div class="caption">
											<i class="fa fa-reorder"></i> Your store's expenses
											
										</div>
										<div class="tools">
										<a href="#basic" data-toggle="modal" class="" style="color:#fff">
										<i class="fa fa-plus fa-lg"></i> 
									</a>
											<div class="modal fade" id="basic">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add miscellaneous expenses</h4>
										</div>
										<div class="modal-body">
											
											
											
											<label>New Category</label>
											<?php echo $this->Form->input('category', array('class' => 'form-control', 'required' => 'true','id'=>'categoryname' ,'label' => false, 'placeholder' => 'Enter category ')); ?>
											
											
										</div>
										<div class="modal-footer">
											<button type="button" class="btn primary" data-dismiss="modal">Cancel</button>
											<button type="button" id="tab1button" class="btn success">Add expense category</button>
										</div>
										
										
										
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
											
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
									
                     
					   <div class="row">
		  <div class="col-md-12">
		 
	 <div class="col-md-3">
		  <label>Category name</label>
		<?php echo $this->Form->input('category', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'options'=>$catelist,'empty'=>'','id' =>'categoryfilter','placeholder'=>'Category name')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		 <label>Status</label>
		<?php echo $this->Form->input('status', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'statuscat','placeholder'=>'Status')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('Search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false,'id'=>'searchcat', 'required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
		
										<!-- END FORM-->
										<!-- END FORM-->
										<hr/>
										<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Category name'); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody id="catde">
						
						
						<?php if(isset($allcats) && !empty($allcats)){
						foreach($allcats as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherCategories']['category'].'</td>';
						echo '<td>'.$catn['OtherCategories']['status'].'</td>';
					echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherCategories']['id'];?>,'OtherCategories','catde');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
					
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} ?>
						</tbody>
						</table>
						
									</div>
									</div>
								</div>
						
								
							</div>
							<div class="tab-pane" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title col-md-12">
										<div class="caption">
											<i class="fa fa-reorder"></i>Your store's income
										</div>
										<div class="tools">
											<a href="#source" data-toggle="modal" class="" style="color:#fff">
										<i class="fa fa-plus fa-lg"></i> 
									</a>
									
											<div class="modal fade" id="source">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add income</h4>
										</div>
										<div class="modal-body">
											<label>New Source</label>
											<input name="source" id="sourcena" placeholder="New Source" class="form-control"/>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn primary" data-dismiss="modal">Cancel</button>
											<button type="button" id="sourcebut" class="btn success">Add income source</button>
										</div>
									</div>
									</div>
									</div>
									<!-- /.modal-content -->
								</div>
										</div>
									
									<div class="portlet-body form">
										
					   <div class="row">
		  <div class="col-md-12">
	 <div class="col-md-3">
		  <label>Source</label>
		<?php echo $this->Form->input('plu', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'sourcefil','placeholder'=>'Source','options'=>$sourcelistitems,'empty'=>'')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		 <label>Status</label>
		<?php echo $this->Form->input('status', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'statusinc','placeholder'=>'Status')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('Search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false,'id'=>'incsearch', 'required' => false,)); ?>
		
	
		<br/>
		</div>
		</div>
		</div>	
		<?php echo $this->form->end(); ?>
		
		<hr/>
										<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Source'); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody id="sourcedeta">
						<?php if(isset($sourcelist) && !empty($sourcelist)){
						foreach($sourcelist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherSources']['source'].'</td>';
						echo '<td>'.$catn['OtherSources']['status'].'</td>';
							echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherSources']['id'];?>,'OtherSources','sourcedeta');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						 
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} ?>
						</tbody>
						</table>
						
									</div>
									</div>
								</div>
							</div>
							<div class="tab-pane " id="tab_2">
								<div class="portlet box blue">
									<div class="portlet-title col-md-12">
										<div class="caption">
											<i class="fa fa-reorder"></i>Your store's profit with-drawer
										</div>
										<div class="tools">
												<a href="#drawer" data-toggle="modal" class="" style="color:#fff">
										<i class="fa fa-plus fa-lg"></i> 
									</a>
									
											<div class="modal fade" id="drawer">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add profit with-drawer</h4>
										</div>
										<div class="modal-body">
											<label>With-drawer</label>
											<input name="drawer" id="drawername" placeholder="With-drawer" class="form-control"/>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn primary" data-dismiss="modal">Cancel</button>
											<button type="button" id="newwithd" class="btn success">Add profit with-drawer</button>
										</div>
									</div>
									</div>
									</div>
										</div>
									</div>
									<div class="portlet-body form">
										
					   <div class="row">
		  <div class="col-md-12">
	 <div class="col-md-3">
		  <label>With-drawer</label>
		<?php echo $this->Form->input('with-drawer', array('class' => 'form-control','type'=>'select','options'=>$withdrawlistitems,'empty'=>'', 'label' => false, 'required' => true, 'id' =>'withdrawfilter','placeholder'=>'With Drawer')); ?>
		<br/>
		</div>
		 
		<div class="col-md-3">
		 <label>Status</label>
		<?php echo $this->Form->input('status', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'withstatus','placeholder'=>'Status')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('Search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false,'id'=>'seachdrawer', 'required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
	
		<hr/>
										<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('With-drawer '); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody id="drawerdata">
							<?php if(isset($withdrawlist) && !empty($withdrawlist)){
						foreach($withdrawlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherWithdraw']['withdraw'].'</td>';
						echo '<td>'.$catn['OtherWithdraw']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherWithdraw']['id'];?>,'OtherWithdraw','drawerdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} ?>
						</tbody>
						</table>
						
									</div>
									
									
									</div>
								</div>
							</div>
							<div class="tab-pane " id="tab_3">
								<div class="portlet box blue">
									<div class="portlet-title col-md-12">
										<div class="caption">
											<i class="fa fa-reorder"></i>Your store's general account
										</div>
										<div class="tools">
										
												<a href="#Account" data-toggle="modal" class="" style="color:#fff">
										<i class="fa fa-plus fa-lg"></i> 
									</a>
									
											<div class="modal fade" id="Account" >
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add general accounts</h4>
										</div>
										<div class="modal-body">
											<label>Account name</label>
											<input name="Account" id="Accountname" placeholder="Account name" class="form-control"/>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn primary" data-dismiss="modal">Cancel</button>
											<button type="button" id="loanaccount" class="btn success">Add  loan account</button>
										</div>
									</div>
									</div>
									</div>
									
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										
                     
					   <div class="row">
		  <div class="col-md-12">
	 <div class="col-md-3">
		  <label>Account</label>
		<?php echo $this->Form->input('account', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => true, 'id' =>'accountsearchname','options'=>$accountlistitems,'empty'=>'')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		 <label>Status</label>
		<?php echo $this->Form->input('status', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'accountstatus','placeholder'=>'Status')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('Search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'id'=>'accountsearch','required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
	
										<!-- END FORM-->
										<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Account'); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody id="accountdata">
						<?php if(isset($accountlist) && !empty($accountlist)){
						foreach($accountlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherAccount']['account'].'</td>';
						echo '<td>'.$catn['OtherAccount']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherAccount']['id'];?>,'OtherAccount','accountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
						
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="3">No records found</td></tr>';
						} ?>
						</tbody>
						</table>
						
									</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_4">
								<div class="portlet box blue">
									<div class="portlet-title col-md-12">
										<div class="caption">
											<i class="fa fa-reorder"></i>Your store's charge account list
										</div>
										<div class="tools">
											
												<a href="#charge" data-toggle="modal" class="" style="color:#fff">
										<i class="fa fa-plus fa-lg"></i> 
									</a>
									
											<div class="modal fade" id="charge" >
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add general accounts</h4>
										</div>
										<div class="modal-body">
											<label>Account name</label>
											<input name="chargeacc" id="chargeacc" placeholder="Account name" class="form-control"/>
																															
											<label>Description</label>
											<input name="Description" id="Description" placeholder="Account name" class="form-control"/>
											
										</div>
										<div class="modal-footer">
											<button type="button" class="btn primary" data-dismiss="modal">Cancel</button>
											<button type="button" id="chloanaccount" class="btn success">Add  loan account</button>
										</div>
									</div>
									</div>
									</div>
										</div>
									</div>
									<div class="portlet-body form">
										
					   <div class="row">
		  <div class="col-md-12">
	 <div class="col-md-3">
		  <label>Account</label>
		<?php echo $this->Form->input('account', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'chaccountsearchname','placeholder'=>'account')); ?>
		<br/>
		</div>
		<div class="col-md-3">
		 <label>Status</label>
		<?php echo $this->Form->input('status', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'chaccountstatus','placeholder'=>'Status')); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('Search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'id'=>'chaccountsearch','required' => false,)); ?>
		<br/>
		</div> 
		</div>
		</div>	
										<!-- END FORM-->
										
										<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Account'); ?></th>
								<th><?php echo $this->Paginator->sort('Description'); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
								<th><?php echo $this->Paginator->sort('Action'); ?></th>
								
						</tr>
						</thead>
						<tbody id="chaccountdata">
						<?php if(isset($chaccountlist) && !empty($chaccountlist)){
						foreach($chaccountlist as $catn){
						echo '<tr>';
						echo '<td>'.$catn['OtherChargeaccount']['account'].'</td>';
						echo '<td>'.$catn['OtherChargeaccount']['description'].'</td>';
						echo '<td>'.$catn['OtherChargeaccount']['status'].'</td>';
						echo '<td>';?>
						<span class="newicon" onclick = "deletefunction(<?php echo $catn['OtherChargeaccount']['id'];?>,'OtherChargeaccount','chaccountdata');"><img src="<?php echo Router::url('/');?>img/delete.png"/></span> 
						
					
						<?php 
						echo '</td>';
						echo '</tr>';
						}
						
						}else{
						echo '<tr><td colspan="4">No records found</td></tr>';
						} ?>
						</tbody>
						</table>
						
									</div>
									
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
<script type="text/javascript">

$(document).ready(function() {

					  $('#tab1button').click(function(){
					  
					var categoryname = $('#categoryname').val();
					if(categoryname.trim()==""){
					alert('Please enter category name');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/newcategory',
									type: 'post',
									data: { cat: categoryname},
									success:function(data){
										$('#catde').prepend(data);
									/*	getplu();*/
									$('#basic').modal('hide');
									
									}
								});
					}

					  });
					  
			$('#searchcat').click(function(){
					  
					var categoryname = $('#categoryfilter').val();
					var statuscat = $('#statuscat').val();
					if(categoryname.trim()=="" && statuscat.trim()==""){
					alert('Please enter filter data');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/filtercategories',
									type: 'post',
									data: { cat: categoryname,statuscat:statuscat},
									success:function(data){
									
										$('#catde').html(data);
									}
								});
					}

					  });
			
				  $('#sourcebut').click(function(){
					  
					var source = $('#sourcena').val();
					if(source.trim()==""){
					alert('Please enter source name');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/newsource',
									type: 'post',
									data: { cat: source},
									success:function(data){
										$('#sourcedeta').prepend(data);
									/*	getplu();*/
									$('#source').modal('hide');
									
									}
								});
					}

					  });
					  
					  $('#incsearch').click(function(){
					  
					var sourcename = $('#sourcefil').val();
					var statusinc = $('#statusinc').val();
					if(sourcename.trim()=="" && statusinc.trim()==""){
					alert('Please enter filter data');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/filtercsource',
									type: 'post',
									data: { sourcename: sourcename,statusinc:statusinc},
									success:function(data){
									
										$('#sourcedeta').html(data);
									}
								});
					}

					  });
			  $('#newwithd').click(function(){
					  
					var drawername = $('#drawername').val();
					if(drawername.trim()==""){
					alert('Please enter With-drawer name');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/withdrawer',
									type: 'post',
									data: { cat: drawername},
									success:function(data){
										$('#drawerdata').prepend(data);
									/*	getplu();*/
									$('#drawer').modal('hide');
									
									}
								});
					}

					  });
					  
					  $('#seachdrawer').click(function(){
					  
					var withdrawfilter = $('#withdrawfilter').val();
					var withstatus = $('#withstatus').val();
					if(withdrawfilter.trim()=="" && withstatus.trim()==""){
					alert('Please enter filter data');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/filterwithdrawer',
									type: 'post',
									data: { withdrawfilter: withdrawfilter,withstatus:withstatus},
									success:function(data){
									
										$('#drawerdata').html(data);
									}
								});
					}

					  });
			$('#loanaccount').click(function(){
					  
					var Accountname = $('#Accountname').val();
					if(Accountname.trim()==""){
					alert('Please enter With-drawer name');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/account',
									type: 'post',
									data: { cat: Accountname},
									success:function(data){
										$('#accountdata').prepend(data);
									/*	getplu();*/
									$('#Account').modal('hide');
									
									}
								});
					}

					  });
					  
					  $('#accountsearch').click(function(){
					  
					var accountsearch = $('#accountsearchname').val();
					var accountstatus = $('#accountstatus').val();
					if(accountsearch.trim()=="" && accountstatus.trim()==""){
					alert('Please enter filter data');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/filteraccount',
									type: 'post',
									data: { accountsearch: accountsearch,accountstatus:accountstatus},
									success:function(data){
									
										$('#accountdata').html(data);
									}
								});
					}

					  });
					  
					  $('#chloanaccount').click(function(){
					  
					var Accountname = $('#chargeacc').val();
					var Description = $('#Description').val();
					if(Accountname.trim()==""){
					alert('Please enter account name');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/chaccount',
									type: 'post',
									data: { cat: Accountname,Description:Description},
									success:function(data){
										$('#chaccountdata').prepend(data);
									/*	getplu();*/
									
									$('#charge').removeClass('in');
									$('#charge').css({'display':'none'});
									$('.modal-backdrop').modal('hide');
									}
								});
					}

					  });
					  
					  $('#chaccountsearch').click(function(){
					  
					var accountsearch = $('#chaccountsearchname').val();
					var accountstatus = $('#chaccountstatus').val();
					if(accountsearch.trim()=="" && accountstatus.trim()==""){
					alert('Please enter filter data');
					}else{
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/chfilteraccount',
									type: 'post',
									data: { accountsearch: accountsearch,accountstatus:accountstatus},
									success:function(data){
									
										$('#chaccountdata').html(data);
									}
								});
					}

					  });
				
			
    });
	function deletefunction(id,model,divid){
		if(confirm('Are you sure want to delete it')){
					$.ajax({
									url: '<?php echo Router::url('/') ?>admin/other_entries/deleterecord',
									type: 'post',
									data: { id: id,model:model,divid:divid},
									success:function(data){
							//		alert(data);
										$('#'+divid).html(data);
									}
								});
				}
				}
</script>