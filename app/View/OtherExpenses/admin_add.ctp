<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Expenses
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <div class="col-md-2 col-xs-6">
                    <label>Expense date</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Expense_date', array('id' => 'Expensedate', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				    <div class="col-md-2 col-xs-6">
                    <label>Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Amount', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				  
				  
				   <div class="col-md-2 col-xs-6">
                    <label>Bank account</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Bank_account', array('id' => 'Bank_account', 'class' => 'form-control','type' => 'select','options'=>$banklist, 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Check number</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Check_number', array('id' => 'Check_number', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
  				   <div class="col-md-2 col-xs-6">
                    <label>Payment mode</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Payment_mode', array('id' => 'Payment_mode', 'class' => 'form-control','type' => 'select','options'=>array('Cash'=>'Cash','Check/EFT/ACH'=>'Check/EFT/ACH'),'label' => false, 'required' => false)); ?>
                  </div>
				   
				  <div class="col-md-2 col-xs-6">
                    <label>Category</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Category', array('id' => 'Category', 'class' => 'form-control','type' => 'select','options'=>$catgryies,'label' => false, 'required' => false)); ?>
                  </div>
				  <div class="col-md-2 col-xs-6">
                    <label>Payee</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Payee', array('id' => 'Payee', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  <div class="col-md-2 col-xs-6">
                    <label>Memo</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('memo', array('id' => 'memo', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				  
				   <div class="col-md-2 col-xs-6">
                    <label></label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
					
					
                  </div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
	</div>
	