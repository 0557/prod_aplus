<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i>  Add income
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <div class="col-md-2 col-xs-6">
                    <label>Income date</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Incomedate', array('id' => 'Incomedate', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				   <div class="col-md-2 col-xs-6">
                    <label>Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('min_cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				   <div class="col-md-2 col-xs-6">
                    <label>Bank account</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Bank_account', array('id' => 'Bank_account', 'class' => 'form-control','type' => 'select','options'=>$banklist, 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Income source</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('incomesource', array('id' => 'incomesource', 'class' => 'form-control','type' => 'select','options'=>$source,'label' => false, 'required' => false)); ?>
                  </div>
  				   <div class="col-md-2 col-xs-6">
                    <label>Memo</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('memo', array('id' => 'memo', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label></label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
                  </div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	</div>
	</div>
	