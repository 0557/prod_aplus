<div class="portlet box blue">
               <div class="page-content portlet-body" >
	
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Your store’s profit withdrawals
							</div>
						
						</div>
						<div class="portlet-body">
						
	<div class="rubyUprodts index">
		
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				
				  <div class="col-md-2 col-xs-6">
                    <label>Withdrawal date</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Withdrawal_date', array('id' => 'Expensedate', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Withdrawal by</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Withdrawalby', array('id' => 'Withdrawalby', 'class' => 'form-control','type' => 'select','options'=>$withdraw,'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Withdrawal mode</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Withdrawal_mode', array('id' => 'Withdrawal_mode', 'class' => 'form-control','type' => 'select','options'=>array('Check'=>'Check','Voucher'=>'Voucher','Cash'=>'Cash'),'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Bank account</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Bank_account', array('id' => 'Bank_account', 'class' => 'form-control','type' => 'select','options'=>$banklist, 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Check number</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Check_number', array('id' => 'Check_number', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
  				  
				   <div class="col-md-2 col-xs-6">
                    <label>Entry source</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Entry_source', array('id' => 'Entry_source', 'class' => 'form-control','type' => 'select','options'=>array('Store'=>'Store','Deli'=>'Deli'),'label' => false, 'required' => false)); ?>
                  </div>
				 
				 
				  <div class="col-md-2 col-xs-6">
                    <label>Min Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('min_cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>Max Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('max_cost', array('id' => 'vendor', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				   <div class="col-md-2 col-xs-6">
                    <label>&nbsp;</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Search', array('id' => 'vendor', 'class' => 'form-control btn brn-success','type' => 'submit', 'label' => false, 'required' => false)); ?>
                  </div>

    <?php echo $this->form->end(); ?>
				</div>
	</div>
	</div>
	
	<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Your store’s profit withdrawals
							</div>
							<div class="caption pull-right">
								<a href="<?php echo Router::url('/');?>admin/other_withdraws/add" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
							</div>
						
						</div>
						<div class="portlet-body">
		

					<div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                       
                                     <th><?php echo __($this->Paginator->sort('Withdrawal date'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Withdrawal by'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Account name'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Withdrawal mode'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Entry source'));?>  </th>
                                     <th><?php echo __($this->Paginator->sort('Amount'));?>  </th>
                                   
                                                 <th><?php echo $this->Paginator->sort('Actions'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($list) && !empty($list)) { 
							   
						
							$ms = "'Are you sure want delete it?'";
                                             foreach ($list as $data) { 
                                               echo '<tr>
                                                      <td>'.$data["PurchasePacks"]["vendor"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Item_Scan_Code"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Item_Name"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Units_Per_Case"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Case_Cost"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Discount"].'</td>
                                                      <td>'.$data["PurchasePacks"]["Rebate"].'</td>
                                                     
                                                      <td>	<a href="'.Router::url('/').'admin/grocery/edit_purchasepack/'.$data["PurchasePacks"]["id"].'" class="newicon red-stripe"><img src="'.Router::url('/').'img/edit.png"/></a>
													<a href="'.Router::url('/').'admin/grocery/purchase_packs/'.$data["PurchasePacks"]["id"].'"  onClick="return confirm('.$ms.');" class="newicon red-stripe" ><img src="'.Router::url('/').'img/delete.png"/></a></td>
												</tr>';
												
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="7">    
												<h3>No data exists for this search criteria.</h3>
        <h4>Try searching again with different criteria values.</h4> </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        
        </div> 
</div>			
</div>			
	</div>
