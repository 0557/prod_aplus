<div class="page-content-wrapper">
    <div class="portlet box blue">

        <div class="page-content portlet-body">        


            <div class="row">
                <div class="col-md-12">                
                    <h3 class="page-title">
                        Pack History
                    </h3>

                </div>
            </div>
            <div class="content-new">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lottery_setting clearfix">
                            <!-- hedaing title -->
                            <div class="confirm_heading clearfix">
                                <h5><strong>Please enter date range for this report  </strong>
                                    <span>  
                                        <input type="button" class="btn btn-warning pull-right" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">
                                    </span>
                                </h5>

                            </div>                           
                            <?php echo $this->Form->create('PackHistory', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

                            <div class="confim_form">
                                <!-- row one -->
                                <div class="row">
                                    <!-- input 1-->
                                    <div class="col-md-2 col-xs-6">
                                        <label>Updated date</label>                            
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>
                                    </div>
                                    <div class="col-md-2 ">
                                        <label>Status</label>
                                        <?php
										$status_options=array('Sold Out'=>'Sold Out','Counting'=>'Counting','Settle'=>'Settle','Return'=>'Return');
										 echo $this->Form->input('pstatus', array('options' =>$status_options, 'empty' => 'All', 'class' => 'form-control', 'label' => false,'value'=>$status));
										  ?>
                                    </div>
                                    <!-- input 1 ends-->
                                    <div class="col-md-8 ">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                        <label>&nbsp;</label>
                                        <a style=" margin-top:24px;" href="<?php echo Router::url('/'); ?>admin/pack_historys/reset" class="btn btn-warning ">Reset</a> 
                                        
                                        
                                        <a  style=" margin-top:24px;"  class="btn btn-success" data-toggle="modal" data-target="#SettleReturnEnter">Enter Settle/Return Packs</a>

                                    </div>

                                </div>
                            </div>
                            
                             
                            <div class="modal fade" id="SettleReturnEnter" role="dialog">
                              <div class="modal-dialog modal-lg">                               
                                <div class="modal-content">
                            <div class="modal-header" style=" padding: 20px;">                                   
                            <h3 class="modal-title">Enter Settle/Return Packs</h3>
                            </div> 
                            <br />                                  
                            <div class="modal-body credit-card-cont">
                            
                            <div class="col-md-2"><span>Date</span> <br>
                            <input type="text" class="amount" name="updated" id="updated"/>
                            </div>
                            <div class="col-md-3"><span>Game No</span> <br>                         
                            <?php	
							$game_nos=array();			
                            echo $this->Form->select('game_no',$game_nos,array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Game', 'id'=>'game_no'));
							?>
                            </div>   
                            <div class="col-md-2"><span>Pack No</span> <br>
                            <input type="text" class="amount" name="pack_no" id="pack_no"/>
                            </div>  
                            <div class="col-md-3"><span>Status</span> <br>                         
                            <?php	
							$stsdta=array('Settle'=>'Settle Pack','Return'=>'Return Pack');			
                            echo $this->Form->select('status',$stsdta,array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Status', 'id'=>'status'));
							?>
                            </div>
                            
                                                                   
                            <div class="col-md-2"> <br>
                            <a class="btn default updatebtn" id="svdta">Enter</a>
                            </div>     
                                                               
                            </div>                                  
                            <br />
                            <div class="modal-footer">                                 
                            <button type="button" id="cash_purchase_finish" class="mdlfnsh" data-dismiss="modal">Finish</button>
                            <button type="button" id="cash_purchase_close" class="mdlcls" data-dismiss="modal" style="float: left;">Close</button>
                            </div>
                            </div>
                            </div>
                            </div>
                            
                            
                            
                            <?php echo $this->form->end(); ?>
                            <!-- form ends -->
                        </div>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable tabbable-custom tabbable-full-width">

                            <div class="tab-content">

                                <div id="tab_1_5" class="tab-pane1">

                                    <?php //echo '<pre>'; print_r($games); die;		 ?>

                                    <table id="example" class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Game Number</th>
                                                <th>Pack Number</th>
                                                <th>Game</th>
                                                <th>Status</th>
                                                <th><Ticket Value</th>
                                                <th>Pack Cost</th>
<!--                                                <th><?php echo $this->Paginator->sort('Ticket per Pack'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Face Value'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Net Value'); ?></th>-->
<!--                                                <th><?php echo $this->Paginator->sort('Action'); ?></th>-->


                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($games) && !empty($games)) {
                                                $ms = "'Are you sure want delete it?'";
												$total_facevalue=0;
                                                foreach ($games as $data) {
													$total_facevalue=$total_facevalue+$data["PackHistory"]["face_value"];
                                                    echo '<tr>   
													<td>' . date('Y-m-d',strtotime($data["PackHistory"]["updated"])) . '</td>                                                    <td>' . $data["PackHistory"]["game_no"] . '</td>
                                                    <td>' . $data["PackHistory"]["pack_no"] . '&nbsp;</td>
                                                    <td>' . $data["PackHistory"]["gamename"] . '&nbsp;</td>
                                                    <td>' . $data["PackHistory"]["status"] . '&nbsp;</td>    
                                                    <td>' . $data["PackHistory"]["ticket_value"] . '&nbsp;</td>
                                                    <td>$ ' . $data["PackHistory"]["face_value"] . '&nbsp;</td>
                                                </tr>';
                                                }
                                            } else {
                                                echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                            }
											if (isset($games) && !empty($games)) 
											{
                                            ?>                                            
                                          <thead>
                                            <tr>

                                                <th>Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>$ <?php echo $total_facevalue; ?></th>

                                            </tr>
                                        </thead>
                                          <?php 
										  }
										  ?>  
                                            
                                        </tbody>
                                    </table>
                                    <div class="margin-top-20">
                                        <ul class="pagination">
                                            <li>
                                                <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                            </li>
                                            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                            <li> 
                                                <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!--end tabbable-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            
            
            
            
                       <?php /*?>  <td>$' . 
                                                            $pack_cost = substr($data["PackHistory"]["ticket_value"],1) * $data["PackHistory"]["ticket_per_pack"];
                                                            $pack_cost . '&nbsp;</td>
                                                    <td>' . $data["PackHistory"]["face_value"] . '&nbsp;</td>
                                                    <td>' . $data["PackHistory"]["net_value"] . '&nbsp;</td>
                                                    <td> <a href="' . Router::url('/') . 'admin/lottery/delete_cpack/' . $data["PackHistory"]["id"] . '" class="newicon red-stripe" onClick="return confirm(' . $ms . ');"><img src="' . Router::url('/') . 'img/delete.png"/></a></td><?php */?>
            
            
            
            
            
            
            
            
            
        </div>
        <style>
            .current{
                background: rgb(238, 238, 238) none repeat scroll 0 0;
                border-color: rgb(221, 221, 221);
                color: rgb(51, 51, 51);
                border: 1px solid rgb(221, 221, 221);
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .view_file{
                width: 30px;
                border: medium none;
                padding: 10px;
                color: #FFF;		
                font-size: 13px;		
            }



        </style>

    </div>
</div>
<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
    <script>
        $(document).ready(function () {
        $('#config-demo').val('');
        });
    </script>
    <?php
}
?>
<script>
                    var tableToExcel = (function () {
                            var uri = 'data:application/vnd.ms-excel;base64,'
                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                                                , base64 = function (s) {
                                                return window.btoa(unescape(encodeURIComponent(s)))
                                                }
                                        , format= function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (table, name) {
                        if (!table.nodeType)
                            table = document.getElementById(table)
                        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
                        window.location.href = uri + base64(format(template, ctx))
                    }
                })()
</script>
<script>
  $(document).ready(function (){         
	  $("#updated").change(function(){
	  	  var updated=this.value;	
		    //alert(updated);  
	        $.ajax({
				url: '<?php echo Router::url('/') ?>admin/pack_historys/getgames',
                type: 'post',
				dataType: 'text',
                data: { updated: updated},
                success:function(data){				
				//alert(data);
				$("#game_no").html(data);
                }
            });
	  
	  });
	   $("#game_no").change(function(){
	  	  var gameno=this.value;	
		    //alert(gameno);  
	        $.ajax({
				url: '<?php echo Router::url('/') ?>admin/pack_historys/getpacks',
                type: 'post',
				dataType: 'text',
                data: { gameno: gameno},
                success:function(data){				
				//alert(data);
				//exit;
				$("#pack_no").val(data);
                }
            });
	  
	  });
	  
	   $("#svdta").click(function(){
	  	  var updated=$("#updated").val();
		  var game_no=$("#game_no").val();	
		  var pack_no=$("#pack_no").val();
		  var status=$("#status").val();		
		    //alert(updated); 
		    //alert(game_no);   
		    //alert(pack_no);
			//alert(status);  
			//exit;
			if(updated==''){
			alert('Please choose a date');	
			exit;
			}
			if(game_no==''){
			alert('Please select a game no');	
			exit;
			}
			if(pack_no==''){
			alert('Please enter a pack no');
			exit;	
			}	
			if(status==''){
			alert('Please choose a status');
			exit;	
			}		
	        $.ajax({
				url: '<?php echo Router::url('/') ?>admin/pack_historys/savepacks',
                type: 'post',
				dataType: 'text',
                data: {updated:updated,game_no:game_no,pack_no:pack_no,status:status},
                success:function(data){				
				alert(data);
		        $("#updated").val('');
		        $("#game_no").html('<option value="">Select Game</option>');	
		        $("#pack_no").val('');
		        $("#status").val('');				
                }
            });
	  
	  });
	  
	   $(".mdlfnsh,.mdlcls").click(function(){
		  //alert('ok'); 
		  window.location.reload();
	   });
	  
  });
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#updated" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>
<style>	
    .amount { width: 100%;  border: 1px solid #ccc;  height: 32px;  margin: 2px 0 0 0;  border-radius: 5px;}  
	.modal-footer{margin-top:60px;}
	.mdlfnsh{width: 100px;border: none;padding: 10px;color: #fff; background-color: #337ab7; float: right;}
	.mdlcls{width: 100px;border: none;padding: 10px;color: #666; background-color: #CCC; float: left; border: 1px solid #ccc;border-radius: 5px;height: 38px; }
</style>
