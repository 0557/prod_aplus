<div  class="row">
    <div class="col-xs-9">
        <h3 class="page-title">Transactions from today</h3>

    </div>
    <div class="col-xs-2">
        <input name="report" id="load_date" class="form-control hasDatepicker" size="16" value="2017-02-05" type="text">
    </div>
</div>

<div class="row">

    <div class="col-md-5" >
        <div class="portlet box blue" style="border:1px solid #cfd0d0">
            <div class="portlet-title col-md-12">
                <div class="caption"><i class="fa fa-cogs"></i>Transaction Summary</div>
            </div>

            <div class="portlet-body">
                <div id="realtime_storelist">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <select id="ddlTransStore" class="form-control livestream">
                                    <option value="-1">All Store</option>
                                    <option value="5020">Passyunk-Gulf</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select id="ddlRegisterType" class="form-control livestream">
                                    <option value="-1">All</option>
                                    <option value="1">Primary</option>
                                    <option value="0">Secondary</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select id="ddlTransType" class="form-control livestream">
                                    <option value="-1">All Type</option>
                                    <option value="1">Void</option>
                                    <option value="2">No Sale</option>
                                    <option value="3">Line void</option>
                                    <option value="4">Refund</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select id="ddlTransInterval" class="form-control livestream">
                                    <option value="-1">Last 50</option>
                                    <option value="1">All Day</option>
                                    <option value="2">Last 5 Minutes</option>
                                    <option value="3">Last Hour</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <style>
                        .tran-sum-span
                        {
                            color:#7aa73a;font-size:14px;
                        }
                        .tran-sum-dollar
                        {
                            font-size:18px;color:#4f87b1
                        }
                    </style>
                    <div class="row" >
                        <div class="col-md-12">
                            <ul style="padding:5px;height: 300px; overflow-y: scroll;overflow-x:hidden" >
                                <a href="#">
                                    <li style="border-bottom: 1px solid #e1e1e1;padding:3px ">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="tran-sum-span" >Passyunk-Gulf</span>
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                            <div class="col-sm-4 tran-sum-dollar" >
                                                $0.99
                                            </div>
                                            <div class="col-sm-2">
                                               <i class="fa fa-chevron-right ">&nbsp;</i>
                                            </div>
                                        </div>
                                    </li>
                                </a>
                                <a href="#">
                                    <li style="border-bottom: 1px solid #e1e1e1;padding:3px  ">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="tran-sum-span">Passyunk-Gulf</span>
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                            <div class="col-sm-4  tran-sum-dollar">
                                                $0.99
                                            </div>
                                            <div class="col-sm-2">
                                               <i class="fa fa-chevron-right ">&nbsp;</i>
                                            </div>
                                        </div>
                                    </li>
                                </a>    
                                <a href="#">    
                                    <li style="border-bottom: 1px solid #e1e1e1;padding:3px  ">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="tran-sum-span">Passyunk-Gulf</span>
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                            <div class="col-sm-4  tran-sum-dollar">
                                                $0.99
                                            </div>
                                            <div class="col-sm-2">
                                               <i class="fa fa-chevron-right ">&nbsp;</i>
                                            </div>
                                        </div>
                                    </li>
                                </a>    
                                </ul>
                            <div class="table-responsive" >
                                
                                
<!--                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <tr>
                                        <td>
                                            <div>
                                                <span style="color:#7aa73a">Passyunk-Gulf</span>
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                        </td>
                                        <td>$0.99</td>
                                        <td>></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                Passyunk-Gulf
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                        </td>
                                        <td>$0.99</td>
                                        <td>></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                Passyunk-Gulf
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                        </td>
                                        <td>$0.99</td>
                                        <td>></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                Passyunk-Gulf
                                                <br>
                                                <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>2/5/2017 1:15:24 AM</span></small>
                                            </div>
                                        </td>
                                        <td>$0.99</td>
                                        <td>></td>
                                    </tr>

                                </table>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7" style="">
        <div class="portlet box blue" style="border:1px solid #cfd0d0;">
            <div class="portlet-title col-md-12">
                <div class="caption"><i class="fa fa-cogs"></i>Transaction Detail</div>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div style="text-align: center"><h4>Passyunk-Gulf<h4></div>
                        <div style="text-align: center">2/5/2017 1:15:24 AM</div>
                        <div>
                            <span class="pull-left">Employee:&nbsp;&nbsp;1</span>
                            <span class="pull-right">
                                Station:&nbsp;&nbsp;101&nbsp;(Primary)
                              </span>
                        </div>
                        
                        
                    </div>
                </div>
                
                
                <div class="row" style="padding:25px;">
                    <div class="" style="height:400px;">
                        <div class="col-md-11" style="border-bottom: 1px  dotted; margin-bottom:10px;" >
                            <div class="receipt-item-name col-md-5 " >GAME LEAF COGNAC 2/9</div>
                            <div class="receipt-item-qty col-md-2">x  1</div>
                            <div class="receipt-item-price col-md-4">$ 0.99</div>
                            <div class="clearfix">&nbsp;</div>
                                
                        </div>
                        <div class="col-md-11 ">
                            <div class="row" style="border-bottom: 1px  dotted">
                                <div class="col-md-5"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4 ">TOTAL</div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4 ">Gross  &nbsp;$ 0.99</div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4 ">Tax  &nbsp;$ 0.00</div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4 ">Net  &nbsp;$ 0.99</div>
                            </div>
                            
                                
                        </div>
                            
                    </div>
                    
                
                </div>
            </div>
        </div>
    </div>


</div>