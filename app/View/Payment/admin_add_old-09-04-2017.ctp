<!--<script type="text/javascript">
jQuery( document ).ready(function() {  
$('#Paymentdate').Zebra_DatePicker({'format':'m-d-Y'});

   $('input').keyup(function(){
   var plus = 0;
   var minus = 0;
   
   $('input[id="plus"]').each(function(){
  
        var value = $(this).val();
                   plus = (+plus) + (+value);
                });
   $('input[id="minus"]').each(function(){
  
        var valuem = $(this).val();
                   minus = (+minus) + (+valuem);
                });
var result = plus - minus;		     
$('#result').val(result);
 });
  
 });  
</script>-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#Paymentdate").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>

<?php echo $this->Form->create('Payment', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php echo $this->Html->script(array('admin/custom')); ?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Add Payment
                        </div>

                    </div>
                </div>
                <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
            </div>

            <div class="form-body col-md-12">
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        

                                <div class="row static-info">
                                    <div class="col-md-2 value">
                                        <label>
                                            Payment date:<span class="star">*</span>	 
                                        </label>

                                        <?php echo $this->Form->input('Paymentdate', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'id' => 'Paymentdate')); ?>

                                    </div>
                                    <div class="col-md-2 value">
                                        <label>
                                            Invoices <span style="color:green">+ $</span>			 
                                        </label>

                                        <?php echo $this->Form->input('Invoices', array('class' => 'form-control', 'required' => false, 'label' => false, 'id' => 'plus', 'value' => '0.0')); ?>

                                    </div>
                                    <div class="col-md-2 value">
                                        <label>
                                            Credit cards <span style="color:red">- $</span>	 
                                        </label>

                                        <?php echo $this->Form->input('Creditcards', array('class' => 'form-control', 'required' => false, 'label' => false, 'id' => 'minus', 'value' => '0.0')); ?>

                                    </div>

                                    <div class="col-md-3 value">
                                        <label>
                                            Credit card fee adjustment <span style="color:green">+ $</span>	 
                                        </label>

                                        <?php echo $this->Form->input('Creditcardfeeadjustment', array('class' => 'form-control', 'required' => false, 'label' => false, 'id' => 'plus', 'value' => '0.0')); ?>

                                    </div>
                                    <div class="col-md-2 value">
                                        <label>
                                            Cash card credits <span style="color:green">+ $</span>	 
                                        </label>

                                        <?php echo $this->Form->input('Cashcardcredits', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'plus', 'value' => '0.0')); ?>

                                    </div>
                                </div>
        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        
                                <div class="row static-info">
                                    <div class="col-md-3 value">
                                        <label>
                                            Cash card commission adjustment <span style="color:red">- $</span>
                                        </label>

                                        <?php echo $this->Form->input('Cashcardcommissionadjustment', array('class' => 'form-control', 'label' => false, 'id' => 'minus', 'value' => '0.0')); ?>

                                    </div>
                                    <div class="col-md-2 value">
                                        <label>
                                            Other charges <span style="color:green">+ $</span>
                                        </label>

                                        <?php echo $this->Form->input('Othercharges', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'id' => 'plus', 'value' => '0.0')); ?>
                                    </div>
                                    <div class="col-md-2 value">
                                        <label>
                                            Miscellaneous memo	 
                                        </label>

                                        <?php echo $this->Form->input('Miscellaneousmemo', array('class' => 'form-control', 'type' => 'text', 'required' => false, 'label' => false)); ?>
                                    </div>

                                    <div class="col-md-2 value">
                                        <label>
                                            Net payments:	 
                                        </label>

                                        <?php echo $this->Form->input('netpayment', array('class' => 'form-control', 'required' => false, 'label' => false, 'readonly' => true, 'id' => 'result')); ?>
                                    </div>
                                </div>

                    </div>

                </div>




                <!-- END FORM-->
            </div>


        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit" class="btn blue">Submit</button>
            <button type="reset" class="btn default">Reset</button>
            <button type="button" onclick="javascript:history.back(1)"
                    class="btn default">Cancel</button>

            <!--<button type="button" class="btn default">Cancel</button>-->
        </div>

    </div>
</div>
<?php echo $this->form->end(); ?>








