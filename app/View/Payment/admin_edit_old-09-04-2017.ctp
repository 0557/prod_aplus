<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#Paymentdate" ).datepicker({ dateFormat: 'mm-dd-yy' });	
  } );
  </script>
<script type="text/javascript">
jQuery( document ).ready(function() {  
//$('#Paymentdate').Zebra_DatePicker({'format':'m-d-Y'});

   $('input').keyup(function(){
   var plus = 0;
   var minus = 0;
   
   $('input[id="plus"]').each(function(){
  
	var value = $(this).val();
		   plus = (+plus) + (+value);
		});
   $('input[id="minus"]').each(function(){
  
	var valuem = $(this).val();
		   minus = (+minus) + (+valuem);
		});
var result = plus - minus;		     
$('#result').val(result);
 });
  
 });  
</script>

<?php echo $this->Form->create('Payment', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php echo $this->Html->script(array('admin/custom')); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Payment
                    </div>
                   
                </div>
            </div>
            <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
        </div>

        <div class="form-body col-md-12">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                          
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Payment date:<span class="star">*</span>	 
                                </div>
                                <div class="col-md-7 value">
								
                                    <?php 
									echo $this->Form->input('Paymentdate', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'id' => 'Paymentdate','value'=>date('m-d-Y',strtotime($editrecord['Payment']['Paymentdate'])))); ?>

                                </div>
                            </div>




                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoices <span style="color:green">+ $</span>			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Invoices', array('class' => 'form-control', 'required' => false, 'label' => false,'id'=>'plus','value'=>$editrecord['Payment']['Invoices'])); ?>
									
                                    <?php echo $this->Form->input('id', array('class' => 'form-control', 'required' => false, 'label' => false,'value'=>$editrecord['Payment']['id'],'type'=>'hidden')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Credit cards <span style="color:red">- $</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Creditcards', array('class' => 'form-control', 'required' => false, 'label' => false,'id'=>'minus','value'=>$editrecord['Payment']['Creditcards'])); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Credit card fee adjustment <span style="color:green">+ $</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Creditcardfeeadjustment', array('class' => 'form-control', 'required' => false, 'label' => false,'id'=>'plus','value'=>$editrecord['Payment']['Creditcardfeeadjustment'])); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Cash card credits <span style="color:green">+ $</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Cashcardcredits', array('class' => 'form-control', 'required' => 'false', 'label' => false,'id'=>'plus','value'=>$editrecord['Payment']['Cashcardcredits'])); ?>

                                </div>
                            </div>
                            


                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                           
                        </div>
                        <div class="portlet-body">
						
						<div class="row static-info">
                                <div class="col-md-5 name">
                                    Cash card commission adjustment <span style="color:red">- $</span>
                                </div>
                                <div class="col-md-7 value">
                                   <?php echo $this->Form->input('Cashcardcommissionadjustment', array('class' => 'form-control', 'label' => false,'id'=>'minus','value'=>$editrecord['Payment']['Cashcardcommissionadjustment'])); ?>

                                </div>
                            </div>

							
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Other charges <span style="color:green">+ $</span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Othercharges', array('class' => 'form-control', 'label' => false, 'required' => 'false','id'=>'plus','value'=>$editrecord['Payment']['Othercharges'])); ?>
                                </div>
                            </div>

                          
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Miscellaneous memo	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Miscellaneousmemo', array('class' => 'form-control', 'type'=>'text', 'required' => false, 'label' => false,'value'=>$editrecord['Payment']['Miscellaneousmemo'])); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Net payments:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('netpayment', array('class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$editrecord['Payment']['netpayment'],'readonly' => true,'id'=>'result')); ?>
                                </div>
                            </div>



                        </div></div></div>

            </div>




            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>








