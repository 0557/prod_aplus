<?php if(@$grocerytab){
$type = "/grocery";
}else{
$type="";
} ?>

<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Payment  <span class="btn green fileinput-button">
                    <?php echo $this->Html->link('<i class="fa fa-plus"></i><span>Add New</span>', array('controller' => 'payment', 'action' => 'add'.$type), array('escape' => false)); ?>

                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                            
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover payment-table">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('Payment date')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Invoices')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Credit cards')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('Credit card fee adjustment')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Cash card credits')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('Cash card commission adjustment ')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('Other charges')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Miscellaneous memo')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Net payments')); ?>  </th>
                                           
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($payment) && !empty($payment)) { ?>
                                            <?php foreach ($payment as $data) { ?> 
                                                <tr>
                                                    <td><span class="nobr"><?php echo date('m-d-Y',strtotime($data['Payment']['Paymentdate'])); ?></span></td>
                                                    <td> <?php echo $data['Payment']['Invoices']; ?> </td>
                                                    <td> <?php echo $data['Payment']['Creditcards']; ?> </td>
                                                    <td>  <?php echo $data['Payment']['Creditcardfeeadjustment']; ?>  </td>
                                                    <td>  <?php echo $data['Payment']['Cashcardcredits']; ?>  </td>
                                                    <td> <?php echo $data['Payment']['Cashcardcommissionadjustment']; ?> </td>
                                                    <td> <?php echo $data['Payment']['Othercharges']; ?>  </td>
                                                    <td> <?php echo $data['Payment']['Miscellaneousmemo']; ?>  </td>
                                                    <td> <?php echo $data['Payment']['netpayment']; ?>  </td>
                                                 
                                                    <td style="width:116px">
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('controller'=>'payment','action'=>'edit/'.$data['Payment']['id'].$type),array('escape' => false,'class'=>'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>',array('controller'=>'payment','action'=>'index',$data['Payment']['id']) ,array('escape' => false,'class'=>'newicon red-stripe delete')); ?>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="11">  no result found </td>
                                                </tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>