<?php
//echo 'my pdf view'; pr($data[0]); die;

App::import('Vendor','xtcpdf');
 
$pdf = new XTCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
 
$pdf->AddPage();

$html='<html>
		<head></head>
		<body>
		<h1>Item Reconcilations Report</h1>
			<table border="1">
				<tr style="background:#CCC;">
					<th align="center">Item</th>
					<th align="center">Description</th>
					<th align="center">On Hand</th>
					<th align="center">On Self</th>
					<th align="center">S/O</th>
					<th align="center">Cost</th>
					<th align="center">Price Difference</th>
				</tr>';
				foreach($data as $dat){
					$sales_inventory = @$pluItemBySale[$dat['RGroceryItem']['plu_no']]; //echo $sales_inventory; die;
					$handvalue =  (($dat['RGroceryItem']['current_inventory'] + $dat['RGroceryItem']['purchase']) - $sales_inventory);
					$on_self= $dat['RGroceryItem']['on_self'];
					$s_o  = $on_self - $handvalue;
					
					$price_diff = $s_o*$dat['RGroceryItem']['price'];
					if($price_diff>0){ 
					$price_diff = round($price_diff,2);
					}

$html.=				'<tr>
					<td align="center">'.$dat['RGroceryItem']['plu_no'].'</td>
					<td align="center">'.$dat['RGroceryItem']['description'].'</td>
					<td align="center">'.$handvalue.'</td>
					<td align="center">'.$on_self.'</td>
					<td align="center">'.$s_o.'</td>
					<td align="center">'.$dat['RGroceryItem']['price'].'</td>
					<td align="center">'.$price_diff.'</td>
				</tr>';
				}
$html.=			'</table>
		</body>
		</html>';
//echo $html; die;

$pdf->writeHTML($html, true, false, true, false, '');
 
$pdf->lastPage();
 
$full_path = WWW_ROOT . 'files/pdf' . DS . $filename;
$pdf->Output($full_path, 'FD');
exit();

