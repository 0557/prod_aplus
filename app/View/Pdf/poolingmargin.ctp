<?php
//echo '<pre>'; print_r($PollingMarginreports); die;
$storeId = $this->Session->read('stores_id');

App::import('Vendor','xtcpdf');
 
$pdf = new XTCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
 
$pdf->AddPage();
$filename=time().".pdf";
$html='<html>
		<head></head>
		<body>
		<h1>Pool Margin Report</h1>
		<p>Store Name: '.$storename.'</p>
		<p>Date: '.$full_date.'</p>
			<table border="1">
				<tr style="background:#CCC;">
					<th align="center">Grade</th>
					<th align="center">Avg Price($)</th>
					<th align="center">Avg Cost($)</th>
					<th align="center">Gallon Sold</th>
					<th align="center">Dollar sold by Price($)</th>
					<th align="center">Dollar sold by Cost($)</th>
					<th align="center">Profit ($)</th>
					<th align="center">Margin($)</th>
				</tr>';
				$_summaryTotal = array();				
							foreach($PollingMarginreports as $_k => $_value) {
								$_ukey = @$prodList[$_value['PollingMarginreport']['fuel_product_number']];
						
								if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
									$_summaryTotal[$_ukey]['fuel_volume'] = $_value['PollingMarginreport']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  = $_value['PollingMarginreport']['fuel_value'];
								} else {
									$_summaryTotal[$_ukey]['fuel_volume'] += $_value['PollingMarginreport']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  += $_value['PollingMarginreport']['fuel_value'];
									}
							}
									
								//pr($_summaryTotal); die;
								if($totaldiff>0) 
								{ 
									$numdays=$totaldiff;
								 } 
								else {
									$numdays=1; 
								}                                      //pr($PollingMarginreports);  
								$total_gallonsold=0;
								$total_profit=0;
			foreach($prodList as $prodLists) {
				$fuel_volme=10; $fuel_val=0;
					if($prodLists=='Diesel'){                                         
					$prdctID=1;
					}
					elseif($prodLists=='Regular'){
					$prdctID=4;
					}
					elseif($prodLists=='Premium') {
					$prdctID=5;
					}
					else
					{
					$prdctID=6;
					} 
					
						  $results2= ClassRegistry::init('FuelProduct')->query("SELECT `FuelProduct`.cost_per_gallon from `fuel_products` as FuelProduct left join  purchase_invoices as PurchaseInvoice on FuelProduct.r_purchase_invoice_id = PurchaseInvoice.id  WHERE FuelProduct.product_id = '".$prdctID."' AND PurchaseInvoice.receving_date >='".$from_date."' AND PurchaseInvoice.receving_date <='".$to_date."'  ");    
										  $ttl=0;
										  $i=0;
										  foreach($results2 as $res){
											  $cgallon = $results2[$i]['FuelProduct']['cost_per_gallon'];
											 $ttl=$ttl+$cgallon;
											 $i++;
										  }
										
										  
									if($ttl!=""){
										$avgcost=$ttl/$numdays;
									}else{
										$avgcost=0;
									}
										$fuel_volme=$_summaryTotal[$prodLists]['fuel_volume'];
										 $fuel_val=$_summaryTotal[$prodLists]['fuel_value'];
										 $avgprc=($fuel_val/$fuel_volme)/$numdays;
										 $gallonsoild=$fuel_volme/$numdays;
										$ap= number_format($avgprc ,4);
										$ac= number_format($avgcost ,4);
										$tg=number_format($fuel_volme/$numdays ,4); 
										$total_gallonsold=$total_gallonsold+($gallonsoild);
										$dlroildp=$ap*$gallonsoild;
										$dp= number_format($dlroildp,4);
										 $dlroildc=$ac*$gallonsoild;
										$dc= number_format($dlroildc,4);
										$profit=number_format($dlroildp-$dlroildc,4); 
										$total_profit=$total_profit+($dlroildp-$dlroildc);
										$margin=number_format($avgprc-$avgcost,4); 
										
									$pooling=($total_profit/$total_gallonsold) ;
					 				$pp=(number_format((float)($pooling), 4, '.', ''));
							$html.=		'<tr>
										<td align="center">'.$prodLists.'</td>
										<td align="center">'.$ap.'</td>
										<td align="center">'.$ac.'</td>
										<td align="center">'.$tg.'</td>
										<td align="center">'.$dp.'</td>
										<td align="center">'.$dc.'</td>
										<td align="center">'.$profit.'</td>
										<td align="center">'.$margin.'</td>
									</tr>';
									}
$html.=			'
<tr style="background:#CCC;">
					<th align="center">Total</th>
					<th align="center">&nbsp;</th>
					<th align="center">&nbsp;</th>
					<th align="center">'.$total_gallonsold.'</th>
					<th align="center">&nbsp;</th>
					<th align="center">&nbsp;</th>
					<th align="center">'.$total_profit.'</th>
					<th align="center">&nbsp;</th>
				</tr>
</table>
 <h4 style="txtrht">
                   <b> Pool Margin= $'.$pp.'</b>
                     					
                </h4>
		</body>
		</html>';
//echo $html; die;

$pdf->writeHTML($html, true, false, true, false, '');
 
$pdf->lastPage();
 
$full_path = WWW_ROOT . 'files/pdf' . DS . $filename;
$pdf->Output($full_path, 'FD');
exit();

