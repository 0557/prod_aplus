<?php
//echo '<pre>'; print_r($revenuesales); die;
$storeId = $this->Session->read('stores_id');

App::import('Vendor','xtcpdf');
 
$pdf = new XTCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
 
$pdf->AddPage();
$filename=time().".pdf";
if($this->Session->read('pos_type')=='ruby2'){
$html='<html>
		<head></head>
		<body style="font-size:11px;">
		<p>Store Name: '.$storename.'</p>
		<p>Date: '.$full_date.'</p>';
		
		if (isset($ruby2summarytotal) && ($ruby2summarytotal['Ruby2Summary']['id']!='')) {
		
           $html.=	'            
		                             <h3> Revenue Sales</h3>
		                              <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Description</th>        
										   <th align="center">Sales</th>                          
                                        </tr>
										       <tr>
                                                    <td>Total Inside Fuel Sales&nbsp;</td>
                                                    <td class="txtrht">$ '.number_format((float)($ruby2summarytotal['Ruby2Summary']['fuel_sales']), 2, '.', '').'&nbsp;</td>   
											   </tr>
                                               <tr>
                                                    <td>Net Department Sales&nbsp;</td>
                                                    <td class="txtrht">$'.number_format((float)($ruby2summarytotal['Ruby2Summary']['merch_sales']), 2, '.', '').'&nbsp;</td>   
											   </tr>  
                                              
                                              <tr>
                                                    <td>Total Lottery Net Sales&nbsp;</td>
                                                    <td class="txtrht">$0.00&nbsp;</td>                                              </tr> 
                                              <tr>
                                                    <td>Total Money Orders Sales&nbsp;</td>
                                                     <td class="txtrht">$0.00&nbsp;</td>                                              </tr>
                                              <tr>
                                                    <td>Total Car Wash Sales&nbsp;</td>
                                                    <td class="txtrht">$0.00&nbsp;</td>                                              </tr>
                                        <tr>                                        
                                           <th align="center">Total Sales</th>
                                            <th align="center">$'.number_format((float)($ruby2summarytotal['Ruby2Summary']['fuel_sales']+$ruby2summarytotal['Ruby2Summary']['merch_sales']), 2, '.', '').'</th>     
                                        </tr>
                                </table>';
								
		}
		
	if (isset($ruby2tax) && $ruby2tax['Ruby2Tax']['id']!='') {							
	$html.=	' 			
      <h3>Other Revenue Sales/Sales Taxes</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Description</th>        
										   <th align="center">Sales</th>                          
                                        </tr>
									    <tr>
											<td>Tax Id&nbsp;</td>
											<td class="txtrht"> '.$ruby2tax['Ruby2Tax']['tax_sysid'].'&nbsp;</td>                                        </tr>
									    <tr>
											<td>Tax Name&nbsp;</td>
											<td class="txtrht"> '.$ruby2tax['Ruby2Tax']['name'].'&nbsp;</td>                                        </tr>
											
										<tr>
											<td>Tax Rate&nbsp;</td>
											<td class="txtrht">$ '.number_format((float)($ruby2tax['Ruby2Tax']['tax_rate']), 2, '.', '').'&nbsp;</td>                  </tr>
									    <tr>
											<td>Actual Tax Rate&nbsp;</td>
											<td class="txtrht">$'.number_format((float)($ruby2tax['Ruby2Tax']['actual_tax_rate']), 2, '.', '').'&nbsp;</td>            </tr> 	
										
										 <tr>
											<td>Taxable Sales&nbsp;</td>
											<td class="txtrht">$ '.number_format((float)($ruby2tax['Ruby2Tax']['taxable_sales']), 2, '.', '').'&nbsp;</td>             </tr>
									    <tr>
											<td>Non Taxable Sales&nbsp;</td>
											<td class="txtrht">$'.number_format((float)($ruby2tax['Ruby2Tax']['non_taxable_sales']), 2, '.', '').'&nbsp;</td>      
										</tr>
										
										 <tr>
											<td>Sales Tax&nbsp;</td>
											<td class="txtrht">$ '.number_format((float)($ruby2tax['Ruby2Tax']['sales_tax']), 2, '.', '').'&nbsp;</td>             </tr>
									    <tr>
											<td>Refund Tax&nbsp;</td>
											<td class="txtrht">$'.number_format((float)($ruby2tax['Ruby2Tax']['refund_tax']), 2, '.', '').'&nbsp;</td>      
										</tr>		
										
									  <tr>
											<td>Net Tax&nbsp;</td>
											<td class="txtrht">$'.number_format((float)($ruby2tax['Ruby2Tax']['net_tax']), 2, '.', '').'&nbsp;</td>      
									  </tr>			  
                               
                                </table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>';
								
	}
		if (isset($ruby2summarytotal) && $ruby2summarytotal['Ruby2Summary']['id']!='') {						
								
	  	$html.=	' 	<h3>Miscellaneous Summary</h3>
                                <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Miscellaneous Summary</th>        
										   <th align="center">Count	</th>   
										   <th align="center">Value	</th>                           
                                        </tr>
										   <tr>
												<td>Payment Out</td>
												<td class="txtrht">0</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['total_payment_out']), 2, '.', '').'&nbsp;</td> 
										   </tr>
										   
										  <tr>
												<td>Payment In</td>
												<td class="txtrht">0</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['total_payment_in']), 2, '.', '').'&nbsp;</td> 
										   </tr> 
										   
										   <tr>
												<td>Sales Tax</td>
												<td class="txtrht">0</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['total_taxes']), 2, '.', '').'&nbsp;</td> 
										   </tr> 
										   
										    <tr>
												<td>Item Count</td>
												<td class="txtrht">0</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['item_count']), 2, '.', '').'&nbsp;</td> 
										   </tr> 
										   
										     <tr>
												<td>Customer Count</td>
												<td class="txtrht">0</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['customer_count']), 2, '.', '').'&nbsp;</td> 
										   </tr> 
										   
										   
								            <tr>
												<td>Safe Drop</td>
												<td class="txtrht">'.number_format((float)($ruby2summarytotal['Ruby2Summary']['safe_drop_count']), 2, '.', '').'&nbsp;</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['safe_drop_amount']), 2, '.', '').'&nbsp;</td> 
										   </tr> 
										   
										   <tr>
												<td>Void Line</td>
												<td class="txtrht">'.number_format((float)($ruby2summarytotal['Ruby2Summary']['void_line_count']), 2, '.', '').'&nbsp;</td>
												<td class="txtrht"> '.number_format((float)($ruby2summarytotal['Ruby2Summary']['void_line_amount']), 2, '.', '').'&nbsp;</td> 
										   </tr> 		   
										   
                                              
                                </table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>								
								<br>
								<br>
								<br>
								<br>';
								
                        }
				if(isset($ruby2summarytotal)&& $ruby2summarytotal['Ruby2Summary']['id']!=''){				
								
						$html.=	'		
						    	  <h3>Grand Totalizer Network</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Name</th>        
										   <th align="center">Start</th>  
										   <th align="center">End</th>   
										   <th align="center">Difference</th>                        
                                        </tr>
										
									    <tr>
											<td>Inside Grand&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_insidegrand'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_insidegrand'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_insidegrand'].'&nbsp;</td> 							
										</tr>
										
										 <tr>
											<td>Inside Sales&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_insidesales'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_insidesales'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_insidesales'].'&nbsp;</td> 							
										</tr>
										
										<tr>
											<td>Outside Grand&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_outsidegrand'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_outsidegrand'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_outsidegrand'].'&nbsp;</td> 							
										</tr>
										
										<tr>
											<td>Outside Sales&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_outsidesales'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_outsidesales'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_outsidesales'].'&nbsp;</td> 							
										</tr>
										
										
										<tr>
											<td>Overall Grand&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_overallgrand'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_overallgrand'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_overallgrand'].'&nbsp;</td> 							
										</tr>
										
										<tr>
											<td>Overall Sales&nbsp;</td>
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['start_overallsales'].'&nbsp;</td>                                        
											<td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['end_overallsales'].'&nbsp;</td> 
									        <td class="txtrht"> '.$ruby2summarytotal['Ruby2Summary']['diff_overallsales'].'&nbsp;</td> 							
										</tr>
									 
                               
                               
                                </table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>';
				}
				
				if(isset($ruby2summary) && $ruby2summary[0]['Ruby2Summary']['id']!=''){	
				
				 $html.=	' 						
									<h3>Money Totals</h3>
                                <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Index</th>        
										   <th align="center">No Of Sales</th>   
										   <th align="center">Value Of Sales</th>                           
                                        </tr>';
								 foreach($ruby2summary as $ruby2summary_row)
							 {	
								 $html.=	' 
								                <tr>
                                                    <td>'.$ruby2summary_row['Ruby2Summary']['mop_name'].'&nbsp;</td>
                                                    <td class="txtrht"> '.$ruby2summary_row['Ruby2Summary']['mop_count'].'&nbsp;</td>
													<td class="txtrht"> '.$ruby2summary_row['Ruby2Summary']['mop_amount'].'&nbsp;</td>                                    </tr>';
							 }
							  $html.=	' 
                              
                                </table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>';
				}
				if(isset($ruby2network) && $ruby2network[0]['Ruby2Network']['id']!=''){				
								
				$html.=	' 		
							  <h3>Credit Card Details</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Card Number</th>        
										   <th align="center">Card Name</th> 
										   <th align="center">Count</th>  
										   <th align="center">Amount</th>                       
                                        </tr>';
							foreach($ruby2network as $ruby2network_row)
							 {										
							$html.=	'			
									<tr>
                                          <td>'.$ruby2network_row['Ruby2Network']['card_number'].'&nbsp;</td>
                                          <td class="txtrht">'.$ruby2network_row['Ruby2Network']['card_name'].'&nbsp;</td> 
										  <td class="txtrht">'.$ruby2network_row['Ruby2Network']['count'].'&nbsp;</td>
										  <td class="txtrht">'.$ruby2network_row['Ruby2Network']['amount'].'&nbsp;</td>
									</tr>';
							 }                               				  
                               
                             $html.=	'   </table>';
								
								
				}
								
				$html.=	'				
	  
		</body>
		</html>';	
}else{
$html='<html>
		<head></head>
		<body style="font-size:11px;">
		<p>Store Name: '.$storename.'</p>
		<p>Date: '.$full_date.'</p>
		<h3> Revenue Sales</h3>
                                <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Description</th>        
										   <th align="center">Sales</th>                          
                                        </tr>';
										$inside_fuelsales=number_format((float)($revenuesales[0]['RevenueSale']['total_inside_fuel_sales']), 2, '.', '');
										$outside_fuelsales=number_format((float)($revenuesales[0]['RevenueSale']['total_outside_fuel_sales']), 2, '.', '');
                                   $netdepartment=number_format((float)($revenuesales[0]['RevenueSale']['total_merchandise_sales']), 2, '.', '');
										$moneyorder=number_format((float)($RubyTotsum[0]['RubyTotsum']['value_of_money_order_sales']), 2, '.', '');
										
										$totalsales= number_format((float)($revenuesales[0]['RevenueSale']['total_inside_fuel_sales']+$revenuesales[0]['RevenueSale']['total_outside_fuel_sales']+$revenuesales[0]['RevenueSale']['total_merchandise_sales']+$RubyTotsum[0]['RubyTotsum']['value_of_money_order_sales']), 2, '.', '');
                                   
       $html.=	'                              <tr>
                                                    <td>Total Inside Fuel Sales&nbsp;</td>
                                                    <td class="txtrht">$ '.$inside_fuelsales.'&nbsp;</td>                                                </tr>
                                               <tr>
                                                    <td>Total Outside Fuel Sales&nbsp;</td>
                                                    <td class="txtrht">$'.$outside_fuelsales.'&nbsp;</td>                                               </tr>  
                                                <tr>
                                                   <td>Net Department Sales&nbsp;</td>
                                                   <td class="txtrht">$'.$netdepartment.'&nbsp;</td>                                               </tr>  
                                              <tr>
                                                    <td>Total Lottery Net Sales&nbsp;</td>
                                                    <td class="txtrht">$0.00&nbsp;</td>                                              </tr> 
                                              <tr>
                                                    <td>Total Money Orders Sales&nbsp;</td>
                                                     <td class="txtrht">$'.$moneyorder.'&nbsp;</td>                                              </tr>
                                              <tr>
                                                    <td>Total Car Wash Sales&nbsp;</td>
                                                    <td class="txtrht">$0.00&nbsp;</td>                                              </tr>
                                        <tr>                                        
                                           <th align="center">Total Sales</th>
                                            <th align="center">$'.$totalsales.'</th>     
                                        </tr>
                                </table>
      <h3>Other Revenue Sales/Sales Taxes</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Description</th>        
										   <th align="center">Sales</th>                          
                                        </tr>';
										$taxablesales=number_format((float)($otherrevenuesales[0]['RubyTottaxe']['total_taxable_sales']), 2, '.', '');
										$taxcollection=number_format((float)($otherrevenuesales[0]['RubyTottaxe']['total_taxes']), 2, '.', '');
										
                                   
                                   
       $html.=	'                              <tr>
                                                    <td>Total Taxable Sales&nbsp;</td>
                                                    <td class="txtrht">$ '.$taxablesales.'&nbsp;</td>                                                </tr>
                                               <tr>
                                                    <td>Total Taxes Collected&nbsp;</td>
                                                    <td class="txtrht">$'.$taxcollection.'&nbsp;</td>                                               </tr>  
                               
                                </table>
	  		<h3>Miscellaneous Summary</h3>
                                <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Miscellaneous Summary</th>        
										   <th align="center">Count	</th>   
										   <th align="center">Value	</th>                           
                                        </tr>';
										$Voids=number_format((float)($RubyMemo1[0]['RubyMemo1']['total_number_of_void_items']), 2, '.', '');
										$valvoid=number_format((float)($RubyMemo1[0]['RubyMemo1']['value_of_void_items_positive_value']), 2, '.', '');
										
                                   $nosale=number_format((float)($RubyMemo1[0]['RubyMemo1']['total_number_of_no_sales']), 2, '.', '');
										$valnosale='';
										
										$safedrop=number_format((float)($RubyPmtout[0]['RubyPmtout']['number_of_safe_drops']), 2, '.', '');
										$valsafedrop=number_format((float)($RubyPmtout[0]['RubyPmtout']['value_of_safe_drops']), 2, '.', '');
										
                                   $error=number_format((float)($RubyMemo1[0]['RubyMemo1']['total_number_of_corrections']), 2, '.', '');
										$valerror=number_format((float)($RubyMemo1[0]['RubyMemo1']['value_of_corrections']), 2, '.', '');
                                   
       $html.=	'                              <tr>
                                                    <td>Voids&nbsp ;</td>
                                                    <td class="txtrht"> '.$Voids.'&nbsp;</td>
													<td class="txtrht"> '.$valvoid.'&nbsp;</td>                                                </tr>
                                               <tr>
                                                    <td>No Sales &nbsp;</td>
                                                    <td class="txtrht">'.$nosale.'&nbsp;</td>
													<td class="txtrht"> '.$valnosale.'&nbsp;</td>                                               </tr>  
                                                <tr>
                                                   <td>Safe Drops&nbsp;</td>
                                                   <td class="txtrht">'.$safedrop.'&nbsp;</td>
												   <td class="txtrht"> '.$valsafedrop.'&nbsp;</td>                                               </tr>  
                                              <tr>
                                                    <td>Error Corrections &nbsp;</td>
                                                     <td class="txtrht">'.$error.'&nbsp;</td>
													 <td class="txtrht"> '.$valerror.'&nbsp;</td>                                              </tr>
                                </table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								
								  <h3>Grocery Sales</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Index</th>        
										   <th align="center">Value</th>                          
                                        </tr>';
										$grostaxablesales=number_format((float)$RubyTottaxe[0]['RubyTottaxe']['total_taxable_sales'], 2, '.', '');
										$grosnontaxablesales=number_format((float)($RubyMemo3[0]['RubyMemo3']['total_merchandise_sales']-$RubyTottaxe[0]['RubyTottaxe']['total_taxable_sales']), 2, '.', '');
										
                                  $grosstotal= number_format((float)$RubyMemo3[0]['RubyMemo3']['total_merchandise_sales'], 2, '.', '');
                                   
       $html.=	'                              <tr>
                                                    <td>Grocery Taxable Sales&nbsp;</td>
                                                    <td class="txtrht">$ '.$grostaxablesales.'&nbsp;</td>                                                </tr>
                                               <tr>
                                                    <td>Grocery Non-Taxable Sales&nbsp;</td>
                                                    <td class="txtrht">$'.$grosnontaxablesales.'&nbsp;</td>                                               </tr>  
												<tr>
                                                    <td>Totals&nbsp;</td>
                                                    <td class="txtrht">$'.$grosstotal.'&nbsp;</td>                                               </tr>  
                               
                               
                                </table>
									<h3>Money Totals</h3>
                                <table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Index</th>        
										   <th align="center">No Of Sales</th>   
										   <th align="center">Value Of Sales</th>                           
                                        </tr>';
										$CASH=$RubyTotmop[0]['RubyTotmop']['number_of_sales'];
										$valCASH=$RubyTotmop[0]['RubyTotmop']['value_of_sales'];
										
                                   $CREDIT=$RubyTotmop[1]['RubyTotmop']['number_of_sales'];
										$valCREDIT=$RubyTotmop[1]['RubyTotmop']['value_of_sales'];
										
										
										$FOODSTAMP=$RubyTotmop[2]['RubyTotmop']['number_of_sales'];
										$valFOODSTAMP=$RubyTotmop[2]['RubyTotmop']['value_of_sales'];
										
                                   $LOTTERY=$RubyTotmop[3]['RubyTotmop']['number_of_sales'];
										$valLOTTERY=$RubyTotmop[3]['RubyTotmop']['value_of_sales'];
										
										
                                   
       $html.=	'                              <tr>
                                                    <td>CASH&nbsp;</td>
                                                    <td class="txtrht"> '.$CASH.'&nbsp;</td>
													<td class="txtrht"> '.$valCASH.'&nbsp;</td>                                                </tr>
                                               <tr>
                                                    <td>CREDIT &nbsp;</td>
                                                    <td class="txtrht">'.$CREDIT.'&nbsp;</td>
													<td class="txtrht"> '.$valCREDIT.'&nbsp;</td>                                               </tr>  
                                                <tr>
                                                   <td>FOODSTAMP&nbsp;</td>
                                                   <td class="txtrht">'.$FOODSTAMP.'&nbsp;</td>
												   <td class="txtrht"> '.$valFOODSTAMP.'&nbsp;</td>                                               </tr>  
                                              
                                              <tr>
                                                    <td>LOTTERY &nbsp;</td>
                                                     <td class="txtrht">'.$LOTTERY.'&nbsp;</td>
													 <td class="txtrht"> '.$valLOTTERY.'&nbsp;</td>                                              </tr>
                              
                                </table>
									  <h3>Credit Card Details</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										  <th align="center">Index</th>        
										   <th align="center">Value</th>                          
                                        </tr>';
										
                                   
       $html.=	'                              <tr>
                                                    <td>No Check Authorization&nbsp;</td>
                                                    <td class="txtrht">$ '.$RubyTotnet[0]['RubyTotnet']['number_of_check_authorizations'].'&nbsp;</td>                                                </tr>
                                               <tr>
                                                   <td>No Of Manual Charges&nbsp;</td>
                                                    <td class="txtrht">$'.$RubyTotnet[0]['RubyTotnet']['number_of_manual_charges'].'&nbsp;</td>                                               </tr>  
												<tr>
                                                  <td>No Of Network Charges&nbsp;</td>
                                                    <td class="txtrht">$'.$RubyTotnet[0]['RubyTotnet']['number_of_network_charges'].'&nbsp;</td>                                               </tr>  
                               					<tr>
                                                   <td>Amount of Network Charges&nbsp;</td>
                                                    <td class="txtrht">$'.$RubyTotnet[0]['RubyTotnet']['amount_of_network_charges'].'&nbsp;</td>                                               </tr>
													<tr>
                                                <td>No Of Network Correction&nbsp;</td>
                                                    <td class="txtrht">$'.$RubyTotnet[0]['RubyTotnet']['number_of_network_corrections'].'&nbsp;</td>                                               </tr>  
                               					<tr>
                                                    <td>Amount Of Network Correction&nbsp;</td>
                                                    <td class="txtrht">$'.$RubyTotnet[0]['RubyTotnet']['amount_of_network_corrections'].'&nbsp;</td>                                               </tr>    
                               
                                </table>
													<br>
								
								<h3>Card</h3>
	  
	     							<table border="1">
                                      <tr style="background:#CCC;">    
										<th align="center">Card Name</th>        
										<th align="center">Card Type Charges</th> 
										<th align="center">No Of Card Charges</th>        
										<th align="center">Amount of Card Charges</th>
										<th align="center">Card Type Corrections</th> 
										<th align="center">No Of Card Correction	</th>        
										<th align="center">Amount of Card Correction</th>                                      </tr>';
										
                         $tcard=  $RubyTotcard[0]['RubyTotcard']['amount_of_card_charges']+$RubyTotcard[1]['RubyTotcard']['amount_of_card_charges']+$RubyTotcard[2]['RubyTotcard']['amount_of_card_charges']+$RubyTotcard[3]['RubyTotcard']['amount_of_card_charges']+$RubyTotcard[4]['RubyTotcard']['amount_of_card_charges']+$RubyTotcard[5]['RubyTotcard']['amount_of_card_charges']; 
						$html.=	'<tr>
						<td>MASTERCARD&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[0]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[0]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[0]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[0]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[0]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[0]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td>
                        </tr>
						<tr>
						<td>VISA&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[1]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[1]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[1]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[1]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[1]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[1]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td>                        </tr>  
						<tr>
						  <td>AMEX&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[2]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[2]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[2]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[2]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[2]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[2]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td> 					 </tr>  
						<tr>
						 <td>DISCOVER&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[3]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[3]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[3]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[3]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[3]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[3]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td>                         </tr>
						<tr>
						 <td>WEX&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[4]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[4]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[4]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[4]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[4]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[4]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td>
						</tr>    
						<tr>
						  <td>VOYAGER&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[5]['RubyTotcard']['card_type_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[5]['RubyTotcard']['number_of_card_charges'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[5]['RubyTotcard']['amount_of_card_charges'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[5]['RubyTotcard']['card_type_corrections'].'&nbsp;</td>
						<td class="txtrht"> '.$RubyTotcard[5]['RubyTotcard']['number_of_card_corrections'].'&nbsp;</td>
						<td class="txtrht">$ '.$RubyTotcard[5]['RubyTotcard']['amount_of_card_corrections'].'&nbsp;</td>               
						 </tr>  
						 
						 <tr>
        <td>Total&nbsp;</td>
        <td class="txtrht">&nbsp;</td>
        <td class="txtrht">&nbsp;</td>        
        <td class="txtrht">$'.$tcard.'  &nbsp;</td>
        <td class="txtrht">&nbsp;</td>
        <td class="txtrht">&nbsp;</td>
        <td class="txtrht">&nbsp;</td>     
        </tr>
						   
                         </table>
	  
	  
		</body>
		</html>';
}
//echo $html; die;

$pdf->writeHTML($html, true, false, true, false, '');
 
$pdf->lastPage();
 
$full_path = WWW_ROOT . 'files/pdf' . DS . $filename;
$pdf->Output($full_path, 'FD');
exit();

