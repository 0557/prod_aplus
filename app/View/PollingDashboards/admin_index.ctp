<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" > 
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                   Sales And Profit Summary		
                </h3>
            
            </div>
        </div>
		<?php echo $this->Form->create('revenue_sales', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
		<div class="row">
         <div class="col-md-12">
            <div class=" col-md-2">Update Date</div>
            <div class="col-md-3">
           		 <?php /*?><input type="text"  placeholder="Select End Date" class="form-control icon_position" id="end_date" name="end_date" value="<?php echo $end_date;?> "> <?php */?> 
                 
            <?php include('test.ctp'); ?>  
            </div>
            <div class=" col-md-1">
            	<button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <div class=" col-md-4">
           		 <a href="<?php echo Router::url('/');?>admin/PollingDashboards/reset" class="btn btn-warning ">Reset</a> 
            </div> 
            </div>
		</div>
<?php echo $this->form->end(); ?>	       
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                    Revenue Sales 					
                </h3>
            
            </div>
        </div>      
         <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">
<div id="loading-image" style="display:none">
           <h3 style="text-align:center"> Loading...</h3>
            </div>
                   
							<?php
							//pr($PollingDashboard);		

							$_summaryTotal = array();				
							foreach($PollingDashboard as $_k => $_value) {
								 $_ukey = @$prodList[$_value['PollingDashboard']['fuel_product_number']];
					
								if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
									$_summaryTotal[$_ukey]['fuel_volume'] = $_value['PollingDashboard']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  = $_value['PollingDashboard']['fuel_value'];
								} else {
									$_summaryTotal[$_ukey]['fuel_volume'] += $_value['PollingDashboard']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  += $_value['PollingDashboard']['fuel_value'];
									}
							}
						
									//pr($_summaryTotal);
									
									 $from_date=$this->Session->read('from_date');
									 $to_date=$this->Session->read('to_date');
										
									
									 if($totaldiff>0) { $numdays=$totaldiff; } else {$numdays=1; }                                      //pr($PollingDashboardss);  
									 $total_gallonsold=0;
									 $total_profit1=0;
									 $total_margin=0;
									 $total_value=0;
									 $piearr=array();
									 $lblarr=array();
									foreach($prodList as $prodLists) {
										$fuel_volme=10; $fuel_val=0; 
										if($prodLists=='Diesel'){                                         
                                        	$prdctID=1;
										}
										elseif($prodLists=='Regular'){
											$prdctID=4;
										}
										elseif($prodLists=='Premium') {
											$prdctID=5;
										}
										else
										{
											$prdctID=6;
										} 
										$storeId = $this->Session->read('stores_id');
										
								
									
										  $results2= ClassRegistry::init('FuelProduct')->query("SELECT `FuelProduct`.cost_per_gallon from `fuel_products` as FuelProduct left join  purchase_invoices as PurchaseInvoice on FuelProduct.r_purchase_invoice_id = PurchaseInvoice.id  WHERE FuelProduct.product_id = '".$prdctID."' AND PurchaseInvoice.receving_date >='".$from_date."' AND PurchaseInvoice.receving_date <='".$to_date."'  ");    
										  $ttl=0;
										  $i=0;
										  foreach($results2 as $res){
											  $cgallon = $results2[$i]['FuelProduct']['cost_per_gallon'];
											 $ttl=$ttl+$cgallon;
											 $i++;
										  }
										// echo $ttl; die;
										  
									if($ttl!=""){
										$avgcost=$ttl/$numdays;
									}else{
										$avgcost=0;
									}
										$fuel_volme=$_summaryTotal[$prodLists]['fuel_volume'];
										 $fuel_val=$_summaryTotal[$prodLists]['fuel_value'];
										 array_push($piearr,$fuel_val);
										 array_push($lblarr,$prodLists);
										 
										 $avgprc=($fuel_val/$fuel_volme)/$numdays;
										  $gallonsoild=$fuel_volme/$numdays;
										  $total_value=$total_value+$fuel_val;
										   $ap= number_format($avgprc ,4);
										   $ac= number_format($avgcost ,4);
										  $dlroildp=$ap*$gallonsoild;
										   $dlroildc=$ac*$gallonsoild;
										   $profit=number_format($dlroildp-$dlroildc,4); 
										$total_profit1=$total_profit1+($dlroildp-$dlroildc);
										$margin=number_format($avgprc-$avgcost,4); 
										$total_margin=$total_margin+($avgprc-$avgcost);
										$total_gallonsold=$total_gallonsold+($gallonsoild);
									 }
									  /* pr($piearr);
									    pr($lblarr);*/
									    ?>                           
						
                         <?php 
						
						$_summaryTotal1 = array();	
									
							foreach($rubyDeptotals as $rubyDeptotal) {
								$kk=$rdepartments[$rubyDeptotal['RubyDeptotal']['department_number']];
								//pr($kk);
								if (array_key_exists($rubyDeptotal['RubyDeptotal']['department_number'],$rdepartments))
								{
								$_summaryTotal1[$kk]['net_sales'] += $rubyDeptotal['RubyDeptotal']['net_sales'];
								}
							}
						$total_sales=array();
						$name=array();
						$mertotal=0;
						foreach($_summaryTotal1 as $key =>$val){
						$mertotal+=$val['net_sales'];
							array_push($total_sales,$val['net_sales']);
							array_push($name,$key);
						}
						$count=count($_summaryTotal1); 
						$_summaryTotal3=array();
						foreach($RubyTotmop as $top){
							$mop=$top['RubyTotmop']['mop_name'];
							if($mop==$top['RubyTotmop']['mop_name']){
								$_summaryTotal3[$mop]['value_of_sales'] += $top['RubyTotmop']['value_of_sales'];	
							}else{
								$_summaryTotal3[$mop]['value_of_sales']=$top['RubyTotmop']['value_of_sales'];	
							}
						}
						$toptotal=0;
						$top_sales=array();
						$mopname=array();
						foreach($_summaryTotal3 as $key3 =>$val3){
							$toptotal+=$val3['value_of_sales'];
							array_push($top_sales,$val3['value_of_sales']);
							array_push($mopname,$key3);
						}
						$count3=count($_summaryTotal3); 
					?>
        <div class="row">
      	<div id="donutchart" class="col-md-4" style="width: 30%; height: 300px;"></div>
         <div id="donutchart1"  class="col-md-4" style="width: 30%; height: 300px;"></div>		 
     <div id="donutchart4"  class="col-md-4" style="width: 30%; height: 300px;"></div>	
        </div>
         <div class="row">
      	<div class="col-md-4"  style="padding-left:70px"><strong>Total Gas Gallons: $<?php echo $total_value ; ?></strong></div>
         <div class="col-md-4" ><strong>Total Merchandise Sales: $<?php echo $mertotal ; ?></strong></div>		 
     <div class="col-md-4" ><strong>Total Method Of Payment: $<?php echo $toptotal ; ?></strong></div>	
        </div>
        <?php 
		
		$total = ClassRegistry::init('SalesbyHour')->query("SELECT `SalesbyHour`.`hours`, `SalesbyHour`.`value_of_merchandise_sales` FROM `ruby_tothrs` AS `SalesbyHour` LEFT JOIN `ruby_headers` AS `RubyHeader` ON (`SalesbyHour`.`ruby_header_id` = `RubyHeader`.`id`) WHERE RubyHeader.ending_date_time >='".$from_date."' AND RubyHeader.ending_date_time <='".$to_date."' AND RubyHeader.store_id=".$storeId."");
	//pr($total); 
		$this->set('total', $total); 
			$_summaryTotal2=array();
			$hours=array();
			$msale=array();
			foreach($total as $val1){
			$jj=$val1['SalesbyHour']['hours'];
			if($jj==$val1['SalesbyHour']['hours']){
			$_summaryTotal2[$jj]['value_of_merchandise_sales'] += $val1['SalesbyHour']['value_of_merchandise_sales'];	
			}else{
				$_summaryTotal2[$jj]['value_of_merchandise_sales']=$val1['SalesbyHour']['value_of_merchandise_sales'];	
			}
			}
			
			foreach($_summaryTotal2 as $key2 =>$val2){
			 $time_in_12_hour_format  = date("g:iA", strtotime($key2.":00"));
				array_push($hours,$time_in_12_hour_format);
				array_push($msale,$val2['value_of_merchandise_sales']);
			}
		$counttotal=count($_summaryTotal2);
		?>
        	  
          <div id="curve_chart" style="width: 100%; height: 500px"></div>
        
                         <div class="tab-content">
                        <div id="tab_1_5" class="tab-pane1">
                            <div class="table-responsive">
                              <table class="table table-striped table-bordered table-advance table-hover" id="getdata">
                                    <thead>
                                        <tr>                                
                                            <th class="header-style">Description</th>
                                            <th class="txtrht header-style">Purchase</th>
                                            <th class="txtrht header-style">Total Sales</th>
                                            <th class="txtrht header-style">Margin </th>
                                            <th class="txtrht header-style">Profit</th>
                                		</tr>
                                    </thead>
                                <tbody>
            					<?php 	
									 $results1= ClassRegistry::init('PurchaseInvoice')->query("SELECT SUM(total_invoice) as cost_of_product from `purchase_invoices`   WHERE receving_date >='".$from_date."' AND receving_date <='".$to_date."'  ");   
										// pr($results1[0][0]['cost_of_product']);
										  $cost_of_product=$results1[0][0]['cost_of_product'];
										  ?>
                                        <tr>
                                        <td><?php echo  "GAS";?></td>
                                        <td class="txtrht"> <?php echo h(number_format((float)($cost_of_product), 4, '.', ''));?>
                                        </td>
                                         <td class="txtrht">
										 <?php echo h(number_format((float)($total_value), 4, '.', ''));?>
                                         </td>
                                        <td class="txtrht">
                                        <?php  $pooling=($total_profit1/$total_gallonsold) ;
					 					echo h(number_format((float)($pooling), 4, '.', ''));?>
										<?php //echo h(number_format((float)($total_margin), 4, '.', ''));?>
                                        </td>
                                        <td class="txtrht">
										<?php echo h(number_format((float)($total_profit1), 4, '.', ''));?>
                                        </td>
                                       </tr>
                                   <?php 
								   $storeId = $this->Session->read('stores_id');			
										 $results3= ClassRegistry::init('PurchaseInvoice')->query("SELECT SUM(gross_amount) as cost_of_product from `r_grocery_invoices` WHERE invoice_date >='".$from_date."' AND invoice_date <='".$to_date."' AND store_id=".$storeId."");   
										  $cost_of_product1=$results3[0][0]['cost_of_product'];
										
										  ?>
                                          <tr>                                          
										  <?php 
                                            $total_selling_price='';
                                            $total_current_cost='';
                                            $total_no_of_items_sold='';
                                            $total_dlllar_sold='';
                                            $total_dlllar_cost='';
                                            $total_total_profit='';		
                                                
                                            foreach ($pluttls as $rubyDeptotal){ 
                                            $storeId = $this->Session->read('stores_id');
                                            $PurchasePack = ClassRegistry::init("PurchasePack")->find('first', array('conditions' => array('PurchasePack.Item_Scan_Code' =>$rubyDeptotal['RubyPluttl']['plu_no'],'PurchasePack.store_id' =>$storeId), 'fields' => array('PurchasePack.id', 'PurchasePack.Unit_per_cost')));
										//	pr($PurchasePack);
                                            if($PurchasePack['PurchasePack']['Unit_per_cost']!=""){
                                            $current_cost=$PurchasePack['PurchasePack']['Unit_per_cost'];
                                            }
                                            else{
                                                $current_cost="0.00";
                                            }
                                            
                                            $total_current_cost=$total_current_cost+$current_cost;
                                            $total_no_of_items_sold=$total_no_of_items_sold+$rubyDeptotal['RubyPluttl']['no_of_items_sold'];
                                            
                                            
                                            $dlllar_sold=$rubyDeptotal['RubyPluttl']['selling_price']*$rubyDeptotal['RubyPluttl']['no_of_items_sold'];
                                            $total_dlllar_sold=$total_dlllar_sold+$dlllar_sold;	
                                            
                                            
                                            $dlllar_cost= $current_cost* $rubyDeptotal['RubyPluttl']['no_of_items_sold'];	
                                            $total_dlllar_cost=$total_dlllar_cost+$dlllar_cost;
                                            $total_profit=$dlllar_sold - $dlllar_cost;
                                            $total_total_profit=$total_total_profit+$total_profit;	
                                            }; ?>
                                          
                                        <td><?php echo  "Cstore";?></td>
                                        <td class="txtrht"> <?php echo h(number_format((float)($cost_of_product1), 4, '.', ''));?>
                                        </td>
                                         <td class="txtrht">
										 <?php echo h(number_format((float)($total_dlllar_sold), 4, '.', ''));?>
                                         </td>
                                        <td class="txtrht">
                                        <?php  $mar=($total_total_profit/$total_dlllar_sold) ;
										 echo h(number_format((float)($mar), 4, '.', ''));?>
                                        </td>
                                        <td class="txtrht">
										<?php echo h(number_format((float)($total_total_profit), 4, '.', ''));?>
                                        </td>
                                     
                                       </tr> 
                                       <tr>
                                       <td ><b>Total</b></td> 
                                       <td>&nbsp;</td> 
                                       <td>&nbsp;</td> 
                                       <td>&nbsp;</td> 
                                       <td class="txtrht"><?php $finalprof=$total_total_profit+$total_profit1 ?>
                                       <b>$<?php echo h(number_format((float)($finalprof), 4, '.', ''));?></b>
                                       </td> 
                                                   
                                 </tbody>
                                 
                              </table>
                            </div>                         
                        </div>                    
                    </div>
                </div>
            </div>
            
            
             
                      
        </div>
       			
  
        
        </div>
        
            
      </div>
   </div>
  
</div>
<style>
.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }
button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": false
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
		$('#end_date').Zebra_DatePicker({format:"Y-m-d"});
	});
</script>


 <?php 
$full_date = $this->Session->read('full_date'); 
if($full_date=='')
{
?>        
<script>
$(document).ready(function(){
$('#config-demo').val('');		
});
</script>
<?php 
}
?> 
  
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['<?php echo $lblarr[0]; ?>',<?php echo $piearr[0]; ?>],
          ['<?php echo $lblarr[1]; ?>',<?php echo $piearr[1]; ?>],
          ['<?php echo $lblarr[2]; ?>',<?php echo $piearr[2]; ?>],
          ['<?php echo $lblarr[3]; ?>',<?php echo $piearr[3]; ?>],
        ]);

        var options = {
          title: 'Gas Sales',
          pieHole: 0.4,
		  legend: 'none',
		  chartArea: {width: "50%"},
		  //pieSliceText: 'label',
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php for($i=0;$i<$count;$i++){ ?>
          ['<?php echo $name[$i]; ?>',<?php echo $total_sales[$i]; ?>],
		  <?php } ?>
       
        ]);

        var options = {
          title: 'Merchandise Sales',
          pieHole: 0.4,
		  legend: 'none',
		  chartArea: {width: "50%"},
		  //pieSliceText: 'label',
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart1'));
        chart.draw(data, options);
      }
    </script> 
    
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php for($i=0;$i<$count3;$i++){ ?>
          ['<?php echo $mopname[$i]; ?>',<?php echo $top_sales[$i]; ?>],
		  <?php } ?>
       
        ]);

        var options = {
          title: 'Method Of Payment',
          pieHole: 0.4,
		  legend: 'none',
		   chartArea: {width: "50%"},
		  //pieSliceText: 'label',
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart4'));
        chart.draw(data, options);
      }
    </script>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Time', "Sales", {type: 'string', role: 'tooltip'}],
           <?php for($k=0;$k<$counttotal;$k++){ ?>
         ['<?php echo $hours[$k]; ?>',<?php echo $msale[$k]; ?>,"<?php echo $hours[$k]; ?> Sales's $<?php echo $msale[$k]; ?>"],
		<?php } ?>
        ]);
        var options = {
          title: 'Sale By Hours',
		   tooltip: {textStyle:  {fontName: 'Arial',fontSize: 12,bold: true}},
          curveType: 'function',
          legend: { position: 'bottom' },
		  chartArea: {width: "90%"},
		 hAxis : { 
        textStyle : {
            fontSize: 12,
			
        // or the number you want
        } ,
		slantedText: true, 
		
    },legend: 'none',
          /*hAxis: { minValue: 0, maxValue: 9 },*/
          curveType: 'function',
          pointSize: 7,
          //dataOpacity: 0.3
        };

         var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

  