<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" > 
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                    Pooling Margin Report			
                </h3>
            
            </div>
        </div>
		<?php echo $this->Form->create('revenue_sales', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
		<div class="row">
         <div class="col-md-12">
            <div class=" col-md-2">Update Date</div>
            <div class="col-md-3">
           		 <?php /*?><input type="text"  placeholder="Select End Date" class="form-control icon_position" id="end_date" name="end_date" value="<?php echo $end_date;?> "> <?php */?> 
                 
            <?php include('test.ctp'); ?>  
            </div>
            <div class=" col-md-1">
            	<button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <?php echo $this->form->end(); ?>	 
            <div class=" col-md-2">
           		 <a href="<?php echo Router::url('/');?>admin/revenue_sales/reset" class="btn btn-warning ">Reset</a> 
            </div> 
            <?php echo $this->Form->create('PollingMarginreport',array('action' => 'create_pdf'), array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
              <div class=" col-md-2">
              <input type="hidden" id="config-demo" name="data[RGroceryInvoice][full_date]" class="form-control" value="<?php echo $full_date; ?>">	
           		 <button  class="btn btn-success search_button" type="submit">Create PDF</button>
            </div> 
             <?php echo $this->form->end(); ?>
            </div>
		</div>
      
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                    Revenue Sales 					
                </h3>
            
            </div>
        </div>      
         <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">
<div id="loading-image" style="display:none">
           <h3 style="text-align:center"> Loading...</h3>
            </div>
                    <div class="tab-content">
                        <div id="tab_1_5" class="tab-pane1">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover" id="getdata">
                                    <thead>
                                        <tr>                                
                                            <th class="header-style">Grade</th>
                                            <th class="txtrht header-style">Avg Price($)</th>
                                            <th class="txtrht header-style">Avg Cost($)</th>
                                            <th class="txtrht header-style">Gallon Sold</th>
                                            <th class="txtrht header-style">Dollar sold by Price($)</th>
                                            <th class="txtrht header-style">Dollar sold by Cost($)</th>
                                            <th class="txtrht header-style">Profit ($) </th>
                                            <th class="txtrht header-style">Margin($)</th>                                		</tr>
                                    </thead>
                                <tbody>
                                				
							<?php
									

							$_summaryTotal = array();				
							foreach($PollingMarginreports as $_k => $_value) {
								$_ukey = @$prodList[$_value['PollingMarginreport']['fuel_product_number']];
						
								if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
									$_summaryTotal[$_ukey]['fuel_volume'] = $_value['PollingMarginreport']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  = $_value['PollingMarginreport']['fuel_value'];
								} else {
									$_summaryTotal[$_ukey]['fuel_volume'] += $_value['PollingMarginreport']['fuel_volume'];
									$_summaryTotal[$_ukey]['fuel_value']  += $_value['PollingMarginreport']['fuel_value'];
									}
							}
									
				//pr($PollingMarginreports);				
									
									
									
									
									
									 $from_date=$this->Session->read('from_date');
									 $to_date=$this->Session->read('to_date');
										
									
									 if($totaldiff>0) { $numdays=$totaldiff; } else {$numdays=1; }                                      //pr($PollingMarginreports);  
									 $total_gallonsold=0;
									 $total_profit=0;
									 
									foreach($prodList as $prodLists) {
										$fuel_volme=10; $fuel_val=0; ?>
                                        <tr>
                                        <td><?php echo $prodLists;?></td>
                                        
										<?php 
										if($prodLists=='Diesel'){                                         
                                        	$prdctID=1;
										}
										elseif($prodLists=='Regular'){
											$prdctID=4;
										}
										elseif($prodLists=='Premium') {
											$prdctID=5;
										}
										else
										{
											$prdctID=6;
										} 
										$storeId = $this->Session->read('stores_id');
										
										/*echo "SELECT `FuelProduct`.cost_per_gallon from `fuel_products` as FuelProduct left join  purchase_invoices as PurchaseInvoice on FuelProduct.r_purchase_invoice_id = PurchaseInvoice.id  WHERE FuelProduct.product_id = '".$prdctID."' AND PurchaseInvoice.load_date >='".$from_date."' AND PurchaseInvoice.load_date <='".$to_date."'  "; die;*/
										  $results2= ClassRegistry::init('FuelProduct')->query("SELECT `FuelProduct`.cost_per_gallon from `fuel_products` as FuelProduct left join  purchase_invoices as PurchaseInvoice on FuelProduct.r_purchase_invoice_id = PurchaseInvoice.id  WHERE FuelProduct.product_id = '".$prdctID."' AND PurchaseInvoice.receving_date >='".$from_date."' AND PurchaseInvoice.receving_date <='".$to_date."'  ");    
										  $ttl=0;
										  $i=0;
										  foreach($results2 as $res){
											  $cgallon = $results2[$i]['FuelProduct']['cost_per_gallon'];
											 $ttl=$ttl+$cgallon;
											 $i++;
										  }
										// echo $ttl; die;
										  
									if($ttl!=""){
										$avgcost=$ttl/$numdays;
									}else{
										$avgcost=0;
									}
										$fuel_volme=$_summaryTotal[$prodLists]['fuel_volume'];
										 $fuel_val=$_summaryTotal[$prodLists]['fuel_value'];
										 $avgprc=($fuel_val/$fuel_volme)/$numdays;
										 $gallonsoild=$fuel_volme/$numdays;
										?>
			
                                 
                                  
                                  
                              
                                        <td class="txtrht"><?php  echo $ap= number_format($avgprc ,4); //echo $fuel_volme; die;?></td>
                                         <td class="txtrht"><?php echo $ac= number_format($avgcost ,4); ?></td>
                                        <td class="txtrht">
										<?php 
										echo number_format($fuel_volme/$numdays ,4); 
										$total_gallonsold=$total_gallonsold+($gallonsoild);
								
										
										?></td>
                                        <td class="txtrht">
										<?php 
							
										 	$dlroildp=$ap*$gallonsoild;
										 	echo number_format($dlroildp,4); 
										 ?></td>
                                        <td class="txtrht">
										<?php
									   $dlroildc=$ac*$gallonsoild;
										 		 echo number_format($dlroildc,4);
										   ?></td>
                                        <td class="txtrht"> 
										<?php echo $profit=number_format($dlroildp-$dlroildc,4); 
										$total_profit=$total_profit+($dlroildp-$dlroildc);
										
										?></td> 
                                        <td class="txtrht"> <?php echo $margin=number_format($avgprc-$avgcost,4); ?></td> 
                                       </tr>
                                       <?php } ?>                            
                                 </tbody>
                                 <tfoot>
                    <tr>
                        <td class="header-style"><b style="font-size:16px;">Total</b></td> 
                        <td class="header-style"><b>&nbsp;</b></td> 
                        <td class="header-style"><b>&nbsp;</b></td> 
                        <td class="txtrht" style="border-top: 2px solid #000 !important;"><strong style="font-size:16px;"><?php echo h(number_format((float)($total_gallonsold), 4, '.', ''));?></strong></td>  
                   
                       <td class="header-style"><b>&nbsp;</b></td> 
                       <td class="header-style"><b>&nbsp;</b></td> 
                   <td class="txtrht" style="border-top: 2px solid #000 !important;"><strong style="font-size:16px;"><?php echo h(number_format((float)($total_profit), 4, '.', ''));?></strong></td> 
                       <td class="header-style"><b>&nbsp;</b></td> 
                  
                    </tr>
					 </tfoot>
                              </table>
                            </div>                         
                        </div>                    
                    </div>
                </div>
            </div>          
        </div>
        
           <div class="row">
            <div class="col-md-12">               
                <h4 class="txtrht">
                   <strong> Pool Margin= $<?php  $pooling=($total_profit/$total_gallonsold) ;
					 echo h(number_format((float)($pooling), 4, '.', ''));?>
                     </strong>					
                </h4>
            
            </div>
        </div> 
      </div>
   </div>
</div>
<style>
.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }
button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": false
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
		$('#end_date').Zebra_DatePicker({format:"Y-m-d"});
	});
</script>
 <?php 
$full_date = $this->Session->read('full_date'); 
if($full_date=='')
{
?>        
<script>
$(document).ready(function(){
$('#config-demo').val('');		
});
</script>
<?php 
}
?>