<?php
$gemcmds = array(
	'vMaintenance^dataset=All' => 'Synchronized All Dataset',
	'vMaintenance^dataset=Item' => 'Synchronized PLUS',
	'vMaintenance^dataset=MerchandiseCode' => 'Synchronized Departments',
	'vMaintenance^dataset=TankProduct' => 'Synchronized TankProduct',
	'vreportpdlist' => 'vreportpdlist',
	'vrubyrept^reptname=tierProduct^period=2^filename=current'		=> 'Fuel Sale',
	'vrubyrept^reptname=department^period=2^filename=current'		=> 'Department Sale',
	'vrubyrept^reptname=plu^period=2^filename=current'		=> 'Current PLU Sale',
	'vrubyrept^reptname=plu^period=2^reptnum=2'		=> 'PLU Sale reptnum=2',
	'vrubyrept^reptname=tank^period=2^filename=current'		=> 'Tank Report',
	'vrubyrept^reptname=tankMonitor^period=2^filename=current'		=> 'TankMonitor',
	'GET ALL TOTALS ftotal12'		=> 'GET ALL TOTALS ftotal12',
	'GET ALL DATA fmop'		=> 'Ruby ==> GET ALL DATA fmop',
	'GET ALL DATA fprod'		=> 'Ruby ==> GET ALL DATA fprod',
	'GET ALL DATA fservlev' => 'Ruby ==> GET ALL DATA fservlev',
	'GET ALL DATA plu' => 'Ruby ==> GET ALL DATA plu',
	'GET ALL DATA ftank' => 'Ruby ==> GET ALL DATA ftank',
	'GET ALL DATA dep' => 'Ruby ==> GET ALL DATA dep',
	'GET ALL TOTALS plutot22' => 'GET ALL TOTALS plutot22',
	'GET ALL TOTALS sumtot12' => 'GET ALL TOTALS sumtot12'
	
	
	
	
	
);
$hours = array();
for($ii=1; $ii<=24;$ii++) {
	$hours["+$ii Hour"] = "Every $ii hours";
}
	$hours["+1 week"] = "Every week";
?>

<div class="page-content">
    <div class="row">	
	<?php echo $this->Form->create('Posrequester'); ?>
		<fieldset>
			<legend><?php echo __('Add Posrequester'); ?></legend>
		<?php
			echo $this->Form->input('store_id');
			echo $this->Form->input('cmd', array('options' => $gemcmds));
			echo $this->Form->input('data');
			echo $this->Form->input('recursive');
			echo $this->Form->input('after_hour', array('options' => $hours));
			//echo $this->Form->input('status', array(''));
			echo $this->Form->hidden('active', array('value' => 1));
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
	</div>
</div>