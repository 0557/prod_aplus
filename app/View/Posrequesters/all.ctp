<div class="page-content">
    <div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('store_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cmd'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('recursive'); ?></th>
			<th><?php echo $this->Paginator->sort('after_hour', array('label' => 'After')); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($posrequesters as $posrequester): ?>
	<tr>
		<td><?php echo h($posrequester['Posrequester']['id']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['store_id']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['cmd']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['status']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['recursive']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['after_hour']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['active']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['created']); ?>&nbsp;</td>
		<td><?php echo h($posrequester['Posrequester']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $posrequester['Posrequester']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $posrequester['Posrequester']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $posrequester['Posrequester']['id']), null, __('Are you sure you want to delete # %s?', $posrequester['Posrequester']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
</div>	
</div>	