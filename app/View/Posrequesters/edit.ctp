<?php
$gemcmds = array(
	'vMaintenance&amp;dataset=Item' => 'Synchronized PLUS',
	'vMaintenance&amp;dataset=MerchandiseCode' => 'Synchronized Departments',
	'vMaintenance&amp;dataset=TankProduct' => 'Synchronized TankProduct',
	'vreportpdlist' => 'vreportpdlist',
	'vrubyrept&amp;reptname=tierProduct&amp;period=2&amp;filename=current'		=> 'vrubyrept'
);
$hours = array();

for($ii=5; $ii<=60; $ii++) {
	$hours["+$ii minutes"] = "Every $ii minutes";
	$ii = $ii+4;
}

for($ii=1; $ii<=24;$ii++) {
	$hours["+$ii Hour"] = "Every $ii hours";
}
	$hours["+1 week"] = "Every week";
?>
<div class="page-content">
    <div class="row">
	<?php echo $this->Form->create('Posrequester'); ?>
		<fieldset>
			<legend><?php echo __('Edit Posrequester'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('cmd');
			echo $this->Form->input('data');
			echo $this->Form->input('recursive');
			echo $this->Form->input('after_hour', array('options' => $hours));
			echo $this->Form->input('status');
			echo $this->Form->input('active');
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
	</div>
</div>