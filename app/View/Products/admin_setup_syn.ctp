<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Seup & Sync Pricebook
							</div>
							
						</div>
						<div class="portlet-body">
								<?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                     
					   <div class="row">
		  <div class="col-md-12">
<div class="col-md-4">
			 <label>Select Category</label>
		<?php 
		$catlist = array('Beer'=>'Beer','Cakes/Pastries'=>'Cakes/Pastries','Candy'=>'Candy','Chips'=>'Chips','Cigarette'=>'Cigarette','Cigarette Cartons'=>'Cigarette Cartons','Grocery (EDBL)'=>'Grocery (EDBL)','GUM AND MINTS'=>'GUM AND MINTS','Household'=>'Household','Instant Lottery'=>'Instant Lottery','Milk / Dairy'=>'Milk / Dairy','Packaged Beverages'=>'Packaged Beverages','Packaged Ice Cream'=>'Packaged Ice Cream','Tobacco'=>'Tobacco','Wine'=>'Wine','Auto Oil'=>'Auto Oil','Cake'=>'Cake');
		echo $this->Form->input('cat', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => true, 'id' =>'department','options'=>$catlist,'empty'=>'search by category')); ?>
		<br/>
		</div>
		
		<div class="col-md-3 pull-right">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('search', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
							
            <?php echo $this->form->end(); ?>
			
			<hr/>
			  <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
							
								<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
								<th><?php echo $this->Paginator->sort('Description'); ?></th>
								<th><?php echo $this->Paginator->sort('Category'); ?></th>
								<th><?php echo $this->Paginator->sort('Cost'); ?></th>
								<th>
								<input type="checkbox" id="selecctall"/></th>
								
						</tr>
						</thead>
						<tbody>
						<?php if (isset($Globalupcslist) && !empty($Globalupcslist)) { ?>
                                            <?php foreach ($Globalupcslist as $data) { ?>
						<tr>
										
                                                    <td><?php echo h($data['Globalupcs']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['description']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['category']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Globalupcs']['cost']); ?>&nbsp;</td>
                                                 <td>
                                                     
                                                   <input type="checkbox" name="import[]" value="<?php echo h($data['Globalupcs']['id']); ?>"/>
                                                       
                                                    </td>
                                                  
							
						</tr>
					<?php } }?>
						</tbody>
						</table>
                            </div>
<span id="postimport" class="btn green">
                        Import Selected items    
						</span>
						</div> </div> 

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({"pageLength": 100});
		
		$('#saledate').Zebra_DatePicker({format:"m-d-Y"});

$('#selecctall').click(function(event) {  
	 $("input:checkbox").prop('checked', $(this).prop("checked"));
       
    });
});
</script>

