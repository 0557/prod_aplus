<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>  
<?php //pr($rGroceryItem);
?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Grocery Item Information -
                    </div>

                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">


                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Department Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rGroceryItem['RGroceryDepartment']['name']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            PLU No:
                                                        </div>
                                                        <div class="col-md-7 value">
<?php echo $rGroceryItem['RGroceryItem']['plu_no']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Description :  
                                                        </div>
                                                        <div class="col-md-7 value">
<?php echo $rGroceryItem['RGroceryItem']['description']; ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Price :	 
                                                        </div>
                                                        <div class="col-md-7 value">
<?php echo $rGroceryItem['RGroceryItem']['price']; ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Fee/Charge:	 
                                                        </div>
                                                        <div class="col-md-7 value">
<?php echo $rGroceryItem['RGroceryItem']['fee']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            PLU in promotion:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="switch-left switch-primary"></span>
                                                            <span class="switch-right switch-info "></span>
<?php echo $this->Form->input('plu_promotion', array('class' => 'make-switch',  'disabled' => true, 'div' => false,'checked'=>($rGroceryItem['RGroceryItem']['plu_promotion'] == 0)?false:true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            PLU may be Sold:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="switch-left switch-primary"></span>
                                                            <span class="switch-right switch-info "></span>
<?php echo $this->Form->input('plu_sold', array('class' => 'make-switch', 'disabled' => true, 'div' => false,'checked'=>($rGroceryItem['RGroceryItem']['plu_sold'] == 0)?false:true ,  'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            PLU may be Return:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="switch-left switch-primary"></span>
                                                            <span class="switch-right switch-info "></span>
<?php echo $this->Form->input('plu_return', array('class' => 'make-switch', 'disabled' => true, 'data-on' => "primary",'checked'=>($rGroceryItem['RGroceryItem']['plu_return'] == 0)?false:true ,  'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Sell Unit :
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rGroceryItem['RGroceryItem']['unit']; ?>
                                                        </div>

                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           Margin % :
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rGroceryItem['RGroceryItem']['margin']; ?>
                                                        </div>

                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           Profit Amount :
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            $<?php echo $rGroceryItem['RGroceryItem']['profit']; ?>
                                                        </div>

                                                    </div>

                                                </div></div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
</div>