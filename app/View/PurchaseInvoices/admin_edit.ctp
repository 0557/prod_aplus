<?php error_reporting(0); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers', 'admin/bootstrap-fileinput')); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers')); ?>
<?php //echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers', 'admin/components-pickers'));             ?>
<?php echo $this->Html->css(array('admin/datetimepicker')); ?>
<?php echo $this->Form->create('PurchaseInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Edit Fuel Purchase INVOICE
                        </div>
                        <!--<div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="" class="reload">
                            </a>
                            <a href="" class="remove">
                            </a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="form-body col-md-12 boderstyle">
                <div class="row">

                    <div class="col-md-6 col-sm-12 ">
                        <div class="portlet yellow box">

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Order Details
                                </div>
                                <div class="actions">
                                    <!--<a href="#" class="btn default btn-sm">
                                            <i class="fa fa-pencil"></i> Edit
                                    </a>-->
                                </div>
                            </div>
                            <div class="portlet-body extra_tt">
                                <div class="row static-info">
                                    <div class="col-md-2 ">
                                        <label>BOL#:</label>
                                        <?php echo $this->Form->input('bol', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Bol')); ?>
                                        <?php echo $this->Form->input('id'); ?>
                                    </div>

                                    <div class="col-md-4 ">
                                        <label>Load Date:</label>
                                        <?php echo $this->Form->input('load_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'value' => date("Y-m-d", strtotime($this->request->data['PurchaseInvoice']['load_date'])))); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Receving Date:</label>
                                        <?php echo $this->Form->input('receving_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'value' => date("Y-m-d", strtotime($this->request->data['PurchaseInvoice']['receving_date'])))); ?>
                                    </div>
                                    <div class="col-md-2">
                                        <label>PO#:</label>

                                        <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Po')); ?>

                                    </div>
                                </div>


                                <div class="row static-info">
                                    <div class="col-md-3">
                                        <label>Carrier:</label>			 

                                        <?php echo $this->Form->input('carrier', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Carrier')); ?>

                                    </div>
                                    <div class="col-md-3">
                                        <label>FEIN:</label>			 

                                        <?php echo $this->Form->input('ship_via', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter FEIN')); ?>

                                    </div>
                                    <div class="col-md-3">
                                        <label> Status:</label>	 

                                        <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>

                                    </div>
                                    <div class="col-md-3">
                                        <label> MOP:</label>		 

                                        <?php echo $this->Form->input('PurchaseInvoice.mop', array('class' => 'select_list_mop form-control', 'onchange' => "mop_function(this.value)", 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'Check', 'Cash Paid' => 'Cash Paid'))); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet blue box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Customer Information
                                </div>
                                <div class="actions">

                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">

                                    <div class="col-md-3">
                                        <label>Supplier:</label>

                                        <?php echo $this->Form->input('supplier_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $wholesale_supplier, 'empty' => 'Select Supplier')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Export Supplier:</label>	 

                                        <?php echo $this->Form->input('export_supplier', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Export Suplier')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Terminal:</label>

                                        <?php echo $this->Form->input('terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Terminal')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Export Terminal:</label>

                                        <?php echo $this->Form->input('export_terminal', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Export Terminal')); ?>
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-3">
                                        <label>Driver:</label>

                                        <?php echo $this->Form->input('driver', array('label' => false, 'required' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Driver')); ?>
                                    </div>
                                    <div class="col-md-9 ">
                                        <label>Comments:</label>

                                        <?php echo $this->Form->textarea('comments', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Comments')); ?>
                                    </div>

                                </div>

                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Upload:
                                    </div>
                                    <div class="col-md-7">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename">
                                                    </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new">
                                                        Select file
                                                    </span>
                                                    <span class="fileinput-exists">
                                                        Change
                                                    </span>
    <!--														<input type="file" name="files">-->

                                                    <?php echo $this->Form->input('files', array('type' => 'file', 'div' => false, 'label' => false, 'required' => 'false')); ?>

                                                    <?php echo $this->Form->input('oldfiles', array('type' => 'hidden', 'div' => false, 'label' => false, 'value' => $this->request->data['PurchaseInvoice']['files'], 'id' => 'removes', 'required' => 'false')); ?>

                                                </span>
                                                <a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove
                                                </a>
                                            </div>
                                            <?php if ($this->request->data['PurchaseInvoice']['files']) { ?>
                                                <div>
                                                    <a download="" target="_blank" href="<?php echo Router::url('/'); ?>files/<?php echo $this->request->data['PurchaseInvoice']['files']; ?>"><i class="fa fa-download"></i>
                                                    </a>
                    <!--<a target="_blank" href="https://docs.google.com/gview?url=http://aplus1.net<?php echo Router::url('/'); ?>files/<?php echo $this->request->data['PurchaseInvoice']['files']; ?>"><i class="fa fa-eye"></i></a> -->

                                                    <span onclick="removefunction(this.id)" id="remove"><i class="fa fa-trash-o"></i></span></div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>


                            </div></div></div>

                </div>




                <!-- END FORM-->
            </div>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 boderstyle">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Fuel Purchase Products
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body" style="overflow:hidden;">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped add">
                                <thead>
                                    <tr>
                                        <th>
                                            Products
                                        </th>
                                        <th>
                                            <!-- Vendor Price-->
                                            Gallons Delivered
                                        </th>
                                        <th>
                                            <!-- Tax Class-->
                                            Cost Per Gallons

                                        <th>
                                            Net Amount
                                        </th>


                                    </tr>
                                </thead>
                                <tbody id="products_add_more">
                                    <?php
                                    //pr($this->request->data['FuelProduct']);
                                    $key = 0;
                                    $k = 0;
                                    foreach ($product as $key1 => $pro) {
                                        //pr($product);
                                        ?>
                                        <tr>
                                            <td><?php echo $pro; ?>
                                                <?php echo $this->Form->input('FuelProduct.product_id.', array('label' => false, 'size' => 8, 'class' => 'form-control', 'type' => 'hidden', 'placeholder' => '0', 'value' => $key1)); ?>
                                            </td>
                                            <td style="background-color: rgb(249, 249, 249); " class="max_open" ><?php echo $this->Form->input('FuelProduct.gallons_delivered.', array('label' => false, 'size' => 8, 'class' => 'form-control chk gallon', 'type' => 'text', 'value' => $this->request->data['FuelProduct'][$key]['gallons_delivered'], 'placeholder' => '0', 'id' => 'Gallons_Delivered_' . $k)); ?>
                                            </td>
                                            <td class="enter_product_quantity" ><?php echo $this->Form->input('FuelProduct.cost_per_gallon.', array('class' => 'form-control chk product', 'type' => 'text', 'label' => false, 'id' => '', 'value' => $this->request->data['FuelProduct'][$key]['cost_per_gallon'], 'required' => 'false', 'placeholder' => '0', 'id' => 'Cost_Per_Gallon_' . $k)); ?></td>
                                            <td class="net_amount"><?php echo $this->Form->input('FuelProduct.net_ammount.', array('label' => false, 'class' => 'form-control', 'value' => $this->request->data['FuelProduct'][$key]['net_ammount'], 'type' => 'text', 'readonly' => true, 'id' => 'Net_Amount_' . $k)); ?></td>

                                        </tr>
                                        <?php
                                        $key++;
                                        $k++;
                                    }
                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-4">
                                <label>Gallon Delivered Total</label>
                                <?php echo $this->Form->input('max_qnty', array('label' => false, 'value' => (isset($this->request->data['PurchaseInvoice']['max_qnty'])) ? $this->request->data['PurchaseInvoice']['max_qnty'] : '0', 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                            </div>
                            <div class="col-md-4">
                                <label>Net Amount Total</label>
                                <?php echo $this->Form->input('total_invoice', array('label' => false, 'value' => (isset($this->request->data['PurchaseInvoice']['total_invoice'])) ? $this->request->data['PurchaseInvoice']['total_invoice'] : '0', 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
                            </div>
                            <div class="col-md-4">
                                <label>Taxes</label>
                                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['PurchaseInvoice']['taxes'])) ? $this->request->data['PurchaseInvoice']['taxes'] : '0', 'onblur' => "getvalue()", 'class' => 'form-control chk', 'type' => 'text', 'placeholder' => '0', 'onkeyup' => 'getvalue()')); ?></span>
                            </div>

                        </div>

                        <div  class="row">
                            <div class="col-md-4">
                                <label>Gross Amount</label>
                                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('gross_amount', array('label' => false, 'value' => (isset($this->request->data['PurchaseInvoice']['gross_amount'])) ? $this->request->data['PurchaseInvoice']['gross_amount'] : '0', 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?></span>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>  
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>


</div>
</div>

<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('FuelProduct.product_id.', array('class' => 'form-control exampleInputName', 'empty' => 'Select Product', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', /* 'multiple' => true, */ /* 'multiple' => 'checkbox', */ "options" => $product)); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px">&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>
</table>
<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->

<script>
    $(document).ready(function () {

        var Gallons_Delivered = 0;
        var Cost_Per_Gallon = 0;
        var Net_Amount = 0;
        var tax = 0;
        var gross = 0;
        var grosstotal = 0;
        jQuery(".chk").bind("keyup", function (e) {
            var Gallons_Delivered_Total = 0;
            var Net_Amount_Total = 0;
            var count_gallons = 0;
            jQuery('.gallon').each(function () {
                Gallons_Delivered = parseFloat(jQuery('#Gallons_Delivered_' + count_gallons).val());

                if (Gallons_Delivered == null || Gallons_Delivered == '' || isNaN(Gallons_Delivered)) {
                    Gallons_Delivered = 0;
                }
                Cost_Per_Gallon = parseFloat(jQuery('#Cost_Per_Gallon_' + count_gallons).val());
                //alert(Cost_Per_Gallon);
                if (Cost_Per_Gallon == null || Cost_Per_Gallon == '' || isNaN(Cost_Per_Gallon)) {
                    Cost_Per_Gallon = 0;
                }
                Net_Amount = parseFloat(Gallons_Delivered) * parseFloat(Cost_Per_Gallon);
                jQuery('#Net_Amount_' + count_gallons).val(Net_Amount);

                count_gallons++;
                Gallons_Delivered_Total += parseFloat(Gallons_Delivered);
                Net_Amount_Total += parseFloat(Net_Amount);



            })
            tax = $('#PurchaseInvoiceTaxes').val();
            if (tax == null || tax == '') {
                tax = 0;
            }
            Gross_Amount = parseFloat(Net_Amount_Total + parseFloat(tax));

            //alert("Net_Amount_Total : "+Gross_Amount);
            jQuery('#PurchaseInvoiceMaxQnty').val(Gallons_Delivered_Total);
            jQuery('#PurchaseInvoiceTotalInvoice').val(Net_Amount_Total);
            jQuery('#PurchaseInvoiceGrossAmount').val(Gross_Amount);


        })


    });
    function mop_function(value) {
        $('#MOP').text(value);
    }

    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }
    $(document).ready(function () {
        // initiate layout and plugins
        //App.init();
        var selected_mop = '<?php echo $this->request->data['PurchaseInvoice']['mop'] ?>';
        $('#MOP').text(selected_mop);
        ComponentsPickers.init();



    });


    /*    function getvalue() {
     var Gross_Amount = 0;
     var max_qnty = 0;
     var max_total = 0;
     var tax = 0;
     
     
     $('td.net_amount').each(function () {
     max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).find('input').val());
     // max_qnty = parseFloat(max_qnty) + parseFloat(jQuery(this).text());
     });
     if (max_qnty == null || max_qnty == '' || isNaN(max_qnty)) {
     
     max_qnty = 0;
     }
     
     $('td.max_open').each(function () {
     max_total = parseFloat(max_total) + parseFloat(jQuery(this).find('input').val());
     
     if (max_total == null || max_total == '' || isNaN(max_total)) {
     
     max_total = 0;
     }
     //max_total = parseFloat(max_total) + parseFloat(jQuery(this).text());
     });
     tax = $('#PurchaseInvoiceTaxes').val();
     if (tax == null || tax == '') {
     tax = 0;
     }
     Gross_Amount = parseFloat(max_qnty + parseFloat(tax));
     
     
     // $('#PurchaseInvoiceMaxQnty').val(max_total);
     
     $('#PurchaseInvoiceTotalInvoice').val(max_qnty);
     $('#PurchaseInvoiceGrossAmount').val(Gross_Amount);
     
     }
     
     
     function quantity_amount() {
     var product_amount = 0;
     var total_amt_final = 0;
     var max_open = 0;
     var max_open_ttl=0;
     
     $('td.max_open').each(function () {
     
     max_open = parseFloat(jQuery(this).find('input').val());
     if(max_open == null || max_open == '' || isNaN(max_open) ){
     max_open = 0;
     }
     
     max_open_ttl = parseFloat(max_open_ttl + max_open);
     
     var max_open_qty = parseFloat(jQuery(this).next('td').find('input').val());
     var total_amt = parseFloat(max_open * max_open_qty);
     if(total_amt == null || total_amt == '' || isNaN(total_amt) ){
     total_amt = 0;
     }
     
     
     // jQuery(this).next('td').next('td').find('input').val(total_amt);
     jQuery(this).next('td').find('input').val(total_amt);
     
     alert("gg");
     total_amt_final = total_amt_final + total_amt;
     //alert(total_amt_final);
     
     product_amount = parseFloat(max_open) + parseFloat(jQuery(this).text());
     //alert(product_amount);
     });
     $('#PurchaseInvoiceMaxQnty').val(max_open_ttl);
     $('#PurchaseInvoiceTotalInvoice').val(total_amt_final);
     getvalue();
     
     }*/


    function removefunction(id) {

        var confirm1 = confirm('Are you sure want to remove file');
        if (confirm1) {
            var idvalue = $('#' + id + 's').val('');
            //alert(idvalue);
            $('#' + id).before("<p>Removed</p>");
            $('#' + id).remove();
        }
    }
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#PurchaseInvoiceLoadDate').Zebra_DatePicker({format: "Y-m-d"});
        $('#PurchaseInvoiceRecevingDate').Zebra_DatePicker({format: "Y-m-d"});
    });
</script>