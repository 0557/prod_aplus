
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    R Fuel Purchase INVOICE 
                    <span class="btn " style="text-align:right">
                        <?php echo $this->Html->link('<i class="fa fa-plus" style="color:#fff"></i>  <span style="color:#fff">  Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('id')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('BOL#'); ?></th>
                                            <th><?php echo $this->Paginator->sort('store'); ?></th>
                                            <th><?php echo $this->Paginator->sort('received_date'); ?></th>
                                            <th><?php echo $this->Paginator->sort('supplier'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Total Invoice Amount'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Status'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Files'); ?></th>

                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($purchaseInvoices) && !empty($purchaseInvoices)) { ?>
<?php //pr($purchaseInvoices); ?>
                                            <?php foreach ($purchaseInvoices as $data) { ?>
                                                <tr>
                                                    <td><?php echo h($data['PurchaseInvoice']['id']); ?>&nbsp;</td>


                                                    <td><?php echo h($data['PurchaseInvoice']['bol']); ?></td>
                                                   <td><?php echo h($data['Store']['name']); ?>&nbsp;</td>

                                                         <td><?php echo date('d F,Y', strtotime($data['PurchaseInvoice']['receving_date'])); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Customer']['name']); ?></td>
                                                    <td><?php echo $data['PurchaseInvoice']['gross_amount']; ?>&nbsp;</td>
                                                    <td <?php if($data['PurchaseInvoice']['status']=='Pending') echo 'style="color:red"'; else echo 'style="color:green"'; ?>><?php echo $data['PurchaseInvoice']['status']; ?>&nbsp;</td>
										<td><?php if($data['PurchaseInvoice']['files']){?>
                                        
                                        <a download="" target="_blank" href="<?php echo Router::url('/'); ?>files/<?php echo $data['PurchaseInvoice']['files'] ; ?>"><i class="fa fa-download"></i><?php echo $data['PurchaseInvoice']['files'];  ?>
														</a>
                                        
                                        
									<!--	<a target="_blank" href="https://docs.google.com/gview?url=http://aplus1.net<?php echo Router::url('/'); ?>files/<?php echo $data['PurchaseInvoice']['files'] ; ?>"><i class="fa fa-eye"></i> <?php echo $data['PurchaseInvoice']['files'];  ?></a>--> <?php } ?>&nbsp;</td>
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>', array('action' => 'view', $data['PurchaseInvoice']['id']), array('escape' => false,'class' => 'newicon red-stripe')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('action' => 'edit', $data['PurchaseInvoice']['id']), array('escape' => false,'class' => 'newicon red-stripe')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>', array('action' => 'delete', $data['PurchaseInvoice']['id']), array('escape' => false,'class' => 'newicon  red-stripe')); ?>

                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>
