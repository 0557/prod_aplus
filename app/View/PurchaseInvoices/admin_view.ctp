<?php error_reporting(0); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
  
    <div class="row">
	
	<div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View R Fuel Purchase INVOICE Info : <?php echo $fuelInvoice['PurchaseInvoice']['id'];     ?>
                    </div>
              
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <!--<h2 class="margin-bottom-20"> View  Info - FuelInvoice : <?php echo $fuelInvoice['PurchaseInvoice']['id'];     ?> </h2>-->
                                        
                                        <div class="row">
                                        <div class="col-md-6 col-sm-12">
												<div class="portlet yellow box">
                                        <div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Order Details
                                                            
                                                            
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
                                                        
                                                         <div class="row static-info">
															<div class="col-md-5 name">
																 <span><strong>Store:</strong> </span>
                                                                 <span> <?php echo h($fuelInvoice['Store']['name']); ?></span>
															</div>
															
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
                                                                 <span><strong> Bol:</strong> </span>
                                                                  <span> <?php echo h($fuelInvoice['PurchaseInvoice']['bol']); ?></span>
															</div>
															
														</div>
                                                       
														<div class="row static-info">
															<div class="col-md-5 col-sm-5 name">
                                                                <span><strong> Load Date:</strong> </span>
															 <span>  <?php echo date('d F Y', strtotime($fuelInvoice['PurchaseInvoice']['load_date'])); ?></span>
															</div>
															<!--<div class="col-md-7 col-sm-7 value">
                                                                                                             <?php echo date('d F Y', strtotime($fuelInvoice['PurchaseInvoice']['load_date'])); ?>
                                                                                                                            <?php //echo $fuelInvoice['FuelInvoice']['load_date']; ?>
																 
																
															</div>-->
														</div>
                                                        
														
                                                        
														<div class="row static-info">
															<div class="col-md-5 name">
                                                              <span><strong> Receving Date:</strong> </span>
																<span>  <?php echo date('d F Y', strtotime($fuelInvoice['PurchaseInvoice']['receving_date'])); ?></span>
															</div>
															
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
                                                                 <span><strong> Po: </strong> </span>
                                                                 <span><?php echo $fuelInvoice['PurchaseInvoice']['po']; ?></span>
															</div>
														</div>
                                                        
														<div class="row static-info">
															<div class="col-md-5 name">
															
                                                              <span><strong> Carrier: </strong> </span>
                                                               <span><?php echo $fuelInvoice['PurchaseInvoice']['carrier']; ?></span>	 
															</div>
															
														</div>
                                                                                                               
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                                  <span><strong>  Status: </strong> </span>
                                                               <span label label-success> <?php if($fuelInvoice['PurchaseInvoice']['status']==1){ echo 'Approval';} else { echo "Pending"; } ?></span>	 
															</div>
												
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                                  <span><strong>  Mop: </strong> </span>
                                                               <span>  <?php echo  $fuelInvoice['PurchaseInvoice']['mop'];  ?></span>	 
															</div>
												
														</div>
                                                        
                                                        
                                                        
                                                    </div>
                                                    </div>
													</div>
                                                    <div class="col-md-6 col-sm-12">
												<div class="portlet blue box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Customer Information
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           <span><strong> Supplier:  </strong> </span>
                                                           <span><?php echo $fuelInvoice['Customer']['name']; ?></span>	 
                                                        </div>
															
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                                 
                                                            <span><strong>  Export Supplier:  </strong> </span>
                                                           <span> <?php echo $fuelInvoice['PurchaseInvoice']['export_supplier']; ?></span>	 
															</div>
															
														</div>
                                                    
														<div class="row static-info">
															<div class="col-md-5 name">
																
                                                           <span><strong>  Terminal:  </strong> </span>
                                                           <span>  <?php echo $fuelInvoice['PurchaseInvoice']['terminal']; ?></span>	 
                                                                 
															</div>
															
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                                   <span><strong>   Export Terminal:  </strong> </span>
                                                           <span>  <?php echo $fuelInvoice['PurchaseInvoice']['export_terminal']; ?></span>	 
                                                                 
															</div>
														
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                           <span><strong>    Driver:  </strong> </span>
                                                           <span>  <?php echo $fuelInvoice['PurchaseInvoice']['driver']; ?></span>	 
															</div>
															
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
														
                                                             <span><strong>   FEIN:	 </strong> </span>
                                                           <span> <?php echo $fuelInvoice['PurchaseInvoice']['ship_via']; ?></span>	 
                                                             
															</div>
															
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																
                                                          <span><strong>   Comments: </strong> </span>
                                                           <span>  <?php echo $fuelInvoice['PurchaseInvoice']['comments']; ?></span>	 
															</div>
															<!--<div class="col-md-7 value">
																 <?php echo $fuelInvoice['PurchaseInvoice']['comments']; ?>
															</div>-->
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
															  <span><strong>  Uploaded file: </strong> </span>
                                                                
                                                                <?php  if(!empty($fuelInvoice['PurchaseInvoice']['files'])){ ?>
                                                       <!-- <a href="https://docs.google.com/gview?url=http://aplus1.net<?php echo Router::url('/'); ?>files/<?php echo $fuelInvoice['PurchaseInvoice']['files'];?>" target="_blank" class="btn default btn-xs red-stripe">View File</a>-->
                                                      <span>    <a download="" target="_blank" href="<?php echo Router::url('/'); ?>files/<?php echo $fuelInvoice['PurchaseInvoice']['files'] ; ?>" class="btn default btn-xs red-stripe">Download File
														</a>
                                                         <span> 
                                                        
                     							 <?php      } else { ?>
                                                        <span class="btn default btn-xs red-stripe"> No  File  </span>
                    								 <?php  } ?>
                                                                
															</div>
															<div class="col-md-7 value">
                                                                                                                 																	 													
																 <?php //echo $fuelInvoice['FuelInvoice']['comments'];  ?>
                                                                <?php /*?> href="<?php echo Router::url('/',true);?>uploads/purchaseinvoice/<?php echo $fuelInvoice['PurchaseInvoice']['files'];?>"<?php */?>
															</div>
														</div>
                                                        </div>
                                                    </div>
													</div>
                                        
                                            
                                 
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>

                </div>
            
    </div></div></div>
    <!-- END PAGE CONTENT-->
    <div class="row">
											<div class="col-md-12 col-sm-12">
												<div class="portlet purple box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i> Fuel Purchase Products
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    
                                                    <div class="portlet-body">
														<div class="table-responsive">
															<table class="table table-hover table-bordered table-striped add">
															<thead>
															<tr>
														<tr>
                                                                                                                    
                                    <th>
                                        Products
                                    </th>
                                    <th>
                                        <!-- Vendor Price-->
                                        Gallons Delivered
                                    </th>
                                    <th>
                                        <!-- Tax Class-->
                                        Cost Per Gallons

                                    <th>
                                        Net Amount
                                    </th>
	
                                                                                                                        </tr>
                                </thead>
                                
                                <?php  $total = 0; 
                               // pr($product);
                                 ?>
                                <tbody>
                                  
                                 <?php 
								 $i=0;
								 foreach($product as $key1=> $pro){
								
									
									if($key1==$fuelInvoice['FuelProduct'][$i]['product_id']){
										//echo $pro;
										$fuelInvoice['FuelProduct'][$i]['pro']=$pro;
									}
						
									
									$i++;
								 }
								 
								// pr($fuelInvoice['FuelProduct']);
								 if (isset($fuelInvoice['FuelProduct']) && !empty($fuelInvoice['FuelProduct'])) { ?>
                                    <?php
										
                                    foreach ($fuelInvoice['FuelProduct'] as $key => $data) {
											
											$nameAndId = $this->Custom->GetProductName($data['product_id']);
											
                                        ?>            
                                      <tr>
                                          
                                            <td><?php  echo $data['pro'];
											
											//$this->requestAction('purchase_invoices/product_name/'.$data['product_id']); 
											?>
                                            </td>
                                            <td><?php echo  $data['gallons_delivered'];  ?>&nbsp;</td> 
                                            <td><?php echo  $data['cost_per_gallon'];  ?>&nbsp;</td>
                                            <td><?php echo $data['net_ammount']; ?>&nbsp;</td>
                                      </tr>                      
                                  <?php } }?>
                                    	</tbody>
                                      </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    
               
                	  <div class="col-md-3">
                       <span class="span_1"><strong>Gallon Delivered Total :</strong></span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $fuelInvoice['PurchaseInvoice']['max_qnty']; ?>
                </span>
                      </div>
                        <div class="col-md-3">
                        <span class="span_1"><strong>Net Amount Total :</strong></span><span class="span_2" id="Net_Amount_Total"><?php echo $fuelInvoice['PurchaseInvoice']['total_invoice']; ?> </span>
                      </div>
                        <div class="col-md-3">
                        <span class="span_1"><strong>Taxes :</strong></span><span class="span_2" id="Taxes"><?php echo  $fuelInvoice['PurchaseInvoice']['taxes'];  ?></span>
                      </div>
                        <div class="col-md-3"><span class="span_1"><strong>Gross Amount :</strong></span><span class="span_2" id="Taxes"><?php echo  $fuelInvoice['PurchaseInvoice']['gross_amount'];  ?></span>
                      </div>
               
                                    
          <!--<ul class="list-unstyled amounts total_last col-md-12">
            <li class="last_content_first">
                <span class="span_1">Gallon Delivered Total</span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $fuelInvoice['PurchaseInvoice']['max_qnty']; ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1">Net Amount Total</span><span class="span_2" id="Net_Amount_Total"><?php echo $fuelInvoice['PurchaseInvoice']['total_invoice']; ?> </span>
            </li>
            <li class="last_content_three">
                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo  $fuelInvoice['PurchaseInvoice']['taxes'];  ?></span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo  $fuelInvoice['PurchaseInvoice']['gross_amount'];  ?></span>
            </li>
            <li class="last_content_five">
                <div class="credit_span"><span class="span_1">MOP :  </span></div><span class="span_2" id="MOP">
                <?php echo  $fuelInvoice['PurchaseInvoice']['mop'];  ?>
                </span>
            </li>
    </ul>-->
                                                    </div>
                                                    </div>
    
