 <?php echo $this->Html->script('/fancybox/source/jquery.fancybox.pack'); ?>
 <?php   echo $this->Html->css(array('/fancybox/source/jquery.fancybox')); ?>  
<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Inventory Barcode</div>
							
						</div>
<div class="portlet-body">
<?php echo $this->Form->create('RGroceryBarcode', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                     
	<div class="row">
		  <div class="col-md-12">
                <div class="col-md-4">
                             <label>Select Department</label>
                        <?php 
                        echo $this->Form->input('cat', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => true, 'id' =>'department','options'=>$departmentlist,'empty'=>'Search By Department', 'selected' => $department_id)); ?>
                        <br/>
                </div>
                
                
             <?php /*?>   <div class="col-md-4">
                    <label>Page No</label>
                    <?php 
					$page_list=array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10');
                    echo $this->Form->input('page_no', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => true, 'id' =>'page_no','options'=>$page_list,'empty'=>'Select Page No', 'selected' =>'')); ?>
                    <br/>
                </div><?php */?>

		
                <div class="col-md-1" id="mnop">
                <label>&nbsp;</label>
                <div class="clearfix"></div>
                <?php echo $this->Form->input('Search', array('class' => 'btn btn-success sow','type'=>'submit', 'label' => false, 'required' => false,)); ?>
                <?php echo $this->form->end(); ?>
                </div>
           
                <div class="col-md-1" id="mnop1">
                <label>&nbsp;</label>
                <?php echo $this->Form->create('RGroceryBarcode', array('action'=>'reset','role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                <?php echo $this->Form->input('Reset', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'required' => false,)); ?>
                <?php echo $this->form->end(); ?>
                </div>
            <?php 
			if($this->Session->read('checkpost')){
			echo $this->Form->create('RGroceryBarcode', array('action'=>'barcode','role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 
			?>
            <div class="col-md-2" id="mnopq">
             <label>&nbsp;</label>
          <div class="clearfix"></div>
          <?php echo $this->Form->input('department_id', array('type'=>'hidden', 'label' => false, 'value'=>$department_id)); ?>
          <?php echo $this->Form->input('Generate Barcode', array('class' => 'btn btn-success sow','type'=>'submit', 'label' => false, 'required' => false,)); ?>
		
		</div>
        <?php 
		echo $this->form->end();
		} 
		?>       
        
		</div>
		</div>
 <?php echo $this->form->end(); ?>  
 
 
 
    
       
 <?php echo $this->Form->create('RGroceryBarcode', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>      
        
    <div class="row">
		  <div class="col-md-12">
                <div class="col-md-4">
                <label>Plu No</label>
               <?php /*?> <input name="plu_no" type="text" id="plunosearch" class="form-control" <?php if($plu_no!=''){?>value="<?php echo $plu_no;?>" <?php }?>/>  
                <?php */?>
                
                <?php 				
                echo $this->Form->input('plu_no', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'plunosearch', 'value' =>$plu_no)); 
				?>                
              
                </div>
                <div class="col-md-4">
                    <label>Page No</label>
                 <?php 
				$page_list=array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9');
                echo $this->Form->input('page_no', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'page_no','options'=>$page_list, 'selected' =>$page_no)); ?>
                    <br/>
                </div>

		
                <div class="col-md-1" id="mnop">
                <label>&nbsp;</label>
                <div class="clearfix"></div>
                <?php echo $this->Form->input('Search', array('class' => 'btn btn-success sow','type'=>'submit', 'label' => false, 'required' => false,)); ?>
                <?php echo $this->form->end(); ?>
                </div>
           
                <div class="col-md-1" id="mnop1">
                <label>&nbsp;</label>
                <?php echo $this->Form->create('RGroceryBarcode', array('action'=>'reset','role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                <?php echo $this->Form->input('Reset', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'required' => false,)); ?>
                <?php echo $this->form->end(); ?>
                </div>
                
				<?php 
                if($this->Session->read('checkpost1')){
                echo $this->Form->create('RGroceryBarcode', array('action'=>'barcode1','role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 
                ?>
                <div class="col-md-2" id="mnopq">
                <label>&nbsp;</label>
                <div class="clearfix"></div>
                <?php echo $this->Form->input('plu_no', array('type'=>'hidden', 'label' => false, 'value'=>$plu_no)); ?>
                <?php echo $this->Form->input('page_no', array('type'=>'hidden', 'label' => false, 'value'=>$page_no)); ?>
                <?php echo $this->Form->input('Generate Barcode', array('class' => 'btn btn-success sow','type'=>'submit', 'label' => false, 'required' => false,)); ?>
                
                </div>
                <?php 
                echo $this->form->end();
                } 
                ?>       
        
		</div>
		</div>    
 							
 <?php echo $this->form->end(); ?>    
			
			<hr/>
         
			  <div class="datatable">
						<?php 
							echo $this->Form->create('RGroceryBarcode', array('action' => 'blukupdate', 'role' => 'form', 'type' => 'file')); 
						?>
						
						
                         <!--<table class="table table-striped table-bordered table-advance table-hover" id="example">-->
                         <table class="table table-striped table-bordered table-advance table-hover">
						<thead>
						<tr>
							
								<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
								<th><?php echo $this->Paginator->sort('Description'); ?></th>
								<th><?php echo $this->Paginator->sort('Starting Inventory'); ?></th>
								<th><?php echo $this->Paginator->sort('Cost'); ?></th>
								
						</tr>
						</thead>
						
						<tbody>
						
						
						<?php if (isset($Globalupcslist) && !empty($Globalupcslist)) { ?>
                                            <?php $i=0; foreach ($Globalupcslist as $data) { ?>
											<tr>
					<?php 
					$sessarr=array();
					$sessar=array();
					$sessarr=$this->Session->read('ScoreCardCriteria');
					$checked="";
					$current_inventory=$data['RGroceryBarcode']['current_inventory'];
					$price=$data['RGroceryBarcode']['price'];
				/*	if($data['RGroceryBarcode']['id']==$sessarr['id']){
						//echo "checked";
						$checked='checked="checked"';
						$current_inventory=$sessarr['current_inventory'];
						$price=$sessarr['price'];
					}*/
					
							if(count($sessarr) > 0){
                            foreach ($sessarr as $sessar) {
                                if($data['RGroceryBarcode']['id']==$sessar['id']){
                                    $checked='checked="checked"';
                                    $current_inventory=$sessar['current_inventory'];
                                    $price=$sessar['price'];
                                }
                            }
							}
					?>
                                                    <td><?php echo h($data['RGroceryBarcode']['plu_no']); ?></td>
                                                    <td>
													<?php echo h($data['RGroceryBarcode']['description']); ?>&nbsp;</td>
                                                    
												<td>
												<?php echo h($current_inventory); ?>
                                                </td>
                                                
												<td>
												<?php echo h($price); ?>
												</td>
                                              
                            
											</tr>
						<?php $i++;} }?>
                        
                        
                        
                        
                      <?php if (isset($Globalupcslist1) && !empty($Globalupcslist1)) { ?>
                                            <?php $i=0; foreach ($Globalupcslist1 as $data1) { ?>
											<tr>
					<?php 
					$sessarr=array();
					$sessar=array();
					$sessarr=$this->Session->read('ScoreCardCriteria');
					$checked="";
					$current_inventory=$data1['RGroceryBarcode']['current_inventory'];
					$price=$data1['RGroceryBarcode']['price'];
				/*	if($data1['RGroceryBarcode']['id']==$sessarr['id']){
						//echo "checked";
						$checked='checked="checked"';
						$current_inventory=$sessarr['current_inventory'];
						$price=$sessarr['price'];
					}*/
					
							if(count($sessarr) > 0){
                            foreach ($sessarr as $sessar) {
                                if($data1['RGroceryBarcode']['id']==$sessar['id']){
                                    $checked='checked="checked"';
                                    $current_inventory=$sessar['current_inventory'];
                                    $price=$sessar['price'];
                                }
                            }
							}
					?>
                                                    <td><?php echo h($data1['RGroceryBarcode']['plu_no']); ?></td>
                                                    <td>
													<?php echo h($data1['RGroceryBarcode']['description']); ?>&nbsp;</td>
                                                    
												<td>
												<?php echo h($current_inventory); ?>
                                                </td>
                                                
												<td>
												<?php echo h($price); ?>
												</td>
                                              
                            
											</tr>
						<?php $i++;} }?>  
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
						</tbody>
						
						</table>
                            </div>
						<?php if (isset($Globalupcslist) && !empty($Globalupcslist)) { ?>

					<span id="postimport">

						<?php echo $this->form->end(); ?>
						
						</span>
			<?php } ?>						

</div> 

</div> 






<script type="text/javascript" >
	$(document).ready(function() {
	  $('#example').DataTable({"pageLength": 100,bFilter: false});
		
		$('#saledate').Zebra_DatePicker({format:"m-d-Y"});

$('#selecctall').click(function(event) {  
	 $("input:checkbox").prop('checked', $(this).prop("checked"));
       
    });
	
	$('.calculation1').click(function(){
				
					$(".various1").fancybox({
					maxWidth	: 400,
					maxHeight	: 650,
					fitToView	: false,
					width		: '40%',
					height		: '65%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none',
                    afterClose	:  function () {
                        window.location.reload();
                    },
					href        :'<?php echo Router::url('/') ?>admin/r_grocery_items/newplu'
					});
				});

/*	
	 $('#postimport').click(function(){
	   var data = new Array();
	   var plu = new Array();
	   var descri = new Array();
	   var startinginven = new Array();
	   var cost = new Array();
			$("input[name='import']:checked").each(function(i) {
				data.push($(this).val());
			});
			$("input[name='plu_no']").each(function(i) {
				plu.push($(this).val());
			});
			$("input[name='description']").each(function(i) {
				descri.push($(this).val());
			});
			$("input[name='startinven']").each(function(i) {
				startinginven.push($(this).val());
			});
			$("input[name='cost']").each(function(i) {
				cost.push($(this).val());
			});


			    $.ajax({
                url: '<?php echo Router::url('/') ?>admin/lottery/import_game',
                type: 'post',
                data: { ids: data,plu:plu,descri:descri,startinginven:startinginven,cost:cost},
                success:function(data){
					alert(data);
//					window.location='<?php echo Router::url('/') ?>admin/lottery/games';
                }
            });
	

			
        });
	*/	$('#plunosearch').keyup(function(){
			var plunosearch = $('#plunosearch').val();
			$('.sorting_1').each(function(){
				var plu = $(this).html();
				if(plu==plunosearch){
					$(this).addClass('green');
					$('.sorting_1').not(this).removeClass('green');
					}else{
											$(this).removeClass('green');

						}
				});
			});
});
</script>

<style>
.green{
	background:#090;
	}
	.sow{float:left; margin-right:5px;}
	.sow .btn{font-size:14px;}
	.importcheck{ margin-left:10px;}
	#mnop .input.submit{ float:left;}
	#mnop div{ display:inline-block;}
	#mnop .clearfix{ display:block;}
	#mnopq{ padding-left:10px;}
	#mnop{ padding-right:0px;}
	#mnop .btn{ font-size:12px !important;}
</style> 