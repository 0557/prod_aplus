<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php echo $this->Form->create('RGroceryDepartment', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php $customer_id = explode(',',$this->request->data['RGroceryDepartment']['customerId']); 
$blue_laws = explode(',',$this->request->data['RGroceryDepartment']['blue_laws']); 
// pr($this->data); die;
?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Department
                    </div>
                </div></div></div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Department:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('department', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>
                                    <?php echo $this->Form->input('id'); ?>
                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Name:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Name')); ?>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Description :			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('description', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Description ')); ?>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Minimum Amount :	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('min_amount', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Min. Amount')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Maximum Amount :	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('max_amount', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Max. Amount')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fee/Charge	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('fee', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Fee/Charge')); ?>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Check Customer ID:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('customerId1', array('class' => 'make-switch', 'checked'=>($customer_id[0] == 0)?false:true ,'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                    <?php echo $this->Form->input('customerId2', array('class' => 'make-switch', 'checked'=>($customer_id[1] == 0)?false:true ,'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Food Stamp:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('food_stamp', array('class' => 'make-switch','div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Is Negative:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('is_negative', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Special Discount:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('special_discount', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Blue Laws:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('blue_laws1', array('class' => 'make-switch','checked'=>($blue_laws[0] == 0)?false:true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                    <?php echo $this->Form->input('blue_laws2', array('class' => 'make-switch','checked'=>($blue_laws[1] == 0)?false:true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Loyalty Redeem:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('loyalty_reddem', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Money Order:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('money_order', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Taxable:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('taxable', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>




                            </div>
                            <div id="class_extra_taxable_option" style="<?php if($this->request->data['RGroceryDepartment']['taxable'] == '0'){ echo "display:none"; }  ?>">
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 1:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax1', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 2:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax2', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 3:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax3', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 4:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax4', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                            </div>
                        
                        </div></div>

                </div>




                <!-- END FORM-->
            </div>


        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit" class="btn blue">Submit</button>
            <button type="reset" class="btn default">Reset</button>
            <button type="button" onclick="javascript:history.back(1)"
                    class="btn default">Cancel</button>

            <!--<button type="button" class="btn default">Cancel</button>-->
        </div>

    </div>
    <?php echo $this->form->end(); ?>

</div>
</div>
<script type="text/javascript">
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    $('#RGroceryDepartmentTaxable').change(function () {
        if (this.checked) {
            $('#class_extra_taxable_option').show('slow', function () {
                $('#class_extra_taxable_option').css('display', 'block');
            });
        } else {
            $('#class_extra_taxable_option').hide('slow', function () {
                $('#class_extra_taxable_option').css('display', 'none');
            });
        }
    });
</script>







