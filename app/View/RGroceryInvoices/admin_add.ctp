<?php //  pr($this->request->data);      die;                     ?>

<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers', 'admin/bootstrap-fileinput', 'admin/jquery-ui')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker', 'jquery-ui.css')); ?>     
<?php echo $this->Form->create('RGroceryInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
    <div class="page-content portlet-body" >
        <div class="row boderstyle">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Add  R Grocery Purchase INVOICE
                        </div>
                    </div>
                    <div class="portlet-body row">

                        <div class="form-body ">
                            <div class="row">

                                <div class="col-md-4 col-sm-12 " >
                                    <div class="portlet box">

                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Order Details
                                            </div>
                                            <div class="actions">
                                                <!--<a href="#" class="btn default btn-sm">
                                                        <i class="fa fa-pencil"></i> Edit
                                                </a>-->
                                            </div>
                                        </div>
                                        <div class="portlet-body extra_tt">

                                            <div class="row static-info">
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Invoice Date <span style="color:red">*</span>
                                                    </label>


                                                    <?php echo $this->Form->input('invoice_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'true', 'size' => '16')); ?>


                                                </div>
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Purchase Order  <span style="color:red">*</span>			 
                                                    </label>

                                                    <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'placeholder' => 'Enter Po')); ?>

                                                </div>
                                            </div>


                                            <div class="row static-info">
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Invoice Number  <span style="color:red">*</span> 		 	 
                                                    </label>


                                                    <?php echo $this->Form->input('invoice', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'placeholder' => 'Enter invoice')); ?>

                                                </div>
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Payment Type <span style="color:red">*</span>
                                                    </label>

                                                    <?php echo $this->Form->input('mop', array('class' => 'form-control select_list_mop', 'onchange' => "mop_function(this.value)", 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'Check', 'Cash Paid' => 'Cash Paid'))); ?>
                                                </div>
                                            </div>


                                            <div class="row static-info">
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Status
                                                    </label>

                                                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>

                                                </div>
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Bank	 
                                                    </label>

                                                    <?php echo $this->Form->input('bank', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $rbanks, 'empty' => '')); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 ">
                                    <div class="portlet box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Vendor Information
                                            </div>
                                            <div class="actions">
                                                <!--<a href="#" class="btn default btn-sm">
                                                        <i class="fa fa-pencil"></i> Edit
                                                </a>-->
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Vendor
                                                    </label>

                                                    <?php echo $this->Form->input('vendor_id', array('class' => 'form-control', 'label' => false, 'required' => 'true', 'type' => 'select', 'options' => $rGroceryvendor, 'empty' => 'Select Vendor')); ?>
                                                </div>
                                                <div class="col-md-6 value">
                                                    <label>
                                                        Check#
                                                    </label>

                                                    <?php echo $this->Form->textarea('terms', array('label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Enter terms')); ?>
                                                </div>
                                            </div>




                                            <div class="row static-info">
                                                <div class="col-md-12 value">
                                                    <label>Terms/ Comments</label>
                                                    <?php echo $this->Form->textarea('comments', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Comments')); ?>
                                                </div>
                                            </div>
                                            <div class="row static-info">

                                                <div class="col-md-12">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="input-group input-large">
                                                            <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                <span class="fileinput-filename">
                                                                </span>
                                                            </div>
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new">
                                                                    Select file
                                                                </span>
                                                                <span class="fileinput-exists">
                                                                    Change
                                                                </span>

                                                                <?php echo $this->Form->input('files', array('type' => 'file', 'div' => false, 'label' => false, 'required' => 'false')); ?>

                                                            </span>
                                                            <a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
                                                                Remove
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div></div></div>



                                <div class="col-md-4 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Total
                                            </div>
                                            <div class="actions">
                                                <!--<a href="#" class="btn default btn-sm">
                                                        <i class="fa fa-pencil"></i> Edit
                                                </a>-->
                                            </div>
                                        </div>

                                        <div class="portlet-body" style="overflow:hidden;">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Total Count Delivered </label>
                                                    <span class="span_2" id="Gallon_Delivered_Total"><?php echo $this->Form->input('total_count', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'maxlength' => '15', 'readonly' => false, 'placeholder' => '0')); ?></span>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Net Amount Total</label>
                                                    <span class="span_2" id="Net_Amount_Total"><?php echo $this->Form->input('net_amount', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'readonly' => false, 'maxlength' => '15', 'placeholder' => '0', 'onchange' => 'GrossAmountInvoice();')); ?></span>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Taxes</label>
                                                    <span class="span_2" id="Taxes"><?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['RGroceryInvoice']['taxes'])) ? $this->request->data['RGroceryInvoice']['taxes'] : '0', 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0', 'maxlength' => '15', 'onchange' => 'GrossAmountInvoice();')); ?></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Gross Amount</label>
                                                    <span class="span_2" id="Taxes"><?php echo $this->Form->input('gross_amount', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'maxlength' => '15', 'placeholder' => '0')); ?></span>
                                                </div>
                                            </div>

                                            <!--                                            <ul class="list-unstyled amounts total_last ">
                                                                                            <li class="last_content_first">
                                                                                                <span class="span_1">Total Count Delivered  </span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $this->Form->input('total_count', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'maxlength' => '15', 'readonly' => false, 'placeholder' => '0')); ?>
                                                                                                </span>
                                                                                            </li>
                                                                                            <li class="last_content_two">
                                                                                                <span class="span_1">Net Amount Total </span><span class="span_2" id="Net_Amount_Total"><?php echo $this->Form->input('net_amount', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'readonly' => false, 'maxlength' => '15', 'placeholder' => '0', 'onchange' => 'GrossAmountInvoice();')); ?></span>
                                                                                            </li>
                                                                                            <li class="last_content_three">
                                                                                                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['RGroceryInvoice']['taxes'])) ? $this->request->data['RGroceryInvoice']['taxes'] : '0', 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0', 'maxlength' => '15', 'onchange' => 'GrossAmountInvoice();')); ?></span>
                                                                                            </li>
                                                                                            <li class="last_content_four">
                                                                                                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('gross_amount', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'maxlength' => '15', 'placeholder' => '0')); ?></span>
                                                                                            </li>
                                            
                                            
                                            
                                            
                                                                                        </ul>-->
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions col-md-12 row" style="text-align:center;">
                                    <button type="submit" class="btn blue" onclick="return validateForm()">Submit</button>
                                    <button type="reset" class="btn default">Reset</button>
                                    <button type="button" onclick="javascript:history.back(1)"
                                            class="btn default">Cancel</button>
                                    <!--<button type="button" class="btn default">Cancel</button>-->
                                </div>


                            </div>




                            <!-- END FORM-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.r_grocery_department_id.', array('class' => 'form-control exampleInputName', 'options' => $rGroceryDepartment, 'empty' => 'Select Depatment', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select')); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.r_grocery_item_id.', array('label' => false, 'class' => 'form-control autocomplete_seacrh', 'type' => 'text', 'placeholder' => 'Enter Inner/Base Upsc')); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.description.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Description')); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.count.', array('label' => false, 'class' => 'form-control count', 'onkeyup' => 'calculation($(this));', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.price.', array('label' => false, 'class' => 'form-control', 'onkeyup' => 'calculation($(this));', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php echo $this->Form->input('RGroceryInvoiceProduct.total_cost.', array('label' => false, 'readonly' => true, 'class' => 'form-control total_cost', 'type' => 'text', 'placeholder' => '0')); ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
<!--        <tr>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.r_grocery_department_id.', array('class' => 'form-control exampleInputName', 'options' => $rGroceryDepartment, 'empty' => 'Select Depatment', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select'));         ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.r_grocery_item_id.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Inner Upsc'));         ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.description.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Description'));         ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.count.', array('label' => false, 'class' => 'form-control calculate count', 'onkeyup' => 'calculation($(this));', 'type' => 'text', 'placeholder' => '0'));         ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.price.', array('label' => false, 'class' => 'form-control calculate', 'onkeyup' => 'calculation($(this));', 'type' => 'text', 'placeholder' => '0'));         ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><?php //echo $this->Form->input('RGroceryInvoiceProduct.total_cost.', array('label' => false, 'readonly' => true, 'class' => 'form-control total_cost', 'type' => 'text', 'placeholder' => '0'));         ?></td>
            <td style="background-color: rgb(249, 249, 249); width: 200px"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>-->
    </tbody>             
</table>
<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->

<script>
    function mop_function(value) {
        $('#MOP').text(value);
    }
    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }

    $(document).ready(function () {
        ComponentsPickers.init();
    });

    $(document).delegate('.Delete_product', 'click', function () {
        //  $(this).parents('tr').next('tr').remove();
        $(this).parents('tr').remove();
        totalCountProduct();
        totalInvoiceAmount();



    });

    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    $("select , input").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    function calculation(currentval) {

        if (currentval.hasClass('calculate')) {
            var value = currentval.parents('tr').find('td:nth-child(4)').find('input').val();
            var count = currentval.parents('tr').find('td:nth-child(3)').find('input').val();
            var total = (parseFloat(value) * parseFloat(count));
            if (!isNaN(total)) {
                var userNo = currentval.parents('tr').find('td:nth-child(5)').find('input').val(total);
            } else {
                var userNo = currentval.parents('tr').find('td:nth-child(5)').find('input').val(0);
            }
        } else {
            var value = currentval.parents('tr').find('td:nth-child(5)').find('input').val();
            var count = currentval.parents('tr').find('td:nth-child(4)').find('input').val();
            var total = (parseFloat(value) * parseFloat(count));
            if (!isNaN(total)) {
                var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(total);
            } else {
                var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(0);
            }
        }

//        var total = (parseFloat(value) * parseFloat(count));
//        if (!isNaN(total)) {
//            var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(total);
//        } else {
//            var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(0);
//        }
        totalCountProduct();
        totalInvoiceAmount();

    }

    function totalCountProduct() {
        var taotalcount = 0
        $('#products_add_more tr').each(function () {
            var curcount = parseFloat($(this).find('input.count').val());
            if (isNaN(curcount)) {
                curcount = 0;
            }
            taotalcount = taotalcount + parseFloat(curcount);
        });
        $('#RGroceryInvoiceTotalCount').val(taotalcount);
    }
    function totalInvoiceAmount() {
        var taotalcount = 0
        $('#products_add_more tr').each(function () {
            var curcount = parseFloat($(this).find('input.total_cost').val());
            if (isNaN(curcount)) {
                curcount = 0;
            }
            taotalcount = taotalcount + parseFloat(curcount);
        });
        $('#RGroceryInvoiceNetAmount').val(taotalcount);
        GrossAmountInvoice();
    }
    $('#RGroceryInvoiceTaxes').keyup(function () {
        GrossAmountInvoice();
    });
    function GrossAmountInvoice() {
        var totalinvice = $('#RGroceryInvoiceNetAmount').val();
        var totalinvice_tax = $('#RGroceryInvoiceTaxes').val();
        var grossamount = parseFloat(totalinvice) + parseFloat(totalinvice_tax);
        $('#RGroceryInvoiceGrossAmount').val(grossamount.toFixed(2));
    }

</script>
<script>
    /*  $(function () {
     $('#products_add_more').delegate('input.autocomplete_seacrh', 'focus', function (e) {
     var department = $(this).parents('td').prev('td').find('select').val();
     if(department != ''){
     $(this).autocomplete({
     source: '<?php echo Router::url('/') ?>r_grocery_mix_matches/autocomplete/'+department,
     change: function (event, ui) {
     $(this).parents('td').next('td').find('input').val(ui.item.desc);
     },
     select: function (event, ui) {
     $(this).parents('td').next('td').find('input').val(ui.item.desc);
     },
     response: function (event, ui) {
     // console.log(ui.content.length);
     if (!ui.content.length) {
     alert('UPC code Not found');
     }
     }
     });
     }else{
     alert('Please Select Department');
     }
     });
     
     });*/

    function validateForm()
    {
        z = document.getElementById('RGroceryInvoiceTotalCount').value
        y = document.getElementById('RGroceryInvoiceNetAmount').value
        x = document.getElementById('RGroceryInvoiceTaxes').value
        if (!z.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && z != '')
        {
            alert("TotalCount alowed maximum 10 digits before decimal point and after 4digits.");
            return false;
        } else if (!y.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && y != '') {
            alert("NetAmount alowed maximum 10 digits before decimal point and after 4digits.");
            return false;
        } else if (!x.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && x != '') {
            alert("Taxes alowed maximum 10 digits before decimal point and after 4digits.");
            return false;
        } else {

            return true;

        }
    }
</script>


<style>
    textarea{
        resize: none;
    }
</style>
<!--<script type="text/javascript" charset="utf-8">
       $(document).ready(function() {
               $('#RGroceryInvoiceInvoiceDate').Zebra_DatePicker({format:"Y-m-d"});
       });
</script>-->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#RGroceryInvoiceInvoiceDate").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>