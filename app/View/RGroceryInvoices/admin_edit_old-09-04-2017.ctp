<?php //  pr($this->request->data);  // die;  ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers', 'admin/bootstrap-fileinput','admin/jquery-ui')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker','jquery-ui.css')); ?>  
<?php echo $this->Form->create('RGroceryInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update  R Grocery Purchase INVOICE
                    </div>
                </div>
            
			 <div class="portlet-body extra_tt">
            <div class="row">
							 <div class="col-md-4 col-sm-12">
                    <div class="portlet  box">
  <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Order Details
                    </div>
                </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-4 name">
                                    Invoice Date<span style="color:red">*</span>
                                </div>
                                <div class="col-md-8 value">
                                 <?php echo $this->Form->input('invoice_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'true','value' => date('Y-m-d', strtotime($this->request->data['RGroceryInvoice']['invoice_date'])), 'size' => '16')); ?>
                                    <!--<div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('invoice_date', array('class' => 'form-control', 'value' => date('d/m/Y - H:m', strtotime($this->request->data['RGroceryInvoice']['invoice_date'])), 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'true', 'size' => '16')); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>-->

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Purchase Order <span style="color:red">*</span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('po', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'placeholder' => 'Enter Po')); ?>
                                    <?php echo $this->Form->input('id'); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Invoice Number  <span style="color:red">*</span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('invoice', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'true', 'placeholder' => 'Enter invoice')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Payment Type <span style="color:red">*</span>			 
                                </div>
                                <div class="col-md-7 value">
 <?php echo $this->Form->input('mop', array('class' => 'form-control select_list_mop', 'onchange' => "mop_function(this.value)", 'empty' => 'Select Mop', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select', "options" => array('EFT' => 'EFT', 'Credit' => 'Credit', 'Check' => 'Check','Cash Paid'=>'Cash Paid'))); ?>
                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Status
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Pending" => "Pending", "Approved" => "Approved"))); ?>
                                </div>
                            </div>
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    Bank	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php  echo $this->Form->input('bank', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => $rbanks,'empty'=>'','default'=>$this->request->data['RGroceryInvoice']['bank'])); ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="portlet  box">
                       <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Vendor Information
                    </div>
                </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Vendor
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('vendor_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'type' => 'select', 'options' => $rGroceryvendor, 'empty' => 'Select Vendor')); ?>
                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Check#	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->textarea('terms', array('label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Enter terms')); ?>
                                </div>
                            </div>


                           
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terms/ Comments 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->textarea('comments', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Comments')); ?>
                                </div>
                            </div>
							 <div class="row static-info">
                               
                                <div class="col-md-12">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename">
                                                </span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new">
                                                    Select file
                                                </span>
                                                <span class="fileinput-exists">
                                                    Change
                                                </span>

                                                <?php echo $this->Form->input('files', array('type' => 'file', 'div' => false, 'label' => false, 'required' => 'false')); ?>

                                            </span>
                                            <a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></div></div>

<div class="col-md-4 col-sm-12">
            <div class="portlet  box">
               <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Total
                    </div>
                </div>
                <div class="portlet-body" style="overflow:hidden;">
        <ul class="list-unstyled amounts total_last">
            <li class="last_content_first">
                <span class="span_1">Total Count Delivered </span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $this->Form->input('total_count', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'readonly' => false, 'placeholder' => '0')); ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1">Net Amount Total </span><span class="span_2" id="Net_Amount_Total"><?php echo $this->Form->input('net_amount', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'value' => (isset($this->request->data['RGroceryInvoice']['net_amount'])) ? $this->request->data['RGroceryInvoice']['net_amount'] : '0','readonly' => false, 'placeholder' => '0')); ?></span>
            </li>
            <li class="last_content_three">
                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('taxes', array('label' => false, 'value' => (isset($this->request->data['RGroceryInvoice']['taxes'])) ? $this->request->data['RGroceryInvoice']['taxes'] : '0', 'class' => 'form-control', 'type' => 'text', 'placeholder' => '0')); ?></span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo $this->Form->input('gross_amount', array('label' => false,'value' => (isset($this->request->data['RGroceryInvoice']['gross_amount'])) ? $this->request->data['RGroceryInvoice']['gross_amount'] : '0', 'class' => 'form-control', 'type' => 'text', 'readonly' => false, 'placeholder' => '0')); ?></span>
            </li>
        </ul>

    </div>
    </div>
    </div>
							
			    <div class="form-actions row" style="text-align:center;">
        <button type="submit" class="btn blue" onclick="return validateForm()">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

	</div>
			</div>
        </div>

      



</div>
</div>
</div>
<?php echo $this->form->end(); ?>

<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->
 <style>
 textarea{
	  resize: none;
 }
 </style>
<script>
    function mop_function(value) {
        $('#MOP').text(value);
    }
    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }
    $(document).ready(function () {
        ComponentsPickers.init();
        var selected_mop = '<?php echo $this->request->data['RGroceryInvoice']['mop'] ?>';
        $('#MOP').text(selected_mop);
       //   totalCountProduct();
//totalInvoiceAmount();
       //  GrossAmountInvoice();
    });

    $(document).delegate('.Delete_product', 'click', function () {
        $(this).parents('tr').remove();
        totalCountProduct();
        totalInvoiceAmount();
         GrossAmountInvoice();
    });
    
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    
    $("select , input").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    function calculation(currentval) {

        var value = currentval.parents('tr').find('td:nth-child(5)').find('input').val();
        var count = currentval.parents('tr').find('td:nth-child(4)').find('input').val();
        var total = (parseFloat(value) * parseFloat(count));
        if (!isNaN(total)) {
            var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(total);
        } else {
            var userNo = currentval.parents('tr').find('td:nth-child(6)').find('input').val(0);
        }
        totalCountProduct();
        totalInvoiceAmount();

    }

    function totalCountProduct() {
        var taotalcount = 0
        $('#products_add_more tr').each(function () {
            var curcount = parseFloat($(this).find('td:nth-child(4)').find('input').val());
            if (isNaN(curcount)) {
                curcount = 0;
            }
            taotalcount = taotalcount + parseFloat(curcount);
        });
        $('#RGroceryInvoiceTotalCount').val(taotalcount);
    }
    
    function totalInvoiceAmount() {
        var taotalcount = 0
        $('#products_add_more tr').each(function () {
            var curcount = parseFloat($(this).find('td:nth-child(6)').find('input').val());
            if (isNaN(curcount)) {
                curcount = 0;
            }
            taotalcount = taotalcount + parseFloat(curcount);
        });
        $('#RGroceryInvoiceNetAmount').val(taotalcount.toFixed(2));
        GrossAmountInvoice();
    }
    
    $('#RGroceryInvoiceTaxes').keyup(function () {
        GrossAmountInvoice();
    });
    
    function GrossAmountInvoice() {
        var totalinvice = $('#RGroceryInvoiceNetAmount').val();
        var totalinvice_tax = $('#RGroceryInvoiceTaxes').val();
        var grossamount = parseFloat(totalinvice) + parseFloat(totalinvice_tax);
        $('#RGroceryInvoiceGrossAmount').val(grossamount.toFixed(2));
    }
    
</script>
<script>
    $(function () {
        $('#products_add_more').delegate('input.autocomplete_seacrh', 'focus', function (e) {
            var department = $(this).parents('td').prev('td').find('select').val();
           if(department != ''){
            $(this).autocomplete({
                source: '<?php echo Router::url('/') ?>r_grocery_mix_matches/autocomplete/'+department,
                change: function (event, ui) {
                    $(this).parents('td').next('td').find('input').val(ui.item.desc);
                },
                select: function (event, ui) {
                    $(this).parents('td').next('td').find('input').val(ui.item.desc);
                },
                response: function (event, ui) {
                    // console.log(ui.content.length);
                    if (!ui.content.length) {
                        alert('UPC code Not found');
                    }
                }
            });
            }else{
                alert('Please Select Department');
            }
        });
        
    });
	
		function validateForm()
{
z =  document.getElementById('RGroceryInvoiceTotalCount').value
y =  document.getElementById('RGroceryInvoiceNetAmount').value
x =  document.getElementById('RGroceryInvoiceTaxes').value
    if(!z.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && z!='')
        {
        alert("TotalCount alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
        }else if(!y.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && y!=''){
        alert("NetAmount alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else if(!x.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && x!=''){
        alert("Taxes alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else{
		
		return true;
			
		}
}

</script>
<!--<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#RGroceryInvoiceInvoiceDate').Zebra_DatePicker({format:"Y-m-d"});
	});
</script>-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#RGroceryInvoiceInvoiceDate" ).datepicker({ dateFormat: 'yy-mm-dd' });	
  } );
  </script>