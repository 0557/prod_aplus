<style>
.perdate .Zebra_DatePicker_Icon_Inside{
		top: -25px !important;
	left: 148px !important;
	}
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
#saveonly, #newandsave, .red-stripe{
color:#fff !important
}
.newiconp img{
padding:7px;}

.icon_image_style img{
width:30px;	
}

.colornbn{ color:#000;}
.newiconnn{ padding: 3px;  display: table;  vertical-align: middle;  margin-top: 9px;  margin-left: 3px;  margin-right: 3px;}
.newiconnn1{ padding: 3px;  display: table;  vertical-align: middle;  margin-top: 8px;  margin-left: 3px;  margin-right: 3px;}
/*div.radio input { opacity:1;  margin-left: -12px !important; height: 14px;  margin-top: 2px;}*/
#mpymt{ margin-bottom: 15px;}
#mpymt label.spciallab{ color:#666666; background-color:#e3e3e3; padding:5px 10px; display:inline-block; border: 1px solid #e3e3e3;}
#mpymt label.nobg{ color:#666666; background-color:#fff;  display:inline-block;}
#mpymt label.spciallab.active{ background-color:#fff;}
#mpymt div.radio input{ opacity:0; visibility:hidden;    display: none;} 
#mpymt div.radio{ width:0; height:0; margin-left: 0px; display: none;}
</style>
       
<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" >
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                   R Grocery Purchase INVOICE <span class="btn green fileinput-button">
                        <?php 					
						echo $this->Html->link('Add New', array('action' => 'admin_add'), array('escape' => true,'data-toggle'=>'tooltip' ,'title'=>'Add New','data-placement'=>'left')); ?>
                    </span>
                </h3>
            </div>
        </div>     
        
<div id="myModal" class="modal fade" role="dialog" data-keyboard="true"> 
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
   <h4 class="modal-title">New Purchase Pack</h4>
      </div>
       <?php echo $this->Html->script('admin/select2.min'); ?>
        <?php
        echo $this->Html->css(array(
            'admin/select2',
	         'admin/select2-metronic',
        ));
        ?> 
      <div class="modal-body">
			 <div class="row">
			  	<?php echo $this->Form->create('PurchasePacks', array('role' => 'form', 'type' => 'file','controller'=>'purchase_packs','action'=>'new_pack'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
				   <div class="col-md-4 col-xs-6">
                    <label>Item Scan Code <span style="color:red">*</span></label>
                    <!-- important for developer please place your clander code on this input-->
					 <?php echo $this->Form->input('invoice', array('id' => 'invoice', 'class' => 'form-control','type' => 'hidden','label' => false)); ?>
                	
					<?php echo $this->Form->input('vendor', array('id' => 'vendor', 'class' => 'form-control','type' => 'hidden','label' => false)); ?>
					<?php echo $this->Form->input('url', array('id' => 'url', 'class' => 'form-control','type' => 'hidden','label' => false,'value'=>'r_grocery_invoices')); ?>
                	
					<?php echo $this->Form->input('Item_Scan_Code', array('id' => 'pul', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>               
                    
                  </div>
				   <div class="col-md-4 col-xs-6">
                    <label>Item Name</label>                
					
                    <?php 					
					//pr($item);
					echo $this->Form->input('field', array(
					'id' => 'description', 'class' => 'form-control select2me','type' => 'select','label' => false,'required' => false,
                    'options' =>$item,
                    'empty' => 'Select Item',
					//'default' => '6740'
                    ));
					?>
                    
                    
                  </div>
			
				   <div class="col-md-4 col-xs-6">
                    <label>Item#&nbsp;&nbsp;<span id="editbt"></span></label>                    
     				               
					<?php echo $this->Form->input('Item#', array('id' => 'item', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>
				  
  				   <div class="col-md-4 col-xs-6">
                    <label>Case Cost <span style="color:red">*</span></label>
                   
                	
					<?php echo $this->Form->input('Case_Cost', array('id' => 'Case_Cost', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'maxlength'=>'15')); ?>
                  </div>
                  
				   <div class="col-md-4 col-xs-6">
                    <label>Units Per Case <span style="color:red">*</span></label>
                
                	
					<?php echo $this->Form->input('Units_Per_Case', array('id' => 'Units_Per_Case', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'maxlength'=>'15')); ?>
                  </div>
			    <div class="col-md-4 col-xs-6">
                    <label>Case Retail <span style="color:red">*</span></label>
                	
					<?php echo $this->Form->input('Case_Retail', array('id' => 'Case_Retail', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'maxlength'=>'15')); ?>
                  </div> 
		          
				     
                  <div class="col-md-4 col-xs-6">
                    <label>Purchase Quantity <span style="color:red">*</span></label>
                	
					<?php echo $this->Form->input('purchase', array('id' => 'purchase', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'maxlength'=>'15')); ?>
                  </div>
                  
   
		
					 <?php echo $this->Form->input('gid', array('id' => 'gid', 'class' => 'form-control','type' => 'hidden', 'label' => false)); ?>
                  

				  <div class="col-md-4 col-xs-6">
                    <label>Gross Amount <span style="color:red">*</span></label>
                  	
					<?php echo $this->Form->input('gross_amount', array('id' => 'gross_amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'maxlength'=>'15')); ?>
                  </div>
                  <div class="clearfix"></div>
                  
				  <div class="col-md-4 col-xs-6">
                  <label></label>					 
                  <input type="button" id="saveonly" value="Save" class="form-control btn brn-success" style="margin-top:5px;"/>
                  </div>
				  <div class="col-md-4 col-xs-6">
                  <label></label>				 
                  <input type="button" id="newandsave" value="Save & New" class="form-control btn brn-success" style="margin-top:5px;"/>
                  </div>
			      <div class="col-md-4 col-xs-6">
				<label>&nbsp;</label>
								<div class="input text">
								<button data-dismiss="modal" class="btn btn-danger" id="calceldata"> Cancel</button>
							</div>
							</div>

    <?php echo $this->form->end(); ?>
				</div>
      </div>
    </div>
  </div>
</div>      
  
        <div class="row">
		  <div class="col-md-12">
          
        
           <?php echo $this->Form->create('RGroceryInvoice', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
           
           
           
           
        <div class="col-md-5" id="mpymt">
        <label class="nobg">Method Of Payment</label> <br/>    
        <input type="radio" name="data[RGroceryInvoice][mop]" value="" class="form-control" id="mop1"><label for="mop1" class="spciallab" id="lblmop1"> All Purchases</label>
        <input type="radio" name="data[RGroceryInvoice][mop]" value="Cash Paid" class="form-control" id="mop2" <?php if($mop=='Cash Paid'){echo 'checked'; } ?>><label for="mop2" class="spciallab" id="lblmop2">By Cash</label>
        <input type="radio" name="data[RGroceryInvoice][mop]" value="Check" class="form-control" id="mop3" <?php if($mop=='Check'){echo 'checked'; } ?>><label for="mop3" class="spciallab" id="lblmop3">By Check</label>
        <input type="radio" name="data[RGroceryInvoice][mop]" value="Credit" class="form-control" id="mop4" <?php if($mop=='Credit'){echo 'checked'; } ?>><label for="mop4" class="spciallab"  id="lblmop4">Unpaid </label>   
         <br/>
        </div>  
        <div class="clearfix"></div>   
		<div class="col-md-2">
        <label>Purchase Date</label>
        <?php /*?><input type="text" id="config-demo" name="data[RGroceryInvoice][form]" class="form-control" value="<?php echo $form; ?>"><?php */?>
		<?php include('test.ctp'); ?>
		<br/>
		</div>
        <div class="col-md-2" id="getplunos">
			<label>Vendor</label>
				<?php echo $this->Form->input('vendor', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'vendor','empty'=>'Vendor','options'=>$rGroceryvendor,'default'=>$vendor_id)); ?>
			<br/>
		</div>
		<div class="col-md-2" id="getplunos">
			<label>Check No#</label>
				<?php echo $this->Form->input('terms', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'terms','placeholder'=>'','value'=>$terms)); ?>
			<br/>
		</div>
        <div class="col-md-2" id="getplunos">
			<label>Search Key</label>
				<?php echo $this->Form->input('search_key', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'search_key','placeholder'=>'','value'=>$search_key)); ?>
			<br/>
		</div> 
		<div class="col-md-2">
			<label>Retail Status</label>
				<?php 
				$retail=array('1'=>'Retailed','2'=>'Partially Retailed','3'=>'Unretailed');
				echo $this->Form->input('status', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'status','options'=>$retail,'empty'=>'Retail Status','default'=>'')); ?>
			<br/>
		</div>
        <div class="col-md-1">
		  <label>&nbsp;</label>
			<?php //echo $this->Form->input('Search', array('class' => 'btn-success','type'=>'button', 'label' => false, 'required' => false,)); ?>
            <input type="submit" class="btn btn-success" value="Search"  />
			<br/>
		</div>
        <?php echo $this->form->end(); ?>
        <div class="col-md-1">
		<label>&nbsp;</label>
        <a href="<?php echo Router::url('/');?>admin/r_grocery_invoices/reset" class="btn btn-warning ">Reset</a>   
		<br/>
        </div>          
		</div>
		</div>       
        
       <?php
	   echo $this->Form->create('RGroceryInvoice', array('role' => 'form','type' => 'file','controller'=>'r_grocery_invoices','action'=>'admin_csv_import'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 
	   ?>           
       <div class="row">  
        <div class="col-md-12">
          <div class="col-md-2">
			<label>Vendor</label>
				<?php echo $this->Form->input('invoice_vendor', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'invoice_vendor','empty'=>'Select Vendor','options'=>$rGroceryvendor)); ?>
			<br/>
		</div>       
          <div class="col-md-4">
        <label>Upload Csv</label>
        <?php echo $this->Form->input('csv_file', array('id' => 'csv_file', 'class' => 'form-control','type' => 'file','label' => false)); ?>
        </div>        
          <div class="col-md-2">
        <label>&nbsp;</label>        
        <button type="submit" class="btn btn-block btn-info tank-filter" onclick="return vndrchk()">Upload</button>
        </div>
        </div>
      </div>  
     <?php echo $this->form->end(); ?>
        
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                          <?php /*?>  <th><?php echo __($this->Paginator->sort('id')); ?>  </th><?php */?>
                                            <th><?php echo $this->Paginator->sort('invoice_date','Invoice Date'); ?></th>
											 <th><?php echo $this->Paginator->sort('customer_id','Vendor '); ?></th>
                                            <!--<th><?php echo $this->Paginator->sort('store_id','Store Name'); ?></th>
											 --><th><?php echo $this->Paginator->sort('Invoice number'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Payment Type'); ?></th>
                                         <th><?php echo $this->Paginator->sort('total_count',' Total Count Delivered  '); ?></th>
                                         <th><?php echo $this->Paginator->sort('gross_amount'); ?></th>
                                            
                                         <th><?php echo $this->Paginator->sort('retail_value'); ?></th>
                                         <th><?php echo $this->Paginator->sort('margin'); ?></th>
                                         <th><?php echo $this->Paginator->sort('status'); ?></th>
                                         <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rgrossInvoice) && !empty($rgrossInvoice)) { ?>
                                        <?php $la=1; foreach ($rgrossInvoice as $data) {
											
											
											
	                $list = ClassRegistry::init('PurchasePacks')->find('all',array('conditions'=>array('PurchasePacks.company_id' => $this->Session->read('Auth.User.company_id'),'PurchasePacks.store_id' => $this->Session->read('stores_id'),'PurchasePacks.vendor' => $data['RGroceryInvoice']['vendor_id'],'PurchasePacks.invoice' => $data['RGroceryInvoice']['invoice']), "order" => "PurchasePacks.id DESC"));
				 
					$conditions = array('PurchasePacks.company_id' =>  $this->Session->read('Auth.User.company_id'),
					'PurchasePacks.store_id' => $this->Session->read('stores_id'),
					'PurchasePacks.vendor'=>$data["RGroceryInvoice"]["vendor_id"],
					'PurchasePacks.invoice'=>$data["RGroceryInvoice"]['invoice']
					);
					
					$grosstotal = ClassRegistry::init('PurchasePacks')->find('all',array('conditions'=>array_filter($conditions),'fields'=>array('sum(PurchasePacks.gross_amount) as totalgross'))); 
					
					$conditions = array('RGroceryInvoice.company_id' =>  $this->Session->read('Auth.User.company_id'),
					'RGroceryInvoice.store_id' => $this->Session->read('stores_id'),
					'RGroceryInvoice.vendor_id'=>$data['RGroceryInvoice']['vendor_id'],
					'RGroceryInvoice.invoice'=>$data['RGroceryInvoice']['invoice']
					);
					
					$totalinvoice = ClassRegistry::init('RGroceryInvoice')->find('all',array('conditions'=>array_filter($conditions),'fields'=>array('sum(RGroceryInvoice.gross_amount) as totalinvoice'))); 	
											
					$margin=0;	
					$cost_value=0;						
					$retail_value=0;											
					if (isset($list) && !empty($list)) { 
					//echo '<pre>';	print_r($list);	die;								  
					foreach ($list as $retaildata) { 
					$cost_value=$cost_value+$retaildata["PurchasePacks"]["Case_Cost"];
					$cost_value=round($cost_value,2);					
										
					$retail_value=$retail_value+$retaildata["PurchasePacks"]["Case_Retail"];
					$retail_value=round($retail_value,2);					
					}					
											
					$profit=$retail_value-round($data['RGroceryInvoice']['gross_amount'],2);
					$margin=(($retail_value-$cost_value)/$retail_value)*100;	
					$margin=round($margin,2);
					}
					
					
					$retail_status='';
					if(isset($totalinvoice)){			
			        $total_invoice_gross_amount=$totalinvoice[0][0]['totalinvoice'];
			        $total_pack_gross_amount=$grosstotal[0][0]['totalgross'];
					if($total_invoice_gross_amount==$total_pack_gross_amount){
					$retail_status='Retailed';
					}
					elseif($total_invoice_gross_amount>0 && $total_invoice_gross_amount!=$total_pack_gross_amount && $total_pack_gross_amount!=0){
					$retail_status='Partially Retailed';	
					}elseif($total_pack_gross_amount==0){
					$retail_status='Unretailed';
					}
					
				    } 
					
						
					?>
                                              <tr>
                                             <?php /*?><td> <?php echo h($data['RGroceryInvoice']['id']); ?>&nbsp;</td><?php */                                              ?>
                                              <td><?php if($data['RGroceryInvoice']['invoice_date']) echo  date('d F,Y', strtotime($data['RGroceryInvoice']['invoice_date']));  ?>&nbsp;</td>
											  <td><?php echo h($data['Customer']['name']); ?></td>
                                              <!--    <td><?php echo h($data['Store']['name']); ?>&nbsp;</td>
													--> <td><?php echo h($data['RGroceryInvoice']['invoice']); ?>&nbsp;</td>
                                                
                                              <td><?php echo h($data['RGroceryInvoice']['mop']); ?>&nbsp;</td>
                                              <td><?php echo h($data['RGroceryInvoice']['total_count']); ?>&nbsp;</td>
                                              <td><?php echo h($data['RGroceryInvoice']['gross_amount']); ?>&nbsp;</td>
                                                  
                                                  
                                              <td><?php echo $retail_value; ?>&nbsp;</td>
                                              <td><?php echo $margin; ?>&nbsp;</td>
                                              <td><?php echo $retail_status; ?>&nbsp;</td>
                                              
                                              
                                              <td class="action-buttons">													
													<img src="<?php echo Router::url('/');?>img/view.png"  data-toggle="collapse" data-target="#demo<?php echo $la; ?>" style="height: 20px; margin: 7px 6px;"   title="View Purchase Pack List"  class="accordion-toggle" />
													
													  <?php //echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>',array('action' => 'view', $data['RGroceryInvoice']['id']), array('class' => 'newicon red-stripe view','escape' => false,'data-toggle'=>'tooltip' ,'title'=>'View Purchase Pack List','data-placement'=>'left')); ?>
													  
													   <?php echo $this->Html->link('<i class="fa fa-pencil"></i>',array('action' => 'edit', $data['RGroceryInvoice']['id']), array('class' => 'newiconnn icon_image_style colornbn edit','escape' => false,'data-toggle'=>'tooltip' ,'title'=>'Edit','data-placement'=>'left')); ?>
													   
													    <?php 
														if($UsersDetails['role_id']!=6)
														{
														echo $this->Html->link('<i class="fa fa-trash-o"></i>',array('action' => 'delete', $data['RGroceryInvoice']['id']), array('class' => 'newiconnn1 icon_image_style colornbn delete','escape' => false,'data-toggle'=>'tooltip' ,'title'=>'Delete','data-placement'=>'left'));
														}
														?>
														
                                                        <?php if (!empty($data['RGroceryInvoice']['files'])) 
														{
														?>
                                                        <a href="<?php echo Router::url('/', true); ?>files/<?php echo $data['RGroceryInvoice']['files']; ?>" target="_blank" class="" style="padding:7px;" ><img src="<?php echo $this->webroot.'images/attachment.png'; ?>" style="height:17px;"></a>
                                                        <?php
														 }else
														 { 
														 ?>
                                <a href="javascript:void(0);" style="width:26px; opacity:0; height:10px;"></a>              
                                                        <?php
														 }
														 ?> 
                                                        <a href="javascript:void(0);"  onClick ="showmodel(<?php echo h($data['RGroceryInvoice']['invoice']); ?>,<?php echo h($data['RGroceryInvoice']['vendor_id']); ?>)"  class="" data-toggle="modal" data-target="#myModal" title="New Pack" style="padding:7px;margin-right:13px;" > <i class="fa fa-plus"></i>
                                                        </a>

                                                    </td>
                                              </tr>
												
        <tr>
            <td colspan="12" class="hiddenRow"><div class="accordian-body collapse" id="demo<?php echo $la; ?>"> 
              <table class="table table-striped">
                      <thead>
                        <tr>					
						                         <th>Item Scan Code </th>
												 <th>Item Name  </th>
												 <th>Units Per Case  </th>
												 <th>Case Cost  </th>
												 <th>Gross Amount  </th>
												 <th>Case Retail  </th>
												 <th>Unit Per Cost  </th>
												 <th>Purchase Qty#  </th>
                                                 <th>Margin</th>
                                                 <th  style="width:85px!important; display:block; overflow:hidden;">Actions</th>
</tr>
                      </thead>
                      <tbody>			

					<?php if(isset($totalinvoice)){?>
				<div class="row"><br/>
				</div>
				<div class="row"  style="background:#ccc">
				  <div class="col-md-4 col-xs-6">
					<h5>Invoice Gross Total = 	<?php if($totalinvoice[0][0]['totalinvoice']==0) echo '0'; else echo round($totalinvoice[0][0]['totalinvoice'],2); ?></h5>
				  </div>
				   <div class="col-md-4 col-xs-6">
					<h5>	Purchase Pack Gross Total = <?php if($grosstotal[0][0]['totalgross']==0) echo '0'; else  echo round($grosstotal[0][0]['totalgross'],2); ?></h5>
				  </div>
				   <div class="col-md-4 col-xs-6">
	<h5>	Difference = <?php $ab = round($totalinvoice[0][0]['totalinvoice'],2);
	$bc = round($grosstotal[0][0]['totalgross'],2);
	echo( $ab- $bc); ?></h5>
				  </div>
				</div>
				  <?php } ?>
		
		
		                    <?php 									
						    //pr($list);
							if (isset($list) && !empty($list)) { 									  
							$ms = "'Are you sure want delete it?'";
						    $Item_Name = '';
						    foreach ($list as $data) { 
												$Item_Id = $data["PurchasePacks"]["Item_Name"];												
												$RGroceryItemslist = ClassRegistry::init('RGroceryItem')->find('all',array('conditions'=>array('RGroceryItem.id' => $Item_Id)));
												if(isset($RGroceryItemslist[0])){													
												$Item_Name = $RGroceryItemslist[0]['RGroceryItem']['description'];
												}
												?>
                                               <tr>                                                      
                                                  <td><?php echo $data["PurchasePacks"]["Item_Scan_Code"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["Item_Name"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["Units_Per_Case"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["Case_Cost"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["gross_amount"];?></td>
                                                  <td><?php echo round($data["PurchasePacks"]["Case_Retail"],2);?></td>
                                                  <td><?php echo $data["PurchasePacks"]["Unit_per_cost"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["purchase"];?></td>
                                                  <td><?php echo $data["PurchasePacks"]["Margin"];?></td>
                                                  <td style="width:85px!important; display:block; overflow:hidden;">
                                                  	<a href="<?php echo Router::url('/').'admin/grocery/edit_purchasepack/'.$data["PurchasePacks"]["id"];?>" class="newiconp red-stripe" style="width:40%; float:left;"><img src="<?php echo Router::url('/');?>img/edit.png" data-toggle="tooltip" title="Edit" data-placement="left" /></a>
													<?php 
													if($UsersDetails["role_id"]!=6){
													?>
													<a href="<?php echo Router::url('/').'admin/grocery/deletepack/'.$data["PurchasePacks"]["id"];?>"  onClick="return  confirm(<?php echo $ms;?>);" class="newiconp red-stripe" style="width:40%; float:left;"><img src="<?php echo Router::url('/');?>img/delete.png" data-toggle="tooltip" title="Delete" data-placement="left"/></a>
													<?php 
													}
													?>
												
													</td>
												</tr>
											<?php
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">    
												<h3>No purchase packs data exists </h3>
        </td>
                                            </tr>';
                                        } ?>

                      
                    
                      </tbody>
               	</table>
              
              </div> </td>
        </tr>

                                            <?php $la++; } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>
<script>
function showmodel(invoice, vendor){
$('#vendor').val(vendor);
$('#invoice').val(invoice);
}
</script>
<script>
    $(document).ready(function(){
	 $('[data-toggle="tooltip"]').tooltip(); 
	$("form :input").attr("autocomplete", "off");
	$('#expiry').Zebra_DatePicker({format:'m-d-Y'});
	
	$("#expiry").attr("readonly", false); 
	
/*	$('#Case_Retail').keyup(function(){
			findretails();
	});
	
	$('#Case_Cost,#Units_Per_Case').keyup(function(){

			var Units_Per_Case = $('#Units_Per_Case').val();
			var Case_Cost = $('#Case_Cost').val();
			var unicost = Case_Cost/Units_Per_Case;
			
			if(Units_Per_Case !='' && Case_Cost != ''){
			findretailss();
			}else{
	$('#Case_Retail').val('');
	}
	});	*/
	
		$('#pul').keyup(function(){
  		var pul = $('#pul').val();
  $.ajax({
    url: '<?php echo Router::url('/') ?>admin/grocery/getdesc1',
	type: 'post',
	data: {pul:pul},
	success:function(data){  
   //  alert(data);  
     var res = $.parseJSON(data);  
	   // alert(res);      
     if(res){    
	 	if(res.result['trick']=='yes'){  
		var item_id = res.result['desc'];
			$('#item').val(res.result['Item']);
		$('#Case_Cost').val(res.result['price']);  
		
		}else{
			var item_id = res.result['id'];
		}
		//alert(item_id); 
		$('#description').select2().select2('val', item_id);
		$('#gid').val(res.result['id']);
		$('#Case_Retail').val(res.result['Case_Retail']);
      	$('#Units_Per_Case').val(res.result['unitpercase']); 
	//	$('#inventory').val(res.result['inven']);
		//$('#price').val(res.result['price']);	
		//alert(res.result['Item']);		
		 /* if(res.result['Item']=='' || res.result['Item']==null){	
			if (item_id!="") {			 
				var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
				$("#editbt").html(a);
				} else {
					$("#editbt").html('');
				}
			}*/
		//btn default btn-xs red-stripe
     }	  
	 else{
      $('#description').select2().select2('val','');
      $('#gid').val('');
      $('#item').val('');
      $('#inventory').val('');
      $('#price').val('');     
         }

     }
   }); 
/*   var price = $('#price').val();
   var Units_Per_Case = $('#Units_Per_Case').val();
   if(Units_Per_Case==""){
   Units_Per_Case= 0;
   }
    //alert(Units_Per_Case);
    var retail = ((price)*(Units_Per_Case)).toFixed(2);
    $('#Case_Retail').val(retail);
      var Case_Cost  = $('#Case_Cost').val();    
      var Case_Retail  = $('#Case_Retail').val(); 
      var margin = (((Case_Retail - Case_Cost)/Case_Retail)*100).toFixed(2);
      $('#Margin').val(margin);*/
             });	
			 
			 
	/*$('#pul').keyup(function(){
  	var pul = $('#pul').val();
    $.ajax({
    url: '<?php echo Router::url('/') ?>admin/grocery/getcost',
                type: 'post',
                data: {pul:pul},
                success:function(data){      
     var res = $.parseJSON(data);      
     if(res){     
		$('#Case_Cost').val(res.result['Case_Cost']);		
		$('#Units_Per_Case').val(res.result['Units_Per_Case']);
		$('#Case_Retail').val(res.result['Case_Retail']);	
     }else{
      $('#Case_Cost').val('');
	  $('#Units_Per_Case').val('');
      $('#Case_Retail').val(''); 
      }
     }
    }); 
 
   });		*/	 
			 
			 
		$('#description').change(function(){
		var id = $('#description').val();
		//alert(id);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/purchasegetplu1',
                type: 'post',
                data: {id:id},
                success:function(data){	
			//	alert(data);
				 var res = $.parseJSON(data);	
				// alert(res.result['plu_no']);			 	
				 if(res){
					if(res.result['trick']=='yes'){  
						$('#item').val(res.result['Item']);
						$('#Case_Cost').val(res.result['price']);  
						
					}else{
						
						$('#item').val('');
						$('#Case_Cost').val('');  
					}
				 		$('#pul').val(res.result['plu_no']);
						$('#gid').val(res.result['id']);
						/*$('#item').val(res.result['Item']);
						$('#inventory').val(res.result['inven']);
						$('#price').val(res.result['price']);		*/		
						$('#Case_Retail').val(res.result['Case_Retail']);		
						$('#Units_Per_Case').val(res.result['unitpercase']); 
						/*if(res.result['Item']=='' || res.result['Item']==null){	
						if (item_id!="") {			 
						var a="<a target='_blank' class=''  href='<?php echo Router::url('/').'admin/r_grocery_items/edit/' ?>"+item_id+"'>Edit</a>";				  				  			   
						$("#editbt").html(a);
						} else {
						$("#editbt").html('');
						}
						}	*/
										
					
				 }else{
				        $('#pul').val('');
						$('#gid').val('');
						$('#item').val('');
						$('#inventory').val('');
						$('#price').val('');
				 
				 }			
                }
            }); 
			
         });  	
		 
		 $('#item').keyup(function(){	
		        $("#editbt").html('');
				var item = $('#item').val();
		        //alert(item);
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/getplu1',
						type: 'post',
						data: {item:item},
						success:function(data){
						//alert(data);
						 var res = $.parseJSON(data);
						if(res){      
							var item_id = res.result['des'];
							$('#pul').val(res.result['plu']);
							$('#description').select2().select2('val', item_id);
							$('#Units_Per_Case').val(res.result['unirpercase']);						
							$('#Case_Cost').val(res.result['casecost']);
							$('#Case_Retail').val(res.result['caseretails']);					
						}
					}
				}); 
	
					
		});				
		 
		 
		 
		 
		 /*$('#description').change(function(){
			    var id = $('#description').val();			
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/grocery/getitemnamecost',
				type: 'post',
				data: {id:id},
				success:function(data){ 
				var res = $.parseJSON(data);      
				if(res){     
				$('#Case_Cost').val(res.result['Case_Cost']);
				$('#Case_Retail').val(res.result['Case_Retail']);	
				}else{
				$('#Case_Cost').val('');
				$('#Case_Retail').val(''); 
				}
				}
				}); 
				
				});	*/
		 
		/* 
		$('#item').keyup(function(){
			    var item = $('#item').val();
				$.ajax({
				url: '<?php //echo Router::url('/') ?>admin/grocery/getitemcost',
				type: 'post',
				data: {item:item},
				success:function(data){      
				var res = $.parseJSON(data);      
				if(res){     
				$('#Case_Cost').val(res.result['Case_Cost']);
				$('#Case_Retail').val(res.result['Case_Retail']);	
				}else{
				$('#Case_Cost').val('');
				$('#Case_Retail').val(''); 
				}
				}
				}); 
				
				});	 */
					 
					 
					 
					 
		$('#newandsave').click(function(){
		z =  document.getElementById('Units_Per_Case').value
        y =  document.getElementById('Case_Cost').value
        x =  document.getElementById('gross_amount').value
        p =  document.getElementById('purchase').value
    if(!z.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && z!='')
        {
        alert("Units Per Case alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
        }else if(!y.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && y!=''){
        alert("Case Cost alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else if(!x.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && x!=''){
        alert("Gross amount alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else if(!p.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && p!=''){
        alert("Purchase quantity alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else{
		
		var vendor = $('#vendor').val();
		var Item_Scan_Code = $('#pul').val();
		var invoice = $('#invoice').val();
		var Item_Name = $('#description').val();
		var Item = $('#item').val();
		var Case_Cost = $('#Case_Cost').val();
		var Units_Per_Case = $('#Units_Per_Case').val();
        var Case_Retail = $('#Case_Retail').val();
		var purchase = $('#purchase').val();
		var purchase_date = $('#expiry').val();		
		var gross_amount = $('#gross_amount').val();
        if(!Item_Scan_Code){
					alert('Please fill all fields');

        }else if(!Case_Cost){
					alert('Please fill all fields');

        }else if(!Units_Per_Case){
					alert('Please fill all fields');

        }else if(!Case_Retail){
					alert('Please fill all fields');

        }else{
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/new_pack_ajax',
						type: 'post',
			data:{Item_Scan_Code:Item_Scan_Code,vendor:vendor,invoice:invoice,Item_Name:Item_Name,Item:Item,Case_Cost:Case_Cost,Units_Per_Case:Units_Per_Case,Case_Retail:Case_Retail,purchase:purchase,purchase_date:purchase_date,gross_amount:gross_amount},
						success:function(data){
						alert(data);
						$('#pul').val('');
						$('#description').select2().select2('val','');
						$('#item').val('');
						$('#Case_Cost').val('');
						$('#Units_Per_Case').val('');
						$('#Case_Retail').val('');
						$('#purchase').val('');
						$('#expiry').val('');
						$('#gross_amount').val('');
						}
					}); 
	
					}
					
				}			
			
			});
					 
		$('#saveonly').click(function(){
		
		z =  document.getElementById('Units_Per_Case').value
y =  document.getElementById('Case_Cost').value
x =  document.getElementById('gross_amount').value
p =  document.getElementById('purchase').value
    if(!z.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && z!='')
        {
        alert("Units Per Case alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
        }else if(!y.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && y!=''){
        alert("Case Cost alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else if(!x.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && x!=''){
        alert("Gross amount alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else if(!p.match(/^\d{0,10}(\.\d{1})?\d{0,3}$/) && p!=''){
        alert("Purchase quantity alowed maximum 10 digits before decimal point and after 4digits.");
        return false;
		}else{
		
		var vendor = $('#vendor').val();
		var Item_Scan_Code = $('#pul').val();
		var invoice = $('#invoice').val();
		var Item_Name = $('#description').val();
		var Item = $('#item').val();
		var Case_Cost = $('#Case_Cost').val();
		var Units_Per_Case = $('#Units_Per_Case').val();
/*		var Net_Cost = $('#Net_Cost').val();
		var Discount = $('#Discount').val();
		var Rebate = $('#Rebate').val();
	*/	var Case_Retail = $('#Case_Retail').val();
		/*var Margin = $('#Margin').val();
	*/	var purchase = $('#purchase').val();
		var purchase_date = $('#expiry').val();
		/*var Unit_per_cost = $('#unitpercost').val();
		var pricevalue = $('#price').val();
		var inventory = $('#inventory').val();
		*/
		var gross_amount = $('#gross_amount').val();
if(!Item_Scan_Code){
					alert('Please fill all fields');

}else if(!Case_Cost){
					alert('Please fill all fields');

}else if(!Units_Per_Case){
					alert('Please fill all fields');

}else if(!Case_Retail){
					alert('Please fill all fields');

}else{
				$.ajax({
						url: '<?php echo Router::url('/') ?>admin/grocery/new_pack_ajax',
						type: 'post',
			data:{Item_Scan_Code:Item_Scan_Code,vendor:vendor,invoice:invoice,Item_Name:Item_Name,Item:Item,Case_Cost:Case_Cost,Units_Per_Case:Units_Per_Case,Case_Retail:Case_Retail,purchase:purchase,purchase_date:purchase_date,gross_amount:gross_amount},
						success:function(data){
						alert(data);
						window.location.reload();
						}
					}); 
	
					}
					
					}
					 });
$('#calceldata').click(function(){
cleardata();
window.location.reload();
});				

$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
			cleardata();
    }
});
			 });
function findretails(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	if(Units_Per_Case=='' || Case_Cost == ''){
	alert('Please enter case cost and unit per case values');	
	$('#Case_Retail').val('');
	}
	
	var Case_Retail = $('#Case_Retail').val();
	if(Case_Retail!='' && !$.isNumeric(Case_Retail)){
	alert('Please enter only numeric value');	
	$('#Case_Retail').val('');	
	}
	

}	

function findretailss(){
	var Units_Per_Case = $('#Units_Per_Case').val();
	var Case_Cost = $('#Case_Cost').val();
	var unicost = Case_Cost/Units_Per_Case;
	var retail = (unicost)*(Units_Per_Case);
	$('#Case_Retail').val(retail);	
}



	function cleardata(){
		
$('#pul').val('');
$('#description').val('');
$('#item').val('');
$('#Case_Cost').val('');
$('#Units_Per_Case').val('');
/*$('#Net_Cost').val('');
$('#Discount').val('');
$('#Rebate').val('');*/
$('#Case_Retail').val('');
/*$('#Margin').val('');*/
$('#purchase').val('');
$('#expiry').val('');
/*$('#unitpercost').val('');
$('#price').val('');
$('#inventory').val('');
*/
$('#gross_amount').val('');
		
		}



</script>

<?php
$mop = $this->Session->read('mop'); 
		//if(isset($mop) && $mop!="") { 
		echo $mop;
		//$radio_id = '';
		if($mop == '') {
			echo $radio_id = 'mop1';
			?>
            <script type="text/javascript">
				$(function() {
				 $('#lblmop1').addClass('active'); 
				});
           </script>
            <?php
		} else if($mop == 'Cash Paid') {
			echo $radio_id = 'mop2';?>
            <script type="text/javascript">
				$(function() {
				 $('#lblmop2').addClass('active');
				});
           </script>
            <?php
		}
		else if($mop == 'Check') {
			echo $radio_id = 'mop3';
			?>
            <script type="text/javascript">
				$(function() {
				 $('#lblmop3').addClass('active');
				});
           </script>
            <?php
		}
		else if($mop == 'Credit') {
			echo $radio_id = 'mop4';
		?>
			<script type="text/javascript">
				$(function() {
				 $('#lblmop4').addClass('active');
				});
           </script>
        <?php }?>
		
        <script type="text/javascript">
			$(function() {
				$('.spciallab').click( function() { //alert('lll');
				$(this).addClass('active').siblings().removeClass('active');
				});
			});
        </script>

<script>
function vndrchk()
{
var vndr=$('#invoice_vendor').val();
var csv=$('#csv_file').val();
if(vndr=='')
{
alert('Please select vendor');
return false;
}
if(csv=='')
{
alert('Please choose a csv file');
return false;
}
}
</script>

 <?php 
if($form=='')
{
?>        
<script>
$(document).ready(function(){
$('#config-demo').val('');		
});
</script>
<?php 
}
?>