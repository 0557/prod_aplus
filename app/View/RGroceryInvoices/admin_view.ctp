<?php //pr($rgrossInvoice);  ?> 
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View R Grocery Purchase INVOICE Info : <?php echo $rgrossInvoice['RGroceryInvoice']['id']; ?>
                    </div>

                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Order Details
                                                    </div>

                                                </div>
                                                <div class="portlet-body">

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Date Of Invoice:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($rgrossInvoice['RGroceryInvoice']['invoice_date'])); ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Po:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($rgrossInvoice['RGroceryInvoice']['po']); ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rgrossInvoice['RGroceryInvoice']['invoice']; ?>
                                                        </div>
                                                    </div>




                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Status:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="label label-success">
                                                                <?php echo $rgrossInvoice['RGroceryInvoice']['status']; ?>
                                                            </span>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Vendor Details
                                                    </div>
                                                    <div class="actions">

                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Vendor: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rgrossInvoice['Customer']['name']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Terms:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rgrossInvoice['RGroceryInvoice']['terms']; ?>
                                                        </div>
                                                    </div>



                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Comments:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rgrossInvoice['RGroceryInvoice']['comments']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Upload file:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php if (!empty($rgrossInvoice['RGroceryInvoice']['files'])) { ?>
                                                                <a href="<?php echo Router::url('/', true); ?>uploads/rgroceryinvoice/<?php echo $rgrossInvoice['RGroceryInvoice']['files']; ?>" target="_blank" class="btn default btn-xs red-stripe">View File</a>
                                                            <?php } else { ?>
                                                                <span class="btn default btn-xs red-stripe"> No  File  </span>
                                                            <?php } ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['comments']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
    <!-- END PAGE CONTENT-->
<div class="row">
        <?php /* ?><div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i> Grocery Purchase Department
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>  Department</th>	
                                    <th> Base/Inner Upsc </th>
                                    <th> Description  </th>
                                    <th>Count(Qty) </th>
                                    <th>Price</th>
                                    <th>Total Cost </th>
                                </tr>
                            </thead>
                            <?php $total = 0; ?>
                            <tbody>

                                <?php if (isset($rgrossInvoice['RGroceryInvoiceProduct']) && !empty($rgrossInvoice['RGroceryInvoiceProduct'])) { ?>
                                    <?php
                                    foreach ($rgrossInvoice['RGroceryInvoiceProduct'] as $key => $data) {

                                        $nameAndId = classregistry::init('RGroceryDepartment')->find('first', array('conditions' => array('RGroceryDepartment.id' => $data['r_grocery_department_id']), 'fields' => array('name')));
                                        ?>            
                                        
                                        <tr>
                                            <td><?php if (!empty($nameAndId)) {
                                                        echo $nameAndId['RGroceryDepartment']['name'];
                                                } ?>
                                            </td>
                                            <td><?php echo $ItemDescription = $this->Custom->GroceryItemName($data['r_grocery_item_id']); ?>&nbsp;</td> 
                                            <td><?php echo $data['description']; ?>&nbsp;</td>
                                            <td><?php echo $data['count']; ?>&nbsp;</td>
                                            <td><?php echo $data['price']; ?>&nbsp;</td>
                                            <td><?php echo $data['total_cost']; ?>&nbsp;</td>



                                        </tr>                      
    <?php }
} ?>


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
 <?php */?>
        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
            <div class="portlet purple box">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                <div class="row">
        <ul class="list-unstyled amounts total_last  col-md-12 ">
            <li class="last_content_first">
                <span class="span_1"><b>Total Count Delivered: </b></span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $rgrossInvoice['RGroceryInvoice']['total_count']; ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1"><b>Net Amount Total: </b></span><span class="span_2" id="Net_Amount_Total"><?php echo $rgrossInvoice['RGroceryInvoice']['net_amount']; ?> </span>
            </li>
            <li class="last_content_three">
                <span class="span_1"><b>Taxes:</b> </span><span class="span_2" id="Taxes"><?php echo $rgrossInvoice['RGroceryInvoice']['taxes']; ?></span>
            </li>
            <li class="last_content_four">
                <span class="span_1"><b>Gross Amount After Adding taxes:</b> </span><span class="span_2" id="Taxes"><?php echo $rgrossInvoice['RGroceryInvoice']['gross_amount']; ?></span>
            </li>
            <li class="last_content_five">
                <div class="credit_span"><span class="span_1"><b>MOP:</b>  </span><span class="span_2" id="MOP">
<?php echo $rgrossInvoice['RGroceryInvoice']['mop']; ?>
                </span></div>
            </li>
        </ul>


					</div>
				</div>
			</div>
		</div>
		</div>
    </div>

