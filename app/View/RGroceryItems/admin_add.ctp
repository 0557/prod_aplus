<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue" >
    <div class="page-content portlet-body" >

        <div class="row" style="border:1px solid #d9d9d9;">
            <div class="col-md-12 " style="padding-left: 0px;padding-right: 0px">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box" style="background-color:#dcdadd">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Add Grocery Item
                        </div>
                    </div></div></div>

            <div class="form-body col-md-12">
                <div class="row">

                    <div class="col-md-2 value">
                        <label>Department:</label>
                        <?php echo $this->Form->input('r_grocery_department_id', array('class' => 'form-control', 'empty' => 'Select Department', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>

                    </div>

                    <div class="col-md-2 value">
                        <label>Plu No:</label>
                        <?php echo $this->Form->input('plu_no', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'pul', 'placeholder' => 'Enter PLU No')); ?>

                    </div>


                    <div class="col-md-3 value">
                        <label>Description :</label>
                        <?php echo $this->Form->input('description', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'description', 'placeholder' => 'Enter Description ')); ?>

                    </div>

                    <div class="col-md-3 value">
                        <label>Primary Vendor:</label>
                        <?php echo $this->Form->input('vendor', array('class' => 'form-control', 'options' => $vendor, 'empty' => 'Select vendor', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>

                    </div>
                </div>
                <br>
                <div class="row">

                    <div class="col-md-2 ">
                        <label>Fee/Charge:</label>
                        <?php echo $this->Form->input('fee', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Fee/Charge')); ?>
                    </div>

                    <div class="col-md-2 value">
                        <label>Price:</label>
                        <?php echo $this->Form->input('price', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Price')); ?>

                    </div>
                    <div class="col-md-2 value">
                        <label>Starting Inventory:</label>
                        <?php echo $this->Form->input('current_inventory', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Starting Inventory')); ?>

                    </div>

                    <div class="col-md-2 value">
                        <label>Units/Case:</label>
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('unit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Sell Unit')); ?>
                    </div>

                    <div class="col-md-2 value">
                        <label>Margin %:</label>
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('margin', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Margin%')); ?>

                    </div>

                    <div class="col-md-2 value">
                        <label>Profit Amount:</label>
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('profit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Profit Amount')); ?>
                    </div>




                </div>

                <br>
                <div class="row">    
                    <style>
                        .plubtns
                        {
                            width:12%;
                        }

                    </style>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-3 plubtns1" >

                                <label>Age Restriction:</label>
                                <?php echo $this->Form->input('age_restriction', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                            </div>

                            <div class="col-md-3 plubtns1">
                                <label>Food Stamp:</label>
                                <?php echo $this->Form->input('food_stamp', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                            </div>

                            <div class="col-md-3 plubtns1">
                                <label>Plu in promotion:</label>
                                <span class="switch-left switch-primary"></span>
                                <span class="switch-right switch-info "></span>
                                <?php echo $this->Form->input('plu_promotion', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                            </div>
                            <div class="col-md-3 plubtns1">
                                <label>Plu may be Sold:</label>
                                <span class="switch-left switch-primary"></span>
                                <span class="switch-right switch-info "></span>
                                <?php echo $this->Form->input('plu_sold', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="row">    
                            <div class="col-md-4 plubtns1">
                                <label>Plu may be Return:</label>
                                <span class="switch-left switch-primary"></span>
                                <span class="switch-right switch-info "></span>
                                <?php echo $this->Form->input('plu_return', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                            </div>

                            <div class="col-md-4 plubtns1">
                                <label>Plu Open:</label>
                                <span class="switch-left switch-primary"></span>
                                <span class="switch-right switch-info "></span>
                                <?php echo $this->Form->input('plu_open', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                            </div>

                            <div class="col-md-4 plubtns1">
                                <label>Plu Tax:</label>
                                <span class="switch-left switch-primary"></span>
                                <span class="switch-right switch-info "></span>
                                <?php echo $this->Form->input('plu_tax', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
                <br>
                <br>
                <div class="row">


                    <div class="form-actions" style="text-align:center;">
                        <div class="col-md-4 value " >

                        </div>
                        <div class="col-md-2 value " style="text-align:right;padding-top:6px">
                            
                            <b>Send To POS:</b>
                            <span class="switch-left switch-primary"></span>
                            <span class="switch-right switch-info " ></span>
                            <?php echo $this->Form->input('pos_refresh', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                        </div>
                        <div class="col-md-6 value " style="text-align:left;">
                            <br>
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="reset" class="btn default">Reset</button>
                            <button type="button" onclick="javascript:history.back(1)"
                                    class="btn default">Cancel</button>
                        </div>


                        <!--<button type="button" class="btn default">Cancel</button>-->
                    </div>
                </div>    

            </div>


        </div>
        <?php echo $this->form->end(); ?>

    </div>
</div>
<script type="text/javascript">
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    $("select").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    $('#RGroceryItemPrice ,#RGroceryItemMargin').keyup(function () {
        var price = $('#RGroceryItemPrice').val();
        price = (isNaN(price) || price != '') ? price : 0;
        var margin = $('#RGroceryItemMargin').val();
        margin = (isNaN(margin) || margin != '') ? margin : 0;
        var profit = parseFloat(price) * parseFloat(margin / 100);
        $('#RGroceryItemProfit').val(profit.toFixed(2));


    });
</script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#pul').keyup(function () {

            var pul = $('#pul').val();
//		alert(pul);
            $.ajax({
                url: '<?php echo Router::url('/') ?>admin/r_grocery_items/getdesc',
                type: 'post',
                data: {pul: pul},
                success: function (data) {
                    //alert(data);
                    $('#description').val(data);
                    /*	getplu();*/
                }
            });
        });
    });

</script>
