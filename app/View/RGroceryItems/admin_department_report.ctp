<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">Starting Inventory report 
<input type="button" class="btn btn-warning pull-right" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
		  <div class="col-md-12">
		    <div class="col-md-3">
			<label>Department</label>
		<?php echo $this->Form->input('department', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'department','options'=>$departmentlist,'empty'=>'','default'=>'')); ?>
		<br/>
		</div>
		</div>
		</div>
		
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">
<div id="loading-image" style="display:none">
           <h3 style="text-align:center"> Loading...</h3>
            </div>
                    <div class="tab-content">

                        <div class="tab-pane1" >

                            <div  id="getdata_wrapper" class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
                                    <thead>
                                        <tr>
                                            <th> Department <?php //echo $this->Paginator->sort('department', 'Department'); ?></th>
                                            <th>PLU No <?php //echo $this->Paginator->sort('plu_no', 'PLU No'); ?></th>
                                            <th> Description <?php //echo $this->Paginator->sort('description'); ?></th>
                                         <th>Starting Inventory <?php //echo $this->Paginator->sort('current_inventory', 'Starting Inventory'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rGroceryItems) && !empty($rGroceryItems)) { ?>
                                            <?php foreach ($rGroceryItems as $data) { ?>
                                                <tr>
													<td><?php echo h(@$departmentlist[$data['RGroceryItem']['r_grocery_department_id']]); ?>&nbsp;</td>
                                                    <td><?php 
													
													echo h($data['RGroceryItem']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['description']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['current_inventory']); ?>&nbsp;</td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="4">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('#department option[value=""]').attr('selected','selected');
	$('#department').change(function(){
		var dep = $('#department').val();
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/r_grocery_items/getitemsreport',
                type: 'post',
                data: { id: dep},
				 beforeSend: function() {
					  $("#loading-image").show();
				   },
                success:function(data){
//				alert(data);
 $('[data-toggle="tooltip"]').tooltip();
					$('#getdata_wrapper').html(data);
					$("#loading-image").hide();
					$(".pagination").hide();
				/*	getplu();*/
                }
            });
		});
		
		
    });

</script>

<style>
#getdata th.actionicons, #getdata td.actionicons {
    width: 153px !important;
}
#loading-image {
    background: #fff none repeat scroll 0 0;
    bottom: 0;
    left: 0;
    opacity: 0.8;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 9999;
}
#loading {
    width: 50px;
    height: 57px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}

</style>

<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()

//, attr, order
 sortSelect('#department', 'text', 'asc');

var sortSelect = function (select, attr, order) {
    if(attr === 'text'){
        if(order === 'asc'){
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end asc
        if(order === 'desc'){
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
        }// end desc
    }

};

</script>
