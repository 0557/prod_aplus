<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue" style="border:2px solid #717171">
    <div class="page-content portlet-body" >

        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box" style="background-color:#dcdadd">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i> Add Grocery Item
                        </div>
                    </div></div></div>

            <div class="form-body col-md-12">
                <div class="row">

                    <div class="col-md-3 value">
                        Department:
                        <?php echo $this->Form->input('r_grocery_department_id', array('class' => 'form-control', 'empty' => 'Select Department', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>
                        <?php echo $this->Form->input('id'); ?>
                    </div>

                    <div class="col-md-3 value">
                        PLU No:
                        <?php echo $this->Form->input('plu_no', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'pul', 'placeholder' => 'Enter PLU No')); ?>

                    </div>


                    <div class="col-md-3 value">
                        Description :
                        <?php echo $this->Form->input('description', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'id' => 'description', 'placeholder' => 'Enter Description ')); ?>

                    </div>

                    <div class="col-md-3 value">
                        Primary Vendor:
                        <?php echo $this->Form->input('vendor', array('class' => 'form-control', 'options' => $vendor, 'empty' => 'Select vendor', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>

                    </div>
                </div>
                <br>
                <div class="row">

                    <div class="col-md-2 ">
                        Fee/Charge:
                        <?php echo $this->Form->input('fee', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Fee/Charge')); ?>
                    </div>

                    <div class="col-md-2 value">
                        Price:
                        <?php echo $this->Form->input('price', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Price')); ?>

                    </div>
                    <div class="col-md-2 value">
                        Starting Inventory:
                        <?php echo $this->Form->input('current_inventory', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Starting Inventory')); ?>

                    </div>

                    <div class="col-md-2 value">
                        Units/Case:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('unit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Sell Unit')); ?>
                    </div>

                    <div class="col-md-2 value">
                        Margin %:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('margin', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Margin%')); ?>

                    </div>

                    <div class="col-md-2 value">
                        Profit Amount:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('profit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Profit Amount')); ?>
                    </div>




                </div>

                <br>
                <div class="row">    
                    <style>
                        .plubtns
                        {
                            width:12%;
                        }

                    </style>
                    <div class="col-md-2 plubtns" >
                        Age Restriction:
                        <?php echo $this->Form->input('age_restriction', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                    </div>

                    <div class="col-md-2 plubtns">
                        Food Stamp:
                        <?php echo $this->Form->input('food_stamp', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                    </div>

                    <div class="col-md-2 plubtns">
                        Plu in promotion :
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('plu_promotion', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                    </div>
                    <div class="col-md-2 plubtns">
                        Plu may be Sold:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('plu_sold', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                    </div>
                    <div class="col-md-2 plubtns">
                        Plu may be Return:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('plu_return', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                    </div>

                    <div class="col-md-2 plubtns">
                        Plu Open:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('plu_open', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                    </div>

                    <div class="col-md-2 plubtns">
                        Plu Tax:
                        <span class="switch-left switch-primary"></span>
                        <span class="switch-right switch-info "></span>
                        <?php echo $this->Form->input('plu_tax', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                    </div>

                    <!-- END FORM-->
                </div>
                <br>
                <br>
                <div class="row">


                    <div class="form-actions" style="text-align:center;">
                        <div class="col-md-4 value " >

                        </div>
                        <div class="col-md-2 value " style="text-align:right;">
                            Send To POS:
                            <span class="switch-left switch-primary"></span>
                            <span class="switch-right switch-info "></span>
                            <?php echo $this->Form->input('pos_refresh', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                        </div>
                        <div class="col-md-6 value " style="text-align:left;">
                            <br>
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="reset" class="btn default">Reset</button>
                            <button type="button" onclick="javascript:history.back(1)"
                                    class="btn default">Cancel</button>
                        </div>


                        <!--<button type="button" class="btn default">Cancel</button>-->
                    </div>
                </div>    

            </div>


        </div>
        <?php echo $this->form->end(); ?>

    </div>
</div>
<script type="text/javascript">
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    $("select").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    $('#RGroceryItemPrice ,#RGroceryItemMargin').keyup(function () {
        var price = $('#RGroceryItemPrice').val();
        price = (isNaN(price) || price != '') ? price : 0;
        var margin = $('#RGroceryItemMargin').val();
        margin = (isNaN(margin) || margin != '') ? margin : 0;
        var profit = parseFloat(price) * parseFloat(margin / 100);
        $('#RGroceryItemProfit').val(profit.toFixed(2));


    });
</script>


