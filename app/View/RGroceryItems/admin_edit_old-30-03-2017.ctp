<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>       
<?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Grocery Item
                    </div>
                </div></div></div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Department:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('r_grocery_department_id', array('class' => 'form-control','empty'=>'Select Department' ,'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>
                                    <?php echo $this->Form->input('id'); ?>
                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU No:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('plu_no', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter PLU No')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    EAN 13 Digit:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('cost', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'EAN 13 Digit')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Description :			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('description', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Description ')); ?>

                                </div>
                            </div>
                            
                         <?php /*?>   <div class="row static-info">
                                <div class="col-md-5 name">
                                    Item#:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('Item#', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter item')); ?>
                                </div>
                            </div><?php */?>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Price:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('price', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Price')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fee/Charge:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('fee', array('class' => 'form-control', 'type' => 'text', 'required' => 'false', 'label' => false, 'placeholder' => 'Fee/Charge')); ?>
                                </div>
                            </div>

							 <div class="row static-info">
                                <div class="col-md-5 name">
                                    Starting Inventory:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('current_inventory', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Starting Inventory')); ?>

                                </div>
                            </div>
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    Primary Vendor:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('vendor', array('class' => 'form-control','options'=>$vendor,'empty'=>'Select vendor' ,'label' => false, 'required' => 'false', 'placeholder' => 'Enter Department ')); ?>

                                </div>
                            </div>
							
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    Send To POS:
                                </div>
                                 <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('pos_refresh', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body">
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    Age Restriction:
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('age_restriction', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
							<div class="row static-info">
                                <div class="col-md-5 name">
                                   Food Stamp:
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('food_stamp', array('class' => 'make-switch', 'div' => false, 'data-on' => "yes", 'data-off' => "no", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU in promotion :
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('plu_promotion', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   PLU may be Sold:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('plu_sold', array('class' => 'make-switch', 'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU may be Return:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('plu_return', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
							
							
							<div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU Open:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('plu_open', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
							
							 <div class="row static-info">
                                <div class="col-md-5 name">
                                    PLU Tax:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('plu_tax', array('class' => 'make-switch', 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>
                            </div>

							

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Sell Unit:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('unit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Sell Unit')); ?>
                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Margin %:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('margin', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Margin%')); ?>

                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Profit Amount:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('profit', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Profit Amount')); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END FORM-->
            </div>


        </div>
        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit" class="btn blue">Submit</button>
            <button type="reset" class="btn default">Reset</button>
            <button type="button" onclick="javascript:history.back(1)"
                    class="btn default">Cancel</button>

            <!--<button type="button" class="btn default">Cancel</button>-->
        </div>

    </div>
    <?php echo $this->form->end(); ?>

</div>
<script type="text/javascript">
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    $("select").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
      $('#RGroceryItemPrice ,#RGroceryItemMargin').keyup(function(){
            var price =  $('#RGroceryItemPrice').val();
            price = (isNaN(price) || price != '')? price : 0 ;
            var margin =  $('#RGroceryItemMargin').val();
            margin = (isNaN(margin) || margin != '') ? margin : 0 ;
            var profit = parseFloat(price) * parseFloat(margin/100);
           $('#RGroceryItemProfit').val((profit).toFixed(2)); 
    });
    
</script>


