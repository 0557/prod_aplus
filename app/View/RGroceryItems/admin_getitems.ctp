<div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered table-advance table-hover" id="">
                                        <thead>
                                            <tr>
                                                <th class="no-sort"> <?php //echo $this->Form->input('all', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'id' =>'selecctall'));     ?> 

                                                    <input type="checkbox" name="all" id="selecctall"/>  </th>
                                                <th><?php echo $this->Paginator->sort('department', 'Department'); ?></th>
                                                <th><?php echo $this->Paginator->sort('plu_no', 'PLU No'); ?></th>
                                                <th><?php echo $this->Paginator->sort('description'); ?></th>
                                                <th><?php echo $this->Paginator->sort('price'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Inventory'); ?></th>
                                                <th><?php echo $this->Paginator->sort(' On-Hand'); ?></th>
                                             <!--   <th><?php echo $this->Paginator->sort('current_inventory', 'Starting Inventory'); ?></th>-->
                                                <th> Margin </th>
                                                <th class="no-sort"> 
                                                    Actions 

                                                    <span class="delete_span">
                                                     <?php echo $this->Form->create('RGroceryItem', array('action' => 'delete_multiple', 'id' => 'multiple_delete_form', 'method' => 'post')); ?>
														<?php echo $this->Form->input('deleted_items', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'id' => 'deleted_items')); ?>
														<?php echo $this->Form->postLink(__('<img src="' . Router::url('/') . 'img/delete_multiple.png"/>'), array($data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon2 red-stripe delete_multiple', 'title' =>'Multiple Delete')); ?>
														<?php echo $this->Form->end(); ?>
                                                    </span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($rGroceryItems) && !empty($rGroceryItems)) { ?>
                                                <?php foreach ($rGroceryItems as $data) { ?>
                                                    <tr>
                                                        <td > <?php //echo $this->Form->input('id[]', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'value' => $data['RGroceryItem']['id']));    ?>
                                                            <input type="checkbox" class="delete_checkbox" name="id[]"  value="<?php echo $data['RGroceryItem']['id']; ?>"/>
                                                        </td>
                                                        <td><?php echo h(@$departmentlist[$data['RGroceryItem']['r_grocery_department_id']]); ?>&nbsp;</td>
                                                        <td><?php echo h($data['RGroceryItem']['plu_no']); ?>&nbsp;</td>
                                                        <td><?php echo h($data['RGroceryItem']['description']); ?>&nbsp;</td>
                                                        <td>$<?php echo h($data['RGroceryItem']['price']); ?>&nbsp;</td>
                                                        <td>
                                                            <?php
                                                            if (isset($rubyid)) {
                                                                $findid = ClassRegistry::init('RubyPluttl')->find('first', array('conditions' => array('plu_no' => $data['RGroceryItem']['plu_no'], 'RubyPluttl.store_id' => $this->Session->read('stores_id')), 'limit' => '1', 'order' => array('RubyPluttl.id' => 'desc'), 'joins' => array(array(
                                                                            'table' => 'ruby_headers',
                                                                            'alias' => 'ruby_heade',
                                                                            'type' => 'INNER',
                                                                            'conditions' => array('ruby_heade.id = RubyPluttl.ruby_header_id', 'ruby_heade.id=' . $rubyid),
                                                                            'order' => array('ending_date_time' => 'desc')))));

                                                                if (@$findid['RubyPluttl']['no_of_items_sold'])
                                                                    echo '
<a href="#" data-toggle="tooltip" title="Today Sale is ' . floor(@$findid['RubyPluttl']['no_of_items_sold']) . '"><i class="fa fa-rss"></i></a>
';                                                            } 
															
											$userObj = ClassRegistry::init('Ruby2Livepos');
											$GroceryItemCount=$userObj->find('all', array('conditions'=>array('Ruby2Livepos.upc' =>$data['RGroceryItem']['plu_no'],'Ruby2Livepos.store_id' =>$this->Session->read('stores_id')),'order'=>array('Ruby2Livepos.periodBeginDate'=>'desc')));			
											//echo '<pre>';print_r($GroceryItemCount);die;				
											if($GroceryItemCount[0]['Ruby2Livepos']['itemCount']){
											$ItemCount=$GroceryItemCount[0]['Ruby2Livepos']['itemCount'];
											}else{
											$ItemCount=0;
											}
											echo '<a href="#" data-toggle="tooltip" title="Today Sale is '.$ItemCount.'"><i class="fa fa-rss"></i></a>';
                                            ?>
                                            </td>

                                                        <td><?php
                                                            $data['RGroceryItem']['sales_inventory'] = @$pluItemBySale[$data['RGroceryItem']['plu_no']];

                                                            if ($data['RGroceryItem']['sales_inventory'] == "")
                                                                $data['RGroceryItem']['sales_inventory'] = 0;

                                                            echo ($data['RGroceryItem']['current_inventory'] + $data['RGroceryItem']['purchase']) - $data['RGroceryItem']['sales_inventory'];
                                                            ?></td>
                                                        <td><?php
                                                            $margin = ClassRegistry::init('PurchasePack')->find('first', array('conditions' => array('Item_Scan_Code' => $data['RGroceryItem']['plu_no'], 'PurchasePack.store_id' => $this->Session->read('stores_id')), 'order' => array('PurchasePack.id' => 'desc')));

                                                            echo @$margin['PurchasePack']['Margin'];
                                                            ?></td>
                                                        <td class="actionicons">

                                                            <?php echo $this->Html->link('<img src="' . Router::url('/') . 'img/view.png"/>', array('action' => 'view', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe view')); ?>
                                                            <?php echo $this->Html->link('<img src="' . Router::url('/') . 'img/edit.png"/>', array('action' => 'edit', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe edit')); ?>
                                                            <?php echo $this->Form->postLink(__('<img src="' . Router::url('/') . 'img/delete.png"/>'), array('action' => 'delete', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe delete')); ?>

                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="10">  no result found </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <script type="text/javascript">
    function updateTextArea() {
        var allVals = [];
        jQuery('.delete_checkbox:checked').each(function () {
            allVals.push(jQuery(this).val());
        });
        jQuery('#deleted_items').val(allVals);
    }

    jQuery(document).ready(function () {
        jQuery('.delete_checkbox').click(updateTextArea);
        jQuery('#selecctall').click(updateTextArea);

        updateTextArea();
    });

    //delete button event by suman
    jQuery(".delete_multiple").click(function () {
        if (jQuery('#deleted_items').val() == "") {
            alert("Please select atleast one checkbox for delete.");
            return false;
        }

        del = confirm("Are you sure to delete permanently?");
        if (del==true)
        {
			jQuery("#multiple_delete_form").submit();
            //return false;
        }        
        
    });
</script>