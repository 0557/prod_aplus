                                <table class="table table-striped table-bordered table-advance table-hover" id="example">
                        <thead>
                                        <tr>
                                            <th> Department <?php //echo $this->Paginator->sort('department', 'Department'); ?></th>
                                            <th>PLU No <?php //echo $this->Paginator->sort('plu_no', 'PLU No'); ?></th>
                                            <th> Description <?php //echo $this->Paginator->sort('description'); ?></th>
                                         <th>Starting Inventory <?php //echo $this->Paginator->sort('current_inventory', 'Starting Inventory'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php if (isset($rGroceryItems) && !empty($rGroceryItems)) { ?>
                                            <?php foreach ($rGroceryItems as $data) { ?>
                                                <tr>
                                                    
													<td><?php echo h(@$rGroceryDepartments[$data['RGroceryItem']['r_grocery_department_id']]); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['description']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['current_inventory']); ?>&nbsp;</td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="4">  no result found </td>
                                            </tr>
                                        <?php } ?>
										</tbody>
										
										 </table>
