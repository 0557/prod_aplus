<div class="page-content-wrapper">
    <div class="portlet box blue">
        <div class="page-content portlet-body" >
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Item Information  &nbsp;&nbsp;&nbsp;&nbsp;
                    </h3>

                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row" style="margin-left:0px;margin-right:0px;">
                
                <div class="col-md-12" style="border:2px solid #717171;margin-bottom:5px;padding-left:0px;padding-right:0px">

                    <?php
                    //echo $this->Session->read('department');

                    if ($this->Session->read('department') != "") {
                        $selected = $this->Session->read('department');
                    } else {
                        $selected = "";
                    }

                    if ($this->Session->read('plu') != "") {
                        $plu_select = $this->Session->read('plu');
                    } else {
                        $plu_select = "";
                    }

                    if ($this->Session->read('description') != "") {
                        $description_select = $this->Session->read('description');
                    } else {
                        $description_select = "";
                    }
                    ?>
                    <?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                    <div class="col-md-2">
                        <label>Department</label>
                        <?php echo $this->Form->input('department', array('class' => 'form-control', 'type' => 'select', 'label' => false, 'required' => false, 'id' => 'department', 'options' => $departmentlist, 'empty' => 'Filter by department', 'default' => $selected)); ?>
                        <br/>
                    </div>
                    <div class="col-md-2" id="getplunos">
                        <label>PLU No#</label>
                        <?php echo $this->Form->input('plu', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'id' => 'pul', 'placeholder' => 'Filter by PLU No', 'value' => $plu_select)); ?>
                        <br/>
                    </div>
                    <div class="col-md-2" id="getplunos">
                        <label>Description</label>
                        <?php echo $this->Form->input('description', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => false, 'id' => 'description', 'placeholder' => 'Filter by description', 'value' => $description_select)); ?>
                        <br/>
                    </div>

                    <div class="col-md-2" style="width:10%;padding-top:5px">
                        <br>
                        <span class="btn "  >
                            <?php echo $this->Html->link('<i class="fa fa-plus" style="color:#fff"></i>  <span style="color:#fff">Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?> 
                        </span>
                        
                    </div>
                    <div class="col-md-2"  style="width:10%;padding-top:5px">
                        <br>
                        <span class="btn ">
                            <a   class="update" style="color:#fff">Push To POS</a> 
                        </span>
                    </div>
                    
                    <div class="col-md-1"  style="width:5%;padding-top:5px">
                        <br>
                        <span class=" ">
                            <a class="" style="color:#fff"><img style="height:35px" src="<?php echo Router::url("/")?>img/icon_xls.png" /></a> 
                        </span>
                    </div>
                    

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom tabbable-full-width">
                        <div id="loading-image" style="display:none">
                            <h3 style="text-align:center"> Loading...</h3>
                        </div>
                        <div class="tab-content">

                            <div id="tab_1_5" class="tab-pane1">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover" id="getdata">
                                        <thead>
                                            <tr>
                                                <th class="no-sort"> <?php //echo $this->Form->input('all', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'id' =>'selecctall'));     ?> 

                                                    <input type="checkbox" name="all" id="selecctall"/>  </th>
                                                <th><?php echo $this->Paginator->sort('department', 'Department'); ?></th>
                                                <th><?php echo $this->Paginator->sort('plu_no', 'PLU No'); ?></th>
                                                <th><?php echo $this->Paginator->sort('description'); ?></th>
                                                <th><?php echo $this->Paginator->sort('price'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Inventory'); ?></th>
                                                <th><?php echo $this->Paginator->sort(' On-Hand'); ?></th>
                                             <!--   <th><?php echo $this->Paginator->sort('current_inventory', 'Starting Inventory'); ?></th>-->
                                                <th> Margin </th>
                                                <th class="no-sort"> 
                                                    Actions 

                                                    <span class="delete_span">
                                                        <?php echo $this->Form->create('RGroceryItem', array('action' => 'delete_multiple', 'role' => 'form', 'type' => 'file', 'id' => 'multiple_delete_form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                                                        <?php echo $this->Form->input('deleted_items', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'id' => 'deleted_items')); ?>

                                                        <?php echo $this->Form->postLink(__('<img src="' . Router::url('/') . 'img/delete_multiple.png"/>'), array($data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon2 red-stripe delete_multiple', 'title' => 'Multiple Delete')); ?>

                                                        <?php echo $this->form->end(); ?>
                                                    </span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($rGroceryItems) && !empty($rGroceryItems)) { ?>
                                                <?php foreach ($rGroceryItems as $data) { ?>
                                                    <tr>
                                                        <td > <?php //echo $this->Form->input('id[]', array('class' => 'form-control','type'=>'checkbox', 'label' => false, 'required' => false, 'value' => $data['RGroceryItem']['id']));    ?>
                                                            <input type="checkbox" class="delete_checkbox" name="id[]"  value="<?php echo $data['RGroceryItem']['id']; ?>"/>
                                                        </td>
                                                        <td><?php echo h(@$departmentlist[$data['RGroceryItem']['r_grocery_department_id']]); ?>&nbsp;</td>
                                                        <td><?php echo h($data['RGroceryItem']['plu_no']); ?>&nbsp;</td>
                                                        <td><?php echo h($data['RGroceryItem']['description']); ?>&nbsp;</td>
                                                        <td>$<?php echo h($data['RGroceryItem']['price']); ?>&nbsp;</td>
                                                        <td>
                                                            <?php
                                                            if (isset($rubyid)) {
                                                                $findid = ClassRegistry::init('RubyPluttl')->find('first', array('conditions' => array('plu_no' => $data['RGroceryItem']['plu_no'], 'RubyPluttl.store_id' => $this->Session->read('stores_id')), 'limit' => '1', 'order' => array('RubyPluttl.id' => 'desc'), 'joins' => array(array(
                                                                            'table' => 'ruby_headers',
                                                                            'alias' => 'ruby_heade',
                                                                            'type' => 'INNER',
                                                                            'conditions' => array('ruby_heade.id = RubyPluttl.ruby_header_id', 'ruby_heade.id=' . $rubyid),
                                                                            'order' => array('ending_date_time' => 'desc')))));

                                                                if (@$findid['RubyPluttl']['no_of_items_sold'])
                                                                    echo '
<a href="#" data-toggle="tooltip" title="Today Sale is ' . floor(@$findid['RubyPluttl']['no_of_items_sold']) . '"><i class="fa fa-rss"></i></a>
';
                                                            } echo '<a href="#" data-toggle="tooltip" title="Today Sale is 0"><i class="fa fa-rss"></i></a>';
                                                            ?></td>

                                                        <td><?php
                                                            $data['RGroceryItem']['sales_inventory'] = @$pluItemBySale[$data['RGroceryItem']['plu_no']];

                                                            if ($data['RGroceryItem']['sales_inventory'] == "")
                                                                $data['RGroceryItem']['sales_inventory'] = 0;

                                                            echo ($data['RGroceryItem']['current_inventory'] + $data['RGroceryItem']['purchase']) - $data['RGroceryItem']['sales_inventory'];
                                                            ?></td>
                                                        <td><?php
                                                            $margin = ClassRegistry::init('PurchasePack')->find('first', array('conditions' => array('Item_Scan_Code' => $data['RGroceryItem']['plu_no'], 'PurchasePack.store_id' => $this->Session->read('stores_id')), 'order' => array('PurchasePack.id' => 'desc')));

                                                            echo @$margin['PurchasePack']['Margin'];
                                                            ?></td>
                                                        <td class="actionicons">

                                                            <?php echo $this->Html->link('<img src="' . Router::url('/') . 'img/view.png"/>', array('action' => 'view', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe view')); ?>
                                                            <?php echo $this->Html->link('<img src="' . Router::url('/') . 'img/edit.png"/>', array('action' => 'edit', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe edit')); ?>
                                                            <?php echo $this->Form->postLink(__('<img src="' . Router::url('/') . 'img/delete.png"/>'), array('action' => 'delete', $data['RGroceryItem']['id']), array('escape' => false, 'class' => 'newicon red-stripe delete')); ?>

                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="10">  no result found </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="margin-top-20">
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => 'Showing {:start} to {:end} of {:count}'
                                    ));

                                    /* echo $this->Paginator->counter(
                                      'Page {:page} of {:pages}, showing {:current} records out of
                                      {:count} total, starting on record {:start}, ending on {:end}'
                                      ); Showing 201 to 283 of 283 entrie
                                     */
                                    ?>
                                </div>
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>

                            <!--end tab-pane-->
                        </div>
                    </div>
                </div>
                <!--end tabbable-->
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <style>
        .current{
            background: rgb(238, 238, 238) none repeat scroll 0 0;
            border-color: rgb(221, 221, 221);
            color: rgb(51, 51, 51);
            border: 1px solid rgb(221, 221, 221);
            float: left;
            line-height: 1.42857;
            margin-left: -1px;
            padding: 6px 12px;
            position: relative;
            text-decoration: none;
        }
    </style>
    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Sync To POS</h4>
                </div>
                <div class="modal-body">
                    Please Call Technical Support 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script type="text/javascript">
    $('[data-toggle=modal]').on('click', function (e) {
        var $target = $($(this).data('target'));
        $target.data('triggered', true);
        setTimeout(function () {
            if ($target.data('triggered')) {
                $target.modal('show')
                        .data('triggered', false); // prevents multiple clicks from reopening
            }
            ;
        }, 1000); // milliseconds
        return false;
    });
    $(document).ready(function () {
        jQuery(".up").hide();
        $('#selecctall').click(function (event) {
            $("input:checkbox").prop('checked', $(this).prop("checked"));

        });
    });
</script>
<script type="text/javascript">
    jQuery(window).bind("load", function () {
        jQuery('#department, #description, #pul').on('keyup change', function (e) {
            //
            jQuery('#loading-image').css('display', 'block');
            var department = jQuery("#department").val();
            var description = jQuery("#description").val();
            var pul = jQuery("#pul").val();


            jQuery("#department_id").val(department);
            jQuery("#plu_id").val(pul);
            jQuery("#des_id").val(description);

            var RGroceryItem = {};
            if (department) {
                RGroceryItem['department'] = department;
            }
            //RGroceryItem['department'] = department;
            if (description) {
                RGroceryItem['description'] = description;
            }
            if (pul) {
                RGroceryItem['plu'] = pul;
            }
            var allpc1 = {RGroceryItem: RGroceryItem}; //alert(JSON.stringify(allpc1));	 
            jQuery.ajax({
                type: "POST",
                data: allpc1,
                url: "<?php echo Router::url('/') ?>admin/r_grocery_items/",
                success: function (data, textStatus) {
                    jQuery('#getdata').html(data);
                    jQuery('#loading-image').css('display', 'none');
                    jQuery('ul.pagination').css('display', 'none');
                    jQuery('.margin-top-20').css('display', 'none');
                    //alert(data);
                    jQuery(".up").show();
                }
            });

        });

        /*jQuery('#exportid').on('click', function(e) {
         var department = jQuery("#department").val();
         var description = jQuery("#description").val();
         var pul = jQuery("#pul").val();
         jQuery.ajax({
         type: "POST",
         data: {department:department,description:description,pul:pul},
         url:  "<?php echo Router::url('/') ?>admin/r_grocery_items/export", 
         success: function (data, textStatus){  
         
         alert(data);
         }
         });	
         
         });	*/

        $('.update').click(function () {
            var arr = [];
            $.each($("input[name='id[]']:checked"), function () {
                arr.push($(this).val());
            });
            var len = arr.length;
            //alert(len);
            if (len <= 1) {
                var id = arr + ',';
            } else {
                var id = arr.toString();
            }
            //alert(id);
            var idstr = id;
            //alert(idstr);
            $.ajax({
                url: '<?php echo Router::url('/') ?>admin/r_grocery_items/posupdate',
                type: 'post',
                data: {idstr: idstr},
                beforeSend: function () {
                    $("#loading-image").show();
                },
                success: function (data) {
                    //alert(data);
                    $("#loading-image").hide();
                    window.location.reload();
                }
            });
        });
    });
</script>

<!--Multiple delete code by Suman-->
<script type="text/javascript">
    function updateTextArea() {
        var allVals = [];
        jQuery('.delete_checkbox:checked').each(function () {
            allVals.push(jQuery(this).val());
        });
        jQuery('#deleted_items').val(allVals);
    }

    jQuery(document).ready(function () {
        jQuery('.delete_checkbox').click(updateTextArea);
        jQuery('#selecctall').click(updateTextArea);

        updateTextArea();
    });

    //delete button event by suman
    jQuery(".delete_multiple").click(function () {
        if (jQuery('#deleted_items').val() == "") {
            alert("Please select atleast one checkbox for delete.");
            return false;
        }

        del = confirm("Are you sure to delete permanently?");
        if (del != true)
        {
            return false;
        }


        jQuery("#multiple_delete_form").submit();
    });
</script>
<style>
    #getdata th.actionicons, #getdata td.actionicons {
        width: 153px !important;
    }
    #loading-image {
        background: #fff none repeat scroll 0 0;
        bottom: 0;
        left: 0;
        opacity: 0.8;
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999;
    }
    #loading {
        width: 50px;
        height: 57px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -28px 0 0 -25px;
    }
    .btn-success{opacity:1 !important; padding:5px 15px!important;  position:relative !important;}
    .portlet .portlet .row h3.page-title{ display:none;}
    .portlet .portlet .row #RGroceryItemAdminIndexForm{display:none;}
    .portlet .portlet ul.pagination{display:block !important;}
    .portlet .portlet .margin-top-20{display:block !important;}
    .table-responsive {
        overflow-x: initial;
    }
</style>