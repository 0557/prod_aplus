				
		<div class="portlet box blue">
               <div class="page-content portlet-body" >
			   <!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
		       <div class="row">
		  <div class="col-md-12">
		    <div class="col-md-4">
		<?php echo $this->Form->input('department', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => false, 'id' =>'department','options'=>$departmentlist,'empty'=>'Filter by department')); ?>
		<br/>
		</div>
			 <div class="col-md-4" id="getplunos">
			<?php echo $this->Form->input('plu', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => false, 'id' =>'pul','placeholder'=>'Filter by PLU No')); ?>
			
			
			<br/>
			
				
			
			</div>
			
			 <div class="col-md-4" id="getplunos">
				<?php echo $this->Html->link("Refresh LIVE data", array('action' => 'refreshLive', 'admin' => true)); ?>
			</div>
		</div>
		</div>	
							
		</div>
					<!-- END SAMPLE TABLE PORTLET-->
	</div>

<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Inventory By Items
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
																<i class="fa fa-calendar"></i> <?php if(isset($enddate)) echo date("m-d-Y", strtotime($enddate));?>													
							</div>
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('Department'); ?></th>
								<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
								<th><?php echo $this->Paginator->sort('Description'); ?></th>
								<th><?php echo $this->Paginator->sort('Starting Inventory'); ?></th>
								<th><?php echo $this->Paginator->sort('Purchase Quantity'); ?></th>
								<th><?php echo $this->Paginator->sort('Sales Inventory'); ?></th>
								<th><?php echo $this->Paginator->sort('On-Hand'); ?></th>
								<th><?php echo $this->Paginator->sort('Status'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php if (isset($rGroceryItems) && !empty($rGroceryItems)) { ?>
                                            <?php foreach ($rGroceryItems as $data) { 
											$sales_inventory = $data['RGroceryItem']['sales_inventory'] = @$pluItemBySale[$data['RGroceryItem']['plu_no']];
											
											if($data['RGroceryItem']['sales_inventory'] == "") $data['RGroceryItem']['sales_inventory'] = 0;
											?>
						<tr>
						
										<td><?php echo h(@$departmentlist[$data['RGroceryItem']['r_grocery_department_id']]); ?>&nbsp;</td>
						
									
                                                    <td><?php echo h($data['RGroceryItem']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['description']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['current_inventory']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryItem']['purchase']); ?>&nbsp;</td>
                                                  
												  <td><?php echo h($data['RGroceryItem']['sales_inventory']); ?>&nbsp;</td>
                                                  
												  <td><?php echo ($data['RGroceryItem']['current_inventory'] + $data['RGroceryItem']['purchase']) - $data['RGroceryItem']['sales_inventory']; 

?>&nbsp;</td>
												
												<td><?php $remaining = ($data['RGroceryItem']['current_inventory']+$data['RGroceryItem']['purchase']) - $sales_inventory;
													
													if($remaining<=0) echo '<p style="background:red;color:#fff;padding:0 10px;"><b>NO STOCK</b></p>';
if($remaining<10 && $remaining>0) echo '<p style="background:orange;color:#fff;padding:0 10px"><b>L-STOCK</b></p>';
if($remaining>=10) echo '<p style="background:green;color:#fff;padding:0 10px"><b>ON STOCK</b></p>';													
?></td>
							
						</tr>
					<?php } }?>
						</tbody>
						</table>
                            </div>
                           
            </div>
						</div>
						</div>
						</div>
						

<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function filter_utank() {	
		var from_date=$('#from').val();
		var to_date=$('#to').val();
		
		var department_number=$('#department_number').val();
		//$('#loaderdiv').show();
		$.ajax({
			type : 'POST',
			url : webroot+'ruby_deptotals/index/',
			dataType : 'html',
			data: {from_date:from_date,to_date:to_date,department_number:department_number},
			success:function(result){
				//$("#example_info").hide();example_filter
				//$("#example_paginate").hide();
				//$("#example_filter").hide();
				//$("#example_length").hide();
				//$(".col-md-2").hide();
				$("#filter").show();
				$("#example").html(result);
			}});
	}
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({"pageLength": 100});
		$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});


	
	$('#department').change(function(){
		var dep = $('#department').val();
		var pul = $('#pul').val();
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/r_grocery_items/getbyitems',
                type: 'post',
                data: { id: dep,pul:pul},
                success:function(data){
//				alert(data);
					$('tbody').html(data);
				/*	getplu();*/
                }
            });
		});
			$('#pul').keyup(function(){
		var dep = $('#department').val();
		var pul = $('#pul').val();
//		alert(pul);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/r_grocery_items/getbyitems',
                type: 'post',
                data: { id: dep,pul:pul},
                success:function(data){
//				alert(data);
					$('tbody').html(data);
				/*	getplu();*/
                }
            });
		});
    });
 /*  function getplu(){
		var dep = $('#department').val();
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/r_grocery_items/getplus',
                type: 'post',
                data: { id: dep},
                success:function(data){
				alert(data);
					$('#pul').html(data);
					
                }
            });
			
			}*/
</script>