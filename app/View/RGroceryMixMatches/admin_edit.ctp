<?php // pr($this->data); // die;  ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/jquery-ui')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker', 'jquery-ui.css')); ?>     
<?php echo $this->Form->create('RGroceryMixMatch', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>Mix And Match
                    </div>
                </div></div></div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Mix And Match Name:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Name')); ?>
                                    <?php echo $this->Form->input('id'); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Mix And Match Short Description:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('description', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Short Description')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Start Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('start_date', array('class' => 'form-control', 'type' => 'text', 'value' => date('d/m/Y - H:m', strtotime($this->request->data['RGroceryMixMatch']['start_date'])), 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    End Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('end_date', array('class' => 'form-control', 'value' => date('d/m/Y - H:m', strtotime($this->request->data['RGroceryMixMatch']['end_date'])), 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Items /UPC Participate  
                    </div>
                </div>
                <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th> Item #/PLU -DDL</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more">
                                <?php if (isset($this->request->data['RGroceryMixMatchParticipate']['r_grocery_department_id']) && !empty($this->request->data['RGroceryMixMatchParticipate']['r_grocery_department_id'])) { ?>
                                    <?php foreach ($this->request->data['RGroceryMixMatchParticipate']['r_grocery_department_id'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_department_id.', array('class' => 'form-control exampleInputName', 'options' => $rGroceryDepartment, 'selected' => $value, 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Department', 'required' => false)); ?>&nbsp;</td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_item_id.', array('label' => false, 'value' => $this->request->data['RGroceryMixMatchParticipate']['r_grocery_item_id'][$key], 'class' => 'form-control autocomplete_seacrh', 'type' => 'text', 'placeholder' => 'Enter Item/Plu')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.description.', array('label' => false, 'readonly' => true, 'value' => $this->request->data['RGroceryMixMatchParticipate']['description'][$key], 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Description')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else if (isset($this->request->data['RGroceryMixMatchParticipate']) && !empty($this->request->data['RGroceryMixMatchParticipate'])) { ?>
                                    <?php foreach ($this->request->data['RGroceryMixMatchParticipate'] as $key1 => $data) { // pr($data); ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_department_id.', array('class' => 'form-control exampleInputName', 'selected' => $data['r_grocery_department_id'], 'options' => $rGroceryDepartment, 'id' => false, 'label' => false, 'type' => 'select', 'empty' => 'Select Department', 'required' => false)); ?>&nbsp;</td>
                                            <?php $ItemDescription = $this->Custom->GroceryItemName($data['r_grocery_item_id']); ?>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_item_id.', array('label' => false, 'value' => $ItemDescription, 'class' => 'form-control autocomplete_seacrh', 'type' => 'text', 'placeholder' => 'Enter Item/Plu')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchParticipate.description.', array('label' => false, 'value' => $data['description'], 'class' => 'form-control', 'readonly' => true, 'type' => 'text', 'placeholder' => 'Enter Description')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                    <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>

                    <button type="button" class="btn blue" onclick="AddProduct();" style="float:right;">Add More</button>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Package Pricing and Quantity Configuration.
                    </div>
                </div>
                <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>Quantity </th>
                                    <th>Numberic field</th>
                                    <th>For Price</th>
                                    <th>Numberic</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more_package">
                                <?php if (isset($this->request->data['RGroceryMixMatchPackage']['quantity']) && !empty($this->request->data['RGroceryMixMatchPackage']['quantity'])) { ?>
    <?php foreach ($this->request->data['RGroceryMixMatchPackage']['quantity'] as $key => $value) { // pr($value); die;  ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.quantity.', array('class' => 'form-control autoGenerate_quantity', 'value' => $value, 'id' => false, 'label' => false, 'required' => false)); ?>&nbsp;</td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric_field.', array('label' => false, 'value' => $this->request->data['RGroceryMixMatchPackage']['numeric_field'][$key], 'class' => 'form-control autocomplete_seacrh', 'type' => 'text', 'placeholder' => 'Enter Numeric Field')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.price.', array('label' => false, 'value' => $this->request->data['RGroceryMixMatchPackage']['price'][$key], 'class' => 'form-control autoGenerate_price', 'type' => 'text', 'placeholder' => 'Enter Price')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric.', array('label' => false, 'value' => $this->request->data['RGroceryMixMatchPackage']['numeric'][$key], 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter numeric')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else if (isset($this->request->data['RGroceryMixMatchPackage']) && !empty($this->request->data['RGroceryMixMatchPackage'])) { ?>
                                        <?php foreach ($this->request->data['RGroceryMixMatchPackage'] as $data) { ?>
                                        <tr>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.quantity.', array('class' => 'form-control autoGenerate_quantity', 'value' => $data['quantity'], 'id' => false, 'label' => false, 'required' => false, 'readonly' => true)); ?>&nbsp;</td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric_field.', array('label' => false, 'value' => $data['numeric_field'], 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Numeric Field')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.price.', array('label' => false, 'readonly' => true, 'value' => $data['price'], 'class' => 'form-control autoGenerate_price', 'type' => 'text', 'placeholder' => 'Enter price')); ?></td>
                                            <td><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'value' => $data['numeric'], 'placeholder' => 'Enter numeric')); ?></td>
                                            <td><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" class="btn blue" onclick="AddProductPackage();" style="float:right;">Add More</button>
                </div>
            </div>

        </div>



    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>
        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>
<table style="display:none">
    <tbody id="AddMore">
        <tr>
            <td style="background-color: rgb(249, 249, 249);"><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_department_id.', array('class' => 'form-control exampleInputName', 'options' => $rGroceryDepartment, 'empty' => 'Select Depatment', 'id' => false, 'label' => false, 'div' => false, 'type' => 'select')); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); "><?php echo $this->Form->input('RGroceryMixMatchParticipate.r_grocery_item_id.', array('label' => false, 'class' => 'form-control autocomplete_seacrh', 'type' => 'text', 'placeholder' => 'Enter Item/plu')); ?></td>
            <td style="background-color: rgb(249, 249, 249);"><?php echo $this->Form->input('RGroceryMixMatchParticipate.description.', array('label' => false, 'class' => 'form-control', 'readonly' => true, 'type' => 'text', 'placeholder' => 'Enter Description')); ?></td>
            <td style="background-color: rgb(249, 249, 249);"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>
<table style="display:none">
    <tbody id="AddMorePackage">
        <tr>
            <td style="background-color: rgb(249, 249, 249); "><?php echo $this->Form->input('RGroceryMixMatchPackage.quantity.', array('class' => 'form-control autoGenerate_quantity', 'id' => false, 'label' => false, 'readonly' => true, 'div' => false, 'placeholder' => 'Enter Quantity')); ?>&nbsp;</td>
            <td style="background-color: rgb(249, 249, 249); "><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric_field.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Numeric Field')); ?></td>
            <td style="background-color: rgb(249, 249, 249);"><?php echo $this->Form->input('RGroceryMixMatchPackage.price.', array('label' => false, 'class' => 'form-control autoGenerate_price', 'readonly' => true, 'type' => 'text', 'placeholder' => 'Enter Price')); ?></td>
            <td style="background-color: rgb(249, 249, 249);"><?php echo $this->Form->input('RGroceryMixMatchPackage.numeric.', array('label' => false, 'class' => 'form-control', 'type' => 'text', 'placeholder' => 'Enter Numeric')); ?></td>
            <td style="background-color: rgb(249, 249, 249);"><span class="label label-success" href='#'> <a class='Delete_product'>Delete</a></span></td>
        </tr>
    </tbody>             
</table>
<!-- END SAMPLE FORM PORTLET-->



<!-- END PAGE CONTENT-->
<!--</div>-->

<script>
    var a = 1;
    function AddProduct() {
        $("#products_add_more").append($("#AddMore").html());
    }
    function AddProductPackage() {
        $("#products_add_more_package").append($("#AddMorePackage").html());
     //   $("#products_add_more_package tr").last().find('.autoGenerate_quantity').val('Quantity ' + (a));
     //   $("#products_add_more_package tr").last().find('.autoGenerate_price').val('Price ' + (a));


         autogenerate();

    }
    function autogenerate() {
        var a = 1;
        var autogenerate_length = $("#products_add_more_package").children().length;
        $($("#products_add_more_package").children()).each(function () {
            console.log(this);
            $(this).find('.autoGenerate_quantity').val('Quantity ' + (a));
            $(this).find('.autoGenerate_price').val('Price ' + (a));
            a++;
        });
//         $("#products_add_more_package tr").last().find('.autoGenerate_quantity').val('Quantity '+(autogenerate_length));
//        $("#products_add_more_package tr").last().find('.autoGenerate_price').val('Price '+(autogenerate_length));
    }

    $(document).ready(function () {
        $(".form_meridian_datetime").datetimepicker({
            isRTL: App.isRTL(),
            format: "dd/mm/yyyy - hh:ii",
            showMeridian: true,
            autoclose: true,
            startDate: '2015-10-30 10:00',
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            todayBtn: true
        });
    });

    $(document).delegate('.Delete_product', 'click', function () {
        $(this).parents('tr').remove();
        autogenerate();

    });

    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    $("select , input").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
</script>
<script>
    $(function () {
        $('#products_add_more').delegate('input.autocomplete_seacrh', 'focus', function (e) {
            var department = $(this).parents('td').prev('td').find('select').val();
            if (department != '') {
                $(this).autocomplete({
                    source: '<?php echo Router::url('/') ?>r_grocery_mix_matches/autocomplete/' + department,
                    change: function (event, ui) {
                        $(this).parents('td').next('td').find('input').val(ui.item.desc);
                    },
                    select: function (event, ui) {
                        $(this).parents('td').next('td').find('input').val(ui.item.desc);
                    },
                    response: function (event, ui) {
                        // console.log(ui.content.length);
                        if (!ui.content.length) {
                            alert('UPC code Not found');
                        }
                    }
                });
            } else {
                alert('Please Select Department');
            }
        });

    });
</script>

