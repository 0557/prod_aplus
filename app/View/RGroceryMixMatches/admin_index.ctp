<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Mix And Matches <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('id')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('name')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('description'); ?></th>
                                            <th><?php echo $this->Paginator->sort('start_date'); ?></th>
                                            <th><?php echo $this->Paginator->sort('end_date'); ?></th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rGroceryMixMatches) && !empty($rGroceryMixMatches)) { ?>
                                            <?php foreach($rGroceryMixMatches as $data) { ?>
                                                <tr>
                                                    <td><?php echo h($data['RGroceryMixMatch']['id']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryMixMatch']['name']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryMixMatch']['description']); ?>&nbsp;</td>
                                                    <td><?php echo date('d F,Y - H:m', strtotime($data['RGroceryMixMatch']['start_date'])); ?>&nbsp;</td>
                                                    <td><?php echo date('d F,Y - H:m', strtotime($data['RGroceryMixMatch']['end_date'])); ?>&nbsp;</td>
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>', array('action' => 'view', $data['RGroceryMixMatch']['id']), array('escape' => false,'class' => 'newicon red-stripe view')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('action' => 'edit', $data['RGroceryMixMatch']['id']), array('escape' => false,'class' => 'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>', array('action' => 'delete', $data['RGroceryMixMatch']['id']), array('escape' => false,'class' => 'newicon red-stripe delete')); ?>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="6">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>

