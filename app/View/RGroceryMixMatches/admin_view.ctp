<?php // pr($rGroceryMixMatch); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


    <!-- BEGIN PAGE CONTENT-->
    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View Mix And Matches Info : <?php echo $rGroceryMixMatch['RGroceryMixMatch']['id']; ?>
                    </div>
                    
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>
                                                    </div>
                                                    <div class="actions">
                                                        <!--<a href="#" class="btn default btn-sm">
                                                                <i class="fa fa-pencil"></i> Edit
                                                        </a>-->
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Mix And Match Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($rGroceryMixMatch['RGroceryMixMatch']['name']); ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Mix And Match Short Description:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($rGroceryMixMatch['RGroceryMixMatch']['description']); ?>

                                                        </div>
                                                    </div>
                                                    
                                                   



                                                    
                                                   

                                                    

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>
                                                    </div>
                                                    <div class="actions">
                                                        <!--<a href="#" class="btn default btn-sm">
                                                                <i class="fa fa-pencil"></i> Edit
                                                        </a>-->
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Start Date:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($rGroceryMixMatch['RGroceryMixMatch']['start_date'])); ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['receving_date']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                          End Date:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($rGroceryMixMatch['RGroceryMixMatch']['end_date'])); ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['receving_date']; ?>

                                                        </div>
                                                    </div>

                                                                                                  </div>
                                            </div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
    <!-- END PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i> Items /UPC Participate
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                <tr>

                                    <th>
                                        Department
                                    </th>
                                    <th>
                                        
                                        Item #/PLU -DDL
                                    </th>
                                    <th>
                                        Description

                                    
                                </tr>
                            </thead>
                            <?php $total = 0; ?>
                            <tbody>
                                <?php if (isset($rGroceryMixMatch['RGroceryMixMatchParticipate']) && !empty($rGroceryMixMatch['RGroceryMixMatchParticipate'])) { //pr($product);  ?>


                                    <?php
                                    foreach ($rGroceryMixMatch['RGroceryMixMatchParticipate'] as $key => $data) {
                                        $nameAndId = $this->Custom->getdepartment($data['r_grocery_department_id']);
                                        $namegetItem = $this->Custom->getItem($data['r_grocery_item_id']);
                                        ?>
                                        <tr>
                                           
                                            <td><?php echo $nameAndId['RGroceryDepartment']['name']; ?> </td>
                                            <td><?php echo $namegetItem['RGroceryItem']['plu_no']; ?></td>
                                            <td><?php echo $data['description']; ?></td>

                                        </tr>

                                    <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                       

                                                                                                      
                    </div>
                </div>

            </div>

        </div>

        
        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i> Package Pricing and Quantity Configuration.

                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                <tr>

                                    <th>
                                        Quantity 
                                    </th>
                                    <th>
                                        
                                        Numberic field
                                    </th>
                                    <th>
                                       For Price
                                    </th>
                                    <th>
                                        	Numberic
                                    </th>

                                    
                                </tr>
                            </thead>
                            <?php $total = 0; ?>
                            <tbody>
                                <?php if (isset($rGroceryMixMatch['RGroceryMixMatchPackage']) && !empty($rGroceryMixMatch['RGroceryMixMatchPackage'])) { //pr($product);  ?>


                                    <?php
                                    foreach ($rGroceryMixMatch['RGroceryMixMatchPackage'] as $key => $data) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                echo $data['quantity'];
                                                ?>
                                            </td>
                                            <td><?php echo $data['numeric_field']; ?> </td>
                                            <td><?php echo $data['price']; ?></td>
                                            <td><?php echo $data['numeric']; ?></td>

                                        </tr>

                                    <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                       

                                                                                                      
                    </div>
                </div>

            </div>

        </div>
        </div>

        


    </div>

    <!-- END CONTENT -->
