<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Promotion Information <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo $this->Paginator->sort('name', 'Promotion Name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('plu_no', 'PLU No'); ?></th>
                                            <th><?php echo $this->Paginator->sort('str_disc_period','Beginning Discount Period'); ?></th>
                                            <th><?php echo $this->Paginator->sort('end_disc_period' ,'End Discount Period'); ?></th>
                                            <th><?php echo $this->Paginator->sort('disc_type', 'Discount is Amount'); ?></th>
                                            <th><?php echo $this->Paginator->sort('disc_amount_percent', 'Discount Amount/Percent'); ?></th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rGroceryPromotion) && !empty($rGroceryPromotion)) { ?>
                                            <?php foreach ($rGroceryPromotion as $data) { ?>
                                                <tr>
                                                    <td><?php echo h($data['RGroceryPromotion']['name']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryPromotion']['plu_no']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryPromotion']['str_disc_period']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RGroceryPromotion']['end_disc_period']); ?>&nbsp;</td>
                                                    <td><?php echo ($data['RGroceryPromotion']['disc_type'] == '0')? 'Percent' : 'Amount'; ?></td>
                                                    <td><?php echo ($data['RGroceryPromotion']['disc_type'] == '0')? '' : '$'; ?><?php echo h($data['RGroceryPromotion']['disc_amount_percent']); ?><?php echo ($data['RGroceryPromotion']['disc_type'] == '0')? '%' : ''; ?></td>
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>', array('action' => 'view', $data['RGroceryPromotion']['id']), array('escape' => false,'class' => 'newicon red-stripe view')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('action' => 'edit', $data['RGroceryPromotion']['id']), array('escape' => false,'class' => 'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Form->postLink(__('<img src="'.Router::url('/').'img/delete.png"/>'), array('action' => 'delete', $data['RGroceryPromotion']['id']), array('escape' => false,'class' => 'newicon red-stripe delete')); ?>
                                                        
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    </div>
</div>

