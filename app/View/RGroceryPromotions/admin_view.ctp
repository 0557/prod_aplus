<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>  
<?php //pr($rGroceryItem);
?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Grocery Promotion Information -
                    </div>

                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">


                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           Promotion Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $rGroceryPromotion['RGroceryPromotion']['name']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            PLU No:
                                                        </div>
                                                        <div class="col-md-7 value">
<?php echo $rGroceryPromotion['RGroceryPromotion']['plu_no']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           Beginning Discount Period :  
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($rGroceryPromotion['RGroceryPromotion']['str_disc_period'])); ?>
                                                       </div>
                                                    </div>
                                               </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            End Discount Period :
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($rGroceryPromotion['RGroceryPromotion']['end_disc_period'])); ?>
                                                          </div>

                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                        Discount is Amount:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo ($rGroceryPromotion['RGroceryPromotion']['disc_type'] == '0')? 'Percent' : 'Amount'; ?>
                                                            <?php // echo $rGroceryPromotion['RGroceryPromotion']['disc_type']; ?>
                                                        </div>

                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                           Discount Amount or Percent:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo ($rGroceryPromotion['RGroceryPromotion']['disc_type'] == '0')? '' : '$'; ?><?php echo h($rGroceryPromotion['RGroceryPromotion']['disc_amount_percent']); ?><?php echo ($rGroceryPromotion['RGroceryPromotion']['disc_type'] == '0')? '%' : ''; ?>
                                                            <?php // echo $rGroceryPromotion['RGroceryPromotion']['disc_amount_percent']; ?>
                                                        </div>

                                                    </div>

                                                </div></div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
</div>