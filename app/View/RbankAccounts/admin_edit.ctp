
<style>

.yellow-box1 {
    margin: 0 0 0 0 !important;
}

</style>

<style type="text/css">


@media only screen 
  and (min-width: 320px) 
  and (max-width: 480px)
  {
  
  .yellow-box1 {
    margin: 0 0 0 0% !important;
}
}

@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.yellow-box1 {
    margin: 0 0 0 0% !important;
}
}


</style>
<?php echo $this->Form->create('RBankAccount',array('action'=>'add_account'))?>
<!--
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
          <li class="<?php echo (($this->params['controller'] ==  'r_banks')) ? 'active' : ''; ?>">
				<?php echo $this->Html->link("<i class='fa fa-university'></i>Account Register ", array('controller' => 'r_banks', 'action' => 'index'), array('escape' => false)); ?>
			 </li>
		    <li class="<?php echo (($this->params['controller'] ==  'r_bank_accounts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-user-secret'></i>Bank Account", array('controller' => 'r_bank_accounts', 'action' => 'index'), array('escape' => false)); ?>
		    </li>
		     <li class="<?php echo (($this->params['controller'] ==  'r_banks')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-user-secret'></i>Bank Report", array('controller' => 'r_banks', 'action' => 'report'), array('escape' => false)); ?>
		    </li>
        </ul>
    </div>
</div>-->
<div class="portlet box blue">
               <div class="page-content portlet-body">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
   						   <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                Edit Bank account 
                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>

        <div class="form-body1">
            <div class="row">

                <div class="col-md-6 col-sm-12 yellow-box1">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                Select Corporation :<span class="star">* </span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('corporation', array('class' => 'form-control', 'options'=>$corporation_list,'label' => false,'required' => 'false', 'default' => $account['RBankAccount']['corporation'],'id'=>'cpt_id','onchange'=>'storelist()')); ?>
                                  
                                </div>
                            </div>

                        
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Select Store:			 
                                </div>
                                <div class="col-md-7 value" id="store">
                                    
                                  <?php echo $this->Form->input('store', array('class' => 'form-control', 'label' => false,'required' => 'false', 'default' => $account['RBankAccount']['store'],'id'=>'cpt_id','onchange'=>'storelist()')); ?>
                                  

                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Account Name:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('id', array('class' => 'form-control' ,'required' => false, 'label' => false, 'type' => 'hidden','value'=>$account['RBankAccount']['id'])); ?>
									  
                                      <?php echo $this->Form->input('account_name', array('class' => 'form-control' ,'required' => false, 'label' => false, 'placeholder' => 'Enter Account Name','value'=>$account['RBankAccount']['account_name'])); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Bank Name:	 
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('bank_name', array('class' => 'form-control' ,'required' => false, 'label' => false, 'placeholder' => 'Enter Bank Name','value'=>$account['RBankAccount']['bank_name'])); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Routing No:	 
                                </div>
                                <div class="col-md-7 value">
                                       <?php echo $this->Form->input('routing_no', array('class' => 'form-control' ,'required' => false, 'label' => false, 'value' => $account['RBankAccount']['routing_no'])); ?>

                                </div>
                            </div>
                         

                           
                        </div>
                    </div>
                </div>
               
               
            </div>
            
            

<div class="form-actions">
        
        <button type="submit" name="Submit" class="btn blue">Submit</button>
<!--        <button type="reset" class="btn default">Reset</button>-->
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>
     
    </div>
            <!-- END FORM-->
        </div>
        </div>


    </div>
    


 <?php echo $this->Form->end(); ?>   

<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function storelist() {	
		var corporation=$('#cpt_id').val();
		
		
		$.ajax({
			type : 'POST',
			url : webroot+'r_bank_accounts/get_store_id/',
			dataType : 'html',
			data: {corporation:corporation},
			success:function(result){
			
			$('#store').html(result);
			
			}});
	}
</script>






