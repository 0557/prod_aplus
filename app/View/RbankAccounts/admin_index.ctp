
<style>

.yellow-box1 {
    margin: 0 0 0 0 !important;
}

</style>

<style type="text/css">


@media only screen 
  and (min-width: 320px) 
  and (max-width: 480px)
  {
  
  .yellow-box1 {
    margin: 0 0 0 0% !important;
}
}

@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.yellow-box1 {
    margin: 0 0 0 0% !important;
}
}


</style>
<?php echo $this->Form->create('RBankAccount',array('action'=>'add_account'))?>
<!--
<div class="row">
    <div class="col-xs-12">
        <ul class="menu-btn">
          <li class="<?php echo (($this->params['controller'] ==  'r_banks')) ? 'active' : ''; ?>">
				<?php echo $this->Html->link("<i class='fa fa-university'></i>Account Register ", array('controller' => 'r_banks', 'action' => 'index'), array('escape' => false)); ?>
			 </li>
		    <li class="<?php echo (($this->params['controller'] ==  'r_bank_accounts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-user-secret'></i>Bank Account", array('controller' => 'r_bank_accounts', 'action' => 'index'), array('escape' => false)); ?>
		    </li>
		     <li class="<?php echo (($this->params['controller'] ==  'r_banks')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-user-secret'></i>Bank Report", array('controller' => 'r_banks', 'action' => 'report'), array('escape' => false)); ?>
		    </li>
        </ul>
    </div>
</div>-->
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                   Bank Accounts <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('account_name')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('bank_name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('routing_no'); ?></th>
                                            <th><?php echo $this->Paginator->sort('create_date'); ?></th>

                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($corporation_list) && !empty($corporation_list)) { ?>
                                           <?php foreach ($corporation_list as $data) {
											  // echo'<pre>'; print_r($data);
											    ?>
                                                <tr>
                                                    <td><?php echo $data['RBankAccount']['bank_id']; ?>&nbsp;</td>


                                                    <td><?php echo $data['RBankAccount']['bank_name']; ?></td>
                                                   <td><?php echo $data['RBankAccount']['routing_no']; ?>&nbsp;</td>

                                                         <td><?php echo date('m-d-Y', strtotime($data['RBankAccount']['create_date'])); ?>&nbsp;</td>
														 
                                                    <td >
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('action' => 'edit', $data['RBankAccount']['id']), array('escape' => false,'class' => 'newicon red-stripe')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>', array('action' => 'index', $data['RBankAccount']['id']), array('escape' => false,'class' => 'newicon red-stripe')); ?>

                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="5">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    </div>
</div>