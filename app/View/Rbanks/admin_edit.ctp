
<style>

.yellow-box1 {
    margin: 0 0 0 24% !important;
}
button.Zebra_DatePicker_Icon {
   
    opacity: 0 !important;
    
}

#date {
    background: rgba(0, 0, 0, 0) url("http://aplus1.net/backoffice/img/calendar.png") no-repeat scroll 287px 0;
}
</style>

<style type="text/css">


@media only screen 
  and (min-width: 320px) 
  and (max-width: 480px)
  {
  
  .yellow-box1 {
    margin: 0 0 0 0% !important;
}

#date {
    background: rgba(0, 0, 0, 0) url("http://aplus1.net/backoffice/img/calendar.png") no-repeat scroll 162px 0;
}
}






@media only screen 
  and (min-width: 360px) 
  and (max-width: 640px)
  {

#date {
    background: rgba(0, 0, 0, 0) url("http://aplus1.net/backoffice/img/calendar.png") no-repeat scroll 201px 0;
}
}



@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.yellow-box1 {
    margin: 0 0 0 0% !important;
}

#date {
    background: rgba(0, 0, 0, 0) url("http://aplus1.net/backoffice/img/calendar.png") no-repeat scroll 589px 0;
}

}
</style>
 <?php echo $this->Form->create('Rbank', array('controller' => 'RBanks','method'=>'post', 'id' => 'form', 'type'=>'file'));
 echo $this->Form->input('id', array('type' => 'hidden')); 
          ?>

<div class="portlet box blue">
               <div class="page-content portlet-body">
   
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Bank Account Edit
                    </div>
                 
                </div></div>
                  
                </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12 yellow-box1">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                Date :<span class="star"> </span>
                                </div>
                                <div class="col-md-7 value">
                                   <?php echo $this->Form->input('date',array('label'=>false,'class'=>'form-control required','placeholder'=>'','id'=>'date')); ?>
                                  
                                </div>
                            </div>

                        
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Check No:<span class="star"> </span>	 
                                </div>
                                <div class="col-md-7 value">
                                         <?php echo $this->Form->input('payee',array('label'=>false,'class'=>'form-control required','placeholder'=>'', 'id'=>'payee')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Transaction Description :	 
                                </div>
                                <div class="col-md-7 value">
                                      <?php echo $this->Form->input('paid_check',array('label'=>false,'class'=>'form-control required','placeholder'=>'', 'id'=>'paid_check')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Deposit, Credit (+) :	 
                                </div>
                                 <?php
                           if($bank['Rbank']['deposit_status']=='')
                         { ?>
                                <div class="col-md-7 value">
                                          <?php echo $this->Form->input('receive_amount',array('label'=>false,'class'=>'form-control required','placeholder'=>'', 'id'=>'receive_amount','onkeypress'=>'return numbervalonly(event)')); ?>
                                  
                                </div>
                            <?php }
	               	         else
		                      { ?>     
                                 <div class="col-md-7 value">
                                          <?php echo $this->Form->input('receive_amount',array('label'=>false,'class'=>'form-control required','placeholder'=>'', 'id'=>'receive_amount','style'=>'background-color: yellow','onkeypress'=>'return numbervalonly(event)')); ?>
                             <?php  } ?>    
                               </div>
                                <div class="col-md-12 value">
                                
                                 <?php
                             if($bank['Rbank']['deposit_status']=='')
                            { ?>
		                   <input type="checkbox" id="status" class="" value="1" name="data[Rbank][deposit_status]" id="status" style="border: none;"  />
		                    <?php }
	               	      else
		                     { ?>
		                  <input type="checkbox" name="data[Rbank][deposit_status]" id="status" class="" value="1" style="border: none;"  checked="checked"  />
		                   <?php  } ?>
                                 
                                    
                                </div>
                                
                            
                            
							
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Payment, Fee Withdrawal (-) :	 
                                </div>
                                <div class="col-md-7 value">
                                          <?php echo $this->Form->input('paid_amount',array('label'=>false,'type'=>'text','class'=>'form-control required','placeholder'=>'', 'id'=>'paid_amount','onkeyup'=>'calculate_balance()','onkeypress'=>'return numbervalonly(event)')); ?>

                                </div>
                            </div>
                            
                              <div class="row static-info">
                                <div class="col-md-5 name">
                                    Balance :	 
                                </div>
                                <div class="col-md-7 value">
                                          <?php echo $this->Form->input('balance',array('label'=>false,'type'=>'text','class'=>'form-control required','placeholder'=>'', 'id'=>'balance','onkeypress'=>'return numbervalonly(event)')); ?>

                                </div>
                            </div>

                           </div>
                        </div>
                    </div>
                </div>
               
               
            </div>
            
             <div class="form-actions" style="text-align:center;">
        
        <button type="submit" name="Submit" class="btn blue">Submit</button>
<!--        <button type="reset" class="btn default">Reset</button>-->
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

       
    </div>



            <!-- END FORM-->
        </div>


    </div>
    </div>
   
</div>
 <?php echo $this->Form->end(); ?>   



<script>
function calculate_balance() {
	//var opening=parseFloat($('#opening').val());
	var paidamount=parseFloat($('#paid_amount').val());
	var ramount=parseFloat($('#receive_amount').val());
   var balanceamount=parseFloat(ramount-paidamount);
		$('#balance').val(balanceamount);
	    // alert(ramount);
	     
}
</script>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	
		$('#date').Zebra_DatePicker({format:"m-d-Y"});
		
		
	});
</script>

<script type="text/javascript">
$("input[type='checkbox']").change(function(){
    if($(this).is(":checked")){
       $('#receive_amount').css('background-color', 'yellow');
    }else{
       $('#receive_amount').css('background-color', 'white');  
    }
});
</script>

<script type="text/javascript">
function numbervalonly(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8 || charCode == 127 || charCode == 46 || (charCode > 47 && charCode < 58))
    return true;
    return false;
}
</script>




