<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">TIER/PR REPORT
</h3>
        </div>
      </div>
    </div>
    <!-- top header ends -->
    <!-- submit form -->
  
	
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-pencil	"></i> Edit TIER/PR REPORT 
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->

						</div>
						<div class="portlet-body">
							
                        	<?php echo $this->Form->create('TpReports', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
				
				
                   <div class="col-md-2 col-xs-6">
                    <label>Date </label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php 
					if($data['TpReports']['createdate']!='0000-00-00'){
					echo $this->Form->input('createdate', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>date('m-d-Y',strtotime($data['TpReports']['createdate'])))); 
					}else{
					echo $this->Form->input('createdate', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); 
					}?>
					
		          </div>
				  
				  
                  <!-- input 1-->
				   <div class="col-xs-6 col-md-2">
                <label>POS Type</label>
				<?php	
				echo $this->Form->input('POS_Type', array('type'=>'text','class' => 'form-control','required' => false, 'label'=>false,'value'=>$data['TpReports']['POS_Type']));?>
              </div>

				  
					<div class="col-md-2 col-xs-6">
                    <label>Product Name</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Product_Name', array('id' => 'Product_Name', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$data['TpReports']['Product_Name'])); ?>
					
					<?php echo $this->Form->input('id', array('id' => 'id', 'class' => 'form-control','type' => 'hidden', 'label' => false, 'required' => true,'value'=>$data['TpReports']['id'])); ?>
                  </div>

					<div class="col-md-2 col-xs-6">
                    <label>Volume</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Volume', array('id' => 'Volume', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['TpReports']['Volume'])); ?>
                  </div>
			
					
                  	<div class="col-md-2 col-xs-6">
                    <label>Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Amount', array('id' => 'Amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['TpReports']['Amount'])); ?>
                  </div>
				  
				  	<div class="col-md-2 col-xs-6">
                    <label>Sequence No</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Sequence_No', array('id' => 'Sequence_No', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['TpReports']['Sequence_No'])); ?>
                  </div>
                 
                </div>
				
				 <div class="row"><br/>
				</div>

              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" id="sunmit" name ="submit" class="btn default updatebtn" id="confirmpackid">Submit</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
          <?php echo $this->form->end(); ?>
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
		
			
				</div>
	
				</div>
				</div>
				</div>
			
			
<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker({format: 'm-d-Y'});
	
	$("#expiry").attr("readonly", false); 
	});

	</script>