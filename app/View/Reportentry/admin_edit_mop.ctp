<div class="portlet box blue">
               <div class="page-content portlet-body" >
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">MOP  REPORT
</h3>
        </div>
      </div>
    </div>
   
   
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-pencil"></i> Edit MOP REPORT 
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->

						</div>
						<div class="portlet-body">
							
                   	<?php echo $this->Form->create('MopReports', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
				
				
                   <div class="col-md-2 col-xs-6">
                    <label>Date </label>
                    <!-- important for developer please place your clander code on this input-->
                			<?php 
					if($data['MopReports']['createdate']!='0000-00-00'){
					
					echo $this->Form->input('createdate', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>date('m-d-Y',strtotime($data['MopReports']['createdate'])))); 
					}else{
					
					echo $this->Form->input('createdate', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); 
					}?>
                  </div>
				  
				  
                  <!-- input 1-->
				   <div class="col-xs-6 col-md-2">
                <label>POS Type</label>
				<?php	
				echo $this->Form->input('POS_Type', array('type'=>'text','class' => 'form-control','required' => false, 'label'=>false,'value'=>$data['MopReports']['POS_Type']));?>
              </div>

				  
					<div class="col-md-2 col-xs-6">
                    <label>Product Name</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Product_Name', array('id' => 'Product_Name', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true,'value'=>$data['MopReports']['Product_Name'])); ?>
                  </div>
			<div class="col-md-2 col-xs-6">
                    <label>Credit Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Credit_Amount', array('id' => 'Credit_Amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['MopReports']['Credit_Amount'])); ?>
                  </div>
				  
					<div class="col-md-2 col-xs-6">
                    <label>Cash Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Cash_Amount', array('id' => 'Cash_Amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['MopReports']['Cash_Amount'])); ?>
                  </div>
				  
				  <div class="col-md-2 col-xs-6">
                    <label>Cheque Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Cheque_Amount', array('id' => 'Cheque_Amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['MopReports']['Cheque_Amount'])); ?>
                  </div>
					
                  
				  	<div class="col-md-2 col-xs-6">
                    <label>Sequence No</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Sequence_No', array('id' => 'Sequence_No', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false,'value'=>$data['MopReports']['Sequence_No'])); ?>

					<?php echo $this->Form->input('id', array('id' => 'id', 'class' => 'form-control','type' => 'hidden', 'label' => false, 'required' => false,'value'=>$data['MopReports']['id'])); ?>
                  </div>
                 
                </div>
				
				 <div class="row"><br/>
				</div>

              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" id="sunmit" name ="submit" class="btn default updatebtn" id="confirmpackid">Submit</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
          <?php echo $this->form->end(); ?>
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
		
			
				</div>
	
				</div>
				</div>
			
			
			
<!--<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker({format: 'm-d-Y'});
	
	$("#expiry").attr("readonly", false); 
	});

	</script>-->
    
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#expiry" ).datepicker({ dateFormat: 'mm-dd-yy' });	
  } );
  </script>