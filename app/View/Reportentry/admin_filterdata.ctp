
				<div class="col-md-4">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> TIER/PR REPORT 
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
								<?php if($tpdate)
								{?>
								<i class="fa fa-calendar"></i> <?php  echo date("m-d-Y", strtotime($tpdate)) ;?>
								<?php }?>					
							</div>
						</div>
						<div class="portlet-body">
							
                           
                            <div class="table-responsive">
                               <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
										   <th><?php echo __($this->Paginator->sort('Product Name')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Volume')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Amount')); ?>  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <?php  
										$vtotal=0; 
										$atotal=0; 
										
										if (isset($treport) && !empty($treport)) {

if(sizeof($treport)==0){
										echo '<td colspan="3">  no result found </td>';
										}

										?>
                                            <?php foreach ($treport as $data){ ?>
                                                <tr>
                                                    <td> <?php echo $data['TpReports']['Product_Name']; ?> </td>
                                                    <td> <?php echo $data['TpReports']['Volume']; ?> </td>
                                                    <td> $<?php echo $data['TpReports']['Amount']; ?> </td>
                                                </tr>
 
                                            <?php 
											
											$vtotal = $vtotal + $data['TpReports']['Volume'];
											$atotal = $atotal + $data['TpReports']['Amount'];

											} ?>
											
											<tr><td><b>Total: </b></td><td><b><?php echo $vtotal; ?></b></td><td><b>$<?php echo $atotal; ?></b></td></tr>
											 
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="3">  no result found </td>
                                                </tr>
											<tr><td><b>Total: </b></td><td><b>0</b></td><td><b>$0</b></td></tr>
                                       <?php } ?>
									  
                                    </tbody>
                                </table>
								
                            </div>
                           
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
		
				<div class="col-md-4">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> TANK REPORT 
							</div>
						<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
								<?php if($tnpdate)
								{?>
								<i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($tnpdate)); ?>
								<?php }?>	
													
							</div>
						</div>
						<div class="portlet-body">
							 
                           
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
										   <th><?php echo __($this->Paginator->sort('Tank No #')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Tank Name')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Volume')); ?>  </th>

                                            <th><?php echo __($this->Paginator->sort('Amount')); ?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php 
										
										
										$vtotal=0; 
										$atotal=0; 
										if (isset($tnreports) && !empty($tnreports)) { 
										if(sizeof($tnreports)==0){
										echo '<td colspan="4">  no result found </td>';
										}
										?>
                                            <?php foreach ($tnreports as $data){ ?>
                                                <tr>
                                                    <td> <?php echo $data['TankReports']['Tank_Number']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Tank_Name']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Volume']; ?> </td>
                                                    <td> $<?php echo $data['TankReports']['Amount']; ?> </td>
                                                   
                                                </tr>

                                            <?php 
											$vtotal = $vtotal + $data['TankReports']['Volume'];
											$atotal = $atotal + $data['TankReports']['Amount'];

											
											} ?>
											<tr><td colspan="2"><b>Total: </b></td><td><b><?php echo $vtotal;?></b></td><td><b>$<?php echo $atotal;?></b></td></tr>
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="4">  no result found </td>
                                                </tr>
											<tr><td colspan="2"><b>Total: </b></td><td><b>0</b></td><td><b>$0</b></td></tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                           
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
	
				<div class="col-md-4">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> MOP REPORT 
							</div>
							
							<!--<div class="tools">
							
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>-->
							<div class="caption pull-right">
							<?php if($mpdate)
								{?>
								<i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($mpdate));  ?>
								<?php }?>	
							</div>
						</div>
						<div class="portlet-body">
							  
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
										   <th><?php echo __($this->Paginator->sort('Product Name')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Cash')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Credit')); ?>  </th>
                                             <th><?php echo __($this->Paginator->sort('Cheque')); ?>  </th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
										
										$catotal=0; 
										$crtotal=0; 
										$cqtotal=0; 
										if (isset($mreports) && !empty($mreports)) { 
										if(sizeof($mreports)==0){
										echo '<td colspan="4">  no result found </td>';
										}
										?>
                                            <?php foreach ($mreports as $data){ ?>
                                                <tr>
                                                    <td> <?php echo $data['MopReports']['Product_Name']; ?> </td>
                                                    <td> $<?php echo $data['MopReports']['Credit_Amount']; ?> </td>
                                                    <td> $<?php echo $data['MopReports']['Cash_Amount']; ?> </td>
                                                    <td> $<?php echo $data['MopReports']['Cheque_Amount']; ?> </td>
                                                   
                                                  
                                                </tr>
 
                                            <?php 
											
											$crtotal = $crtotal + $data['MopReports']['Credit_Amount'];
											$catotal = $catotal + $data['MopReports']['Cash_Amount'];
											$cqtotal = $cqtotal + $data['MopReports']['Cheque_Amount'];

											} ?>
											
											<tr><td><b>Total: </b></td>
											<td><b>$<?php echo $crtotal; ?></b></td>
											<td><b>$<?php echo $catotal; ?></b></td>
											<td><b>$<?php echo $cqtotal; ?></b></td>
											</tr>
											 
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="4">  no result found </td>
                                                </tr>
											<tr><td><b>Total: </b></td><td><b>$0</b></td><td><b>$0</b></td></tr>
                                       <?php } ?>
									   
                                     
                                    </tbody>
                                </table>
                            </div>
                           
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->

			
