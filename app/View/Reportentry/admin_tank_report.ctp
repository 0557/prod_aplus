<div class="portlet box blue">
               <div class="page-content portlet-body" >
			   <div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-plus"></i> New TANK REPORT
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->

						</div>
						<div class="portlet-body">
							
              	<?php echo $this->Form->create('TankReports', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
              <div class="confim_form">
              <!-- row one -->
                <div class="row">
				
				
                   <div class="col-md-2 col-xs-6">
                    <label>Date </label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('createdate', array('id' => 'expiry', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				  
                  <!-- input 1-->
				   <div class="col-xs-6 col-md-2">
                <label>POS Type</label>
				<?php	
				echo $this->Form->input('POS_Type', array('type'=>'text','class' => 'form-control','required' => false, 'label'=>false));?>
              </div>
				<div class="col-md-2 col-xs-6">
                    <label>Tank Number</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Tank_Number', array('id' => 'Tank_Number', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
					<div class="col-md-2 col-xs-6">
                    <label>Tank Name</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Tank_Name', array('id' => 'Tank_Name', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => true)); ?>
                  </div>

					<div class="col-md-2 col-xs-6">
                    <label>Volume</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Volume', array('id' => 'Volume', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
			
					
                  	<div class="col-md-2 col-xs-6">
                    <label>Amount</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Amount', array('id' => 'Amount', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
				  
				  	<div class="col-md-2 col-xs-6">
                    <label>Sequence No</label>
                    <!-- important for developer please place your clander code on this input-->
                	
					<?php echo $this->Form->input('Sequence_No', array('id' => 'Sequence_No', 'class' => 'form-control','type' => 'text', 'label' => false, 'required' => false)); ?>
                  </div>
                 
                </div>
				
				 <div class="row"><br/>
				</div>

              <div class="row">
         
              <div class="col-xs-6 col-md-2">
			   <label>&nbsp;</label>
                <button type="submit" id="sunmit" name ="submit" class="btn default updatebtn" id="confirmpackid">Submit</button>
              </div>
         
              </div>
              <!-- row two ends -->
              </div>
          <?php echo $this->form->end(); ?>
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
		
			
				</div>
	
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-bars"></i> TANK REPORT
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->

						</div>
						<div class="portlet-body">
							
                           
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
										   <th><?php echo __($this->Paginator->sort('Tank Number')); ?>  </th>
										   <th><?php echo __($this->Paginator->sort('Tank Name')); ?>  </th>
										   <th><?php echo __($this->Paginator->sort('POS Type')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Volume')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Amount')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Sequence No #')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Action')); ?>  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <?php  
										$vtotal=0; 
										$atotal=0; 
										if (isset($list) && !empty($list)) { ?>
                                            <?php foreach ($list as $data){ ?>
                                                <tr>
                                                    <td> <?php echo $data['TankReports']['Tank_Number']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Tank_Name']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['POS_Type']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Volume']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Amount']; ?> </td>
                                                    <td> <?php echo $data['TankReports']['Sequence_No']; ?> </td>
                                                   
                                                    <td>
                                                 <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('action'=>'edit_tank_report',$data['TankReports']['id']) ,array('escape' => false,'class'=>'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>',array('action'=>'tank_report',$data['TankReports']['id']) ,array('escape' => false,'class'=>'newicon red-stripe delete')); ?>
                                                        
                                                    </td>
                                                </tr>
 
                                            <?php 
											
											$vtotal = $vtotal + $data['TankReports']['Volume'];
											$atotal = $atotal + $data['TankReports']['Amount'];

											} ?>
											
											<tr><td colspan="3"><b>Total:</b> </td><td><b><?php echo $vtotal; ?></b></td><td colspan="3"><b>$<?php echo $atotal; ?></b></td></tr>
											 
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="7">  no result found </td>
                                                </tr>
											<tr><td colspan="3"><b>Total:</b> </td><td>0</td><td colspan="3">$0</td></tr>
                                       <?php } ?>
									  
                                    </tbody>
                                </table>
								
                            </div>
                           
            </div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
		
			
				</div>
				</div>
				</div>
			
<!--<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker({format: 'm-d-Y'});
	
	$("#expiry").attr("readonly", false); 
	});

	</script>-->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#expiry" ).datepicker({ dateFormat: 'mm-dd-yy' });
  } );
  </script>