<?php 
$fMopList = array(0=>'All MOP',1 => 'CASH', 2 => 'CRED', 3 => 'CHEC');
?>
<div class="portlet box blue">
    <div class="page-content portlet-body" >
<?php if($this->Session->read('stores_id')){?>
            <div class="row">
                <div class="col-xs-12">
                  <h3 class="page-title">Dashboard</h3>
                </div>
            </div>

                <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="portlet box blue">
                                        <div class="portlet-body">
                                        
                                                   <?php echo $this->Form->create('report', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                <div class="col-md-4" style="padding-left:0px;">
                                        <label style="display:block;"><i class="fa fa-calendar"></i> Update Date</label> <input type="text" class="form-control" id="expiry" name="expiry" value="<?php echo $endate;?>"> 
                                        </div>
                                        <div class="col-md-3">
                                        <label style="display:block;">&nbsp;</label>
                                        <button type="submit" class="btn btn-success" ><i class="fa fa-arrow-left fa-fw"></i> Get Report</button></div>
                              <?php echo $this->form->end(); ?>
                               <div class="ht50"></div>         
                                            
                        </div>
                                
                    </div>
                </div>
                </div>

            	<div class="row" id="filterdata">
             
                    <div class="col-md-4">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title col-md-12">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i> TIER/PR REPORT 
                                </div>
                              
                           <div class="caption pull-right">
                                    <?php if($endate)
                                    {?>
                                    <i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($endate)) ;?>
                                    <?php }?>					
                                </div>
                            </div>
                            <div class="portlet-body">
                                
                               
                                <div class="table-responsive">
                                   <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                               <th><?php echo __($this->Paginator->sort('Product Name')); ?>  </th>
                                                <th><?php echo __($this->Paginator->sort('Volume')); ?>  </th>
                                                <th><?php echo __($this->Paginator->sort('Amount')); ?>  </th>
                                            </tr>
                                        </thead>
                                        <tbody>                                           
                                            <?php  
										$_summaryTotal = array();
										if(!empty($rubyUprodts)) {
										//pr($rubyUprodts);die;	
										foreach($rubyUprodts as $_k => $_value) {
										$_ukey = @$prodList[$_value['RubyUprodt']['fuel_product_number']];
										
										if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
										$_summaryTotal[$_ukey]['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
										$_summaryTotal[$_ukey]['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
										} else {
										$_summaryTotal[$_ukey]['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
										$_summaryTotal[$_ukey]['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
										}
										
										if (isset($_summaryTotal['Total']['fuel_volume'])) {
										$_summaryTotal['Total']['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
										$_summaryTotal['Total']['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
										} else {
										$_summaryTotal['Total']['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
										$_summaryTotal['Total']['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
										}
										}
										}
											
                                         
                                          if(!empty($rubyUprodts) && !empty($_summaryTotal)) {											
											foreach($prodList as $key => $value){
                                            ?>
                                            <tr>
                                            <td>
                                            <?php
                                            echo $value;
											?> 
                                            </td>
                                            <td> <?php //echo $_summaryTotal[$value]['fuel_volume'];
											       if($_summaryTotal[$value]['fuel_volume']!=''){
												    echo  $_summaryTotal[$value]['fuel_volume'];
												   }else{
													echo "0";
												   }									
											 ?> </td>
                                            <td> $<?php //echo $_summaryTotal[$value]['fuel_value'];
													if($_summaryTotal[$value]['fuel_value']!=''){
													echo  $_summaryTotal[$value]['fuel_value'];
													}else{
													echo "0";
													}									
											 ?> </td>
                                            </tr>     
                                            <?php                                               
                                             }                                        	
										    ?>                                                
                                        <tr>
                                        <td><b>Total:</b> </td>
                                        <td><b><?php echo $_summaryTotal['Total']['fuel_volume']; ?></b></td>
                                        <td><b>$<?php echo $_summaryTotal['Total']['fuel_value']; ?></b></td>
                                        </tr>                                                 
                                        <?php
										}else 
										{
									    ?>
                                        <tr>
                                        <td colspan="3">No results found! </td>
                                        </tr>
                                        <tr>
                                        <td><b>Total:</b> </td>
                                        <td><b>0</b></td>
                                        <td><b>$0</b></td>
                                        </tr>
                                        <?php
										}
										?>                                          
                                        </tbody>
                                    </table>
                                    
                                </div>
                               
                </div>
                            </div>
                        </div>
                        
            
                    <div class="col-md-4">                        
                        <div class="portlet box yellow">
                            <div class="portlet-title col-md-12">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i> TANK REPORT 
                                </div>                          
                              <div class="caption pull-right">
                                    <?php if($endate)
                                    {?>
                                    <i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($endate));?>
                                    <?php }?>	
                                                        
                                </div>
                            </div>
                            <div class="portlet-body">
                                 
                               
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                               <th><?php echo __($this->Paginator->sort('Tank No #')); ?>  </th>
                                                <th><?php echo __($this->Paginator->sort('Volume')); ?>  </th>
    
                                                <th> <?php echo __($this->Paginator->sort('Amount')); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                             
                                            $vtotal=0; 
                                            $atotal=0; 
                                            if (isset($tnreports) && !empty($tnreports)) { 
                                            if(sizeof($tnreports)==0){
                                            echo '<td colspan="4">  no result found </td>';
                                            }
                                            ?>
                                                <?php
												 //echo '<pre>';print_r($tnreports);die;
												 foreach ($tnreports as $data){
													 
													/* if($data['RubyUtankt']['fuel_volume']!=0 && $data['RubyUtankt']['fuel_value']!=0){*/
													  ?>
                                                    <tr>
                                                        <td> <?php echo $data['RubyUtankt']['tank_number']; ?> </td>
                                                        <td> <?php echo $data['RubyUtankt']['fuel_volume']; ?> </td>
                                                        <td> $<?php echo $data['RubyUtankt']['fuel_value']; ?> </td>
                                                       
                                                    </tr>
    
                                                <?php 
                                                $vtotal = $vtotal + $data['RubyUtankt']['fuel_volume'];
                                                $atotal = $atotal + $data['RubyUtankt']['fuel_value'];
                                                /*}*/ 
											    }?>
                                                <tr>
                                                <td><b>Total:</b> </td>
                                                <td><b><?php echo $vtotal;?></b></td>
                                                <td><b>$<?php echo $atotal;?></b></td>
                                                </tr>
                                            <?php }else { ?>
                                                <tr>
                                                        <td colspan="4">  no result found </td>
                                                    </tr>
                                                <tr><td colspan="2"><b>Total:</b> </td><td><b>0</b></td><td><b>$0</b></td></tr>
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                               
                </div>
                            </div>
                        </div>
                        
        
                    <div class="col-md-4">                   
                        <div class="portlet box green">
                            <div class="portlet-title col-md-12">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i> MOP REPORT 
                                </div>                               
                                <div class="caption pull-right">
                                <?php if($endate)
                                    {?>
                                    <i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($endate)); ?>
                                    <?php }?>	
                                </div>
                            </div>
                            <div class="portlet-body">
                                  
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                               <th><?php echo __($this->Paginator->sort('Product Name')); ?>  </th>                                               <th><?php echo __($this->Paginator->sort('Cash ')); ?>  </th>
                                               <th><?php echo __($this->Paginator->sort('Credit')); ?>  </th>
                                               <th><?php echo __($this->Paginator->sort('Cheque')); ?>  </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php   
											//pr($prodList);  die; 
											$total_cash=0;
											$total_credit=0;
											$total_cheque=0;
											//pr($mreports); die;
																					
                                            if (isset($mreports) && !empty($mreports) && sizeof($mreports)!=0) { 
											    foreach($prodList as $key=>$val){
													$mop_array = array();
													foreach($mreports as $mop_row){		
														//pr($mop_row['RubyUprodt']);
														if($key==$mop_row['RubyUprodt']['fuel_product_number']){
															if($mop_row['RubyUprodt']['fmop1']==1){	
																$mop_array[1]=$mop_row['RubyUprodt'];
															}
															if($mop_row['RubyUprodt']['fmop1']==2){	
																$mop_array[2]=$mop_row['RubyUprodt'];
															}
															if($mop_row['RubyUprodt']['fmop1']==3){	
																$mop_array[3]=$mop_row['RubyUprodt'];
															}
															
														}
													}	
											//pr($mop_array[2]);	
											 
											  $total_cash=$total_cash+$mop_array[1]['fuel_value'];
											  $total_credit=$total_credit+$mop_array[2]['fuel_value'];
											  $total_cheque=$total_cheque+$mop_array[3]['fuel_value'];
											 
											?> 
											<tr>                                              
                                                   <td><?php echo $val; ?></td>  
                                                   <td>$<?php
												   if($mop_array[1]['fuel_value']!=''){
												    echo  $mop_array[1]['fuel_value'];
												   }else{
													echo "0";
												   }											   
												   ?>
                                                   </td>
                                                   <td>$<?php 
												     if($mop_array[2]['fuel_value']!=''){
												     echo  $mop_array[2]['fuel_value'];
												   }else{
													 echo "0";
												   }
												 ?></td>
                                                   <td>$<?php 												   
												     if($mop_array[3]['fuel_value']!=''){
												     echo  $mop_array[3]['fuel_value'];
												   }else{
													 echo "0";
												   }
												  ?></td>
                                              </tr>
											<?php 
											}
                                            ?>
                                            <?php
											
					/*						$diesel_key = array_search ('Diesel',$prodList);
											$regular_key = array_search ('Regular',$prodList);
											$premium_key = array_search ('Premium',$prodList);
											$plus_key = array_search ('Plus',$prodList);
											$mop_diesel_array=array();	
											$mop_regular_array=array();
											$mop_premium_array=array();
											$mop_plus_array=array();
											*/
									
											/*foreach($mreports as $mop_row){		
											//pr($mop_row);die;
											if($mop_row['RubyUprodt']['fuel_product_number']==$diesel_key){
											if($mop_row['RubyUprodt']['fmop1']==1){	
											$mop_diesel_array[1]=$mop_row;
											}
											if($mop_row['RubyUprodt']['fmop1']==2){		
											$mop_diesel_array[2]=$mop_row;	
											}
											if($mop_row['RubyUprodt']['fmop1']==3){			
											$mop_diesel_array[3]=$mop_row;
											}
											}
											if($mop_row['RubyUprodt']['fuel_product_number']==$regular_key){
											
											if($mop_row['RubyUprodt']['fmop1']==1){	
											$mop_regular_array[1]=$mop_row;
											}
											if($mop_row['RubyUprodt']['fmop1']==2){		
											$mop_regular_array[2]=$mop_row;	
											}
											if($mop_row['RubyUprodt']['fmop1']==3){			
											$mop_regular_array[3]=$mop_row;
											}
											}
											if($mop_row['RubyUprodt']['fuel_product_number']==$premium_key){
											
											if($mop_row['RubyUprodt']['fmop1']==1){	
											$mop_premium_array[1]=$mop_row;
											}
											if($mop_row['RubyUprodt']['fmop1']==2){		
											$mop_premium_array[2]=$mop_row;	
											}
											if($mop_row['RubyUprodt']['fmop1']==3){			
											$mop_premium_array[3]=$mop_row;
											}
											}
											if($mop_row['RubyUprodt']['fuel_product_number']==$plus_key){
											
											if($mop_row['RubyUprodt']['fmop1']==1){	
											$mop_plus_array[1]=$mop_row;
											}
											if($mop_row['RubyUprodt']['fmop1']==2){		
											$mop_plus_array[2]=$mop_row;	
											}
											if($mop_row['RubyUprodt']['fmop1']==3){			
											$mop_plus_array[3]=$mop_row;
											}
											}
											}	*/
											?>	
                                           <?php /*?> <?php if(isset($diesel_key) && $diesel_key!='')
											{
											?>
										      <tr>                                              
                                                   <?php 
												   $total_cash=$total_cash+$mop_diesel_array[1]['RubyUprodt']['fuel_value'];
												   $total_credit=$total_credit+$mop_diesel_array[2]['RubyUprodt']['fuel_value'];
												   $total_cheque=$total_cheque+$mop_diesel_array[3]['RubyUprodt']['fuel_value'];
												   ?>
                                                   <td>Diesel</td>  
                                                   <td>$<?php echo $mop_diesel_array[1]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php 
												     if($mop_diesel_array[2]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_diesel_array[2]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												   
												  // echo $mop_diesel_array[2]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php 
												   
												     if($mop_diesel_array[3]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_diesel_array[3]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												   
												   
												  // echo $mop_diesel_array[3]['RubyUprodt']['fuel_value']; ?></td>
                                              </tr> 
                                           <?php
										    }										   
										    ?>   
                                              <?php if(isset($regular_key) && $regular_key!='')
											{
											?> 		
											  <tr>
                                                   <?php 
												   $total_cash=$total_cash+$mop_regular_array[1]['RubyUprodt']['fuel_value'];
												   $total_credit=$total_credit+$mop_regular_array[2]['RubyUprodt']['fuel_value'];
												   $total_cheque=$total_cheque+$mop_regular_array[3]['RubyUprodt']['fuel_value'];
												   ?>
                                                   <td>Regular</td>  
                                                   <td>$<?php echo $mop_regular_array[1]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php 
												   
												     if($mop_regular_array[2]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_regular_array[2]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												   
												   ?></td>
                                                   <td>$<?php 
												      if($mop_regular_array[3]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_regular_array[3]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												   
												   //echo $mop_regular_array[3]['RubyUprodt']['fuel_value']; ?></td>
                                              </tr>
                                             <?php
										    }										   
										    ?>   
                                              <?php if(isset($premium_key) && $premium_key!='')
											{
											?> 
											  <tr> 
                                                   <?php 
												   $total_cash=$total_cash+$mop_premium_array[1]['RubyUprodt']['fuel_value'];
												   $total_credit=$total_credit+$mop_premium_array[2]['RubyUprodt']['fuel_value'];
												   $total_cheque=$total_cheque+$mop_premium_array[3]['RubyUprodt']['fuel_value'];
												   ?>
                                                   <td>Premium</td>  
                                                   <td>$<?php echo $mop_premium_array[1]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php
												 
												   if($mop_premium_array[2]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_premium_array[2]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }?></td>
                                                   <td>$<?php 
												      if($mop_premium_array[3]['RubyUprodt']['fuel_value']!=''){
												   echo $mop_premium_array[3]['RubyUprodt']['fuel_value']; }else{
													   echo "0";
												   } ?></td>
                                              </tr>
                                              <?php
										    }										   
										    ?> 
                                            
                                            <?php if(isset($plus_key) && $plus_key!='')
											{
											?> 
											  <tr>
                                                   <?php 
												   $total_cash=$total_cash+$mop_plus_array[1]['RubyUprodt']['fuel_value'];
												   $total_credit=$total_credit+$mop_plus_array[2]['RubyUprodt']['fuel_value'];
												   $total_cheque=$total_cheque+$mop_plus_array[3]['RubyUprodt']['fuel_value'];
												   ?>
                                                   <td>Plus</td>  
                                                   <td>$<?php echo $mop_plus_array[1]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php
												   if($mop_plus_array[2]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_plus_array[2]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												 //   echo $mop_plus_array[2]['RubyUprodt']['fuel_value']; ?></td>
                                                   <td>$<?php
												     if($mop_plus_array[3]['RubyUprodt']['fuel_value']!=''){
												   
												 echo  $mop_plus_array[3]['RubyUprodt']['fuel_value'];
												   }else{
													   echo "0";
												   }
												   
												   
												   // echo $mop_plus_array[3]['RubyUprodt']['fuel_value']; ?></td>
                                              </tr>											
                                             <?php
										    }										   
										    ?>    <?php */?>
                                                <tr>
                                                <td><b>Total:</b> </td>                                           
                                                <td><b>$<?php echo $total_cash; ?></b></td>
                                                <td><b>$<?php echo $total_credit; ?></b></td>
                                                <td><b>$<?php echo $total_cheque; ?></b></td>
                                                </tr>
                                                 
                                                <?php
												 }else{
											    ?>
                                                <tr>
                                                <td colspan="4"> No results found! </td>
                                                </tr>
                                                <tr>
                                                <td><b>Total:</b></td>
                                                <td><b>$0<b></td>
                                                <td><b>$0<b></td>
                                                <td><b>$0</b></td>
                                                </tr>
                                               <?php 
											   }
											   ?>
                                           
                                         
                                        </tbody>
                                    </table>
                                </div>
                               
                </div>
                            </div>
                            
                    </div>
                 
			    </div>
                <div class="row" >
                
                 <div class="col-md-12">    
                  <table id="example" class="table table-striped table-bordered table-advance table-hover" bgcolor="#0033FF">
                                            
                            <tbody>
                             
                                    <?php
                                    //echo '<pre>'; print_r($Salereports);die;
                                    $total_amount = 0;
                                     if (isset($Salereports) && !empty($Salereports)) { 
                                    foreach ($Salereports as $data) {
                                      
                                        $amount = $data['Houseacccustsale']['amount'];
                                        $total_amount = $total_amount + $amount;
                                        ?>
                                        
                                    <?php } }
                                    ?>
                                <thead bgcolor="#3399FF">
                                    <tr>
                                        <th colspan=10>Total House Account Sales:</th>
                                        <th></th>  
                                        <th></th>    
                                        <th></th> 
                                        <th><?php echo '$ ' . $total_amount; ?></th>                                       
                                    </tr>
                                </thead>  
                              
                            </tbody>
                        </table>
                 </div>
                </div>
                
	</div>
</div>
			

<?php } ?>

<!--<script type="text/javascript" charset="utf-8">
   $(document).ready(function (e) {

	$('#expiry').Zebra_DatePicker({format: 'Y-m-d'});
	
	$("#expiry").attr("readonly", false); 
	
	});

	</script>-->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#expiry" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>
    
    <style type="text/css">
    button.Zebra_DatePicker_Icon_Inside{ top:24px !important;}
	.ht50{ display:block; height:50px;}
    </style>