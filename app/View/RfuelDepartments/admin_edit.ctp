<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit R Fuel Department
                    </div>
                    
                </div>
                <div class="portlet-body form">
                    <?php echo $this->Form->create('RfuelDepartment', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                    <!--<form role="form">-->
                    <div class="form-body">
<!--                        <div class="form-group">
                            <label for="CustomerStore">Store</label>
                         <?php if(isset($_SESSION['store_id']) || !empty($_SESSION['store_id'])) {  ?>
                            <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'empty' => 'Select Store','selected' =>$_SESSION['store_id'],'options'=>$stores)); ?>
                            <?php }else {  ?>
                            <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'empty' => 'Select Store','options'=>$stores)); ?>
                            <?php } ?>
                            <span class="help-block">

                            </span>
                        </div>-->
                       
                        
                        <div class="form-group">
                            <label for="exampleInputName">Department Name</label>
                            <?php 
							echo $this->Form->input('id');
							
							echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'id' => 'exampleInputName', 'required' => 'false', 'placeholder' => 'Enter Department Name')); ?>

                            <span class="help-block">

                            </span>
                        </div>
                      
                        

                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <?php echo $this->form->end(); ?>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>

    </div>

    <!-- END PAGE CONTENT-->
</div>
</div>
