<div class="portlet box blue">
               <div class="page-content portlet-body" >
   
    <div class="row">
	
	<div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View Fuel Product Info : <?php echo $wholesaleProduct['Rproduct']['id'];     ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <!--<h2 class="margin-bottom-20"> View  Info - FuelInvoice : <?php echo $wholesaleProduct['Rproduct']['id'];     ?> </h2>-->
                                        
                                        <div class="row">
                                        <div class="col-md-6 col-sm-12">
												<div class="portlet yellow box">
                                        <div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Product Information
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																 Select Department:
															</div>
															<div class="col-md-7 value">
																<?php echo h($wholesaleProduct['FuelDepartment']['name']); ?>
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Product Name:
															</div>
															<div class="col-md-7 value">
																<?php echo h($wholesaleProduct['Rproduct']['name']); ?>
																
															</div>
														</div>
                                                       
                                                       
<!--														<div class="row static-info">
															<div class="col-md-5 name">
															Load Date:	 
															</div>
															<div class="col-md-7 value">
                                                                                                             <?php echo date('d F Y h:i A', strtotime($wholesaleProduct['Rproduct']['load_date'])); ?>
                                                                                                                            <?php //echo $fuelInvoice['FuelInvoice']['load_date']; ?>
																 
																
															</div>
														</div>-->
                                                        
														
                                                        
														
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Description: 
															</div>
															<div class="col-md-7 value">
															<?php echo $wholesaleProduct['Rproduct']['description']; ?>	 
																
															</div>
														</div>
                                                        
														
                                                                                                               <div class="row static-info">
															<div class="col-md-5 name">
															Status:	 
															</div>
															<div class="col-md-7 value">
                                                                                                                            <span class="label label-success">	<?php echo $wholesaleProduct['Rproduct']['status']; ?></span>
																
															</div>
														</div>
                                                        
                                                        
                                                    </div>
                                                    </div>
													</div>
                                                    <div class="col-md-6 col-sm-12">
												<div class="portlet blue box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Manage Inventory
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
<!--                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Select Store:
															</div>
															<div class="col-md-7 value">
																<?php echo $wholesaleProduct['Store']['name']; ?>
															</div>
														</div>-->
                                                    <div class="row static-info">
															<div class="col-md-5 name">
																Select Vender: 
															</div>
															<div class="col-md-7 value">
																<?php echo $wholesaleProduct['Customer']['name']; ?>
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Price Per Gallon:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['Rproduct']['price']; ?>
															</div>
														</div>
                                                    
														<div class="row static-info">
															<div class="col-md-5 name">
																 Vendor Price:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['Rproduct']['vendor_price']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Tax Class:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['Rproduct']['tax_class']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Qty Available in Gallon:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['Rproduct']['qty']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Opening Book Inventory:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['Rproduct']['opening_product_amount']; ?>
															</div>
														</div>
                                                       
                                                        </div>
                                                    </div>
													</div>
                                        
                                            
                                 
                                <!-- END FORM-->
                            </div>
                                        
                                         <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption information_product">
                        <i class="fa fa-reorder"></i> UST Information
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>
            
             <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Tank No:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['tank_no']; ?>
                                    
                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Capacity:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['tank_capacity']; ?>
                                   
                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Min Qty:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['min_qty']; ?>
                                   
                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Max Qty:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['max_qty']; ?>
                                    
                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Piping:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['piping']; ?>
                                                                 
                                
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Type:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['Rproduct']['tank_type']; ?>
                                    
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Piping Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo date('d F Y h:i A', strtotime($wholesaleProduct['Rproduct']['Piping_date'])); ?>
                                    
                                </div>
                            </div>
   
                        </div></div></div>
               
            </div>
                                        
                                        
                                        
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div></div></div>
    <!-- END PAGE CONTENT-->
   