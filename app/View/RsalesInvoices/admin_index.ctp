
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                 Fuel Sales INVOICE <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('id')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('store_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('customer_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('bill_to'); ?></th>
                                            <th><?php echo $this->Paginator->sort('ship_to'); ?></th>
                                            <th><?php echo $this->Paginator->sort('bol'); ?></th>
                                            <th><?php echo $this->Paginator->sort('po'); ?></th>
                                            <th><?php echo $this->Paginator->sort('invoice_date'); ?></th>
                                            <th><?php echo $this->Paginator->sort('due_date'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status'); ?></th>
                                            
                                            <th>Total</th>

                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rsalesInvoice) && !empty($rsalesInvoice)) { ?>

                                            <?php foreach ($rsalesInvoice as $data) { ?>
                                                <tr>
                                                    <td><?php echo h($data['RsalesInvoice']['id']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Store']['name']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Customer']['name']); ?>&nbsp;</td>
                                                      <td><?php echo h($data['RsalesInvoice']['bill_to']); ?>&nbsp;</td>
                                                       <td><?php echo h($data['RsalesInvoice']['ship_to']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RsalesInvoice']['bol']); ?></td>
                                                   <td><?php echo h($data['RsalesInvoice']['po']); ?>&nbsp;</td>
                                                   <td><?php echo  date('d F,Y', strtotime($data['RsalesInvoice']['invoice_date']));  ?>&nbsp;</td>
                                                    <td><?php echo  date('d F,Y', strtotime($data['RsalesInvoice']['due_date']));  ?>&nbsp;</td>
                                                    <td><span class="label label-success"><?php echo h($data['RsalesInvoice']['status']); ?></span></td>
                                                    
                                                    
                                                   

                                                    

                                                    <td><?php echo $data['RsalesInvoice']['gross_amount']; ?>&nbsp;</td>
                                                    

                                                    <td>
                                                        <?php echo $this->Html->link('View', array('action' => 'view', $data['RsalesInvoice']['id']), array('class' => 'btn default btn-xs red-stripe')); ?>
                                                        <?php echo $this->Html->link('Edit', array('action' => 'edit', $data['RsalesInvoice']['id']), array('class' => 'btn default btn-xs red-stripe')); ?>
                                                        <?php echo $this->Html->link('Delete', array('action' => 'delete', $data['RsalesInvoice']['id']), array('class' => 'btn default btn-xs red-stripe')); ?>

                                                        <?php
                                         if(!empty($data['RsalesInvoice']['invoice_file'])){ ?>
                                                        <a href="<?php echo Router::url('/',true);?>uploads/sale_invoice/<?php echo $data['RsalesInvoice']['invoice_file'];?>" target="_blank" class="btn default btn-xs red-stripe" >Print Pdf</a>
                      <?php      } else { ?>
                                                        <span class="btn default btn-xs red-stripe"> No  File  </span>
                     <?php  } ?>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>
