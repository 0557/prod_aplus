
<style>
.t-con{
 padding:25px 0;
 border:1px solid #ddd;
}
.datepicker{
 z-index:10000;
}

.with-padding {
    padding: 16px;
}
.checker, .checker span, .checker input {
	width: 18px;
	height: 18px;
	border-radius: 50%;
	margin-top: 2px;
}
.checker span:after {
	content: "";
	background: url(../images/forms/choice_white.png) no-repeat 50% !important;
	opacity: 0;
	filter: alpha(Opacity=0);
	display: block;
	height: 100%;
	width: 100%;
	position: absolute;
	top: 0;
	left: 0;
}
.newbrdr{
 /*border-left: 1px solid #ddd !important;*/
}
.select2-container-multi .select2-choices {
border: none;
}
.select2-container {
 border:1px solid #ddd;
}
.send-mar {
    margin-top:6px;
}
.previous-file{
	 float:right;
	 margin-left:10px;
}
.upload-button-remove > span{
	margin:0 !important;
}
.previous-file > btn-default {
	border:none;
}

.date_box{
	position:relative;
}
.Zebra_DatePicker
{
	z-index:2000 !important;	
}
.Zebra_DatePicker_Icon
{
	visibility:hidden;
}
.form-co {
    border: 1px solid #adadad;
    height: 30px;
}


.fc.fc-ltr.fc-unthemed {
    margin: 30px auto 30px 279px;
    width: 78%;
}

</style>

<style type="text/css">
@media only screen 
  and (min-width: 320px) 
  and (max-width: 480px)
  {
.fc.fc-ltr.fc-unthemed {
    margin: 30px auto 30px 20px;
    width: 90%;
}  

}



@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.fc.fc-ltr.fc-unthemed {
    margin: 30px auto 30px 20px;
    width: 90%;
}  
}








</style>


<link href='<?php echo $this->webroot; ?>css/fullcalender/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo $this->webroot; ?>css/fullcalender/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo $this->webroot; ?>js/fullcalender/moment.min.js'></script>
<script src='<?php echo $this->webroot; ?>js/fullcalender/fullcalendar.min.js'></script>

<script>

	$(document).ready(function() {	
		var today =new Date();
		
		$('#calendar').fullCalendar({			
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month'
			},
			defaultDate: '<?php echo (isset($_REQUEST['date']) && !empty($_REQUEST['date']))?date('Y-m-d', strtotime($_REQUEST['date'])):date('Y-m-d');?>',
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
			   var s=new Date('<?php echo date('Y-m-d', strtotime(' -1 day')); ?>');
			   if(s<start){
			   	var momentDate = start;
                var momentDateString = momentDate.format("DD-MM-YYYY");
			   	$("#startDate").val(momentDateString);
			   	
			   	$("#add-event-form").modal('show');
			   }
               

			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: {
				url: '<?php echo $this->webroot ?>RubyDailyreportings/get_daily_reporting_jsondata',
				error: function() {
					
				}
			},
			eventClick: function(calEvent) {
				
		    $("#loaderdiv").show();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $this->webroot ?>RubyDailyreportings/get_daily_reporting",
                data: {id: calEvent.id},
                success: function(data) {					
                    $('#event_details .modal-body').html(data);
                    $("#loaderdiv").hide();
                    $('#open_modal').click();
                    $("#removebtn").show();
                	}
           	 	});
        	}
		});
		
		
		
	});

</script>
<script>
	function convert(str) {
    var mnths = { 
        Jan:"01", Feb:"02", Mar:"03", Apr:"04", May:"05", Jun:"06",
        Jul:"07", Aug:"08", Sep:"09", Oct:"10", Nov:"11", Dec:"12"
    },
    date = str.split(" ");

    return [ date[3], mnths[date[1]], date[2] ].join("-");
}

</script>

<?php echo $this->Session->flash(); ?>
<div id="loaderdiv" style="position: absolute; width: 100%; text-align: center; vertical-align: middle; display:none; z-index:99; margin-top:42px;">

</div>
<!-- Content -->

<div class="clearfix">
<div id='calendar'></div>

</div>

<!-- Start Of Modal -->

<div class="modal fade" id="event_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header colourheading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
       <h4 class="modal-title" id="myModalLabel">Event Details</h4>
      </div>
      <div class="modal-body with-padding"></div>
    </div>
  </div>
</div>
<!-- End of Modal -->
<!-- MODAL FOR EVENT DATA SUBMIT -->
<div class="modal fade" id="add-event-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header colourheading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
       <h4 class="modal-title" id="myModalLabel">Event Details</h4>
      </div>
      <div class="modal-body with-padding">
      	<div id="" title="Add New Event">
      	    <div id="add_time_error"></div>
      	
      	
		
	
</div>	
      </div>
    </div>
  </div>
</div>
<!-- MODAL FOR EVENT DATA SUBMIT -->


<!-- for datepicker -->


<script type="text/javascript">
$(document).ready(function (){
	 $('#startDate').Zebra_DatePicker({
	 	direction: true,
	 });
	 $('#endDate').Zebra_DatePicker({
	 	direction: true
	 });
})
</script>