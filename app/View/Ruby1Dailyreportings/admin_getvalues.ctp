				<?php
                $_summaryTotal = array();
                if(!empty($rubyUprodts)) {
                //pr($rubyUprodts);die;	
                foreach($rubyUprodts as $_k => $_value) {
                $_ukey = @$prodList[$_value['RubyUprodt']['fuel_product_number']];
                
                if (!isset($_summaryTotal[$_ukey]['fuel_volume'])) {
                $_summaryTotal[$_ukey]['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
                $_summaryTotal[$_ukey]['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
                } else {
                $_summaryTotal[$_ukey]['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
                $_summaryTotal[$_ukey]['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
                }
                
                if (isset($_summaryTotal['Total']['fuel_volume'])) {
                $_summaryTotal['Total']['fuel_volume'] += $_value['RubyUprodt']['fuel_volume'];
                $_summaryTotal['Total']['fuel_value']  += $_value['RubyUprodt']['fuel_value'];
                } else {
                $_summaryTotal['Total']['fuel_volume'] = $_value['RubyUprodt']['fuel_volume'];
                $_summaryTotal['Total']['fuel_value']  = $_value['RubyUprodt']['fuel_value'];
                }
                }
				
				//pr($_summaryTotal);die;	
                }				
                ?>
               
               <div class="panel-body form-container">             
              
                <div class="row">
                
                  <div class="col-md-12 col-sm-12">
            
                    <div class="col-md-6" data-spy="scroll">
                      
                      <div class="inside-sales">
                        <h2>Inside Sales</h2>
                      </div>                    
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                           <div class="form-cont">                
                            <div class="form-control">
                            <?php
                            if(!isset($RubyTottaxe[0])){$RubyTottaxe[0]['RubyTottaxe']['total_taxable_sales']='0.00';}
                            if(!isset($RubyMemo3[0])){$RubyMemo3[0]['RubyMemo3']['total_merchandise_sales']='0.00';}
                            if(!isset($otherrevenuesales[0])){$otherrevenuesales[0]['RubyTottaxe']['total_taxes']='0.00';}
							if(!isset($RubyTotmop[0])){$RubyTotmop[0]['RubyTotmop']['value_of_sales']='0.00';}
							if(!isset($RubyTotmop[1])){$RubyTotmop[1]['RubyTotmop']['value_of_sales']='0.00';}
                            if(!isset($RubyTotmop[2])){$RubyTotmop[2]['RubyTotmop']['value_of_sales']='0.00';}
							if(!isset($RubyTotmop[3])){$RubyTotmop[3]['RubyTotmop']['value_of_sales']='0.00';}
                            ?>
                            
                               <?php
                               $grocery_tax=number_format((float)($RubyTottaxe[0]['RubyTottaxe']['total_taxable_sales']), 2, '.', '');	
                                echo $this->Form->input('Grocey_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t1','onkeyup'=>'change()','value'=>$grocery_tax)); ?>
                               </div>              
                          </div>          
                          <div class="badge bg-primary"><?php echo $grocery_tax;?></div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Non Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            
                              <?php 
                              $grocery_non_tax=number_format((float)($RubyMemo3[0]['RubyMemo3']['total_merchandise_sales']-$RubyTottaxe[0]['RubyTottaxe']['total_taxable_sales']), 2, '.', '');				  
                              
                              echo $this->Form->input('Grocey_Non',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t2','onkeyup'=>'change()','value'=>$grocery_non_tax)); ?>
                              
                            </div>
                          </div>
                          <div class="badge bg-primary"><?php echo $grocery_non_tax;?></div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Grocery</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php 
                             $total_grocery=number_format((float)($RubyMemo3[0]['RubyMemo3']['total_merchandise_sales']), 2, '.', '');
                             echo $this->Form->input('Total_Grocery',array('type'=>'text','class'=>'total-field','placeholder'=>'0','label'=>false, 'id'=>'t3','readonly'=>'readonly','value'=>$total_grocery)); ?>
                             
                            </div>
                          </div>
                        </div>
                      </div>     
                                   
                      <div class="inside-sales">
                        <h2>Sales Tax</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales-Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php
                             $sales_tax=number_format((float)($otherrevenuesales[0]['RubyTottaxe']['total_taxes']), 2, '.', '');
                              echo $this->Form->input('Sales_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false,'value'=>$sales_tax,'onkeyup'=>'change()',)); ?>
                              
                            </div>
                          </div>
                        </div>
                      </div>  
                                        
                      <div class="inside-sales">
                        <h2>Gas Sold (Gallons)</h2>
                      </div>
                      <div class="grocery-cont">
                      <?php foreach($_summaryTotal as $key => $sumtot){
						 
						  if($key!=="Total"){
						  
						   ?>
                      <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $key; ?> Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
							<?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$sumtot_galons=number_format((float)($sumtot['fuel_volume']), 2, '.', '');	
							}else{
							$sumtot_galons=0.00;		
							}
                            ?>                            
							<?php
                            echo $this->Form->input($key.'_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>$sumtot_galons));
                            ?>                 
                            </div>
                            
                            </div>
                            <div class="badge bg-primary">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $sumtot_galons;		
							}else{
							echo '0.00';	
							}
                            ?>
                            </div>
                            </div>
                      <?php }}  ?>
                     
                  <!--      <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
							<?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$regular_galons=number_format((float)($_summaryTotal['Regular']['fuel_volume']), 2, '.', '');	
							}else{
							$regular_galons=0.00;		
							}
                            ?>                            
							<?php
                            echo $this->Form->input('Regular_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>$regular_galons));
                            ?>                 
                            </div>
                            
                            </div>
                            <div class="badge bg-primary">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $regular_galons;		
							}else{
							echo '0.00';	
							}
                            ?>
                            </div>
                            </div>
                           <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$plus_galons=number_format((float)($_summaryTotal['Plus']['fuel_volume']), 2, '.', '');	
							}else{
							$plus_galons=0.00;		
							}
                            ?> 
                            <?php                                                  
                             echo $this->Form->input('Plus_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>$plus_galons)); ?>
                             
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
							<?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $plus_galons;		
							}else{
							echo '0.00';	
							}
                            ?>
                           </div>
                          </div>
                          <div class="grocery-group">
                          <div class="grocery-tax addon">Super Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$super_galons=number_format((float)($_summaryTotal['Premium']['fuel_volume']), 2, '.', '');	
							}else{
							$super_galons=0.00;		
							}
                            ?> 
                              <?php                            
                              echo $this->Form->input('Super_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>$super_galons)); ?>
                             
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
				          <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $super_galons;		
							}else{
							echo '0.00';	
							}
                            ?> 
                          </div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$diesel_galons=number_format((float)($_summaryTotal['Diesel']['fuel_volume']), 2, '.', '');	
							}else{
							$diesel_galons=0.00;		
							}
                            ?>
                             <?php                         
                             echo $this->Form->input('Diesel_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>$diesel_galons)); ?>
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
				           <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $diesel_galons;		
							}else{
							echo '0.00';	
							}
                            ?>
                          </div>
                        </div>-->
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">                          
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$total_gas_gallon=number_format((float)($_summaryTotal['Total']['fuel_volume']), 2, '.', '');	
							}else{
							$total_gas_gallon=0.00;		
							}
                            ?>                           
                            <?php echo $this->Form->input('Total_Gas_Gallon',array('type'=>'text','class'=>'hash-field totalgallon','placeholder'=>'0.00','label'=>false,'readonly'=>'readonly','value'=>$total_gas_gallon));
					        ?>                              
                             
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="inside-sales">
                        <h2>Gas Sold (Amount)</h2>
                      </div>
                      <div class="grocery-cont">
                       <?php foreach($_summaryTotal as $key => $sumtot){
						  if($key!=="Total"){ ?>
                     		 <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $key; ?> Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$sumtot_amount_sold=number_format((float)($sumtot['fuel_value']), 2, '.', '');	
							}else{
							$sumtot_amount_sold=0.00;		
							}
                            ?> 
                         <?php                       
                          echo $this->Form->input($key.'_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>$sumtot_amount_sold));
                          ?>
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
					        <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $sumtot_amount_sold;		
							}else{
							echo '0.00';	
							}
                            ?>
                        </div>
                        </div>
                       <?php }}  ?>
                      
                 <!--       <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$regular_amount_sold=number_format((float)($_summaryTotal['Regular']['fuel_value']), 2, '.', '');	
							}else{
							$regular_amount_sold=0.00;		
							}
                            ?> 
                         <?php                       
                          echo $this->Form->input('Regular_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>$regular_amount_sold));
                          ?>
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
					        <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							echo $regular_amount_sold;		
							}else{
							echo '0.00';	
							}
                            ?>
                        </div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$plus_amount_sold=number_format((float)($_summaryTotal['Plus']['fuel_value']), 2, '.', '');	
							}else{
							$plus_amount_sold=0.00;		
							}
                            ?>
                         <?php                         
                         echo $this->Form->input('Plus_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>$plus_amount_sold)); ?>
                          
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
				            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {						
							echo $plus_amount_sold;		
							}else{
							echo '0.00';	
							}
                            ?>       
                        </div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$super_amount_sold=number_format((float)($_summaryTotal['Premium']['fuel_value']), 2, '.', '');	
							}else{
							$super_amount_sold=0.00;		
							}
                            ?>
                           <?php                         
                            echo $this->Form->input('Super_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>$super_amount_sold)); ?>
                          
                            </div>
                            
                          </div>
                          <div class="badge bg-primary"> 
						  <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {						
							echo $super_amount_sold;		
							}else{
							echo '0.00';	
							}
                            ?>  
                            </div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$diesel_amount_sold=number_format((float)($_summaryTotal['Diesel']['fuel_value']), 2, '.', '');	
							}else{
							$diesel_amount_sold=0.00;		
							}
                            ?>
                          <?php                        
                           echo $this->Form->input('Diesel_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>$diesel_amount_sold)); ?>
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">
				            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {						
							echo $diesel_amount_sold;		
							}else{
							echo '0.00';	
							}
                            ?>
                            </div>
                        </div>-->
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php						
							if(!empty($rubyUprodts) && !empty($_summaryTotal)) {							
							$total_gas_amt_sold=number_format((float)($_summaryTotal['Total']['fuel_value']), 2, '.', '');	
							}else{
							$total_gas_amt_sold=0.00;		
							}
                            ?>
                            <?php echo $this->Form->input('Total_Gas_Amt_Sold',array('type'=>'text','class'=>'total-field TotalAmount','placeholder'=>'0.00','label'=>false,'readonly'=>'readonly','value'=>$total_gas_amt_sold)); ?>
                          
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="inside-sales">
                        <h2>Credit Card Batches</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by credit card:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                       <?php 
                       $sales_by_credit_card=number_format((float)($RubyTotmop[2]['RubyTotmop']['value_of_sales']), 2, '.', '');
                       echo $this->Form->input('Sales_by_credit_card',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false,'value'=>$sales_by_credit_card,'onkeyup'=>'outchange()')); ?>
                            
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                     
                    
                      <div class="inside-sales">
                        <h2>Lottery Tickets Sold</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off sales:</div>
                          <div class="grocery-tax-pos addon pos">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php 
						  $scratch_off_sales=number_format((float)($RubyTotmop[3]['RubyTotmop']['value_of_sales']), 2, '.', '');
						  echo $this->Form->input('Scratch_off_sales',array('type'=>'text','class'=>'amount-field','value'=>$scratch_off_sales,'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                      </div>                      
                      
                      <div class="inside-sales">
                        <h2>Lottery/Lotto Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online sales</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php 
						   $net_online_sales=0.00;						   
						   echo $this->Form->input('Net_online_sales',array('type'=>'text','class'=>'amount-field lottery','value'=>$net_online_sales,'label'=>false,'id'=>'netonline','onkeyup'=>'onlinesales(),change()')); ?>
                           
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php
							  $net_online_cashes=0.00;
							  echo $this->Form->input('Net_online_cashes',array('type'=>'text','class'=>'negetive-field lotto','value'=>$net_online_cashes,'label'=>false,'id'=>'onlinecash','onkeyup'=>'onlinesales(),outchange()')); ?>
                              
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                     <?php
					  $scratch_off_cashes=0.00;					 
					  echo $this->Form->input('Scratch_off_cashes',array('type'=>'text','class'=>'negetive-field lotto','value'=>$scratch_off_cashes,'label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Settlement:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Settlement',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','label'=>false)); ?>
                            
                             
                            </div>
                          </div>
                           <div class="badge bg-primary">0.00</div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Adjustment:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php
							  $adjustment=0.00;
							   echo $this->Form->input('Adjustment',array('type'=>'text','class'=>'negetive-field lotto','value'=>$adjustment,'label'=>false,'onkeyup'=>'outchange()')); ?>
                         
                            
                            </div>
                          </div>
                        
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Online credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php
							 $online_credit=0.00;
							 echo $this->Form->input('Online_credit',array('type'=>'text','class'=>'negetive-field lotto','value'=>$online_credit,'label'=>false,'onkeyup'=>'outchange()')); ?>                         
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php 
							$scratch_off_credit=0.00;
							echo $this->Form->input('OScratch_off_credit',array('type'=>'text','class'=>'negetive-field lotto','value'=>$scratch_off_credit,'label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Commission:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                 <?php echo $this->Form->input('Lotto_Commission',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false,'id'=>'commission')); ?>
                            
                            
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Balance</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Lotto_Balance',array('type'=>'text','class'=>'amount-field balance','placeholder'=>'0.00','label'=>false,'id'=>'balance','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                      </div>                    
                      
                      <div class="inside-sales">
                        <h2>Money Order Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. count</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('M_O_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. amount</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php							
							$mo_amount=0.00;
							echo $this->Form->input('M_O_amount',array('type'=>'text','class'=>'amount-field','value'=>$mo_amount,'label'=>false,'onkeyup'=>'change()')); ?>
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php
							 $mo_fees_collected=0.00;
							  echo $this->Form->input('M_O_fees_collected',array('type'=>'text','class'=>'amount-field','value'=>$mo_fees_collected,'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Bill Pay Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Bill_pay_count',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php
							  $bill_pay_amount=0.00;
							  echo $this->Form->input('Bill_pay_amount',array('type'=>'text','class'=>'amount-field','value'=> $bill_pay_amount,'label'=>false,'onkeyup'=>'change()')); ?>
                             
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                       <?php
					   $bill_pay_fees_collected=0.00;
					   echo $this->Form->input('Bill_pay_fees_collected',array('type'=>'text','class'=>'amount-field','value'=>$bill_pay_fees_collected,'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                        
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Money Transfer I Reading</h2>
                      </div>                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('M_T_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                            
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php
							  $mt_amount=0.00;
							   echo $this->Form->input('M_T_amount',array('type'=>'text','class'=>'amount-field','value'=>$mt_amount,'label'=>false,'onkeyup'=>'change()')); ?>
                         
                       
                            </div>
                          </div>
                          
                        </div>
                        
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Food Stamp Reading</h2>
                      </div>                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by EBT/Foods:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php 
						 $sales_by_ebt=0.00;						 
						 echo $this->Form->input('Sales_by_EBT/Foods',array('type'=>'text','class'=>'negetive-field','value'=>$sales_by_ebt,'label'=>false,'onkeyup'=>'outchange()')); ?>                         
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>TNRCC Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Regular_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                       
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Regular_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Plus_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Plus_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                                <?php echo $this->Form->input('Super_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                          
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Super_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Diesel_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Diesel_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Cash Card Sold</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"># of gas cards sold:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('No_of_gas_cards_sold',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">$ of gas cards sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                        <?php 
						$dolar_of_gas_cards_sold=0.00;
						echo $this->Form->input('Amount_of_gas_cards_sold',array('type'=>'text','class'=>'dollar-field','value'=>$dolar_of_gas_cards_sold,'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Gas card comm.:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Gas_card_comm',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                     
                  
                      
                    </div>       
                    
                    <div class="col-md-6" data-spy="scroll">
                      
                      
                       <div class="inside-sales">
                        <h2>Cash Purchases</h2>
                      </div>
                      <!--inside sales ends-->
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Purchases:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php 
							 $cash_purchases=0.00;
							 echo $this->Form->input('Cash_Purchases',array('type'=>'text','class'=>'negetive-field','value'=>$cash_purchases,'label'=>false,'readonly'=>'readonly')); ?>                              
                            </div>
                          </div>
                         
                          <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Pending Invoice:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php 
							$pending_invoice=0.00;
							echo $this->Form->input('Pending_Invoice',array('type'=>'text','class'=>'negetive-field','value'=>$pending_invoice,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Purchase Rebates:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php 
							$purchase_rebates=0.00;
							echo $this->Form->input('Purchase_Rebates',array('type'=>'text','class'=>'amount-field','value'=>$purchase_rebates,'label'=>false,'readonly'=>'readonly')); ?>
                            
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      
             <div class="inside-sales">
                        <h2>Other Money Paid</h2>
                      </div>
                      <!--inside sales ends-->
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Customer account</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $other_money_customer_account=0.00;
							 echo $this->Form->input('Customer_account_sale',array('type'=>'text','class'=>'negetive-field','value'=>$other_money_customer_account,'label'=>false,'readonly'=>'readonly')); ?>
                              
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Expenses:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $cash_expenses=0.00;
							 echo $this->Form->input('Cash_Expenses',array('type'=>'text','class'=>'negetive-field','value'=>$cash_expenses,'label'=>false,'readonly'=>'readonly')); ?>
                              
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Cash Withdrawn:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                              <?php 
							  $cash_withdrawn=0.00;
							  echo $this->Form->input('Cash_Withdrawn',array('type'=>'text','class'=>'negetive-field','value'=>$cash_withdrawn,'label'=>false,'readonly'=>'readonly')); ?>
                              
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                       
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Paid :</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $loan_paid=0.00;
							 echo $this->Form->input('Loan_Paid',array('type'=>'text','class'=>'negetive-field','value'=>$loan_paid,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      
                       <div class="inside-sales">
                        <h2>Other Receipts</h2>
                      </div>
                       <div class="grocery-group">
                          <div class="grocery-tax addon">Customer account </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $other_receipts_customer_account=0.00;
							  echo $this->Form->input('Customer_account_paid',array('type'=>'text','class'=>'amount-field','value'=>$other_receipts_customer_account,'label'=>false,'readonly'=>'readonly')); ?>
                           
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Other Income</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                              <?php 
							  $other_income=0.00;
							  echo $this->Form->input('other_income',array('type'=>'text','class'=>'amount-field','value'=>$other_income,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Received </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                               <?php 
							   $loan_received=0.00;
							   echo $this->Form->input('loan_received',array('type'=>'text','class'=>'amount-field','value'=>$loan_received,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                       <div class="inside-sales">
                        <h2>Cash Control</h2>
                      </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Opening Cash:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php
							$opening_cash=number_format((float)($RubyTotmop[0]['RubyTotmop']['value_of_sales']), 2, '.', '');
							 echo $this->Form->input('Opening_Cash',array('type'=>'text','class'=>'dollar-field','value'=>$opening_cash,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                          
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Opening checks:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $opening_checks=number_format((float)($RubyTotmop[1]['RubyTotmop']['value_of_sales']), 2, '.', '');
							    echo $this->Form->input('Opening_Checks',array('type'=>'text','class'=>'dollar-field','value'=>$opening_checks,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              
                            </div>
                          </div>
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Total Opening Amt:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php 
							 $total_opening_amount=$opening_cash+$opening_checks;
							 echo $this->Form->input('Total_Opening_Amt',array('type'=>'text','class'=>'amount-field','value'=>$total_opening_amount,'label'=>false,'readonly'=>'readonly')); ?>
                            
                            </div>
                          </div>
                          
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      <script type="text/javascript">
						$(document).ready(function(){ 
							$(".money_info_add").click(function(){ 
								var bank_name = $("#bank_name").val();
								var bn_name=$("#bank_name option:selected").text();
								//alert(bn_name);
								var withdrawal_type = $("#withdrawal_type").val();
								var check = $("#check").val();
								var amount = $("#amount").val();
								var memo = $("#memo").val();								
								if(!bank_name) {
								alert('Please select bank .');
								return false;
								}
								if(!withdrawal_type) {
								alert('Please select withdrawal type.');
								return false;
								}
								if(!check) {
								alert('Please enter check number.');
								return false;
								}
								
								if(!$.isNumeric(amount)) {
								alert('Please enter only numeric number in amount.');
								$("#amount").val('');
								return false;
								
								}	
								
								if(!amount) {
								alert('Please enter amount.');
								return false;
								}
														
								
								if(bank_name !="" && withdrawal_type !="" && check != "" ) {
								showFormValues(bank_name,bn_name,withdrawal_type, check, amount, memo);
								}
							});
						
						});
						var showFormValuesFuncCalls = 0;
						var money_total = 0;
						function showFormValues (bank_name,bn_name,withdrawal_type, check, amount, memo) {
							showFormValuesFuncCalls++;
							$(".before_add_div").hide();
							$(".after_add_div").show();
							$('#money_withdrawal tr:last').after('<tr id=tr_'+showFormValuesFuncCalls+'><td>'+bn_name+'</td><td>'+withdrawal_type+'</td><td>'+check+'</td><td id=money_withdrawal_amount_'+showFormValuesFuncCalls+'>'+amount+'</td><td>'+memo+'</td><td><a href=javascript:void(0);  id=td_'+showFormValuesFuncCalls+' onclick="delete_money_withdrawal('+showFormValuesFuncCalls+');"><i class="fa fa-trash"></i></a></td></tr>');
							
						document.getElementById("jsondiv1").innerHTML += "<br /><input type='hidden' id='bankname1"+showFormValuesFuncCalls+"'  value='"+bank_name+"' style='width:100px' /><input type='hidden' style='width:100px'  value='"+withdrawal_type+"' id='withdrawaltype"+showFormValuesFuncCalls+"' /><input type='hidden' style='width:100px'  value='"+check+"' id='check"+showFormValuesFuncCalls+"' /><input type='hidden' style='width:100px'  value='"+amount+"' id='amount"+showFormValuesFuncCalls+"' /><input type='hidden' style='width:100px'  value='"+memo+"' id='memo1"+showFormValuesFuncCalls+"' />";
							
							
							money_withdrawal_total(amount);
							
							$("#bank_name").val('');
							$("#withdrawal_type").val('');
							$("#check").val('');
							$("#amount").val('');
							$("#memo").val('');
							 $('#count_input1').val(showFormValuesFuncCalls);
							
						}
						
						function money_withdrawal_total(amount) {
							money_total = parseFloat(money_total)+parseFloat(amount); 
							$("#money_withdrawal_total").html("Total money withdrawn: $"+money_total);
						}
						
						function delete_money_withdrawal(row_id) {
							var delete_amount=$('#money_withdrawal_amount_'+row_id).html();	
							money_total = parseFloat(money_total)-parseFloat(delete_amount); 
							$("#money_withdrawal_total").html("Total money withdrawn: $"+money_total);						
							$('#tr_'+row_id).remove();
							
						   $( '#bankname1'+row_id ).remove();
						   $( '#withdrawaltype'+row_id ).remove();
						   $( '#check'+row_id ).remove();
						   $( '#amount'+row_id ).remove();
						   $( '#memo1'+row_id ).remove();
						   
						   $('#count_input1').val(row_id);
						   document.getElementById("count_inputdelete").innerHTML += "<br /><input type='text' id='deleter1"+row_id+"'  value='1' style='width:100px' />";
							
							
						}
						
						$("#money_withdrawn_close, #money_withdrawn_finish").click(function(){ 						
						$("#Money_from_Bank").val(money_total);
						});						
					
						
						</script> 
                        <script>
  
$(document).ready(function(){
 
    $("#money_withdrawn_close, #money_withdrawn_finish").click(function(){
         var myObj = [];
   var countinput = $("#count_input1").val()
      //alert(countinput);
   item = {};
   var i;
   for(i = 1; i <= countinput; i++) {
    var chkdelete = $('#deleter'+i).val();
  //  alert(chkdelete);
    if(chkdelete) { //alert('if')
	 } 
	else {
   //  alert('else'); 
     myObj.push({
     account_name: $('#bankname1'+i).val(),
     withdrawal_type: $('#withdrawaltype'+i).val(),
     withdrawal_check_no: $('#check'+i).val(),
     withdrawal_amount: $('#amount'+i).val(),
     withdrawal_memo: $('#memo1'+i).val()   
    });
    }
    }
    var json = JSON.stringify(myObj);
 //   alert(json);
    $('#input_hidden_field_obj1').val(json);
 	change();
	
    });
});
</script> 
                        
                      <div class="grocery-group">
                          <div class="grocery-tax addon">Money from Bank:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $money_from_bank=0.00;
							 echo $this->Form->input('Money_from_Bank',array('type'=>'text','class'=>'amount-field','value'=>$money_from_bank,'label'=>false,'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                          <a class="btn-default" data-toggle="modal" data-target="#myModalMoney"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModalMoney" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              
                                  <div class="modal-content">
                                    <div class="modal-header" style=" padding: 20px;">
                                     <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                      <h3 class="modal-title">Enter money withdrawn from bank</h3>
                                    </div>
                                    
                                    
                                        <div class="modal-body credit-card-cont">
                                            <div class="batch-add"><span>Bank account</span>
                                                <div class="form-group">
                                                <?php echo $this->Form->select('bank_name', $bank, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Bank', 'id'=>'bank_name')); ?>
                                                                                          
                                                </div>
                                            </div>
                                  
                                            <div class="batch-add"><span>Withdrawal type</span>
                                                <div class="form-group">
                                                    <select class="form-control credit-add1" name="withdrawal_type" id="withdrawal_type">
                                                        <option value="">Select Type</option>
                                                        <option value="Check">Check</option>
                                                        <option value="Voucher">Voucher</option>                                     
                                                    </select>                                      
                                                </div>
                                            </div>
                                            
                                            <div class="batch-amount"><span>Check #</span> <br>
                                                <input type="text" class="amount" name="check" id="check"/>
                                            </div>
                                            
                                            <div class="batch-amount"><span>Amount</span> <br>
                                                <input type="text" class="amount" name="amount" id="amount"/>
                                            </div>
                                            <div class="batch-amount"><span>MEMO</span> <br>
                                                <input type="text" class="amount" name="memo" id="memo"/>
                                            </div>
                                            
                                            <div class="batch-amount"> <br>
                                                <a class="btn-default money_info_add">Add to list</a>
                                            </div>
                                        
                                        </div>
                                    
                                    
                                    
                                    <div class="before_add_div">
                                    <h3 class="no-report">No entries made for this day report. <br>
                                      <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                    </div>
                                    
                                    <div class="after_add_div table-responsive" style="display:none;">
                                        <table class="table table-hover" id="money_withdrawal">
                                            <tr>
                                                <td>Bank Name</td>
                                                <td>Withdral Type</td>
                                                <td>Check</td>
                                                <td>Amount</td>
                                                <td>MEMO</td>
                                                <td>Action</td>
                                            </tr>
                                        </table>
                                        
                                        <div class="modal-body credit-card-cont" id="money_withdrawal_total"></div>
                                        
                                         <div class="" id="jsondiv1"></div>
                                     <input class="" type="hidden" id="count_input1">
                                    </div>  
                                      
                                      
                                    <div class="modal-footer">                                 
                                      <button type="button" id="money_withdrawn_finish" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                        <button type="button" id="money_withdrawn_close" class="btn-default" data-dismiss="modal" style="float: left;">Close</button>
                                    </div>
                                  </div>
                              
                            </div>
                          </div>
                        </div>
                         <input id="input_hidden_field_obj1" type="hidden" name="input_hidden_field_obj1" />
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Chk Cashing Comm.</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php 
							 $chk_cashing_comm=0.00;
							 echo $this->Form->input('Chk_Cashing_Comm',array('type'=>'text','class'=>'amount-field','value'=>$chk_cashing_comm,'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                         
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Put In ATM:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                               <?php
							   $put_in_atm=0.00;
							    echo $this->Form->input('Put_In_ATM',array('type'=>'text','class'=>'negetive-field','value'=>$put_in_atm,'label'=>false,'onkeyup'=>'outchange()')); ?>
                            
                            </div>
                          </div>
                          
                          
                        </div>
                        <script>
     var myObj = [];
   $(function() {
    
    $('#totalshowtable').hide();
   $("#deposit_total_amount").click(function(){
    var cash_amount=parseInt($('#deposit_cash_amount').val());
    var check_amount=parseInt($('#deposit_check_amount').val());
    var total=cash_amount+check_amount;
    $('#deposit_total_amount').val(total);
    });
	
     $("#deposit_cash_amount, #deposit_check_amount").keyup(function(){
        
          var cash_amount=parseInt($('#deposit_cash_amount').val());
    var check_amount=parseInt($('#deposit_check_amount').val());
    var total=cash_amount+check_amount;
    $('#deposit_total_amount').val(total);
	 });
    
     $('#add_deposit').click(function(){
	
	var cash_amount=parseInt($('#deposit_cash_amount').val());
	var check_amount=parseInt($('#deposit_check_amount').val());
	var total=cash_amount+check_amount;
	$('#deposit_total_amount').val(total);	 
		 
     var acc_name=$('#account_name').val();
	 var b_name=$("#account_name option:selected").text();
	// alert(b_name);
     var cash_amount=$('#deposit_cash_amount').val();
     var check_amount=$('#deposit_check_amount').val();
     var total_amount=$('#deposit_total_amount').val();
     var deposit_memo=$('#deposit_memo').val();
     
     if(!acc_name) {
          alert('Please enter bank name.');
      return false;
        }
      if(!cash_amount) {
          alert('Please enter cash amount.');
      return false;
        }
      if(!check_amount) {
          alert('Please enter check amount.');
      return false;
        }
	 if(!$.isNumeric(cash_amount)) {
		alert('Please enter only numeric number in amount.');
		$("#deposit_cash_amount").val("");
		//return false;
		
		}
		
	if(!$.isNumeric(check_amount)) {
		alert('Please enter only numeric number in amount.');
		$("#deposit_check_amount").val("");
		return false;
		}		
      if(!total_amount) {
          alert('Please enter total amount.');
      return false;
        }
     
     if(acc_name !="" && cash_amount !="" && check_amount != ""  && total_amount != "") {
      showFormValues1(acc_name,b_name,cash_amount, check_amount, total_amount, deposit_memo);
     }
    });
   });
      var showFormValuesFuncCalls1 = 0;
      var money_total1 = 0;
      function showFormValues1(acc_name,b_name,cash_amount, check_amount, total_amount, deposit_memo) {
      
      showFormValuesFuncCalls1++; 
       
       $("#noshow").hide();
       $("#totalshowtable").show();
       
       $('#money_deposit tr:last').after("<tr id=tr1_"+showFormValuesFuncCalls1+"><td>"+b_name+"</td><td>"+cash_amount+"</td><td>"+check_amount+"</td><td id=money_deposit_amount"+showFormValuesFuncCalls1+">"+total_amount+"</td><td>"+deposit_memo+"</td><td><a href=javascript:void(0); id=td_"+showFormValuesFuncCalls1+" onclick='delete_money_deposit("+showFormValuesFuncCalls1+");'><i class='fa fa-trash'></i><a></td></tr>");
       
       
     
        document.getElementById("jsondiv").innerHTML += "<br /><input type='hidden' id='bankname"+showFormValuesFuncCalls1+"'  value='"+acc_name+"' style='width:100px' /><input type='hidden' style='width:100px'  value='"+cash_amount+"' id='cashamount"+showFormValuesFuncCalls1+"' /><input type='hidden' style='width:100px'  value='"+check_amount+"' id='checkamount"+showFormValuesFuncCalls1+"' /><input type='hidden' style='width:100px'  value='"+total_amount+"' id='totalamount"+showFormValuesFuncCalls1+"' /><input type='hidden' style='width:100px'  value='"+deposit_memo+"' id='memo"+showFormValuesFuncCalls1+"' />";
        
        
      // alert(showFormValuesFuncCalls1); 
       
       money_deposit_total(total_amount);
       
       $('#account_name').val('');
       $('#deposit_cash_amount').val('');
       $('#deposit_check_amount').val('');
       $('#deposit_total_amount').val('');
       $('#deposit_memo').val('');
       
       $('#count_input').val(showFormValuesFuncCalls1);
       
      }
      
      
      function money_deposit_total(total_amount) {
       money_total1 = parseFloat(money_total1)+parseFloat(total_amount); 
       $("#money_deposit_total").html("Total money deposited: $"+money_total1);
      }
      
      function delete_money_deposit(row_id) {
       
       var delete_amount1=$('#money_deposit_amount'+row_id).html();
       money_total1 = parseFloat(money_total1)-parseFloat(delete_amount1); 
       
       $("#money_deposit_total").html("Total money deposited: $"+money_total1);       
       $('#tr1_'+row_id).remove(); 
       $( '#bankname'+row_id ).remove();
       $( '#cashamount'+row_id ).remove();
       $( '#checkamount'+row_id ).remove();
       $( '#totalamount'+row_id ).remove();
       $( '#memo'+row_id ).remove();
       $('#count_input').val(showFormValuesFuncCalls1);
       document.getElementById("count_inputdelete").innerHTML += "<br /><input type='text' id='deleter"+row_id+"'  value='1' style='width:100px' />";
       
      
      }    
      
	  $("#money_deposit_close, #money_deposit_finish").click(function(){       
      $("#Total_deposits").val(money_total1);
      });
      
            </script> 
                    <script>
  
$(document).ready(function(){
 
    $("#money_deposit_close, #money_deposit_finish").click(function(){
         var myObj = [];
   var countinput = $("#count_input").val()
      //alert(countinput);
   item = {};
   var i;
   for(i = 1; i <= countinput; i++) {
    var chkdelete = $('#deleter'+i).val();
   // alert(chkdelete);
    if(chkdelete) { //alert('if') 
	} else {
    // alert('else'); 
     myObj.push({
     account_name: $('#bankname'+i).val(),
     deposit_cash_amount: $('#cashamount'+i).val(),
     deposit_check_amount: $('#checkamount'+i).val(),
     deposit_total_amount: $('#totalamount'+i).val(),
     deposit_memo: $('#memo'+i).val()   
    });
    }
    }
    var json = JSON.stringify(myObj);
    //alert(json);
    $('#input_hidden_field_obj').val(json);
  outchange();
    });
});
</script>
                        
             <!--       <script>
					var myObj = [];
			$(function() {
				
				$('#totalshowtable').hide();
			$("#deposit_total_amount").click(function(){
				var cash_amount=parseInt($('#deposit_cash_amount').val());
				var check_amount=parseInt($('#deposit_check_amount').val());
				var total=cash_amount+check_amount;
				$('#deposit_total_amount').val(total);
				});
				
				
            $('#add_deposit').click(function(){
					var acc_name=$('#account_name').val();
					var cash_amount=$('#deposit_cash_amount').val();
					var check_amount=$('#deposit_check_amount').val();
					var total_amount=$('#deposit_total_amount').val();
					var deposit_memo=$('#deposit_memo').val();
					
					if(!acc_name) {
     					alert('Please enter bank name.');
						return false;
    				}
						if(!cash_amount) {
     					alert('Please enter cash amount.');
						return false;
    				}
						if(!check_amount) {
     					alert('Please enter check amount.');
						return false;
    				}
						if(!total_amount) {
     					alert('Please enter total amount.');
						return false;
    				}
					
					if(acc_name !="" && cash_amount !="" && check_amount != ""  && total_amount != "") {
						showFormValues1(acc_name, cash_amount, check_amount, total_amount, deposit_memo);
					}
				});
			});
						var showFormValuesFuncCalls1 = 0;
						var money_total1 = 0;
						function showFormValues1(acc_name, cash_amount, check_amount, total_amount, deposit_memo) {
						
						showFormValuesFuncCalls1++;	
							
							$("#noshow").hide();
							$("#totalshowtable").show();
							
							$('#money_deposit tr:last').after("<tr id=tr_"+showFormValuesFuncCalls1+"><td>"+acc_name+"</td><td>"+cash_amount+"</td><td>"+check_amount+"</td><td id=money_deposit_amount"+showFormValuesFuncCalls1+">"+total_amount+"</td><td>"+deposit_memo+"</td><td><a href=javascript:void(0); id=td_"+showFormValuesFuncCalls1+" onclick='delete_money_deposit("+showFormValuesFuncCalls1+");'>delete<a></td></tr>");
							
							
					
								document.getElementById("jsondiv").innerHTML += "<br /><input type='text' id='bankname"+showFormValuesFuncCalls1+"'  value='"+acc_name+"' style='width:100px' /><input type='text' style='width:100px'  value='"+cash_amount+"' id='cashamount"+showFormValuesFuncCalls1+"' /><input type='text' style='width:100px'  value='"+check_amount+"' id='checkamount"+showFormValuesFuncCalls1+"' /><input type='text' style='width:100px'  value='"+total_amount+"' id='totalamount"+showFormValuesFuncCalls1+"' /><input type='text' style='width:100px'  value='"+deposit_memo+"' id='memo"+showFormValuesFuncCalls1+"' />";
								
								
							alert(showFormValuesFuncCalls1);	
							
							money_deposit_total(total_amount);
							
							$('#account_name').val('');
							$('#deposit_cash_amount').val('');
							$('#deposit_check_amount').val('');
							$('#deposit_total_amount').val('');
							$('#deposit_memo').val('');
							
							$('#count_input').val(showFormValuesFuncCalls1);
							
						}
						
						
						function money_deposit_total(total_amount) {
							money_total1 = parseFloat(money_total1)+parseFloat(total_amount); 
							$("#money_deposit_total").html("Total money deposited: $"+money_total1);
						}
						
						function delete_money_deposit(row_id) {
							
							var delete_amount1=$('#money_deposit_amount'+row_id).html();
							money_total1 = parseFloat(money_total1)-parseFloat(delete_amount1); 
							
							$("#money_deposit_total").html("Total money deposited: $"+money_total1);       
							$('#tr_'+row_id).remove(); 
						/*	$('#bankname'+row_id).val('');
							$('#cashamount'+row_id).val('');
							$('#checkamount'+row_id).val('');
							$('#totalamount'+row_id).val('');
							$('#memo'+row_id).val('');*/
							$( '#bankname'+row_id ).remove();
							$( '#cashamount'+row_id ).remove();
							$( '#checkamount'+row_id ).remove();
							$( '#totalamount'+row_id ).remove();
							$( '#memo'+row_id ).remove();
						
						}
						
						
						
            </script> 
                    <script>
		
$(document).ready(function(){
	
    $("#json").click(function(){
         var myObj = [];
		 var countinput = $("#count_input").val()
	     //alert(countinput);
		 item = {};
		 var i;
		 for(i = 1; i <= countinput; i++) {
			var bankname = $('#bankname'+i).val();
			  //alert(bankname);
			 
				var cashamount = $('#cashamount'+i).val();
				var checkamount = $('#checkamount'+i).val();
				var totalamount = $('#totalamount'+i).val();
				var memo = $('#memo'+i).val();		 
       item["account_name"] = bankname;
       item["deposit_cash_amount"] = cashamount;
       item["deposit_check_amount"] = checkamount;
       item["deposit_total_amount"] = totalamount;
       item["deposit_memo"] = memo;
       
       myObj.push(item);
        
       
       var json = JSON.stringify(myObj);
       alert(json);
	   $('#input_hidden_field_obj').val(json);
	   var value_obj = $('#input_hidden_field_obj').val();
	   value_obj = JSON.parse(value_obj);

console.log(value_obj);

		 }
    });
});
</script>  --> 
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total deposits:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php 
							 $total_deposits=0.00;
							 echo $this->Form->input('Total_deposits',array('type'=>'text','class'=>'negetive-field','value'=>$total_deposits,'label'=>false,'readonly'=>'readonly')); ?>
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#tolaldeposit"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="tolaldeposit" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                             <!--     <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                  <h3 class="modal-title">Enter money deposited to bank</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Bank Name<br />&nbsp;</span>
                                    <div class="form-group">
                                       <?php echo $this->Form->select('account_name', $bank, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Bank', 'id'=>'account_name')); ?>
                                      <!--<select name="account_name" id="account_name" class="form-control credit-add1">
                                        <option value="P N C Bank">P N C Bank</option>
                                        <option value="Store day report">Store day report</option>
                                        <option value="T D Bank">T D Bank</option>
                                      </select>-->
                                      
                                    </div>
                                  </div>
                                  <div class="batch-amount"><span>Deposit Cash Amount</span> <br>
                                    <input type="text" name="deposit_cash_amount" id="deposit_cash_amount" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Deposit Check Amount</span> <br>
                                    <input type="text" class="amount" name="deposit_check_amount" id="deposit_check_amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Deposit Total Amount</span> <br>
                                    <input type="text" class="amount" id="deposit_total_amount" name="deposit_total_amount" readonly="readonly"/>
                                  </div>
                                  <div class="batch-amount"><span>Deposit Memo <br />&nbsp;</span> <br>
                                    <input type="text" class="amount" id="deposit_memo" name="deposit_memo"/>
                                  </div>
                                  <div class="batch-amount"><span style="display: block;">&nbsp; <br>&nbsp; </span>
                                  <a href="javascript:void(0);" class="btn btn-default" id="add_deposit">Add to list</a>
                                 <!--   <button id="add_deposit" class="btn-default">Add Batch</button>
                                  <button type="button" id="json" class="btn btn-default">Json</button>-->
                                  </div>
                                </div>
                                <h3 class="no-report" id="noshow">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                  
                                  <div id="totalshowtable" class="EnetUI-PadTop-Large">
                                  <table class="table table-hover" id="money_deposit">
                                  <tr>
                                  <th >Bank account</th>
                                  <th>Cash amount</th>
                                  <th>Check amount</th>
                                  <th>Total deposit</th>
                                  <th>Memo</th>
                                  <th></th>
                                  </tr>
                                 
                                  <tbody>
                                  
                                  </tbody>
                                  </table>
                                    <div class="modal-body credit-card-cont" id="money_deposit_total"></div>
                                    
                                     <div class="" id="jsondiv"></div>
                                     <input class="" type="hidden" id="count_input">
                                  </div>
                                  
                                  
                                <div class="modal-footer">
                                 
                                  <button type="button" id="money_deposit_finish" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                    <button type="button" id="money_deposit_close" class="btn-default" data-dismiss="modal" style="float: left;">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <input id="input_hidden_field_obj" type="hidden" name="input_hidden_field_obj" />
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Cash:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php 
							 $closing_cash=0.00;
							 echo $this->Form->input('Closing_Cash',array('type'=>'text','class'=>'negetive-field','value'=>$closing_cash,'label'=>false,'onkeyup'=>'ccashamount(),outchange()','id'=>'ccash')); ?>
                             
                            </div>
                          </div>
                          
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Checks:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php
							$closing_checks=0.00;
							echo $this->Form->input('Closing_Checks',array('type'=>'text','class'=>'negetive-field','value'=>$closing_checks,'label'=>false,'onkeyup'=>'ccheckamount(),outchange()','id'=>'ccheck'));
							 ?>                          
                            </div>
                          </div>                          
                     
                          
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Closing Amt:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php
							 $total_closing_amount=0.00;
							 echo $this->Form->input('Total_Closing_Amt',array('type'=>'text','class'=>'dollar-field','value'=>$total_closing_amount,'label'=>false,'id'=>'totalcamt','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                
              </div>
              
               <div class="enter-total">
                  <div class="col-md-2 z-reading hideleb"><span >ZReading</span> <br>
                    <?php echo $this->Form->input('z_reading',array('type'=>'text','class'=>'negetive-field lotto amount','value'=>$max_z_reading+1,'readonly'=>'readonly'));
					//'onkeypress'=>'javascript:return isNumber(event)'
					?>
                   <!-- <div class="positive"><span id=""><?php //echo $max_z_reading+1; ?></span></div>-->
                  </div>                  
                  <?php 
				  $total_in=$total_grocery+$total_gas_amt_sold+$sales_tax+$net_online_sales+$scratch_off_sales+$dolar_of_gas_cards_sold+$other_receipts_customer_account+$other_income+$loan_received+$purchase_rebates+$chk_cashing_comm+$money_from_bank+$total_opening_amount+$mo_amount+$mo_fees_collected+$bill_pay_amount+$bill_pay_fees_collected+$mt_amount;
				  
				  
				  ?>                  
                  <?php $total_out=$total_deposits+$put_in_atm+$cash_purchases+$pending_invoice+$sales_by_credit_card+$cash_expenses+$other_money_customer_account+$net_online_cashes+$scratch_off_cashes+$adjustment+$online_credit+$scratch_off_credit+$loan_paid+$cash_withdrawn+$total_closing_amount+$sales_by_ebt;
				  ?>
                  <div class="col-md-2 z-reading "><span>Total in</span> <br>
                   <!-- <input type="hidden" id="totalinval" value="" name="data[RubyDailyreporting][Total_In]" />-->                    <?php echo $this->Form->input('Total_In',array('type'=>'hidden','label'=>false,'value'=>$total_in)); ?> 
                    <div class="positive">$<span id="totalin"><?php echo $total_in;?></span></div>
                  </div>	
                  
                   <div class="col-md-2 z-reading"><span>Total Out</span> <br>
                   <!--<input type="hidden" id="totaloutval" value="" name="data[RubyDailyreporting][Total_Out]" />-->
                   <?php echo $this->Form->input('Total_Out',array('type'=>'hidden','label'=>false,'value'=>$total_out)); ?> 
                    <div class="negetive">$<span id="totalout"><?php echo $total_out;?></span></div>
                  </div>
                  <div class="col-md-2 z-reading"><span>Short Over</span> <br>
                   <!-- <input type="hidden" id="shorttotal" value="" name="data[RubyDailyreporting][Short_Over]" />-->
                    <?php 
					if($total_in<$total_out){
					$short_overs=$total_out-$total_in;				
					$sign='+';	
					}else if ($total_out<$total_in){					
					$short_overs=$total_in-$total_out;	
					$sign='-';	
					} else {
					$short_overs=$total_out-$total_in;	
					$sign='';	
					}					
					$short_over=number_format((float)($short_overs), 2, '.', '');	
					echo $this->Form->input('Short_Over',array('type'=>'hidden','label'=>false,'value'=>$short_over)); ?> 
                    <div class="short-over"><span id="short_over_sign"><?php echo $sign;?></span>$<span id="overtotal"><?php echo $short_over;?></span></div>
                  </div>
                  <div class="col-md-2 z-reading"><span></span> <br>
                    <div class="attach"> <i class="fa fa-paperclip"></i>
                    </div>
                    <div class="attach"> 
                    <i class="fa fa-pencil-square-o"></i>
            </div>
            </div>
            
             <div class="col-md-1 save-reportcont"> 
             <input type="submit" id="form_submit" value="Save Report" name="Save Report" class="btn-primary save-report">
            <!-- <button type="button" class="btn-primary save-report">Save Report</button>-->
                  </div>
            </div>
            <style>
            .hideleb label{ display:none;}
			.hideleb .amount{ width:100%; text-align:left; padding-left:5px;}
            </style>
            <style>
      .hideleb label{ display:none;}
      .hideleb .amount{ width:100%; text-align:left; padding-left:5px;}
      .batch-add { float: left; width: 162px; font-size: 12px;}
      .amount { width: 100%;  border: 1px solid #ccc;  height: 32px;  margin: 2px 0 0 0;  border-radius: 5px;}      </style>
