<div  class="row">
    <div class="col-xs-9">
        <h3 class="page-title">Transactions from today</h3>

    </div>
    <div class="col-xs-2">
       <!-- <input name="report" id="load_date" class="form-control hasDatepicker" size="16" value="2017-02-05" type="text">-->
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
    jQuery(document).ready(function(){      
		 jQuery(".lveps").click(function() {
			var lveps_id = $(this).attr('id');
			//alert(lveps_id);
				jQuery.ajax({
				url: '<?php echo Router::url('/') ?>admin/ruby2_livepos/getresult',
                type: 'post',
				 dataType: 'text',
                data: {lveps_id: lveps_id},
                success:function(data){				
				//alert(data);
				jQuery('#ajxdv').html(data);
				
                }
            });
        });	
		
		
		jQuery("#transInterval").change(function() {
			var srch_val = $(this).val();
			    //alert(srch_val);
				jQuery.ajax({
				url: '<?php echo Router::url('/') ?>admin/ruby2_livepos/getsrchresult',
                type: 'post',
				 dataType: 'text',
                data: {srch_val:srch_val},
                success:function(data){				
				//alert(data);
				jQuery('#srchdv ul').html(data);
				jQuery('#ajxdv').html('');
                }
            });
        });	
       
    });   
       
</script>
<div class="row">

    <div class="col-md-5" >
        <div class="portlet box blue" style="border:1px solid #cfd0d0">
            <div class="portlet-title col-md-12">
                <div class="caption"><i class="fa fa-cogs"></i>Transaction Summary</div>
            </div>

            <div class="portlet-body">
                <div id="realtime_storelist">
                    <div class="row">
                        <div class="col-md-12">
                            <?php /*?><div class="col-md-4">
<!--                                <select id="ddlTransStore" class="form-control livestream">
                                    <option value="-1">All Store</option>
                                    <option value="5020">Passyunk-Gulf</option>
                                </select>-->
                                <?php echo $this->Form->input('store_id',array('options' => $stores,'class'=>'form-control livestream','label'=>false));?>
                            </div>
                            <div class="col-md-4">
                                <select id="registerType" class="form-control livestream">
<!--                                    <option value="-1">All</option>-->
                                    <option value="1">Primary</option>
                                    <option value="0">Secondary</option>
                                </select>
                            </div><?php */?>
                            
                            <div class="col-md-4">
                                <select id="transInterval" class="form-control livestream">
                                    <option value="">All</option>
                                    <option value="50">Last 50</option>
                                    <option value="1">All Day</option>
                                    <option value="300">Last 5 Minutes</option>
                                    <option value="3600">Last Hour</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <style>
                        .tran-sum-span
                        {
                            color:#7aa73a;font-size:14px;
                        }
                        .tran-sum-dollar
                        {
                            font-size:18px;color:#4f87b1
                        }
                    </style>
                    <div class="row" >
                        <div class="col-md-12"  id="srchdv">
                            <ul id="listData" style="padding:5px;height: 300px; overflow-y: scroll;overflow-x:hidden" >
                                
                                <?php 								
								//echo '<pre>';print_r($ruby2Livepos);die;
								foreach($ruby2Livepos as $ruby2Livepo): ?>
                                <a href="javascript:void(0);" id="<?php echo $ruby2Livepo['Ruby2Livepo']['id'];?>" class="lveps">
                                    <li style="border-bottom: 1px solid #e1e1e1;padding:3px ">
                                        <div class="row">
                                            <div class="col-sm-6">
                 <?php /*?><span class="tran-sum-span" ><?php echo $ruby2Livepo['Store']['name'] ?></span><br><?php */?>
                                             <small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span><?php echo h($ruby2Livepo['Ruby2Livepo']['ReceiptDate'])." ".h($ruby2Livepo['Ruby2Livepo']['ReceiptTime']); ?></span></small>
                                            </div>
                                            <div class="col-sm-4 tran-sum-dollar" >
                                                <?php echo h($ruby2Livepo['Ruby2Livepo']['TransactionID']); ?>
                                            </div>
                                            <div class="col-sm-2">
                                               <i class="fa fa-chevron-right ">&nbsp;</i>
                                            </div>
                                        </div>
                                    </li>
                                </a>                                
                                <?php endforeach; ?>                                 
                                </ul>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7" style="">
        <div class="portlet box blue" style="border:1px solid #cfd0d0;">
            <div class="portlet-title col-md-12">
                <div class="caption"><i class="fa fa-cogs"></i>Transaction Detail</div>
            </div>

            <div class="portlet-body" style=" min-height:385px;" id="ajxdv">
            
            </div>
        </div>
    </div>


</div>
<!--<script>
$(document).ready(function(){
   
	$('#store_id').change(function(){
		 var store_id = $(this).val();
		 var store_name = $("#store_id option:selected").text();
		 var regtype = $("#registerType option:selected").val();
		 var transInterval = $("#transInterval option:selected").val();
		 
		 if(regtype != 0)
		 {
			 changeData(store_id,store_name,regtype,transInterval);
		 }
		 
	});
	$('#registerType').change(function(){
		 var store_id = $("#store_id option:selected").val();
		 var store_name = $("#store_id option:selected").text();
		 var regtype = $(this).val();
		 var transInterval = $("#transInterval option:selected").val();
		 //alert(regtype);
		 if(regtype == 0)
		 {
			 $('#listData').empty();
		 }
		 else
		 {
			 changeData(store_id,store_name,regtype,transInterval);
		 }
	});
	$('#transInterval').change(function(){
		 var store_id = $("#store_id option:selected").val();
		 var store_name = $("#store_id option:selected").text();
		 var regtype = $("#registerType option:selected").val();
		 var transInterval = $(this).val();
		 
		 if(regtype != 0)
		 {
			 changeData(store_id,store_name,regtype,transInterval);
		 }
	});
	
	
	function changeData(store_id,store_name,regtype,transInterval)
	{
		$('#listData').empty();
		// alert(transInterval);
		 
		 var parms = "store_id="+store_id+"&regtype="+regtype+"&transInterval="+transInterval;	
		 
		 
		 $.ajax({
			 type:'POST',
			 url:'ruby2_livepos/fetch_data',
			 dataType:'json',
			 data:parms,
			 success:function(data){
				 //alert(data);
				 //alert('st -'+data);
				 //alert(JSON.stringify(data));
				 //console.log(JSON.stringify(data));
				 $.each(data,function(key,val){
					 console.log(val.a.name);
					 console.log(val.b.amount);
					  //$.each(val,function(key,val){
						  
						  //alert(key);
						  //console.log(key);
						  
						  
						var listData = '<a href="#">'+
								'<li  style="border-bottom: 1px solid #e1e1e1;padding:3px ">'+
									'<div class="row">'+
										'<div class="col-sm-6">'+
											'<span class="tran-sum-span" style="color:#7aa73a;font-size:12px;" >'+val.a.name+'</span>'+
											'<br>'+
											'<small class="EnetUI-Font-De-Emphasize" style="font-size: 0.8em;"><span>'+val.b.periodBeginDate+'</span></small>'+
										'</div>'+
										'<div class="col-sm-4 tran-sum-dollar" style="font-size:18px;color:#4f87b1" >$ '+
											val.b.amount
										+'</div>'+
										'<div class="col-sm-2">'+
										   '<i class="fa fa-chevron-right ">&nbsp;</i>'+
										'</div>'+
									'</div>'+
								'</li>'+
							'</a>';
						  //alert(listdata);
						 $('#listData').append(listData); 					  
					  //});
				 });
			 }
		 });
	}
});
</script>-->                  