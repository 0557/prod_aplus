<div class="ruby2Livepos form">
<?php echo $this->Form->create('Ruby2Livepo'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ruby2 Livepo'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('store_id');
		echo $this->Form->input('periodBeginDate');
		echo $this->Form->input('upc');
		echo $this->Form->input('name');
		echo $this->Form->input('salePrice');
		echo $this->Form->input('originalPrice');
		echo $this->Form->input('count');
		echo $this->Form->input('amount');
		echo $this->Form->input('itemCount');
		echo $this->Form->input('percentOfSales');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Ruby2Livepo.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Ruby2Livepo.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby2 Livepos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
