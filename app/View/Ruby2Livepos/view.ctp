<div class="ruby2Livepos view">
<h2><?php echo __('Ruby2 Livepo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ruby2Livepo['Store']['name'], array('controller' => 'stores', 'action' => 'view', $ruby2Livepo['Store']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PeriodBeginDate'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['periodBeginDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Upc'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['upc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SalePrice'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['salePrice']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('OriginalPrice'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['originalPrice']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Count'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['count']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ItemCount'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['itemCount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PercentOfSales'); ?></dt>
		<dd>
			<?php echo h($ruby2Livepo['Ruby2Livepo']['percentOfSales']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby2 Livepo'), array('action' => 'edit', $ruby2Livepo['Ruby2Livepo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby2 Livepo'), array('action' => 'delete', $ruby2Livepo['Ruby2Livepo']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $ruby2Livepo['Ruby2Livepo']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby2 Livepos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby2 Livepo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
