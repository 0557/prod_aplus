<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style type="text/css">
.form-control {
    height: 33px; !important; 
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>/css/new-form.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
function change() { 
    var t1Val = parseFloat(document.getElementById("t1").value);
    var t2Val = parseFloat(document.getElementById("t2").value);
	var t3Val=t1Val+t2Val;
	(document.getElementById("t3")).value =(t3Val);	
	var Sales_Tax=parseFloat(document.getElementById("Sales_Tax").value);
	var Total_Gas_Amt_Sold=parseFloat(document.getElementById("Total_Gas_Amt_Sold").value);
	var netonline=parseFloat(document.getElementById("netonline").value);
	var Scratch_off_sales=parseFloat(document.getElementById("Scratch_off_sales").value);
	var Amount_of_gas_cards_sold=parseFloat(document.getElementById("Amount_of_gas_cards_sold").value);
	var M_O_amount=parseFloat(document.getElementById("M_O_amount").value);
	var M_O_fees_collected=parseFloat(document.getElementById("M_O_fees_collected").value);
	var Bill_pay_amount=parseFloat(document.getElementById("Bill_pay_amount").value);
	var Bill_pay_fees_collected=parseFloat(document.getElementById("Bill_pay_fees_collected").value);
	var M_T_amount=parseFloat(document.getElementById("M_T_amount").value);	
	var Customer_account_paid=parseFloat(document.getElementById("Customer_account_paid").value);
	var other_income=parseFloat(document.getElementById("other_income").value);
	var loan_received=parseFloat(document.getElementById("loan_received").value);	
	var Opening_Cash=parseFloat(document.getElementById("Opening_Cash").value);	
	var Opening_Checks=parseFloat(document.getElementById("Opening_Checks").value);
	var Purchase_Rebates=parseFloat(document.getElementById("Purchase_Rebates").value);
	var Chk_Cashing_Comm=parseFloat(document.getElementById("Chk_Cashing_Comm").value);
	var Money_from_Bank=parseFloat(document.getElementById("Money_from_Bank").value);
	var Total_Opening_Amt=parseFloat(document.getElementById("Total_Opening_Amt").value);	
	
	
	var Put_In_ATM=parseFloat(document.getElementById("Put_In_ATM").value);
	var Total_deposits=parseFloat(document.getElementById("Total_deposits").value);
	var ccash=parseFloat(document.getElementById("ccash").value);
	var ccheck=parseFloat(document.getElementById("ccheck").value);	
	var totalcamt=(Opening_Cash+Opening_Checks+Total_Opening_Amt+Money_from_Bank+Chk_Cashing_Comm)-(Put_In_ATM+Total_deposits+ccash+ccheck);
	(document.getElementById("totalcamt")).value =(totalcamt);	
	
	var Total_In=t3Val+Sales_Tax+Total_Gas_Amt_Sold+netonline+Scratch_off_sales+Amount_of_gas_cards_sold+M_O_amount+M_O_fees_collected+Bill_pay_amount+Bill_pay_fees_collected+M_T_amount+Customer_account_paid+other_income+loan_received+Opening_Cash+Opening_Checks+Purchase_Rebates+Chk_Cashing_Comm+Money_from_Bank+Total_Opening_Amt;
	
	(document.getElementById("Total_In")).value =(Total_In);
	$('#totalin').html(Total_In.toFixed(2));
	
	var Total_Out=parseFloat(document.getElementById("Total_Out").value);	
	var Short_Over;	
	var Short_Over_Sign;
	if(Total_In>Total_Out){
		Short_Over=Total_In-Total_Out;		
		$('#short_over_sign').html('-');
		Short_Over_Sign='-';
	} else if(Total_Out>Total_In){
		Short_Over=Total_Out-Total_In;
		$('#short_over_sign').html('+');
		Short_Over_Sign='+';
	}else{
		Short_Over=Total_Out-Total_In;
		$('#short_over_sign').html('');
		Short_Over_Sign='';
	}
	//var Short_Over=Total_In-Total_Out;
	//alert(Short_Over);
	(document.getElementById("Short_Over")).value =(Short_Over_Sign+Short_Over);
	$('#overtotal').html(Short_Over.toFixed(2));
         
};

function outchange() {    
	var Total_deposits=parseFloat(document.getElementById("Total_deposits").value);
	var Put_In_ATM=parseFloat(document.getElementById("Put_In_ATM").value);	
	var paid_out=parseFloat(document.getElementById("paid_out").value);	
	var Cash_Purchases=parseFloat(document.getElementById("Cash_Purchases").value);	
	var Pending_Invoice=parseFloat(document.getElementById("Pending_Invoice").value);	
	var Sales_by_credit_card=parseFloat(document.getElementById("Sales_by_credit_card").value);
	var Cash_Expenses=parseFloat(document.getElementById("Cash_Expenses").value);
	var Customer_account_sale=parseFloat(document.getElementById("Customer_account_sale").value);	
	var onlinecash=parseFloat(document.getElementById("onlinecash").value);	
	var Scratch_off_cashes=parseFloat(document.getElementById("Scratch_off_cashes").value);	
	var Adjustment=parseFloat(document.getElementById("Adjustment").value);	
	var Online_credit=parseFloat(document.getElementById("Online_credit").value);	
	var OScratch_off_credit=parseFloat(document.getElementById("OScratch_off_credit").value);
	var Loan_Paid=parseFloat(document.getElementById("Loan_Paid").value);
	var Cash_Withdrawn=parseFloat(document.getElementById("Cash_Withdrawn").value);	
	var Sales_by_EBT=parseFloat(document.getElementById("Sales_by_EBT/Foods").value);
	var ccash = parseFloat(document.getElementById("ccash").value);
    var ccheck = parseFloat(document.getElementById("ccheck").value);
	var coupon_amount=parseFloat(document.getElementById("coupon_amount").value);
	
	var Opening_Cash=parseFloat(document.getElementById("Opening_Cash").value);
	var Opening_Checks=parseFloat(document.getElementById("Opening_Checks").value);
	var Total_Opening_Amt=parseFloat(document.getElementById("Total_Opening_Amt").value);
	var Money_from_Bank=parseFloat(document.getElementById("Money_from_Bank").value);
	var Chk_Cashing_Comm=parseFloat(document.getElementById("Chk_Cashing_Comm").value);	
	var totalcamt=(Opening_Cash+Opening_Checks+Total_Opening_Amt+Money_from_Bank+Chk_Cashing_Comm)-(Put_In_ATM+Total_deposits+ccash+ccheck);
	(document.getElementById("totalcamt")).value =(totalcamt);		
	
		
	var Total_Out=Total_deposits+Put_In_ATM+ccash+ccheck+paid_out+Cash_Purchases+Pending_Invoice+Sales_by_credit_card+Cash_Expenses+Customer_account_sale+onlinecash+Scratch_off_cashes+Adjustment+Online_credit+OScratch_off_credit+Loan_Paid+Cash_Withdrawn+Sales_by_EBT+coupon_amount;
	(document.getElementById("Total_Out")).value =(Total_Out);
	$('#totalout').html(Total_Out.toFixed(2));
	
	var Total_In=parseFloat(document.getElementById("Total_In").value);
	var Short_Over;	
	var Short_Over_Sign;
	if(Total_In>Total_Out){
		Short_Over =Total_In-Total_Out;		
		$('#short_over_sign').html('-');
		Short_Over_Sign='-';
	} else if(Total_Out>Total_In){
		Short_Over =Total_Out-Total_In;
		$('#short_over_sign').html('+');
		Short_Over_Sign='+';
	}else{
		Short_Over =Total_Out-Total_In;
		$('#short_over_sign').html('');
		Short_Over_Sign='';
	}
	//var Short_Over=Total_In-Total_Out;
	//alert(Short_Over);	
	(document.getElementById("Short_Over")).value =(Short_Over_Sign+Short_Over);
	$('#overtotal').html(Short_Over.toFixed(2));
         
};
/*function gallon_change()
{
var Total_gallon=parseFloat(0);
$( ".Gallons" ).each(function() { 
Total_gallon=parseFloat(Total_gallon)+parseFloat($(this).val());
});	
document.getElementById("Total_Gas_Gallon").value =(Total_gallon);
};

function amountsold_change()
{
var Total_amountsold=parseFloat(0);
$( ".Amount" ).each(function() { 
Total_amountsold=parseFloat(Total_amountsold)+parseFloat($(this).val());
});	
document.getElementById("Total_Gas_Amt_Sold").value =(Total_amountsold.toFixed(2));
change();
};*/

function commission_cal()
{
var net_online_sales = $('#netonline').val();	
var settlement = $('#Settlement').val();
//alert(net_online_sales);
//alert(settlement);
var commission=((parseFloat(net_online_sales)+parseFloat(settlement))*5)/100;
//alert(commission);
document.getElementById("commission").value =(commission);
};
function balance_cal()
{
var net_online_sales = $('#netonline').val();	
var settlement = $('#Settlement').val();
//alert(net_online_sales);
//alert(settlement);
var onlinecash = $('#onlinecash').val();	
var Scratch_off_cashes = $('#Scratch_off_cashes').val();
var Adjustment = $('#Adjustment').val();	
var Online_credit = $('#Online_credit').val();
var OScratch_off_credit = $('#OScratch_off_credit').val();	
var commission = $('#commission').val();
var balance=((parseFloat(net_online_sales)+parseFloat(settlement))-(parseFloat(onlinecash)+parseFloat(Scratch_off_cashes)+parseFloat(Adjustment)+parseFloat(Online_credit)+parseFloat(OScratch_off_credit)+parseFloat(commission)));
//alert(commission);
document.getElementById("balance").value =(balance);
};
function file_check() {					
	var filename = $('#attachment_file').val();				
	var filename1 = filename.split('.');
	var filename2 = filename1.pop();				
	var ext = filename2.toLowerCase();								
	if($.inArray(ext, ['pdf','xls','docs','jpg','JPG','jpeg','JPEG','png','PNG']) == -1) {
		alert('Invalid file format!. Please select pdf,xls,docs,jpg,JPG,jpeg,JPEG,png or PNG.');
		$('#attachment_file').val('');
	}			
};
 
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#cal" ).datepicker({ dateFormat: 'mm-dd-yy' });	
  } );
  </script>
<script>
    $(document).ready(function(){ 
        //iterate through each textboxes and add keyup
        //handler to trigger sum event
		//$('#cal').Zebra_DatePicker({format:'m-d-Y'});
	$("#cal").attr("readonly", false); 
	
		 $("#getvalue").click(function() {
			var adate = $('#cal').val();
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/ruby_dailyreportings/getvalues',
                type: 'post',
				 dataType: 'text',
                data: { adate: adate},
                success:function(data){				
				//alert(data);
				$('#getvaluep').html(data);
				/*	getplu();*/
                }
            });
        });	
       
    });   
       
</script>


</head>

<body>
 <?php echo $this->Form->create('RubyDailyreporting',array('type' => 'file','action'=>'add_reporting'))?>

<div class="panel panel-default">

               <div class="enter-daily"> 
               <div class="col-md-8 col-sm-12">
               <!--<span><i class="fa fa-calendar"></i> </span>Enter daily report for <?php  echo date('jS F Y')?> &nbsp;&nbsp;&nbsp;&nbsp;  -->          
             </div>
                  <div class="col-md-2 col-sm-12">
                  <input type="text" class="form-control" name="cal" id='cal'/>
                  </div>
                  <div class="col-md-2 col-sm-12">
                 <input type="button" class="btn btn-success" value="Get Data" id='getvalue'/>
              
              </div>
              </div>
              
               <div class="row"><br/></div>  
               
               <div id="getvaluep">  
              
               <div class="panel-body form-container">
             
              
                <div class="row">
                
                  <div class="col-md-12 col-sm-12">
            
                    <div class="col-md-6" data-spy="scroll">
                     
                    
                      <div class="inside-sales">
                        <h2>Inside Sales</h2>
                      </div>
                    
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                           <div class="form-cont">                
                            <div class="form-control">
                               <?php echo $this->Form->input('Grocey_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t1','onkeyup'=>'change()')); ?>
                               </div>              
                          </div>          
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Non Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Grocey_Non',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t2','onkeyup'=>'change1()')); ?>
                              
                            </div>
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Grocery</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Total_Grocery',array('type'=>'text','class'=>'total-field','placeholder'=>'0','label'=>false, 'id'=>'t3','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                        </div>
                      </div>
                   
                      
                      <div class="inside-sales">
                        <h2>Net Tax</h2>
                      </div>
                   
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Sales_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Sold (Gallons)</h2>
                      </div>
                  <?php  //pr($prodList); ?>
                      <div class="grocery-cont">
                      <?php foreach($prodList as $key => $pro){ ?>
                      
                        <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $pro; ?> Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php echo $this->Form->input($pro.'_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <?php }?>
              <!--          <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Plus_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Super_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Diesel_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>-->
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                      <?php echo $this->Form->input('Total_Gas_Gallon',array('type'=>'text','class'=>'hash-field totalgallon','placeholder'=>'0.00','label'=>false,'readonly'=>'readonly')); ?>
                              
                             
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Sold (Amount)</h2>
                      </div>
                
                      <div class="grocery-cont">
                         <?php foreach($prodList as $key => $pro){ ?>
                        <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $pro; ?> Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php echo $this->Form->input($pro.'_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                         <?php }?>
           <!--             <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php echo $this->Form->input('Plus_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false)); ?>
                          
                            
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php echo $this->Form->input('Super_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false)); ?>
                          
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php echo $this->Form->input('Diesel_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false)); ?>
                          
                           
                            </div>
                            
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>-->
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php echo $this->Form->input('Total_Gas_Amt_Sold',array('type'=>'text','class'=>'total-field TotalAmount','placeholder'=>'0.00','label'=>false,'readonly'=>'readonly')); ?>
                          
                            </div>
                          </div>
                        </div>
                      </div>
                      
                
                      <div class="inside-sales">
                        <h2>Credit Card Batches</h2>
                      </div>
                  
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by credit card:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                       <?php echo $this->Form->input('Sales_by_credit_card',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
          
                          
                        </div>
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Lottery Tickets Sold</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off sales:</div>
                          <div class="grocery-tax-pos addon pos">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php echo $this->Form->input('Scratch_off_sales',array('type'=>'text','class'=>'amount-field','placeholder'=>'$0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          <div class="badge bg-primary">0.00</div>
                        </div>
                      </div>
                      
                      
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Lottery/Lotto Reading</h2>
                      </div>
                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online sales</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php echo $this->Form->input('Net_online_sales',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','label'=>false,'id'=>'netonline','onkeyup'=>'onlinesales()')); ?>
                           
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Net_online_cashes',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false,'id'=>'onlinecash','onkeyup'=>'onlinesales()')); ?>
                              
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                     <?php echo $this->Form->input('Scratch_off_cashes',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Settlement:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Settlement',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','label'=>false)); ?>
                            
                             
                            </div>
                          </div>
                           <div class="badge bg-primary">0.00</div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Adjustment:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Adjustment',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false)); ?>
                         
                            
                            </div>
                          </div>
                        
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Online credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Online_credit',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false)); ?>
                         
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('OScratch_off_credit',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Commission:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                 <?php echo $this->Form->input('Lotto_Commission',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false,'id'=>'commission')); ?>
                            
                            
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Balance</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Lotto_Balance',array('type'=>'text','class'=>'amount-field balance','placeholder'=>'0.00','label'=>false,'id'=>'balance','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Money Order Reading</h2>
                      </div>
                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. count</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('M_O_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. amount</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('M_O_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('M_O_fees_collected',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                             
                            </div>
                          </div>
                          
                        </div>
                        
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Bill Pay Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Bill_pay_count',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Bill_pay_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                       <?php echo $this->Form->input('Bill_pay_fees_collected',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                        
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Money Transfer I Reading</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('M_T_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                            
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('M_T_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                       
                            </div>
                          </div>
                          
                        </div>
                        
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Food Stamp Reading</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by EBT/Foods:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php echo $this->Form->input('Sales_by_EBT/Foods',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                   
                      
                      
                      <div class="inside-sales">
                        <h2>TNRCC Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Regular_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                       
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Regular_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Plus_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Plus_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                                <?php echo $this->Form->input('Super_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                          
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Super_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                         
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Diesel_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Diesel_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Cash Card Sold</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"># of gas cards sold:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('No_of_gas_cards_sold',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">$ of gas cards sold:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                        <?php echo $this->Form->input('Amount_of_gas_cards_sold',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Gas card comm.:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Gas_card_comm',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                     
                   
                      <div class="inside-sales">
                        <h2>Coupon</h2>
                      </div>                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Coupon Count</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('coupon_count',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','id'=>'coupon_count','label'=>false)); ?>                            
                            </div>
                          </div>                         
                        </div>                      
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Coupon Amount</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('coupon_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'coupon_amount','label'=>false)); ?>
                              
                            </div>
                          </div>
                        </div>
                        
                      </div>   
                   
                   
            
                      
                    </div>       
                    
                    <div class="col-md-6" data-spy="scroll">                     
                      
                      
                      	<?php
                        if($this->Session->read('pos_type')=='ruby2'){
                        ?>
                        <div class="inside-sales">
                            <h2>Safe Drop</h2>
                          </div>                     
                        <div class="grocery-group">
                              <div class="grocery-tax addon">Safe Drop:</div>
                              <div class="grocery-tax-pos addon credit">-$</div>
                              <div class="form-cont">
                                <div class="form-control">
                         <?php 
                         echo $this->Form->input('paid_out',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','label'=>false,'onkeyup'=>'outchange()')); 
						 ?>                             
                            </div>
                              </div>
                              
                            </div>  
                        <?php
                        }else{
						echo $this->Form->input('paid_out',array('type'=>'hidden','value'=>''));	
						}
                        ?>
                      
                      
                      
                       <div class="inside-sales">
                        <h2>Cash Purchases</h2>
                      </div>
                      <!--inside sales ends-->
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Purchases:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Cash_Purchases',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>        
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Pending Invoice:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Pending_Invoice',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>           
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Purchase Rebates:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Purchase_Rebates',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>        
                          
                        </div>
                        
                      </div>
                      
             <div class="inside-sales">
                        <h2>Other Money Paid</h2>
                      </div>
                      <!--inside sales ends-->
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">House Accounts:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Customer_account_sale',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>         
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Expenses:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Cash_Expenses',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>      
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Cash Withdrawn:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Cash_Withdrawn',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                              
                            </div>
                          </div>         
                          
                        </div>
                        
                       
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Paid :</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Loan_Paid',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>       
                          
                        </div>
                        
                      </div>
                      
                       <div class="inside-sales">
                        <h2>Other Receipts</h2>
                      </div>
                       <div class="grocery-group">
                          <div class="grocery-tax addon">Customer account </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Customer_account_paid',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                           
                            </div>
                          </div>        
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Other Income</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('other_income',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>        
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Received </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('loan_received',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>        
                          
                        </div>
                        
                       <div class="inside-sales">
                        <h2>Cash Control</h2>
                      </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Opening Cash:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Opening_Cash',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Opening checks:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('Opening_Checks',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                         
                          
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Total Opening Amt:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Total_Opening_Amt',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                        </div>                       
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Money from Bank:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Money_from_Bank',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
             <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i></a>-->
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Chk Cashing Comm.</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Chk_Cashing_Comm',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                         
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Put In ATM:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('Put_In_ATM',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total deposits:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Total_deposits',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false)); ?>
                             
                            </div>
                          </div>
                         <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Cash:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Closing_Cash',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false,'onkeyup'=>'ccashamount()','id'=>'ccash')); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Checks:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Closing_Checks',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','label'=>false,'onkeyup'=>'ccheckamount()','id'=>'ccheck')); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Closing Amt:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Total_Closing_Amt',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','label'=>false,'id'=>'totalcamt','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                    </div>
                  </div>
                </div>
                
              </div>
              
               
            
               </div>    
    
</div>


<div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>

<?php echo $this->Form->end(); ?>  
