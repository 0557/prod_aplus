<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style type="text/css">
.form-control {
    height: 33px; !important; 
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>/css/new-form.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
function change() { 
    //alert('ok');
    var t1Val = parseFloat(document.getElementById("t1").value);
    var t2Val = parseFloat(document.getElementById("t2").value);
	var t3Val=t1Val+t2Val;
	(document.getElementById("t3")).value =(t3Val);	
	var Sales_Tax=parseFloat(document.getElementById("Sales_Tax").value);
	var Total_Gas_Amt_Sold=parseFloat(document.getElementById("Total_Gas_Amt_Sold").value);
	var netonline=parseFloat(document.getElementById("netonline").value);
	var Scratch_off_sales=parseFloat(document.getElementById("Scratch_off_sales").value);
	var Amount_of_gas_cards_sold=parseFloat(document.getElementById("Amount_of_gas_cards_sold").value);
	var M_O_amount=parseFloat(document.getElementById("M_O_amount").value);
	var M_O_fees_collected=parseFloat(document.getElementById("M_O_fees_collected").value);
	var Bill_pay_amount=parseFloat(document.getElementById("Bill_pay_amount").value);
	var Bill_pay_fees_collected=parseFloat(document.getElementById("Bill_pay_fees_collected").value);
	var M_T_amount=parseFloat(document.getElementById("M_T_amount").value);	
	var Customer_account_paid=parseFloat(document.getElementById("Customer_account_paid").value);
	var other_income=parseFloat(document.getElementById("other_income").value);
	var loan_received=parseFloat(document.getElementById("loan_received").value);	
	var Opening_Cash=parseFloat(document.getElementById("Opening_Cash").value);	
	var Opening_Checks=parseFloat(document.getElementById("Opening_Checks").value);
	var Purchase_Rebates=parseFloat(document.getElementById("Purchase_Rebates").value);
	var Chk_Cashing_Comm=parseFloat(document.getElementById("Chk_Cashing_Comm").value);
	var Money_from_Bank=parseFloat(document.getElementById("Money_from_Bank").value);
	var Total_Opening_Amt=parseFloat(document.getElementById("Total_Opening_Amt").value);	
	
	
	var Put_In_ATM=parseFloat(document.getElementById("Put_In_ATM").value);
	var Total_deposits=parseFloat(document.getElementById("Total_deposits").value);
	var ccash=parseFloat(document.getElementById("ccash").value);
	var ccheck=parseFloat(document.getElementById("ccheck").value);	
	var totalcamt=(Opening_Cash+Opening_Checks+Total_Opening_Amt+Money_from_Bank+Chk_Cashing_Comm)-(Put_In_ATM+Total_deposits+ccash+ccheck);
	(document.getElementById("totalcamt")).value =(totalcamt);	
	
	var Total_In=t3Val+Sales_Tax+Total_Gas_Amt_Sold+netonline+Scratch_off_sales+Amount_of_gas_cards_sold+M_O_amount+M_O_fees_collected+Bill_pay_amount+Bill_pay_fees_collected+M_T_amount+Customer_account_paid+other_income+loan_received+Opening_Cash+Opening_Checks+Purchase_Rebates+Chk_Cashing_Comm+Money_from_Bank+Total_Opening_Amt;
	
	(document.getElementById("Total_In")).value =(Total_In);
	$('#totalin').html(Total_In.toFixed(2));
	
	var Total_Out=parseFloat(document.getElementById("Total_Out").value);	
	var Short_Over;
	var Short_Over_Sign;	
	if(Total_In>Total_Out){
		Short_Over=Total_In-Total_Out;		
		$('#short_over_sign').html('-');
		Short_Over_Sign='-';
	} else if(Total_Out>Total_In){
		Short_Over=Total_Out-Total_In;
		$('#short_over_sign').html('+');
		Short_Over_Sign='+';
	}else{
		Short_Over=Total_Out-Total_In;
		$('#short_over_sign').html('');
		Short_Over_Sign='';
	}
	//var Short_Over=Total_In-Total_Out;
	//alert(Short_Over);
	(document.getElementById("Short_Over")).value =(Short_Over_Sign+Short_Over);
	$('#overtotal').html(Short_Over.toFixed(2));
         
};

function outchange() {    
	var Total_deposits=parseFloat(document.getElementById("Total_deposits").value);
	var Put_In_ATM=parseFloat(document.getElementById("Put_In_ATM").value);	
	var paid_out=parseFloat(document.getElementById("paid_out").value);	
	var Cash_Purchases=parseFloat(document.getElementById("Cash_Purchases").value);	
	var Pending_Invoice=parseFloat(document.getElementById("Pending_Invoice").value);	
	var Sales_by_credit_card=parseFloat(document.getElementById("Sales_by_credit_card").value);
	var Cash_Expenses=parseFloat(document.getElementById("Cash_Expenses").value);
	var Customer_account_sale=parseFloat(document.getElementById("Customer_account_sale").value);	
	var onlinecash=parseFloat(document.getElementById("onlinecash").value);	
	var Scratch_off_cashes=parseFloat(document.getElementById("Scratch_off_cashes").value);	
	var Adjustment=parseFloat(document.getElementById("Adjustment").value);	
	var Online_credit=parseFloat(document.getElementById("Online_credit").value);	
	var OScratch_off_credit=parseFloat(document.getElementById("OScratch_off_credit").value);
	var Loan_Paid=parseFloat(document.getElementById("Loan_Paid").value);
	var Cash_Withdrawn=parseFloat(document.getElementById("Cash_Withdrawn").value);	
	var Sales_by_EBT=parseFloat(document.getElementById("Sales_by_EBT/Foods").value);
	var ccash = parseFloat(document.getElementById("ccash").value);
    var ccheck = parseFloat(document.getElementById("ccheck").value);
	var coupon_amount=parseFloat(document.getElementById("coupon_amount").value);
	
	var Opening_Cash=parseFloat(document.getElementById("Opening_Cash").value);
	var Opening_Checks=parseFloat(document.getElementById("Opening_Checks").value);
	var Total_Opening_Amt=parseFloat(document.getElementById("Total_Opening_Amt").value);
	var Money_from_Bank=parseFloat(document.getElementById("Money_from_Bank").value);
	var Chk_Cashing_Comm=parseFloat(document.getElementById("Chk_Cashing_Comm").value);	
	var totalcamt=(Opening_Cash+Opening_Checks+Total_Opening_Amt+Money_from_Bank+Chk_Cashing_Comm)-(Put_In_ATM+Total_deposits+ccash+ccheck);
	(document.getElementById("totalcamt")).value =(totalcamt);		
	
		
	var Total_Out=Total_deposits+Put_In_ATM+ccash+ccheck+paid_out+Cash_Purchases+Pending_Invoice+Sales_by_credit_card+Cash_Expenses+Customer_account_sale+onlinecash+Scratch_off_cashes+Adjustment+Online_credit+OScratch_off_credit+Loan_Paid+Cash_Withdrawn+Sales_by_EBT+coupon_amount;
	(document.getElementById("Total_Out")).value =(Total_Out);
	$('#totalout').html(Total_Out.toFixed(2));
	
	var Total_In=parseFloat(document.getElementById("Total_In").value);
	var Short_Over;
	var Short_Over_Sign;	
	if(Total_In>Total_Out){
		Short_Over =Total_In-Total_Out;		
		$('#short_over_sign').html('-');
		Short_Over_Sign='-';
	} else if(Total_Out>Total_In){
		Short_Over =Total_Out-Total_In;
		$('#short_over_sign').html('+');
		Short_Over_Sign='+';
	}else{
		Short_Over =Total_Out-Total_In;
		$('#short_over_sign').html('');
		Short_Over_Sign='';
	}
	//var Short_Over=Total_In-Total_Out;
	//alert(Short_Over);	
	(document.getElementById("Short_Over")).value =(Short_Over_Sign+Short_Over);
	$('#overtotal').html(Short_Over.toFixed(2));
         
};
/*function gallon_change()
{
var Total_gallon=parseFloat(0);
$( ".Gallons" ).each(function() { 
Total_gallon=parseFloat(Total_gallon)+parseFloat($(this).val());
});	
document.getElementById("Total_Gas_Gallon").value =(Total_gallon);
};

function amountsold_change()
{
var Total_amountsold=parseFloat(0);
$( ".Amount" ).each(function() { 
Total_amountsold=parseFloat(Total_amountsold)+parseFloat($(this).val());
});	
document.getElementById("Total_Gas_Amt_Sold").value =(Total_amountsold.toFixed(2));
change();
};*/
function commission_cal()
{
var net_online_sales = $('#netonline').val();	
var settlement = $('#Settlement').val();
//alert(net_online_sales);
//alert(settlement);
var commission=((parseFloat(net_online_sales)+parseFloat(settlement))*5)/100;
//alert(commission);
document.getElementById("commission").value =(commission);
};
function balance_cal()
{
var net_online_sales = $('#netonline').val();	
var settlement = $('#Settlement').val();
//alert(net_online_sales);
//alert(settlement);
var onlinecash = $('#onlinecash').val();	
var Scratch_off_cashes = $('#Scratch_off_cashes').val();
var Adjustment = $('#Adjustment').val();	
var Online_credit = $('#Online_credit').val();
var OScratch_off_credit = $('#OScratch_off_credit').val();	
var commission = $('#commission').val();
var balance=((parseFloat(net_online_sales)+parseFloat(settlement))-(parseFloat(onlinecash)+parseFloat(Scratch_off_cashes)+parseFloat(Adjustment)+parseFloat(Online_credit)+parseFloat(OScratch_off_credit)+parseFloat(commission)));
//alert(commission);
document.getElementById("balance").value =(balance);
};

function note_retrieve(){ 
	var note_val=$('#modal_note').val();							
	$("#note").val(note_val);
};
 function file_check() {					
	var filename = $('#attachment_file').val();				
	var filename1 = filename.split('.');
	var filename2 = filename1.pop();				
	var ext = filename2.toLowerCase();								
	if($.inArray(ext, ['pdf','xls','docs','jpg','JPG','jpeg','JPEG','png','PNG']) == -1) {
		alert('Invalid file format!. Please select pdf,xls,docs,jpg,JPG,jpeg,JPEG,png or PNG.');
		$('#attachment_file').val('');
	}			
};
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
   // $( "#cal" ).datepicker({ dateFormat: 'mm-dd-yy' });	
  } );
  </script>
<script>
    $(document).ready(function(){      
	$("#cal").attr("readonly", false); 	
		 $("#getvalue").click(function() {			
			var adate = $('#cal').val();
			var z_reading = $('#z_reading').val();
				$.ajax({
				url: '<?php echo Router::url('/') ?>admin/ruby_dailyreportings/editgetvalues',
                type: 'post',
				 dataType: 'text',
                data: { adate: adate, z_reading:z_reading},
                success:function(data){			
				$('#getvaluep').html(data);
                }
            });
        });
      
    });  
    
</script>


</head>

<body>
 <?php echo $this->Form->create('RubyDailyreporting',array('type' => 'file','action'=>'edit_reporting/'.$rubydailyreporting['id']))?>
<div class="panel panel-default">
               <div class="enter-daily"> 
               <div class="col-md-8 col-sm-12">
               <span><i class="fa fa-calendar"></i> </span>Daily report for <?php  echo date('jS F Y',strtotime($rubydailyreporting['reporting_date']));?> &nbsp;&nbsp;&nbsp;&nbsp;            
             </div>
                  <div class="col-md-2 col-sm-12">
                  <input type="text" class="form-control" name="cal" id='cal' value="<?php  echo date('m-d-Y',strtotime($rubydailyreporting['reporting_date']));?>" disabled/>
                  <input type="hidden"  name="cals" id='cals' value="<?php  echo date('m-d-Y',strtotime($rubydailyreporting['reporting_date']));?>" />
                  </div>
                 <?php /*?> <div class="col-md-2 col-sm-12">
                 <input type="button" class="btn btn-success" value="Get Data" id='getvalue'/>
                 </div><?php */?>
              </div>
              
               <div class="row"><br/></div>  
               
               <div id="getvaluep">  
              
               
              <div class="panel-body form-container">
             
              
                <div class="row">
                
                  <div class="col-md-12 col-sm-12">
            
                    <div class="col-md-6" data-spy="scroll">
                     
                    <?php //echo '<pre>';print_r($rubydailyreporting);die;?>
                      <div class="inside-sales">
                        <h2>Inside Sales</h2>
                      </div>
                    
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                           <div class="form-cont">                
                            <div class="form-control">
                               <?php echo $this->Form->input('Grocey_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t1','value'=>number_format((float)($rubydailyreporting['Grocey_Tax']), 2, '.', ''),'onkeyup'=>'change()')); ?>
                               </div>              
                          </div>          
     <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting['Grocey_Tax']), 2, '.', '');?></div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Grocey-Non Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Grocey_Non',array('type'=>'text','class'=>'amount-field','placeholder'=>'0','label'=>false, 'id'=>'t2','value'=>number_format((float)($rubydailyreporting['Grocey_Non']), 2, '.', ''),'onkeyup'=>'change()')); ?>                              
                            </div>
                          </div>
                          <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting['Grocey_Non']), 2, '.', '');?></div>
                        </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Grocery</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Total_Grocery',array('type'=>'text','class'=>'total-field','placeholder'=>'0','label'=>false, 'id'=>'t3','value'=>number_format((float)($rubydailyreporting['Total_Grocery']), 2, '.', ''),'readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                        </div>
                      </div>
                   
                      
                      <div class="inside-sales">
                        <h2>Net Tax</h2>
                      </div>
                   
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net Tax</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Sales_Tax',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Sales_Tax','value'=>number_format((float)($rubydailyreporting['Sales_Tax']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                              
                            </div>
                          </div>
                        </div>
                      </div>                     
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Sold (Gallons)</h2>
                      </div>
                
                      <div class="grocery-cont">
                  <?php /*?>    <?php foreach($prodList as $key => $pro){ ?>
                      
                        <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $pro; ?> Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php 						
						  echo $this->Form->input($pro.'_Gallon',array('type'=>'text','class'=>'hash-field Gallons','placeholder'=>'0.00','label'=>false,'value'=>number_format((float)($rubydailyreporting[$pro.'_Gallon']), 2, '.', ''),'onkeyup'=>'gallon_change()')); ?>
                            </div>
                            
                          </div>
                          <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting[$pro.'_Gallon']), 2, '.', ''); ?></div>
                        </div>
                        <?php }?>   <?php */?>                     
             
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Gallons:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                      <?php echo $this->Form->input('Total_Gas_Gallon',array('type'=>'text','class'=>'hash-field totalgallon','placeholder'=>'0.00','id'=>'Total_Gas_Gallon','value'=>number_format((float)($rubydailyreporting['Total_Gas_Gallon']), 2, '.', ''),'label'=>false,'readonly'=>'readonly')); ?>
                              
                             
                            </div>
                          </div>
                        </div>
                      </div>                      
                      
                      <div class="inside-sales">
                        <h2>Gas Sold (Amount)</h2>
                      </div>
                
                      <div class="grocery-cont">
                         <?php /*?><?php foreach($prodList as $key => $pro){ ?>
                        <div class="grocery-group">
                          <div class="grocery-tax addon"><?php echo $pro; ?> Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php echo $this->Form->input($pro.'_Amt_Sold',array('type'=>'text','class'=>'amount-field Amount','placeholder'=>'0.00','label'=>false,'value'=>number_format((float)($rubydailyreporting[$pro.'_Amt_Sold']), 2, '.', ''),'onkeyup'=>'amountsold_change()')); ?>
                            
                            </div>
                            
                          </div>
                          <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting[$pro.'_Amt_Sold']), 2, '.', ''); ?></div>
                        </div>
                         <?php }?><?php */?>
          
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Gas Amt Sold:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                            <?php echo $this->Form->input('Total_Gas_Amt_Sold',array('type'=>'text','class'=>'total-field TotalAmount','placeholder'=>'0.00','id'=>'Total_Gas_Amt_Sold','value'=>number_format((float)($rubydailyreporting['Total_Gas_Amt_Sold']), 2, '.', ''),'label'=>false,'readonly'=>'readonly')); ?>
                          
                            </div>
                          </div>
                        </div>
                      </div>
                      
                
                      <div class="inside-sales">
                        <h2>Credit Card Batches</h2>
                      </div>
                  
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by credit card:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                       <?php echo $this->Form->input('Sales_by_credit_card',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','id'=>'Sales_by_credit_card','value'=>number_format((float)($rubydailyreporting['Sales_by_credit_card']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); ?>
                            
                            </div>
                          </div>
                          <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>
                          
                        </div>
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Lottery Tickets Sold</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off sales:</div>
                          <div class="grocery-tax-pos addon pos">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                          <?php echo $this->Form->input('Scratch_off_sales',array('type'=>'text','class'=>'amount-field','placeholder'=>'$0.00','id'=>'Scratch_off_sales','value'=>number_format((float)($rubydailyreporting['Scratch_off_sales']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                          <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting['Scratch_off_sales']), 2, '.', '');?></div>
                        </div>
                      </div>
                      
                      
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Lottery/Lotto Reading</h2>
                      </div>
                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online sales</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                           <?php echo $this->Form->input('Net_online_sales',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Net_online_sales']), 2, '.', ''),'label'=>false,'id'=>'netonline','onkeyup'=>'change(),commission_cal(),balance_cal()')); ?>
                           
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Net online cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Net_online_cashes',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Net_online_cashes']), 2, '.', ''),'label'=>false,'id'=>'onlinecash','onkeyup'=>'outchange(),balance_cal()')); ?>
                              
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off cashes:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                     <?php echo $this->Form->input('Scratch_off_cashes',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','id'=>'Scratch_off_cashes','value'=>number_format((float)($rubydailyreporting['Scratch_off_cashes']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange(),balance_cal()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Settlement:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Settlement',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','id'=>'Settlement','value'=>number_format((float)($rubydailyreporting['Settlement']), 2, '.', ''),'label'=>false,'onkeyup'=>'commission_cal(),balance_cal()')); ?>
                            
                             
                            </div>
                          </div>
                           <div class="badge bg-primary"><?php echo number_format((float)($rubydailyreporting['Settlement']), 2, '.', '');?></div>
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Adjustment:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Adjustment',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','id'=>'Adjustment','value'=>number_format((float)($rubydailyreporting['Adjustment']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange(),balance_cal()')); ?>
                         
                            
                            </div>
                          </div>
                        
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Online credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Online_credit',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','id'=>'Online_credit','value'=>number_format((float)($rubydailyreporting['Online_credit']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange(),balance_cal()')); ?>
                         
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Scratch-off credit:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('OScratch_off_credit',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','id'=>'OScratch_off_credit','value'=>number_format((float)($rubydailyreporting['OScratch_off_credit']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange(),balance_cal()')); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Commission:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                 <?php echo $this->Form->input('Lotto_Commission',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'commission','value'=>number_format((float)($rubydailyreporting['Lotto_Commission']), 2, '.', ''),'label'=>false,'id'=>'commission','onkeyup'=>'balance_cal()')); ?>
                            
                            
                            </div>
                          </div>
                         
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Balance</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Lotto_Balance',array('type'=>'text','class'=>'amount-field balance','placeholder'=>'0.00','id'=>'balance','value'=>number_format((float)($rubydailyreporting['Lotto_Balance']), 2, '.', ''),'label'=>false,'id'=>'balance','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         
                        </div>
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Money Order Reading</h2>
                      </div>
                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. count</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('M_O_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'M_O_count','value'=>number_format((float)($rubydailyreporting['M_O_count']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. amount</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('M_O_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'M_O_amount','value'=>number_format((float)($rubydailyreporting['M_O_amount']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.O. fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('M_O_fees_collected',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'M_O_fees_collected','value'=>number_format((float)($rubydailyreporting['M_O_fees_collected']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                             
                            </div>
                          </div>
                          
                        </div>
                        
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Bill Pay Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Bill_pay_count',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','id'=>'Bill_pay_count','value'=>number_format((float)($rubydailyreporting['Bill_pay_count']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Bill_pay_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Bill_pay_amount','value'=>number_format((float)($rubydailyreporting['Bill_pay_amount']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                             
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Bill pay fees collected</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                       <?php echo $this->Form->input('Bill_pay_fees_collected',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Bill_pay_fees_collected','value'=>number_format((float)($rubydailyreporting['Bill_pay_fees_collected']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                        
                      </div>
                      
                      
                      
                      <div class="inside-sales">
                        <h2>Money Transfer I Reading</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. count:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('M_T_count',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'M_T_count','value'=>number_format((float)($rubydailyreporting['M_T_count']), 2, '.', ''),'label'=>false)); ?>
                         
                            
                            </div>
                          </div>
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">M.T. amount:</div>
                          <div class="grocery-tax-pos addon">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('M_T_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'M_T_amount','value'=>number_format((float)($rubydailyreporting['M_T_amount']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                         
                       


                            </div>
                          </div>
                          
                        </div>
                        
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Food Stamp Reading</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Sales by EBT/Foods:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                         <?php echo $this->Form->input('Sales_by_EBT/Foods',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','id'=>'Sales_by_EBT/Foods','value'=>number_format((float)($rubydailyreporting['Sales_by_EBT/Foods']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); ?>
                         
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                   
                      
                      
                      <div class="inside-sales">
                        <h2>TNRCC Reading</h2>
                      </div>
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Regular_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Regular_Stick_Inches']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                       
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Regular Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Regular_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Regular_Stick_Gal']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Plus_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Plus_Stick_Inches']), 2, '.', ''),'label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Plus Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Plus_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Plus_Stick_Gal']), 2, '.', ''),'label'=>false)); ?>
                              
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Inches</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                                <?php echo $this->Form->input('Super_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Super_Stick_Inches']), 2, '.', ''),'label'=>false)); ?>
                          
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Super Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Super_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Super_Stick_Gal']), 2, '.', ''),'label'=>false)); ?>
                         
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Inches:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Diesel_Stick_Inches',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Diesel_Stick_Inches']), 2, '.', ''),'label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Diesel Stick Gal.</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Diesel_Stick_Gal',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Diesel_Stick_Gal']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="inside-sales">
                        <h2>Gas Cash Card Sold</h2>
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"># of gas cards sold:</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('No_of_gas_cards_sold',array('type'=>'text','class'=>'hash-field','placeholder'=>'0.00','id'=>'No_of_gas_cards_sold','value'=>number_format((float)($rubydailyreporting['No_of_gas_cards_sold']), 2, '.', ''),'label'=>false)); ?>
                             
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">$ of gas cards sold:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                        <?php echo $this->Form->input('Amount_of_gas_cards_sold',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','id'=>'Amount_of_gas_cards_sold','value'=>number_format((float)($rubydailyreporting['Amount_of_gas_cards_sold']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                      
                      
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Gas card comm.:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Gas_card_comm',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Gas_card_comm']), 2, '.', ''),'label'=>false)); ?>
                            
                            </div>
                          </div>
                          
                        </div>
            
                      </div>
                     
                   
                      <div class="inside-sales">
                        <h2>Coupon</h2>
                      </div>                     
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Coupon Count</div>
                          <div class="grocery-tax-pos addon neutral">#</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('coupon_count',array('type'=>'text','class'=>'amount-field lottery','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['coupon_count']), 2, '.', ''),'id'=>'coupon_count','label'=>false)); ?>                            
                            </div>
                          </div>                         
                        </div>                      
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Coupon Amount</div>
                         <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('coupon_amount',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['coupon_amount']), 2, '.', ''),'id'=>'coupon_amount','label'=>false,'onkeyup'=>'outchange()')); ?>
                              
                            </div>
                          </div>
                        </div>
                        
                      </div>   
                   
                   
            
                      
                    </div>       
                    
                    <div class="col-md-6" data-spy="scroll">                     
                      
                       	<?php
                        if($this->Session->read('pos_type')=='ruby2'){
                        ?>
                        <div class="inside-sales">
                            <h2>Safe Drop</h2>
                          </div>                     
                        <div class="grocery-group">
                              <div class="grocery-tax addon">Safe Drop:</div>
                              <div class="grocery-tax-pos addon credit">-$</div>
                              <div class="form-cont">
                                <div class="form-control">
                         <?php 
                         echo $this->Form->input('paid_out',array('type'=>'text','class'=>'negetive-field lotto','placeholder'=>'0.00','id'=>'paid_out','value'=>number_format((float)($rubydailyreporting['paid_out']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); 
						 ?>                             
                            </div>
                              </div>
                              
                            </div>  
                        <?php
                        }else{
						echo $this->Form->input('paid_out',array('type'=>'hidden','id'=>'paid_out','value'=>0));	
						}
                        ?>                  
                       <div class="inside-sales">
                        <h2>Cash Purchases</h2>
                      </div>                    
                      <div class="grocery-cont">
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Purchases:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Cash_Purchases',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Cash_Purchases']), 2, '.', ''),'id'=>'Cash_Purchases','label'=>false,'onkeyup'=>'outchange()')); ?>
                              
                            </div>
                          </div>
                          <a class="btn-default" data-toggle="modal" data-target="#CashPurchaseModal"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="CashPurchaseModal" role="dialog">
                              <div class="modal-dialog modal-lg">                               
                                <div class="modal-content">
                            <div class="modal-header" style=" padding: 20px;">                                   
                            <h3 class="modal-title">Enter cash purchases</h3>
                            </div>                                   
                            <div class="modal-body credit-card-cont">
                            <div class="batch-add"><span>Vendor</span>
                            <div class="form-group">
                            <?php				
                            echo $this->Form->select('purchase_vendor', $purchase_vendor, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Vendor', 'id'=>'purchase_vendor')); ?>
                            </div>
                            </div>
                            <div class="batch-amount"><span>Amount</span> <br>
                            <input type="text" class="amount" name="purchase_amount" id="purchase_amount"/>
                            </div>
                            <div class="batch-amount"><span>Invoice No</span> <br>
                            <input type="text" class="amount" name="purchase_invoice_no" id="purchase_invoice_no"/>
                            </div>                                            
                            <div class="batch-amount"> <br>
                            <a class="btn-default cash_purchase_add">Add to list</a>
                            </div>                                        
                            </div>                                   
                            <?php if(isset($cash_purchase_details) && count($cash_purchase_details)==0)
							{
							?>
                            <div class="cp_before_add_div">
                            <h3 class="no-report">No entries made for this day report. <br>
                            <span style="font-size: 11px;">Start by making entries above</span> </h3>
                            </div>    
							<?php 
							}?>                                
                            <div class="cp_after_add_div table-responsive" <?php if(isset($cash_purchase_details) && count($cash_purchase_details)==0){?>style="display:none;" <?php } ?>>
                           
                            <div class="" id="jsondivcp"></div>
                            <table class="table table-hover" id="tbl_cash_purchase">
                            <tr>
                            <td>Vendor</td>                                             
                            <td>Amount</td>
                            <td>Invoice No</td>
                            <td>Action</td>
                            </tr>
                            <?php
							$i=0;
							$total_amount=0;
							if(isset($cash_purchase_details) && count($cash_purchase_details)>0)
							{							
							foreach($cash_purchase_details as $cash_purchase_row)
							{
							$i++;
							$total_amount=$total_amount+$cash_purchase_row['RGroceryInvoice']['gross_amount'];
							?>
                            <tr id="trcp_<?php echo $i;?>">
                            <td>
							<?php 
							//echo $cash_purchase_row['RGroceryInvoice']['vendor_id'];
							//print_r($purchase_vendor);							
							foreach($purchase_vendor as $key=>$val)
							{
								if($key==$cash_purchase_row['RGroceryInvoice']['vendor_id'])
								{
									echo $val;
								}
							}							
							?>
                            </td>
                            <td id="cash_purchase_amount_<?php echo $i;?>">
							<?php echo $cash_purchase_row['RGroceryInvoice']['gross_amount'];?>
                            </td>
                            <td>
                            <?php echo $cash_purchase_row['RGroceryInvoice']['invoice'];?>
                            </td>
                            <td><a href=javascript:void(0); id="td_" onclick="delete_cash_purchase(<?php echo $i;?>);"><i class="fa fa-trash"></i></a></td>
                            </tr>                            
                            <script type="text/javascript">							
							document.getElementById("jsondivcp").innerHTML += "<br /><input type='hidden' id='purchase_vendor<?php echo $i;?>'  value='<?php echo $cash_purchase_row['RGroceryInvoice']['vendor_id'];?>' style='width:100px' /><input type='hidden' style='width:100px'  value='<?php echo $cash_purchase_row['RGroceryInvoice']['gross_amount'];?>' id='purchase_amount<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $cash_purchase_row['RGroceryInvoice']['invoice'];?>' id='purchase_invoice_no<?php echo $i;?>' />";
                            </script>
                            <?php 							
							}
							}
							?>                
                            </table>
                            <div class="modal-body credit-card-cont" id="cash_purchase_total">Total cash purchase amount:<?php echo $total_amount;?></div>
                           
                            <input class="" type="hidden" id="count_inputcp" value="<?php echo $i;?>">
                            </div> 
                            <div class="modal-footer">                                 
                            <button type="button" id="cash_purchase_finish" class="btn-primary finish" data-dismiss="modal">Finish</button>
                            <button type="button" id="cash_purchase_close" class="btn-default" data-dismiss="modal" style="float: left;">Close</button>
                            </div>
                            </div>
                            </div>
                            </div> 
                        </div>
                        
                        <input id="input_hidden_field_objcp" type="hidden" name="input_hidden_field_objcp" />
                        
                        <script type="text/javascript">
							$(document).ready(function(){ 
								$(".cash_purchase_add").click(function(){ 
									var purchase_vendor = $("#purchase_vendor").val();
									var prchs_vendor=$("#purchase_vendor option:selected").text();
									//alert(prchs_vendor);									
									var purchase_amount=$("#purchase_amount").val();
									var purchase_invoice_no=$("#purchase_invoice_no").val();								
									if(!purchase_vendor) {
									alert('Please select vendor .');
									return false;
									}									
									if(!$.isNumeric(purchase_amount)) {
									alert('Please enter only numeric number in amount.');
									$("#purchase_amount").val('');
									return false;									
									}
									if(!purchase_amount) {
									alert('Please enter amount.');
									return false;
									}
									if(!$.isNumeric(purchase_invoice_no)) {
									alert('Please enter only numeric number in invoice.');
									$("#purchase_invoice_no").val('');
									return false;									
									}
									if(!purchase_invoice_no) {
									alert('Please enter invoice number.');
									return false;
									}															
									if(purchase_vendor !="" && purchase_amount !="" && purchase_invoice_no != "" ) {
										//alert(purchase_amount);
									showFormValuescp(purchase_vendor,prchs_vendor,purchase_amount,purchase_invoice_no);
									}
								});
							
							});
							var showFormValuesFuncCallscp=<?php echo $i;?>;
							var cash_purchase_total =<?php echo $total_amount;?>;
							function showFormValuescp(purchase_vendor,prchs_vendor,purchase_amount,purchase_invoice_no) {
								showFormValuesFuncCallscp++;
								$(".cp_before_add_div").hide();
								$(".cp_after_add_div").show();
								$('#tbl_cash_purchase tr:last').after('<tr id=trcp_'+showFormValuesFuncCallscp+'><td>'+prchs_vendor+'</td><td id=cash_purchase_amount_'+showFormValuesFuncCallscp+'>'+purchase_amount+'</td><td>'+purchase_invoice_no+'</td><td><a href=javascript:void(0); id=td_'+showFormValuesFuncCallscp+' onclick="delete_cash_purchase('+showFormValuesFuncCallscp+');"><i class="fa fa-trash"></i></a></td></tr>');
																
							document.getElementById("jsondivcp").innerHTML += "<br /><input type='hidden' id='purchase_vendor"+showFormValuesFuncCallscp+"'  value='"+purchase_vendor+"' style='width:100px' /><input type='hidden' style='width:100px'  value='"+purchase_amount+"' id='purchase_amount"+showFormValuesFuncCallscp+"' /><input type='hidden' style='width:100px'  value='"+purchase_invoice_no+"' id='purchase_invoice_no"+showFormValuesFuncCallscp+"' />";
								
								
								cash_purchase_totals(purchase_amount);								
								$("#purchase_vendor").val('');
								$("#purchase_amount").val('');
								$("#purchase_invoice_no").val('');								
								$('#count_inputcp').val(showFormValuesFuncCallscp);
								
							}
							
							function cash_purchase_totals(purchase_amount) {
								//alert("ghfh"+purchase_amount);
								cash_purchase_total = parseFloat(cash_purchase_total)+parseFloat(purchase_amount); 
								$("#cash_purchase_total").html("Total cash purchase amount: $"+cash_purchase_total);
							}
							
							function delete_cash_purchase(row_id) {								
								//alert(row_id);
								var delete_cp_amount=$('#cash_purchase_amount_'+row_id).html();	
								cash_purchase_total = parseFloat(cash_purchase_total)-parseFloat(delete_cp_amount); 
								$("#cash_purchase_total").html("Total cash purchase amount: $"+cash_purchase_total);						
								$('#trcp_'+row_id).remove();
								
							   $( '#purchase_vendor'+row_id ).remove();
							   $( '#purchase_amount'+row_id ).remove();
							   $( '#purchase_invoice_no'+row_id ).remove();							 
							   //$('#count_inputcp').val(row_id);
							}
							
							$("#cash_purchase_finish").click(function(){ 						
							$("#Cash_Purchases").val(cash_purchase_total);
							change();
                            outchange();
							});						
							
						</script> 
						<script>
							$(document).ready(function(){
							$("#cash_purchase_finish").click(function(){
							var myObj = [];
							var countinput = $("#count_inputcp").val()
							//alert(countinput);
							item = {};
							var i;
							for(i = 1; i <= countinput; i++) {
							id_exists=$('#purchase_vendor'+i).length;
							//alert(id_exists);
							if(id_exists!=0){	
							myObj.push({
							purchase_vendor: $('#purchase_vendor'+i).val(),
							purchase_amount: $('#purchase_amount'+i).val(),
							purchase_invoice_no: $('#purchase_invoice_no'+i).val()
							});
							}														
							}
							var json = JSON.stringify(myObj);
							//alert(json);
							$('#input_hidden_field_objcp').val(json);
							change();
                            outchange();							
							});
							});
                        </script>                        
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Pending Invoice:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Pending_Invoice',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Pending_Invoice']), 2, '.', ''),'id'=>'Pending_Invoice','label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
        <!--                  <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Purchase Rebates:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Purchase_Rebates',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Purchase_Rebates','value'=>number_format((float)($rubydailyreporting['Purchase_Rebates']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
        <!--                  <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                      </div>
                      
             <div class="inside-sales">
                        <h2>Other Money Paid</h2>
                      </div>
                      <!--inside sales ends-->
                      <div class="grocery-cont">
                        <div class="grocery-group">
                          <div class="grocery-tax addon">House Accounts:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Customer_account_sale',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Customer_account_sale']), 2, '.', ''),'id'=>'Customer_account_sale','label'=>false,'onkeyup'=>'outchange()')); ?>
                              
                            </div>
                          </div>
                          <a class="btn-default" data-toggle="modal" data-target="#HouseAccountModal"><i class="fa fa-pencil-square-o"></i> </a> 
                          <div class="modal fade" id="HouseAccountModal" role="dialog">
                              <div class="modal-dialog modal-lg">                               
                                <div class="modal-content">
                            <div class="modal-header" style=" padding: 20px;">                                   
                            <h3 class="modal-title">Enter House Accounts</h3>
                            </div>                                   
                            <div class="modal-body credit-card-cont">
                            <div class="batch-add"><span>Fuel Product Type:</span>
                            <div class="form-group">
                            <?php	
							$fuel_product_types = array('Regular' => 'Regular', 'Super' => 'Super', 'Plus' => 'Plus', 'Diesel' => 'Diesel', 'Kerosene' => 'Kerosene', 'Off Road' => 'Off Road',);
										
                            echo $this->Form->select('fuel_product_type', $fuel_product_types, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Type', 'id'=>'fuel_product_type')); ?>
                            </div>
                            </div>
                            
                            <div class="batch-amount"><span>Account Name</span> <br>
                            <input type="text" class="amount" name="account_name" id="account_name"/>
                            </div>
                            <div class="batch-amount"><span>Vehical Tag#</span> <br>
                            <input type="text" class="amount" name="vehical_tag" id="vehical_tag"/>
                            </div>   
                            
                            <div class="batch-amount"><span>Gallons Purchased</span> <br>
                            <input type="text" class="amount" name="gallons_purchased" id="gallons_purchased"/>
                            </div>                            
                            
                            <div class="batch-amount"><span>Amount</span> <br>
                            <input type="text" class="amount" name="ha_amount" id="ha_amount"/>
                            </div>
                            <div class="batch-amount"><span>Memo</span> <br>
                            <input type="text" class="amount" name="ha_memo" id="ha_memo"/>
                            </div>                              
                                                                     
                            <div class="batch-amount"> <br>
                            <a class="btn-default ha_add">Add to list</a>
                            </div>                                        
                            </div>                                   
                            
                            <?php if(isset($ha_details) && count($ha_details)==0)
							{
							?>
                            <div class="ha_before_add_div">
                            <h3 class="no-report">No entries made for this day report. <br>
                            <span style="font-size: 11px;">Start by making entries above</span>
                            </h3>
                            </div>     
							<?php 
							}?> 
                                                              
                            <div class="ha_after_add_div table-responsive" <?php if(isset($ha_details) && count($ha_details)==0){?>style="display:none;" <?php } ?>>
                            <div class="" id="jsondivha"></div>
                            <table class="table table-hover" id="tbl_ha">
                            <tr>
                            <td>Fuel</td> 
                            <td>Account Name</td>
                            <td>Vehical Tag</td>
                            <td>Gallons Purchased</td>
                            <td>Amount</td>
                            <td>Invoice No</td>
                            <td>Action</td>
                            </tr>
                            
                             <?php
							$i=0;
							$total_ha_amount=0;
							if(isset($ha_details) && count($ha_details)>0)
							{							
							foreach($ha_details as $ha_row)
							{
							$i++;
							$total_ha_amount=$total_ha_amount+$ha_row['Houseacccustsale']['amount'];
							?>
                            <tr id="trha_<?php echo $i;?>">
                            <td>
							<?php 
							echo $ha_row['Houseacccustsale']['fuel_product_type'];							
							?>
                            </td>
                            <td>
                            <?php echo $ha_row['Houseacccustsale']['account_name'];?>
                            </td>
                            <td>
                            <?php echo $ha_row['Houseacccustsale']['vehical_tag'];?>
                            </td>
                            <td>
                            <?php echo $ha_row['Houseacccustsale']['gallons_purchased'];?>
                            </td>
                            <td id="ha_amount_<?php echo $i;?>">
							<?php echo $ha_row['Houseacccustsale']['amount'];?>
                            </td>
                            <td>
                            <?php echo $ha_row['Houseacccustsale']['memo'];?>
                            </td>
                            <td><a href=javascript:void(0); id="td_<?php echo $i;?>" onclick="delete_ha(<?php echo $i;?>);"><i class="fa fa-trash"></i></a></td>
                            </tr>                            
                            <script type="text/javascript">							
							document.getElementById("jsondivha").innerHTML += "<br /><input type='hidden' id='fuel_product_type<?php echo $i;?>'  value='<?php echo $ha_row['Houseacccustsale']['fuel_product_type'];?>' style='width:100px' /><input type='hidden' style='width:100px'  value='<?php echo $ha_row['Houseacccustsale']['account_name'];?>' id='account_name<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ha_row['Houseacccustsale']['vehical_tag'];?>' id='vehical_tag<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ha_row['Houseacccustsale']['gallons_purchased'];?>' id='gallons_purchased<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ha_row['Houseacccustsale']['amount'];?>' id='ha_amount<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ha_row['Houseacccustsale']['memo'];?>' id='ha_memo<?php echo $i;?>' />";
                            </script>
                            <?php 							
							}
							}
							?> 
                            
                            </table>
                            <div class="modal-body credit-card-cont" id="ha_total">Total house accounts amount:$<?php echo $total_ha_amount;?></div>
                           
                            <input class="" type="hidden" id="count_inputha" value="<?php echo $i;?>">
                            </div> 
                            <div class="modal-footer">                                 
                           <button type="button" id="ha_finish" class="btn-primary finish" data-dismiss="modal">Finish</button>
                           <button type="button" id="ha_close" class="btn-default" data-dismiss="modal" style="float: left;">Close</button>
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        
                         <input id="input_hidden_field_objha" type="hidden" name="input_hidden_field_objha" />
                        
                        <script type="text/javascript">
							$(document).ready(function(){ 
								$(".ha_add").click(function(){ 
									var fuel_product_type = $("#fuel_product_type").val();
									var fuel_product_txt=$("#fuel_product_type option:selected").text();
									//alert(prchs_vendor);									
									var account_name=$("#account_name").val();
									var vehical_tag=$("#vehical_tag").val();	
									var gallons_purchased=$("#gallons_purchased").val();
									var ha_amount=$("#ha_amount").val();	
									var ha_memo=$("#ha_memo").val();
																
									if(!fuel_product_type) {
									alert('Please select fuel product type .');
									return false;
									}									
									if(!$.isNumeric(ha_amount)) {
									alert('Please enter only numeric number in amount.');
									$("#ha_amount").val('');
									return false;									
									}
									if(!ha_amount) {
									alert('Please enter amount.');
									return false;
									}
									if(!account_name) {
									alert('Please enter account name.');
									return false;
									}						
									
																							
									if(fuel_product_type !="" && ha_amount !="" && account_name != "" ) {
									showFormValuesha(fuel_product_type,fuel_product_txt,account_name,vehical_tag,gallons_purchased,ha_amount,ha_memo);
									}
								});
							
							});
							var showFormValuesFuncCallsha=<?php echo $i;?>;
							var ha_total = <?php echo $total_ha_amount;?>;
							function showFormValuesha(fuel_product_type,fuel_product_txt,account_name,vehical_tag,gallons_purchased,ha_amount,ha_memo) {
								showFormValuesFuncCallsha++;
								$(".ha_before_add_div").hide();
								$(".ha_after_add_div").show();
								$('#tbl_ha tr:last').after('<tr id=trha_'+showFormValuesFuncCallsha+'><td>'+fuel_product_txt+'</td><td>'+account_name+'</td><td>'+vehical_tag+'</td><td>'+gallons_purchased+'</td><td id=ha_amount_'+showFormValuesFuncCallsha+'>'+ha_amount+'</td><td>'+ha_memo+'</td><td><a href=javascript:void(0); id=td_'+showFormValuesFuncCallsha+' onclick="delete_ha('+showFormValuesFuncCallsha+');"><i class="fa fa-trash"></i></a></td></tr>');
																
							document.getElementById("jsondivha").innerHTML += "<br /><input type='hidden' id='fuel_product_type"+showFormValuesFuncCallsha+"'  value='"+fuel_product_type+"' style='width:100px' /><input type='hidden' style='width:100px'  value='"+account_name+"' id='account_name"+showFormValuesFuncCallsha+"' /><input type='hidden' style='width:100px'  value='"+vehical_tag+"' id='vehical_tag"+showFormValuesFuncCallsha+"' /><input type='hidden' style='width:100px'  value='"+gallons_purchased+"' id='gallons_purchased"+showFormValuesFuncCallsha+"' /><input type='hidden' style='width:100px'  value='"+ha_amount+"' id='ha_amount"+showFormValuesFuncCallsha+"' /><input type='hidden' style='width:100px'  value='"+ha_memo+"' id='ha_memo"+showFormValuesFuncCallsha+"' />";
								
								
								ha_totals(ha_amount);								
								$("#fuel_product_type").val('');
								$("#account_name").val('');
								$("#vehical_tag").val('');
								$("#gallons_purchased").val('');
								$("#ha_amount").val('');
								$("#ha_memo").val('');									
								$('#count_inputha').val(showFormValuesFuncCallsha);
								
							}
							
							function ha_totals(ha_amount) {
								//alert("ghfh"+ha_amount);
								ha_total = parseFloat(ha_total)+parseFloat(ha_amount); 
								$("#ha_total").html("Total house accounts amount: $"+ha_total);
							}
							
							function delete_ha(row_id) {
								var delete_ha_amount=$('#ha_amount_'+row_id).html();	
								ha_total = parseFloat(ha_total)-parseFloat(delete_ha_amount); 
								$("#ha_total").html("Total house accounts amount: $"+ha_total);						
								$('#trha_'+row_id).remove();
								
							   $( '#fuel_product_type'+row_id ).remove();
							   $( '#account_name'+row_id ).remove();
							   $( '#vehical_tag'+row_id ).remove();
							   $( '#gallons_purchased'+row_id ).remove();
							   $( '#ha_amount'+row_id ).remove();
							   $( '#ha_memo'+row_id ).remove();								 
							   
							}
							
							$("#ha_close, #ha_finish").click(function(){ 						
							$("#Customer_account_sale").val(ha_total);
							change();
                            outchange();
							});						
							
						</script> 
						<script>
							$(document).ready(function(){
							$("#ha_close, #ha_finish").click(function(){
							var myObj = [];
							var countinput = $("#count_inputha").val()
							//alert(countinput);
							item = {};
							var i;
							for(i = 1; i <= countinput; i++) {
							id_exists=$('#fuel_product_type'+i).length;
							//alert(id_exists);
							if(id_exists!=0){	
							myObj.push({
							fuel_product_type: $('#fuel_product_type'+i).val(),
							account_name: $('#account_name'+i).val(),
							vehical_tag: $('#vehical_tag'+i).val(),
							
							
							gallons_purchased: $('#gallons_purchased'+i).val(),
							ha_amount: $('#ha_amount'+i).val(),
							ha_memo: $('#ha_memo'+i).val()
							});	
							}
							}
							var json = JSON.stringify(myObj);
							//   alert(json);
							$('#input_hidden_field_objha').val(json);
							change();
                            outchange();							
							});
							});
                        </script> 
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Cash Expenses:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Cash_Expenses',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Cash_Expenses']), 2, '.', ''),'id'=>'Cash_Expenses','label'=>false,'onkeyup'=>'outchange()')); ?>
                              
                            </div>
                          </div>
                           
                         <a class="btn-default" data-toggle="modal" data-target="#CashExpensesModal"><i class="fa fa-pencil-square-o"></i> </a>                   
                         <div class="modal fade" id="CashExpensesModal" role="dialog">
                              <div class="modal-dialog modal-lg">                               
                                <div class="modal-content">
                            <div class="modal-header" style=" padding: 20px;">                                   
                            <h3 class="modal-title">Enter Cash Expenses</h3>
                            </div>                                   
                            <div class="modal-body credit-card-cont">
                            <div class="batch-add"><span>Category:</span>
                            <div class="form-group">
                            <?php							
                            echo $this->Form->select('category',$catgories, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Category', 'id'=>'category')); ?>
                            </div>
                            </div>
                            
                            <div class="batch-add"><span>Payment Mode:</span>
                            <div class="form-group">
                            <?php	
							$payment_modes = array('Cash' => 'Cash','Check/EFT/ACH' =>'Check/EFT/ACH');
										
                            echo $this->Form->select('payment_mode',$payment_modes, array('class' => 'form-control credit-add1', 'label'=> false, 'empty' => 'Select Mode', 'id'=>'payment_mode')); ?>
                            </div>
                            </div>                            
                            
                            <div class="batch-amount"><span>Cheque Number</span> <br>
                            <input type="text" class="amount" name="cheque_number" id="cheque_number"/>
                            </div>                          
                            
                            <div class="batch-amount"><span>Amount</span> <br>
                            <input type="text" class="amount" name="ce_amount" id="ce_amount"/>
                            </div>
                            
                            <div class="batch-amount"><span>Payee</span> <br>
                            <input type="text" class="amount" name="ce_payee" id="ce_payee"/>
                            </div>  
                            
                            <div class="batch-amount"><span>Memo</span> <br>
                            <input type="text" class="amount" name="ce_memo" id="ce_memo"/>
                            </div>  
                            
                                                                     
                            <div class="batch-amount"> <br>
                            <a class="btn-default ce_add">Add to list</a>
                            </div>                                        
                            </div>   
                            <?php if(isset($cash_expenses_details) && count($cash_expenses_details)==0)
							{
							?>
                            <div class="ce_before_add_div">
                            <h3 class="no-report">No entries made for this day report. <br>
                            <span style="font-size: 11px;">Start by making entries above</span> </h3>
                            </div>    
							<?php 
							}?>                             
                                                             
                            <div class="ce_after_add_div table-responsive" <?php if(isset($cash_expenses_details) && count($cash_expenses_details)==0){?>style="display:none;" <?php } ?>>
                            <div class="" id="jsondivce"></div>
                            <table class="table table-hover" id="tbl_ce">
                            <tr>
                            <td>Category</td> 
                            <td>Payment Mode</td>
                            <td>Cheque Number</td>
                            <td>Amount</td>
                            <td>Payee</td>                   
                            <td>Memo</td>
                            <td>Action</td>
                            </tr>                            
                            
                            <?php
							$i=0;
							$total_ce_amount=0;
							if(isset($cash_expenses_details) && count($cash_expenses_details)>0)
							{							
							foreach($cash_expenses_details as $ce_row)
							{
							$i++;
							$total_ce_amount=$total_ce_amount+$ce_row['CashExpense']['amount'];
							?>
                            <tr id="trce_<?php echo $i;?>">
                            <td>
							<?php 							
							foreach($catgories as $key=>$val)
							{
								if($key==$ce_row['CashExpense']['category'])
								{
									echo $val;
								}
							}							
							?>
                            </td>
                            <td>
                            <?php echo $ce_row['CashExpense']['payment_mode'];?>
                            </td>
                            <td>
                            <?php echo $ce_row['CashExpense']['cheque_number'];?>
                            </td>
                            <td id="ce_amount_<?php echo $i;?>">
                            <?php echo $ce_row['CashExpense']['amount'];?>
                            </td>
                            <td >
							<?php echo $ce_row['CashExpense']['payee'];?>
                            </td>
                            <td>
                            <?php echo $ce_row['CashExpense']['memo'];?>
                            </td>
                            <td><a href=javascript:void(0); id="td_<?php echo $i;?>" onclick="delete_ce(<?php echo $i;?>);"><i class="fa fa-trash"></i></a></td>
                            </tr>                            
                            <script type="text/javascript">							
							document.getElementById("jsondivce").innerHTML += "<br /><input type='hidden' id='category<?php echo $i;?>'  value='<?php echo $ce_row['CashExpense']['category'];?>' style='width:100px' /><input type='hidden' style='width:100px'  value='<?php echo $ce_row['CashExpense']['payment_mode'];?>' id='payment_mode<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ce_row['CashExpense']['cheque_number'];?>' id='cheque_number<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ce_row['CashExpense']['amount'];?>' id='ce_amount<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ce_row['CashExpense']['payee'];?>' id='ce_payee<?php echo $i;?>' /><input type='hidden' style='width:100px'  value='<?php echo $ce_row['CashExpense']['memo'];?>' id='ce_memo<?php echo $i;?>' />";
                            </script>
                            <?php 							
							}
							}
							?>                             
                            
                            </table>
                            <div class="modal-body credit-card-cont" id="ce_total">Total Cash Expenses amount:$<?php echo $total_ce_amount;?></div>
                            
                            <input class="" type="hidden" id="count_inputce" value="<?php echo $i;?>">
                            </div> 
                            <div class="modal-footer">                                 
                           <button type="button" id="ce_finish" class="btn-primary finish" data-dismiss="modal">Finish</button>
                           <button type="button" id="ce_close" class="btn-default" data-dismiss="modal" style="float: left;">Close</button>
                            </div>
                            </div>
                            </div>
                            </div>
                           
                          
                        </div>
                        <input id="input_hidden_field_objce" type="hidden" name="input_hidden_field_objce" />
                        
                        <script type="text/javascript">
							$(document).ready(function(){ 
								$(".ce_add").click(function(){ 
									var category = $("#category").val();
									var category_txt=$("#category option:selected").text();
									//alert(category);									
									var payment_mode=$("#payment_mode").val();
									var cheque_number=$("#cheque_number").val();	
									var ce_amount=$("#ce_amount").val();
									var ce_payee=$("#ce_payee").val();	
									var ce_memo=$("#ce_memo").val();
																
									if(!category) {
									alert('Please select category .');
									return false;
									}	
									if(!payment_mode) {
									alert('Please select payment mode .');
									return false;
									}
									/*if(!cheque_number) {
									alert('Please enter cheque number.');
									return false;
									}	*/
									if(!ce_amount) {
									alert('Please enter amount.');
									return false;
									}								
									if(!$.isNumeric(ce_amount)) {
									alert('Please enter only numeric number in amount.');
									$("#ce_amount").val('');
									return false;									
									}							

																							
									if(category !="" && payment_mode !="" && ce_amount != "") {
									showFormValuesce(category,category_txt,payment_mode,cheque_number,ce_amount,ce_payee,ce_memo);
									}
								});
							
							});
							var showFormValuesFuncCallsce=<?php echo $i;?>;
							var ce_total = <?php echo $total_ce_amount;?>;
							function showFormValuesce(category,category_txt,payment_mode,cheque_number,ce_amount,ce_payee,ce_memo) {
								showFormValuesFuncCallsce++;
								$(".ce_before_add_div").hide();
								$(".ce_after_add_div").show();
								$('#tbl_ce tr:last').after('<tr id=trce_'+showFormValuesFuncCallsce+'><td>'+category_txt+'</td><td>'+payment_mode+'</td><td>'+cheque_number+'</td><td id=ce_amount_'+showFormValuesFuncCallsce+'>'+ce_amount+'</td><td>'+ce_payee+'</td><td>'+ce_memo+'</td><td><a href=javascript:void(0); id=td_'+showFormValuesFuncCallsce+' onclick="delete_ce('+showFormValuesFuncCallsce+');"><i class="fa fa-trash"></i></a></td></tr>');
																
							document.getElementById("jsondivce").innerHTML += "<br /><input type='hidden' id='category"+showFormValuesFuncCallsce+"'  value='"+category+"' style='width:100px' /><input type='hidden' style='width:100px'  value='"+payment_mode+"' id='payment_mode"+showFormValuesFuncCallsce+"' /><input type='hidden' style='width:100px'  value='"+cheque_number+"' id='cheque_number"+showFormValuesFuncCallsce+"' /><input type='hidden' style='width:100px'  value='"+ce_amount+"' id='ce_amount"+showFormValuesFuncCallsce+"' /><input type='hidden' style='width:100px'  value='"+ce_payee+"' id='ce_payee"+showFormValuesFuncCallsce+"' /><input type='hidden' style='width:100px'  value='"+ce_memo+"' id='ce_memo"+showFormValuesFuncCallsce+"' />";
								
								
								ce_totals(ce_amount);								
								$("#category").val('');
								$("#payment_mode").val('');
								$("#cheque_number").val('');
								$("#ce_amount").val('');
								$("#ce_payee").val('');
								$("#ce_memo").val('');									
								$('#count_inputce').val(showFormValuesFuncCallsce);
								
							}
							
							function ce_totals(ce_amount) {
								//alert("ghfh"+ce_amount);
								ce_total = parseFloat(ce_total)+parseFloat(ce_amount); 
								$("#ce_total").html("Total Cash Expenses amount: $"+ce_total);
							}
							
							function delete_ce(row_id) {
								var delete_ce_amount=$('#ce_amount_'+row_id).html();	
								ce_total = parseFloat(ce_total)-parseFloat(delete_ce_amount); 
								$("#ce_total").html("Total house accounts amount: $"+ce_total);						
								$('#trce_'+row_id).remove();
								
							   $( '#category'+row_id ).remove();
							   $( '#payment_mode'+row_id ).remove();
							   $( '#cheque_number'+row_id ).remove();
							   $( '#ce_amount'+row_id ).remove();
							   $( '#ce_payee'+row_id ).remove();
							   $( '#ce_memo'+row_id ).remove();								 
							   
							}
							
							$("#ce_close, #ce_finish").click(function(){ 						
							$("#Cash_Expenses").val(ce_total);
							change();
                            outchange();
							});						
							
						</script> 
						<script>
							$(document).ready(function(){
							$("#ce_close, #ce_finish").click(function(){
							var myObj = [];
							var countinput = $("#count_inputce").val()
							//alert(countinput);
							item = {};
							var i;
							for(i = 1; i <= countinput; i++) {
							id_exists=$('#category'+i).length;
							//alert(id_exists);
							if(id_exists!=0){	
							myObj.push({
							category: $('#category'+i).val(),
							payment_mode: $('#payment_mode'+i).val(),
							cheque_number: $('#cheque_number'+i).val(),						
							ce_amount: $('#ce_amount'+i).val(),
							ce_payee: $('#ce_payee'+i).val(),
							ce_memo: $('#ce_memo'+i).val()
							});	
							}
							}
							var json = JSON.stringify(myObj);
							//   alert(json);
							$('#input_hidden_field_objce').val(json);
							change();
                            outchange();							
							});
							});
                        </script> 
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon"> Cash Withdrawn:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('Cash_Withdrawn',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Cash_Withdrawn']), 2, '.', ''),'id'=>'Cash_Withdrawn','label'=>false,'onkeyup'=>'outchange()')); ?>
                              
                            </div>
                          </div>
         <!--                 <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                       
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Paid :</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Loan_Paid',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Loan_Paid']), 2, '.', ''),'id'=>'Loan_Paid','label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
        <!--                  <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                      </div>
                      
                       <div class="inside-sales">
                        <h2>Other Receipts</h2>
                      </div>
                       <div class="grocery-group">
                          <div class="grocery-tax addon">Customer account </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Customer_account_paid',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Customer_account_paid','value'=>number_format((float)($rubydailyreporting['Customer_account_paid']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                           
                            </div>
                          </div>
         <!--                 <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Other Income</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                              <?php echo $this->Form->input('other_income',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'other_income','value'=>number_format((float)($rubydailyreporting['other_income']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
         <!--                 <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Loan Received </div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('loan_received',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'loan_received','value'=>number_format((float)($rubydailyreporting['loan_received']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>                             
                            </div>
                          </div>
         <!--                 <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                       <div class="inside-sales">
                        <h2>Cash Control</h2>
                      </div>
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Opening Cash:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Opening_Cash',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','id'=>'Opening_Cash','value'=>number_format((float)($rubydailyreporting['Opening_Cash']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Opening checks:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('Opening_Checks',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','id'=>'Opening_Checks','value'=>number_format((float)($rubydailyreporting['Opening_Checks']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
                         
                          
                        </div>
                     
                     
                     <div class="grocery-group">
                          <div class="grocery-tax addon">Total Opening Amt:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Total_Opening_Amt',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Total_Opening_Amt','value'=>number_format((float)($rubydailyreporting['Total_Opening_Amt']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>                            
                            </div>
                          </div>
                        </div>                       
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Money from Bank:</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Money_from_Bank',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Money_from_Bank','value'=>number_format((float)($rubydailyreporting['Money_from_Bank']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                             
                            </div>
                          </div>
             <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i></a>-->
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Chk Cashing Comm.</div>
                          <div class="grocery-tax-pos addon debit">+$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Chk_Cashing_Comm',array('type'=>'text','class'=>'amount-field','placeholder'=>'0.00','id'=>'Chk_Cashing_Comm','value'=>number_format((float)($rubydailyreporting['Chk_Cashing_Comm']), 2, '.', ''),'label'=>false,'onkeyup'=>'change()')); ?>
                            
                            </div>
                          </div>
                         
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Put In ATM:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                               <?php echo $this->Form->input('Put_In_ATM',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','id'=>'Put_In_ATM','value'=>number_format((float)($rubydailyreporting['Put_In_ATM']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); ?>
                            
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total deposits:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Total_deposits',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Total_deposits']), 2, '.', ''),'id'=>'Total_deposits','label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
                         <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Cash:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                             <?php echo $this->Form->input('Closing_Cash',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','id'=>'ccash','value'=>number_format((float)($rubydailyreporting['Closing_Cash']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Closing Checks:</div>
                          <div class="grocery-tax-pos addon credit">-$</div>
                          <div class="form-cont">
                            <div class="form-control">
                            <?php echo $this->Form->input('Closing_Checks',array('type'=>'text','class'=>'negetive-field','placeholder'=>'0.00','id'=>'ccheck','value'=>number_format((float)($rubydailyreporting['Closing_Checks']), 2, '.', ''),'label'=>false,'onkeyup'=>'outchange()')); ?>
                             
                            </div>
                          </div>
                          
                          
                        </div>
                        
                        
                        <div class="grocery-group">
                          <div class="grocery-tax addon">Total Closing Amt:</div>
                          <div class="grocery-tax-pos addon neutral">$</div>
                          <div class="form-cont">
                            <div class="form-control form-cont-bg">
                             <?php echo $this->Form->input('Total_Closing_Amt',array('type'=>'text','class'=>'dollar-field','placeholder'=>'0.00','value'=>number_format((float)($rubydailyreporting['Total_Closing_Amt']), 2, '.', ''),'label'=>false,'id'=>'totalcamt','readonly'=>'readonly')); ?>
                             
                            </div>
                          </div>
                         <!-- <a class="btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i> </a>-->
                          
                        </div>
                    </div>
                  </div>
                </div>
                
              </div>
              
              
              <div class="enter-total">
              
              <div class="col-md-1 z-reading hideleb"><span >ZReading</span> <br>
                    <?php echo $this->Form->input('z_reading',array('type'=>'text','id'=>'z_reading','class'=>'negetive-field lotto amount','value'=>number_format((float)($rubydailyreporting['z_reading']), 0, '.', ''),'readonly'=>'readonly'));					
					?>                 
                  </div>
              <div class="col-md-2 z-reading hideleb"><span >Verified by</span> <br>
                    <?php echo $this->Form->input('verified_by',array('type'=>'text','class'=>'negetive-field lotto amount','value'=>$rubydailyreporting['verified_by']));
					?>                  
                  </div>
              <div class="col-md-1 z-reading hideleb"><span >Note</span> <br>
                <?php echo $this->Form->input('note',array('type'=>'hidden','id'=>'note','class'=>'negetive-field lotto amount','value'=>$rubydailyreporting['note']));
				?>
                 <a class="btn-default" data-toggle="modal" data-target="#myModalNote"><i class="fa fa-pencil-square-o"></i> </a>
                          <div class="modal fade" id="myModalNote" role="dialog">
                            <div class="modal-dialog modal-lg">                              
                                  <div class="modal-content">
                                    <div class="modal-header" style=" padding: 20px;">                                     
                                      <h3 class="modal-title">Enter Note</h3>
                                    </div>                                   
                                    <div class="modal-body credit-card-cont">
                                            <div class=""><span>Note</span> <br>  
      <textarea class="note_amount" name="modal_note" id="modal_note"><?php echo $rubydailyreporting['note'];?></textarea>
                                            </div>                                           
                                        </div>                                 
                                      
        <div class="modal-footer">                                 
          <button type="button" id="note_finish" class="btn-primary finish" data-dismiss="modal" onclick="note_retrieve()">Finish</button>
          <button type="button" id="note_close" class="btn-default" data-dismiss="modal" onclick="note_retrieve()" style="float: left;">Close</button>
        </div>
        
        </div>
                              
        </div>
        
        </div>
                
                
                
                
                </div>
              <div class="col-md-2">
              
              <div class="col-md-7 nopaddingmrgn z-reading hideleb"><span>Attachment</span> <br>
                <?php echo $this->Form->input('file',array('type'=>'file','id'=>'attachment_file','class'=>'negetive-field lotto amount smltpgpfv','onchange'=>'file_check()'));
                ?>     
                 <label for="attachment_file" class="attachment_filecss"></label>  
              </div> 
              <?php if($rubydailyreporting['file']!='')
			  {
			  ?>
              <div class="col-md-5 nopaddingmrgn z-reading hideleb"> <span>View </span> <br>
              <a href="<?php echo $this->webroot.'rubydailyreportingdocs/'.$rubydailyreporting['file'];?>" class="btn-success view_file fltlft" target="_blank"><i class="fa fa-eye"></i></a>
             </div>
              <?php
			   }
			   ?>  
               </div>
              <div class="col-md-2 z-reading "><span>Total in</span> <br>
                    <?php echo $this->Form->input('Total_In',array('type'=>'hidden','id'=>'Total_In','label'=>false,'value'=>number_format((float)($rubydailyreporting['Total_In']), 2, '.', ''))); ?> 
                    <div class="positive">$<span id="totalin"><?php echo number_format((float)($rubydailyreporting['Total_In']), 2, '.', '');?></span></div>
                  </div>	
              <div class="col-md-2 z-reading"><span>Total Out</span> <br>                
                   <?php echo $this->Form->input('Total_Out',array('type'=>'hidden','id'=>'Total_Out','label'=>false,'value'=>number_format((float)($rubydailyreporting['Total_Out']), 2, '.', ''))); ?> 
                    <div class="negetive">$<span id="totalout"><?php echo number_format((float)($rubydailyreporting['Total_Out']), 2, '.', '');?></span></div>
                  </div>
              <div class="col-md-2 z-reading"><span>Short Over</span> <br>                  
                    <?php 				
					echo $this->Form->input('Short_Over',array('type'=>'hidden','id'=>'Short_Over','label'=>false,'value'=>number_format((float)($rubydailyreporting['Short_Over']), 2, '.', ''))); ?> 
                    <div class="short-over"><span id="short_over_sign"><?php echo $sign;?></span>$<span id="overtotal"><?php echo number_format((float)($rubydailyreporting['Short_Over']), 2, '.', '');?></span></div>
                  </div>
                  
            <div class="clearfix"></div>
             <div class="col-md-2 flgtrtrt "> 
             <input type="submit" id="form_submit" value="Save Report" name="Save Report" class="btn-primary save-report">
             </div>
            </div>
            
            
            <style>
            .hideleb label{ display:none;}
			.hideleb .amount{ width:100%; text-align:left; padding-left:5px;}
            </style>
            <style>
      .hideleb label{ display:none;}
      .hideleb .amount{ width:100%; text-align:left; padding-left:5px;}
      .batch-add { float: left; width: 162px; font-size: 12px;}
      .amount { width: 100%;  border: 1px solid #ccc;  height: 32px;  margin: 2px 0 0 0;  border-radius: 5px;} 
	  .note_amount { width: 100%;  border: 1px solid #ccc;  height: 110px;  margin: 2px 0 0 0;  border-radius: 5px;}
	  .modal-content {width:725px;}
	  .enter-total {
    margin: 30px auto;
    border: 1px solid #eaeef1;
    border-radius: 5px;
}    


	.view_file{
		width: 30px;
		border: medium none;
		padding: 8px;
		color: #FFF;
		float: right;
		font-size: 13px;
		margin: 17px 0 0 -0;
	}
	.nopaddingmrgn{ padding:0px !important;}
	.smltpgpfv{ padding-top:5px;}


    input[type=file]#attachment_file{ opacity:0;}
   label.attachment_filecss{ display:block !important; position:relative;}
   label.attachment_filecss:before{font: normal normal normal 14px/1 FontAwesome;  font-size: 20px;  content: "\f093";  color: #788288;   margin-top: -31px;
    position: absolute;  margin-left: 12px;  border: 1px solid #c5c4c4;  padding: 5px 7px;  border-radius: 5px;}
 .fltlft{ float:left; margin-top:0px;}
 .flgtrtrt .save-report{ float:left;}



 </style>
              
              
              
               
            
               </div>    
    
       </div>


      <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-lg"> 
                              
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style=" padding: 20px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h3 class="modal-title">Enter credit card batches</h3>
                                </div>
                                <div class="modal-body credit-card-cont">
                                  <div class="batch-add"><span>Batch type</span>
                                    <div class="form-group">
                                      <select class="form-control credit-add1">
                                        <option>All Credit Card</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                      </select>
                                      <div class="credit-add">
                                        <button class="btn-default"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="batch-type"><span>Batch#</span> <br>
                                    <input type="text" class="batch-field"/>
                                  </div>
                                  <div class="batch-amount"><span>Amount</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Fee</span> <br>
                                    <input type="text" class="amount"/>
                                  </div>
                                  <div class="batch-amount"><span>Net</span> <br>
                                    <input type="text" class="net"/>
                                  </div>
                                  <div class="batch-amount"> <br>
                                    <button class="btn-default">Add Batch</button>
                                  </div>
                                </div>
                                <h3 class="no-report">No entries made for this day report. <br>
                                  <span style="font-size: 11px;">Start by making entries above</span> </h3>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn-primary finish" data-dismiss="modal">Finish</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                          
                          
                          
                          
                          
                          

<?php echo $this->Form->end(); ?>  
