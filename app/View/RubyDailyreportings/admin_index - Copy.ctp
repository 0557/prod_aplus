<?php /* <div class="row">
  <div class="col-xs-12">
    <ul class="menu-btn1">
        <li class="<?php echo (($this->params['controller'] ==  'ruby_uprodts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bar-chart'></i>Sales by MOP", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utankts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-pie-chart'></i>Sale By Tank No", array('controller' => 'ruby_utankts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<!--<li class="<?php echo (($this->params['controller'] ==  'ruby_uautots')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-line-chart'></i>Sales by MOP", array('controller' => 'ruby_uautots', 'action' => 'index'), array('escape' => false)); ?>
		</li>-->
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uhosets')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Sales HOSC", array('controller' => 'ruby_uhosets', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_userlvts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-calculator'></i>Sale By Service Level", array('controller' => 'ruby_userlvts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utierts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-clipboard'></i>Sale By Tier Product", array('controller' => 'ruby_utierts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_edispts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-o'></i>Dispenser Sales", array('controller' => 'ruby_edispts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dcrstats')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-large'></i>Sale By DCR No", array('controller' => 'ruby_dcrstats', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_deptotals')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-list'></i>Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dailyreportings')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-columns'></i>Daily Reporting", array('controller' => 'ruby_dailyreportings', 'action' => 'index'), array('escape' => false)); ?>
		</li>
    </ul>
  </div>
</div>
*/?>

<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body">
<div class="rubyUtierts index">
	<h3 class="page-title"><?php echo __('Daily Reporting'); ?> <span class="btn green fileinput-button" style="margin-right:10px;">
                        <a href="<?php echo Router::url('/') ?>admin/ruby_dailyreportings/add"><i class="fa fa-plus"></i>  <span>Add New</span></a> 
							
                    </span></h3>
	
<div class="row">
<div class="col-md-12">
       <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Date'); ?></th>
			<th><?php echo $this->Paginator->sort('Zreading'); ?></th>
			<th><?php echo $this->Paginator->sort('Total In'); ?></th>
			<th><?php echo $this->Paginator->sort('Total Out'); ?></th>
			<th><?php echo $this->Paginator->sort('Short/OVer'); ?></th>
		
			
			<th><?php echo 'Action'; ?></th>
			
			
	</tr>
	</thead>
	<tbody>
	<?php if(isset($reports)) { foreach ($reports as $rubyUtiert): ?>
	<tr>
	   <td><?php echo h($rubyUtiert['RubyDailyreporting']['create_date']); ?>&nbsp;</td>
		<td><?php echo h($rubyUtiert['RubyDailyreporting']['z_reading']); ?>&nbsp;</td>
		<td><?php echo $rubyUtiert['RubyDailyreporting']['Total_In']; ?>&nbsp;</td>
		<td><?php echo h($rubyUtiert['RubyDailyreporting']['Total_Out']); ?>&nbsp;</td>
		<td><?php echo h($rubyUtiert['RubyDailyreporting']['Short_Over']); ?>&nbsp;</td>
		  <td>
                                                        <?php echo $this->Html->link("<i class='fa fa-edit'></i>",array('controller'=>'ruby_dailyreportings','action'=>'edit/'.$rubyUtiert['RubyDailyreporting']['id']),array('escape' => false,'class'=>'btn default btn-xs red-stripe edit')); ?>
                                                        <?php echo $this->Html->link("<i class='fa fa-trash'></i>",array('controller'=>'ruby_dailyreportings','action'=>'index',$rubyUtiert['RubyDailyreporting']['id']) ,array('escape' => false,'class'=>'btn default btn-xs red-stripe delete')); ?>
                                                    </td>
		
	</tr>
<?php endforeach; } ?>
	</tbody>
	</table>
	</div>
</div>
</div>
	</div>
	</div>
</div>
</div>
</div>


