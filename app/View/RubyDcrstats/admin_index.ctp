<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
<div class="rubyDcrstats index">
	<h3 class="page-title"><?php echo __('Ruby Dcrstats'); ?></h3>
	 <div class="row">
               <?php echo $this->element('table_filter_datepicker'); ?>

		<div class="col-md-3 top-space">
                      <label>Filter by:</label>
					  <div class="input select">
                      <select data-placeholder="Select Tank" class="select_box form-control" tabindex="2" id="tank">
                       
                        <option value="">Select Tank</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                      </select>
					  </div>
                    </div>
		    <div class="col-md-1 middel-space top-space">
                     <label>&nbsp;</label>
                      <button type="button" class="btn btn-block btn-info tank-filter" onclick="filter_utank();">Filter</button>
                    </div>
                    <div class="col-md-1 middel-space top-space">
                     <label>&nbsp;</label>
			<button type="button" class="btn btn-info btn-block dropdown-toggle pull-right action-text tank-filter" data-toggle="dropdown">Action <span class="caret"></span></button>
   		  <ul class="dropdown-menu btn-block icons-right dropdown-menu-right">
        
         		<li><a href="#" target="_blank"><i class="icon-link"></i> Save as pdf</a></li>
        	        <li><a href="#" target="_blank"><i class="icon-link"></i> Save as xls</a></li>
                  </ul>
               </div>  
            </div>
<br>

<div class="row">
<div class="col-md-12">
       <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('dcr_number'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_mop_number'); ?></th>
			<th><?php echo $this->Paginator->sort('number_of_customers'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			
			<th><?php echo $this->Paginator->sort('percentage_of_dcr'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_of_fuel'); ?></th>
			<th><?php echo 'Beginning Date'; ?></th>
			<th><?php echo 'Ending Date'; ?></th>
			
			
	</tr>
	</thead> 
	<tbody>
	<?php foreach ($rubyDcrstats as $rubyDcrstat): ?>
	<tr>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['dcr_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['name']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_mop_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['number_of_customers']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_value']); ?>&nbsp;</td>
		
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_dcr']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_fuel']); ?>&nbsp;</td>
		<td><?php echo CakeTime::format($rubyDcrstat['RubyHeader']['beginning_date_time'], '%m-%d-%Y'); ?></td>
		<td><?php echo CakeTime::format($rubyDcrstat['RubyHeader']['ending_date_time'], '%m-%d-%Y'); ?></td>
		
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	</div>
</div>
</div>
	</div>
</div>
</div> 
</div>
</div>

