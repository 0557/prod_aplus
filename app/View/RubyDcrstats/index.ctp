<div class="rubyDcrstats index">
	<h2><?php echo __('Ruby Dcrstats'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('ruby_header_id'); ?></th>
			<th><?php echo $this->Paginator->sort('dcr_number'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_mop_number'); ?></th>
			<th><?php echo $this->Paginator->sort('number_of_customers'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_fueling _point'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_of_dcr'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_of_fuel'); ?></th>
			<th><?php echo $this->Paginator->sort('create'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyDcrstats as $rubyDcrstat): ?>
	<tr>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyDcrstat['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyDcrstat['RubyHeader']['id'])); ?>
		</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['dcr_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['name']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_mop_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['number_of_customers']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['fuel_value']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['percentage_fueling _point']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_dcr']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_fuel']); ?>&nbsp;</td>
		<td><?php echo h($rubyDcrstat['RubyDcrstat']['create']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rubyDcrstat['RubyDcrstat']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyDcrstat['RubyDcrstat']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyDcrstat['RubyDcrstat']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyDcrstat['RubyDcrstat']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ruby Dcrstat'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
