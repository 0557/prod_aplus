<div class="rubyDcrstats view">
<h2><?php echo __('Ruby Dcrstat'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyDcrstat['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyDcrstat['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dcr Number'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['dcr_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Mop Number'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['fuel_mop_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number Of Customers'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['number_of_customers']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percentage Fueling  Point'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['percentage_fueling _point']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percentage Of Dcr'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_dcr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percentage Of Fuel'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['percentage_of_fuel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyDcrstat['RubyDcrstat']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Dcrstat'), array('action' => 'edit', $rubyDcrstat['RubyDcrstat']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Dcrstat'), array('action' => 'delete', $rubyDcrstat['RubyDcrstat']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyDcrstat['RubyDcrstat']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Dcrstats'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Dcrstat'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
