<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>  
<?php $customer_id = explode(',',$RubyDepartment['RubyDepartment']['customerId']); 
$blue_laws = explode(',',$RubyDepartment['RubyDepartment']['blue_laws']); 
//pr($RubyDepartment); 
?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Department Information - <?php echo $RubyDepartment['RubyDepartment']['name']; ?>
                    </div>
                
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

              

                
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Department Number:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['number']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['name']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Description :  
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['description']; ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Minimum Amount :	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['min_amount']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Maximum Amount :	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['max_amount']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Fee/Charge:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $RubyDepartment['RubyDepartment']['fee']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                           <div class="portlet blue box">
                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Check Customer ID:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('customerId1', array('class' => 'make-switch', 'checked'=>($customer_id[0] == 0)?false:true , 'disabled'=>true ,'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                    <?php echo $this->Form->input('customerId2', array('class' => 'make-switch', 'checked'=>($customer_id[1] == 0)?false:true ,'disabled'=>true ,'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Food Stamp:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('food_stamp', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['food_stamp'] == 0)?false:true ,'disabled'=>true ,'div' => false, 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'required' => 'false')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Is Negative:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('is_negative', array('class' => 'make-switch','checked'=>($RubyDepartment['RubyDepartment']['is_negative'] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Special Discount:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('special_discount', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['special_discount'] == 0)?false:true ,'disabled'=>true ,'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Blue Laws:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('blue_laws1', array('class' => 'make-switch','checked'=>($blue_laws[0] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                    <?php echo $this->Form->input('blue_laws2', array('class' => 'make-switch','checked'=>($blue_laws[1] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Loyalty Redeem:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('loyalty_reddem', array('class' => 'make-switch','checked'=>($RubyDepartment['RubyDepartment']['loyalty_reddem'] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Money Order:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span>
                                    <?php echo $this->Form->input('money_order', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['money_order'] == 0)?false:true ,'disabled'=>true ,'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                </div>

                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Taxable:
                                </div>
                                <div class="col-md-7 value">
                                    <span class="switch-left switch-primary"></span>
                                    <span class="switch-right switch-info "></span
                                     
                                    <?php echo $this->Form->input('tax', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['tax'] == "N")?false:true ,'disabled'=>true ,'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                </div>




                            </div>
                           <?php /*?> <?php if($RubyDepartment['RubyDepartment']['taxable'] == '1')  { ?>
                            <div id="class_extra_taxable_option">
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 1:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax1', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['tax1'] == 0)?false:true ,'disabled'=>true ,'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 2:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax2', array('class' => 'make-switch','checked'=>($RubyDepartment['RubyDepartment']['tax2'] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 3:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax3', array('class' => 'make-switch','checked'=>($RubyDepartment['RubyDepartment']['tax3'] == 0)?false:true ,'disabled'=>true , 'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name">
                                        Tax 4:
                                    </div>
                                    <div class="col-md-7 value">
                                        <span class="switch-left switch-primary"></span>
                                        <span class="switch-right switch-info "></span>
                                        <?php echo $this->Form->input('tax4', array('class' => 'make-switch', 'checked'=>($RubyDepartment['RubyDepartment']['tax4'] == 0)?false:true ,'disabled'=>true ,'data-on' => "primary", 'data-off' => "info", 'type' => 'checkbox', 'label' => false, 'div' => false, 'required' => 'false')); ?>

                                    </div>
                                </div>
                            </div>
                            <?php } ?><?php */?>
                        </div></div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>




                                </div>
                            </div>

                        </div>
                  
           
            </div></div></div>
    <!-- END PAGE CONTENT-->
</div>
</div>