<?php
$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));
?>
				
					
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
						  
			<?php echo $this->Form->create('ruby_deptotals', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
			
	<i class="fa fa-calendar"></i> Update Date <input type="text"  placeholder="Select From Date" id = "from" name ="from" value="<?php if(isset($enddate)) echo date("m-d-Y", strtotime($enddate)); ?>">
	
				 <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Get Report</button>
				
               
						    <?php echo $this->form->end(); ?>
						
							
		</div>
			
	</div>

<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Dashboard
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
																<i class="fa fa-calendar"></i> <?php if(isset($enddate)) echo date("m-d-Y", strtotime($enddate));?>													
							</div>
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
								<th><?php echo $this->Paginator->sort('department_number'); ?></th>
								<th><?php echo $this->Paginator->sort('number_of_trasactions'); ?></th>
								<th><?php echo $this->Paginator->sort('number_items'); ?></th>
								<th><?php echo $this->Paginator->sort('net_sales'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php
						$total_net_sales=0;
						 foreach ($rubyDeptotals as $rubyDeptotal): ?>
						<tr>
							<td><?php echo @$rdepartments[$rubyDeptotal['RubyDeptotal']['department_number']]; ?>&nbsp;</td>
							<td><?php echo h($rubyDeptotal['RubyDeptotal']['number_of_trasactions']); ?>&nbsp;</td>
							<td><?php echo h($rubyDeptotal['RubyDeptotal']['number_items']); ?>&nbsp;</td>
							<td><?php
							$total_net_sales=$total_net_sales+$rubyDeptotal['RubyDeptotal']['net_sales'];						
							echo h($rubyDeptotal['RubyDeptotal']['net_sales']); ?>&nbsp;
                            </td>
							
						</tr>
					<?php endforeach; ?>                   
                    
                        <tr>
                            <td><p style="display:none;">Total Net Sales</p></td>                         
                            <td></td>
                            <td><b>Total Net Sales</b></td>
                            <td><b><?php echo $total_net_sales; ?></b></td>
                        </tr>                   
                    
						</tbody>
						</table>
                     
                        
                     </div>
                           
            </div>
						</div>
						
						
					

<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function filter_utank() {	
		var from_date=$('#from').val();
		var to_date=$('#to').val();
		
		var department_number=$('#department_number').val();
		//$('#loaderdiv').show();
		$.ajax({
			type : 'POST',
			url : webroot+'ruby_deptotals/index/',
			dataType : 'html',
			data: {from_date:from_date,to_date:to_date,department_number:department_number},
			success:function(result){
				//$("#example_info").hide();example_filter
				//$("#example_paginate").hide();
				//$("#example_filter").hide();
				//$("#example_length").hide();
				//$(".col-md-2").hide();
				$("#filter").show();
				$("#example").html(result);
			}});
	}
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable();
		//$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		//$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});
	});
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#from" ).datepicker({ dateFormat: 'mm-dd-yy' });
  } );
  </script>