<div class="rubyDeptotals form">
<?php echo $this->Form->create('RubyDeptotal'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Ruby Deptotal'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('store_id');
		echo $this->Form->input('beginning_date_time');
		echo $this->Form->input('ending_date_time');
		echo $this->Form->input('department_number');
		echo $this->Form->input('number_of_trasactions');
		echo $this->Form->input('net_sales');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyDeptotal.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyDeptotal.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Deptotals'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
