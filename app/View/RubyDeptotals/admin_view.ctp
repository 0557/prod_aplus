<div class="rubyDeptotals view">
<h2><?php echo __('Ruby Deptotal'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyDeptotal['Store']['name'], array('controller' => 'stores', 'action' => 'view', $rubyDeptotal['Store']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Beginning Date Time'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['beginning_date_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ending Date Time'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['ending_date_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Department Number'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['department_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number Of Trasactions'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['number_of_trasactions']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Net Sales'); ?></dt>
		<dd>
			<?php echo h($rubyDeptotal['RubyDeptotal']['net_sales']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Deptotal'), array('action' => 'edit', $rubyDeptotal['RubyDeptotal']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Deptotal'), array('action' => 'delete', $rubyDeptotal['RubyDeptotal']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyDeptotal['RubyDeptotal']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Deptotals'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Deptotal'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
