
<div class="col-md-12">
    <div class="block-inner clearfix tooltip-examples">
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example1">
	<thead>
	<tr>
		  	<th class="sorting_asc"><?php echo $this->Paginator->sort('beginning_date_time'); ?></th>
			<th class="sorting_asc"><?php echo $this->Paginator->sort('ending_date_time'); ?></th>
			<th class="sorting_asc" ><?php echo $this->Paginator->sort('department_number'); ?></th>
			<th class="sorting_asc"><?php echo $this->Paginator->sort('number_of_trasactions'); ?></th>
			<th class="sorting_asc"><?php echo $this->Paginator->sort('number_items'); ?></th>
			<th class="sorting_asc"><?php echo $this->Paginator->sort('net_sales'); ?></th>
	</tr>
	</thead>
	    <tbody>
		<?php foreach ($rubyDeptotals as $rubyDeptotal): ?>
		<tr>
			<td><?php echo h($rubyDeptotal['RubyDeptotal']['beginning_date_time']); ?>&nbsp;</td>
			<td><?php echo h($rubyDeptotal['RubyDeptotal']['ending_date_time']); ?>&nbsp;</td>
			<td><?php echo @$rdepartments[$rubyDeptotal['RubyDeptotal']['department_number']]; ?>&nbsp;</td>
			<td><?php echo h($rubyDeptotal['RubyDeptotal']['number_of_trasactions']); ?>&nbsp;</td>
			<td><?php echo h($rubyDeptotal['RubyDeptotal']['number_items']); ?>&nbsp;</td>
			<td><?php echo h($rubyDeptotal['RubyDeptotal']['net_sales']); ?>&nbsp;</td>			
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	</div>

</div>
</div>


