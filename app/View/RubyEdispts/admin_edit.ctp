<div class="rubyEdispts form">
<?php echo $this->Form->create('RubyEdispt'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Ruby Edispt'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('name');
		echo $this->Form->input('fueling_point_number');
		echo $this->Form->input('fuel_product_number');
		echo $this->Form->input('fuel_volume');
		echo $this->Form->input('fuel_value');
		echo $this->Form->input('fuel_pointing_status');
		echo $this->Form->input('fuel_product_id');
		echo $this->Form->input('create');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyEdispt.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyEdispt.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Edispts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fuel Products'), array('controller' => 'fuel_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fuel Product'), array('controller' => 'fuel_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
