<?php
//$tankList = array(1 => 'REG', 2 => 'PREM', 3 => 'DIESEL', 4 => 'UNLD2');
$prodList = ClassRegistry::init('RubyFprod')->find('list', array('conditions' => array('store_id' => $_SESSION['store_id']), 'fields' => array('fuel_product_number', 'fuel_product_name')));

//print_r($prodList);exit;
$fMopList = array(1 => 'CASH', 2 => 'CRED', 3 => 'CHEC');


?>
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
<div class="rubyEdispts index">
	<h3 class="page-title"><?php echo __('Ruby Edispts'); ?></h3>
	 <div class="row">
               <?php echo $this->element('table_filter_datepicker'); ?>

		<div class="col-md-3 top-space">
                      <label>Filter by:</label>
					  <div class="input select">
                      <select data-placeholder="Select Tank" class="select_box form-control" tabindex="2" id="tank">
                       
                        <option value="">Select Tank</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                      </select>
                    </div>
                    </div>
		    <div class="col-md-1 middel-space top-space">
                     <label>&nbsp;</label>
                      <button type="button" class="btn btn-block btn-info tank-filter" onclick="filter_utank();">Filter</button>
                    </div>
                    <div class="col-md-1 middel-space top-space">
                     <label>&nbsp;</label>
			<button type="button" class="btn btn-info btn-block dropdown-toggle pull-right action-text tank-filter" data-toggle="dropdown">Action <span class="caret"></span></button>
   		  <ul class="dropdown-menu btn-block icons-right dropdown-menu-right">
        
         		<li><a href="#" target="_blank"><i class="icon-link"></i> Save as pdf</a></li>
        	        <li><a href="#" target="_blank"><i class="icon-link"></i> Save as xls</a></li>
                  </ul>
               </div>  
            </div>
<br>

<div class="row">
<div class="col-md-12">
       <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('fueling_point_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_pointing_status'); ?></th>
			
			<th><?php echo 'Beginning Date'; ?></th>
			<th><?php echo 'Ending Date'; ?></th>
			
			
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyEdispts as $rubyEdispt): ?>
	<tr>
		<td><?php echo h($rubyEdispt['RubyEdispt']['name']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fueling_point_number']); ?>&nbsp;</td>
		<td><?php echo @$prodList[$rubyEdispt['RubyEdispt']['fuel_product_number']]; ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_value']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_pointing_status']); ?>&nbsp;</td>
		
		<td><?php echo CakeTime::format($rubyEdispt['RubyHeader']['beginning_date_time'], '%m-%d-%Y'); ?></td>
		<td><?php echo CakeTime::format($rubyEdispt['RubyHeader']['ending_date_time'], '%m-%d-%Y'); ?></td>
		
		
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	</div>
</div>
</div>
	</div>
</div>
</div>
</div>
</div>


