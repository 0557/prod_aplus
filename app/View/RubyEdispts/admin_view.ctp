<div class="rubyEdispts view">
<h2><?php echo __('Ruby Edispt'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyEdispt['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyEdispt['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fueling Point Number'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['fueling_point_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Number'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['fuel_product_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Pointing Status'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['fuel_pointing_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyEdispt['FuelProduct']['id'], array('controller' => 'fuel_products', 'action' => 'view', $rubyEdispt['FuelProduct']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyEdispt['RubyEdispt']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Edispt'), array('action' => 'edit', $rubyEdispt['RubyEdispt']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Edispt'), array('action' => 'delete', $rubyEdispt['RubyEdispt']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyEdispt['RubyEdispt']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Edispts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Edispt'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fuel Products'), array('controller' => 'fuel_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fuel Product'), array('controller' => 'fuel_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
