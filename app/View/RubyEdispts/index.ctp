<div class="rubyEdispts index">
	<h2><?php echo __('Ruby Edispts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('ruby_header_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('fueling_point_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_pointing_status'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('create'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyEdispts as $rubyEdispt): ?>
	<tr>
		<td><?php echo h($rubyEdispt['RubyEdispt']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyEdispt['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyEdispt['RubyHeader']['id'])); ?>
		</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['name']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fueling_point_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_product_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_value']); ?>&nbsp;</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['fuel_pointing_status']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyEdispt['FuelProduct']['id'], array('controller' => 'fuel_products', 'action' => 'view', $rubyEdispt['FuelProduct']['id'])); ?>
		</td>
		<td><?php echo h($rubyEdispt['RubyEdispt']['create']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rubyEdispt['RubyEdispt']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyEdispt['RubyEdispt']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyEdispt['RubyEdispt']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyEdispt['RubyEdispt']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ruby Edispt'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fuel Products'), array('controller' => 'fuel_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fuel Product'), array('controller' => 'fuel_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
