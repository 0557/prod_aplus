  <li class="<?php echo (($this->params['controller'] ==  'ruby_uprodts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bar-chart'></i>Sales by MOP", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utankts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-pie-chart'></i>Sale By Tank No", array('controller' => 'ruby_utankts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<!--<li class="<?php echo (($this->params['controller'] ==  'ruby_uautots')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-line-chart'></i>Sales by MOP", array('controller' => 'ruby_uautots', 'action' => 'index'), array('escape' => false)); ?>
		</li>-->
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uhosets')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Sales HOSC", array('controller' => 'ruby_uhosets', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_userlvts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-calculator'></i>Sale By Service Level", array('controller' => 'ruby_userlvts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utierts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-clipboard'></i>Sale By Tier Product", array('controller' => 'ruby_utierts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_edispts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-o'></i>Dispenser Sales", array('controller' => 'ruby_edispts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dcrstats')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-large'></i>Sale By DCR No", array('controller' => 'ruby_dcrstats', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_deptotals')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-list'></i>Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dailyreportings')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-columns'></i>Daily Reporting", array('controller' => 'ruby_dailyreportings', 'action' => 'index'), array('escape' => false)); ?>
		</li>