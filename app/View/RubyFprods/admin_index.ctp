<div class="page-content-wrapper">
    <div class="page-content">
<div class="rubyEdispts index">
	<h3 class="page-title"><?php echo __('Ruby Fprods'); ?>  <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>Add New</span>', array('action' => 'admin_add'), array('escape' => false,'class'=>'btn green fileinput-button pull-right')); ?> </h3>
	  
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('fuel_product_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_name'); ?></th>
			<th><?php echo $this->Paginator->sort('display_name'); ?></th>
			<th><?php echo $this->Paginator->sort('Sequence No#'); ?></th>
			<th><?php echo $this->Paginator->sort('Status'); ?></th>
			
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyFprods as $rubyFprod): ?>
	<tr>
		<td><?php echo h($rubyFprod['RubyFprod']['fuel_product_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['fuel_product_name']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['display_name']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['sequence']); ?>&nbsp;</td>
		<td><?php if($rubyFprod['RubyFprod']['status']==1) echo 'Active'; else echo 'Deactive';
			
		?>&nbsp;</td>
		
		<td><?php echo h($rubyFprod['RubyFprod']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyFprod['RubyFprod']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyFprod['RubyFprod']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFprod['RubyFprod']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
</div>
