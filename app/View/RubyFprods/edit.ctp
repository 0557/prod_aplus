<div class="rubyFprods form">
<?php echo $this->Form->create('RubyFprod'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ruby Fprod'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('fuel_product_number');
		echo $this->Form->input('fuel_product_name');
		echo $this->Form->input('first_tank');
		echo $this->Form->input('percentage_blend');
		echo $this->Form->input('percentage_blend_2nd_tank');
		echo $this->Form->input('department_number');
		echo $this->Form->input('network_product_code');
		echo $this->Form->input('fuel_product_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyFprod.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyFprod.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Fprods'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
