<div class="rubyFprods index">
	<h2><?php echo __('Ruby Fprods'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('ruby_header_id'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_name'); ?></th>
			<th><?php echo $this->Paginator->sort('first_tank'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_blend'); ?></th>
			<th><?php echo $this->Paginator->sort('percentage_blend_2nd_tank'); ?></th>
			<th><?php echo $this->Paginator->sort('department_number'); ?></th>
			<th><?php echo $this->Paginator->sort('network_product_code'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_product_code'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyFprods as $rubyFprod): ?>
	<tr>
		<td><?php echo h($rubyFprod['RubyFprod']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyFprod['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyFprod['RubyHeader']['id'])); ?>
		</td>
		<td><?php echo h($rubyFprod['RubyFprod']['fuel_product_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['fuel_product_name']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['first_tank']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['percentage_blend']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['percentage_blend_2nd_tank']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['department_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['network_product_code']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['fuel_product_code']); ?>&nbsp;</td>
		<td><?php echo h($rubyFprod['RubyFprod']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rubyFprod['RubyFprod']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyFprod['RubyFprod']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyFprod['RubyFprod']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFprod['RubyFprod']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
