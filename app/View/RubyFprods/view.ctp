<div class="rubyFprods view">
<h2><?php echo __('Ruby Fprod'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyFprod['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyFprod['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Number'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['fuel_product_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Name'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['fuel_product_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Tank'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['first_tank']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percentage Blend'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['percentage_blend']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percentage Blend 2nd Tank'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['percentage_blend_2nd_tank']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Department Number'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['department_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Network Product Code'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['network_product_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Code'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['fuel_product_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rubyFprod['RubyFprod']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Fprod'), array('action' => 'edit', $rubyFprod['RubyFprod']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Fprod'), array('action' => 'delete', $rubyFprod['RubyFprod']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFprod['RubyFprod']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Fprods'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
