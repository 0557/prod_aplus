<div class="rubyFservlevs form">
<?php echo $this->Form->create('RubyFservlev'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Ruby Fservlev'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('service_level_number');
		echo $this->Form->input('service_level_name');
		echo $this->Form->input('create');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyFservlev.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyFservlev.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Fservlevs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
