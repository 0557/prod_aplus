<div class="rubyFservlevs index">
	<h2><?php echo __('Ruby Fservlevs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('ruby_header_id'); ?></th>
			<th><?php echo $this->Paginator->sort('service_level_number'); ?></th>
			<th><?php echo $this->Paginator->sort('service_level_name'); ?></th>
			<th><?php echo $this->Paginator->sort('create'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyFservlevs as $rubyFservlev): ?>
	<tr>
		<td><?php echo h($rubyFservlev['RubyFservlev']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyFservlev['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyFservlev['RubyHeader']['id'])); ?>
		</td>
		<td><?php echo h($rubyFservlev['RubyFservlev']['service_level_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyFservlev['RubyFservlev']['service_level_name']); ?>&nbsp;</td>
		<td><?php echo h($rubyFservlev['RubyFservlev']['create']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rubyFservlev['RubyFservlev']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyFservlev['RubyFservlev']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyFservlev['RubyFservlev']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFservlev['RubyFservlev']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ruby Fservlev'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
