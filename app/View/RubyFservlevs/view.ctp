<div class="rubyFservlevs view">
<h2><?php echo __('Ruby Fservlev'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyFservlev['RubyFservlev']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyFservlev['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyFservlev['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service Level Number'); ?></dt>
		<dd>
			<?php echo h($rubyFservlev['RubyFservlev']['service_level_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service Level Name'); ?></dt>
		<dd>
			<?php echo h($rubyFservlev['RubyFservlev']['service_level_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyFservlev['RubyFservlev']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Fservlev'), array('action' => 'edit', $rubyFservlev['RubyFservlev']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Fservlev'), array('action' => 'delete', $rubyFservlev['RubyFservlev']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFservlev['RubyFservlev']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Fservlevs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Fservlev'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
