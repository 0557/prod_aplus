<div class="rubyFtanks form">
<?php echo $this->Form->create('RubyFtank'); ?>
	<fieldset>
		<legend><?php echo __('Add Ruby Ftank'); ?></legend>
	<?php
		echo $this->Form->input('store_id');
		echo $this->Form->input('name');
		echo $this->Form->input('tank_number');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ruby Ftanks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
