<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Ftank List <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo $this->Paginator->sort('number', 'Tank Number'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name', 'Tank Name'); ?></th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($rubyFtanks) && !empty($rubyFtanks)) { ?>
                                            <?php foreach ($rubyFtanks as $data) {
											//print_r($data);die;
											?>
                                                <tr>
                                                    <td><?php echo h($data['RubyFtank']['tank_number']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['RubyFtank']['name']); ?>&nbsp;</td>
                                                    
                                         
                                                    <td>                    
                                                    <?php 
													echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('action' => 'edit', $data['RubyFtank']['id']), array('escape' => false,'class' => 'newicon red-stripe edit'));
													?>
													<?php 
                                                    echo $this->Form->postLink(__('<img src="'.Router::url('/').'img/delete.png"/>'),array('action' => 'delete', $data['RubyFtank']['id']),array('escape' => false,'class' => 'newicon red-stripe delete','confirm' => __('Are you sure you want to delete this tank?')));
                                                    ?>                                            
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    </div>
</div>

