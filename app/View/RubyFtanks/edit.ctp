<div class="rubyFtanks form">
<?php echo $this->Form->create('RubyFtank'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ruby Ftank'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('store_id');
		echo $this->Form->input('name');
		echo $this->Form->input('tank_number');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyFtank.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyFtank.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Ftanks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
