<table class="table table-striped table-bordered table-advance table-hover" id="example1" >
	<thead>
	<tr>

			<th><?php echo $this->Paginator->sort('tank_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			<th><?php echo 'Beginning Date Time'; ?></th>
			<th><?php echo 'Ending Date Time'; ?></th>
		
		
			
			
			
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyUtankt as $rubyUtankt): ?>
	<tr>
		
		<td><?php echo $rubyUtankt['RubyUtankt']['tank_number']; ?>&nbsp;</td>
		<td><?php echo $rubyUtankt['RubyUtankt']['fuel_volume']; ?>&nbsp;</td>
		<td>$<?php echo $rubyUtankt['RubyUtankt']['fuel_value']; ?>&nbsp;</td>
		<td><?php  echo  date('m-d-Y', strtotime($rubyUtankt['RubyHeader']['beginning_date_time'])); ?></td>
		<td><?php echo  date('m-d-Y', strtotime($rubyUtankt['RubyHeader']['ending_date_time'])); ?></td>
		
		
		
	
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example1').DataTable();
           
	} );
  
</script>
