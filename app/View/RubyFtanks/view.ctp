<div class="rubyFtanks view">
<h2><?php echo __('Ruby Ftank'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyFtank['RubyFtank']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyFtank['Store']['name'], array('controller' => 'stores', 'action' => 'view', $rubyFtank['Store']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyFtank['RubyFtank']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tank Number'); ?></dt>
		<dd>
			<?php echo h($rubyFtank['RubyFtank']['tank_number']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Ftank'), array('action' => 'edit', $rubyFtank['RubyFtank']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Ftank'), array('action' => 'delete', $rubyFtank['RubyFtank']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFtank['RubyFtank']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Ftanks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Ftank'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
	</ul>
</div>
