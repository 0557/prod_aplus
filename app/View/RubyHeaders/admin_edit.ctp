<div class="rubyHeaders form">
<?php echo $this->Form->create('RubyHeader'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Ruby Header'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('store_id');
		echo $this->Form->input('header');
		echo $this->Form->input('beginning_date_time');
		echo $this->Form->input('ending_date_time');
		echo $this->Form->input('store_number');
		echo $this->Form->input('sequence_number');
		echo $this->Form->input('operator_id');
		echo $this->Form->input('keyboard_number');
		echo $this->Form->input('requested_period_number');
		echo $this->Form->input('requested_set_number');
		echo $this->Form->input('next_lower_period_b');
		echo $this->Form->input('next_lower_period_e');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyHeader.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyHeader.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Fprods'), array('controller' => 'ruby_fprods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('controller' => 'ruby_fprods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uprodts'), array('controller' => 'ruby_uprodts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uprodt'), array('controller' => 'ruby_uprodts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Utankts'), array('controller' => 'ruby_utankts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Utankt'), array('controller' => 'ruby_utankts', 'action' => 'add')); ?> </li>
	</ul>
</div>
