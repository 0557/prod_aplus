<div class="rubyHeaders index">
	<h2><?php echo __('Ruby Headers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('store_id'); ?></th>
			<th><?php echo $this->Paginator->sort('header'); ?></th>
			<th><?php echo $this->Paginator->sort('beginning_date_time'); ?></th>
			<th><?php echo $this->Paginator->sort('ending_date_time'); ?></th>
			<th><?php echo $this->Paginator->sort('store_number'); ?></th>
			<th><?php echo $this->Paginator->sort('sequence_number'); ?></th>
			<th><?php echo $this->Paginator->sort('operator_id'); ?></th>
			<th><?php echo $this->Paginator->sort('keyboard_number'); ?></th>
			<th><?php echo $this->Paginator->sort('requested_period_number'); ?></th>
			<th><?php echo $this->Paginator->sort('requested_set_number'); ?></th>
			<th><?php echo $this->Paginator->sort('next_lower_period_b'); ?></th>
			<th><?php echo $this->Paginator->sort('next_lower_period_e'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyHeaders as $rubyHeader): ?>
	<tr>
		<td><?php echo h($rubyHeader['RubyHeader']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($rubyHeader['Store']['name'], array('controller' => 'stores', 'action' => 'view', $rubyHeader['Store']['id'])); ?>
		</td>
		<td><?php echo h($rubyHeader['RubyHeader']['header']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['beginning_date_time']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['ending_date_time']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['store_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['sequence_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['operator_id']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['keyboard_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['requested_period_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['requested_set_number']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['next_lower_period_b']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['next_lower_period_e']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['created']); ?>&nbsp;</td>
		<td><?php echo h($rubyHeader['RubyHeader']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $rubyHeader['RubyHeader']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rubyHeader['RubyHeader']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rubyHeader['RubyHeader']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyHeader['RubyHeader']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Fprods'), array('controller' => 'ruby_fprods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('controller' => 'ruby_fprods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uprodts'), array('controller' => 'ruby_uprodts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uprodt'), array('controller' => 'ruby_uprodts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Utankts'), array('controller' => 'ruby_utankts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Utankt'), array('controller' => 'ruby_utankts', 'action' => 'add')); ?> </li>
	</ul>
</div>
