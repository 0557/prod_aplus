<div class="rubyHeaders view">
<h2><?php echo __('Ruby Header'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyHeader['Store']['name'], array('controller' => 'stores', 'action' => 'view', $rubyHeader['Store']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Header'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['header']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Beginning Date Time'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['beginning_date_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ending Date Time'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['ending_date_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store Number'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['store_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sequence Number'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['sequence_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Operator Id'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['operator_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Keyboard Number'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['keyboard_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requested Period Number'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['requested_period_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requested Set Number'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['requested_set_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Next Lower Period B'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['next_lower_period_b']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Next Lower Period E'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['next_lower_period_e']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($rubyHeader['RubyHeader']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Header'), array('action' => 'edit', $rubyHeader['RubyHeader']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Header'), array('action' => 'delete', $rubyHeader['RubyHeader']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyHeader['RubyHeader']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores'), array('controller' => 'stores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Store'), array('controller' => 'stores', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Fprods'), array('controller' => 'ruby_fprods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('controller' => 'ruby_fprods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uprodts'), array('controller' => 'ruby_uprodts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uprodt'), array('controller' => 'ruby_uprodts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Utankts'), array('controller' => 'ruby_utankts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Utankt'), array('controller' => 'ruby_utankts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Ruby Fprods'); ?></h3>
	<?php if (!empty($rubyHeader['RubyFprod'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Ruby Header Id'); ?></th>
		<th><?php echo __('Fuel Product Number'); ?></th>
		<th><?php echo __('Fuel Product Name'); ?></th>
		<th><?php echo __('First Tank'); ?></th>
		<th><?php echo __('Percentage Blend'); ?></th>
		<th><?php echo __('Percentage Blend 2nd Tank'); ?></th>
		<th><?php echo __('Department Number'); ?></th>
		<th><?php echo __('Network Product Code'); ?></th>
		<th><?php echo __('Fuel Product Code'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($rubyHeader['RubyFprod'] as $rubyFprod): ?>
		<tr>
			<td><?php echo $rubyFprod['id']; ?></td>
			<td><?php echo $rubyFprod['ruby_header_id']; ?></td>
			<td><?php echo $rubyFprod['fuel_product_number']; ?></td>
			<td><?php echo $rubyFprod['fuel_product_name']; ?></td>
			<td><?php echo $rubyFprod['first_tank']; ?></td>
			<td><?php echo $rubyFprod['percentage_blend']; ?></td>
			<td><?php echo $rubyFprod['percentage_blend_2nd_tank']; ?></td>
			<td><?php echo $rubyFprod['department_number']; ?></td>
			<td><?php echo $rubyFprod['network_product_code']; ?></td>
			<td><?php echo $rubyFprod['fuel_product_code']; ?></td>
			<td><?php echo $rubyFprod['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ruby_fprods', 'action' => 'view', $rubyFprod['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ruby_fprods', 'action' => 'edit', $rubyFprod['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ruby_fprods', 'action' => 'delete', $rubyFprod['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyFprod['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ruby Fprod'), array('controller' => 'ruby_fprods', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Ruby Uprodts'); ?></h3>
	<?php if (!empty($rubyHeader['RubyUprodt'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Ruby Header Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Fuel Product Number'); ?></th>
		<th><?php echo __('Fmop1'); ?></th>
		<th><?php echo __('Fuel Volume'); ?></th>
		<th><?php echo __('Fuel Value'); ?></th>
		<th><?php echo __('Fprodid'); ?></th>
		<th><?php echo __('Fpricemop0'); ?></th>
		<th><?php echo __('Fpricemop1'); ?></th>
		<th><?php echo __('Fpricemop2'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($rubyHeader['RubyUprodt'] as $rubyUprodt): ?>
		<tr>
			<td><?php echo $rubyUprodt['id']; ?></td>
			<td><?php echo $rubyUprodt['ruby_header_id']; ?></td>
			<td><?php echo $rubyUprodt['name']; ?></td>
			<td><?php echo $rubyUprodt['fuel_product_number']; ?></td>
			<td><?php echo $rubyUprodt['fmop1']; ?></td>
			<td><?php echo $rubyUprodt['fuel_volume']; ?></td>
			<td><?php echo $rubyUprodt['fuel_value']; ?></td>
			<td><?php echo $rubyUprodt['fprodid']; ?></td>
			<td><?php echo $rubyUprodt['fpricemop0']; ?></td>
			<td><?php echo $rubyUprodt['fpricemop1']; ?></td>
			<td><?php echo $rubyUprodt['fpricemop2']; ?></td>
			<td><?php echo $rubyUprodt['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ruby_uprodts', 'action' => 'view', $rubyUprodt['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ruby_uprodts', 'action' => 'edit', $rubyUprodt['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ruby_uprodts', 'action' => 'delete', $rubyUprodt['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUprodt['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ruby Uprodt'), array('controller' => 'ruby_uprodts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Ruby Utankts'); ?></h3>
	<?php if (!empty($rubyHeader['RubyUtankt'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Ruby Header Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Tank Number'); ?></th>
		<th><?php echo __('Fuel Volume'); ?></th>
		<th><?php echo __('Fuel Value'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($rubyHeader['RubyUtankt'] as $rubyUtankt): ?>
		<tr>
			<td><?php echo $rubyUtankt['id']; ?></td>
			<td><?php echo $rubyUtankt['ruby_header_id']; ?></td>
			<td><?php echo $rubyUtankt['name']; ?></td>
			<td><?php echo $rubyUtankt['tank_number']; ?></td>
			<td><?php echo $rubyUtankt['fuel_volume']; ?></td>
			<td><?php echo $rubyUtankt['fuel_value']; ?></td>
			<td><?php echo $rubyUtankt['created']; ?></td>
			<td><?php echo $rubyUtankt['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ruby_utankts', 'action' => 'view', $rubyUtankt['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ruby_utankts', 'action' => 'edit', $rubyUtankt['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ruby_utankts', 'action' => 'delete', $rubyUtankt['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUtankt['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ruby Utankt'), array('controller' => 'ruby_utankts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
