<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers','admin/bootstrap-fileinput')); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker')); ?>
<?php echo $this->Html->script(array('admin/bootstrap-switch.min')); ?>
<?php echo $this->Html->css(array('admin/bootstrap-switch.min')); ?>     

<?php echo $this->Form->create('RubyMop', array('role' => 'form', 'action' => 'edit', 'method' => 'post')); ?>
<?php // pr($this->request->data); die; ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">
        <div class="col-md-12 ">            
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Mop
                    </div>
                </div></div></div>

        <div class="form-body">
           

                <div class="col-md-8 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">                           
                            
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Mop Number:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('number', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Mop Number ')); ?>
                                </div>
                            </div>
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Mop Name :			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Mop Name ')); ?>
                                </div>
                            </div>
                            
                                   
                            
                            <div class="row static-info">
                                <div class="form-actions" style="text-align:center;">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="reset" class="btn default">Reset</button>
                                    <button type="button" onclick="javascript:history.back(1)"
                                    class="btn default">Cancel</button>
                                </div>
                            </div> 
                             

                        </div>
                    </div>
                </div>
                
           


        </div>
        

    </div>
    <?php echo $this->form->end(); ?>

</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        // initiate layout and plugins
        //App.init();
        ComponentsPickers.init();
       
    });
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
    $("select").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
</script>


