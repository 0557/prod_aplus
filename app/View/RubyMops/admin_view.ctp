<div class="rubyMops view">
<h2><?php echo __('Ruby Mop'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyMop['RubyMop']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyMop['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyMop['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Mop Number'); ?></dt>
		<dd>
			<?php echo h($rubyMop['RubyMop']['fuel_mop_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Mop Name'); ?></dt>
		<dd>
			<?php echo h($rubyMop['RubyMop']['fuel_mop_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyMop['RubyMop']['Create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Mop'), array('action' => 'edit', $rubyMop['RubyMop']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Mop'), array('action' => 'delete', $rubyMop['RubyMop']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyMop['RubyMop']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Mops'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Mop'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
