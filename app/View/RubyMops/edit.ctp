<div class="rubyMops form">
<?php echo $this->Form->create('RubyMop'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ruby Mop'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('fuel_mop_number');
		echo $this->Form->input('fuel_mop_name');
		echo $this->Form->input('Create');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyMop.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyMop.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Mops'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
