<?php
$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$description =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'description'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$depart =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'r_grocery_department_id'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$salesinventory =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'current_inventory'),'order'=>array('id'=>'desc'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

$purchase =  ClassRegistry::init('RGroceryItem')->find('list', array('fields' => array('plu_no', 'purchase'),'order'=>array('id'=>'desc'),'conditions'=>array('store_id'=>$this->Session->read('stores_id'))));

?> 
		
			
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-body col-md-12">
			 <form method="post" action=""> 
			
	 <div class=" col-md-4">
			<?php include('test.ctp'); ?>
	</div>
		<!-- <div class=" col-md-4">
			<?php echo $this->Form->input('department', array('id' => 'department', 'class' => 'form-control','type' => 'select', 'options'=>$rdepartments,'empty'=>'All Departments','label' => false,'required' => false)); ?>

	</div>-->

	<div class=" col-md-4">
				 <button name="getdata" class="btn btn-success" type="submit"><i class="fa fa-arrow-left fa-fw"></i> Get Report</button>
				 <?php if(isset($enddate1)) { ?>	

<input type="button" class="btn btn-warning" onclick="tableToExcel('example', 'W3C Example Table')" value="Export to Excel">
														<?php } ?>	
				
		</div>
				
               
						    <?php echo $this->form->end(); ?>
						
							
		</div>
					<!-- END SAMPLE TABLE PORTLET-->
	</div>

<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Sales Summary Report
							</div>
							<!--	<div class="tools">
								<a href="javascript:;" class="collapse">
								</a> </div>	
							-->
<div class="caption pull-right">
																<i class="fa fa-calendar"></i> <?php if(isset($enddate)) echo date("m-d-Y", strtotime($enddate));?>	<?php if(isset($enddate1)) echo $enddate1;?>
													
							</div>
						</div>
						<div class="portlet-body">
							
                     
					
                            <div class="datatable">
                             		<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('Department'); ?></th>
							<th><?php echo $this->Paginator->sort('PLU No'); ?></th>
							<th><?php echo $this->Paginator->sort('Description'); ?></th>
							<th><?php echo $this->Paginator->sort('Items Sold'); ?></th>
							<th><?php echo $this->Paginator->sort('Starting Invnetory'); ?></th>
							<th><?php echo $this->Paginator->sort('Purchase'); ?></th>
					
								
						</tr>
						</thead>
						<tbody>
						<?php $i=1;
						if(isset($pluttls)){
		foreach ($pluttls as $rubyDeptotal): 
						 ?>
						<tr>
						<td><?php echo str_replace(' ','',@$rdepartments[@$depart[$rubyDeptotal['RubyPluttl']['plu_no']]]); ?>&nbsp;</td>
							<td><?php echo h($rubyDeptotal['RubyPluttl']['plu_no']); ?>&nbsp;</td>
								<td><?php echo @$description[$rubyDeptotal['RubyPluttl']['plu_no']]; ?>&nbsp;</td>
								<td><?php echo h($rubyDeptotal[0]['no_of_items_sold']); ?>&nbsp;</td>
							<td><?php echo @$salesinventory[$rubyDeptotal['RubyPluttl']['plu_no']]; ?>&nbsp;</td>
							<td><?php echo @$purchase[$rubyDeptotal['RubyPluttl']['plu_no']]; ?>&nbsp;</td>
							
							
							
							
						</tr>
					<?php  $i++; endforeach;
}?>
						</tbody>
						</table>
                            </div>
                           
            </div>
						</div>
						
						
						<?php
$rdepartments =  ClassRegistry::init('RubyDepartment')->find('list', array('fields' => array('number', 'name')));
?>

<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function filter_utank() {	
		var from_date=$('#from').val();
		var to_date=$('#to').val();
		
		var department_number=$('#department_number').val();
		//$('#loaderdiv').show();
		$.ajax({
			type : 'POST',
			url : webroot+'ruby_deptotals/index/',
			dataType : 'html',
			data: {from_date:from_date,to_date:to_date,department_number:department_number},
			success:function(result){
				//$("#example_info").hide();example_filter
				//$("#example_paginate").hide();
				//$("#example_filter").hide();
				//$("#example_length").hide();
				//$(".col-md-2").hide();
				$("#filter").show();
				$("#example").html(result);
			}});
	}
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": false,
	   aaSorting: [[5, 'desc']]
	  });
		$('#from').Zebra_DatePicker({direction: -1,format:"m-d-Y",pair: $('#to')});
		$('#to').Zebra_DatePicker({direction: 1,format:"m-d-Y"});
		
		
		    
	});
</script>
<style>
#example th,#example td{
width:100px!important;}
body{
font-size:12px;}
</style>

<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
</script>
