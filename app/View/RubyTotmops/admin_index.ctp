<div class="page-content-wrapper">
<div class="portlet box blue">
     <div class="page-content portlet-body" > 
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                    Method Of Payments
                </h3>
            
            </div>
        </div>
		<?php echo $this->Form->create('revenue_sales', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
		<div class="row">
         <div class="col-md-12">
            <div class=" col-md-2">Update Date</div>
            <div class="col-md-3">
           		 <input type="text"  placeholder="Select End Date" class="form-control icon_position" id="end_date" name="end_date" value="<?php echo $end_date;?> ">  
            </div>
            <div class=" col-md-1">
            	<button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <div class=" col-md-4">
           		 <a href="<?php echo Router::url('/');?>admin/ruby_totmops/reset" class="btn btn-warning ">Reset</a> 
            </div> 
            </div>
		</div>
<?php echo $this->form->end(); ?>	       
         
         <div class="row">
            <div class="col-md-12">               
                <h3 class="page-title">
                    Method Of Payments			
                </h3>
            
            </div>
        </div>      
         <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">
<div id="loading-image" style="display:none">
           <h3 style="text-align:center"> Loading...</h3>
            </div>
                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover" id="getdata">
                                
                                <?php 										
										//pr($RubyTotmop);die;										
										if (isset($RubyTotmop[0]) && ($RubyTotmop[0]['RubyTotmop']['id']!='')) { ?>
                                    <thead>
                                        <tr>
                                        
                                     <th class="header-style">Method Of Payment</th>
                                     <th class="txtrht header-style">Number Of Sales</th>
                                     <th class="txtrht header-style">Value Of Sales</th>     
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    
                                    <?php
                                    foreach($RubyTotmop as $RubyTotmop_row){
									?>                                 
                                               
                                  <tr>
                                        <td><?php echo $RubyTotmop_row['RubyTotmop']['mop_name']; ?>&nbsp;</td>
                                         <td class="txtrht"><?php echo h(number_format((float)($RubyTotmop_row['RubyTotmop']['number_of_sales']), 2, '.', '')); ?>&nbsp;</td>  
                                         <td class="txtrht">$<?php echo h(number_format((float)($RubyTotmop_row['RubyTotmop']['value_of_sales']), 2, '.', '')); ?>&nbsp;</td>                                            
                                  </tr>
                                              
                                 <?php }?>
                                  
                                  
                                       
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="2">No results found!</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                
                                    
                                </table>
                            </div>
                           
                                
                            </div>
                    
                    </div>
                </div>
            </div>
          
        </div>
        
    
   
      
    </div>
</div>


</div>


<style>

.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }

button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
.txtrht{
	text-align:right !important;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "bPaginate": false
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
		//$('#end_date').Zebra_DatePicker({format:"Y-m-d"});
	});
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });	
  } );
  </script>