<?php
//$tankList = array(1 => 'REG', 2 => 'PREM', 3 => 'DIESEL', 4 => 'UNLD2');
$prodList = ClassRegistry::init('RubyFprod')->find('list', array('conditions' => array('store_id' => $_SESSION['store_id']), 'fields' => array('fuel_product_number', 'fuel_product_name')));

//print_r($prodList);exit;
$fMopList = array(1 => 'CASH', 2 => 'CRED', 3 => 'CHEC');


?>
<div class="row">
  <div class="col-xs-12">
	  <ul class="menu-btn1">
		  <li class="<?php echo (($this->params['controller'] ==  'ruby_uprodts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-bar-chart'></i>Daily Sales", array('controller' => 'ruby_uprodts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utankts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-pie-chart'></i>Sale By Tank No", array('controller' => 'ruby_utankts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uautots')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-line-chart'></i>Sales by MOP", array('controller' => 'ruby_uautots', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_uhosets')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-text-o'></i>Sales HOSC", array('controller' => 'ruby_uhosets', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_userlvts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-calculator'></i>Sale By Service Level", array('controller' => 'ruby_userlvts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_utierts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-clipboard'></i>Sale By Tier Product", array('controller' => 'ruby_utierts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_edispts')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-file-o'></i>Dispenser Sales", array('controller' => 'ruby_edispts', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dcrstats')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-large'></i>Sale By DCR No", array('controller' => 'ruby_dcrstats', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		
		<li class="<?php echo (($this->params['controller'] ==  'ruby_deptotals')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-th-list'></i>Grocery Sales", array('controller' => 'ruby_deptotals', 'action' => 'index'), array('escape' => false)); ?>
		</li>
		<li class="<?php echo (($this->params['controller'] ==  'ruby_dailyreportings')) ? 'active' : ''; ?>">
			<?php echo $this->Html->link("<i class='fa fa-columns'></i>Daily Reporting", array('controller' => 'ruby_dailyreportings', 'action' => 'index'), array('escape' => false)); ?>
		</li>
	  </ul>
  </div>
</div>
<div class="page-content-wrapper">
    <div class="page-content">
<div class="rubyUautots index">
	<h3 class="page-title"><?php echo __('Ruby Uautots'); ?></h3>
	 <div class="row">
               <?php echo $this->element('table_filter_datepicker'); ?>

		<div class="col-md-4 top-space">
                      <label>Filter by:</label>
                      <select data-placeholder="Select Tank" class="select_box" tabindex="2" id="tank">
                       
                        <option value="">Select Tank</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                      </select>
                    </div>
		    <div class="col-md-1 middel-space top-space">
                      <!--<label>&nbsp;</label>-->
                      <button type="button" class="btn btn-block btn-info tank-filter" onclick="filter_utank();">Filter</button>
                    </div>
                    <div class="col-md-1 middel-space top-space">
			<button type="button" class="btn btn-info btn-block dropdown-toggle pull-right action-text tank-filter" data-toggle="dropdown">Action <span class="caret"></span></button>
   		  <ul class="dropdown-menu btn-block icons-right dropdown-menu-right">
        
         		<li><a href="#" target="_blank"><i class="icon-link"></i> Save as pdf</a></li>
        	        <li><a href="#" target="_blank"><i class="icon-link"></i> Save as xls</a></li>
                  </ul>
               </div>  
            </div>
<br>
    <div class="row">
<div class="col-md-12">
       <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">    
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
				<th><?php echo $this->Paginator->sort('name'); ?></th>
				<th><?php echo $this->Paginator->sort('fuel_mop_number'); ?></th>
				<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
				<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
				<th><?php echo 'Beginning Date'; ?></th>
				<th><?php echo 'Ending Date'; ?></th>
		
			
			
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rubyUautots as $rubyUautot): ?>
	<tr>
		<td><?php echo h($rubyUautot['RubyUautot']['name']); ?>&nbsp;</td>
		<td><?php echo @$fMopList[$rubyUautot['RubyUautot']['fuel_mop_number']]; ?>&nbsp;</td>
		<td><?php echo h($rubyUautot['RubyUautot']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyUautot['RubyUautot']['fuel_value']); ?>&nbsp;</td>
		
		<td><?php echo CakeTime::format($rubyUautot['RubyHeader']['beginning_date_time'], '%m-%d-%Y'); ?></td>
		<td><?php echo CakeTime::format($rubyUautot['RubyHeader']['ending_date_time'], '%m-%d-%Y'); ?></td>
		
		
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		</div>
		</div>
	</div>
</div>
	
		</div>
		</div>
	</div>



