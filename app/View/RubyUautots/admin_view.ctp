<div class="rubyUautots view">
<h2><?php echo __('Ruby Uautot'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUautot['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUautot['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Mop Number'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['fuel_mop_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyUautot['RubyUautot']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Uautot'), array('action' => 'edit', $rubyUautot['RubyUautot']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Uautot'), array('action' => 'delete', $rubyUautot['RubyUautot']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUautot['RubyUautot']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uautots'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uautot'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
