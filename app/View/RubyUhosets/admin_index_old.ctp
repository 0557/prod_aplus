<?php
//$tankList = array(1 => 'REG', 2 => 'PREM', 3 => 'DIESEL', 4 => 'UNLD2');
$prodList = ClassRegistry::init('RubyFprod')->find('list', array('conditions' => array('store_id' => $_SESSION['store_id']), 'fields' => array('fuel_product_number', 'display_name')));




//print_r($prodList);exit;
$fMopList = array(1 => 'CASH', 2 => 'CRED', 3 => 'CHEC');


?>
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
<div class="rubyUhosets index">
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Ruby Uhosets
							</div>
						
						</div>
						<div class="portlet-body">
						
	
	<div class="row">
	 <div class="col-md-2">
							<label>From:</label>
							<input type="text" class="form-control" placeholder="Select From Date" value="<?php echo date('m-d-Y',strtotime(@$datevalue['RubyHeader']['beginning_date_time'])); ?>" id="from">
							</div>

					<div class="col-md-2">
							<label>To:</label>
							<input type="text" class="form-control" placeholder="Select To Date" id="to" value="<?php echo date('m-d-Y',strtotime(@$datevalue['RubyHeader']['ending_date_time'])); ?>" >
							</div>
							
              <?php //echo $this->element('table_filter_datepicker'); ?>

		<div class="col-md-2 top-space">
                      <label>Filter by:</label>
						  <?php echo $this->Form->input('fueling_point_number', array('class' => 'select_box form-control','id'=>'mop', 'label' => false, 'required' => 'true','options'=>$fueling_point_numbera, 'empty' => 'Select Number')); ?>
						  
						
                    </div>
		    <div class="col-md-2 middel-space top-space">
                      <label>&nbsp;</label>
                      <button type="button" class="btn btn-block btn-info tank-filter" onclick="filter_utank();">Filter</button>
                    </div>
                    <div class="col-md-2 middel-space top-space">
			                      <label>&nbsp;</label>
<button type="button" class="btn btn-info btn-block dropdown-toggle pull-right action-text tank-filter" data-toggle="dropdown">Action <span class="caret"></span></button>
   		  <ul class="dropdown-menu icons-right btn-block dropdown-menu-right">
        
         		<li><a href="#" target="_blank"><i class="icon-link"></i> Save as pdf</a></li>
        	        <li><a href="#" target="_blank"><i class="icon-link"></i> Save as xls</a></li>
                  </ul>
               </div>  
            </div>
<br>
</div>
</div>
<div class="row">
	<div class="col-md-12">
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Sales
							</div>
						
						</div>
						<div class="portlet-body">
	
       <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example">
	<thead>
	<tr>
				<th><?php echo $this->Paginator->sort('Display Name'); ?></th>
<!--				<th><?php echo $this->Paginator->sort('fueling_point_number'); ?></th>
				<th><?php echo $this->Paginator->sort('fuel_product_number'); ?></th>
	-->			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
				<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
		<!--		<th><?php echo $this->Paginator->sort('fuel_product_id'); ?></th>
			-->   <th><?php echo 'Beginning Date '; ?></th>
				<th><?php echo 'Ending Date '; ?></th>
			
			
	</tr>
	</thead>
	<tbody id="dataget">
	<?php foreach ($rubyUhosets as $rubyUhoset): ?>
	<tr>
	   <td><?php echo @$prodList[$rubyUhoset['RubyUhoset']['fuel_product_number']]; ?>&nbsp;</td>
<!--		<td><?php echo h($rubyUhoset['RubyUhoset']['fueling_point_number']); ?>&nbsp;</td>
		<td><?php echo @$prodList[$rubyUhoset['RubyUhoset']['fuel_product_number']]; ?>&nbsp;</td>
	-->	<td><?php echo h($rubyUhoset['RubyUhoset']['fuel_volume']); ?>&nbsp;</td>
		<td><?php echo h($rubyUhoset['RubyUhoset']['fuel_value']); ?>&nbsp;</td>
		<!--<td><?php echo h($rubyUhoset['RubyUhoset']['fuel_product_id']); ?>&nbsp;</td>
		-->
		<td><?php echo CakeTime::format($rubyUhoset['RubyHeader']['beginning_date_time'], '%m-%d-%Y'); ?></td>
		<td><?php echo CakeTime::format($rubyUhoset['RubyHeader']['ending_date_time'], '%m-%d-%Y'); ?></td>
						
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
				</div>
		</div>
	</div>
</div>
	</div>

</div>
	</div>
	</div>


<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function filter_utank() {	
		var from_date=$('#from').val();
		var to_date=$('#to').val();
		
		var mop=$('#mop').val();
		//$('#loaderdiv').show();
		$.ajax({
			type : 'POST',
			url : webroot+'ruby_uhosets/index/',
			dataType : 'html',
			data: {from_date:from_date,to_date:to_date,mop:mop},
			success:function(result){
					$("#example_info").hide();
					$("#example_paginate").hide();
			$("#example_filter").hide();
			$("#example_length").hide();
			$(".filterdata .col-md-2 ").hide();
			$("#filter").show();
			$("#dataget").html(result);

			}});
	}
</script>