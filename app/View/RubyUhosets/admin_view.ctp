<div class="rubyUhosets view">
<h2><?php echo __('Ruby Uhoset'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUhoset['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUhoset['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fueling Point Number'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['fueling_point_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Number'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['fuel_product_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Id'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['fuel_product_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyUhoset['RubyUhoset']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Uhoset'), array('action' => 'edit', $rubyUhoset['RubyUhoset']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Uhoset'), array('action' => 'delete', $rubyUhoset['RubyUhoset']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUhoset['RubyUhoset']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uhosets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uhoset'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
