<div class="rubyUprodts view">
<h2><?php echo __('Ruby Uprodt'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUprodt['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUprodt['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Number'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fuel_product_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fmop1'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fmop1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fprodid'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fprodid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fpricemop0'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fpricemop0']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fpricemop1'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fpricemop1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fpricemop2'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['fpricemop2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rubyUprodt['RubyUprodt']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Uprodt'), array('action' => 'edit', $rubyUprodt['RubyUprodt']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Uprodt'), array('action' => 'delete', $rubyUprodt['RubyUprodt']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUprodt['RubyUprodt']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Uprodts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Uprodt'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
