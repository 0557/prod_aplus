<div class="rubyUserlvts form">
<?php echo $this->Form->create('RubyUserlvt'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ruby Userlvt'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('service_level_number');
		echo $this->Form->input('fuel_mop_number');
		echo $this->Form->input('fuel_volume');
		echo $this->Form->input('fuel_value');
		echo $this->Form->input('create');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RubyUserlvt.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('RubyUserlvt.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Userlvts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
