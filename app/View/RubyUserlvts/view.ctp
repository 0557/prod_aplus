<div class="rubyUserlvts view">
<h2><?php echo __('Ruby Userlvt'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUserlvt['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUserlvt['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service Level Number'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['service_level_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Mop Number'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['fuel_mop_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyUserlvt['RubyUserlvt']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Userlvt'), array('action' => 'edit', $rubyUserlvt['RubyUserlvt']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Userlvt'), array('action' => 'delete', $rubyUserlvt['RubyUserlvt']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUserlvt['RubyUserlvt']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Userlvts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Userlvt'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
