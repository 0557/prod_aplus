<div class="rubyUtankts form">
<?php echo $this->Form->create('RubyUtankt'); ?>
	<fieldset>
		<legend><?php echo __('Add Ruby Utankt'); ?></legend>
	<?php
		echo $this->Form->input('ruby_header_id');
		echo $this->Form->input('name');
		echo $this->Form->input('tank_number');
		echo $this->Form->input('fuel_volume');
		echo $this->Form->input('fuel_value');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ruby Utankts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
