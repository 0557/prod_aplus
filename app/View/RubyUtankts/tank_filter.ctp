<?php
//$tankList = array(1 => 'REG', 2 => 'PREM', 3 => 'DIESEL', 4 => 'UNLD2');

//print_r($prodList);exit;
$fMopList = array(1 => 'CASH', 2 => 'CRED', 3 => 'CHEC');
?>
 
<div class="col-md-12">

   
    <div class="block-inner clearfix tooltip-examples">
 	   
 	 <div class="datatable">
	<table class="table table-striped table-bordered table-advance table-hover" id="example1">
	<thead>
	<tr>

			<th><?php echo $this->Paginator->sort('tank_number'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_volume'); ?></th>
			<th><?php echo $this->Paginator->sort('fuel_value'); ?></th>
			<th><?php echo 'Beginning Date'; ?></th>
			<th><?php echo 'Ending Date '; ?></th>
		    <th><?php echo 'Status'; ?></th>
		
			
			
			
	</tr>
	</thead>
	<tbody id="utank_list">
	<?php foreach ($rubyUtankts as $rubyUtankt): ?>
	<tr>
		
		<td><?php echo $rubyUtankt['RubyUtankt']['tank_number']; ?>&nbsp;</td>
		<td><?php echo $rubyUtankt['RubyUtankt']['fuel_volume']; ?>&nbsp;</td>
		<td>$<?php echo $rubyUtankt['RubyUtankt']['fuel_value']; ?>&nbsp;</td>
		
		<td><?php echo CakeTime::format($rubyUtankt['RubyHeader']['beginning_date_time'], '%m-%d-%Y'); ?></td>
		<td><?php echo CakeTime::format($rubyUtankt['RubyHeader']['ending_date_time'], '%m-%d-%Y'); ?></td>
		<td>
		<?php if($rubyUtankt['RubyUtankt']['status']=='0'){
		?>
        <a href="<?php echo Router::url('/');?>admin/ruby_utankts/available/<?php echo $rubyUtankt['RubyUtankt']['tank_number'];?>" class="btn  btn-warning status_width">Unavailable</a>
		<?php
		}else{
		?>	
		<a href="<?php echo Router::url('/');?>admin/ruby_utankts/unavailable/<?php echo $rubyUtankt['RubyUtankt']['tank_number'];?>" class="btn  btn-success status_width">Available</a>	
		<?php
		}
		?>
        </td>
		
		
	
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	</div>

</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example1').DataTable();
           
	} );
  
</script>


