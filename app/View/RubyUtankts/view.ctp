<div class="rubyUtankts view">
<h2><?php echo __('Ruby Utankt'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUtankt['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUtankt['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tank Number'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['tank_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($rubyUtankt['RubyUtankt']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Utankt'), array('action' => 'edit', $rubyUtankt['RubyUtankt']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Utankt'), array('action' => 'delete', $rubyUtankt['RubyUtankt']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUtankt['RubyUtankt']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Utankts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Utankt'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
	</ul>
</div>
