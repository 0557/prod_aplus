<div class="rubyUtierts view">
<h2><?php echo __('Ruby Utiert'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ruby Header'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUtiert['RubyHeader']['id'], array('controller' => 'ruby_headers', 'action' => 'view', $rubyUtiert['RubyHeader']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Tier Number'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['price_tier_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product Number'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['fuel_product_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Volume'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['fuel_volume']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Value'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['fuel_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuel Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($rubyUtiert['FuelProduct']['id'], array('controller' => 'fuel_products', 'action' => 'view', $rubyUtiert['FuelProduct']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($rubyUtiert['RubyUtiert']['create']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ruby Utiert'), array('action' => 'edit', $rubyUtiert['RubyUtiert']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ruby Utiert'), array('action' => 'delete', $rubyUtiert['RubyUtiert']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rubyUtiert['RubyUtiert']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Utierts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Utiert'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ruby Headers'), array('controller' => 'ruby_headers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ruby Header'), array('controller' => 'ruby_headers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fuel Products'), array('controller' => 'fuel_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fuel Product'), array('controller' => 'fuel_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
