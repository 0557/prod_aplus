<?php //pr($wholesaleProduct); ?>
<div class="page-content">
   
    <div class="row">
	
	<div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View Fuel Mixes Info : <?php echo $wholesaleProduct['RwholesaleProduct']['id'];     ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <!--<h2 class="margin-bottom-20"> View  Info - FuelInvoice : <?php echo $wholesaleProduct['RwholesaleProduct']['id'];     ?> </h2>-->
                                        
                                        <div class="row">
                                        <div class="col-md-6 col-sm-12">
												<div class="portlet yellow box">
                                        <div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Product Information
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																 Select Department:
															</div>
															<div class="col-md-7 value">
																<?php echo h($wholesaleProduct['FuelDepartment']['name']); ?>
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Product Name:
															</div>
															<div class="col-md-7 value">
																<?php echo h($wholesaleProduct['RwholesaleProduct']['name']); ?>
																
															</div>
														</div>
                                                       
                                                       
<!--														<div class="row static-info">
															<div class="col-md-5 name">
															Load Date:	 
															</div>
															<div class="col-md-7 value">
                                                                                                             <?php echo date('d F Y h:i A', strtotime($wholesaleProduct['RwholesaleProduct']['load_date'])); ?>
                                                                                                                            <?php //echo $fuelInvoice['FuelInvoice']['load_date']; ?>
																 
																
															</div>
														</div>-->
                                                        
														
                                                        
														
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Description: 
															</div>
															<div class="col-md-7 value">
															<?php echo $wholesaleProduct['RwholesaleProduct']['description']; ?>	 
																
															</div>
														</div>
                                                        
														
                                                                                                               <div class="row static-info">
															<div class="col-md-5 name">
															Status:	 
															</div>
															<div class="col-md-7 value">
                                                                                                                            <span class="label label-success">	<?php echo $wholesaleProduct['RwholesaleProduct']['status']; ?></span>
																
															</div>
														</div>
                                                        
                                                        
                                                    </div>
                                                    </div>
													</div>
                                                    <div class="col-md-6 col-sm-12">
												<div class="portlet blue box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Manage Inventory
														</div>
														<div class="actions">
															<!--<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>-->
														</div>
													</div>
                                                    <div class="portlet-body">
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Select Store: 
															</div>
															<div class="col-md-7 value">
																<?php echo $wholesaleProduct['Store']['name']; ?>
															</div>
														</div>
                                                    <div class="row static-info">
															<div class="col-md-5 name">
																Select Vender: 
															</div>
															<div class="col-md-7 value">
																<?php echo $wholesaleProduct['Customer']['name']; ?>
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Price Per Gallon:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['RwholesaleProduct']['price']; ?>
															</div>
														</div>
                                                    
														<div class="row static-info">
															<div class="col-md-5 name">
																 Vendor Price:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['RwholesaleProduct']['vendor_price']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Tax Class:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['RwholesaleProduct']['tax_class']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Qty Available in Gallon:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['RwholesaleProduct']['qty']; ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Opening Book Inventory:
															</div>
															<div class="col-md-7 value">
																 <?php echo $wholesaleProduct['RwholesaleProduct']['opening_product_amount']; ?>
															</div>
														</div>
                                                       
                                                        </div>
                                                    </div>
													</div>
                                        
                                            
                                 
                                <!-- END FORM-->
                            </div>
                                        
                                         <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption information_product">
                        <i class="fa fa-reorder"></i> UST Information
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>
            
             <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Tank No:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['tank_no']; ?>
                                    
                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Capacity:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['tank_capacity']; ?>
                                   
                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Min Qty:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['min_qty']; ?>
                                   
                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Max Qty:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['max_qty']; ?>
                                    
                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Piping:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['piping']; ?>
                                                                 
                                
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Type:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $wholesaleProduct['RwholesaleProduct']['tank_type']; ?>
                                    
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Piping Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo date('d F Y h:i A', strtotime($wholesaleProduct['RwholesaleProduct']['Piping_date'])); ?>
                                    
                                </div>
                            </div>
   
                        </div></div></div>
               
            </div>
                                        
                                        
                                        
                        </div>
                    </div>

                            <div class="portlet-body" style="overflow:hidden;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>
                                      Base Product Name(1):
                                    </th>
                                    <th>
                                        <!-- Vendor Price-->
                                       Mix(%)
                                    </th>
                                    <th>
                                        <!-- Tax Class-->
                                        Base Product Name(2):

                                    <th>
                                      Base Product 2(%):
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="products_add_more">
                                <?php if (isset($wholesaleProduct['RfuelProduct'][0]) && !empty($wholesaleProduct['RfuelProduct'][0])) { ?>
                  <?php  $nameAndId = $this->Custom->GetProductName($wholesaleProduct['RfuelProduct'][0]['base_product_first']); ?>
                  <?php  $nameAndIds = $this->Custom->GetProductName($wholesaleProduct['RfuelProduct'][0]['base_product_second']); ?>
                                <tr>
                                    <td><?php echo $nameAndId['WholesaleProduct']['name'];   ?></td>
                                    <td><?php echo  $wholesaleProduct['RfuelProduct'][0]['first_per']    ?> </td>
                                    <td><?php echo  $nameAndIds['WholesaleProduct']['name'];   ?></td>
                                    <td><?php echo  $wholesaleProduct['RfuelProduct'][0]['second_per']    ?></td>
                                   
                                </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                            
                </div>
            </div>
        </div>
    </div></div></div>
    <!-- END PAGE CONTENT-->
 