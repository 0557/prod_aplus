<?php
 // INCLUDE THE phpToPDF.php FILE
require("phpToPDF.php"); 

// PUT YOUR HTML IN A VARIABLE
$my_html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <div style="width:1103px; margin:75px auto 0">
            <div>
                <div style=" float:left; width:274px;"> <!--<img src="http://192.168.1.118/backoffice/app/webroot/img/ncEgjBRcA.png" />-->
                    <img src="<?php echo WWW_ROOT.ncEgjBRcA.png; ?>" />
                    <p style="font-size: 26px;
                       margin: 0;"> 1200 LAUREL OAK RD SUITE 108 </p>
                    <p style="font-size: 26px;
                       margin: 0;">V OORHEES,NJ 08043</p>
                    <h1 style="font-size: 26px;
                        margin: 15px 0; ">Bill To:</h1>
                    <p style="font-size: 26px;
                       margin: 0;" > LAWNSIDE CITGO</p>
                    <p style="font-size: 26px;
                       margin: 0;">355 WHITE HORSE PIKE</p>
                    <p style="font-size: 26px;
                       margin: 0;">LAWNSIDE, NJ 08045</p>
                </div>
                <div style="float:right; width:664px">
                    <ul style="list-style: outside none none;
                        margin: 0;">
                        <li style=" float:left; margin-right:30px">
                            <h3 style="font-size:25.5573px;
                                margin: 0;">Invoice Date</h3>
                            <p style="font-size: 26px;
                               margin:0;">8/5/2015</p>
                        </li>
                        <li style=" float:left;">
                            <h3 style="font-size:25.5573px;
                                margin:0;">Invoice#</h3>
                            <p style="font-size: 26px;
                               margin: 0;">8/5/2015</p>
                        </li>
                    </ul>
                    <h1 style="float: right;
                        font-family: Arial;
                        font-size: 42.6667px;
                        margin-top: 0;">Invoice</h1>
                    <div style="border: 5px solid #7f7f7f;
                         clear: both;
                         float: right;
                         overflow: auto;
                         padding: 16px 30px;
                         width: 485px;">
                        <h3 style="float: left;font-size: 25.5573px;
                            left: 162.773px;">PLEASE PAY</h3>
                        <p>$3,935.60</p>
                    </div>
                    <h1 style="clear: both;
                        float: right;
                        font-size: 20px;
                        width: 551px; font-size:25.5573px; font-weight:normal;">Make checks payable to:<span style="float: right;
                                                   font-size: 25.5573px; font-weight:bold;">SATRAJ, INC</span></h1>
                    <div style="clear: both;
                         float: right;
                         width: 552px;">
                        <h1 style="font-size:25.5573px;
                            margin:0 0 15px;">Ship To:</h1>
                        <p style="font-size: 26px;
                           margin: 0;">LAWNSIDE CITGO<br />
                            355 WHITE HORSE PIKE<br />
                            LAWNSIDE, NJ 08045</p>
                    </div>
                </div>
            </div>
            <div  style="border-top: 5px dotted gray;
                  clear: both;
                  display: inline-block;
                  margin: 25px 0;
                  padding-top: 20px;
                  width: 1103px;">
                <div style="float: left;">
                    <h3 style="font-size:25.5573px;
                        margin: 0;">SATRAJ,INC</h3>
                    <p style="font-size: 26px;
                       margin: 0;">1200 LAURELOAK RD<br />
                        SUITE 108<br />
                        VOORHEES, NJ 08043</p>
                </div>
                <div style="float: right;">
                    <h3 style="font-size: 24.5573px;
                        font-weight: normal;
                        margin: 0;">PLEASE DETACH AND RETURN TOP PORTION WITH PAYMENT</h3>
                    <table style="margin: 35px 0 0;" width="707" border="1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="font-size: 24px; margin-top: 9px; padding: 16px 0;text-align: center;">Ship Via</td>
                            <td style="font-size: 24px;margin-top: 9px; padding: 16px 0; text-align: center;">BOL #</td>
                            <td style="font-size: 24px;margin-top: 9px;padding: 16px 0; text-align: center;">Ship Date</td>
                            <td style="font-size: 24px;margin-top: 9px;padding: 16px 0;text-align: center;">Due Date</td>
                            <td style="font-size: 24px;margin-top: 9px; padding: 16px 0;text-align: center;">Terms</td>
                        </tr>
                        <tr>
                            <td style="font-size: 24px;margin-top: 9px; padding: 16px 0;text-align: center;">ETLO</td>
                            <td style="font-size: 24px; margin-top: 9px;padding: 16px 0;text-align: center;">1145182</td>
                            <td style="font-size: 24px;margin-top: 9px;padding: 16px 0;text-align: center;">8/5/2015</td>
                            <td style="font-size: 24px;margin-top: 9px;padding: 16px 0;text-align: center;">8/10/2015</td>
                            <td style="font-size: 24px;margin-top: 9px;padding: 16px 0;text-align: center;">5 DAYS</td>
                        </tr>
                    </table>
                </div>
            </div>
            <table cellpadding="0" cellspacing="0" width="1103" style="text-align:left; border:1px solid;">
                <thead>
                    <tr>
                        <th style="width:655px; border-right: 1px solid;padding-left:2px; border-bottom: 1px solid;"><h2 style="font-size: 25.5573px;margin: 0;">Description</h2></th>
                        <th style="border-right: 1px solid;"><h2 style="font-size: 25.5573px;margin: 0; border-bottom: 1px solid;">Qty</h2></th>
                        <th style="border-right: 1px solid;"><h2 style="font-size: 25.5573px;margin: 0; border-bottom: 1px solid;">Rate</h2></th>
                        <th style="border-bottom: 1px solid;"><h2 style="font-size: 25.5573px;margin: 0;">Amount</h2></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-right: 1px solid; width:655px; padding-left:2px;font-size: 25px;">ULTRA LOW SULFURDIESEL</td>
                        <td style="border-right: 1px solid;width:154px;padding-left:2px;font-size: 25px;" >2,000</td>
                        <td style="border-right: 1px solid;width:154px;padding-left:2px;font-size: 25px;">1.5469</td>
                        <td style="border-right: width:154px;padding-left:2px;font-size: 25px;">3,093.80</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr >
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">FEDERAL DIESEL TAX</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">2,000</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">0.244</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">488.00</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">NJ GROSSRECEIPT TAX</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">2,000</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">0.04</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">80.00</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">FEDERAL DIESEL OIL SPILLTAX</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">2,000</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">0.0019</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">3.80</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">NJ State DieselTax</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">2,000</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">0.135</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">270.00</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">NJ DIESEL TAX</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">841.80</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:655px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;border-right: 1px solid;padding-left:2px;font-size: 25px;">&nbsp;</td>
                        <td style="width:154px;padding-left:2px;font-size: 25px;">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <table style=" float: right;width: 463px; border:1px solid; border-top:0;" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid;"><h2 style="font-size: 25.5573px;padding-left: 2px;margin: 0">Subtotal</h2></td>
                        <td style="border-bottom: 1px solid;font-size: 25px;padding-left: 2px;width: 154px;">$3,935.60</td>
                    </tr>
                    <tr >
                        <td style="border-bottom: 1px solid;"><h2 style="font-size: 25.5573px;padding-left: 2px;margin: 0;">Payments/Credits</h2></td>
                        <td style="border-bottom: 1px solid;font-size: 25px;padding-left: 2px;width: 154px;">$0.00</td>
                    </tr>
                    <tr>
                        <td ><h2 style="font-size: 25.5573px;padding-left: 2px;margin: 0;">Balance Due</h2></td>
                        <td style="font-size: 25px;padding-left: 2px;width: 154px;"><strong>$3,935.60</strong></td>
                    </tr>
                </tbody>
            </table>
            <table style="clear:both; width:400px" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style=" font-size: 21.2053px; padding-bottom: 10px;"><strong>Billing Inqueries? Call</strong></td> <td style=" font-size: 21.2053px; padding-bottom: 10px;"><strong>856-258-6014</strong></td>
                    </tr>
                    <tr>
                        <td style=" font-size: 21.2053px;"><strong>Fax</strong></td> <td style=" font-size: 21.2053px;"><strong>856-632-7743</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
';
//echo $my_html; die;
// SET YOUR PDF OPTIONS
// FOR ALL AVAILABLE OPTIONS, VISIT HERE:  http://phptopdf.com/documentation/
$pdf_options = array(
  "source_type" => 'html',
  "source" => $my_html,
  "action" => 'save',
  "save_directory" => WWW_ROOT . '/',
  "file_name" => 'html_01.pdf');

// CALL THE phpToPDF FUNCTION WITH THE OPTIONS SET ABOVE
phptopdf($pdf_options);

// OPTIONAL - PUT A LINK TO DOWNLOAD THE PDF YOU JUST CREATED
echo ("<a href='html_01.pdf'>Download Your PDF</a>");
?>