<?php // pr($salesInvoice); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


    <!-- BEGIN PAGE CONTENT-->
    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> View Fuel Sales INVOICE Info : <?php echo $salesInvoice['SalesInvoice']['id']; ?>
                    </div>
                    <span style="float: right; background:rgb(255, 255, 255); padding: 1px 8px;">
                        <a href="<?php echo Router::url('/',true);?>uploads/sale_invoice/<?php echo $salesInvoice['SalesInvoice']['invoice_file'];?>" target="_blank" >Print Pdf</a>
                    </span>
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <!--<h2 class="margin-bottom-20"> View  Info - FuelInvoice : <?php echo $salesInvoice['SalesInvoice']['id']; ?> </h2>-->

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Order Details
                                                    </div>
                                                    <div class="actions">
                                                        <!--<a href="#" class="btn default btn-sm">
                                                                <i class="fa fa-pencil"></i> Edit
                                                        </a>-->
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice TO Corporation:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($salesInvoice['Corporation']['name']); ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice TO Store:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($salesInvoice['Store']['name']); ?>

                                                        </div>
                                                    </div>
                                                    <span class="option_customer"> or</span>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice to Customer 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php //echo $salesInvoice['Customer']['name'];   ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Bill to:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($salesInvoice['SalesInvoice']['bill_to']); ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Billing Address:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo h($salesInvoice['SalesInvoice']['billing_address']); ?>

                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice date:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($salesInvoice['SalesInvoice']['invoice_date'])); ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['load_date']; ?>


                                                        </div>
                                                    </div>



                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            FEIN:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($salesInvoice['SalesInvoice']['ship_date'])); ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['receving_date']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Po: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['po']; ?>	 

                                                        </div>
                                                    </div>

                                                    

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-cogs"></i>Supplier Details
                                                    </div>
                                                    <div class="actions">
                                                        <!--<a href="#" class="btn default btn-sm">
                                                                <i class="fa fa-pencil"></i> Edit
                                                        </a>-->
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Bill to:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['ship_to']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Ship to:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['shiping_address']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Due date:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo date('d F Y h:i A', strtotime($salesInvoice['SalesInvoice']['due_date'])); ?>
                                                            <?php //echo $fuelInvoice['FuelInvoice']['receving_date'];   ?>

                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Bol:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['bol']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Terminal:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['terminal']; ?>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Terms:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['terms']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Ship via:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['ship_via']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Invoice:	 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $salesInvoice['SalesInvoice']['incoice_type']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Status:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <span class="label label-success">
                                                                <?php
                                                                if ($salesInvoice['SalesInvoice']['status'] == 1) {
                                                                    echo 'Approval';
                                                                } else {
                                                                    echo "Pending";
                                                                }
                                                                ?>
                                                            </span>
                                                                                                                           <!--<span class="label label-info label-sm">
                                                                                                                                    Email confirmation was sent
                                                                                                                           </span>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div></div></div>
    <!-- END PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet purple box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i> Fuel Sales Products
                    </div>
                    <div class="actions">
                        <!--<a href="#" class="btn default btn-sm">
                                <i class="fa fa-pencil"></i> Edit
                        </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                <tr>

                                    <th>
                                        Products
                                    </th>
                                    <th>
                                        <!-- Vendor Price-->
                                        Gallons Delivered
                                    </th>
                                    <th>
                                        <!-- Tax Class-->
                                        Cost Per Gallons

                                    <th>
                                        Net Amount
                                    </th>

                                </tr>
                            </thead>
                            <?php $total = 0; ?>
                            <tbody>
                                <?php if (isset($salesInvoice['FuelProduct']) && !empty($salesInvoice['FuelProduct'])) { //pr($product);  ?>


                                    <?php
                                    foreach ($salesInvoice['FuelProduct'] as $key => $data) {
                                        $nameAndId = $this->Custom->GetProductName($data['product_id']);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                echo $nameAndId['WholesaleProduct']['name'];
                                                ?>
                                            </td>
                                            <td class='max_open'><?php echo $data['gallons_delivered']; ?> </td>
                                            <td><?php echo $data['cost_per_gallon']; ?></td>
                                            <td class="net_amount"><?php echo $data['net_ammount']; ?></td>

                                        </tr>

                                    <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>

                        <table class="table table-hover table-bordered table-striped add">
                            <thead>
                                <tr>
                                    <th>
                                        Tax Zone
                                    </th>
                                    <th>

                                        USA State
                                    </th>
                                    <th>

                                        Tax Description
                                    </th>   
                                    <th>
                                        Tax on Gallon Delivered(Qty)
                                    </th>
                                    <th>
                                        Rate(%)on Gallon 
                                    </th>
                                    <th>
                                        Net Amount
                                    </th>


                                </tr>
                            </thead>
                            <tbody id="gallon_add_more">

<?php foreach ($salesInvoice['TaxeZone'] as $data) {  ?>   
                                    <tr>
                                        <td><?php echo $data['tax_zone']; ?></td>
                                         <?php $state_name = classregistry::init('State')->find('first', array('conditions' => array('State.id' => $data['state_id']), 'fields' => array('State.name'))); ?>
                                        <td><?php echo $state_name['State']['name']; ?></td>
                                        <td><?php echo $data['tax_decription']; ?></td>
                                        <td><?php echo $data['tax_on_gallon_qty']; ?></td>
                                        <td><?php echo $data['rate_on_gallon']; ?></td>
                                        <td><?php echo $data['tax_net_amount_gallon']; ?></td>




                                    </tr>
<?php } ?>





                            </tbody>
                        </table>                                                                               
                    </div>
                </div>

            </div>

        </div>


        <ul class="list-unstyled amounts total_last">
            <li class="last_content_first">
                <span class="span_1">Gallon Delivered Total</span><span class="span_2" id="Gallon_Delivered_Total"><?php echo $salesInvoice['SalesInvoice']['max_qnty']; ?>
                </span>
            </li>
            <li class="last_content_two">
                <span class="span_1">Net Amount Total</span><span class="span_2" id="Net_Amount_Total"><?php echo $salesInvoice['SalesInvoice']['total_invoice']; ?> </span>
            </li>
            <li class="last_content_three">
                <span class="span_1">Taxes</span><span class="span_2" id="Taxes"><?php echo $salesInvoice['SalesInvoice']['taxes']; ?></span>
            </li>
            <li class="last_content_four">
                <span class="span_1">Gross Amount</span><span class="span_2" id="Taxes"><?php echo $salesInvoice['SalesInvoice']['gross_amount']; ?></span>
            </li>
              <li class="last_content_four">
                <span class="span_1">Discount</span><span class="span_2" id="Taxes"><?php echo $salesInvoice['SalesInvoice']['discount']; ?></span>
            </li>
              <li class="last_content_four">
                <span class="span_1">Total Amount</span><span class="span_2" id="Taxes"><?php echo $salesInvoice['SalesInvoice']['total_amount']; ?></span>
            </li>
            <li class="last_content_five">
                <div class="credit_span"><span class="span_1">MOP :  </span></div><span class="span_2" id="MOP">
<?php echo $salesInvoice['SalesInvoice']['mop']; ?>
                </span>
            </li>
        </ul>


    </div>
    </div>

    <!-- END CONTENT -->
