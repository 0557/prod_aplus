<div class="salesInvoices form">
<?php echo $this->Form->create('SalesInvoice'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sales Invoice'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('bol');
		echo $this->Form->input('po');
		echo $this->Form->input('purchase_order_no');
		echo $this->Form->input('load_date');
		echo $this->Form->input('receving_date');
		echo $this->Form->input('supplier_id');
		echo $this->Form->input('terminal');
		echo $this->Form->input('export_suplier');
		echo $this->Form->input('export_terminal');
		echo $this->Form->input('carrier');
		echo $this->Form->input('driver');
		echo $this->Form->input('invoice');
		echo $this->Form->input('product_id');
		echo $this->Form->input('max_qnty');
		echo $this->Form->input('total_invoice');
		echo $this->Form->input('mop');
		echo $this->Form->input('status');
		echo $this->Form->input('comments');
		echo $this->Form->input('store_bill');
		echo $this->Form->input('customer_bill');
		echo $this->Form->input('store_ship');
		echo $this->Form->input('customer_ship');
		echo $this->Form->input('invoice_date');
		echo $this->Form->input('ship_date');
		echo $this->Form->input('terms');
		echo $this->Form->input('due_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SalesInvoice.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('SalesInvoice.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Sales Invoices'), array('action' => 'index')); ?></li>
	</ul>
</div>
