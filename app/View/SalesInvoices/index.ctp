<div class="salesInvoices index">
	<h2><?php echo __('Sales Invoices'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('bol'); ?></th>
			<th><?php echo $this->Paginator->sort('po'); ?></th>
			<th><?php echo $this->Paginator->sort('purchase_order_no'); ?></th>
			<th><?php echo $this->Paginator->sort('load_date'); ?></th>
			<th><?php echo $this->Paginator->sort('receving_date'); ?></th>
			<th><?php echo $this->Paginator->sort('supplier_id'); ?></th>
			<th><?php echo $this->Paginator->sort('terminal'); ?></th>
			<th><?php echo $this->Paginator->sort('export_suplier'); ?></th>
			<th><?php echo $this->Paginator->sort('export_terminal'); ?></th>
			<th><?php echo $this->Paginator->sort('carrier'); ?></th>
			<th><?php echo $this->Paginator->sort('driver'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice'); ?></th>
			<th><?php echo $this->Paginator->sort('product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('max_qnty'); ?></th>
			<th><?php echo $this->Paginator->sort('total_invoice'); ?></th>
			<th><?php echo $this->Paginator->sort('mop'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('comments'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('store_bill'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_bill'); ?></th>
			<th><?php echo $this->Paginator->sort('store_ship'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_ship'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice_date'); ?></th>
			<th><?php echo $this->Paginator->sort('ship_date'); ?></th>
			<th><?php echo $this->Paginator->sort('terms'); ?></th>
			<th><?php echo $this->Paginator->sort('due_date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($salesInvoices as $salesInvoice): ?>
	<tr>
		<td><?php echo h($salesInvoice['SalesInvoice']['id']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['bol']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['po']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['purchase_order_no']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['load_date']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['receving_date']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['supplier_id']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['terminal']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['export_suplier']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['export_terminal']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['carrier']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['driver']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['invoice']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['product_id']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['max_qnty']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['total_invoice']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['mop']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['status']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['comments']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['created']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['store_bill']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['customer_bill']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['store_ship']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['customer_ship']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['invoice_date']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['ship_date']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['terms']); ?>&nbsp;</td>
		<td><?php echo h($salesInvoice['SalesInvoice']['due_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $salesInvoice['SalesInvoice']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $salesInvoice['SalesInvoice']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $salesInvoice['SalesInvoice']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesInvoice['SalesInvoice']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sales Invoice'), array('action' => 'add')); ?></li>
	</ul>
</div>
