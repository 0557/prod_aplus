<div class="salesInvoices view">
<h2><?php echo __('Sales Invoice'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bol'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['bol']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Po'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['po']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Purchase Order No'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['purchase_order_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Load Date'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['load_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Receving Date'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['receving_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supplier Id'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['supplier_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Terminal'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['terminal']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Export Suplier'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['export_suplier']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Export Terminal'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['export_terminal']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Carrier'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['carrier']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Driver'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoice'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['invoice']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Id'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['product_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Max Qnty'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['max_qnty']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Invoice'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['total_invoice']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mop'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['mop']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comments'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store Bill'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['store_bill']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Bill'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['customer_bill']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store Ship'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['store_ship']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Ship'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['customer_ship']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoice Date'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['invoice_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ship Date'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['ship_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Terms'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['terms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Due Date'); ?></dt>
		<dd>
			<?php echo h($salesInvoice['SalesInvoice']['due_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sales Invoice'), array('action' => 'edit', $salesInvoice['SalesInvoice']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sales Invoice'), array('action' => 'delete', $salesInvoice['SalesInvoice']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesInvoice['SalesInvoice']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales Invoices'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sales Invoice'), array('action' => 'add')); ?> </li>
	</ul>
</div>
