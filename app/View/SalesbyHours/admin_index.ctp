
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
		<div class="rubyDcrstats index">
			<h3 class="page-title"><?php echo __('Sales By Hours'); ?></h3>   
			
			 <?php echo $this->Form->create('salesby_hours', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 
	?>			
		<div class="row">
         <div class="col-md-12">
            <div class=" col-md-2">Update Date</div>
            <div class="col-md-3">
           		 <input type="text"  placeholder="Select End Date" class="form-control icon_position" id="end_date" name="end_date" value="<?php echo $end_date;?> ">  
            </div>
            <div class=" col-md-1">
            <button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <div class=" col-md-4">
           		  <a href="<?php echo Router::url('/');?>admin/salesby_hours/reset" class="btn btn-warning ">Reset</a>  
            </div> 
            </div>
		</div>
<?php echo $this->form->end(); ?>	        
			
			<div class="row">
				<div class="col-md-12">
					<div class="block-inner clearfix tooltip-examples">
					
						<div class="datatable">
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
                        		<th><?php echo $this->Paginator->sort('hours'); ?></th>
								<th><?php echo $this->Paginator->sort('number_of_fuel_only_customers'); ?></th>
								<th><?php echo $this->Paginator->sort('number_fuel_and_merchandise'); ?></th>
								<th><?php echo $this->Paginator->sort('number_merchandise_customers'); ?></th>
								<th><?php echo $this->Paginator->sort('number_of_items_this_hour'); ?></th>
								<th><?php echo $this->Paginator->sort('value_of_fuel_only_sales'); ?></th>
								<th><?php echo $this->Paginator->sort('value_of_fuel_merchandise_sales'); ?></th>
                                <th><?php echo $this->Paginator->sort('value_of_merchandise_sales'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($SalesbyHours as $SalesbyHour): ?>
						<tr>
                       			<td><?php echo h($SalesbyHour['SalesbyHour']['hours']); ?>&nbsp;</td>
                                <td><?php echo h($SalesbyHour['SalesbyHour']['number_of_fuel_only_customers']); ?>&nbsp;</td>
                                <td><?php echo h($SalesbyHour['SalesbyHour']['number_fuel_and_merchandise']); ?>&nbsp;</td>
                                <td><?php echo h($SalesbyHour['SalesbyHour']['number_merchandise_customers']); ?>&nbsp;</td>
                                <td><?php echo h($SalesbyHour['SalesbyHour']['number_of_items_this_hour']); ?>&nbsp;</td>
                                <td>$<?php  echo  h($SalesbyHour['SalesbyHour']['value_of_fuel_only_sales']); ?>&nbsp;</td>
                                <td>$<?php echo h($SalesbyHour['SalesbyHour']['value_of_fuel_merchandise_sales']); ?>&nbsp;</td>
                                <td>$<?php echo h($SalesbyHour['SalesbyHour']['value_of_merchandise_sales']); ?>&nbsp;</td>					</tr>
					<?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td><b><?php echo $total[0][0]['hours']; ?></b></td> 
                        <td><b><?php echo $total[0][0]['number_of_fuel_only_customers']; ?></b></td> 
                        <td><b><?php echo $total[0][0]['number_fuel_and_merchandise']; ?></b></td> 
                        <td><b><?php echo $total[0][0]['number_merchandise_customers']; ?></b></td> 
                        <td><b><?php echo $total[0][0]['number_of_items_this_hour']; ?></b></td> 
                        <td>$<b><?php echo $total[0][0]['value_of_fuel_only_sales']; ?></b></td> 
                        <td>$<b><?php echo $total[0][0]['value_of_fuel_merchandise_sales']; ?></b></td> 
                        <td>$<b><?php echo $total[0][0]['value_of_merchandise_sales']; ?></b></td> 
                  
                    </tr>
					 </tfoot>	
						</table>
                       
						<p>
						<?php
						echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>
						<div class="paging">
						<?php
							echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
						?>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<style>
.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }

button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "pageLength": 50
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
	});
</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>