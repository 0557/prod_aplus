<div class="row">&nbsp;</div>
<div class="portlet box blue">
						<div class="portlet-title col-md-12">
							<div class="caption">
								<i class="fa fa-cogs"></i> Sales Inventory Entrry
							</div>
							
						</div>
						<div class="portlet-body">
								<?php echo $this->Form->create('RGroceryItem', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
                     
					   <div class="row">
		  <div class="col-md-12">
<!--		    <div class="col-md-4">
			 <label>Select Department</label>
		<?php echo $this->Form->input('department', array('class' => 'form-control','type'=>'select', 'label' => false, 'required' => true, 'id' =>'department','options'=>$departmentlist,'empty'=>'Filter by department')); ?>
		<br/>
		</div>
	-->	 <div class="col-md-4">
		  <label>Enter PLU No</label>
		<?php echo $this->Form->input('plu', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'pul','placeholder'=>'Filter by PLU No')); ?>
		<br/>
		</div>
		 <div class="col-md-4">
		  <label>Description</label>
		<?php echo $this->Form->input('desc', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'description','placeholder'=>'Description','readonly'=>'true')); ?>
		<br/>
		</div>
		<div class="col-md-2">
		 <label>No of Sales</label>
		<?php echo $this->Form->input('sales_inventory', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'salesinv','placeholder'=>'No of sales')); ?>
		<br/>
		</div>
		 <div class="col-md-2">
		  <label>Date</label>
		<?php echo $this->Form->input('updated_at', array('class' => 'form-control','type'=>'text', 'label' => false, 'required' => true, 'id' =>'saledate','value'=>$rGroceryItems)); ?>
		<br/>
		</div>
		
		<div class="col-md-3">
		  <label>&nbsp;</label>
		<?php echo $this->Form->input('submit', array('class' => 'btn btn-success','type'=>'submit', 'label' => false, 'required' => false,)); ?>
		<br/>
		</div>
		</div>
		</div>	
							
            </div> <?php echo $this->form->end(); ?>
						</div>

<script type="text/javascript" charset="utf-8">
	var webroot='<?php echo $this->webroot;?>';
	function filter_utank() {	
		var from_date=$('#from').val();
		var to_date=$('#to').val();
		
		var department_number=$('#department_number').val();
		//$('#loaderdiv').show();
		$.ajax({
			type : 'POST',
			url : webroot+'ruby_deptotals/index/',
			dataType : 'html',
			data: {from_date:from_date,to_date:to_date,department_number:department_number},
			success:function(result){
				//$("#example_info").hide();example_filter
				//$("#example_paginate").hide();
				//$("#example_filter").hide();
				//$("#example_length").hide();
				//$(".col-md-2").hide();
				$("#filter").show();
				$("#example").html(result);
			}});
	}
</script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({"pageLength": 100});
		
		$('#saledate').Zebra_DatePicker({format:"m-d-Y"});
		$('#pul').keyup(function(){
		
		var pul = $('#pul').val();
//		alert(pul);
		$.ajax({
				url: '<?php echo Router::url('/') ?>admin/salesentry/getdesc',
                type: 'post',
                data: {pul:pul},
                success:function(data){
			//	alert(data);
					$('#description').val(data);
			
                }
            });
		});
/*		$('#department').change(function(){
			$('#pul').val('');
		});*/
});
</script>

