<?php echo $this->Html->script(array('datepicker/bootstrap-datepicker')); ?>
<?php echo $this->Html->css(array('admin/datepicker')); ?>  
<style>
    .search-form-default {
        background: rgb(240, 246, 250) none repeat scroll 0 0;
        margin-bottom: 25px;
        padding: 12px 14px;
    }

    .table-responsive .table-striped th {
        background: #ccc none repeat scroll 0 0 !important;
    }
</style>
<?php echo $this->Form->create('Shiftend', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Shift End
                    </div>

                </div>
            </div>
        </div>


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="dataTables_filter">
                        <label> 
                            <?php echo $this->Form->input('product_id', array('div' => false, 'label' => false, 'required' => 'false', 'empty' => 'Select Product', 'options' => $mixer_product, 'type' => 'select', 'class' => 'form-control input-medium input-inline')); ?>

                        </label>
                    </div>
                </div>




                <!--12-02-2012--><?php // echo date("d-m-Y");  die;  ?>
                <div class="col-md-4">
                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="<?php echo date("d-m-Y"); ?>" class="input-group input-medium date date-picker">
                        <?php echo $this->Form->input('date', array('div' => false, 'label' => false, 'required' => 'false', 'placeholder' => 'Select Date', 'type' => 'text', 'class' => 'form-control')); ?>
                        <span class="input-group-btn">
                            <button type="button" class="btn default"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="dataTables_filter">
                        <label> 
                            <?php echo $this->Form->input('shift_id', array('div' => false, 'label' => false, 'required' => 'false', 'empty' => 'Select Shift', 'type' => 'select', 'class' => 'form-control input-medium input-inline', 'options' => array("1" => "1", "2" => "2", "3" => "3", "4" => "4"))); ?>

                        </label>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <tr>
                                        <th width="10%">Product  </th>
                                        <th width="10%">Open Qty  </th>
                                        <th width="10%"> Purch Qty </th>
                                        <th width="10%"> Avg Cost GL </th>
                                        <th width="10%"> Sales Qty </th>
                                        <th width="10%"> Cost of Sales </th>
                                        <th width="10%"> Book Inventory Qty </th>
                                        <th width="10%"> Stick Reading </th>
                                        <th width="10%"> Short / Over Qty </th>
                                        <th width="10%">Ending  Qty</th>

                                    </tr>
                                    <tr>
                                        <td class="product_mixer"><?php //echo $this->Form->input('product_id', array('class' => 'form-control', 'label' => false, 'type' => 'select', 'empty' => 'Select Product','options'=>$mixer_product, 'required' => 'false', 'placeholder' => '0'));    ?> </td>
                                        <td> <?php echo $this->Form->input('open_qty', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?> </td>
                                        <td> <?php echo $this->Form->input('purchase_qty', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?>  </td>
                                        <td> <?php echo $this->Form->input('avg_cost_gl', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?> </td>
                                        <td> <?php echo $this->Form->input('sale_qty', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?>  </td>
                                        <td><?php echo $this->Form->input('cost_of_sale', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0', "onkeyup" => "add_product()")); ?></td>
                                        <td> <?php echo $this->Form->input('book_inventory_qty', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>
                                        <td> <?php echo $this->Form->input('stick_reading', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?></td>
                                        <td> <?php echo $this->Form->input('Short_over_qty', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>
                                        <td><?php echo $this->Form->input('ending_qty', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>


                                    </tr>

                                    <tr>
                                        <th> Product  </th>
                                        <th> Open Amount($) </th>
                                        <th> Purch Amount ($) </th>
                                        <th>  </th>
                                        <th> Sales Amount ($) </th>
                                        <th>  </th>
                                        <th> Book Inventory Amount ($) </th>
                                        <th>  </th>
                                        <th> Short / Over amount ($) </th>
                                        <th> Ending  Amount ($) </th>


                                    </tr>
                                    <tr>
                                        <td class="product_mixer">  </td>
                                        <td> <?php echo $this->Form->input('open_amount', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?> </td>
                                        <td> <?php echo $this->Form->input('puraches_amount', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?>  </td>
                                        <td> -  </td>
                                        <td> <?php echo $this->Form->input('sales_amount', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => '0', "onkeyup" => "add_product()")); ?>  </td>
                                        <td>-</td>
                                        <td><?php echo $this->Form->input('book_inventory_amount', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>
                                        <td> -</td>
                                        <td><?php echo $this->Form->input('short_over_amount', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>
                                        <td><?php echo $this->Form->input('ending_amount', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly', 'placeholder' => '0')); ?></td>


                                    </tr>

                                </table>
                            </div>

                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>

            </div>
            <!--end tabbable-->

        </div>
        <div class="form-actions" style="text-align:center;">
            <button type="submit" class="btn blue">Submit</button>
            <button type="reset" class="btn default">Reset</button>
            <button type="button" onclick="javascript:history.back(1)"
                    class="btn default">Cancel</button>

            <!--<button type="button" class="btn default">Cancel</button>-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>

</div>

<?php echo $this->form->end(); ?>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>

<script>
    $(document).ready(function () {

        $("#ShiftendDate").click(function () {
            $("#ShiftendDate").val('');
        });
        
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }
        $('#ShiftendProductId').on('change', function () {
            var select_product = $('#ShiftendProductId').find(":selected").text();
            $(".product_mixer").text(select_product);
        });

        add_product();
    });

    function add_product() {
        var avg_cost = 0;
        var Purache_Amount = 0;
        var openning_amount = 0;
        var Purchase_Qty = 0;
        var sale_qty = 0;
        var cost_of_sale = 0;
        var Book_inventory_qty = 0;
        var Open_Qty = 0;
        var Inventory_Amount = 0;
        var Stick_Reading = 0;
        var Short_Over_Qty = 0;
        var Short_Over_Amount = 0;
        var Ending_Qty = 0;
        var Ending_Amount = 0;


        if (parseFloat($('#ShiftendOpenQty').val())) {
            Open_Qty = parseFloat($('#ShiftendOpenQty').val());
        }
        if (parseFloat($('#ShiftendPurachesAmount').val())) {
            Purache_Amount = parseFloat($('#ShiftendPurachesAmount').val());
        }
        if (parseFloat($('#ShiftendOpenAmount').val())) {
            openning_amount = parseFloat($('#ShiftendOpenAmount').val());
        }
        if (parseFloat($('#ShiftendPurchaseQty').val())) {
            Purchase_Qty = parseFloat($('#ShiftendPurchaseQty').val());
        }

        if (isNaN(openning_amount) || isNaN(Purache_Amount) || isNaN(Purchase_Qty) || isNaN(Open_Qty)) {
            openning_amount = 0;
            Purache_Amount = 0;
            Purchase_Qty = 0;
            Open_Qty = 0;
        }

        var first_part = (openning_amount + Purache_Amount);
        var second_part = (Open_Qty + Purchase_Qty);
//        alert(first_part);alert(second_part);
        avg_cost = (first_part / second_part);
//        alert(avg_cost);
        if (isNaN(avg_cost) || !isFinite(avg_cost)) {
            avg_cost = 0;
        }
        //  var divide = parseFloat(Purache_Amount/Open_Qty);
        //  avg_cost = (openning_amount + divide + Purchase_Qty);
        parseFloat($('#ShiftendAvgCostGl').val(avg_cost));
        if (parseFloat($('#ShiftendSaleQty').val())) {
            sale_qty = parseFloat($('#ShiftendSaleQty').val());
        }
        cost_of_sale = avg_cost * sale_qty;
        parseFloat($('#ShiftendCostOfSale').val(cost_of_sale));
        //parseFloat($('#ShiftendSalesAmount').val(cost_of_sale));


        var difference = parseFloat(Purchase_Qty - sale_qty);
        Book_inventory_qty = parseFloat(Open_Qty + difference);

        parseFloat($('#ShiftendBookInventoryQty').val(Book_inventory_qty));

        Inventory_Amount = parseFloat(Book_inventory_qty * avg_cost);

        parseFloat($('#ShiftendBookInventoryAmount').val(Inventory_Amount));
        if (parseFloat($('#ShiftendStickReading').val())) {
            Stick_Reading = parseFloat($('#ShiftendStickReading').val());
        }
        Short_Over_Qty = parseFloat(Book_inventory_qty - Stick_Reading);
        parseFloat($('#ShiftendShortOverQty').val(Short_Over_Qty));

        Short_Over_Amount = parseFloat(Short_Over_Qty * avg_cost);

        parseFloat($('#ShiftendShortOverAmount').val(Short_Over_Amount));
        if (Stick_Reading > 0) {
            Ending_Qty = Short_Over_Qty
        }
        if (Stick_Reading == 0) {
            Ending_Qty = Book_inventory_qty;
        }
        parseFloat($('#ShiftendEndingQty').val(Ending_Qty));
        Ending_Amount = parseFloat(Ending_Qty * avg_cost);
        parseFloat($('#ShiftendEndingAmount').val(Short_Over_Amount));

    }


</script>