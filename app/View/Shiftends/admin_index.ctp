<?php echo $this->Html->script(array('datepicker/bootstrap-datepicker')); ?>
<?php echo $this->Html->css(array('admin/datepicker')); ?>  
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Shifted  <span class="btn green fileinput-button">
                        <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array('action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <?php echo $this->Form->create('Shiftend', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="dataTables_filter">
                        <label> 
                            <?php echo $this->Form->input('product_id', array('div' => false, 'id' => 'pruductChange', 'label' => false, 'required' => 'false', 'empty' => 'Select Product', 'options' => $mixer_product, 'type' => 'select', 'class' => 'form-control input-medium input-inline')); ?>

                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012" class="input-group input-medium date date-picker">
                        <?php echo $this->Form->input('date', array('div' => false, 'label' => false, 'required' => 'false', 'placeholder' => 'Select Date', 'type' => 'text', 'class' => 'form-control')); ?>
                        <span class="input-group-btn">
                            <button type="button" class="btn default"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dataTables_filter">
                        <label> 
                            <?php echo $this->Form->input('shift_id', array('div' => false, 'label' => false, 'required' => 'false', 'empty' => 'Select Shift', 'type' => 'select', 'class' => 'form-control input-medium input-inline', 'options' => array("1" => "1", "2" => "2", "3" => "3", "4" => "4"))); ?>

                        </label>
                    </div>
                </div>

            </div>
            <?php echo $this->form->end(); ?> 
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo 'id'; ?>  </th>
                                            <th>Product Id</th>
                                            <th>Date</th>
                                            <th>Shift Id</th>
                                            <th>Open Qty</th>
                                            <th>Purchase Qty</th>
                                            <th>Short Over Qty</th>
                                            <th>Ending Qty</th>
                                            <th>Ending Amount</th>
                                            <th>Book Inventory Amount</th>
                                            <th>Short Over Amount</th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($shiftend) && !empty($shiftend)) { ?>

                                            <?php foreach ($shiftend as $data) { ?>
                                                <tr>
                                                    <td><?php echo h($data['Shiftend']['id']); ?>&nbsp;</td>
                                                    <td><?php
													 $fproname =  ClassRegistry::init('RubyFprod')->find('first',array('conditions'=>array('RubyFprod.fuel_product_number'=>$data['Shiftend']['product_id'],'RubyFprod.store_id'=>$this->Session->read('stores_id')))); 
													
													 echo @$fproname['RubyFprod']['display_name'];
?></td>

                                                    <td><?php echo h($data['Shiftend']['date']); ?></td>
                                                    <td><?php echo h($data['Shiftend']['shift_id']); ?>&nbsp;</td>
                                                    <td><?php echo h($data['Shiftend']['open_qty']); ?>&nbsp;</td>

                                                    <td><?php echo $data['Shiftend']['purchase_qty'] ?>&nbsp;</td>
                                                    <td><?php echo $data['Shiftend']['Short_over_qty']; ?>&nbsp;</td>
                                                    <td><?php echo h($data['Shiftend']['ending_qty']); ?></td>
                                                    <td><?php echo h($data['Shiftend']['ending_amount']); ?>&nbsp;</td>

                                                    <td><?php echo h($data['Shiftend']['book_inventory_amount']); ?>&nbsp;</td>

                                                    <td><?php echo $data['Shiftend']['short_over_amount']; ?>&nbsp;</td>


                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>', array('action' => 'view', $data['Shiftend']['id']), array('escape' => false,'class' => 'newicon red-stripe view')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('action' => 'edit', $data['Shiftend']['id']), array('escape' => false,'class' => 'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>', array('action' => 'delete', $data['Shiftend']['id']), array('escape' => false,'class' => 'newicon red-stripe delete')); ?>
                                                    </td>

                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>
<script>
    $(document).ready(function () {
        
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }

        $("#pruductChange, #ShiftendDate, #ShiftendShiftId ").change(function () {
            $('#ShiftendAdminIndexForm').submit();
        });


    });
</script>