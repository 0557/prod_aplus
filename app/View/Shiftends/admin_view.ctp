

<style>
    .search-form-default {
        background: rgb(240, 246, 250) none repeat scroll 0 0;
        margin-bottom: 25px;
        padding: 12px 14px;
       
}
     .table-responsive .table-striped th {
  background: #ff9500 none repeat scroll 0 0 !important;
}
</style>

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Shift End View Info :<?php echo $shiftend['Shiftend']['id']; ?>
                    </div>

                </div>
            </div>
        </div>


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
<!--            <div class="col-md-12">
                 BEGIN PAGE TITLE & BREADCRUMB
                <h3 class="page-title">
                    Shift End  

                </h3>

                 END PAGE TITLE & BREADCRUMB
            </div>-->
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="dataTables_filter">
                    
                        <label> 
                           <?php echo $this->Form->input('product_id', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly',"value"=>$shiftend['RwholesaleProduct']['name'])); ?>

                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dataTables_filter">
                        
                       

                        <div class="dataTables_filter">
                            <?php echo $this->Form->input('Short_over_qty', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly',"value"=>$shiftend['Shiftend']['date'])); ?>
                            
                           
                              </div>


                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dataTables_filter">
                        <label> 
                            <?php echo $this->Form->input('Short_over_qty', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly',"value"=>$shiftend['Shiftend']['shift_id'])); ?>
                            
                        </label>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                            
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <tr>
                                        <th width="10%">Product  </th>
                                        <th width="10%">Open Qty  </th>
                                        <th width="10%"> Purch Qty </th>
                                        <th width="10%"> Avg Cost GL </th>
                                        <th width="10%"> Sales Qty </th>
                                        <th width="10%"> Cost of Sales </th>
                                        <th width="10%"> Book Inventory Qty </th>
                                        <th width="10%"> Stick Reading </th>
                                        <th width="10%"> Short / Over Qty </th>
                                        <th width="10%">Ending  Qty</th>

                                    </tr>
                                    <tr>
                                        <td><?php echo $shiftend['RwholesaleProduct']['name']; ?> </td>
                                        <td><?php echo $shiftend['Shiftend']['open_qty']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['purchase_qty']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['avg_cost_gl']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['sale_qty']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['cost_of_sale']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['book_inventory_qty']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['stick_reading']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['Short_over_qty']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['ending_qty']; ?></td>
                                        

                                    </tr>

                                    <tr>
                                        <th> Product  </th>
                                        <th> Open Amount($) </th>
                                        <th> Purch Amount ($) </th>
                                        <th>  </th>
                                        <th> Sales Amount ($) </th>
                                        <th>  </th>
                                        <th> Book Inventory Amount ($) </th>
                                        <th>  </th>
                                        <th> Short / Over amount ($) </th>
                                        <th> Ending  Amount ($) </th>


                                    </tr>
                                    <tr>
                                        <td> <?php echo $shiftend['RwholesaleProduct']['name']; ?> </td>
                                       <td><?php echo $shiftend['Shiftend']['open_amount']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['puraches_amount']; ?></td>
                                        <td>-</td>
                                        <td><?php echo $shiftend['Shiftend']['sales_amount']; ?></td>
                                        <td>-</td>
                                        <td><?php echo $shiftend['Shiftend']['book_inventory_amount']; ?></td>
                                        <td>-</td>
                                        <td><?php echo $shiftend['Shiftend']['short_over_amount']; ?></td>
                                        <td><?php echo $shiftend['Shiftend']['ending_amount']; ?></td>

                                    </tr>

                                </table>
                            </div>
                            
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
                
            </div>
            <!--end tabbable-->
            
        </div>
        
    </div>
    
</div>


<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
        border-color: rgb(221, 221, 221);
        color: rgb(51, 51, 51);
        border: 1px solid rgb(221, 221, 221);
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 6px 12px;
        position: relative;
        text-decoration: none;
    }
</style>

