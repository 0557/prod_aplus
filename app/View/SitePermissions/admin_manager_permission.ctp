<div class="row-fluid sortable">
  <div class="box span12">
    <div class="box-header well" data-original-title>
      <h2><i class="icon-edit"></i> <?php echo __('Add/Edit Set Permissions'); ?></h2>
      <div class="box-icon"> <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a> </div>
    </div>
    <div class="box-content"> <?php echo $this->Form->create('SitePermission',array("class"=>"form-horizontal")); ?>
      <?php echo $this->form->input('user_id',array('type'=>'hidden','escape'=>'false','value'=>$user_id))?>
      <legend>Add/Edit Set Permissions</legend>
	   
         <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                        <thead>
                                            <tr>
                                                <th>Manager</th>
                                                <th>View</th>
                                                <th>Add</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
<?php //pr($managers); die;

	foreach ($managers as $manager):
        
            ?>
                                        <tr>
                                            <td><?php echo $manager['Manager']['name'] ?>&nbsp;</td>
                                            <td>
                                            <label class="checkbox inline">
                
                  <input type="checkbox"   value="1" <?php if(isset($users_permissions[$manager['Manager']['id']]['is_read']) && $users_permissions[$manager['Manager']['id']]['is_read']==1) { ?> checked="checked" <?php } ?> name="data[Permission][is_read][<?php echo $manager['Manager']['id'];?>]"  >
                           </label>
                            </td>

                          <td> <label class="checkbox inline">
                                  <input type="checkbox"  value="1" <?php if(isset($users_permissions[$manager['Manager']['id']]['is_add']) && $users_permissions[$manager['Manager']['id']]['is_add']==1) { ?> checked="checked" <?php } ?> name="data[Permission][is_add][<?php echo $manager['Manager']['id'];?>]" >
                         </label></td>
                                  <td> <label class="checkbox inline">  <input type="checkbox"  value="1" <?php if(isset($users_permissions[$manager['Manager']['id']]['is_edit']) && $users_permissions[$manager['Manager']['id']]['is_edit']==1) { ?> checked="checked" <?php } ?> name="data[Permission][is_edit][<?php echo $manager['Manager']['id'];?>]">  </label></td>
                                   <td><label class="checkbox inline">  <input type="checkbox"  value="1" <?php if(isset($users_permissions[$manager['Manager']['id']]['is_delete']) && $users_permissions[$manager['Manager']['id']]['is_delete']==1) { ?> checked="checked" <?php } ?> name="data[Permission][is_delete][<?php echo $manager['Manager']['id'];?>]"> </label></td>

	    </tr>  
                  
                                   <?php endforeach; ?>
                                  

                                            </tbody>
                                        </table>
      
       <div class="form-actions"> <?php echo $this->Form->submit('Save',array("label"=>false,"div"=>false,"class"=>"btn btn-primary"));?>
        <button type="reset" class="btn" onclick="javascript:history.back(1)">Cancel</button>
      </div>
     
      <?php echo $this->Form->end(); ?> </div>
  </div>
 
</div>

