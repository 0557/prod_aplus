<script type="text/javascript">
jQuery( document ).ready(function() {  
	// validate the form when it is submitted
	$("#StoreAdminAddForm").validate();
	
		$("#StoreCorporationId").rules("add", {
			required:true,
			messages: {
				required: "Please select Corporation"
			}
		});
		$("#StoreName").rules("add", {
			required:true,
			messages: {
				required: "Please enter Name"
			}
		});
	   $("#StoreSalesTax").rules("add", {
			required:true,
			messages: {
				required: "Please enter Sales Tax"
			}
		});
	   $("#StoreEmail").rules("add", {
		    
			required:true,
			email: true,
			messages: {
				required: "Please enter Email"
			}
		});
	   
	   
	   
   
 });  
</script>


<?php echo $this->Form->create('Store', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php echo $this->Html->script(array('admin/custom')); ?>
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Store
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div>
                 <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
                 </div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <!--                            <div class="caption">
                                                            <i class="fa fa-cogs"></i>Product Information
                                                        </div>
                                                        <div class="actions">
                                                            <a href="#" class="btn default btn-sm">
                                                                    <i class="fa fa-pencil"></i> Edit
                                                            </a>
                                                        </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Select Corporation :<span class="star">* </span>
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('corporation_id', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'empty' => 'Select Corporation')); ?>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Name:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Name')); ?>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Business Type:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php $business_type = Configure::read('business_type'); ?>
                                    <?php echo $this->Form->input('business_type', array('class' => 'form-control', 'type' => 'select', 'options' => $business_type, 'required' => 'false', 'id' => 'exampleInputPayroll', 'label' => false, 'placeholder' => 'Enter Payroll Account')); ?>

                                </div>
                            </div>


                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Sales Tax#:	<span class="star">* </span> 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('sales_tax', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Sales Tax#')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Phone No:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('phone', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Phone')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fax:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('fax', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Fax')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Email:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email')); ?>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                           
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Address:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Address')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Country:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('country_id', array('type' => 'select','id'=>'AjaxCountry' ,'class' => 'form-control', 'label' => false, 'required' => 'false', "empty" => "Select Country", 'options' => $countries)); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    State:
                                </div>
                                <div class="col-md-7 value">
                                    <div id="Ajax_State">
                                        <?php echo $this->Form->input('state_id', array("label" => false, 'id'=>'AjaxState',"div" => false, "class" => "form-control", "label" => false, "empty" => "Select State", 'required' => 'false')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    City:
                                </div>
                                <div class="col-md-7 value">
                                    <div id="Ajax_City">
                                        <?php echo $this->Form->input('city_id', array("label" => false,'id'=>'AjaxCity' , "div" => false, "class" => "form-control", "label" => false, "empty" => "Select City", 'required' => 'false')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Zip Code:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div id="Ajax_ZipCode">
                                    <?php echo $this->Form->input('zip_code', array('class' => 'form-control','id'=>'AjaxZipCode' ,'required' => false,'type'=>'select'  ,'label' => false, "empty" => "Select ZipCode")); ?>
                                    </div>
                                </div>
                            </div>



                        </div></div></div>

            </div>




            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
<?php echo $this->form->end(); ?>






