
<div class="page-content">
   
    <div class="row">
	
	<div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>   View Stores Info - <?php echo $store['Store']['name'];     ?>
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">

                <div class="tab-content">

                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        
                                        
                                        <div class="row">
                                        <div class="col-md-6 col-sm-12">
												<div class="portlet yellow box">
                                        <div class="portlet-title">
<!--														<div class="caption">
															<i class="fa fa-cogs"></i>Product Information
														</div>
														<div class="actions">
															<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>
														</div>-->
													</div>
                                                    <div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																Corporation Name:
															</div>
															<div class="col-md-7 value">
																  <?php echo $store['Corporation']['name'];     ?>
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Store Name:
															</div>
															<div class="col-md-7 value">
																  <?php echo $store['Store']['name'];     ?>
																
															</div>
														</div>
                                 										
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																Buisness Type:  
															</div>
															<div class="col-md-7 value">
															   <?php echo $store['Store']['business_type'];     ?>
																
															</div>
														</div>
                                                        
														
                                                                                                               <div class="row static-info">
															<div class="col-md-5 name">
															Sales Tax#: 
															</div>
															<div class="col-md-7 value">
                                                                                                                             <?php echo $store['Store']['sales_tax'];     ?>
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
															Phone No:	 
															</div>
															<div class="col-md-7 value">
                                                                                                                              <?php echo $store['Store']['phone'];     ?>
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
															Fax:	 
															</div>
															<div class="col-md-7 value">
                                                                                                                             <?php echo $store['Store']['fax'];     ?>  
																
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
															Email:	 
															</div>
															<div class="col-md-7 value">
                                                                                                                             <?php echo $store['Store']['email'];     ?>  
																
															</div>
														</div>
                                                        
                                                         
                                                    </div>
                                                    </div>
													</div>
                                                    <div class="col-md-6 col-sm-12">
												<div class="portlet blue box">
													<div class="portlet-title">
<!--														<div class="caption">
															<i class="fa fa-cogs"></i>Manage Inventory
														</div>
														<div class="actions">
															<a href="#" class="btn default btn-sm">
																<i class="fa fa-pencil"></i> Edit
															</a>
														</div>-->
													</div>
                                                    <div class="portlet-body">
                                                    <div class="row static-info">
															<div class="col-md-5 name">
																Address: 
															</div>
															<div class="col-md-7 value">
																 <?php echo $store['Store']['address'];     ?>
															</div>
														</div>
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Country: 
															</div>
															<div class="col-md-7 value">
																 <?php echo $store['Country']['name'];     ?>
															</div>
														</div>
                                                    
														<div class="row static-info">
															<div class="col-md-5 name">
																 State:
															</div>
															<div class="col-md-7 value">
																 <?php echo $store['State']['name'];     ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 City:
															</div>
															<div class="col-md-7 value">
																 <?php echo $store['City']['name'];     ?>
															</div>
														</div>
                                                        
                                                        <div class="row static-info">
															<div class="col-md-5 name">
																 Zip Code::
															</div>
															<div class="col-md-7 value">
																   <?php echo $store['Store']['zip_code'];     ?>
															</div>
														</div>
                                                        
                                                        
                                                       
                                                        </div>
                                                    </div>
													</div>
                                        
                                            
                                 
                                <!-- END FORM-->
                            </div>
                                        
                                        
                                        
                                        
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div></div></div>
    <!-- END PAGE CONTENT-->
   