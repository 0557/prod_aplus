<?php echo $this->Form->create('Globalupcs', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php echo $this->Html->script(array('admin/custom')); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Data
                    </div>
                   
                </div>
            </div>
        </div>

        <div class="form-body col-md-12">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                          
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Starting Inventory:<span class="star">*</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('starting_inventory', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'value' => $Globalupcslist['Globalupcs']['starting_inventory'])); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Cost:<span class="star">*</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('cost', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'value' => $Globalupcslist['Globalupcs']['cost'])); ?>
                                    <?php echo $this->Form->input('id', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'required' => 'false', 'value' => $Globalupcslist['Globalupcs']['id'])); ?>

                                </div>
                            </div>





                       </div></div>

            </div>




            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>

                                <?php echo $this->Html->link('Cancel',array('controller'=>'storesettings','action'=>'index'),array('escape' => false,'class'=>'btn default')); ?>
								
						
        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>


<script type="text/javascript">
    function SelectType(value) {
        if (value == 'corporation') {
            jQuery("#is_corporation_div").show();
        } else {
            jQuery("#is_corporation_div").hide();
        }
    }
    jQuery(document).ready(function () {

        $('#CustomerCorporationId').change(function () {
            var corporation_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/customers/stores",
                data: 'corporation_id=' + corporation_id,
                success: function (data) {
                    $("#store_div").html(data);
                }
            });

        });
    });

</script>







