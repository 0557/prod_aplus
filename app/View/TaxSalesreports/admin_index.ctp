
<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >
		<div class="rubyDcrstats index">
			<h3 class="page-title"><?php echo __('Tax Sales Report'); ?></h3>   
			
			 <?php echo $this->Form->create('tax_salesreports', array('role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 
	?>			
		<div class="row">
         <div class="col-md-12">
            <div class=" col-md-2">Update Date</div>
            <div class="col-md-3">            
            <?php
			//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';die;
			//echo 'pos_type::'.$this->Session->read('pos_type');die;
			if($this->Session->read('pos_type')=='ruby2'){
			include('test.ctp');
			}else{
			?>            
            <input type="text"  placeholder="Select End Date" class="form-control icon_position" id="end_date" name="end_date" value="<?php echo $end_date;?> ">                  
            <?php 
			} 
			?>    
            </div>
            <div class=" col-md-1">
            <button  class="btn btn-success search_button" type="submit">Search</button>
            </div> 
            <div class=" col-md-1">
           		  <a href="<?php echo Router::url('/');?>admin/tax_salesreports/reset" class="btn btn-warning ">Reset</a>  
            </div>             
            <?php echo $this->form->end(); ?>	 
			<?php             			
			if($this->Session->read('pos_type')=='ruby2'){
            echo $this->Form->create('TaxSalesreport', array('action'=>'ruby2tax','role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 		
            ?>
            <div class="col-md-2">            
            <?php 
			echo $this->Form->input('full_date', array('type'=>'hidden', 'label' => false, 'value'=>$full_date)); 
			?>
            <?php 
			echo $this->Form->input('Generate Excel', array('class' => 'btn btn-info sow','type'=>'submit', 'label' => false, 'required' => false,));
			?>            
            </div>
            <?php 
            echo $this->form->end();
            }else{
			
            echo $this->Form->create('TaxSalesreport', array('action'=>'taxsale','role' => 'form'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); 		
            ?>
            <div class="col-md-2">            
            <?php 
			echo $this->Form->input('end_date', array('type'=>'hidden', 'label' => false, 'value'=>$end_date)); 
			?>
            <?php 
			echo $this->Form->input('Generate Excel', array('class' => 'btn btn-info sow','type'=>'submit', 'label' => false, 'required' => false,));
			?>            
            </div>
            <?php 
            echo $this->form->end();
            
			}
            ?>            
            
            </div>
		</div>
        
			
			<div class="row">
				<div class="col-md-12">
					<div class="block-inner clearfix tooltip-examples">
					
						<div class="datatable">
                        <?php 
						if($this->Session->read('pos_type')=='ruby2'){
							//echo '<pre>'; print_r($TaxSalesreport);
							
							?>
                        <table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
                        		<th><?php echo 'Date'; ?></th>
								<th><?php echo 'Taxable Sales'; ?></th>
								<th><?php echo 'Non Taxable Sales'; ?></th>
								<th><?php echo 'Sales Tax'; ?></th>
                                <th><?php echo 'Refund Tax'; ?></th>
							    <th><?php echo 'Net Tax'; ?></th>
                                
						</tr>
						</thead>
						<tbody>
						<?php 						
						$tot_taxable_sales='';
						$tot_non_taxable_sales='';
						$tot_sales_tax='';
						$tot_refund_tax='';
						$tot_net_tax='';
						foreach ($Ruby2Taxs as $Ruby2Tax):
						$tot_taxable_sales=$tot_taxable_sales+$Ruby2Tax['Ruby2Tax']['taxable_sales'];
						$tot_non_taxable_sales=$tot_non_taxable_sales+$Ruby2Tax['Ruby2Tax']['non_taxable_sales'];
						$tot_sales_tax=$tot_sales_tax+$Ruby2Tax['Ruby2Tax']['sales_tax'];
						$tot_refund_tax=$tot_refund_tax+$Ruby2Tax['Ruby2Tax']['refund_tax'];
						$tot_net_tax=$tot_net_tax+$Ruby2Tax['Ruby2Tax']['net_tax'];
						?>
						<tr>                       			
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['period_end_date']); ?>&nbsp;</td>
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['taxable_sales']); ?>&nbsp;</td>
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['non_taxable_sales']); ?>&nbsp;</td>
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['sales_tax']); ?>&nbsp;</td>
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['refund_tax']); ?>&nbsp;</td>
                                <td><?php echo h($Ruby2Tax['Ruby2Tax']['net_tax']); ?>&nbsp;</td>
                               
                        </tr>
					    <?php endforeach; ?>                    
						</tbody>                     
                          <thead>
                            <tr>
                                    <th><?php echo 'Total'; ?></th>
                                    <th><?php echo $tot_taxable_sales; ?></th>
                                    <th><?php echo $tot_non_taxable_sales; ?></th>
                                    <th><?php echo $tot_sales_tax; ?></th>
                                    <th><?php echo $tot_refund_tax; ?></th>
                                    <th><?php echo $tot_net_tax; ?></th>
                                    
                            </tr>
						  </thead>                      
						</table>
                        <?php
						}else{
						?>
						<table class="table table-striped table-bordered table-advance table-hover" id="example">
						<thead>
						<tr>
                        		<th><?php echo $this->Paginator->sort('tax_table_index'); ?></th>
								<th><?php echo $this->Paginator->sort('total_taxes'); ?></th>
								<th><?php echo $this->Paginator->sort('total_taxable_sales'); ?></th>
								<th><?php echo $this->Paginator->sort('total_exempt_taxes'); ?></th>
								<th><?php echo $this->Paginator->sort('total_tax_exempt_sales'); ?></th>
							
						</tr>
						</thead>
						<tbody>
						<?php foreach ($TaxSalesreports as $TaxSalesreport): ?>
						<tr>
                            <td><?php echo h($TaxSalesreport['TaxSalesreport']['tax_table_index']); ?>&nbsp;</td>
                            <td><?php echo h($TaxSalesreport['TaxSalesreport']['total_taxes']); ?>&nbsp;</td>
                            <td><?php echo h($TaxSalesreport['TaxSalesreport']['total_taxable_sales']); ?>&nbsp;</td>
                            <td><?php echo h($TaxSalesreport['TaxSalesreport']['total_exempt_taxes']); ?>&nbsp;</td>
                            <td><?php echo h($TaxSalesreport['TaxSalesreport']['total_tax_exempt_sales']); ?>&nbsp;</td>
                        </tr>
					<?php endforeach; ?>
                    
                    
						</tbody>
						</table>
                        <p>
						<?php
						echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>
						<div class="paging">
						<?php
							echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
						?>
						</div>
                        
                        <?php
					    }
						?>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<style>
.Zebra_DatePicker_Icon_Inside{ margin-top: 0px!important; }

button.Zebra_DatePicker_Icon {
    border-radius: 0 3px 3px;
    left: auto !important;
    right: 30px;
    top: 1px !important;
}
.Zebra_DatePicker {
    position: absolute;
    background: #3a4b55;
    border: 1px solid #3a4b55;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    display: none;
    z-index: 1000;
    margin-left: -224px;
    top: 191px!important;
    font-family: Tahoma,Arial,Helvetica,sans-serif;
    font-size: 13px;
    margin-top: 0;
    z-index: 10000;
}
</style>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
	  $('#example').DataTable({
	  "pageLength": 50
	  });
		//$('#end_date').Zebra_DatePicker({direction: -1,format:"Y-m-d"});
	});
</script>
  <?php
  if($this->Session->read('pos_type')!='ruby2'){	 
  ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>
  <?php
  }
  ?>