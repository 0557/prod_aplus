
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
               <div class="page-content portlet-body">
  <div class="content-new">
      <div class="row">
        <div class="col-xs-12">
          <h3 class="page-title">Users
		  
		  	<a href="<?php echo Router::url('/');?>admin/users/new_add/" class="btn  notfinew default btn-xs red-stripe"  ><i class="fa fa-plus"></i> Add New</a>
</h3>
        </div>
      </div>
    </div>

  <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
					<div class="row"><br/>
				</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                
                                            <th><?php echo $this->Paginator->sort('Store'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Email'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Registered Date'); ?></th>
                                           
                                                <th><?php echo $this->Paginator->sort('Action'); ?></th>
                                            
                                         
                                              </tr>
                                    </thead>
                                    <tbody>
                               <?php         if (isset($usersdata) && !empty($usersdata)) { 
							   
					
							$ms = "'Are you sure want delete it?'";
                                             foreach ($usersdata as $data) { 
											 if($data["User"]["store_id"]!=""){
                                               echo '<tr>
                                                      
                                                      <td>'.$data["User"]["store_id"].'</td>
                                                    <td>'.$data["User"]["username"].'&nbsp;</td>
                                                    <td>'.$data["User"]["email"].'&nbsp;</td>
                                                    <td>'.$data["User"]["created"].'&nbsp;</td>
                                                   
                                                 	<td>   
													<a href="'.Router::url('/').'admin/users/edit/'.$data["User"]["id"].'" class="newicon  red-stripe"><img src="'.Router::url('/').'img/edit.png"/></a>
													<a href="'.Router::url('/').'admin/users/add/'.$data["User"]["id"].'"  onClick="return confirm('.$ms.');" class="newicon red-stripe" ><img src="'.Router::url('/').'img/delete.png"/></a>
												';
												echo '</td>
                                               
												</tr>';
												}
											}
                                              
                                       } else { 
                                           echo ' <tr>
                                                <td colspan="10">  no result found </td>
                                            </tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
		
        </div>
   

    </div>


    
  



