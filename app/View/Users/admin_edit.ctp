
<?php echo $this->Html->script(array('admin/custom')); ?>
 <?php echo $this->Form->create('User', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Company
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div>
                  <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
                </div>

        <div class="form-body">
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
						</div>
						
                        <div class="portlet-body">
						
							<?php /*?><div class="row static-info" style="display:none;">
                                <div class="col-md-5 name">
                                   Select Store:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('store_id', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Select Store', 'default'=>$userdetails['User']['store_id'])); ?>

                                </div>
                            </div><?php */?>
 <div class="row">&nbsp;</div>
							  <div class="row static-info">
                                <div class="col-md-5 name">
                                   Name:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('username', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Name','value'=>$userdetails['User']['username'])); ?>

                                </div>
                            </div>
							 <div class="row">&nbsp;</div>
						  <div class="row static-info">
                                <div class="col-md-5 name">
                                   Email:<span class="star">* </span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email', 'value'=>$userdetails['User']['email'], 'readonly'=>'readonly')); ?>
									
	                                  			  <?php echo $this->Form->input('id', array('class' => 'form-control', 'required' => 'false','type'=>'hidden', 'label' => false, 'placeholder' => 'Enter Email', 'value'=>$userdetails['User']['id'])); ?>			

                                </div>
                            </div>
 <div class="row">&nbsp;</div>
                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Password:<span class="star">* </span>			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('oldpassword', array('class' => 'form-control',  'required' => 'false', 'label' => false, 'placeholder' => 'Enter Password')); ?>

                                </div>
                            </div>
                                                 
                        
               
            </div>
             <div class="row">&nbsp;</div>
             <div class="form-actions" style="text-align:right;">
        <button type="submit" class="btn blue">Update</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>
	</div>


            <!-- END FORM-->
        </div>


    </div>
   

</div>
<?php echo $this->form->end(); ?>
