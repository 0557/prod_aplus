<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.dynamic.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.gauge.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.core.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.effects.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.vprogress.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
 <?php
   echo $this->Html->css(array('dashboard'));
 ?> 
<style>
.btnstl{border-radius: 100% !important;
   height: 18px;
    width: 20px;
    background-color: #F00;
    border: none;}
	.btnstlg{border-radius: 100% !important;
   height: 18px;
    width: 20px;
    background-color: #090;
    border: none;}
</style>
<div class="page-content">
            <?php echo $this->Form->create('User', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
            <div class="row">
                                    <div class="col-md-3 col-xs-6">
                                        <label>Report date</label>                          
                                        <?php
                                        include('multidatepicker.ctp');
                                        ?>
                                    </div>
                                  
                                    <div class="col-md-4 ">
                                        <label>&nbsp;</label>
                                        <button type="submit" name ="submit" class="btn default updatebtn" style=" margin-top:24px;">Run report</button>
                                        <label>&nbsp;</label>
                                        <a style="margin-top:24px; background-color: #999;color:#FFF;" href="<?php echo Router::url('/'); ?>admin/users/index" class="btn" id="rstbtn">Reset</a> 

                                    </div>

              </div>
            <?php echo $this->form->end(); ?>
             <br />
             <br />
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="details">
							<div class="number">
						    Gas Sales
							</div>
							<div class="desc">
                            $<?php if($total_gas_sales!=''){echo number_format($total_gas_sales,2);}else{echo '0.00';} ?>
							</div>
						</div>
					</div>
				</div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						
						<div class="details">
							<div class="number">
                             Grocery Sales
							</div>
							<div class="desc">
                            $<?php if($total_merchandise_sales!=''){echo number_format($total_merchandise_sales,2);}else{echo '0.00';} ?>
							</div>
						</div>
						
					</div>
				</div>
                
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						
						<div class="details">
							<div class="number">
                             Lottery Sales
							</div>
							<div class="desc">
							$<?php if($daily_reportings['total_lottery_sales']!=''){echo number_format($daily_reportings['total_lottery_sales'],2);}else{echo '0.00';} ?>
							</div>
						</div>
						
					</div>
				</div>
                
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						
						<div class="details">
							<div class="number">
						      ATM Amount
							</div>
							<div class="desc">
						    $<?php if($daily_reportings['total_atm_amount']!=''){echo number_format($daily_reportings['total_atm_amount'],2);}else{echo '0.00';} ?>
							</div>
						</div>
						
					</div>
				</div>
                
				
                
			</div>
            <div class="row">
            
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="details">
							<div class="number">
                            Gallon Purchase
							</div>
							<div class="desc">							
                             $<?php if($purchase_invoices['total_gross_amount']!=''){echo number_format($purchase_invoices['total_gross_amount'],2);}else{echo '0.00';} ?>
                             
							</div>
						</div>
					</div>
				</div>     
                
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="details">
							<div class="number">
							Grocery Purchase
							</div>
							<div class="desc">
                               $<?php if($grocery_invoices['total_gross_amount']!=''){echo number_format($grocery_invoices['total_gross_amount'],2);}else{echo '0.00';} ?>
							</div>
						</div>
						
					</div>
				</div>
                
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="details">
							<div class="number">
								Gas Credit
							</div>
							<div class="desc">
                              $0.00
							</div>
						</div>
					</div>
				</div>
                
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="details">
							<div class="number">
                             Gas Cash 
							</div>
							<div class="desc">
								$0.00
							</div>
						</div>
						
					</div>
				</div>
     
                
			</div>
            
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					 <strong>Graph Gas Sales</strong>
                    <img src="<?php echo $this->webroot; ?>/images/GasDashboards.png" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <strong>Graph Merchandise Sales</strong>
				<img src="<?php echo $this->webroot; ?>/images/GasDashboards.png" />	
				</div>
     
			</div>
            
             <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                        <th><?php echo __($this->Paginator->sort('Store Id')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('Store Name')); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('Date added'); ?></th>
                                            <th><?php echo $this->Paginator->sort('Last Date'); ?></th>
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($storedata) && !empty($storedata)) {
											//echo'<pre>'; print_r($storedata);
											 ?>
                                           <?php foreach ($storedata as $data) {
											  // echo'<pre>'; print_r($data);
											 /* echo "SELECT ending_date_time from ruby_headers where store_id=".$data['stores']['id']." order by id desc limit 0,1"; die;*/
											   $results2= ClassRegistry::init('RubyHeader')->query("SELECT ending_date_time from ruby_headers where store_id=".$data['stores']['id']." order by id desc limit 0,1"); 
											 
											    ?>
                                                <tr>
                                                 <td><?php echo $data['stores']['id']; ?>&nbsp;</td>
                                                    <td><?php echo $data['stores']['name']; ?>&nbsp;</td>
                                                    <td><?php echo $data['ruby_headers']['beginning_date_time']; ?></td>
                                                   <td><?php echo  $results2[0]['ruby_headers']['ending_date_time'] ;  ; ?>&nbsp;</td>
                                                       <!--  <td><?php echo date('m-d-Y', strtotime($data['RBankAccount']['create_date'])); ?>&nbsp;</td>-->
														 
                               <td >
								<?php 
								if($data['ruby_headers']['ending_date_time']!='')
								{
                                $stop_date = date('Y-m-d', strtotime($data['ruby_headers']['ending_date_time'] . ' +1 day'));
                                $date = date('Y-m-d');
                                if($stop_date==$date){
                                ?>
                                <img src="<?php echo $this->webroot.'images/green.png'; ?>">
                                <?php
                                }else{
                                ?>
                                <img src="<?php echo $this->webroot.'images/purple.png'; ?>">
                                <?php
                                }
								}
                                ?>
                               </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="5">  no result found </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--<div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>-->
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div> 
 
     <div class="clearfix">
     
  </div>               

</div>
<?php
if ($full_date == '' || $full_date == '// - //') {
    ?>        
        <script>
            $(document).ready(function () {
                $('#config-demo').val('');
            });
        </script>
    <?php
}
?>
<style>
#rstbtn:hover { background-color: #2487fc !important;}
</style>