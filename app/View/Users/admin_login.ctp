<!-- BEGIN LOGIN FORM -->
<?php echo $this->Form->create('User',array('class'=>'login-form')); ?>
<!--<form class="login-form" action="index.html" method="post">-->
    <h3 class="form-title">Login to your account</h3>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span>
            Enter any username and password.
        </span>
    </div>   
    
    <div class="form-group">     
        <label class="control-label visible-ie8 visible-ie9">Type</label>
        <div class="input-icon">           
            <?php 
			$options=array('company'=>'Company','store'=>'Store');
			echo $this->Form->input('User.type',array('id'=>'login_type','class'=>'form-control','type'=>'select','label'=>false,'options'=>$options)); ?>           
        </div>
    </div>
    
    
    <div class="form-group" id="emaildv">        
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <?php echo $this->Form->input('User.email',array('class'=>'form-control placeholder-no-fix','autocomplete'=>'off','placeholder'=>'Username','label'=>false, 'type'=>'text', 'maxlength'=>100, 'id'=>'usrnminpt')); ?>            
        </div>
    </div> 
    
    
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <?php echo $this->Form->input('User.password',array('class'=>'form-control placeholder-no-fix','type'=>'password' ,'autocomplete'=>'off','placeholder'=>'Password','label'=>false)); ?>
            <!--<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>-->
        </div>
    </div>
    <div class="form-actions">
        <label class="checkbox">
            <?php echo $this->Form->input("remember_me", array("type" => "checkbox", "label" => false,"div"=>false,  "value" => "1"));?>
        Remember me
            <!--<input type="checkbox" name="remember" value="1"/> Remember me--> </label>
        <button type="submit" class="btn green pull-right">
            Login <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>
  
    <div class="forget-password">
        <h4>Forgot your password ?</h4>
        <p>
            no worries, click
            <a href="javascript:;" id="forget-password">
                here
            </a>
            to reset your password.
        </p>
    </div>

</form>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="index.html" method="post">
    <h3>Forget Password ?</h3>
    <p>
        Enter your e-mail address below to reset your password.
    </p>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        </div>
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn">
            <i class="m-icon-swapleft"></i> Back </button>
        <button type="submit" class="btn green pull-right">
            Submit <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->