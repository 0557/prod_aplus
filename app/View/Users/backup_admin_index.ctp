
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.dynamic.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.gauge.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.core.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.common.effects.jss"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/dashboard/RGraph.vprogress.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>

<style>
.bar-cont {
	width: 200px;
	float: right;
}
.bar-cont1 {
	width: 100%;
	margin: 0;
}
.bar-regular {
	background: #3366cc none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-plus {
	background: #dc3912 none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-super {
	background: #ff9600 none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-diesel {
	background: #19981f none repeat scroll 0 0;
	float: left;
	height: 13px;
	margin: 0 10px 0 0;
	width: 76px;
}
.bar-reg {
	width: 90px;
	float: left;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif
}
.percentage {
	font-family: arial;
	font-weight: 600;
	text-align: right;
	width: 74%;
}
.gas-dashboard {
	margin: 10px 0 40px;
	overflow: hidden;
	width: 300px;
	float: left;
}
.gas-dashboard1 {
	margin: 10px 0 40px;
	overflow: hidden;
	width: 300px;
	text-align: right;
	float: right
}
.graph-bar {
	width: 100%;
	position: relative;
	overflow: hidden;
	margin: 40px 0 75px 0;
	background: #e0e6f0;
	padding: 29px 0 0 20px;
}
.gas-control {
	background: #fff;
	border: 1px solid #ddd;
	overflow: hidden;
	margin: 0 0 30px 20px;
}
.gas-control1 {
	background: #dff0d8;
	padding: 10px;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif
}
.triangle {
	float: left;
	overflow: hidden;
	padding: 9px 0 0 11px;
	width: 34px;
}
.triangle-text {
	float: left;
	font-family: arial;
	font-size: 12px;
	overflow: hidden;
	padding: 12px 0 0;
	width: 115px;
}
.main-cont {
	width: 1170px;
	margin: 0 auto;
}
.col-left {
	width: 50%;
	position: relative;
	overflow: hidden;
	float: left;
}

.backoffice-chart{width:900px;
 height:350px;
 float: left; 
 }
 
 
 
 
@media only screen and (max-width: 1160px)
  
   {
   	.main-cont {
    margin: 0 auto;
    width: 100%;
}

}


@media only screen 
  and (min-width: 768px) 
  and (max-width: 1024px) 
  {
.bar-cont {
    float: right;
    width: 127px;
}
.bar-cont1 {
    margin: 0 0 4px;
    overflow: hidden !important;
    width: 100%;
}

.bar-regular {
    background: #3366cc none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}

.bar-reg {
    float: left;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    width: 49px;
}
.bar-plus {
    background: #dc3912 none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}
.bar-super {
    background: #ff9600 none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}
.bar-diesel {
    background: #19981f none repeat scroll 0 0;
    float: left;
    height: 13px;
    margin: 0 10px 0 0;
    width: 65px;
}

}




</style>

<div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    Widget settings form goes here
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn blue">Save changes</button>
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                   
                
                    <!-- BEGIN DASHBOARD STATS -->
                  
                  <div class="main-cont">
  <div class="gas-dashboard">Gas dashboard</div>
  <!--<div class="gas-dashboard1">Sort by Year | Month | Date</div>-->
 <div class="graph-bar">
    <canvas style="float: left; width: 82%; margin:0 0 20px 0;" height="350" width="900" id="cvs2">[No canvas support]</canvas>
     <div class="bar-cont">
      <?php foreach($product as $cat) { ?>
      <div class="bar-cont1" style="overflow: hidden;">
        <div class="bar-<?php  echo strtolower( $cat['RubyFprod']['display_name']); ?>"></div>
       
        <div class="bar-reg"><?php echo $cat['RubyFprod']['display_name']; ?></div>
      </div>
      <?php  } ?>
      
    </div>
    <!--bar content div ends--> 
    
    <script>
    window.onload = function ()
    {
         var bar = new RGraph.Bar({
            id: 'cvs2',
            data: [[3,4,6,5],[2,5,3,2],[1,5,2,6],[1,4,6,16],[1,6,8,1],[1,6,8,7],[2,5,3,5],[2,5,3,4],[1,4,6,2],[1,4,6,1] ,[1,4,6,5],[1,4,6,8],[1,4,6,7]],
            options: {
                backgroundGridVlines: false,
                backgroundGridBorder: false,
                noyaxis: true,
                labels:['dec', 'Jan', 'Feb', 'March', 'April', 'May', 'June', 'July' , 'Aug' , 'Sept' , 'Oct' , 'Nov'],
                colors: ['Gradient(#3366cc:#27afe9:#058DC7:#058DC7:#058DC7)', 'Gradient(#3366cc:#50B332:#B1E59F:#058DC7)', 'Gradient(#3366cc:#EC561B:#F59F7D)'],
                hmargin: 15,
                strokestyle: 'white',
                linewidth: 1,
                shadowColor: '#ccc',
                shadowOffsetx: 0,
                shadowOffsety: 0,
                textSize: 10,
                shadowBlur: 10
            }
        }).wave({frames:30})
    };
	</script> 
  </div>
  <!--canvas1 div ends-->
 <!-- <div class="gas-control">
    <div class="gas-control1">Gas Control points as of 21.12.2015</div>
    <div style="background:#000;">
      <div class="triangle"><img src="<?php echo $this->webroot; ?>/images/triangle.jpg" /></div>
      <div class="triangle-text">123456</div>
      <div class="triangle"><img src="<?php echo $this->webroot; ?>/images/triangle.jpg" /></div>
      <div class="triangle-text">123456</div>
      <div class="triangle"><img src="<?php echo $this->webroot; ?>/images/triangle.jpg" /></div>
      <div class="triangle-text">123456</div>
    </div>
  </div>-->
  <!--<div class="col-left">
    <div class="percentage">Sales By Percentage</div>
    <canvas id="cvs1" width="625" height="350">[No canvas support]</canvas>
    <script>
       
        {
            var pie = new RGraph.Pie({
                id: 'cvs1',
                data: [1,8,6,3,5,2,5],
                options: {
                    shadow: true,
                    shadowOffsetx: 0,
                    shadowOffsety: 5,
                    shadowColor: '#aaa',
                    radius: 80,
                    variant: 'donut3d', 
                    labels: ['Regular 20%','Hennings','Luis','Plus25%','John','Luis','Pete'],
                    labelsSticks: true,
                    labelsSticksLength: 15,
					gutterTop: 35,
            gutterLeft: 0,
            gutterRight: 5,
            gutterBottom: 10,
                }
            }).fadeIn();
        };
    </script> 
  </div>--->
  <!--canvas 2 div ends-->
  
  <!--<div class="col-left">
    <div class="percentage">Total Collection Cash || Crd|| Check</div>
    <canvas id="cvs3" width="625" height="350">[No canvas support]</canvas>
    <script>
       
        {
            var pie = new RGraph.Pie({
                id: 'cvs3',
                data: [1,8,6,3,5,2,5],
                options: {
                    shadow: true,
                    shadowOffsetx: 0,
                    shadowOffsety: 5,
                    shadowColor: '#aaa',
                    radius: 80,
                    variant: 'donut3d', 
                    labels: ['Regular 20%','Hennings','Luis','Plus25%','John','Luis','Pete'],
                    labelsSticks: true,
                    labelsSticksLength: 15,
					gutterTop: 35,
            gutterLeft: 10,
            gutterRight: 5,
            gutterBottom: 10,
                }
            }).fadeIn();
        };
    </script> 
  </div>-->
  <!--canvas 3 ends-->
  
 <!-- <div style="width:100%; position:relative; overflow:hidden; margin:20px 0 75px 0;">
    <div class="percentage">Sales By Hours || Customer Visited</div>
    <canvas id="cvs4" width="900" height="350" style="float:left">[No canvas support]</canvas>
    <script>
    var line = new RGraph.Line({
        id: 'cvs4',
        data: [8,9,15,16,21,23, 14, 4,13,19,18,11],
        options: {
            numxticks: 11,
            numyticks: 5,
            backgroundGridVlines: false,
            backgroundGridBorder: false,
            colors: ['red'],
            linewidth: 2,
            gutterLeft: 40,
            gutterRight: 20,
            gutterBottom: 50,
            labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            shadow: false,
            noxaxis: true,
            textSize: 16,
            textColor: '#999',
            textAngle: 0,
            spline: true,
            hmargin: 5,
            clearto: 'white',
            tickmarks: function (obj, data, value, index, x, y, color, prevX, prevY)
            {
                var co = obj.context,
                    ra = 4;
                        
                // Clear a white space
                RGraph.path2(co, 'b a % % % 0 6.28 false f white', x, y, ra + 6);

                // Draw the center of the tickmark
                RGraph.path2(co, 'b a % % % 0 6.28 false c f #f00', x, y, ra);
                
                // Draw the outer ring
                RGraph.path2(co, 'b a % % % 0 6.28 false', x, y, ra + 1);
                RGraph.path2(co, 'a % % % 6.28 0 true c f rgba(255,200,200,0.5)', x, y, ra + 5);
            }
        }
    }).trace2({frames: 60});
</script> 
  </div>-->
  <!--canvas 4 ends-->
  
  <!--<canvas id="cvs5" width="1100" height="250"> [No canvas support] </canvas>-->
  <!---<script>
    var gauge1 = new RGraph.Gauge({
                id: 'cvs5',
                min: 0,
                max: 100,
                value: 84,
                options: {
					title: 'Volume',
                titleBottom: 'Pa',
                titleBottomColor: '#aaa',
                centerx: 125,
                radius: 100,
                adjustable: true,     
                    border: {
                        outer: 'Gradient(white:white:white:white:white:white:white:white:white:white:#aaa)'
                    }
                }
            }).grow({frames: 25});
			
			var gauge2 = new RGraph.Gauge({
                id: 'cvs5',
                min: 0,
                max: 100,
                value: 84,
                options: {
					title: 'Volume',
                titleBottom: 'Pa',
                titleBottomColor: '#aaa',
                centerx: 340,
                radius: 100,
                adjustable: true,     
                    border: {
                        outer: 'Gradient(white:white:white:white:white:white:white:white:white:white:#aaa)'
                    }
                }
            }).grow({frames: 25});
			
			var gauge3 = new RGraph.Gauge({
                id: 'cvs5',
                min: 0,
                max: 100,
                value: 84,
                options: {
					title: 'Volume',
                titleBottom: 'Pa',
                titleBottomColor: '#aaa',
                centerx: 560,
                radius: 100,
                adjustable: true,     
                    border: {
                        outer: 'Gradient(white:white:white:white:white:white:white:white:white:white:#aaa)'
                    }
                }
            }).grow({frames: 25});
			
			var gauge4 = new RGraph.Gauge({
                id: 'cvs5',
                min: 0,
                max: 100,
                value: 84,
                options: {
					title: 'Volume',
                titleBottom: 'Pa',
                titleBottomColor: '#aaa',
                centerx: 780,
                radius: 100,
                adjustable: true,     
                    border: {
                        outer: 'Gradient(white:white:white:white:white:white:white:white:white:white:#aaa)'
                    }
                }
            }).grow({frames: 25});
</script>--> 
  
  <!--canvas 5 ends--> 
</div>
                  
                    <!-- END DASHBOARD STATS -->
                    <div class="clearfix">
                    </div>
                 
                   
                    
                   
                </div>
