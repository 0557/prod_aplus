<script type="text/javascript">
jQuery( document ).ready(function() {  
	// validate the form when it is submitted
	$("#CustomerAdminAddVendorForm").validate(

	);
	
		$("#CustomerRetailType").rules("add", {
			required:true,
			messages: {
				required: "Please select Vendor type"
			}
		});
		$("#CustomerName").rules("add", {
			required:true,
			messages: {
				required: "Please enter Vendor name."
		}
	});    
   
 });  
</script>
<?php $option = Configure::read('bool'); ?>
<?php $type = Configure::read('type'); ?>
<?php echo $this->Form->create('Customer', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<?php echo $this->Html->script(array('admin/custom')); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Vendors
                    </div>
                   
                </div>
            </div>
            <p style="padding-left:20px;"><span class="star" >*</span> for mandetory field</p>
        </div>

        <div class="form-body col-md-12">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body extra_tt">
                          
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Name:<span class="star">*</span>	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Name')); ?>

                                </div>
                            </div>




                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Contact Person:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('contact_person', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Contact Person')); ?>

                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    GL:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('gl', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter GL')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Terms:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('terms', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Terms')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Email:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'placeholder' => 'Enter Email')); ?>

                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    E D I Parser:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('edi_parser', array('class' => 'form-control', 'options' => $option, 'required' => false, 'label' => false)); ?>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                           
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Address:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Address')); ?>
                                </div>
                            </div>

                          
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Phone:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('phone', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Phone')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Fax:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('fax', array('class' => 'form-control', 'required' => false, 'label' => false, 'placeholder' => 'Enter Fax')); ?>
                                </div>
                            </div>



                        </div></div></div>

            </div>




            <!-- END FORM-->
        </div>


    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>


<script type="text/javascript">
    function SelectType(value) {
        if (value == 'corporation') {
            jQuery("#is_corporation_div").show();
        } else {
            jQuery("#is_corporation_div").hide();
        }
    }
    jQuery(document).ready(function () {

        $('#CustomerCorporationId').change(function () {
            var corporation_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo Router::url('/') ?>admin/customers/stores",
                data: 'corporation_id=' + corporation_id,
                success: function (data) {
                    $("#store_div").html(data);
                }
            });

        });
    });

</script>







