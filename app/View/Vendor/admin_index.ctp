<?php if(@$grocerytab){
$type = "/grocery";
}else{
$type="";
} ?>

<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Vendors  <span class="btn green fileinput-button">
                    <?php echo $this->Html->link('<i class="fa fa-plus"></i><span>Add New</span>', array('controller' => 'vendor', 'action' => 'add'.$type), array('escape' => false)); ?>

                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                            
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo __($this->Paginator->sort('Vendor Id')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('name')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('store' ,'Store')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('address','Address')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('email','Email')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('contact_person','Contact Person')); ?>   </th>
                                            <th><?php echo __($this->Paginator->sort('gl','GL#')); ?>  </th>
                                           
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($vendor) && !empty($vendor)) { ?>
                                            <?php foreach ($vendor as $data) { ?> 
                                                <tr>
                                                    <td> <?php echo $data['Customer']['id']; ?> </td>
                                                    <td> <?php echo $data['Customer']['name']; ?> </td>
                                                    <td> <?php echo $data['Store']['name']; ?> </td>
                                                    <td>  <?php echo $data['Customer']['address']; ?>  </td>
                                                    <td>  <?php echo $data['Customer']['email']; ?>  </td>
                                                    <td> <?php echo $data['Customer']['contact_person']; ?> </td>
                                                    <td> <?php echo $data['Customer']['gl']; ?>  </td>
                                                 
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>',array('controller'=>'vendor','action'=>'edit/'.$data['Customer']['id'].$type),array('escape' => false,'class'=>'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>',array('controller'=>'vendor','action'=>'index',$data['Customer']['id']) ,array('escape' => false,'class'=>'newicon red-stripe delete')); ?>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="9">  no result found </td>
                                                </tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>