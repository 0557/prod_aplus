
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers','admin/bootstrap-fileinput')); ?>
<?php echo $this->Html->script(array('admin/bootstrap-datetimepicker.min', 'admin/components-pickers')); ?>
<?php echo $this->Html->css(array('admin/datetimepicker')); ?>      
 <?php echo $this->Form->create('WholesaleProduct', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>

<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
       <div class="col-xs-12">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add Wholesale Product
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>

        <div class="form-body">
            <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Select Department
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('fuel_department_id', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'empty' => 'Select Department')); ?>
                                    
                                </div>
                            </div>

                           
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Product Name:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Product Name')); ?>

                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Description:			 
                                </div>
                                <div class="col-md-7 value">
                                   <?php echo $this->Form->input('description', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Description')); ?>

                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Status:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("Enable" => "Enable", "Disable" => "Disable"))); ?>

                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <!--<a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Select Vender:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('customer_id', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'empty' => 'Select Vendor')); ?>
        </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Price Per Gallon:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('price', array('class' => 'form-control', 'label' => false, 'onkeyup'=>'product_value()' , 'onblur'=>"product_value()","onfocus"=>"myFunction(this)" , 'required' => 'false', 'placeholder' => 'Enter Price Per Gallon')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Vendor Price:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('vendor_price', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Vendor Price')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tax Class:
                                </div>
                                <div class="col-md-7 value">
                                   <?php echo $this->Form->input('tax_class', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'empty' => 'Select Tax Class',"type"=>"select","options"=>array("Taxable"=>"Taxable","Non Taxable"=>"Non Taxable"))); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Qty Available in Gallon:	 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('qty', array('class' => 'form-control', 'label' => false, 'onkeyup'=>'product_value()' , 'onblur'=>"product_value()" ,"onfocus"=>"myFunction(this)"  ,  'required' => 'false', 'placeholder' => 'Enter Qty')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Opening Book Inventory: 
                                </div>
                                <div class="col-md-7 value">
                                     <?php echo $this->Form->input('opening_product_amount', array('label' => false, 'value' => 0, 'class' => 'form-control', 'type' => 'text', 'readonly' => true, 'placeholder' => '0')); ?>
                                </div>
                            </div>
   
                        
                        </div></div></div>
               
            </div>
            
            <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption information_product">
                        <i class="fa fa-reorder"></i> UST Information
                    </div>
                    <!--<div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>-->
                </div></div></div>
            
             <div class="row">

                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">

                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Information
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                  Tank No:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('tank_no', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Tank No')); ?>
                                    
                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Capacity:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('tank_capacity', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Tank Capacity')); ?>

                                </div>
                            </div>

                            
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Min Qty:			 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('min_qty', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Min Qty')); ?>

                                </div>
                            </div>
                            

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Max Qty:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('max_qty', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Max Qty')); ?>

                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                <i class="fa fa-cogs"></i>Manage Inventory
                            </div>
                            <div class="actions">
                                <a href="#" class="btn default btn-sm">
                                        <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Piping:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('piping', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'options' => array("EMPTY" => "EMPTY", "Steel" => "Steel","C" => "C", "5435" => "5435"))); ?>



                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                   Tank Type:	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('tank_type', array('class' => 'form-control', 'label' => false,  'required' => 'false', 'placeholder' => 'Enter Tank Type')); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Piping Date:	 
                                </div>
                                <div class="col-md-7 value">
                                    <div class="input-group date form_meridian_datetime" data-date="<?php echo date("Y-m-d"); ?>T15:25:00Z">
                                        <?php echo $this->Form->input('Piping_date', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'div' => false, 'required' => 'false', 'size' => '16', 'readonly' => 'readonly')); ?>
<!--												<input type="text" size="16" readonly class="form-control">-->
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                        </span>
                                        <span class="input-group-btn extra_button">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>

                                </div>
                            </div>
   
                        </div></div></div>
               
            </div>



            <!-- END FORM-->
        </div>
       </div>
    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
        <button type="reset" class="btn default">Reset</button>
        <button type="button" onclick="javascript:history.back(1)"
                class="btn default">Cancel</button>

        <!--<button type="button" class="btn default">Cancel</button>-->
    </div>

</div>
</div>
<?php echo $this->form->end(); ?>

<script>
    $(document).ready(function () {
        // initiate layout and plugins
        //App.init();
        ComponentsPickers.init();
       product_value();
      
    });
    
    
    function product_value(){
        var ProductOpeningProductAmount = 0 ;
        var ProductPrice = parseFloat(jQuery('#WholesaleProductPrice').val());
        var ProductQty = parseFloat(jQuery('#WholesaleProductQty').val());
         ProductOpeningProductAmount = parseFloat(ProductPrice * ProductQty);
        if(ProductOpeningProductAmount == null || ProductOpeningProductAmount == '' || isNaN(ProductOpeningProductAmount) ){
            ProductOpeningProductAmount = 0;
        }
         parseFloat(jQuery('#WholesaleProductOpeningProductAmount').val(ProductOpeningProductAmount));
    }
    
    function myFunction(x) {
        var value = x.value;
        if(value == 0){
         x.value = "";
      }
     }
    
    </script>



