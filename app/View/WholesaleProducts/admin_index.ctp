<div class="page-content-wrapper">
<div class="portlet box blue">
               <div class="page-content portlet-body" >


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Wholesale Products <span class="btn green fileinput-button">
                    <?php echo $this->Html->link('<i class="fa fa-plus"></i>  <span>  Add New</span>', array( 'action' => 'admin_add'), array('escape' => false)); ?>
                    </span>

                </h3>

                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-full-width">

                    <div class="tab-content">

                        <div id="tab_1_5" class="tab-pane1">
                           
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
										   <th><?php echo __($this->Paginator->sort('id')); ?>  </th>
                                            <th><?php echo __($this->Paginator->sort('name')); ?>  </th>
											<th><?php echo __($this->Paginator->sort('price')); ?>  </th>
											<th><?php echo __($this->Paginator->sort('fuel_department_id','Department')); ?>  </th>
											<th><?php echo __($this->Paginator->sort('qty','Qty(Gallon)')); ?>  </th>
											<th><?php echo __($this->Paginator->sort('created','Created Date')); ?>  </th>
                                           
                                            <th> Actions </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($wholesaleProducts) && !empty($wholesaleProducts)) { ?>
                                           <?php foreach ($wholesaleProducts as $wholesaleProduct){ ?>
                                                <tr>
												    <td><?php echo h($wholesaleProduct['WholesaleProduct']['id']); ?>&nbsp;</td>
                                                    <td><?php echo h($wholesaleProduct['WholesaleProduct']['name']); ?>&nbsp;</td>
													
													<td><?php echo h($wholesaleProduct['WholesaleProduct']['price']); ?>$</td>
													<td><?php echo h($wholesaleProduct['FuelDepartment']['name']); ?>&nbsp;</td>
													
													<td><?php echo h($wholesaleProduct['WholesaleProduct']['qty']); ?>&nbsp;</td>
													
													<td><?php echo date('F d,Y',strtotime($wholesaleProduct['WholesaleProduct']['created'])); ?>&nbsp;</td>
                                                   
                                                    <td>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/view.png"/>', array('action'=>'view',$wholesaleProduct['WholesaleProduct']['id']) ,array('escape' => false,'class'=>'newicon red-stripe view')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/edit.png"/>', array('action'=>'edit',$wholesaleProduct['WholesaleProduct']['id']) ,array('escape' => false,'class'=>'newicon red-stripe edit')); ?>
                                                        <?php echo $this->Html->link('<img src="'.Router::url('/').'img/delete.png"/>', array('action'=>'delete',$wholesaleProduct['WholesaleProduct']['id']) ,array('escape' => false,'class'=>'newicon red-stripe delete')); ?>
                                                        
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php }else { ?>
                                            <tr>
                                                    <td colspan="9">  no result found </td>
                                                </tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="margin-top-20">
                                <ul class="pagination">
                                    <li>
                                        <?php echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('escape' => false)); ?> 
                                    </li>
                                             <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>                                    
                                    <li> 
                                        <?php echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('escape' => false)); ?> 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
            <!--end tabbable-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    </div>
</div>
<style>
    .current{
        background: rgb(238, 238, 238) none repeat scroll 0 0;
border-color: rgb(221, 221, 221);
color: rgb(51, 51, 51);
border: 1px solid rgb(221, 221, 221);
float: left;
line-height: 1.42857;
margin-left: -1px;
padding: 6px 12px;
position: relative;
text-decoration: none;
    }
</style>
