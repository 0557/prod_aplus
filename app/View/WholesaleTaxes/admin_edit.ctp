<?php echo $this->Form->create('WholesaleTax', array('role' => 'form', 'type' => 'file'), array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false))); ?>
<div class="portlet box blue">
               <div class="page-content portlet-body" >
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Edit Wholesale Tax
                    </div>
                </div>
            </div>
        </div>

        <div class="form-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow box">
                        <div class="portlet-title"></div>
                        <div class="portlet-body extra_tt">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Select Tax Zone:
                                </div>
                                <div class="col-md-7 value">
                                    <?php $options = array('Federal'=>'Federal','County'=>'County','State'=>'State','Municipality'=>'Municipality'); ?>
                                    <?php echo $this->Form->input('tax_zone', array('class' => 'form-control', 'label' => false, 'type'=>'select','options'=>$options,'empty'=>'Select Tax Zone' ,'required' => 'false')); ?>
                                     <?php echo $this->Form->input('id'); ?>
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Select State :	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('state_id', array('class' => 'form-control', 'required' => 'false', 'label' => false, 'empty' => 'Select State')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="portlet blue box">
                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Tax Description:
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('description', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Tax Description')); ?>
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 name">
                                    Tax Rate % :	 
                                </div>
                                <div class="col-md-7 value">
                                    <?php echo $this->Form->input('rate', array('class' => 'form-control', 'label' => false, 'required' => 'false', 'placeholder' => 'Enter Tax Rate')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
    <div class="form-actions" style="text-align:center;">
        <button type="submit" class="btn blue">Submit</button>
<!--        <button type="reset" class="btn default">Reset</button>-->
        <button type="button" onclick="javascript:history.back(1)" class="btn default">Cancel</button>
    </div>
</div>
</div>
<?php echo $this->form->end(); ?>


<script>
    $("input , textarea").keyup(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });

    $("select , input").change(function () {
        if ($(this).next().hasClass('error-message')) {
            $(this).next().hide('slow', function () {
                $(this).next().remove();
            });
        }
    });
</script>




