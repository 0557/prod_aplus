<div class="portlet box blue">
               <div class="page-content portlet-body" >

    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>  View Tax Info - <?php echo $wholesaleTax['WholesaleTax']['id']; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless tabbable-reversed">
                <div class="tab-content">
                    <div class="tab-pane" id="tab_3">
                        <div class="portlet box blue">
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet yellow box">
                                                <div class="portlet-title">

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Tax Zone:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $wholesaleTax['WholesaleTax']['tax_zone']; ?>

                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            State:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $wholesaleTax['State']['name']; ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="portlet blue box">
                                                <div class="portlet-title">
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Tax Description: 
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $wholesaleTax['WholesaleTax']['description']; ?>
                                                        </div>
                                                    </div>


                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Tax Rate %:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <?php echo $wholesaleTax['WholesaleTax']['rate']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
