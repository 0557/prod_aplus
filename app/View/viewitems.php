<?php
$CI = & get_instance();

$baseurl = $CI->config->item("base_url");
?>
<?php $this->load->view('admin/includes/header'); ?>


<?php $this->load->view('admin/includes/sidebar'); ?>
<?php $this->load->view('admin/includes/sidebartop'); ?>
<script src="http://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"></script>
<script src="http://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script>


    $(document).ready(function () {
	$('#example').DataTable();
    });
</script>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
	<div class="page-title">
	    <div class="title_left">
		<h3>
                    View Content Pages

                </h3>
	    </div>


	</div>
	<div class="clearfix"></div>

	<div class="row">

	    <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">

		    <div class="x_content">
			<table id="example" class="table table-striped responsive-utilities jambo_table">
			    <thead>
				<tr class="headings">

				    <th>S No</th>
				    <th>Title</th>
				    <th>price</th>
				    <th>Short_Desc</th>
				    <th>Description</th>
				    <th>Image </th>
				    <th>Type </th>
				    <th>Status </th>
				    <th class=" no-link last"><span class="nobr">Action</span>
				    </th>
				</tr>
			    </thead>

			    <tbody>
				<?php
				$query = $this->db->query("SELECT * FROM tr_items");
				foreach ($query->result() as $row) {
				    ?>
    				<tr class="even pointer">

    				    <td class=""><?php echo $row->id; ?></td>
    				    <td class=""><?php echo $row->title; ?></td>
    				    </td>
    				    <td class=""><?php echo $row->cost; ?></td>
    				    </td>
    				    <td class=""><?php echo $row->description; ?></td>
    				    <td class=""><?php echo $row->short_description; ?></td>
    				    <td class=""><?php if ($row->image != "") { ?><img src="<?php echo $baseurl; ?>pages/<?php echo $row->image; ?>" style="width:100px;height:100px;"><?php
					    } else {
						echo "No Image";
					    }
					    ?></td>
    				    <td class=""><?php echo $row->type; ?></td>
    				    <td class=""><?php echo $row->status; ?></td>

    				    <td class=" last">
    					<a class="btn btn-primary" href="<?php echo $baseurl; ?>index.php/admin/editingitem?id=<?php echo $row->id; ?>">Edit</a>
    					<a class="btn btn-danger" onClick="return confirm('Are you sure want to delete it?')" href="<?php echo $baseurl; ?>index.php/admin/deleteitem?id=<?php echo $row->id; ?>">Delete</a>

    				    </td>
    				</tr>
				<?php } ?>
			    </tbody>

			</table>
		    </div>
		</div>
	    </div>

	</div>
    </div>
    <!-- footer content -->

    <!-- /footer content -->

</div>
<!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<?php $this->load->view('admin/includes/jsfiles'); ?>

<!-- Datatables -->

</body>

</html>