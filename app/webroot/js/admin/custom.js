
jQuery(document).ready(function () {

    // binds form submission and fields to the validation engine
    $('#AjaxCountry').change(function () {
        var country_id = $(this).val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: SITEURL + 'admin/stores/states/' + country_id,
            data: 'country_id=' + country_id,
            beforeSend: function () {
                App.blockUI({
                    target: '#Ajax_State',
                    iconOnly: true
                });
            },
            success: function (data) {
                App.unblockUI('#Ajax_State');
                var html = '<option value="">Select State</option>';
                $.each(data, function (key, value) {
                    html += '<option value=' + key + '>' + value + '</option>';
                });
                $('#AjaxState').html(html);
            }
        });

    });

    jQuery(document).delegate('#AjaxState', 'change', function () {
        //jQuery("#CorporationStateId").change(function () {
        var state_id = $(this).val();
        // alert(state_id);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: SITEURL + 'admin/stores/cities/' + state_id,
            data: 'country_id=' + state_id,
            beforeSend: function () {
                App.blockUI({
                    target: '#Ajax_City',
                    iconOnly: true
                });
            },
            success: function (data) {
                App.unblockUI('#Ajax_City');
                var html = '<option value="">Select City</option>';
                $.each(data, function (key, value) {
                    html += '<option value=' + key + '>' + value + '</option>';
                });
                $('#AjaxCity').html(html);
            }
        });
    });

    jQuery(document).delegate('#AjaxCity', 'change', function () {

        //jQuery("#CorporationStateId").change(function () {
        var city = $("#AjaxCity option:selected").text();
        //    var state = $("#AjaxState option:selected").text();
        // alert(state_id);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: SITEURL + "admin/stores/zipcode/" + city,
            beforeSend: function () {
                App.blockUI({
                    target: '#Ajax_ZipCode',
                    iconOnly: true
                });
            },
            // data: 'country_id=' + state_id,
            success: function (data) {
                App.unblockUI('#Ajax_ZipCode');
                var html = '<option value="">Select ZipCode</option>';
                $.each(data, function (key, value) {
                    html += '<option value=' + key + '>' + value + '</option>';
                });
                $('#AjaxZipCode').html(html);
            }
        });
    });
});
